#!/usr/bin/python

##
## This is the script to copy resources from BOX To our project path
##
##

# imports
import os;
import shutil;

boxPath = "";

def createDir(path):
    if os.path.exists(path) == False:
        os.mkdir(path);

def copyFile(srcFile, dstFile):
    shutil.copy(srcFile, dstFile);

def setupPath():
    if(setupRootPath() < 0):
        return -1;

    if(setupSourcePath() < 0):
        return -1;


    setupTargetPath();

    return 0;

# Check
def setupBoxPath():
    path = os.getenv('BOX_PATH', "");
    # print "boxPath: " + path;
    if(path == ""):
        print "Please define the box path to environment 'BOX_PATH'";
        print "e.g add export BOX_PATH=/Users/kenlee/Box\ Sync/ in your .bash_profile";
        print " and then run 'source ~/.bash_profile' to update it"
        return -1;

    if(os.path.exists(path) == False):
        print "Box Path '%s' not exist, please check again. " % path;
        return -1;

    global boxPath;
    boxPath = path;

    return 0;

def setupRootPath():
    scriptDir = os.path.dirname(os.path.realpath(__file__));
    global projectRoot, cocosRoot;

    projectRoot  = os.path.dirname(scriptDir);
    cocosRoot = os.path.join(projectRoot, "CocosGUI");

    print "scriptDir=%s proj=%s cocos=%s" % (scriptDir, projectRoot, cocosRoot);


    if(os.path.exists(cocosRoot) == False):
        print "CocosRoot Path '%s' not exist, please define the link using 'ln -s <your cocosstudio path> ./CocosGUI'" % cocosRoot;
        return -1;

    return 0;

def setupSourcePath():
    global cocosRoot, srcResPath;

    srcResPath = os.path.join(cocosRoot, "Animation/res");

    print "Source Information"
    print "Root Path          : " + cocosRoot;
    print "Resource Path      : " + srcResPath;

    if(os.path.exists(srcResPath) == False):
        print "Resource Path '%s' not exist, please check again. " % srcResPath;
        return -1;

    return 0;

def checkFile(filename):
    acceptList = [".png", ".jpg", ".plist", ".csb", ".ttf", ".fnt"];
    excludeList = ["(info@aurorainno.com)"];

    for item in excludeList:
        if( item in filename):
            return False;


    for item in acceptList:
        if(filename.endswith(item)):
            return True;

    return False;

def copyResource():
    global srcResPath, targetResPath;

    # copy the file srcResPath to targetResPath

    for path, dirs, files in os.walk(srcResPath):
        # path example: import/res/drawable
        print "\npath=%s" % path;

        # for item in dirs:
        #     # basename example: drawable
        #     #basename = os.path.basename(path);
        #     print "dir: %s" % item;



        for item in files:
            # basename example: drawable
            #basename = os.path.basename(path);
            if(checkFile(item) == False):
                continue;

            relativePath = path.replace(srcResPath, "");
            targetPath = targetResPath + relativePath + "/";
            srcFile = path + "/" + item;

            createDir(targetPath);

            # print "relativePath: %s file: %s" % (relativePath, item);
            print "[%s] -> [%s]" % (srcFile, targetPath);
            copyFile(srcFile, targetPath);





def setupTargetPath():
    global rootPath, targetResPath;

    scriptPath = os.path.dirname(os.path.realpath(__file__));

    rootPath = os.path.dirname(scriptPath);
    targetResPath = os.path.join(rootPath, "Resources/res");

    print "Target Information"
    print "Project Root       : " + rootPath;
    print "Target Res Path    : " + targetResPath;

    # Create the directory
    createDir(targetResPath);


# Main
def main():
    print "Danball Resource Update Script";
    print "~ copy the resource from CocosStudio to project resource ~";

    isOkay = setupRootPath();
    if(isOkay < 0):
        quit();


# Testing
def test_checkBoxPath():
    setupBoxPath();
    print "SetupPath";
    setupSourcePath();
    setupTargetPath();

def test_copy_resources():
    setupPath();
    copyResource();
    print "";

def test_setupRootPath():
    setupRootPath();


def test():
    print "Start Testing";
    # test_checkBoxPath();
    # test_setupRootPath();
    test_copy_resources();


#
#setupPath();
#main();
test();
