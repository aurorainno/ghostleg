#!/usr/bin/python

##
## A script generate State Logic
##
##

# imports
import os;
import sys;
import shutil;

CLASS_FOLDER_PATH = "Classes/Model/StateLogic"      # Related the Root
#CLASS_FOLDER_PATH = "script"           # This is testing path
TEMPLATE_NAME = "StateLogic"


sourceContent = "";
headerContent = "";

scriptPath = os.path.dirname(os.path.realpath(__file__));
rootPath = os.path.dirname(scriptPath);

targetClassPath = "";
targetSourceFile = "";
targetHeaderFile = "";

stateName = "";
className = "";

isTargetSourceExist = False;
isTargetHeaderExist = False;

# Read the content of the given file
def readFile(filename):
  fo = open(filename, "r");
  str = fo.read();
  fo.close();
  return str;

def writeFile(filename, content):
  fo = open(filename, "w");
  fo.write(content);
  fo.close();


def createDir(path):
    if os.path.exists(path) == False:
        os.mkdir(path);

def loadTemplate():
    global sourceContent, headerContent;

    filenamePrefix = TEMPLATE_NAME;

    sourceFile = scriptPath + "/template/" + filenamePrefix + ".cpp.template";
    headerFile = scriptPath + "/template/" + filenamePrefix + ".h.template";

    print "DEBUG: %s" % sourceFile;
    print "DEBUG: %s" % headerFile;

    sourceContent = readFile(sourceFile);
    headerContent = readFile(headerFile);

    return True;

def generateSource():
    global stateName, className;
    global sourceContent, headerContent;
    global targetSourceFile, targetHeaderFile;

    finalSource = sourceContent.replace("#ClassName#", className);
    finalHeader = headerContent.replace("#ClassName#", className);

    #print "%s\n\n----\n" % finalSource;
    #print "%s\n\n----\n" % finalHeader;

    writeFile(targetSourceFile, finalSource);
    writeFile(targetHeaderFile, finalHeader);


def setupPath():
    global stateName, className;
    global targetClassPath, targetSourceFile, targetHeaderFile;
    global isTargetHeaderExist, isTargetSourceExist;

    className = "StateLogic" + stateName;

    targetClassPath = rootPath + "/" + CLASS_FOLDER_PATH;
    targetSourceFile = targetClassPath + "/" + className + ".cpp";
    targetHeaderFile = targetClassPath + "/" + className + ".h";

    isTargetSourceExist = os.path.exists(targetSourceFile);
    isTargetHeaderExist = os.path.exists(targetHeaderFile);

# Main




def main():
    # Main Entry
    global stateName;

    if len(sys.argv) <= 1:
        print("Usage:")
        print("%s (StateName)" % sys.argv[0])
        quit()

    stateName = sys.argv[1]

    print "Create new StateLogic '%s'" % stateName;

    setupPath();
    info();

    # ERROR Checking
    if isTargetSourceExist:
        print "Target Source already exist. please delete it first";
        quit();

    if isTargetHeaderExist:
        print "Target Header already exist. please delete it first";
        quit();

    # Core Logic : Load Template & Write the final source
    loadTemplate();
    generateSource();

def info():
    global className, targetClassPath, targetSourceFile, targetHeaderFile;
    global isTargetHeaderExist, isTargetSourceExist;
    print "ClassName        : %s" % className;
    print "Class folder     : %s" % targetClassPath;
    print "Source           : %s" % targetSourceFile;
    print "Header           : %s" % targetHeaderFile;
    print "Source Exist?    : %d" % isTargetSourceExist;
    print "Header Exist?    : %d" % isTargetHeaderExist;

# Testing
def test_loadTemplate():
    loadTemplate();
    print "%s" % sourceContent;
    print "%s" % headerContent;

def test_setupPath():

    setupPath();
    info();

def test_generateSource():
    setupPath();
    info();
    loadTemplate();
    generateSource();

def test():
    global stateName;
    stateName = "OrderSelect"
    print "Start Testing";
    # test_loadTemplate();
    # test_setupPath();
    test_generateSource();


#
#setupPath();
main();
#test();
