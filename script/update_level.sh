#!/bin/sh

BOX_PATH=/Users/kenlee/box/

GHOST_LEG_SRC=$BOX_PATH/project/Astrodog/LevelData/ghostleg
GHOST_LEG_PROJECT=/workspace/auro/game/project/GhostLeg/mapTool
GHOST_LEG_PROJECT_DATA=$GHOST_LEG_PROJECT/data

# This is for checking
#ls "$GHOST_LEG_SRC_SPRITE"
# ls "$GHOST_LEG_CCS_PROJECT_PATH"
#ls $GHOST_LEG_SRC_SPRITE/*.plist

# Copy the data files
cp $GHOST_LEG_SRC/*.tmx $GHOST_LEG_PROJECT_DATA
cp $GHOST_LEG_SRC/*.tsx $GHOST_LEG_PROJECT_DATA
cp $GHOST_LEG_SRC/tile/*.png $GHOST_LEG_PROJECT_DATA/tile

# Run the Script to generate data
cd $GHOST_LEG_PROJECT
./createLevelData.py
