#!/usr/bin/python

##
## This is the script to copy stage data from BOX To our project path
##
##

# imports
import os;
import shutil;
import sys
import subprocess


boxPath = "";
projectStagePath = "project/monsterEx/LevelData/stage";
stageDataPath = "Resources/level/stage"



def createDir(path):
    if os.path.exists(path) == False:
        os.mkdir(path);

def copyFile(srcFile, dstFile):
    shutil.copy(srcFile, dstFile);


def isStageFile(filename):
    acceptList = [".json"];
    excludeList = ["(info@aurorainno.com)"];

    for item in excludeList:
        if( item in filename):
            return False;


    for item in acceptList:
        if(filename.endswith(item)):
            return True;

    return False;

# Check
def checkBoxPath():
    path = os.getenv('BOX_PATH', "");
    # print "boxPath: " + path;
    if(path == ""):
        print "Please define the box path to environment 'BOX_PATH'";
        print "e.g add export BOX_PATH=/Users/kenlee/Box\ Sync/ in your .bash_profile";
        print " and then run 'source ~/.bash_profile' to update it"
        return -1;

    if(os.path.exists(path) == False):
        print "Box Path '%s' not exist, please check again. " % path;
        return -1;

    global boxPath;
    boxPath = path;

    return 0;

def setupPath():
    global projectStagePath, stageDataPath;

    global boxPath, rootPath, resPath, srcResPath, targetResPath;
    global srcDataPath, targetDataPath, mapToolPath, mapToolDataPath;

    # root path
    scriptPath = os.path.dirname(os.path.realpath(__file__));
    rootPath = os.path.dirname(scriptPath);

    # source data
    srcDataPath = os.path.join(boxPath, projectStagePath);

    # target data
    targetDataPath = os.path.join(rootPath, stageDataPath);

    print "Path Information"
    print "Project Root       : " + rootPath;
    print "Box Path           : " + boxPath;
    print "Source Path        : " + srcDataPath;
    print "Target Res Path    : " + targetDataPath;


def copyStageData():
    global srcDataPath, targetDataPath;

    # copy the file srcResPath to targetResPath

    for path, dirs, files in os.walk(srcDataPath):
        # path example: import/res/drawable
        #print "\npath=%s dirs=%s" % (path, dirs);

        # for item in dirs:
        #     # basename example: drawable
        #     #basename = os.path.basename(path);
        #     print "dir: %s" % item;

        relativePath = path.replace(srcDataPath, "");
        if(relativePath != ""):
            continue;

        for item in files:
            # basename example: drawable
            #basename = os.path.basename(path);
            if(isStageFile(item) == False):
                continue;


            targetPath = targetDataPath + "/";
            srcFile = path + "/" + item;

            createDir(targetPath);

            # print "relativePath: %s file: %s" % (relativePath, item);
            #print "DEBUG: path=[%s]" % relativePath;
            print "Copying [%s] -> [%s]" % (srcFile, targetPath);
            copyFile(srcFile, targetPath);

# Main
def main():
    print "Astrodog Stage Data Update Script";
    print "~ copy the resource from Astrodog Box Path to project resource ~";

    if(checkBoxPath() < 0):
        quit();

    print "Box Path: " + boxPath;

    setupPath();
    copyStageData();

#
#setupPath();
main();
#test();
