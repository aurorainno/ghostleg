#!/usr/bin/python

##
## This is the script to copy resources from BOX To our project path
##
##

# imports
import os;
import shutil;
import sys
import subprocess


boxPath = "";
projectLevelPath = "project/monsterEx/LevelData/map";
mapDataPath = "Resources/level/mapData"



def createDir(path):
    if os.path.exists(path) == False:
        os.mkdir(path);

def copyFile(srcFile, dstFile):
    shutil.copy(srcFile, dstFile);


def isMapFile(filename):
    acceptList = [".tmx", ".tsx"];
    excludeList = ["(info@aurorainno.com)"];

    for item in excludeList:
        if( item in filename):
            return False;


    for item in acceptList:
        if(filename.endswith(item)):
            return True;

    return False;

# Check
def checkBoxPath():
    path = os.getenv('BOX_PATH', "");
    # print "boxPath: " + path;
    if(path == ""):
        print "Please define the box path to environment 'BOX_PATH'";
        print "e.g add export BOX_PATH=/Users/kenlee/Box\ Sync/ in your .bash_profile";
        print " and then run 'source ~/.bash_profile' to update it"
        return -1;

    if(os.path.exists(path) == False):
        print "Box Path '%s' not exist, please check again. " % path;
        return -1;

    global boxPath;
    boxPath = path;

    return 0;

def setupPath():
    global projectLevelPath, mapDataPath;

    global boxPath, rootPath, resPath, srcResPath, targetResPath;
    global srcDataPath, targetDataPath, mapToolPath, mapToolDataPath;

    # root path
    scriptPath = os.path.dirname(os.path.realpath(__file__));
    rootPath = os.path.dirname(scriptPath);

    # source data
    srcDataPath = os.path.join(boxPath, projectLevelPath);

    # target data
    targetDataPath = os.path.join(rootPath, mapDataPath);

    # Map
    mapToolPath = os.path.join(rootPath, "mapTool");
    mapToolDataPath = os.path.join(mapToolPath, "mapTmx");

    print "Path Information"
    print "Project Root       : " + rootPath;
    print "Box Path           : " + boxPath;
    print "Source Path        : " + srcDataPath;
    print "Target Res Path    : " + targetDataPath;
    print "Map Tool Path      : " + mapToolPath;
    print "Map Tool Data Path : " + mapToolDataPath;

def copyTileImages():
    global srcDataPath, mapToolDataPath;

    # copy the file srcResPath to targetResPath
    acceptPath = ["enemy", "npc"];
    excludePrefix = ["xxx", ".DS"];

    for path, dirs, files in os.walk(srcDataPath):
        basepathname = os.path.basename(path);
        #print "path=%s dirs=(%s) files=(%s) basepathname=(%s)" % (path, dirs, files, basepathname);

        relativePath = path.replace(srcDataPath, "");

        if (basepathname in acceptPath) == False:
            continue;

        for item in files:

            tokens = item.split("_");
            prefix = tokens[0];
            if (prefix in excludePrefix):
                continue;
            #print "item=%s path=%s prefix=%s" % (item, path, prefix);

            targetPath = mapToolDataPath + relativePath + "/";
            srcFile = path + "/" + item;

            createDir(targetPath);

            #print "Copying [%s] -> [%s]" % (srcFile, targetPath);
            copyFile(srcFile, targetPath);

def copyMapData():
    global srcDataPath, mapToolDataPath;

    # copy the file srcResPath to targetResPath

    for path, dirs, files in os.walk(srcDataPath):
        # path example: import/res/drawable
        #print "\npath=%s dirs=%s" % (path, dirs);

        # for item in dirs:
        #     # basename example: drawable
        #     #basename = os.path.basename(path);
        #     print "dir: %s" % item;

        relativePath = path.replace(srcDataPath, "");
        if(relativePath != ""):
            continue;

        for item in files:
            # basename example: drawable
            #basename = os.path.basename(path);
            if(isMapFile(item) == False):
                continue;


            targetPath = mapToolDataPath + relativePath + "/";
            srcFile = path + "/" + item;

            createDir(targetPath);

            # print "relativePath: %s file: %s" % (relativePath, item);
            #print "DEBUG: path=[%s]" % relativePath;
            print "Copying [%s] -> [%s]" % (srcFile, targetPath);
            copyFile(srcFile, targetPath);

def convertLevelFile():
    global mapToolPath;

    print "Convert Level File";

    os.chdir(mapToolPath);

    cmd = "python ./convertMap.py";
    os.system(cmd)

# Main
def main():
    print "Astrodog Level Data Update Script";
    print "~ copy the resource from Astrodog Box Path to project resource ~";

    if(checkBoxPath() < 0):
        quit();

    print "Box Path: " + boxPath;

    setupPath();
    copyMapData();
    copyTileImages();
    convertLevelFile();

# Testing
def test_checkBoxPath():
    checkBoxPath();
    print "SetupPath";
    setupPath();

def test_copyResource():
    checkBoxPath();
    print "SetupPath";
    setupPath();
    copyMapData();

def test_convert():
    setupPath();
    convertLevelFile();

def test_copyTileImages():
    checkBoxPath();
    setupPath();
    copyTileImages();

def test():
    print "Start Testing";
    #test_checkBoxPath();
    test_copyResource();
    #test_convert();
    #test_copyTileImages();


mapFilter = "";
if len(sys.argv) >= 2:
  mapFilter = sys.argv[1];

#
#setupPath();
main();
#test();
