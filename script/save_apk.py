#!/usr/bin/python

# ------------------------------
# Import the resource folder
#
#
# ------------------------------

## Import

import sys
import os
import shutil			# for file copy

## Search path (Relative Name to ~/Project/ )
apk_search_path = [
    "proj.android-studio/app/build/outputs/apk/GhostLeg-release.apk",    # By cocos2d-x build script
    "proj.android-studio/app/GhostLeg-release.apk"                          # By Android Studio
];


## Copy File
def copy_file(src_file, dst_file):

  # check for the parent folder
  parent_dir = os.path.dirname(dst_file)

  #print("parent_dir: %s" % parent_dir)
  if(os.path.exists(parent_dir) == False):
    os.makedirs(parent_dir)

  # copy the file
  shutil.copy(src_file, dst_file)
#end

##

def copy_resource(src, dst):
  for path, dirs, files in os.walk(src):
    #print("path: %s | %s  | %s\n" % (path, dirs, files))

    # path example: import/res/drawable
    for item in files:
      # basename example: drawable
      basename = os.path.basename(path)

      src_file = os.path.join(path, item)
      dst_file = os.path.join(dst, basename, item)

      #
      # debug
      print("src_file: [%s] dst_file: [%s]" % (src_file, dst_file))

      copy_file(src_file, dst_file)


#end

## Copy Resource

def remove_quote(input):
    result = input.replace("\"", "");

    return result;


##
def grep_build_info(main_path):
    gradle_prop_file = get_gradle_filepath();

    print("gradle.prop=%s" % (gradle_prop_file));
    if(os.path.exists(gradle_prop_file) == False):
        print("Missing gradle properties file. path=" + gradle_prop_file);
        return False;

    content = read_file(gradle_prop_file);

    lines = content.splitlines();

    for line in lines:
        line = line.strip();
        # print "DEBUG: " + line;

        token = line.split(" ");
        name = token[0];
        value = "" if len(token) <= 1 else token[1];
        value = remove_quote(value);

        if(name == "versionCode"):
            print "value=" + value;
            build = value;
        elif(name == "versionName"):
            print "value=" + value;
            versionName = value;


    result = {};
    result["versionName"] = versionName;
    result["build"] = build;

    #print "DEBUG: %s " % result;

    return result;

def copy_apk(main_path):
    info = grep_build_info(main_path);

    if(info == False):
        print("Fail to find the build information");
        return;




    apk_file = get_apk_file();
    if(apk_file == ""):
        print("Fail to find a valid apk");
        return;


    dst_path = get_target_apk_path();

    filename = "monstersEx_%s_%s.apk" % (info["versionName"], info["build"]);
    dst_file = os.path.join(dst_path, filename);


    # print "DEBUG: info=%s" % info;
    # print "DEBUG: filename=%s" % filename;
    # print "DEBUG: main_path=" + main_path;
    # print "DEBUG: src=" + apk_file;
    # print "DEBUG: dst_path=" + dst_path;
    # print "DEBUG: dst_file=" + dst_file;

    copy_file(apk_file, dst_file);


def get_apk_file():

    for file in apk_search_path:
        apk_file = os.path.join(main_path, file);
        # print "file: %s" % apk_file;
        if(os.path.exists(apk_file)):
            return apk_file;

    return "none";

def get_main_path():
    pwd = os.path.dirname(sys.argv[0]);
    main_path = os.path.dirname(pwd);

    return os.path.abspath(main_path);

def get_apk_filepath():
    apk_file = os.path.join(main_path, "proj.android-studio/app", "GhostLeg-release.apk");

    return apk_file;

def get_target_apk_path():
    apk_path = os.path.join(main_path, "../apk");

    return apk_path;

def get_gradle_filepath():
    apk_file = os.path.join(main_path, "proj.android-studio/app", "build.gradle");

    return apk_file;


def read_file(filename):
  fo = open(filename, "r")
  str = fo.read()
  fo.close()
  return str




def show_path_info():
    main = get_main_path();
    apk_file = get_apk_filepath();
    gradle_file = get_gradle_filepath();
    dst_path = get_target_apk_path();

    print "main_path    : %s" % main;
    print "apk_file     : %s" % apk_file;
    print "gradle_file  : %s" % gradle_file;
    print "dst_path     : %s" % dst_path;

## Testing
def test_grep_build_info():
    pwd = os.path.dirname(sys.argv[0]);
    main_path = os.path.dirname(pwd);

    print("pwd=" + main_path);
    info = grep_build_info(main_path);

    print "DEBUG: %s " % info;

def test_copy_apk():
    main_path = get_main_path();

    print("pwd=" + main_path);
    copy_apk(main_path);

def test_get_apk_file():
    file = get_apk_file();
    print("file=" + file);


def run_tests():
    global main_path;

    main_path = get_main_path();
    #test_grep_build_info();
    #test_copy_apk();
    test_get_apk_file();



## end


# -------------- main --------------
#run_tests();
#quit();

# -------------- main --------------
main_path = get_main_path();

show_path_info();
copy_apk(main_path);
