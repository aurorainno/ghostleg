#!/usr/bin/python

##
## This is the script to copy resources from BOX To our project path
##
##

# imports
import os;
import shutil;

boxPath = "";

def createDir(path):
    if os.path.exists(path) == False:
        os.mkdir(path);

def copyFile(srcFile, dstFile):
    shutil.copy(srcFile, dstFile);


def checkFile(filename):
    acceptList = [".png", ".jpg", ".plist", ".csb", ".ttf", ".fnt"];
    excludeList = ["(info@aurorainno.com)"];

    for item in excludeList:
        if( item in filename):
            return False;


    for item in acceptList:
        if(filename.endswith(item)):
            return True;

    return False;

# Check
def checkBoxPath():
    path = os.getenv('BOX_PATH', "");
    # print "boxPath: " + path;
    if(path == ""):
        print "Please define the box path to environment 'BOX_PATH'";
        print "e.g add export BOX_PATH=/Users/kenlee/Box\ Sync/ in your .bash_profile";
        print " and then run 'source ~/.bash_profile' to update it"
        return -1;

    if(os.path.exists(path) == False):
        print "Box Path '%s' not exist, please check again. " % path;
        return -1;

    global boxPath;
    boxPath = path;

    return 0;

def setupPath():
    global boxPath, rootPath, resPath, srcResPath, targetResPath;

    # srcResPath = os.path.join(boxPath, "project/monsterEx/Animation/AnimalAnime/res");
    srcResPath = os.path.join("/workspace/temp/", "AnimalAnimeSpriteSheet/res");
    scriptPath = os.path.dirname(os.path.realpath(__file__));

    rootPath = os.path.dirname(scriptPath);
    targetResPath = os.path.join(rootPath, "Resources/res");

    print "Path Information"
    print "Project Root       : " + rootPath;
    print "Box Path           : " + boxPath;
    print "Source Path        : " + srcResPath;
    print "Target Res Path    : " + targetResPath;

def copyResource():
    global srcResPath, targetResPath;

    # copy the file srcResPath to targetResPath

    for path, dirs, files in os.walk(srcResPath):
        # path example: import/res/drawable
        print "\npath=%s" % path;

        # for item in dirs:
        #     # basename example: drawable
        #     #basename = os.path.basename(path);
        #     print "dir: %s" % item;



        for item in files:
            # basename example: drawable
            #basename = os.path.basename(path);
            if(checkFile(item) == False):
                continue;

            relativePath = path.replace(srcResPath, "");
            targetPath = targetResPath + relativePath + "/";
            srcFile = path + "/" + item;

            createDir(targetPath);

            # print "relativePath: %s file: %s" % (relativePath, item);
            print "[%s] -> [%s]" % (srcFile, targetPath);
            copyFile(srcFile, targetPath);


# Main
def main():
    print "Danball Resource Update Script";
    print "~ copy the resource from CocosStudio to project resource ~";

    #if(checkBoxPath() < 0):
    #    quit();

    #print "Box Path: " + boxPath;

    setupPath();
    copyResource();

# Testing
def test_checkBoxPath():
    checkBoxPath();
    print "SetupPath";
    setupPath();

def test():
    print "Start Testing";
    test_checkBoxPath();


#
#setupPath();
main();
#test();
