#!/usr/bin/python

##
## This is the script to copy resources from BOX To our project path
##
##

# imports
import os;
import shutil;


def setupPath():
    global cocosRoot, projectRoot, cocosSource;

    scriptDir = os.path.dirname(os.path.realpath(__file__));
    projectRoot  = os.path.dirname(scriptDir);
    cocosRoot = os.path.join(projectRoot, "cocos2d");
    cocosSource = os.path.join(projectRoot, "cocos2d-src");

def copyFile(srcFile, dstFile):
    shutil.copy(srcFile, dstFile);

def copyDir(srcDir, dstDir):
    #copytree(srcDir, dstDir);
    cmd = "cp -R %s %s" % (srcDir, dstDir);
    print "%s" % (cmd)
    os.system(cmd);

# def copytree(src, dst, symlinks=False, ignore=None):
#     names = os.listdir(src)
#     if ignore is not None:
#         ignored_names = ignore(src, names)
#     else:
#         ignored_names = set()
#
#     os.makedirs(dst)
#     errors = []
#     for name in names:
#         if name in ignored_names:
#             continue
#         srcname = os.path.join(src, name)
#         dstname = os.path.join(dst, name)
#         try:
#             if symlinks and os.path.islink(srcname):
#                 linkto = os.readlink(srcname)
#                 os.symlink(linkto, dstname)
#             elif os.path.isdir(srcname):
#                 copytree(srcname, dstname, symlinks, ignore)
#             else:
#                 copy2(srcname, dstname)
#             # XXX What about devices, sockets etc.?
#         except (IOError, os.error) as why:
#             errors.append((srcname, dstname, str(why)))
#         # catch the Error from the recursive copytree so that we can
#         # continue with other files
#         except Error as err:
#             errors.extend(err.args[0])
#     try:
#         copystat(src, dst)
#     except WindowsError:
#         # can't copy file access times on Windows
#         pass
#     except OSError as why:
#         errors.extend((src, dst, str(why)))
#     if errors:
#         raise Error(errors)

def removeUnUsed():
    # Remove by pattern
    dirList = [
        "cocos",
        "build",
        "extensions",
        "external",
    ];

    for dirname in dirList:
        fullpath = os.path.join(cocosRoot, dirname);
        for path, dirs, files in os.walk(fullpath):
            # path example: import/res/drawable
            print "path=%s %s\n" % (path, dirs);

        # for item in dirs:
        #     # basename example: drawable
        #     #basename = os.path.basename(path);
        #     print "dir: %s" % item;



def copyLibrary():
    global cocosRoot, projectRoot, cocosSource;
    copyList = [
        "cocos",
        "build",
        "extensions",
        "external",
    ];

    for item in copyList:
        print "item=%s" % item;
        item = item + "/";
        srcDir = os.path.join(cocosSource, item);
        dstDir = os.path.join(cocosRoot, item);
        copyDir(srcDir, dstDir);

setupPath();
print "cocos=%s" % (cocosRoot);

# copyLibrary();
removeUnUsed();
