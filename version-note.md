Monsters Express Version Note
========================
## 1.1.0
- Collect Coin in different cases can score as well 
	- Cashout
	- Kill Alien
	- Delivered to NPC
	- Collect Coin
- Fix: Puzzle and Booster Count haven't saved
- Fix: SpeedUp will cancel the Booster Effect
- Fix: Remove Coin in NPC map (which cannot be collected)
- Fine Tune the GUI
- Add Booster Dialog when Replay
- Fix: Facebook connect with chinese name 
- Fix: Review url  

## 1.0.1
- Remove the restore purchase button based on the feedback from apple



## 1.0.0 

## 0.27.0
- The Jumping alien animation (move behaviour) updated
- SFX for jumping alien, laser warning, flydown/up alien
- Modify Shop Icons
- Shooter Alien anchor point fixed
- 



## 0.25.0
## Graphics 
- Fix NPC 15
- Scale Tuned: Coin (0.9 -> 0.8)  Magnet (0.5 -> 0.4)  Cashout (0.4) 

## Bugfix
- EnemyBlock not working

## 0.24.0
------------------------------
### Changes (Map and Graphics)
- Stage 1 ~ 5
- Enemy Behaviour 


### Changes
- Polish: Title Screen added BGM & SFX
- Polish: Coin SFX
- Polish: Cyclone Initial Speed
- Polish: Update NPC animation
- Bugfix: Tutorial not giving any reward
- Bugfix: incorrect puzzle, magnet, cashout hitbox


## 0.23.0
------------------------------
### Changes 
- Bugfix
- Add Ad Network: InMobi


## 0.22.0
------------------------------
### Changes 
- Bugfix


## 0.21.0
------------------------------

### Changes
- Tutorial Bugfix and Fine tune
- Updated Clear Stage popup
- Updated Character and Dead Animation
- Updated Coin Animation
- Updated Text for Character Name, Booster Description
- Fix IPAD launch image

## 0.20.0
------------------------------
### Gameplay 
- Stage 3 map is updated
- Stage 5 map is updated

### Changes 
- Ranking Server change from uid to playerID
- Facebook connect bugfix
- Advertisement bugfix
- Support Invite Friend
- Support iPad  
- Remove Global Leaderboard
- Character animation updated

## More Detail 
- Reward Video will use Adcolony first
- NPC will show emoji instead of "..."
- Fix the zOrder between the enemy / player / ....
- Add "Attack" animation to the Chaser
- Fix Share Score




## 0.19.0
------------------------------
### Gameplay 
- Stage 3 map is updated
- Stage 5 map is updated


### Changes
- Ranking Server change from uid to playerID
- Modify Facebook connect handling
- Support Invite Friend
- Support iPad Resolution 
- Android Build Ready
- Reward Video will use Adcolony first
- Remove Global Leaderboard
- NPC will show emoji instead of "..."
- Fix the zOrder between the enemy / player / ....
- Add "Attack" animation to the Chaser
- Fix Share Score



$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
--------------------------
Bring from AstroDog!!!

1.3.x Feature List
--------------------
- 2 New Abilities: Snowball and Speed Up
- 3 New Enemies: Barrier, Snow Meteor, Santa Wolves
- Christmas Planet
- 


1.3.4 (build 114)
----------------------
- Add the Candy Shop (iOS)
- Bugfix the unlock dog showing the christmas dogs
- Christmas Music

1.3.1 (build 111) 
----------------------
- Update the dog name
- Improve the Santa Wolves logic
- Christmas GUI update

1.3.0 (build 110) 
----------------------
- Add the new ability 'snowball' - the capsule logic
- Add the new ability 'speed up' - the capsule logic
- Add the Christmas Map
- Add new enemies 'santa wolves', ice barrier, snow meteor


1.2.6 (build 106) 
----------------------
- bugfix: the quick teleport bug
- bugfix: Cannot change share app in android
- bugfix: Default dog having "Storge" after a game

1.2.4/1.2.5 (build 104-105)	 
----------------------
- Bugfix: item can used after die 
- Bugfix: Default Dog got 'power up' voice
- New Best Score (Main & End Game)
- Impoved Tutorial
- Coachmark for Visit Planet & Dog
- Modification on the Dog Selection Page


1.2.3 (build 103)	 
----------------------
- Polish the Main Screen
- Update the christmas theme
- Add the Hint popup for the main screen
- Modify the Unlock Planet / Dog Layer
- Improve the GA tracking 

1.2.2 (build 102)	 
----------------------
- Improve the speed setting in Earth Stage
- Update the new snowflake particle 
- New Ability (StarMagnet) for grab star during the activated period
- Modify Invulnerable to self activate  (by calvin)


1.2.1 (build 101)	// Note with start with 100 
----------------------
- Add Next Unlock information 
- Add Speed information in Gm and Game screen


1.2.0 (build 100)	// Note with start with 100 
----------------------
- Add the resource for planet
- Improved Tutorial
- Daily Reward
- Christmas Theme
	- Christmas AppIcon
	- Christmas Main Scene

- Improve initial Speed & difficulty 

1.1.8 (build 47)
----------------------
- Improve the speed 
- Improve the Drop Line Gesture 


1.1.7 (build 46)
----------------------
- Bugfix: Incorrect animation for [10. Schnauzer] & [17. Mini Pinscher]
- Improved Level
- Fix the crash when hit rock in map 60069 (contain enemy20)
- Add deep link handling


1.1.6 (build 42 - 45) / Release
----------------------------------------
- Extends the threshold for item and enemies cleanup
- Modify the Space Objects
- Fix save image problem
- Tutorial Message changed
- Review the Dog Achievement setting

1.1.5 (build 41)	/ Release
--------------------------------------------
- Change Double Star to Bonus Star (bonus based on distance & no bonus if distance less than 30)
- Add One more StarPack
- Add different objects for different planets
- Bugfix the mastery & suit description bugs
- Change the Planet Unlock GUI (a bit)
- Update the New level
- Fix the random lines algorithm (pick one from the set line with same color)
- Fine Tune some particle and visual
- Build All Dog Data (price, challenge)

1.1.1 (build 34)	/ Beta 
--------------------------------------------
- Update All Dog animation, id, name, achievement
- Some bugfix and polishing
- Update some sound effects
- Fix Ad bug 
- Update AppIcon 
- Better payment success dialog

1.1.0 (build 28)   / Beta 
--------------------------------------------
- Dog System 
- Player Record
- Planet System 
- Free App Version
- Advertisement Placement 
- 


1.0.27 (build 27)	/ Release
------------------------------------------------------
- bugfix: IAP Bug for Android (Wrong in-app key)

1.0.26 #done
------------------
- bugfix: can pause when die
- Add macro (DISABLE_AD) to disable all ad logic (for android version)

1.0.25 #done
------------------
- Update: version -> 1.0.25
- Android Share: change from jpeg -> png
- Revive will recover energy as well
- Modify the IAP Pricing
- TODO: modify mastery and suit setting

1.0.24 #done
------------------
- Update: version -> 1.0.24
- Fine tune the map data
- Fine tune the share image
- Fix for Android share module

1.0.23 #done
------------------
- Bugfix background music missing
- Update the line color
- Fix Android build
- Fix: still can drop line when no energy
- Fix: Touch line remain on the screen after die
- Update: play fail sound when the line is not complete
- Update: decrease initial player speed -> 130
- Update: version change to 1.0.23
- Update: temp disable GrabStar sound because of strange noise!
- Update: the map 

1.0.22 #done
------------------
- Change the background musics
- Update the line color
- Fix Android build


1.0.21 #done
------------------
- Improve the space background
- Make Option / Coin Scene background moving

1.0.20 #done
1.0.19 #done
------------------
- Add Paid version "BuildConfiguration" & Scheme
- Rename as Astrodog
- Add Paid Version Setting

1.0.18 #done
------------------
- Speed up handling
- Change to Drag Line mode to play
- Add Clean up logic in Model Layer
- Add missile acceleration
- Modified the missile targeting logic
- Debug: use swipe to activate capsule
- Debug: Improve the Ad selection logic to prevent No ad display 


1.0.17 #done
-----------------
- Update: minor bug fix for Android version


1.0.15 #done
-----------------
- Update: Add Facebook Like handling
- Update: Add Dying Animation
- Update: Improve a bit the background parallax scene
- Fix: Die Map GA Logging
- Fix: Save/Get bool in Android


1.0.14 #done
-----------------
- Fix: no regenerate at mastery lv 0 
- Fix: no alert for fall down monster
- Fix: AutoHide monster (res9) struck
- Add: Google Analytics Event & Screen Tracking
- Add: Add Adcolony for reward and interstitual video
- Fix: Not pause our music before play Video Ad
- Fix: Not Enough Coin Ad Handling
- Update: Improved Missile Visual
- Update: Add acceleration for Enemy
- Update: Polish Game Sound Effect
- Fix: Fail to play adcolony in Android
- Fix: Fail to revive in Android

1.0.13 #done
---------------
- Update: Map updated
- Update: Enemy data updated
- Fix: Crash caused by random line in LevelData.shuffleLineVector
- Update: Add Visual Effect when collect the capsule
- Update: Finalize GrabStar / Slow / Timestop UI 
- Add: Gm View for debugging the game
- Add: Slow screen visual effect
- Add: Energy bar visual effect
- Add: Tutorial



1.0.12 #done
---------------
- Added: Admob Advertisement
- Added: Share Score (iOS only)
- Added: Revive After view Ad (One more chance)
- Added: Player Items
- Update: Monster Logic improved
	- Speed supported random
- Update: Map tool, fix incorrect gx->px conversion
- Update: Monster Data file using JSON
- Bugfix: Monster Position (incorrect anchor before)
- Android supported, but contains few bugs

