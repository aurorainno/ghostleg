//
//  LeaderboardRewardLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 24/2/2017.
//
//

#include "LeaderboardRewardLayer.h"
#include "LeaderboardRewardManager.h"
#include "LeaderboardReward.h"
#include "CommonType.h"
#include "CommonMacro.h"
#include "ViewHelper.h"
#include "Analytics.h"


using namespace std;

bool LeaderboardRewardLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    
    
    Node* rootNode = CSLoader::createNode("LeaderboardRewardLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

void LeaderboardRewardLayer::onEnter()
{
    Layer::onEnter();
    vector<LeaderboardReward*> rewardList;
    map<LeaderboardRewardManager::LeaderboardRewardType,
        map<int,vector<LeaderboardReward*>>>                rewardMap = LeaderboardRewardManager::instance()->getRewardMap();
    
    
    
    
    if(mType == Local){
        rewardList = rewardMap[LeaderboardRewardManager::Local][mPlanetID];
    }else{
        rewardList = rewardMap[LeaderboardRewardManager::Global][mPlanetID];
    }
    
//    for(int i=0;i<10;i++){
//        LeaderboardReward* reward = LeaderboardReward::create();
//        if(i<5){
//            reward->setAmount(100);
//            reward->setRewardType(MoneyTypeStar);
//        }else{
//            reward->setAmount(20);
//            reward->setRewardType(MoneyTypeDiamond);
//        }
//        rewardList.push_back(reward);
//    }
    
    setupRewardList(rewardList);
	
	LOG_SCREEN(Analytics::scn_ranking_reward);
}

void LeaderboardRewardLayer::setupUI(cocos2d::Node *rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    mCancelBtn = rootNode->getChildByName<Button*>("backBtn");
    mCancelBtn->addClickEventListener([&](Ref*){
        this->removeFromParent();
    });
    
    for(int i=0;i<10;i++){
        std::string name = StringUtils::format("reward%02d",i+1);
        Node* target = rootNode->getChildByName(name);
        mRewardViewList.push_back(target);
    }
}

void LeaderboardRewardLayer::setupRewardList(std::vector<LeaderboardReward *> rewardList)
{
    int index = 0;
    for(Node* rewardPanel : mRewardViewList){
        Sprite* diamond = rewardPanel->getChildByName<Sprite*>("diamondSprite");
        Sprite* star = rewardPanel->getChildByName<Sprite*>("starSprite");
        Text* amount = rewardPanel->getChildByName<Text*>("amountText");
        LeaderboardReward* data = rewardList.at(index);
        if(data->getRewardType() == MoneyTypeDiamond){
            diamond->setVisible(true);
            star->setVisible(false);
        }else{
            diamond->setVisible(false);
            star->setVisible(true);
        }
        amount->setString(StringUtils::format("%d",data->getAmount()));
        index++;
    }
}


