//
//  FireBulletBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 6/3/2017.
//
//

#include "FireBulletBehaviour.h"
#include "Enemy.h"
#include "StringHelper.h"
#include "EnemyFactory.h"
#include "BulletBehaviour.h"
#include "Player.h"
#include "GameWorld.h"
#include "ModelLayer.h"

#define kBulletEnemyID 80

FireBulletBehaviour::FireBulletBehaviour()
: EnemyBehaviour()
, mAccumTime(0)
, mCooldown(0)
, mOffsetY(0)
, mOffsetX(0)
, mState(Unknown)
{}


void FireBulletBehaviour::onCreate()
{
    updateProperty();
    getEnemy()->setAnimationEndCallFunc(GameModel::Attack, CC_CALLBACK_0(FireBulletBehaviour::onAttack, this));
    getEnemy()->setOnFrameEventCallback([=](const std::string &eventName, EventFrame* event){
        if(eventName == "EmitBullet"){
            fireBullet();
        }
    });
    
    getEnemy()->changeToIdle();
}

void FireBulletBehaviour::onVisible()
{
    
}

void FireBulletBehaviour::onActive()
{
    changeToAttack();
}

void FireBulletBehaviour::onUpdate(float delta)
{
    if(getEnemy()->getEffectStatus() == EffectStatusTimestop||
       getEnemy()->getState() !=  Enemy::State::Active){
        return;
    }
    
    switch (mState) {
        case Idle:
            mAccumTime += delta;
            if(mAccumTime >= mCooldown){
                changeToAttack();
            }
            break;
            
        case Attack:
            break;
            
        default:
            break;
    }
}

void FireBulletBehaviour::fireBullet()
{
    //TODO
//    log("fire bullet!");
    Node* emitNode = getEnemy()->getNodeWithName("EmitNode");
    Vec2 bulletPos = emitNode->convertToWorldSpace(Vec2::ZERO);
    bulletPos.y += GameWorld::instance()->getCameraY();
    
    Player* player = GameWorld::instance()->getPlayer();
    Vec2 playerPos = player->getPosition();
 
    Vec2 targetPos = playerPos + Vec2(mOffsetX,mOffsetY);
    
    Enemy *enemy = Enemy::create();
    enemy->setObjectID(EnemyFactory::instance()->nextObjectID());
    
    // Attach a behaviour to the enemy
    EnemyBehaviour *enemyBehaviour = EnemyFactory::instance()->createBehaviourByMonsterID(kBulletEnemyID);
    BulletBehaviour *behaviour = dynamic_cast<BulletBehaviour* >(enemyBehaviour);
    if(behaviour) {
        behaviour->setEnemy(enemy);
        behaviour->updateProperty();
        behaviour->setNewSpeed(bulletPos, targetPos);
        enemy->setBehaviour(behaviour);
        enemy->setResource(behaviour->getRes());
    } else {
        log("createEnemy: error: changing the id [%d] -> [%d]", kBulletEnemyID, 1);
        enemy->setResource(1);	// resID = monsterId
    }
    
    enemy->setupAttribute();
    
    if(EnemyFactory::instance()->getCurrentEffectStatus() != EffectStatusNone) {
        enemy->setEffectStatus(EnemyFactory::instance()->getCurrentEffectStatus());
    }

    enemy->setPosition(bulletPos);
    enemy->setupHitboxes();
    
    
    GameWorld::instance()->getModelLayer()->addEnemy(enemy);

}

void FireBulletBehaviour::changeToIdle()
{
    getEnemy()->changeToIdle();
    mAccumTime = 0;
    mState = Idle;
}

void FireBulletBehaviour::onAttack()
{
    changeToIdle();
}

void FireBulletBehaviour::changeToAttack()
{
    getEnemy()->setAction(GameModel::Attack);
    mState = Attack;
}

void FireBulletBehaviour::updateProperty()
{
    if(mData == nullptr) {
        log("FireBulletBehaviour::updateProperty :data is null");
        return;
    }
    
    std::map<std::string, std::string> property = mData->getMapProperty("fireBulletSetting");
    
    if(property.find("cooldown") == property.end()) {	// not found
        mCooldown = 2.0f;
    } else {
        mCooldown = aurora::StringHelper::parseFloat(property["cooldown"]);
    }
    
    mOffsetX = aurora::StringHelper::parseFloat(property["offsetX"]);
    mOffsetY = aurora::StringHelper::parseFloat(property["offsetY"]);
    
}

std::string FireBulletBehaviour::toString()
{
    return "FireBulletBehaviour";
}
