//
//  NotEnoughCandyDialog.hpp
//  GhostLeg
//
//  Created by Calvin on 14/12/2016.
//
//

#ifndef NotEnoughCandyDialog_hpp
#define NotEnoughCandyDialog_hpp

#include <stdio.h>
#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NotEnoughCandyDialog : public Layer
{
public:
    
    CREATE_FUNC(NotEnoughCandyDialog);
    
    NotEnoughCandyDialog();
    virtual bool init() override;
    
    void setupUI();
    
    void setPopScene(bool shouldPop); //remove this layer after pressing planetBtn
    
private:
    bool mShouldPopScene;
    Node* mRootNode;
    Button* mShopBtn;
    Button* mPlanetBtn;
    Button* mCancelBtn;
    
};
#endif /* NotEnoughCandyDialog_hpp */
