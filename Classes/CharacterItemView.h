//
//  CharacterItemView.hpp
//  GhostLeg
//
//  Created by Calvin on 14/2/2017.
//
//

#ifndef CharacterItemView_hpp
#define CharacterItemView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "PlayerCharProfile.h"


class AnimeNode;
class DogData;

USING_NS_CC;
using namespace cocos2d::ui;

class CharacterItemView : public Layer
{
public:
    CREATE_FUNC(CharacterItemView);
    virtual bool init();
    
    void setupUI(Node* rootNode);

    void setView(PlayerCharProfile* charProfile);
    
    void setUnlockedView();
    void setLockedView();
    
    void setBtnCallback(std::function<void(int)> callback);
    void setTouchCallback(std::function<void(int, Widget::TouchEventType)> callback);
    void setUnlockCallback(std::function<void(int, Widget::TouchEventType)> callback);
    
    Vec2 getCharCenterPos();
    Vec2 getCharTouchArea();
    
    Vec2 getCharIconWorldPos();
    
private:
    CC_SYNTHESIZE_RETAIN(PlayerCharProfile*, mPlayerProfile, PlayerProfile);
    Node *mLockedPanel, *mUnlockedPanel;
    Node *mRootNode;
    AnimeNode *mCharAnimeNode;
    
    Layout *mTouchPanel;
    std::function<void(int)> mBtnCallback;
    std::function<void(int, Widget::TouchEventType)> mTouchCallback;
    std::function<void(int, Widget::TouchEventType)> mUnlockCallback;
};


#endif /* CharacterItemView_hpp */
