//
//  BoosterControl.cpp
//  GhostLeg
//
//  Created by Ken Lee on 24/1/2017.
//
//

#include "BoosterControl.h"
#include "CircularMaskNode.h"
#include "ItemManager.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "Player.h"

namespace {
	std::string getBoosterImage(BoosterItemType type) {
        switch(type) {
            case BoosterItemType::BoosterItemUnbreakable:
                return "guiImage/ui_booster_item3.png";
                
            case BoosterItemType::BoosterItemMissile:
                return "guiImage/ui_booster_item1.png";
                
            case BoosterItemType::BoosterItemTimeStop:
                return "guiImage/ui_booster_item2.png";
                
            case BoosterItemType::BoosterItemDoubleCoin:
                return "guiImage/ui_booster_item5.png";
                
            case BoosterItemType::BoosterItemSnowball:
                return "guiImage/ui_booster_item4.png";
                
            default:
                return "";
        }

	}
}


BoosterControl::BoosterControl()
: mRootNode(nullptr)
, mCallback()
, mIsCooldown(false)
{
	
}

bool BoosterControl::init()
{
	if(!Layer::init()){
		return false;
	}
	
	mRootNode = CSLoader::createNode("gui/BoosterControl.csb");
	setupUI();
	
	setActiveState();
	
	return true;
}

void BoosterControl::setupUI()
{
	Node *mainPanel = mRootNode;	//->getChildByName("mainPanel");
	
	setContentSize(mRootNode->getContentSize());
	
//	Button *mBoosterButton;
//	Sprite *mCooldownSprite;
//	Text *mCountText;

	mBoosterButton = mainPanel->getChildByName<Button *>("boosterButton");
 //   mBoosterButton->setContentSize(Size(100,100));
    
	mCooldownSprite = mainPanel->getChildByName<Sprite *>("cooldownSprite");
    mCountPanel = mainPanel->getChildByName("counterPanel");
	mCountText = mCountPanel->getChildByName<Text *>("countText");
	
	mCooldownSprite->setVisible(false);
    
    CircularMaskNode *node = CircularMaskNode::create();
    Size size = mRootNode->getContentSize();
    node->setPosition(Vec2(size.width/2,size.height/2));
    Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");
    sprite->setScale(0.6);
    node->addChild(sprite);
    mRootNode->addChild(node,-1);
    
    mCooldownMask = node;
    mCooldownMask->setVisible(false);

	
	mBoosterButton->addClickEventListener([&](Ref *) {
		onBoosterClicked();
	});
	
//	mPlanetBtn = mainPanel->getChildByName<Button*>("planetBtn");
//	mShopBtn = mainPanel->getChildByName<Button*>("shopBtn");
//	mCancelBtn = mainPanel->getChildByName<Button*>("cancelBtn");
//	
//	mPlanetBtn->addClickEventListener([&](Ref*){
//		MainSceneLayer::setNextDefaultPlanet(51);
//		if(mShouldPopScene){
//			Director::getInstance()->popScene();
//		}else{
//			this->removeFromParent();
//		}
//	});
//	
//	mShopBtn->addClickEventListener([&](Ref*){
//		this->removeFromParent();
//		auto scene = CoinShopSceneLayer::createScene();
//		Director::getInstance()->pushScene(scene);
//	});
//	
//	mCancelBtn->addClickEventListener([&](Ref*){
//		this->removeFromParent();
//	});
	
	addChild(mRootNode);
}


void BoosterControl::update(float delta)
{
    switch (mState) {
		case Active: {
			break;
		}

		case Using: {
            handleUsing();
			break;
		}

		case Cooldown: {
            handleCooldown(delta);
			break;
		}

		default: {
			break;
		}
    }
//	if(mIsCooldown) {
//		handleCooldown(delta);
//	}
}

void BoosterControl::setBoosterType(BoosterItemType type)
{
	mType = type;
	
	if(type == BoosterItemNone) {
		setVisible(false);
		return;
	}

	
	setVisible(true);
	
	std::string image = getBoosterImage(type);
	
	mBoosterButton->loadTextureNormal(image);
    mBoosterButton->loadTextureDisabled(image);
    updateCounterUI();
    updateButtonState();
}

BoosterItemType BoosterControl::getBoosterType()
{
	return mType;
}

void BoosterControl::updateCounterUI()
{
    int count = ItemManager::instance()->getBoosterCount(mType);
    if(count<=0){
        mCountPanel->setVisible(false);
    }else{
        mCountPanel->setVisible(true);
        mCountText->setString(StringUtils::format("%d",count));
    }
}

void BoosterControl::updateButtonState()
{
    int count = ItemManager::instance()->getBoosterCount(mType);
    if(count<=0){
        changeOpacity(100);
 //       mBoosterButton->loadTextureDisabled("");
        mBoosterButton->setEnabled(false);
    }else{
        changeOpacity(255);
        std::string image = getBoosterImage(mType);
        mBoosterButton->loadTextureDisabled(image);
        mBoosterButton->setEnabled(true);
    }
}


void BoosterControl::onBoosterClicked()
{
	// TODO
	
	// Turn
	bool isOkay = doCallback();
	
	if(isOkay) {
		GameSound::playSound(GameSound::VoiceLaugh);
		
		// TODO:
		//	- deduce booster count (ui & ItemManager)
		//  - turn to cooldown mode
        ItemManager::instance()->deduceBoosterCount(mType);
        
        updateCounterUI();
//        updateButtonState();
        bool isInstantActive = ItemManager::instance()->getBoosterIsInstantActive(mType);
        if (isInstantActive) {
            setCooldownState();
        }else{
            setUsingState();
        }
		
	}
}

bool BoosterControl::doCallback()
{
	if(mCallback) {
		return mCallback(this);
	}
	return false;
}

void BoosterControl::setCallback(const BoosterControllCallback &callback)
{
	mCallback = callback;
}


#pragma mark - Active & Cooldown state
void BoosterControl::setActiveState()
{
	mIsCooldown = false;
    mState = Active;
//	mBoosterButton->setEnabled(true);
    mCooldownMask->setVisible(false);
    updateButtonState();
	changeOpacity(255);
}

void BoosterControl::setUsingState()
{
    mBoosterButton->setEnabled(false);
    changeOpacity(100);
    mState = Using;
}

// 
void BoosterControl::setCooldownState()
{
	mBoosterButton->setEnabled(false);
	changeOpacity(100);

    int count = ItemManager::instance()->getBoosterCount(mType);
    if(count<=0){
        mRootNode->setVisible(false);
        return;
    }
    
    mCooldownTime = ItemManager::instance()->getBoosterCooldown(mType);
    mCooldownRemain = mCooldownTime;
    
    mCooldownMask->setVisible(true);
    mCooldownMask->updateAngleByPercent(100);
	
	mIsCooldown = true;
    mState = Cooldown;
}

void BoosterControl::changeOpacity(int value)
{
	mBoosterButton->setOpacity(value);
	mCountText->setOpacity(value);
}

void BoosterControl::handleUsing()
{
    float remainTime = GameWorld::instance()->getPlayer()->getItemEffectRemainTime(mType);
    if(remainTime<=0){
        setCooldownState();
    }
}

void BoosterControl::handleCooldown(float delta)
{
	mCooldownRemain -= delta;
	log("cooldown: %f", mCooldownRemain);
    
    mCooldownMask->updateAngleByPercent(mCooldownRemain/mCooldownTime*100);

	if(mCooldownRemain <= 0) {
		setActiveState();
	}
}
