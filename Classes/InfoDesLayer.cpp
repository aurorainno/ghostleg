//
//  InfoDesLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 12/4/2017.
//
//

#include "InfoDesLayer.h"

bool InfoDesLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("InfoDesLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

void InfoDesLayer::setupUI(cocos2d::Node *rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    Button *cancelBtn = rootNode->getChildByName<Button*>("cancelBtn");
    cancelBtn->addClickEventListener([=](Ref*){
        this->removeFromParent();
    });
}
