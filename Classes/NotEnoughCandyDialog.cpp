//
//  NotEnoughCandyDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 14/12/2016.
//
//

#include "NotEnoughCandyDialog.h"
#include "GameWorld.h"
#include "CommonMacro.h"
#include "MainScene.h"
#include "CoinShopScene.h"

NotEnoughCandyDialog::NotEnoughCandyDialog()
:mRootNode(nullptr)
,mShouldPopScene(false)
{
    
}

bool NotEnoughCandyDialog::init()
{
    if(!Layer::init()){
        return false;
    }
    
    mRootNode = CSLoader::createNode("NotEnoughCandyDialog.csb");
    setupUI();
    
    return true;
}

void NotEnoughCandyDialog::setupUI()
{
    FIX_UI_LAYOUT(mRootNode);
    Node *mainPanel = mRootNode->getChildByName("mainPanel");
    
    mPlanetBtn = mainPanel->getChildByName<Button*>("planetBtn");
    mShopBtn = mainPanel->getChildByName<Button*>("shopBtn");
    mCancelBtn = mainPanel->getChildByName<Button*>("cancelBtn");
    
    mPlanetBtn->addClickEventListener([&](Ref*){
        MainSceneLayer::setNextDefaultPlanet(51);
        if(mShouldPopScene){
            Director::getInstance()->popScene();
        }else{
            this->removeFromParent();
        }
    });
    
    mShopBtn->addClickEventListener([&](Ref*){
        this->removeFromParent();
        auto scene = CoinShopSceneLayer::createScene();
        Director::getInstance()->pushScene(scene);
    });
    
    mCancelBtn->addClickEventListener([&](Ref*){
        this->removeFromParent();
    });
    
    addChild(mRootNode);
}

void NotEnoughCandyDialog::setPopScene(bool shouldPopScene){
    mShouldPopScene = shouldPopScene;
}


