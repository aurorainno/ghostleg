//
//  GalleryView.hpp
//  GhostLeg
//
//  Created by Calvin on 15/3/2017.
//
//

#ifndef GalleryView_hpp
#define GalleryView_hpp

#include <stdio.h>
#include <queue>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class GalleryView : public Layer
{
public:
    enum State{
        Idle,
        Rotate
    };
    
    GalleryView();
    
    CREATE_FUNC(GalleryView);
    
    void update(float delta);
    void onEnter();
    
    template <typename T>
    void setNodeList(std::vector<T> nodeList){
        for(T node : nodeList){
            //        static_assert(std::is_base_of<Node, T>::value, "T must derive from Node");
//            node = static_cast<Node*>(node);
            node->retain();
            mNodeList.push_back(node);
        }
    }
    
    void setRotationTime(float timeInSec);
    void setPeriod(float period);
    void setTargetPosition(Vec2 pos);
    
private:
    void prepareView();
    
    void changeToIdle();
    void changeToRotate();
    
    
private:
    State mState;
    std::vector<Node*> mNodeList;
    Node* mFirstNode;
    Node* mSecondNode;
    float mCounter;
    float mRotationTime;
    float mPeriod;
    float mRotationSpeed;
    Vec2 mTargetPos;
    int mIndex;
    
};

#endif /* GalleryView_hpp */
