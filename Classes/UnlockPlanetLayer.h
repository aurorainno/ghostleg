//
//  UnlockPlanetLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 2/11/2016.
//
//

#ifndef UnlockPlanetLayer_hpp
#define UnlockPlanetLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class DogData;
class MainSceneLayer;

class UnlockPlanetLayer : public Layer
{
public:
    CREATE_FUNC(UnlockPlanetLayer);
    
    virtual bool init() override;
    
    CC_SYNTHESIZE(ActionTimeline *, mPlanetAction, PlanetAction);
    CC_SYNTHESIZE(Node *, mParentLayer, ParentLayer);

    void setCloseCallback(const std::function<void(int)> &callback);

	void setupGUI(const std::string &csb);
	void setupAnimation(const std::string &csb);
	
	void runAnimation(const std::string &name);
    
    void setPlanetList(std::vector<int> planetList);
    
    void updatePlanetVisual();
    void setPlanetCsb(const std::string &csbName, bool isLocked);
    void setPlanetCsbAnimationForLockState(bool isLocked);
    void setPlanetCsbAnimation(const std::string &name);

	virtual void onEnter();
	
private:
	void attachFireworkEffect(Node *mainPanel);
	
private:
    std::vector<int> mPlanetList;
    int mPlanetID;
    Button* mOKBtn;
    Sprite *mNameSprite;
    Node *mPlanetNodeParent;		// the parent node hold the planet Node
    Node *mPlanetNode;
    Node *mMainPanel;
    cocostudio::timeline::ActionTimeline *mTimeline;
	
    std::function<void(int)> mCloseCallback;

};




#endif /* UnlockPlanetLayer_hpp */
