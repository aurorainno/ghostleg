//
//  InfoDesLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 12/4/2017.
//
//

#ifndef InfoDesLayer_hpp
#define InfoDesLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "PlayerCharProfile.h"

USING_NS_CC;
using namespace cocos2d::ui;

class InfoDesLayer : public Layer
{
public:
    CREATE_FUNC(InfoDesLayer);
    
    virtual bool init();
    
    void setupUI(Node* rootNode);
    
};

#endif /* InfoDesLayer_hpp */
