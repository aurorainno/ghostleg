//
//  DeliveryCompleteNoticeView.hpp
//  GhostLeg
//
//  Created by Calvin on 20/3/2017.
//
//

#ifndef DeliveryCompleteNoticeView_hpp
#define DeliveryCompleteNoticeView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NPCOrder;
class RichTextLabel;

class DeliveryCompleteNoticeView : public Layer
{
public:
    CREATE_FUNC(DeliveryCompleteNoticeView);
    virtual bool init();
    
    void setupUI(Node* rootNode);
    
    void setReward(NPCOrder* npcOrder);
    
    void setBonusTip(int value);
    
    void setOnShowRewardCallback(std::function<void()> callback);
    void setOnCloseCallback(std::function<void()> callback);
    
    void showView();
    
    void closeView();
    
    void showReward();
    
    
private:
    Node *mRootNode;
    Node *mRewardPanel;
    Node *mTipsPanel;
    Sprite* mNpcSprite;
    
    RichTextLabel* mMainRewardText;
    RichTextLabel* mBonusRewardText;
    Text* mMainRewardPopText;
    Text* mBonusRewardPopText;
    
    int mReward;
    int mTips;
    
    std::function<void()> mOnShowRewardCallback;
    std::function<void()> mOnCloseCallback;
};

#endif /* DeliveryCompleteNoticeView_hpp */
