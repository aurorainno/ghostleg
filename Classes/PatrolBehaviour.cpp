//
//  PetrolBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 21/2/2017.
//
//

#include "PatrolBehaviour.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "GameWorld.h"
#include "GameSound.h"
#include "GameScene.h"
#include "StringHelper.h"

//namespace {
//    Vec2 calcNewPosition(const Vec2& oldPos, const float speedX, const float speedY, float delta) {
//        Vec2 pos = oldPos;
//        
//        pos.x += delta * speedX;
//        pos.y += delta * speedY;
//        
//        return pos;
//    }
//}

PatrolBehaviour::PatrolBehaviour()
: EnemyBehaviour()
, mMode(MovingMode::Horizontal)
, mCurrentDir(1)
, mDisplacement(0)
, mMaxDisplacement(0)
{}

void PatrolBehaviour::onCreate()
{
    getEnemy()->changeToIdle();
    updateProperty();
}

void PatrolBehaviour::onVisible()
{
    if(!getEnemy()->getSoundPlayed()) {
//        GameSound::playEnemyAppearSound(getEnemy()->getResID());
    }
}


void PatrolBehaviour::onActive()
{
    //    float posX = getEnemy()->getPositionX();
    //
    //    log("onActive! posX: %.0f",posX);
    //    GameWorld::instance()->getGameUILayer()->showCaution(posX);
    updateEnemyFaceDir();
}

void PatrolBehaviour::updateEnemyFaceDir()
{
    if(mMode == MovingMode::Vertical){
        return;
    }
	
	Enemy *enemy = getEnemy();
	if(enemy->getEnemyData()->isTrap()) {
		return;	// no need to change
	}

    
    bool toLeft = mCurrentDir == -1 ? true : false;
	enemy->setFace(toLeft);
}

void PatrolBehaviour::onUpdate(float delta)
{
    // log("onUpdate!");
    
    Enemy *enemy = getEnemy();
    if(enemy == nullptr) {
        return;
    }
    
    if(enemy->getState() == Enemy::State::Active &&
       enemy->getEffectStatus()!=EffectStatusTimestop) {
        if(mDisplacement >= mMaxDisplacement){
            mDisplacement = 0;
			
			mCurrentDir = -mCurrentDir;
			updateEnemyFaceDir();
			
        }
        
        if(mMode == MovingMode::Horizontal){
            float speedX = fabsf(getSpeedX());
            float speedY = fabsf(getSpeedY());
            Vec2 newPos = enemy->getPosition() + Vec2(mCurrentDir * speedX * delta , -speedY * delta);
            enemy->setPosition(newPos);
            mDisplacement += speedX * delta;
        }else if(mMode == MovingMode::Vertical){
            float speedY = fabsf(getSpeedY());
            float speedX = getSpeedX();
            Vec2 newPos = enemy->getPosition() + Vec2(speedX * delta , mCurrentDir * speedY * delta);
            enemy->setPosition(newPos);
            mDisplacement += speedY * delta;
        }
        
 //       enemy->moveFree(delta);
        
        // enemy->move(delta);
        //		Vec2 speed = enemy->getFinalSpeed();
        //		Vec2 newPos = calcNewPosition(getPosition(), speed.x, speed.y, delta);
        //		
        //		enemy->setPosition(newPos);
        
        
    }
}

std::string PatrolBehaviour::toString()
{
    return "PetrolBehaviour";
}

void PatrolBehaviour::updateProperty()
{
    if(mData == nullptr) {
        log("PatrolBehaviour::updateProperty :data is null");
        return;
    }
    
    std::map<std::string, std::string> property = mData->getMapProperty("patrolSetting");
    
    if(property["mode"] == "Horizontal"){
        mMode = MovingMode::Horizontal;
        mCurrentDir = getEnemy()->isFaceLeft() ? -1 : 1;
    }else if(property["mode"] == "Vertical"){
        mMode = MovingMode::Vertical;
        int dir = STR_TO_INT(property["upFirst"]);
        mCurrentDir = dir > 0 ? 1 : -1;
    }
    
    mMaxDisplacement = STR_TO_INT(property["distance"]);
    mDisplacement = 0;
    //log("debug: item=%s", property["item"].c_str());
}

