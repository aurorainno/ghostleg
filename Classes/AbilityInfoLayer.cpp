//
//  AbilityInfoLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 10/4/2017.
//
//

#include "AbilityInfoLayer.h"
#include "PlayerAbility.h"
#include "RichTextLabel.h"
#include "StringHelper.h"
#include "FBProfilePic.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "AstrodogClient.h"

const Color4B kColorYellow = Color4B(0xff, 0xde, 0x01, 255);


const std::string kContentRichTextName = "contentRichText";

bool AbilityInfoLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("AbilityInfoLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
	
	LOG_SCREEN(Analytics::Screen::scn_character_ability);
    
    return true;
}

void AbilityInfoLayer::setupUI(cocos2d::Node *rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    mRootNode = rootNode->getChildByName("mainPanel");
    mNameText = mRootNode->getChildByName<Text*>("nameText");
    
    Button* backBtn = mRootNode->getChildByName<Button*>("backBtn");
    backBtn->addClickEventListener([=](Ref*){
        this->removeFromParent();
    });
    
    //setup rich text
    for(int level=1;level<=5;level++){
        Node* panel = mRootNode->getChildByName(StringUtils::format("level%dPanel",level));
       
        Node* unlockedView = panel->getChildByName("unlockedView");
        Node* lockedView = panel->getChildByName("lockedView");
        
        Text* contentText = unlockedView->getChildByName<Text*>("contentText");
        RichTextLabel* contentRichTextLabel = RichTextLabel::createFromText(contentText);
        
        contentRichTextLabel->setTextColor(1, kColorYellow);
        
        contentRichTextLabel->setName(kContentRichTextName);
        
        unlockedView->addChild(contentRichTextLabel);
        contentText->setVisible(false);
        
        Text* contentTextTwo = lockedView->getChildByName<Text*>("contentText");
        RichTextLabel* contentRichTextLabelTwo = RichTextLabel::createFromText(contentTextTwo);
        
        contentRichTextLabelTwo->setTextColor(1, kColorYellow);
        
        contentRichTextLabelTwo->setName(kContentRichTextName);
        
        lockedView->addChild(contentRichTextLabelTwo);
        contentTextTwo->setVisible(false);
    }
    
    mStarText = (Text*) rootNode->getChildByName("totalCoinText");
    mDiamondText = rootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    

    mFbName = (Text *) rootNode->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) rootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);

}

void AbilityInfoLayer::onEnter()
{
    Layer::onEnter();
    updateDiamond();
    updateStars();
    setupFbProfile();
}

void AbilityInfoLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void AbilityInfoLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void AbilityInfoLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        mFbName->setString(user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }
        
        mProfilePic->setWithFBId(user->getFbID());
    }else{
        //       mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}


void AbilityInfoLayer::setView(PlayerCharProfile *charProfile)
{
    setPlayerProfile(charProfile);
    mNameText->setString(charProfile->getName());
    int currentLevel = charProfile->getCurrentLevel();
    for(int level=1;level<=5;level++){
        Node* panel = mRootNode->getChildByName(StringUtils::format("level%dPanel",level));
        PlayerAbility* ability = charProfile->getAbilityByLevel(level);
        Node* mainView;
        if(level<=currentLevel){
            mainView = panel->getChildByName("unlockedView");
            panel->getChildByName("lockedView")->setVisible(false);
        }else{
            mainView = panel->getChildByName("lockedView");
            panel->getChildByName("unlockedView")->setVisible(false);
        }
        mainView->setVisible(true);
        
        RichTextLabel *content = mainView->getChildByName<RichTextLabel *>(kContentRichTextName);
        setRichTextContent(content,ability->getDesc());
    }
}

void AbilityInfoLayer::setRichTextContent(RichTextLabel *richText, std::string content)
{
    std::vector<std::string> strList = aurora::StringHelper::split(content, '\n');
    int maxLineSupported = 3; //change this value require change in cocosStudio
    int multiplyer = maxLineSupported - (int)strList.size();
    if(multiplyer < 0){
        multiplyer =  0;
    }
    
    float disY = -7 * multiplyer;
    richText->setPositionY(richText->getPositionY() + disY);
    richText->setString(content);
}
