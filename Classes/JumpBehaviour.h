//
//  JumpBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 3/3/2017.
//
//

#ifndef JumpBehaviour_hpp
#define JumpBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

class JumpBehaviour : public EnemyBehaviour
{
public:
    CREATE_FUNC(JumpBehaviour);
    
    JumpBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onCreate();
    virtual void onVisible();
    virtual void onActive();
    virtual void onUpdate(float delta);
    
    void onJump();
    void startJump();
    void updateProperty();
    
private:
    float mCooldown;
    float mAccumTime;
    float mDisplacementPerFrame;
    float mDistance;
    bool mStartCountDown;
    bool mIsJumping;
    bool mIsLeft;
    int mJumpFrame;
    
    float mDistanceTravelled;
    
};


#endif /* JumpBehaviour_hpp */
