//
//  BasePlayerAbility.cpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#include "PlayerAbility.h"
#include "StringHelper.h"
#include "GameplaySetting.h"

PlayerAbility::PlayerAbility(Type type)
: mParam1(0)
, mParam2(0)
, mParam3(0)
, mIsGlobalEffect(false)
{
	mType = type;
}

bool PlayerAbility::init()
{
	return true;
}


std::string PlayerAbility::toString()
{
	return "BasePlayerAbility";
}


void PlayerAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');
	
	for(int i=0; i<tokens.size(); i++) {
		std::string valueStr = tokens[i];
		
		float value = STR_TO_FLOAT(valueStr);
		// log("parse: %s value=%f", valueStr.c_str(), value);
		
		if(0 == i) {
			setParam1(value);
		} else if(1 == i) {
			setParam2(value);
		} else if(2 == i) {
			setParam3(value);
		}
	}
	
	// TODO!!
}

void PlayerAbility::updateSetting(GameplaySetting *setting, bool onlyGlobal)
{
	if(setting == nullptr) {
		log("PlayerAbility.updateSetting: setting is null");
		return;
	}
	
	//log("%s : onlyGlobal=%d isAbilityGlobal=%d", toString().c_str(), onlyGlobal, isGlobalEffect());
	
	if(onlyGlobal != isGlobalEffect()) {
		return;			// only update when setting is global setting and ability is global effect
	}
	
	updateGameplaySetting(setting);
}

void PlayerAbility::updateGameplaySetting(GameplaySetting *setting)
{
	
}

float PlayerAbility::getMainValue()
{
	return mParam1;
}


std::string PlayerAbility::getDesc()	// For UI
{
	return "TODO: Testing <c1>SomeValue</c1>!!!";
}
