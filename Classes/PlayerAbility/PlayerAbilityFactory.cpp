//
//  PlayerAbilityFactory.cpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#include "PlayerAbilityFactory.h"
#include "PlayerAbility.h"
#include "StringHelper.h"

#pragma mark - Different Ability 
// General
#include "NoneAbility.h"
#include "BonusTipAbility.h"
#include "RatingBonusAbility.h"
#include "ExtraRewardAbility.h"
#include "ExtraTimeAbility.h"
#include "BonusAttributeAbility.h"
#include "StunReductionAbility.h"
#include "DoubleStarChanceAbility.h"

// Reward
#include "KillRewardAbility.h"
#include "BonusScoreAbility.h"
#include "TravelRewardAbility.h"

// PowerUp & Booster Related
#include "BoosterAbility.h"
#include "PowerUpAbility.h"

// Defend
#include "AttackBlockerAbility.h"
#include "SlowChaserAbility.h"

// Starting Booster
#include "StartBoosterAbility.h"
#include "StartPowerUpAbility.h"
//


//
static PlayerAbilityFactory *sInstance = nullptr;

PlayerAbilityFactory *PlayerAbilityFactory::instance()
{
	if(!sInstance) {
		sInstance = new PlayerAbilityFactory();
	}
	return sInstance;
}

PlayerAbilityFactory::PlayerAbilityFactory()
{
}



PlayerAbility::Type PlayerAbilityFactory::parseType(const std::string &typeStr)
{
	int start = (int) PlayerAbility::PlayerAbilityNone;
	int end = (int) PlayerAbility::PlayerAbilityEndOfList;
	
	for(int value=start; value < end; value++) {
		PlayerAbility::Type type = (PlayerAbility::Type) value;
		std::string typeName = getTypeName(type);
		
		//log("debug: type=%d typeStr=%s myType=%s", type, typeStr.c_str(), typeName.c_str());
		if(typeName == typeStr) {
			return type;
		}
	}
	
	return PlayerAbility::PlayerAbilityNone;
}


std::string PlayerAbilityFactory::getTypeName(const PlayerAbility::Type &type)
{
	switch(type) {
		case PlayerAbility::PlayerAbilityNone				: return "none";
		
		// Attribute
		case PlayerAbility::PlayerAbilityBonusAttribute		: return "attribute";
			
		//
		case PlayerAbility::PlayerAbilityBonusTip			: return "bonusTip";
		case PlayerAbility::PlayerAbilityRatingBonus		: return "ratingBonus";		// No need
		case PlayerAbility::PlayerAbilityExtraTime			: return "extraTime";		// No need
		case PlayerAbility::PlayerAbilityExtraReward		: return "extraReward";
		case PlayerAbility::PlayerAbilityStunReduction		: return "stunReduction";
		case PlayerAbility::PlayerAbilityDoubleStarChance	: return "doubleStarChance";
			
		// Reward
		case PlayerAbility::PlayerAbilityKillReward			: return "killReward";
		case PlayerAbility::PlayerAbilityBonusScore			: return "bonusScore";
		case PlayerAbility::PlayerAbilityTravelReward		: return "travelReward";
			
		// Attack blocing & Defend
		case PlayerAbility::PlayerAbilityAttackBlocker		: return "attackBlocker";
		case PlayerAbility::PlayerAbilitySlowChaser			: return "slowChaser";
			
		// PowerUp & Booster Related
		case PlayerAbility::PlayerAbilityBooster			: return "booster";
		case PlayerAbility::PlayerAbilityPowerUp			: return "powerUp";
			
		// Starting Booster
		case PlayerAbility::PlayerAbilityStartBooster		: return "startBooster";
		case PlayerAbility::PlayerAbilityStartPowerUp		: return "startPowerUp";
			
		default												: return "error";
	}
}


PlayerAbility *PlayerAbilityFactory::create(const PlayerAbility::Type &type)
{
	switch(type) {
		case PlayerAbility::PlayerAbilityNone:
			return NoneAbility::create();
		
		//
		case PlayerAbility::PlayerAbilityBonusAttribute:
			return BonusAttributeAbility::create();
			
		//
		case PlayerAbility::PlayerAbilityBonusTip:
			return BonusTipAbility::create();
			
		case PlayerAbility::PlayerAbilityRatingBonus:
			return RatingBonusAbility::create();
			
		case PlayerAbility::PlayerAbilityExtraTime:
			return ExtraTimeAbility::create();
			
		case PlayerAbility::PlayerAbilityExtraReward:
			return ExtraRewardAbility::create();
			
			
		case PlayerAbility::PlayerAbilityStunReduction:
			return StunReductionAbility::create();
			
		// Reward
		case PlayerAbility::PlayerAbilityKillReward:
			return KillRewardAbility::create();
		
		case PlayerAbility::PlayerAbilityDoubleStarChance:
			return DoubleStarChanceAbility::create();
			
		case PlayerAbility::PlayerAbilityBonusScore:
			return BonusScoreAbility::create();
		
		case PlayerAbility::PlayerAbilityTravelReward:
			return TravelRewardAbility::create();
			
		// Defend
		case PlayerAbility::PlayerAbilityAttackBlocker:
			return AttackBlockerAbility::create();
			
		case PlayerAbility::PlayerAbilitySlowChaser:
			return SlowChaserAbility::create();
			
		// PowerUp & Booster Related
		case PlayerAbility::PlayerAbilityBooster:
			return BoosterAbility::create();
			
		case PlayerAbility::PlayerAbilityPowerUp:
			return PowerUpAbility::create();
			
		// Starting Booster
		case PlayerAbility::PlayerAbilityStartBooster:
			return StartBoosterAbility::create();
			
		case PlayerAbility::PlayerAbilityStartPowerUp:
			return StartPowerUpAbility::create();
			
		default:
			return nullptr;		// Undefined type
	}
}

PlayerAbility *PlayerAbilityFactory::create(const std::string &abilityStr)
{
	std::vector<std::string> tokens = aurora::StringHelper::split(abilityStr, '/');
	std::string abilityName = tokens.size() <= 0 ? "" : tokens[0];
	std::string paramStr = tokens.size() <= 1 ? "" : tokens[1];
	std::string isGlobalStr = tokens.size() <= 2 ? "" : tokens[2];
	
	
	
	PlayerAbility::Type type = parseType(abilityName);
	
	PlayerAbility *ability = create(type);
	ability->parse(paramStr);
	
	bool isGlobal = isGlobalStr == "global";
	ability->setGlobalEffect(isGlobal);
	
	//log("create: %s,%s", abilityName.c_str(), paramStr.c_str());
	
	return ability;
}

