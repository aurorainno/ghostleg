//
//  PowerUpAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef PowerUpAbility_hpp
#define PowerUpAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class PowerUpAbility : public PlayerAbility
{
public:
	enum Type {
		Unknown,
		LongerSpeedUp,
		FasterSpeedUp,
		LongerCashOut,
		LongerMagnet,
	};
public:
	CREATE_FUNC(PowerUpAbility);

public:
	PowerUpAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	std::string getDesc();
	
private:
	Type mType;
	std::string mTypeStr;
};

#endif /* NoneAbility_hpp */
