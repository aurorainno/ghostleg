//
//  BonusTipAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "BonusTipAbility.h"
#include "GameplaySetting.h"

BonusTipAbility::BonusTipAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityBonusTip)
{

}

std::string BonusTipAbility::toString()
{
	return StringUtils::format("BonusTipAbility: bonusPercent=%f", getBonusPercent());
}

void BonusTipAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void BonusTipAbility::updateGameplaySetting(GameplaySetting *setting)
{
	setting->setAttributeValue(GameplaySetting::BonusTipPercent, getBonusPercent());
}

float BonusTipAbility::getBonusPercent()
{
	return getParam1() / 100;
}

std::string BonusTipAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	
	float percent = getParam1();		// e.g 50%
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";

	return prefix + StringUtils::format("Extra <c1>%.0f%%</c1> reward from NPC", percent);
}
