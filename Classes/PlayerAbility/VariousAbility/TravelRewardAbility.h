//
//  TravelRewardAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef TravelRewardAbility_hpp
#define TravelRewardAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class TravelRewardAbility : public PlayerAbility
{
public:
	CREATE_FUNC(TravelRewardAbility);

public:
	TravelRewardAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
