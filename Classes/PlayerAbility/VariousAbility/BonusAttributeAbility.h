//
//  BonusAttributeAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef BonusAttributeAbility_hpp
#define BonusAttributeAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class BonusAttributeAbility : public PlayerAbility
{
public:
	enum Type {
		Unknown,
		MaxSpeed,
		StunReduction,
		Acceleration,
		TurnSpeed,
	};
	
public:
	CREATE_FUNC(BonusAttributeAbility);

public:
	BonusAttributeAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();

private:
	Type mType;
	std::string mTypeStr;
};

#endif /* NoneAbility_hpp */
