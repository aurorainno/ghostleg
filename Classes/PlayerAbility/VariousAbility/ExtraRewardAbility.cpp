//
//  ExtraRewardAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "ExtraRewardAbility.h"
#include "GameplaySetting.h"

ExtraRewardAbility::ExtraRewardAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityExtraReward)
{

}

std::string ExtraRewardAbility::toString()
{
	return StringUtils::format("ExtraRewardAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void ExtraRewardAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void ExtraRewardAbility::updateGameplaySetting(GameplaySetting *setting)
{
	log("DEBUG: ExtraRewardAbility: setAttribute: mParam1=%f", mParam1);
	setting->setAttributeValue(GameplaySetting::BonusReward, mParam1);
}

std::string ExtraRewardAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	int addCoin = (int) getParam1();
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	
	return prefix + StringUtils::format("Extra <c1>%d</c1> coins reward from Customers", addCoin);
}
