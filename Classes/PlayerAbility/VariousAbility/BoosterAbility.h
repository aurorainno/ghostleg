//
//  BoosterAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef BoosterAbility_hpp
#define BoosterAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class BoosterAbility : public PlayerAbility
{
public:
	enum Type {
		Unknown,
		SlowerMissile,
		SlowerCyclone,
		LongerTimeStop,
		LongerShield,
		LongerDoubleCoin,
	};

public:
	CREATE_FUNC(BoosterAbility);

public:
	BoosterAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
	
private:
	Type mType;
	std::string mTypeStr;
};

#endif /* NoneAbility_hpp */
