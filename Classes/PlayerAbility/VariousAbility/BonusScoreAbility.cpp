//
//  BonusScoreAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "BonusScoreAbility.h"
#include "GameplaySetting.h"

BonusScoreAbility::BonusScoreAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityBonusScore)
{

}

std::string BonusScoreAbility::toString()
{
	return StringUtils::format("BonusScoreAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void BonusScoreAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void BonusScoreAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	setting->setBetterAttributeValue(GameplaySetting::BonusScore, getParam1());
}

std::string BonusScoreAbility::getDesc()
{
	int percent = (int) getParam1();
//	std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";

	return prefix + StringUtils::format("Increase score by <c1>%d%%</c1>", percent);
}
