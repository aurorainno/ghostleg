//
//  BoosterAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "BoosterAbility.h"
#include "GameplaySetting.h"
#include "StringHelper.h"

namespace {
	std::string getStringFormat(BoosterAbility::Type type)
	{
		switch (type) {
			case BoosterAbility::SlowerMissile	: return "<c1>%.0f%%</c1> slower for Missile speed";
			case BoosterAbility::SlowerCyclone	: return "<c1>%.0f%%</c1> slower for Cyclone speed";
			case BoosterAbility::LongerTimeStop	: return "Increase duration of Time Stopper \nby <c1>%.1f</c1> sec";
			case BoosterAbility::LongerShield	: return "Increase duration of Shield \nby <c1>%.1f</c1> sec";
			case BoosterAbility::LongerDoubleCoin : return "Increase duration of Double Coin \nby <c1>%.1f</c1> sec";
			default				: return "%.1f";
		}
	}
}

BoosterAbility::BoosterAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityBooster)
{

}

std::string BoosterAbility::toString()
{
	return StringUtils::format("BoosterAbility: type=%s p1=%f p2=%f p3=%f",
							   mTypeStr.c_str(),
							   mParam1, mParam2, mParam3);
}

void BoosterAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');
	
	mTypeStr = tokens[0];
	std::string valueStr = tokens.size() <= 1 ? "0" : tokens[1];
	
	//
	if("slowerMissile" == mTypeStr) {
		mType = Type::SlowerMissile;
	} else if("slowerCyclone" == mTypeStr) {
		mType = Type::SlowerCyclone;
	} else if("longerTimeStop" == mTypeStr) {
		mType = Type::LongerTimeStop;
	} else if("longerShield" == mTypeStr) {
		mType = Type::LongerShield;
	} else if("longerDoubleCoin" == mTypeStr) {
		mType = Type::LongerDoubleCoin;
	} else {
		mType = Type::Unknown;
	}
	
	
	//
	setParam1(STR_TO_FLOAT(valueStr));
}

void BoosterAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	

	GameplaySetting::Attribute attribute;
	
	switch (mType) {
		case SlowerMissile	: attribute = GameplaySetting::MissleSpeedReduction; break;
		case SlowerCyclone	: attribute = GameplaySetting::CycloneSpeedReduction; break;
		case LongerTimeStop	: attribute = GameplaySetting::ExtraTimeStopDuration; break;
		case LongerShield	: attribute = GameplaySetting::ExtraShieldDuration; break;
		case LongerDoubleCoin	: attribute = GameplaySetting::ExtraDoubleCoinDuration; break;
		default: {
			return;
		}
	}
	
	
	setting->setBetterAttributeValue(attribute, getParam1());
}


std::string BoosterAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	std::string formatStr = getStringFormat(mType);
	float value = getParam1();
	
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
    
	return prefix + StringUtils::format(formatStr.c_str(), value);
}
