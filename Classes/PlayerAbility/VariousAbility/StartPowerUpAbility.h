//
//  StartPowerUpAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef StartPowerUpAbility_hpp
#define StartPowerUpAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class StartPowerUpAbility : public PlayerAbility
{
public:
	CREATE_FUNC(StartPowerUpAbility);

public:
	StartPowerUpAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
