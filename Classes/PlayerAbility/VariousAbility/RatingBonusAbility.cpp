//
//  RatingBonusAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "RatingBonusAbility.h"
#include "GameplaySetting.h"

namespace {
	std::string getRatingName(int rating)
	{
		return StringUtils::format("%d star rating", rating);
	}
}

RatingBonusAbility::RatingBonusAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityRatingBonus)
{

}

std::string RatingBonusAbility::toString()
{
	int rating = (int) getParam1();
	int extraCoin = (int) getParam2();

	return StringUtils::format("RatingBonusAbility: rating=%d extraCoin=%d", rating, extraCoin);
}

void RatingBonusAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void RatingBonusAbility::updateGameplaySetting(GameplaySetting *setting)
{
	int rating = (int) getParam1();
	int extraCoin = (int) getParam2();
	
	setting->setExtraReward(rating, extraCoin);
}

std::string RatingBonusAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	
	std::string ratingStr = getRatingName(getParam1());
	int extraCoin = getParam2();
	
	return StringUtils::format("Get <c1>%d</c1> more coins\nfor %s", extraCoin, ratingStr.c_str());
}
