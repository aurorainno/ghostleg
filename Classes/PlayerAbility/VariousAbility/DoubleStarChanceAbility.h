//
//  DoubleStarChanceAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef DoubleStarChanceAbility_hpp
#define DoubleStarChanceAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class DoubleStarChanceAbility : public PlayerAbility
{
public:
	CREATE_FUNC(DoubleStarChanceAbility);

public:
	DoubleStarChanceAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
