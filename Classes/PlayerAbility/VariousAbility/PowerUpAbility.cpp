//
//  PowerUpAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "PowerUpAbility.h"
#include "GameplaySetting.h"
#include "StringHelper.h"

namespace {
	std::string getStringFormat(PowerUpAbility::Type type)
	{
		switch (type) {
			case PowerUpAbility::LongerSpeedUp	: return "Increase duration of Speed Up\nby <c1>%.1f</c1> sec";
			case PowerUpAbility::FasterSpeedUp	: return "Increase Speed Up by <c1>%.0f%%</c1>";
			case PowerUpAbility::LongerCashOut	: return "Increase duration of Zap\nby <c1>%.1f</c1> sec";
			case PowerUpAbility::LongerMagnet	: return "Increase duration of Magnet\nby <c1>%.1f</c1> sec";
			default				: return "%.1f";
		}
	}
}

PowerUpAbility::PowerUpAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityPowerUp)
{
	
}

std::string PowerUpAbility::toString()
{
	return StringUtils::format("PowerUpAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void PowerUpAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');

	mTypeStr = tokens[0];
	std::string valueStr = tokens.size() <= 1 ? "0" : tokens[1];
	
	//
	if("longerSpeedUp" == mTypeStr) {
		mType = Type::LongerSpeedUp;
	} else if("longerMagnet" == mTypeStr) {
		mType = Type::LongerMagnet;
	} else if("longerCashOut" == mTypeStr) {
		mType = Type::LongerCashOut;
	} else if("fasterSpeedUp" == mTypeStr) {
		mType = Type::FasterSpeedUp;
	} else {
		mType = Type::Unknown;
	}
	
	
	//
	setParam1(STR_TO_FLOAT(valueStr));
	
	//PlayerAbility::parse(paramStr);
}

void PowerUpAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	GameplaySetting::Attribute attribute;
	
	switch (mType) {
		case LongerSpeedUp	: attribute = GameplaySetting::ExtraSpeedUpDuration; break;
		case FasterSpeedUp	: attribute = GameplaySetting::ExtraSpeedUpEffect; break;
		case LongerCashOut	: attribute = GameplaySetting::ExtraCashOutDuration; break;
		case LongerMagnet	: attribute = GameplaySetting::ExtraMagnetDuration; break;
		default: {
			return;
		}
	}
	
	
	setting->setBetterAttributeValue(attribute, getParam1());
}

std::string PowerUpAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	std::string formatStr = getStringFormat(mType);
	float duration = getParam1();
	
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	return prefix + StringUtils::format(formatStr.c_str(), duration);
}
