//
//  StartBoosterAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "StartBoosterAbility.h"
#include "GameplaySetting.h"
#include "CommonType.h"
#include "StringHelper.h"

namespace  {
	std::string getBoosterName(int value) {
		if(BoosterItemUnbreakable == value) {
			return "Shield";
		}
		
		return StringUtils::format("booster-%d", value);
	}
}


StartBoosterAbility::StartBoosterAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityStartBooster)
{

}

std::string StartBoosterAbility::toString()
{
	return StringUtils::format("StartBoosterAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void StartBoosterAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	// startBooster/shield
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');
	
	std::string boosterName = tokens[0];
	
	BoosterItemType boosterType;
	//
	if("shield" == boosterName) {
		boosterType = BoosterItemUnbreakable;
	} else {
		boosterType = BoosterItemNone;
	}
	
	
	//
	setParam1(boosterType);

}

void StartBoosterAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	setting->setBetterAttributeValue(GameplaySetting::StartBooster, getParam1());
}

std::string StartBoosterAbility::getDesc()
{
	int boosterType = (int) getParam1();
	std::string boosterName = getBoosterName(boosterType);
	
//	std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	return prefix + StringUtils::format("Start with a %s", boosterName.c_str());

}
