//
//  ShieldAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "ShieldAbility.h"
#include "GameplaySetting.h"

ShieldAbility::ShieldAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityNone)
{

}

std::string ShieldAbility::toString()
{
	return StringUtils::format("ShieldAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void ShieldAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void ShieldAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting) {
		setting->setBetterAttributeValue(GameplaySetting::ExtraShieldDuration, getParam1());
	}
}

std::string ShieldAbility::getDesc()
{
	int percent = (int) (getParam1() * 100);
	// std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???

	float duration = getParam1();
	
	return StringUtils::format("+<c1>%.1f</c1>sec Shield duration", duration);
}
