//
//  KillRewardAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef KillRewardAbility_hpp
#define KillRewardAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class KillRewardAbility : public PlayerAbility
{
public:
	CREATE_FUNC(KillRewardAbility);

public:
	KillRewardAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
