//
//  StartBoosterAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef StartBoosterAbility_hpp
#define StartBoosterAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class StartBoosterAbility : public PlayerAbility
{
public:
	CREATE_FUNC(StartBoosterAbility);

public:
	StartBoosterAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
	

};

#endif /* NoneAbility_hpp */
