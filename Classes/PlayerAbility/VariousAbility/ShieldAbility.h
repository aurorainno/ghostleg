//
//  ShieldAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef ShieldAbility_hpp
#define ShieldAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class ShieldAbility : public PlayerAbility
{
public:
	CREATE_FUNC(ShieldAbility);

public:
	ShieldAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
