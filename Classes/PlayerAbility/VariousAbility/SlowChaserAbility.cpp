//
//  SlowChaserAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "SlowChaserAbility.h"
#include "GameplaySetting.h"

SlowChaserAbility::SlowChaserAbility()
: PlayerAbility(PlayerAbility::PlayerAbilitySlowChaser)
{

}

std::string SlowChaserAbility::toString()
{
	return StringUtils::format("SlowChaserAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void SlowChaserAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void SlowChaserAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	setting->setBetterAttributeValue(GameplaySetting::Attribute::ChaserSlowRatio, getParam1());
}

std::string SlowChaserAbility::getDesc()
{
	int percent = (int) getParam1();
//	std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	return prefix + StringUtils::format("Slow the Giant Bogey by <c1>%d%%</c1>", percent);
}
