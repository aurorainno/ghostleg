//
//  AttackBlockerAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "AttackBlockerAbility.h"
#include "GameplaySetting.h"
#include "StringHelper.h"

namespace {
//	ChanceEnemyBlocker,
//	BulletBlocker,
//	BombBlocker,
//	LaserBlocker,

	std::string getStringFormat(AttackBlockerAbility::Type type)
	{
		switch (type) {
			case AttackBlockerAbility::ChanceEnemyBlocker:
				return "<c1>%.0f%%</c1> of blocking an Alien hit";
				
			case AttackBlockerAbility::BulletBlocker:
				return "Block <c1>%.0f</c1> times of bullet attack";
				
			case AttackBlockerAbility::BombBlocker:
				return "Block <c1>%.0f</c1> bomb attack";
				
			case AttackBlockerAbility::LaserBlocker:
				return "Block <c1>%.0f</c1> laser attack";
				
			default				: return "";
		}
	}
}


AttackBlockerAbility::AttackBlockerAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityAttackBlocker)
{

}

std::string AttackBlockerAbility::toString()
{
	return StringUtils::format("AttackBlockerAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void AttackBlockerAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');
	
	mTypeStr = tokens[0];
	std::string valueStr = tokens.size() <= 1 ? "0" : tokens[1];
	
	//
	if("blockEnemyChance" == mTypeStr) {
		mType = Type::ChanceEnemyBlocker;
	} else if("blockBullet" == mTypeStr) {
		mType = Type::BulletBlocker;
	} else if("blockBomb" == mTypeStr) {
		mType = Type::BombBlocker;
	} else if("blockLaser" == mTypeStr) {
		mType = Type::LaserBlocker;
	} else {
		mType = Type::Unknown;
	}
	
	
	//
	setParam1(STR_TO_FLOAT(valueStr));
	
	//PlayerAbility::parse(paramStr);
}

void AttackBlockerAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	GameplaySetting::Attribute attribute;
	
	switch (mType) {
		case ChanceEnemyBlocker	: attribute = GameplaySetting::ChanceEnemyBlocker; break;
		case BulletBlocker	: attribute = GameplaySetting::BulletBlocker; break;
		case BombBlocker	: attribute = GameplaySetting::BombBlocker; break;
		case LaserBlocker	: attribute = GameplaySetting::LaserBlocker; break;
		default: {
			return;
		}
	}
	
	
	setting->setBetterAttributeValue(attribute, getParam1());

}

std::string AttackBlockerAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	std::string formatStr = getStringFormat(mType);
	float value = getParam1();
	
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	
	return prefix + StringUtils::format(formatStr.c_str(), value);
}
