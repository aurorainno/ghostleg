//
//  NoneAbility.hpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#ifndef NoneAbility_hpp
#define NoneAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class NoneAbility : public PlayerAbility
{
public:
	CREATE_FUNC(NoneAbility);
	
public:
	NoneAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
};

#endif /* NoneAbility_hpp */
