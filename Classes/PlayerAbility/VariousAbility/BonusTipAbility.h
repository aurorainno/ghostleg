//
//  BonusTipAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef BonusTipAbility_hpp
#define BonusTipAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class BonusTipAbility : public PlayerAbility
{
public:
	CREATE_FUNC(BonusTipAbility);

public:
	BonusTipAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	
	float getBonusPercent();
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
