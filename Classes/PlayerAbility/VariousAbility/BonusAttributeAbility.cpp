//
//  BonusAttributeAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "BonusAttributeAbility.h"
#include "GameplaySetting.h"
#include "StringHelper.h"


namespace {
	//	ChanceEnemyBlocker,
	//	BulletBlocker,
	//	BombBlocker,
	//	LaserBlocker,
	
	std::string getStringFormat(BonusAttributeAbility::Type type)
	{
		switch (type) {
			case BonusAttributeAbility::MaxSpeed:
				return "Increase Max Speed by <c1>%.0f%%</c1>";
				
			case BonusAttributeAbility::StunReduction:
				return "Stun duration reduced by <c1>%.0f%%</c1>";
				
			case BonusAttributeAbility::Acceleration:
				return "Increase Acceleration by <c1>%.0f%%</c1>";
				
			case BonusAttributeAbility::TurnSpeed:
				return "Increase Slide speed by <c1>%.0f%%</c1>";
				
			default: return "";
		}
	}
}


BonusAttributeAbility::BonusAttributeAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityBonusAttribute)
{

}

std::string BonusAttributeAbility::toString()
{
	return StringUtils::format("BonusAttributeAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void BonusAttributeAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');
	
	mTypeStr = tokens[0];
	std::string valueStr = tokens.size() <= 1 ? "0" : tokens[1];
	
	//
	if("maxSpeed" == mTypeStr) {
		mType = Type::MaxSpeed;
	} else if("stunReduction" == mTypeStr) {
		mType = Type::StunReduction;
	} else if("acceleration" == mTypeStr) {
		mType = Type::Acceleration;
	} else if("turnSpeed" == mTypeStr) {
		mType = Type::TurnSpeed;
	} else {
		mType = Type::Unknown;
	}
	
	
	//
	setParam1(STR_TO_FLOAT(valueStr));
}

void BonusAttributeAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	GameplaySetting::Attribute attribute;
	
	switch (mType) {
		case MaxSpeed		: attribute = GameplaySetting::BonusMaxSpeed; break;
		case StunReduction	: attribute = GameplaySetting::BonusStunReduction; break;
		case Acceleration	: attribute = GameplaySetting::BonusAcceleration; break;
		case TurnSpeed		: attribute = GameplaySetting::BonusTurnSpeed; break;
		default: {
			return;
		}
	}
	
	
	setting->setBetterAttributeValue(attribute, getParam1());

}

std::string BonusAttributeAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	float value =  getParam1();
//	std::string isAll = isGlobalEffect() ? " All" : "";
	std::string formatStr = getStringFormat(mType);
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	
	return prefix + StringUtils::format(formatStr.c_str(), value);
}
