//
//  RatingBonusAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef RatingBonusAbility_hpp
#define RatingBonusAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class RatingBonusAbility : public PlayerAbility
{
public:
	CREATE_FUNC(RatingBonusAbility);

public:
	RatingBonusAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
