//
//  TravelRewardAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "TravelRewardAbility.h"
#include "GameplaySetting.h"

TravelRewardAbility::TravelRewardAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityTravelReward)
{

}

std::string TravelRewardAbility::toString()
{
	return StringUtils::format("TravelRewardAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void TravelRewardAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void TravelRewardAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	setting->setBetterAttributeValue(GameplaySetting::TravelReward, getParam1());
}

std::string TravelRewardAbility::getDesc()
{
	int coin = (int) getParam1();
//	std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	return prefix + StringUtils::format("Reward <c1>%d</c1> coins after travel\n<c1>%d</c1> metres", coin, 50);	// 50: @see player.cpp
}
