//
//  AttackBlockerAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef AttackBlockerAbility_hpp
#define AttackBlockerAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class AttackBlockerAbility : public PlayerAbility
{
public:
	enum Type {
		Unknown,
		ChanceEnemyBlocker,
		BulletBlocker,
		BombBlocker,
		LaserBlocker,
	};

public:
	CREATE_FUNC(AttackBlockerAbility);

public:
	AttackBlockerAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();

private:
	Type mType;
	std::string mTypeStr;
};

#endif /* NoneAbility_hpp */
