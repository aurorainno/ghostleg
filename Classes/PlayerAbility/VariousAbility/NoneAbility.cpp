//
//  NoneAbility.cpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#include "NoneAbility.h"
#include "GameplaySetting.h"

NoneAbility::NoneAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityNone)
{
	
}

std::string NoneAbility::toString()
{
	return "NoneAbility";
}

void NoneAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void NoneAbility::updateGameplaySetting(GameplaySetting *setting)
{
	
}
