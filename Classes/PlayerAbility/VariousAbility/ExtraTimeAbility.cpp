//
//  ExtraTimeAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "ExtraTimeAbility.h"
#include "GameplaySetting.h"

ExtraTimeAbility::ExtraTimeAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityExtraTime)
{

}

std::string ExtraTimeAbility::toString()
{
	return StringUtils::format("ExtraTimeAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void ExtraTimeAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void ExtraTimeAbility::updateGameplaySetting(GameplaySetting *setting)
{
	setting->setAttributeValue(GameplaySetting::ExtraTimeLimit, mParam1);
}

std::string ExtraTimeAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	int addSec = (int) getParam1();
	std::string sStr = addSec <= 1 ? "" : "s";
	
	return StringUtils::format("Time <c1>+%d</c1> second%s", addSec, sStr.c_str());
}
