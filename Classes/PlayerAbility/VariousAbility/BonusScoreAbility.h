//
//  BonusScoreAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef BonusScoreAbility_hpp
#define BonusScoreAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class BonusScoreAbility : public PlayerAbility
{
public:
	CREATE_FUNC(BonusScoreAbility);

public:
	BonusScoreAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
