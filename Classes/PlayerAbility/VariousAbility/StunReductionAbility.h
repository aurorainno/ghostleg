//
//  StunReductionAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef StunReductionAbility_hpp
#define StunReductionAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class StunReductionAbility : public PlayerAbility
{
public:
	CREATE_FUNC(StunReductionAbility);

public:
	StunReductionAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);	
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
