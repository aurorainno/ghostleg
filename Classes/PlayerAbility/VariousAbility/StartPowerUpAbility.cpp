//
//  StartPowerUpAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "StartPowerUpAbility.h"
#include "GameplaySetting.h"
#include "ItemEffect.h"
#include "StringHelper.h"

namespace  {
	std::string getPowerUpName(int value) {
		if(ItemEffect::Type::ItemEffectSpeedUp == value) {
			return "SpeedUp";
		}
		
		return StringUtils::format("powerup-%d", value);
	}
}


StartPowerUpAbility::StartPowerUpAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityStartPowerUp)
{

}

std::string StartPowerUpAbility::toString()
{
	return StringUtils::format("StartPowerUpAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void StartPowerUpAbility::parse(const std::string &paramStr)
{
	if(paramStr == "") {
		return;
	}
	
	// startBooster/shield
	std::vector<std::string> tokens = aurora::StringHelper::split(paramStr, ',');
	
	std::string effectName = tokens[0];
	
	ItemEffect::Type effectType;
	//
	if("speedUp" == effectName) {
		effectType = ItemEffect::Type::ItemEffectSpeedUp;
	} else {
		effectType = ItemEffect::Type::ItemEffectNone;
	}
	
	//
	setParam1(effectType);

}

void StartPowerUpAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	setting->setBetterAttributeValue(GameplaySetting::StartPowerUp, getParam1());
}

std::string StartPowerUpAbility::getDesc()
{
	int type = (int) getParam1();
	std::string name = getPowerUpName(type);
	
//	std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???
    
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	
	return prefix + "Speed up when game starts";
}
