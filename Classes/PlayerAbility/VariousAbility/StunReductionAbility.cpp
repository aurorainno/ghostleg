//
//  StunReductionAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "StunReductionAbility.h"
#include "GameplaySetting.h"

StunReductionAbility::StunReductionAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityStunReduction)
{

}

std::string StunReductionAbility::toString()
{
	return StringUtils::format("StunReductionAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void StunReductionAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void StunReductionAbility::updateGameplaySetting(GameplaySetting *setting)
{
	setting->setAttributeValue(GameplaySetting::BonusStunReduction, getMainValue());
}

std::string StunReductionAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	int sec = (int) getParam1();
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
    
	return prefix + StringUtils::format("Reduce the stun effect by <c1>%d</c1> second", sec);
}
