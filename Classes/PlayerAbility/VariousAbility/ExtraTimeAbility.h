//
//  ExtraTimeAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef ExtraTimeAbility_hpp
#define ExtraTimeAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class ExtraTimeAbility : public PlayerAbility
{
public:
	CREATE_FUNC(ExtraTimeAbility);

public:
	ExtraTimeAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
