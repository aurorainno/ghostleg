//
//  KillRewardAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//

#include "KillRewardAbility.h"
#include "GameplaySetting.h"

KillRewardAbility::KillRewardAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityKillReward)
{

}

std::string KillRewardAbility::toString()
{
	return StringUtils::format("KillRewardAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void KillRewardAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void KillRewardAbility::updateGameplaySetting(GameplaySetting *setting)
{
	if(setting == nullptr) {
		return;
	}
	
	setting->setBetterAttributeValue(GameplaySetting::KillEnemyReward, getParam1());
}

std::string KillRewardAbility::getDesc()
{
	int coin = (int) getParam1();
//	std::string isAll = isGlobalEffect() ? " (Global)" : "";		// ???
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
	return prefix + StringUtils::format("Add <c1>%d</c1> coins when an alien is killed", coin);
}
