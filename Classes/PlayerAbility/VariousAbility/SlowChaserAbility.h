//
//  SlowChaserAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef SlowChaserAbility_hpp
#define SlowChaserAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class SlowChaserAbility : public PlayerAbility
{
public:
	CREATE_FUNC(SlowChaserAbility);

public:
	SlowChaserAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
