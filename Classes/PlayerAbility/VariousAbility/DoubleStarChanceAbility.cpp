//
//  DoubleStarChanceAbility.cpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
///
//
#include "DoubleStarChanceAbility.h"
#include "GameplaySetting.h"

DoubleStarChanceAbility::DoubleStarChanceAbility()
: PlayerAbility(PlayerAbility::PlayerAbilityDoubleStarChance)
{

}

std::string DoubleStarChanceAbility::toString()
{
	return StringUtils::format("DoubleStarChanceAbility: p1=%f p2=%f p3=%f", mParam1, mParam2, mParam3);
}

void DoubleStarChanceAbility::parse(const std::string &paramStr)
{
	PlayerAbility::parse(paramStr);
}

void DoubleStarChanceAbility::updateGameplaySetting(GameplaySetting *setting)
{
	setting->setAttributeValue(GameplaySetting::DoubleStarPercent, mParam1);
}

std::string DoubleStarChanceAbility::getDesc()
{
	//return "TODO: Testing <c1>SomeValue</c1>!!!";
	int percent = (int) getParam1();
    std::string prefix = isGlobalEffect() ? "For All Characters:\n" : "";
    
	return prefix + StringUtils::format("Increase chance of double coin \nby <c1>%d%%</c1>", percent);
}
