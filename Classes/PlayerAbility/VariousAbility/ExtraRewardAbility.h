//
//  ExtraRewardAbility.hpp
//  Monsters Express
//
//  Created by script/createPlayerAbility.py
//
//

#ifndef ExtraRewardAbility_hpp
#define ExtraRewardAbility_hpp

#include <stdio.h>

#include "PlayerAbility.h"

class ExtraRewardAbility : public PlayerAbility
{
public:
	CREATE_FUNC(ExtraRewardAbility);

public:
	ExtraRewardAbility();
	virtual std::string toString();
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	virtual std::string getDesc();
};

#endif /* NoneAbility_hpp */
