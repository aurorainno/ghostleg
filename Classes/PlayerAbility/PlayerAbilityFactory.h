//
//  PlayerAbilityFactory.hpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#ifndef PlayerAbilityFactory_hpp
#define PlayerAbilityFactory_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "PlayerAbility.h"

USING_NS_CC;

class PlayerAbility;

class PlayerAbilityFactory
{
public:
	static std::string getTypeName(const PlayerAbility::Type &type);
	static PlayerAbility::Type parseType(const std::string &typeStr);
public:
	static PlayerAbilityFactory *instance();
	static PlayerAbility *create(const PlayerAbility::Type &type);
	static PlayerAbility *create(const std::string &abilityStr);
	
private:
	PlayerAbilityFactory();
};



#endif /* PlayerAbilityFactory_hpp */
