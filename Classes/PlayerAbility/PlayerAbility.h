//
//  BasePlayerAbility.hpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#ifndef BasePlayerAbility_hpp
#define BasePlayerAbility_hpp

#include <stdio.h>


#include <stdio.h>
#include <string>
#include <map>
#include "cocos2d.h"

#include "CommonMacro.h"
#include "CommonType.h"

USING_NS_CC;

class GameplaySetting;

class PlayerAbility : public Ref
{
public:		// static
	enum Type {
		PlayerAbilityNone,				// none
		
		// Attribute
		PlayerAbilityBonusAttribute,	// Bonus Max Speed, Stun, Turn Speed, Acceleration
	
		//
		PlayerAbilityBonusTip,			// bonus_tip
		PlayerAbilityRatingBonus,		// Extra Coin for Rating
		PlayerAbilityExtraTime,			// Extra Timelimit for the Order
		PlayerAbilityExtraReward,		// Extra reward from npc
		PlayerAbilityStunReduction,		// Reduction of the stun
		PlayerAbilityDoubleStarChance,	// Double Star Chance
		
		// Reward
		PlayerAbilityKillReward,
		PlayerAbilityBonusScore,
		PlayerAbilityTravelReward,
		
		// PowerUP & Booster
		PlayerAbilityPowerUp,
		
		PlayerAbilityBooster,
		PlayerAbilityAttackBlocker,
		PlayerAbilitySlowChaser,
		
		// Starting Booster or Power UP
		PlayerAbilityStartBooster,
		PlayerAbilityStartPowerUp,
		
		PlayerAbilityEndOfList,			// No for use, just treat as a terminator
	};
	
	
public:			// properties
	CC_SYNTHESIZE(Type, mType, Type);
	SYNTHESIZE_BOOL(mIsGlobalEffect, GlobalEffect);	// is the ability apply to all character
	CC_SYNTHESIZE(float, mParam1, Param1);
	CC_SYNTHESIZE(float, mParam2, Param2);
	CC_SYNTHESIZE(float, mParam3, Param3);
	
	
	void updateSetting(GameplaySetting *setting, bool onlyGlobal = false);
	
	virtual void parse(const std::string &paramStr);
	virtual void updateGameplaySetting(GameplaySetting *setting);
	
	virtual float getMainValue();				// the main value 
	
public:
	PlayerAbility(Type type);
	virtual bool init();
	
	virtual std::string getDesc();	// For UI
	
	virtual std::string toString();
};


#endif /* BasePlayerAbility_hpp */
