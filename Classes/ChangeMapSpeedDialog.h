//
//  ChangeMapSpeedDialog.hpp
//  GhostLeg
//
//  Created by Calvin on 24/11/2016.
//
//

#ifndef ChangeMapSpeedDialog_hpp
#define ChangeMapSpeedDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class ChangeMapSpeedDialog : public Layer
{
public:
    CREATE_FUNC(ChangeMapSpeedDialog);
    
    virtual bool init() override;
    
    void setupUI();
    
    void setupListener();
    void updateText();
    
private:
    Button* mAddBtn;
    Button* mReduceBtn;
    Button* mResetBtn;
    Button* mBackBtn;
    Text* mOffsetText;
};

#endif /* ChangeMapSpeedDialog_hpp */
