//
//  LuckyDrawDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 2/2/2017.
//
//

#include "LuckyDrawDialog.h"
#include "CommonMacro.h"
#include "NotEnoughMoneyDialog.h"
#include "LuckyDrawLayer.h"
#include "LuckyDrawManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "AdManager.h"
#include "TimeHelper.h"
#include "CommonType.h"
#include "Analytics.h"
#include "ViewHelper.h"

LuckyDrawDialog::LuckyDrawDialog()
:mPosterIndex(0)
,mOnFreeDrawCallback(0)
,mOnExitCallback(0)
{}

bool LuckyDrawDialog::init()
{
    if(!Layer::init()){
        return false;
    }
    
    Node* rootNode = CSLoader::createNode("gui/LuckyDrawDialog.csb");
    setupUI(rootNode);
    addChild(rootNode);
    scheduleUpdate();
    
    return true;
}

void LuckyDrawDialog::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    Node *mainPanel = rootNode->getChildByName("mainPanel");
    
    mStarText = (Text*) rootNode->getChildByName("totalCoinText");
    mDiamondText = rootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mFbConnectBtn = (Button*) rootNode->getChildByName("fbBtn");
    mFbName = (Text *) rootNode->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) rootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
    mFbConnectBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });

    Node* bgFrame = mainPanel->getChildByName("bgFrame");
    int zOrder = 0;
    for(Node* child : bgFrame->getChildren()){
        Sprite* bgSprite = dynamic_cast<Sprite*>(child);
        bgSprite->setLocalZOrder(zOrder);
        mPosterList.push_back(bgSprite);
        zOrder++;
    }
    
    CallFunc* callFunc = CallFunc::create([=]{
        this->fadeInNextPoster();
    });
    
    DelayTime* delay = DelayTime::create(4);
    RepeatForever* repeat = RepeatForever::create(Sequence::create(delay, callFunc, NULL));
    runAction(repeat);

    
    mDrawBtn = mainPanel->getChildByName<Button*>("purchaseBtn");
    mAdBtn = mainPanel->getChildByName<Button*>("useAdButton");
    mCancelBtn = mainPanel->getChildByName<Button*>("backBtn");
    
    mAdBtn->addClickEventListener([=](Ref*){
        playVideoAd();
    });
    
    mDrawBtn->addClickEventListener([&](Ref*){
        int price = LuckyDrawManager::instance()->getDrawPrice().amount;
        int playerPrice = GameManager::instance()->getMoneyValue(MoneyType::MoneyTypeDiamond);
        
        if(playerPrice>=price){
            LuckyDrawLayer *layer = LuckyDrawLayer::create();
            layer->setIsFreeDraw(false);
            layer->setOnExitCallback([=]{
              this->updateStars();
              this->updateDiamond();
          });
            addChild(layer);
        }else{
			showNotEnoughDialog();
        }
        
//        this->removeFromParent();
    });
    
    Text* title = mDrawBtn->getChildByName<Text*>("title");
    title->setString(StringUtils::format("%d",LuckyDrawManager::instance()->getDrawPrice().amount));
    
    
    mCancelBtn->addClickEventListener([=](Ref*){
        this->removeFromParent();
    });
    
    NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
    dialog->setMode(NotEnoughMoneyDialog::DiamondMode);
    dialog->setRedirectByScene(false);
    dialog->setAutoremove(false);
    dialog->setVisible(false);
    dialog->setCallbackAfterRedirect([=](){
        this->updateStars();
        this->updateDiamond();
        mNotEnoughDialog->setVisible(false);
    });
    rootNode->addChild(dialog);
    mNotEnoughDialog = dialog;
    
}

void LuckyDrawDialog::update(float delta)
{
    setWatchAdBtn();
}

bool LuckyDrawDialog::playVideoAd()
{
	return AdManager::instance()->playVideoAd(AdManager::AdFreeGacha,
							[&](bool isFinished){
										handlePlayAdEnd();
					});
	
	
	
	// Old code
//    // Android and iOS ad colony behaviour
//    //	in IOS, sequence is rewarded -> finished
//    //	in Android, sequence is finished -> rewarded
//    
//    // Note: We can use Reward for Revive, because it has a daily cap!!
//    // Show Video Ad
//    AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
//        handlePlayAdEnd();
//    });
//    
//    AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
//        
//    });
//    
//    bool isOkay = AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
//    
////    Analytics::instance()->logGameContinue("continue_with_ad", 2200);	// 15 stars
//    
//    return isOkay;
}


void LuckyDrawDialog::handlePlayAdEnd()
{
    int timeInSec = TimeHelper::getCurrentTimeStampInSecSinceMay2017();
    GameManager::instance()->getUserData()->setGachaLastPlayAdTime(timeInSec);
    setWatchAdBtn();
    if(mOnFreeDrawCallback){
        mOnFreeDrawCallback();
    }
    LuckyDrawLayer *layer = LuckyDrawLayer::create();
    layer->setIsFreeDraw(true);
    addChild(layer);
}

void LuckyDrawDialog::setWatchAdBtn()
{
    int lastPlayAdTime =  GameManager::instance()->getUserData()->getGachaLastPlayAdTime();
    int currentTime = TimeHelper::getCurrentTimeStampInSecSinceMay2017();
    
    int timeDiff = currentTime - lastPlayAdTime;
    if(timeDiff < 0){
        //unexpected behaviour, maybe user cheating
        return;
    }
    
    Text* content = mAdBtn->getChildByName<Text*>("text");
    if(timeDiff > kPlayAdCountDownInSec){
        content->setString("watch ad");
        content->enableOutline(Color4B(0x12, 0x97, 0x48, 0xff));
        mAdBtn->setEnabled(true);
    }else{
        int remainTime = kPlayAdCountDownInSec - timeDiff;
        map<std::string,int> displayTimeMap = TimeHelper::parseMillis(remainTime*1000);
        std::string timeString = StringUtils::format("%02d:%02d:%02d",displayTimeMap["hours"],displayTimeMap["minutes"],displayTimeMap["seconds"]);
        content->setString(timeString);
        content->enableOutline(Color4B(0x67, 0x67, 0x67, 0xff));
        mAdBtn->setEnabled(false);
    }
}


void LuckyDrawDialog::fadeInNextPoster()
{
    Sprite* currentPoster = mPosterList.at(mPosterIndex);
    if(mPosterIndex+1 > mPosterList.size() - 1){
        mPosterIndex = -1;
    }
    Sprite* nextPoster = mPosterList.at(mPosterIndex+1);
    
    nextPoster->setVisible(true);
    nextPoster->setOpacity(0);
    
    for(int i=0; i<mPosterList.size(); i++){
        if(i!=mPosterIndex+1){
            mPosterList.at(i)->setLocalZOrder(0);
        }else{
            mPosterList.at(i)->setLocalZOrder(3);
        }
    }
    
    FadeIn* fadeIn = FadeIn::create(1);
    nextPoster->runAction(fadeIn);

    mPosterIndex++;
}

void LuckyDrawDialog::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void LuckyDrawDialog::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void LuckyDrawDialog::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
		
		//mFbName->setString(user->getName());
		ViewHelper::setUnicodeText(mFbName, user->getName());
		
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
//        mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}


void LuckyDrawDialog::onExit()
{
    Layer::onExit();
    if(mOnExitCallback){
        mOnExitCallback();
    }
    
    
    
}

void LuckyDrawDialog::onEnter()
{
    Layer::onEnter();
    updateDiamond();
    updateStars();
    setupFbProfile();
    setWatchAdBtn();
	
	LOG_SCREEN(Analytics::scn_luckydraw1);
}

void LuckyDrawDialog::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}

void LuckyDrawDialog::setOnExitCallback(std::function<void()> callback)
{
    mOnExitCallback = callback;
}

void LuckyDrawDialog::setOnFreeDrawCallback(std::function<void()> callback)
{
    mOnFreeDrawCallback = callback;
}


void LuckyDrawDialog::showNotEnoughDialog()
{
    mNotEnoughDialog->setVisible(true);
    
	//dialog->setMode(NotEnoughMoneyDialog::Dialog)
	
	
}
