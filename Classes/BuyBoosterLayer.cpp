//
//  BuyBoosterLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 24/1/2017.
//
//

#include "BuyBoosterLayer.h"
#include "CommonMacro.h"
#include "RichTextLabel.h"
#include "ItemManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "CoinShopScene.h"
#include "NotEnoughMoneyDialog.h"
#include "Analytics.h"
#include "MainScene.h"

const Color4B kColorYellow = Color4B(255, 211, 49, 255);
const Color4B kColorGreen = Color4B(14, 255, 217, 255);

BuyBoosterLayer::BuyBoosterLayer()
{}

bool BuyBoosterLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("BuyBoosterLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    //
    //    setVisible(false);
	
	LOG_SCREEN(Analytics::scn_booster_buy);
	
    return true;
}

void BuyBoosterLayer::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    mMainPanel = rootNode->getChildByName("mainPanel");
    mTopPanel = rootNode->getChildByName("topPanel");
    
    mNextBtn = (Button*) mMainPanel->getChildByName("rightButton");
    mNextBtn->addClickEventListener([&](Ref*){
        this->onAmountChange(1);
    });

    mPreviousBtn = (Button*) mMainPanel->getChildByName("leftButton");
    mPreviousBtn->addClickEventListener([&](Ref*){
        this->onAmountChange(-1);
    });
    
    mCancelBtn = (Button*) mMainPanel->getChildByName("cancelBtn");
    mCancelBtn->addClickEventListener([&](Ref*){
        if(this->getParent()){
            this->removeFromParent();
        }
    });
    
    
    
    mPurchaseBtn = (Button*) mMainPanel->getChildByName("purchaseBtn");
    mPurchaseBtn->addClickEventListener([&](Ref*){
        bool canBuy;
        int price = mOriginalPrice * mCurrentAmount;
        
        if(mMoneyType == MoneyTypeStar){
            int ownedCoin = GameManager::instance()->getUserData()->getTotalCoin();
            canBuy = ownedCoin >= price;
        }else if(mMoneyType == MoneyTypeDiamond){
            int ownedDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
            canBuy = ownedDiamond >= price;
        }
        
        if(!canBuy){
//            auto scene = CoinShopSceneLayer::createScene();
//            Director::getInstance()->pushScene(scene)
            
            if(mMoneyType == MoneyTypeStar){
                mNotEnoughDialog->setMode(NotEnoughMoneyDialog::DialogMode::StarMode);
            }else if(mMoneyType == MoneyTypeDiamond){
                mNotEnoughDialog->setMode(NotEnoughMoneyDialog::DialogMode::DiamondMode);
            }

            mNotEnoughDialog->setVisible(true);
            
        }else {
            if(mMoneyType == MoneyTypeStar){
                GameManager::instance()->getUserData()->addCoin(-price);
            }else if(mMoneyType == MoneyTypeDiamond){
                GameManager::instance()->getUserData()->addDiamond(-price);
            }
            
            if(mPurchaseSuccessCallback){
                mPurchaseSuccessCallback(mCurrentAmount);
            }
			
			logPurchase(mMoneyType, price, mCurrentAmount);
			
			this->removeFromParent();
        }
        
		
    });
    
    mTotalCoinText = (Text *) mTopPanel->getChildByName("totalCoinText");
    mDiamondPanel = mTopPanel->getChildByName("diamondPanel");
    mFbName = (Text *) mTopPanel->getChildByName("fbName");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    
    // mProfilePic->setPlaceHolderImage("guiImage/default_profilepic.png");
    
    mProfilePicFrame = (Sprite *) mTopPanel->getChildByName("profilePicFrame");
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);

    
    mPriceText = mPurchaseBtn->getChildByName<Text*>("title");
    mStarSprite = mPurchaseBtn->getChildByName<Sprite*>("starSprite");
    mDiamondSprite = mPurchaseBtn->getChildByName<Sprite*>("diamondSprite");
    
    mItemIconSprite = mMainPanel->getChildByName<Sprite*>("itemSprite");
    
    mCountPanel = mMainPanel->getChildByName("counterPanel");
    mCountText = mMainPanel->getChildByName<Text*>("countText");
    
    mItemName = mMainPanel->getChildByName<Text*>("skillName");
    
    
    mInfoLabel = mMainPanel->getChildByName<Text*>("skillInfo");
//    mInfoLabel = RichTextLabel::createFromText(infoLabel);
//    mInfoLabel->setTextColor(1, kColorGreen);
//    mInfoLabel->setTextColor(2, kColorYellow);
//    mMainPanel->addChild(mInfoLabel);
    
}

void BuyBoosterLayer::onEnter()
{
    Layer::onEnter();
    setupNotEnoughDialog();
    updateDiamond();
    updateTotalCoin();
    setupFBProfile();
}



void BuyBoosterLayer::logPurchase(MoneyType moneyType, int price, int amount)
{
//
	
	if(moneyType != MoneyTypeStar && moneyType != MoneyTypeDiamond){
		log("logPurchase: unknown money type");
		return;
	}		
	
	std::string itemName = ItemManager::instance()->getBoosterName(mMyItemType);
	Analytics::instance()->logConsume(Analytics::act_buy_booster, itemName, price, moneyType);
	
	Analytics::instance()->logCollectBooster(Analytics::from_booster_shop, mMyItemType, amount);
	
}


void BuyBoosterLayer::setupNotEnoughDialog()
{
    NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
    dialog->setRedirectByScene(false);
    dialog->setAutoremove(false);
    dialog->setVisible(false);
    
    Node* parentNode = getParent();
    while (parentNode) {
        MainSceneLayer* mainScene = dynamic_cast<MainSceneLayer*>(parentNode);
        if(!mainScene){
            parentNode = parentNode->getParent();
        }else{
            break;
        }
    }
    
    if(parentNode){
        parentNode->addChild(dialog);
        dialog->setCallbackAfterRedirect([=](){
            mNotEnoughDialog->setVisible(false);
            MainSceneLayer* mainScene = static_cast<MainSceneLayer*>(parentNode);
            mainScene->updateDiamond();
            mainScene->updateTotalCoin();
            this->updateDiamond();
            this->updateTotalCoin();
        });
    }else{
        dialog->setCallbackAfterRedirect([=](){
            mNotEnoughDialog->setVisible(false);
            this->updateDiamond();
            this->updateTotalCoin();
        });

        addChild(dialog);
    }
    
    mNotEnoughDialog = dialog;
    
}

void BuyBoosterLayer::setupView(BoosterItemType type)
{
	mMyItemType = type;
    std::string boosterIconRes = getBoosterIconResNameByType(type);
    std::string itemName = ItemManager::instance()->getBoosterName(type);
    std::string itemInfo = ItemManager::instance()->getBoosterInfo(type);
    
    int ownedCount = ItemManager::instance()->getBoosterCount(type);
    
    Price price = ItemManager::instance()->getBoosterPrice(type);
    if(price.type == MoneyType::MoneyTypeStar){
        mStarSprite->setVisible(true);
        mDiamondSprite->setVisible(false);
    }else if(price.type == MoneyType::MoneyTypeDiamond){
        mStarSprite->setVisible(false);
        mDiamondSprite->setVisible(true);
    }
    
    mOriginalPrice = price.amount;
    mMoneyType = price.type;
    mCurrentAmount = 1;
    mCountText->setString(StringUtils::format("%d",mCurrentAmount));
    
    if(ownedCount>0){
        Text* playerOwnedLabel = mCountPanel->getChildByName<Text*>("countText");
        playerOwnedLabel->setString(StringUtils::format("%d",ownedCount));
    }else{
        mCountPanel->setVisible(false);
    }
    
    mPriceText->setString(StringUtils::format("%d",mOriginalPrice));
    
    mItemIconSprite->setTexture(boosterIconRes);
    
    mInfoLabel->setString(itemInfo);
    mItemName->setString(itemName);
}

void BuyBoosterLayer::onAmountChange(int amountToAdd)
{
    int amount = mCurrentAmount + amountToAdd;
    if(amount<1 || amount>99){
        return;
    }
    
    mCurrentAmount = amount;
    int price = mOriginalPrice * mCurrentAmount;
    
    mCountText->setString(StringUtils::format("%d",mCurrentAmount));
    mPriceText->setString(StringUtils::format("%d",price));
    
    mPreviousBtn->setVisible(true);
    mNextBtn->setVisible(true);
    
    if(mCurrentAmount == 1){
        mPreviousBtn->setVisible(false);
    }else if(mCurrentAmount == 99){
        mNextBtn->setVisible(false);
    }
}

std::string BuyBoosterLayer::getBoosterIconResNameByType(BoosterItemType type)
{
    switch(type){
        case BoosterItemSnowball:
        {
            return "guiImage/ui_booster_item4.png";
        }
        case BoosterItemMissile:
        {
            return "guiImage/ui_booster_item1.png";
        }
        case BoosterItemTimeStop:
        {
            return "guiImage/ui_booster_item2.png";
        }
        case BoosterItemDoubleCoin:
        {
            return "guiImage/ui_booster_item5.png";
        }
        case BoosterItemUnbreakable:
        {
            return "guiImage/ui_booster_item3.png";
        }
        default:
            return "";
    }
}

std::string BuyBoosterLayer::getBoosterBgResNameByType(BoosterItemType type)
{
    int id = static_cast<int>(type);
    return StringUtils::format("guiImage/ability_bg_%02d.png",id);
}

void BuyBoosterLayer::setPurchaseSuccessCallback(std::function<void (int)> callback)
{
    mPurchaseSuccessCallback = callback;
}


void BuyBoosterLayer::setupFBProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        //
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        mFbName->setString(user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }
        mProfilePic->setWithFBId(user->getFbID());
    }else{
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }

}

void BuyBoosterLayer::updateTotalCoin()
{
    char temp[100];
    sprintf(temp, "%d", GameManager::instance()->getUserData()->getTotalCoin());
    if(mTotalCoinText) {
        mTotalCoinText->setString(temp);
    }
}

void BuyBoosterLayer::updateDiamond()
{
    if(mDiamondPanel == nullptr){
        return;
    }
    
    Text* diamondText = mDiamondPanel->getChildByName<Text*>("totalDiamondText");
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    diamondText->setString(StringUtils::format("%d",numOfDiamond));
}



