//
//  GameplaySetting.cpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#include "GameplaySetting.h"
#include <set>

#define RETURN_CASE_NAME(__x)	case __x			: return #__x;


std::string GameplaySetting::getAttributeName(const Attribute &attribute)
{
	switch (attribute) {
		RETURN_CASE_NAME(BonusMaxSpeed)
		RETURN_CASE_NAME(BonusTurnSpeed)
		RETURN_CASE_NAME(BonusStunReduction)
		RETURN_CASE_NAME(BonusAcceleration)
			
		RETURN_CASE_NAME(BonusTipPercent)
		RETURN_CASE_NAME(BonusReward)
		
		RETURN_CASE_NAME(DoubleStarPercent)
		RETURN_CASE_NAME(ExtraTimeLimit)
			
		// PowerUp & Booster Related
		RETURN_CASE_NAME(MissleSpeedReduction)
			
		default							: return StringUtils::format("attr-%d", attribute);
	}
}

GameplaySetting::GameplaySetting()
: Ref()
, mAttributeMap()
, mExtraRewardMap()
, mMaxSpeed(200)
, mTurnSpeed(100)
, mAcceleration(100)
, mStunReduction(0.3)
{
	
}

bool GameplaySetting::init()
{
	return true;
}

std::vector<GameplaySetting::Attribute> GameplaySetting::getAttributeList()
{
	std::vector<Attribute> result;
	
	for (std::map<Attribute,float>::iterator it=mAttributeMap.begin();
					it!=mAttributeMap.end(); ++it) {
		
		result.push_back(it->first);
	}
	return result;
}

void GameplaySetting::add(GameplaySetting *otherSetting)
{
	// std::map<Attribute, float> mAttributeMap;
	if(otherSetting == nullptr) {
		return;
	}
	
	std::vector<Attribute> otherAttribute = otherSetting->getAttributeList();
	
	if(otherAttribute.size() == 0) {
		return;	// no need
	}

	for(Attribute attribute : otherAttribute) {
		//attributeSet.insert(attribute);
		float value = otherSetting->getAttributeValue(attribute);
		
		addAttributeValue(attribute, value);
	}
	
	
	
	
	//
//	for (std::map<Attribute,float>::iterator it=mymap.begin(); it!=mymap.end(); ++it)
//		std::cout << it->first << " => " << it->second << '\n';
//	
}

std::string GameplaySetting::toString(bool skipUndefined)
{
	std::string resultStr = "GameplaySetting\n";
	
	int start = (int) BonusTipPercent;
	int end = (int) (AttributeEnd);
	
	
	resultStr += "Attribute:\n";
	for(int value=start; value < end; value++) {
		//log("DEBUG: value=%d", value);
		Attribute attribute = (Attribute) value;
		if(mAttributeMap.find(attribute) == mAttributeMap.end()) {		// no such attribute
			continue;
		}
		
		std::string attributeName = getAttributeName(attribute);
		
		float attributeValue = getAttributeValue(attribute);
		
		resultStr += StringUtils::format("%s=%f\n", attributeName.c_str(), attributeValue);
	}
	
//	resultStr += "RatingReward:\n";
//	for (std::map<int,int>::iterator it=mExtraRewardMap.begin(); it!=mExtraRewardMap.end(); ++it) {
//		int rating = it->first;
//		int value = it->second;
//		
//		resultStr += StringUtils::format("rating=%d value=%d", rating, value);
//		resultStr += "\n";
//	}
	
	return resultStr;
}

void GameplaySetting::setBetterAttributeValue(Attribute attribute, float value)
{
	float currentValue = getAttributeValue(attribute);
	if(currentValue > value) {
		return;
	}
	setAttributeValue(attribute, value);
}



void GameplaySetting::setAttributeValue(Attribute attribute, float value)
{
	mAttributeMap[attribute] = value;
}

void GameplaySetting::addAttributeValue(Attribute attribute, float value)
{
	float currentValue = getAttributeValue(attribute);
	setAttributeValue(attribute, (currentValue + value));
}

float GameplaySetting::getMainAttribute(Attribute attribute)
{
	// Main Attribute
	float baseValue;
	float bonusPercent;
	
	switch(attribute) {
		case MaxSpeed:
		{
			baseValue = mMaxSpeed; bonusPercent = getAttributeValue(BonusMaxSpeed);
			break;
		}
			
		case TurnSpeed:
		{
			baseValue = mTurnSpeed; bonusPercent = getAttributeValue(BonusTurnSpeed);
			break;
		}
			
		case StunReduction:
		{
			baseValue = mStunReduction; bonusPercent = getAttributeValue(BonusStunReduction);
			break;
		}
			
		case Acceleration:
		{
			baseValue = mAcceleration; bonusPercent = getAttributeValue(BonusAcceleration);
			break;
		}
			
		default: {
			return 0;
		}
	}
	
	
	float returnValue = baseValue;
	returnValue += baseValue * bonusPercent / 100;
	
	return returnValue;
}


float GameplaySetting::getAttributeValue(Attribute attribute)
{
	
	
	// Ability Attribute
	if(mAttributeMap.find(attribute) == mAttributeMap.end()) {
		return 0;
	}
	
	return mAttributeMap.at(attribute);
}

void GameplaySetting::reset()
{
	mAttributeMap.clear();
	mExtraRewardMap.clear();
}

#pragma mark - Extra Reward
void GameplaySetting::setExtraReward(int rating, int extraReward)
{
	mExtraRewardMap[rating] = extraReward;
}

int GameplaySetting::getExtraReward(int rating)
{
	if(mExtraRewardMap.find(rating) == mExtraRewardMap.end()) {
		return 0;
	}
	
	return mExtraRewardMap.at(rating);
}
