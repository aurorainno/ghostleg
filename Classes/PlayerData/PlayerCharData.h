//
//  PlayerCharData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#ifndef PlayerCharData_hpp
#define PlayerCharData_hpp

#include <stdio.h>


#include "JSONData.h"
#include "cocos2d.h"
#include "CommonType.h"
#include <stdio.h>
#include <map>

USING_NS_CC;

class PlayerCharData : public Ref, public JSONData
{
public:
	CC_SYNTHESIZE(int, mCharID, CharID);
	CC_SYNTHESIZE(int, mCurrentLevel, CurrentLevel);		// 0 - locked, 1 - different level
	CC_SYNTHESIZE(int, mCollectedFragment, CollectedFragment);
    CC_SYNTHESIZE(bool, mIsNewChar, IsNewChar);
	
public:
	CREATE_FUNC(PlayerCharData);
	
	PlayerCharData();
	
public:
	virtual bool init() { return true; }
	
	void alterFragmentCount(int change);
	void upgradeLevel();
	void reset();
	void resetLevel();
	
	std::string toString();
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);	// object value -> JSON
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
	
};


#endif /* PlayerCharData_hpp */
