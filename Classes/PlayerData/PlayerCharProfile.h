//
//  PlayerCharProfile.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#ifndef PlayerCharProfile_hpp
#define PlayerCharProfile_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "CommonType.h"
#include <stdio.h>
#include <map>

#include "PlayerCharData.h"
#include "CharGameData.h"
#include "CharUpgradeCost.h"

class GameplaySetting;

USING_NS_CC;

class PlayerCharProfile : public Ref
{
public: // Type and Enum
	enum UpgradeStatus
	{
		UpgradeOk,
		UpgradeNotEnoughFragment,
		UpgradeNotEnoughMoney,
		UpgradeAlreadyMax,
		UpgradeError,
		UpgradeAlreadyUnlocked,
	};
	
public:
	CC_SYNTHESIZE(int, mCharID, CharID);
	CC_SYNTHESIZE(PlayerCharData *, mPlayerCharData, PlayerCharData);
	CC_SYNTHESIZE(CharGameData *, mCharGameData, CharGameData);
	
public:
	CREATE_FUNC(PlayerCharProfile);
	
	PlayerCharProfile();
	
public:
	virtual bool init() { return true; }
	
	void setup(int charID);
	void update();
	void updateWithSetting(GameplaySetting *setting);
	
	void updateGlobalGameplaySetting(GameplaySetting *playSetting);
	void updateGameplaySetting(GameplaySetting *playSetting);
	
	std::string getName();
	
	// Attribute
	int getMaxSpeed();
	float getAcceleration();
	int getTurnSpeed();
	float getStunReduction();		//
	
	// Level Related
	int getCurrentLevel();
	bool isLocked();
	bool isMaxLevel();
    
    // New unlocked character
    bool isNewChar() ;
    void setNewChar(bool isNewChar);
	
	// Upgrade
	int getUpgradeFragCount();	// or unlock Cost
	int getUpgradeMoney();
	int getCollectedFragCount();
	
	// Upgrade Logic
	UpgradeStatus upgrade();
	void doUpgradeLogic();		// just for debugging purpose
	void upgradeToMax();		// just for debugging purpos
	int alterFragmentCount(int change);
	
	// Description
	std::string getUpgradeDescription();
	std::string getUnlockDescription();
	
	std::string toString();
	std::string infoProfile();		// for Testing Character Selection
	std::string infoAbilityList();
	
	Vector<PlayerAbility *> getActiveAbilityList();
	PlayerAbility *getAbilityByLevel(int level);
	
private:
	void updateAbilityBuff();
	
	
private:
	int mMaxSpeed;
	int mTurnSpeed;
	float mAcceleration;
	float mStunReduction;
};


#endif /* PlayerCharProfile_hpp */
