//
//  PlayerBooster.cpp
//  GhostLeg
//
//  Created by Ken Lee on 19/1/2017.
//
//

#include "PlayerBooster.h"
#include "JSONHelper.h"

PlayerBooster::PlayerBooster()
: Ref()
{
	
}

bool PlayerBooster::init()
{
	return true;
}

void PlayerBooster::setBoosterCount(BoosterItemType type, int value)
{
	mBoosterCount[(int) type] = value;
}

#pragma mark - JSON Data
void PlayerBooster::defineJSON(rapidjson::Document::AllocatorType &allocator,
							   rapidjson::Value &outValue)
{

	for (std::map<int, int>::iterator it=mBoosterCount.begin(); it!=mBoosterCount.end(); ++it)
	{
		int key = it->first;
		int value = it->second;
		
		std::string keyName = StringUtils::format("booster_%d", key);
		
		addInt(allocator, keyName, value, outValue);
	}
}

bool PlayerBooster::parseJSON(const rapidjson::Value &jsonValue)
{
	//aurora::JSONHelper::getJsonStrIntMap(jsonValue, <#const std::string &key#>, <#std::map<std::string, int> &outMap#>)
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr)
	{
		std::string key = itr->name.GetString();
		const rapidjson::Value &object = jsonValue[key.c_str()];
		int value = object.GetInt();
		
		int keyValue;
		sscanf(key.c_str(), "booster_%d", &keyValue);
		
		setBoosterCount((BoosterItemType)keyValue, value);
	}

	
//	setUid(aurora::JSONHelper::getInt(json, "uid", 0));
//	setName(aurora::JSONHelper::getString(json, "name", "Player"));
//	setFbID(aurora::JSONHelper::getString(json, "fbid", ""));
//	setCountry(aurora::JSONHelper::getString(json, "country", kDefaultCountryCode));

	
	
	return true;
}


void PlayerBooster::setDefault()
{
	BoosterItemType boosterType[5] = {
		BoosterItemMissile,
		BoosterItemTimeStop,
		BoosterItemSnowball,
		BoosterItemUnbreakable,
		BoosterItemDoubleCoin,
	};
	
	for(int i=0; i<5; i++) {
		setBoosterCount(boosterType[i], 1);
	}
	//void
}

int PlayerBooster::getBoosterCount(BoosterItemType type)
{
	return mBoosterCount[type];
}

void PlayerBooster::addBooster(BoosterItemType type, int addValue)
{
	changeBooster(type, addValue);
}

void PlayerBooster::deduceBooster(BoosterItemType type, int deduceValue)
{
	changeBooster(type, -deduceValue);
}

void PlayerBooster::changeBooster(BoosterItemType type, int changeValue)
{
	int count = getBoosterCount(type);
	int newCount = count + changeValue;
	if(newCount < 0) {
		newCount = 0;
	}
	
	mBoosterCount[type] = newCount;
}

std::string PlayerBooster::toString()
{
	std::string result = "";
	for (std::map<int, int>::iterator it=mBoosterCount.begin(); it!=mBoosterCount.end(); ++it)
	{
		int key = it->first;
		int value = it->second;
		
		std::string keyName = StringUtils::format("booster_%d=%d", key, value);

		result += keyName + " ";
	}
	
	return result;
}
//
//std::string PlayerBooster::generateDataContent()
//{
//	return "";
//}
//
//void PlayerBooster::parseDataContent(const std::string &content)
//{
//	
//}
//
//void PlayerBooster::setDefault()
//{
//	
//}
