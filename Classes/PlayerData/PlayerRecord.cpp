//
//  PlayerRecord.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/10/2016.
//
//

#include "PlayerRecord.h"
#include "JSONHelper.h"
#include "StringHelper.h"
#include "PlayerGameResult.h"

#define STR_EQUAL(__str1__, __str2__)	strcmp(__str1__, __str2__) == 0

#define kKeyPlayTimeRecord "playTimeRecord"

const char *PlayerRecord::KeyBestDistance = "bestDistance";
const char *PlayerRecord::KeyPlayCount = "playCount";
const char *PlayerRecord::KeyStarTotal = "starTotal";
const char *PlayerRecord::KeyStarInOneGame = "starOneGame";
const char *PlayerRecord::KeyPlayDayCount = "playDayCount";
const char *PlayerRecord::KeyLastPlayTime = "lastPlayTime";
const char *PlayerRecord::KeyKillTotal = "killTotal";
const char *PlayerRecord::KeyKillInOneGame = "killOneGame";
const char *PlayerRecord::KeyHasClear = "hasClear";


namespace {
	PlayerRecord::RecordUpdateType getUpdateType(const char *input) {
		
		// Best
		if(STR_EQUAL(PlayerRecord::KeyBestDistance, input)
			|| STR_EQUAL(PlayerRecord::KeyStarInOneGame, input)
		    || STR_EQUAL(PlayerRecord::KeyKillInOneGame, input)
		    || STR_EQUAL(PlayerRecord::KeyHasClear, input))
		{
			return PlayerRecord::eUpdateTypeBest;
		}
		
		return PlayerRecord::eUpdateTypeIncrement;
	}
}


PlayerRecord::PlayerRecord()
: BasePlayerData("playerRecord")		//
, mRecordDataMap()
, mPlayerCharDataMap()
{
	setupMockPlayerCharData();		// For testing
}

std::map<std::string, int> PlayerRecord::getDogRecordMap(int dogID)
{
	std::vector<std::string> keys;
	keys.push_back(KeyPlayCount);
	keys.push_back(KeyStarTotal);
	keys.push_back(KeyStarInOneGame);
	keys.push_back(KeyKillTotal);
	keys.push_back(KeyKillInOneGame);
	keys.push_back(KeyBestDistance);
	keys.push_back(KeyHasClear);
	
	std::map<std::string, int> result;
	
	for(int i=0; i<keys.size(); i++) {
		std::string key = keys[i];
		
		result[key] = getDogRecord(key.c_str(), dogID);
	}
	
	return result;
}


std::string PlayerRecord::toString()
{
	std::string result = BasePlayerData::toString() + "\n";
	
	
	for (std::map<std::string, int>::iterator it=mRecordDataMap.begin(); it!=mRecordDataMap.end(); ++it) {
		std::string key = it->first;
		int value = it->second;
		
		result += StringUtils::format("%s=%d\n", key.c_str(), value);
	}
	
	return result;
}


#pragma mark - Helper Method
int PlayerRecord::getPlanetRecord(const char *key, int planetID)
{
	std::string recordKey = StringUtils::format("%s_planet%03d", key, planetID);
	
	return getRecordData(recordKey);
}



int PlayerRecord::getDogRecord(const char *key, int dogID)
{
	std::string recordKey = StringUtils::format("%s_dog%03d", key, dogID);

	return getRecordData(recordKey);
}


bool PlayerRecord::getNewRecordValue(const std::string &recordKey, RecordUpdateType updateType, int newValue, int &outValue)
{
	bool anyUpdate = true;		// usually will update
	
	int currentValue = getRecordData(recordKey);
	
	switch (updateType) {
		case eUpdateTypeBest:
		{
			anyUpdate = newValue > currentValue;		// note: record break
			outValue = anyUpdate ? newValue : currentValue;
			break;
		}
		
		case eUpdateTypeIncrement:
		{
			anyUpdate = true;
			outValue = currentValue + newValue;
			break;
		}
			
		case eUpdateTypeSet:
		{
			anyUpdate = true;
			outValue = newValue;
			break;
		}
		
		default: {
			break;
		}
	}
	
	
	return anyUpdate;
}


bool PlayerRecord::updateRecordData(const char *prefix, const char *suffix, int newValue)
{
	std::string recordKey = StringUtils::format("%s%s", prefix, suffix);
	
	PlayerRecord::RecordUpdateType updateType = getUpdateType(prefix);
	
	int finalValue;
	bool anyUpdate = getNewRecordValue(recordKey, updateType, newValue, finalValue);
	
	/// PlayerRecord::RecordUpdateType updateType =
	if(anyUpdate) {
		setRecordData(recordKey, finalValue);
	}
	
	bool isRecordBreak = eUpdateTypeBest == updateType && anyUpdate;
	
	return isRecordBreak;
	
}

bool PlayerRecord::setDogRecord(const char *key, int dogID, int newValue)			// true if the record just break
{
	std::string suffix = StringUtils::format("_dog%03d", dogID);
	
	return updateRecordData(key, suffix.c_str(), newValue);
}

bool PlayerRecord::setPlanetRecord(const char *key,  int planetID, int newValue)		// true if the record just break
{
	std::string suffix =  StringUtils::format("_planet%03d", planetID);
	
	return updateRecordData(key, suffix.c_str(), newValue);
	
	
}

bool PlayerRecord::setPlayerRecord(const char *key, int newValue)
{
	return updateRecordData(key, "", newValue);
}

void PlayerRecord::clear()
{
	mRecordDataMap.clear();
	
	save();
}

#pragma mark - Parse, Generate logic
std::string PlayerRecord::generateDataContent()
{
	return toJSONContent();
}

void PlayerRecord::parseDataContent(const std::string &content)
{
	parseJSONContent(content);
}

void PlayerRecord::setDefault()
{
	
}


void PlayerRecord::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
	
	for (std::map<std::string, int>::iterator it=mRecordDataMap.begin(); it!=mRecordDataMap.end(); ++it) {
		std::string key = it->first;
		int value = it->second;
	
		addInt(allocator, key, value, outValue);
	}
}

bool PlayerRecord::parseJSON(const rapidjson::Value &jsonValue)
{
	mRecordDataMap.clear();
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr) {
		std::string key = itr->name.GetString();
		const rapidjson::Value &object = jsonValue[key.c_str()];
		int value = object.GetInt();

		// log("DEBUG: key=%s", key.c_str());
		
		mRecordDataMap[key] = value;
	}
	
	return true;
}



#pragma mark - Internal Data
void PlayerRecord::setRecordData(const std::string &recordKey, int value)
{
	mRecordDataMap[recordKey] = value;
}


int PlayerRecord::getRecordData(const std::string &recordKey)
{
	return mRecordDataMap[recordKey];
}

void PlayerRecord::updatePlayerRecordWithResult(PlayerGameResult *result)
{
	if(result == nullptr) {
		log("updatePlayerRecord: result is null");
		return;
	}
	
	// Key & value
	std::map<std::string, int> keyValueMap;
	keyValueMap[KeyBestDistance] = result->getDistance();
	keyValueMap[KeyPlayCount] = result->getPlayCount();
	keyValueMap[KeyStarInOneGame] = result->getStarCollected();
	keyValueMap[KeyStarTotal] = result->getStarCollected();
	keyValueMap[KeyKillInOneGame] = result->getKill();
	keyValueMap[KeyKillTotal] = result->getKill();
	keyValueMap[KeyHasClear] = result->hasClearStage() ? 1 : 0;
	
	// TODO: Add Kill stat
	
	for (std::map<std::string, int>::iterator it=keyValueMap.begin(); it!=keyValueMap.end(); ++it) {
		std::string key = it->first;
		int value = it->second;
		
		const char *k = key.c_str();
		
		setPlayerRecord(k, value);
		setDogRecord(k, result->getDogPlayed(), value);
		setPlanetRecord(k, result->getPlanetPlayed(), value);
	}
	
	// Play in a row (consecuting play in N days)

	save();
}


void PlayerRecord::updatePlayTimeRecord()
{
    
    // Getting Timestamp of NOW
    time_t timer;
    struct tm defaultTime = {0};
    double seconds;
    int days;
    
    defaultTime.tm_hour = 0;   defaultTime.tm_min = 0; defaultTime.tm_sec = 0;
    defaultTime.tm_year = 116; defaultTime.tm_mon = 0; defaultTime.tm_mday = 1;
    
    time(&timer);  /* get current time; same as: timer = time(NULL)  */
    
    seconds = difftime(timer,mktime(&defaultTime));
    // seconds = difftime(mktime(&virtualTime),mktime(&y2k));
    days = floor(seconds/86400);
    
    
    int lastPlayTime = getRecordData(KeyLastPlayTime);
    int lastPlayCount = getRecordData(KeyPlayDayCount);
    
    
    // loadTimeRecord();
    
    if(days == lastPlayTime){
        log("lastPlayTime=%d, continuousPlayDay=%d\n",lastPlayTime,lastPlayCount);
        return;
    }else if(lastPlayTime == 0){
        lastPlayCount = 1;
    }else if(std::abs(days - lastPlayTime) == 1){
        lastPlayCount++;
    }else{
        lastPlayCount = 1;
    }
    
//    if(mContinuousPlayDay==7){
//        mContinuousPlayDay = 1;
//        mHasPlayed7Days = true;
//    }
    
    lastPlayTime = days;
    
  //  saveTimeRecord();
    
    
    setRecordData(KeyLastPlayTime, lastPlayTime);
    setRecordData(KeyPlayDayCount, lastPlayCount);

    save();
    log("lastPlayTime=%d, continuousPlayDay=%d\n",lastPlayTime,lastPlayCount);
    
//    printf ("%d days since Oct 5, 2016 in the current timezone\n", days);
}

bool PlayerRecord::hasPlayed7Days()
{
    return mHasPlayed7Days;
}


int PlayerRecord::getPlayCount()
{
	return getRecordData(KeyPlayCount);
}


// ScoreWeight help to normalize different metrics for sorting
float PlayerRecord::getKeyScoreWeight(const std::string &keyStr)
{
	if(KeyStarTotal == keyStr) {
		return 0.01f;		// value / 100
	} else if(KeyStarTotal == keyStr) {
		return 0.1f;		// value / 10
	} else if(KeyKillTotal == keyStr) {
		return 5.0f;		// value / 10
	} else if(KeyKillInOneGame == keyStr) {
		return 7.0f;		// value / 10
	} else if(KeyPlayDayCount == keyStr) {
		return 100.0f;		// least suggest
	}
	
	return 1.0f;
}

std::string PlayerRecord::getUnlockMessageTemplate(const std::string &key)
{
//	const char *PlayerRecord::KeyBestDistance = "bestDistance";
//	const char *PlayerRecord::KeyPlayCount = "playCount";
//	const char *PlayerRecord::KeyStarTotal = "starTotal";
//	const char *PlayerRecord::KeyStarInOneGame = "starOneGame";
//	const char *PlayerRecord::KeyPlayDayCount = "playDayCount";
//	const char *PlayerRecord::KeyLastPlayTime = "lastPlayTime";
//	const char *PlayerRecord::KeyKillTotal = "killTotal";
//	const char *PlayerRecord::KeyKillInOneGame = "killOneGame";
	if(KeyBestDistance == key) {
		return "Travel #req metres#planet\nto unlock #item";
	} else if(KeyPlayCount == key) {
		return "Play #remain more time\nto unlock #item";
	} else if(KeyStarTotal == key) {
		return "Collect #remain more star\nto unlock #item";
	} else if(KeyStarInOneGame == key) {
		return "Collect #remain more star in a game\nto unlock #item";
	} else if(KeyPlayDayCount == key) {
		return "Play in the next #remain day\nto unlock #item";
	} else if(KeyKillTotal == key) {
		return "Rid off #remain more wolves\nto unlock #item";
	} else if(KeyKillInOneGame == key) {
		return "Rid off #remain more wolves in a game\nto unlock #item";
	}
	
	return "";
}



#pragma mark - PlayerCharData
void PlayerRecord::setupMockPlayerCharData()
{
	//PlayerCharData firstData
	PlayerCharData *data;
	
	// 1st Data
	data = PlayerCharData::create();
	data->setCharID(1);
	data->setCurrentLevel(1);
	data->setCollectedFragment(5);
	savePlayerCharData(data);
	
	// 2st Data
	data = PlayerCharData::create();
	data->setCharID(2);
	data->setCurrentLevel(0);
	data->setCollectedFragment(8);
	savePlayerCharData(data);
	

}

std::string PlayerRecord::infoPlayerCharData()
{
	std::string info = "";
	
	std::vector<int> keys = mPlayerCharDataMap.keys();
	for(int key : keys) {
		PlayerCharData *charData = mPlayerCharDataMap.at(key);
		
		info += charData->toString() + "\n";
	}
	
	return info;
}


PlayerCharData *PlayerRecord::createNewPlayerCharData(int charID)
{
	PlayerCharData *charData = PlayerCharData::create();
	charData->setCharID(charID);
	charData->setCurrentLevel(0);
	charData->setCollectedFragment(0);
	
	savePlayerCharData(charData);
	
	return charData;
}


PlayerCharData *PlayerRecord::getPlayerCharData(int charID)
{
	if(mPlayerCharDataMap.find(charID) == mPlayerCharDataMap.end()) {
		//
		return createNewPlayerCharData(charID);
	}
	
	
	return mPlayerCharDataMap.at(charID);
}

void PlayerRecord::savePlayerCharData(PlayerCharData *data)
{
	if(data == nullptr) {
		log("PlayerRecord.savePlayerCharData: data is null");
		return;
	}
	
	mPlayerCharDataMap.insert(data->getCharID(), data);
}
