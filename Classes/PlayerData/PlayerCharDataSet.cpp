//
//  PlayerCharDataSet.cpp
//  GhostLeg
//
//  Created by Ken Lee on 5/4/2017.
//
//

#include "PlayerCharDataSet.h"
#include "JSONHelper.h"

const int kDefaultCharID = 1;

PlayerCharDataSet::PlayerCharDataSet()
: BasePlayerData("playerCharData")
, mPlayerCharDataMap()
, mSelected(kDefaultCharID)
{
	
}


void PlayerCharDataSet::setupMockData()
{
	//PlayerCharData firstData
	PlayerCharData *data;
	
	// 1st Data
	data = PlayerCharData::create();
	data->setCharID(1);
	data->setCurrentLevel(1);
	data->setCollectedFragment(5);
	addPlayerCharData(data);
	
	// 2st Data
	data = PlayerCharData::create();
	data->setCharID(2);
	data->setCurrentLevel(0);
	data->setCollectedFragment(8);
	addPlayerCharData(data);
	
	
}

std::string PlayerCharDataSet::toString()
{
	std::string info = StringUtils::format("selectedChar=%d\n", mSelected);
	
	std::vector<int> keys = mPlayerCharDataMap.keys();
	for(int key : keys) {
		PlayerCharData *charData = mPlayerCharDataMap.at(key);
		
		info += charData->toString() + "\n";
	}
	
	return info;
}

void PlayerCharDataSet::reset()
{
	std::vector<int> keys = mPlayerCharDataMap.keys();
	for(int charID : keys) {
		PlayerCharData *charData = mPlayerCharDataMap.at(charID);
		charData->reset();
		
		if(charID == kDefaultCharID) {
			charData->setCurrentLevel(1);
		}
	}

	mSelected = kDefaultCharID;
	
	save();
}

int PlayerCharDataSet::alterFragmentCount(int charID, int change, bool autoSave)
{
	PlayerCharData *charData = getPlayerCharData(charID);
	if(charData == nullptr) {
		log("PlayerCharDataSet.alterFragmentCount: charData is null. charID=%d", charID);
		return 0;
	}
	
	charData->alterFragmentCount(change);
	
	if(autoSave) {
		save();
	}
	
	return charData->getCollectedFragment();
}


PlayerCharData *PlayerCharDataSet::createNewPlayerCharData(int charID)
{
	PlayerCharData *charData = PlayerCharData::create();
	charData->setCharID(charID);
	charData->setCurrentLevel(0);
	charData->setCollectedFragment(0);
	
	if(charID == kDefaultCharID) {
		charData->setCurrentLevel(1);
	}
	
	
	addPlayerCharData(charData);
	
	save();		//
	
	return charData;
}

void PlayerCharDataSet::unlockChar(int charID)
{
	PlayerCharData *charData = getPlayerCharData(charID);
	if(charData != nullptr && charData->getCurrentLevel() == 0) {
		charData->upgradeLevel();
	}
}

PlayerCharData *PlayerCharDataSet::getPlayerCharData(int charID)
{
	if(mPlayerCharDataMap.find(charID) == mPlayerCharDataMap.end()) {
		//
		return createNewPlayerCharData(charID);
	}
	
	
	return mPlayerCharDataMap.at(charID);
}




void PlayerCharDataSet::addPlayerCharData(PlayerCharData *data)
{
	if(data == nullptr) {
		log("PlayerCharDataSet: data is null");
		return;
	}
	
	mPlayerCharDataMap.insert(data->getCharID(), data);
}

#pragma mark - JSON Data
void PlayerCharDataSet::defineJSON(rapidjson::Document::AllocatorType &allocator,
								   rapidjson::Value &outValue)	// object value -> JSON
{
	
	// Selected
	addInt(allocator, "selectedChar", mSelected, outValue);
	
	
	// Character List
	Vector<PlayerCharData *> dataArray;
	
	std::vector<int> keys = mPlayerCharDataMap.keys();
	for(int key : keys) {
		PlayerCharData *charData = mPlayerCharDataMap.at(key);
		dataArray.pushBack(charData);
	}
	
	addObjectArray(allocator, "data", dataArray, outValue);
}

bool PlayerCharDataSet::parseJSON(const rapidjson::Value &jsonValue)		// JSON DAta -> this object value
{
	//
	// Selected
	mSelected = aurora::JSONHelper::getInt(jsonValue, "selectedChar", kDefaultCharID);
	
	
	//
	mPlayerCharDataMap.clear();
	
	const rapidjson::Value &jsonArray = jsonValue["data"];
	// log("DEBUG: jsonArray size: %d", jsonArray.Size());
	for(int i=0; i<jsonArray.Size(); i++) {
		PlayerCharData *charData = PlayerCharData::create();
		
		charData->parseJSON(jsonArray[i]);
		
		addPlayerCharData(charData);
	}
	
	
	return true;
}

#pragma mark - Base Player Data
std::string PlayerCharDataSet::generateDataContent()
{
	return toJSONContent();
}

void PlayerCharDataSet::parseDataContent(const std::string &content)
{
	parseJSONContent(content);
}

void PlayerCharDataSet::setDefault()
{
	// ??
}


#pragma mark - player selection
int PlayerCharDataSet::getSelectedChar()
{
	return mSelected;
}

void PlayerCharDataSet::selectChar(int charID)
{
	mSelected = charID;
	
	save();
}
