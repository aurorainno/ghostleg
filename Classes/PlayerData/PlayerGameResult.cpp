//
//  PlayerGameResult.cpp
//  BounceDefense
//
//  Created by Ken Lee on 19/9/2016.
//
//

#include "PlayerGameResult.h"


//CC_SYNTHESIZE(int, mTotalScore, TotalScore);
//CC_SYNTHESIZE(int, mDeliveryScore, DeliveryScore);
//CC_SYNTHESIZE(int, mKillScore, KillScore);
//CC_SYNTHESIZE(int, mClearStageScore, ClearStageScore);
//CC_SYNTHESIZE(int, mBonusScore, BonusScore);


PlayerGameResult::PlayerGameResult()
: mDogPlayed(0)
, mPlanetPlayed(0)
, mStarCollected(0)
, mDistance(0)
, mKill(0)
, mPlayCount(1)
, mTotalScore(0)
, mDeliveryScore(0)
, mKillScore(0)
, mClearStageScore(0)
, mBonusScore(0)
, mDistanceScore(0)
, mCollectScore(0)
, mUsedBooster(BoosterItemNone)
, mUsedBoosterCount(0)
{
	
}


bool PlayerGameResult::init()
{
	return true;
}

void PlayerGameResult::reset()
{
	mStarCollected = 0;
	mDistance = 0;
	mKill = 0;
	
	mTotalScore = 0;
	mDeliveryScore = 0;
	mKillScore = 0;
	mClearStageScore = 0;
	mBonusScore = 0;
	mDistanceScore = 0;
	mCollectScore = 0;
	
	mUsedBooster = BoosterItemNone;
	mUsedBoosterCount = 0;
}

std::string PlayerGameResult::toString()
{
	std::string result = "";

	result += StringUtils::format("dog=%d", mDogPlayed);
	result += StringUtils::format(" planet=%d", mPlanetPlayed);
	result += StringUtils::format(" star=%d", mStarCollected);
	result += StringUtils::format(" kill=%d", mKill);
	result += StringUtils::format(" playCount=%d", mPlayCount);
	result += StringUtils::format(" distance=%d", mDistance);
	
//	CC_SYNTHESIZE(int, mDeliveryScore, DeliveryScore);
//	CC_SYNTHESIZE(int, mKillScore, KillScore);
//	CC_SYNTHESIZE(int, mClearStageScore, ClearStageScore);
//	CC_SYNTHESIZE(int, mBonusScore, BonusScore);
	
	result += StringUtils::format(" totalScore=%d", mTotalScore);
	result += StringUtils::format(" deliveryScore=%d", mDeliveryScore);
	result += StringUtils::format(" killScore=%d", mKillScore);
	result += StringUtils::format(" clearStageScore=%d", mClearStageScore);
	result += StringUtils::format(" distanceScore=%d", mDistanceScore);
	result += StringUtils::format(" bonusScore=%d", mBonusScore);
	
	return result;
}


void PlayerGameResult::multiply(int value)		// this is for debugging
{
	mStarCollected *= value;
	mKill *= value;
	mDistance *=value;
	mPlayCount = value;
}

bool PlayerGameResult::hasClearStage()
{
	return mClearStageScore > 0;
}


void PlayerGameResult::addScore(int value, ScoreType type)
{
	mTotalScore += value;
	switch(type) {
		case ScoreTypeDelivery	: { mDeliveryScore += value; break; }
		case ScoreTypeKill		: { mKillScore += value; break; }
		case ScoreTypeAllClear	: { mClearStageScore += value; break; }
		case ScoreTypeBonus		: { mBonusScore += value; break; }
		case ScoreTypeDistance	: { mDistanceScore += value; break; }
		case ScoreTypeCollect	: { mCollectScore += value; break; }
	}
}


void PlayerGameResult::setBonusScore(float bonusPercent)	// note: call before goto th EndGame Screen
{
	if(bonusPercent <= 0) {
		mBonusScore = 0;
		return;
	}
	
	int bonusScore = (int)(mTotalScore * bonusPercent);
	
	addScore(bonusScore, ScoreTypeBonus);
}

void PlayerGameResult::addBoosterUse(BoosterItemType itemType)
{
	mUsedBooster = itemType;
	mUsedBoosterCount++;
}
