//
//  PlayerRecord.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/10/2016.
//
//
#ifndef PlayerRecord_hpp
#define PlayerRecord_hpp

#include <stdio.h>
#include "BasePlayerData.h"
#include "JSONData.h"
#include <map>
#include <string>
#include "PlayerCharData.h"

USING_NS_CC;

class PlayerGameResult;

class PlayerRecord : public BasePlayerData, public JSONData
{
public:
	enum RecordUpdateType{
		eUpdateTypeIncrement,	// accumlation
		eUpdateTypeBest,		// set if the value is a better one
		eUpdateTypeSet,			// just set the value
	};
	
public:
	static const char *KeyBestDistance;
	static const char *KeyPlayCount;
	static const char *KeyStarTotal;
	static const char *KeyStarInOneGame;
	static const char *KeyLastPlayTime;
    static const char *KeyPlayDayCount;
	static const char *KeyKillTotal;
	static const char *KeyKillInOneGame;
	static const char *KeyHasClear;
public:
	CREATE_FUNC(PlayerRecord);
	PlayerRecord();
	
	int getPlayCount();
	
	virtual std::string toString();

	void updatePlayerRecordWithResult(PlayerGameResult *result);
    
    void updatePlayTimeRecord();
	
	std::map<std::string, int> getDogRecordMap(int dogID);

#pragma mark - Static Helper Method
public:
	static float getKeyScoreWeight(const std::string &key);
	static std::string getUnlockMessageTemplate(const std::string &key);
	
	
#pragma mark - Helper Method
public:
	void clear();
	bool setDogRecord(const char *key, int dogID, int newValue);			// true if the record just break
	bool setPlanetRecord(const char *key, int planetID, int newValue);		// true if the record just break
	bool setPlayerRecord(const char *key, int newValue);					// true if the record just break
	
	int getDogRecord(const char *key, int dogID);
	int getPlanetRecord(const char *key, int planetID);
	
protected:
	bool updateRecordData(const char *prefix, const char *suffix, int newValue);
	bool getNewRecordValue(const std::string &recordKey, RecordUpdateType updateType, int newValue, int &outValue);	// return 'true' if any update
	
#pragma mark - Internal Data
public:
	void setRecordData(const std::string &recordKey, int value);
	int getRecordData(const std::string &recordKey);
    
    bool saveTimeRecord();
    bool loadTimeRecord();
    
    bool hasPlayed7Days();
	
protected:
	std::map<std::string, int> mRecordDataMap;

    bool mHasPlayed7Days;

	
#pragma mark - PlayerCharData
public:
	void setupMockPlayerCharData();
	std::string infoPlayerCharData();
	PlayerCharData *getPlayerCharData(int charID);
	
private:
	void savePlayerCharData(PlayerCharData *data);
	PlayerCharData *createNewPlayerCharData(int charID);
	
private:
	Map<int, PlayerCharData *> mPlayerCharDataMap;
	
	
	
	
#pragma mark - JSON Data 
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);	// object value -> JSON
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
public:	// abstract methods
	virtual std::string generateDataContent();					// Need implementation
	virtual void parseDataContent(const std::string &content);	// Need implementation
	virtual void setDefault();										// Need implementation
	
};

#endif
