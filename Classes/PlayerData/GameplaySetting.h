//
//  GameplaySetting.hpp
//  GhostLeg
//
//  Created by Ken Lee on 27/3/2017.
//
//

#ifndef GameplaySetting_hpp
#define GameplaySetting_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <map>
#include "CommonMacro.h"

USING_NS_CC;

class GameplaySetting : public Ref
{
public:		// static
	enum Attribute {
		BonusTipPercent,						// none
		BonusReward,
		
		BonusMaxSpeed,							// by percentage
		BonusTurnSpeed,
		BonusStunReduction,						// by second
		BonusAcceleration,
		
		DoubleStarPercent,
		ExtraTimeLimit,							// by second
		
		// Reward
		KillEnemyReward,
		BonusScore,								// Bonus Total Score by Percent
		TravelReward,							//
		
		// Power & Booster Related
		MissleSpeedReduction,					// percent to slow the missile
		CycloneSpeedReduction,					// percent to slow
		ExtraTimeStopDuration,					// second to add the default
		ExtraShieldDuration,					// second to add the default
		ExtraDoubleCoinDuration,
		
		// Power UP
		ExtraCashOutDuration,					// second to add the default
		ExtraMagnetDuration,					// second to add the default
		ExtraSpeedUpDuration,					// second to add the default
		ExtraSpeedUpEffect,						// Add on the speedup multipler
		
		
		// Defend Attribute
		BulletBlocker,
		LaserBlocker,
		BombBlocker,
		ChanceEnemyBlocker,
		ChaserSlowRatio,
		
		//
		MaxSpeed,
		TurnSpeed,
		Acceleration,
		StunReduction,
		
		// Starting Booster/SpeedUp
		StartBooster,
		StartPowerUp,
		
		//
		
		AttributeEnd,
	};
	
	
public:
	CREATE_FUNC(GameplaySetting);
	static std::string getAttributeName(const Attribute &attribute);
	
public:
	CC_SYNTHESIZE(float, mMaxSpeed, MaxSpeed);
	CC_SYNTHESIZE(float, mTurnSpeed, TurnSpeed);
	CC_SYNTHESIZE(float, mAcceleration, Acceleration);
	CC_SYNTHESIZE(float, mStunReduction, StunReduction);

	
	
public:
	void setBetterAttributeValue(Attribute attribute, float value);
	void setAttributeValue(Attribute attribute, float value);
	void addAttributeValue(Attribute attribute, float value);
	float getAttributeValue(Attribute attribute);
	
	float getMainAttribute(Attribute attribute);
	
#pragma mark - Extra Reward
	void setExtraReward(int rating, int extraReward);
	int getExtraReward(int rating);
	
	
#pragma mark - Core
public:
	GameplaySetting();
	virtual bool init();
	virtual std::string toString(bool skipUndefined=true);
	void reset();
	
	void add(GameplaySetting *setting);		// Add the value of the given setting, using in global + player
	std::vector<GameplaySetting::Attribute> getAttributeList();

	
private:
	std::map<Attribute, float> mAttributeMap;
	std::map<int, int> mExtraRewardMap;		// key = rating   value = reward
};
#endif /* GameplaySetting_hpp */
