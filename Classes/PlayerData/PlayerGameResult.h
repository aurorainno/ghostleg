//
//  PlayerGameResult.hpp
//  BounceDefense
//
//  Created by Ken Lee on 19/9/2016.
//
//

#ifndef PlayerGameResult_hpp
#define PlayerGameResult_hpp

#include <stdio.h>


#include <vector>
#include <string>
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class PlayerGameResult : public Ref
{
public:
	
public:
	CREATE_FUNC(PlayerGameResult);
	
	PlayerGameResult();
	virtual bool init();
	void reset();
	void multiply(int value);		// this is for debugging
	
	

#pragma mark - property
public:
	CC_SYNTHESIZE(int, mDogPlayed, DogPlayed);		// the DogID of the dog is being used
	CC_SYNTHESIZE(int, mPlanetPlayed, PlanetPlayed);		// the DogID of the dog is being used
	CC_SYNTHESIZE(int, mStarCollected, StarCollected);
	CC_SYNTHESIZE(int, mDistance, Distance);
	CC_SYNTHESIZE(int, mPlayCount, PlayCount);
	CC_SYNTHESIZE(int, mKill, Kill);
	
	
	CC_SYNTHESIZE(int, mTotalScore, TotalScore);
	CC_SYNTHESIZE(int, mDeliveryScore, DeliveryScore);
	CC_SYNTHESIZE(int, mKillScore, KillScore);
	CC_SYNTHESIZE(int, mDistanceScore, DistanceScore);
	CC_SYNTHESIZE(int, mCollectScore, CollectScore);
	CC_SYNTHESIZE(int, mClearStageScore, ClearStageScore);
	CC_SYNTHESIZE(int, mBonusScore, BonusScore);
	
	CC_SYNTHESIZE(BoosterItemType, mUsedBooster, UsedBooster);
	CC_SYNTHESIZE(int, mUsedBoosterCount, UsedBoosterCount);
	
	bool hasClearStage();
	
	void addScore(int value, ScoreType type);
	void setBonusScore(float bonusPercent);
	void addBoosterUse(BoosterItemType itemType);
	
#pragma mark - information
	std::string toString();
};

#endif /* PlayerGameResult_hpp */
