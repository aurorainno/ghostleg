//
//  PlayerCharData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#include "PlayerCharData.h"
#include "JSONHelper.h"

PlayerCharData::PlayerCharData()
: Ref()
, mCharID(0)
, mCurrentLevel(0)
, mCollectedFragment(0)
, mIsNewChar(0)
{
	
}

std::string PlayerCharData::toString()
{
	return StringUtils::format("charID=%d currentLevel=%d collectedFrag=%d",
							   mCharID, mCurrentLevel, mCollectedFragment);
}

#pragma mark - JSON Data
void PlayerCharData::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
	addInt(allocator, "charID", mCharID, outValue);
	addInt(allocator, "currentLevel", mCurrentLevel, outValue);
	addInt(allocator, "collectedFrag", mCollectedFragment, outValue);
	
}

bool PlayerCharData::parseJSON(const rapidjson::Value &jsonValue)		// JSON DAta -> this object value
{
	mCharID = aurora::JSONHelper::getInt(jsonValue, "charID", 0);
	mCurrentLevel = aurora::JSONHelper::getInt(jsonValue, "currentLevel", 0);
	mCollectedFragment = aurora::JSONHelper::getInt(jsonValue, "collectedFrag", 0);
	
	return true;
}

void PlayerCharData::alterFragmentCount(int change)
{
	int newValue = mCollectedFragment + change;
	if(newValue < 0) {
		newValue = 0;
	}
	
	mCollectedFragment = newValue;
}

void PlayerCharData::reset()
{
	mCurrentLevel = 0;
	mCollectedFragment = 0;
}

void PlayerCharData::resetLevel()
{
	mCurrentLevel = 0;

}

void PlayerCharData::upgradeLevel()
{
	mCurrentLevel++;
}
