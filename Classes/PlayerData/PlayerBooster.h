//
//  PlayerBooster.hpp
//  GhostLeg
//
//  Created by Ken Lee on 19/1/2017.
//
//

#ifndef PlayerBooster_hpp
#define PlayerBooster_hpp
#include "JSONData.h"
#include "cocos2d.h"
#include "CommonType.h"
#include <stdio.h>
#include <map>

USING_NS_CC;

class PlayerBooster : public cocos2d::Ref, public JSONData
{
public:
	CREATE_FUNC(PlayerBooster);
	
	PlayerBooster();
	
public:
	virtual bool init();

	void setBoosterCount(BoosterItemType type, int value);
	int getBoosterCount(BoosterItemType type);
	void addBooster(BoosterItemType type, int addValue = 1);
	void deduceBooster(BoosterItemType type, int deduceValue = 1);
	void changeBooster(BoosterItemType type, int changeValue);

	std::string toString();
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);	// object value -> JSON
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
//public:	// abstract methods
//	virtual std::string generateDataContent();					// Need implementation
//	virtual void parseDataContent(const std::string &content);	// Need implementation
	virtual void setDefault();										// Need implementation
	
private:
	std::map<int, int> mBoosterCount;
	
};

#endif /* PlayerBooster_hpp */
