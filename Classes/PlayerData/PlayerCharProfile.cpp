//
//  PlayerCharProfile.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#include "PlayerCharProfile.h"
#include "Constant.h"
#include "GameManager.h"
#include "PlayerManager.h"
#include "PlayerCharData.h"
#include "CharGameData.h"
#include "CharUpgradeCost.h"
#include "PlayerCharDataSet.h"
#include "GameplaySetting.h"
#include "PlayerAbility.h"
#include "Analytics.h"

PlayerCharProfile::PlayerCharProfile()
: mCharID(0)
, mPlayerCharData(nullptr)		// note: the reference is holding by GameManager::playerRecord
, mCharGameData(nullptr)		// note: the reference is holding by PlayerManager
, mMaxSpeed(100)
, mTurnSpeed(100)
, mAcceleration(100)
, mStunReduction(0)
{

	
}



void PlayerCharProfile::setup(int charID)
{
	setCharID(charID);
	mPlayerCharData = PlayerManager::instance()->getPlayerCharData(getCharID());
	mCharGameData = PlayerManager::instance()->getCharGameData(getCharID());
	
	update();
}

void PlayerCharProfile::updateWithSetting(GameplaySetting *setting)
{
	
	mMaxSpeed = setting->getMainAttribute(GameplaySetting::MaxSpeed);
	mAcceleration = setting->getMainAttribute(GameplaySetting::Acceleration);
	mTurnSpeed = setting->getMainAttribute(GameplaySetting::TurnSpeed);
	mStunReduction = setting->getMainAttribute(GameplaySetting::StunReduction);
	
}

void PlayerCharProfile::update()
{
	GameplaySetting *setting = PlayerManager::instance()->getCharacterGameplaySetting(this);
	
	// log("DEBUG: charID=%d setting=%s", getCharID(), setting->toString().c_str());
	updateWithSetting(setting);
	
	// updateAbilityBuff();
}

// deprecated
void PlayerCharProfile::updateAbilityBuff()
{
	GameplaySetting *setting = GameplaySetting::create();
	
//	log("DEBUG: abilityList=%s", mCharGameData->infoAbilityList().c_str());
	
	for(int level=1; level<=getCurrentLevel(); level++) {
		PlayerAbility *ability = mCharGameData->getAbilityAtLevel(level);
		if(ability == nullptr) {
//			log("DEBUG: Ability is null. level=%d", level);
			continue;
		}
		ability->updateGameplaySetting(setting);
	}
//	log("before: maxSpeed        : %d", mMaxSpeed);
//	log("before: mStunReduction  : %f", mStunReduction);
//	log("bonusSpeed: %f", setting->getAttributeValue(GameplaySetting::BonusMaxSpeed));
//	log("stunReduction: %f", setting->getAttributeValue(GameplaySetting::BonusStunReduction));
	
	mMaxSpeed += (int)((float) mMaxSpeed * setting->getAttributeValue(GameplaySetting::BonusMaxSpeed));
	mStunReduction += setting->getAttributeValue(GameplaySetting::BonusStunReduction);
	
//	log("after: maxSpeed        : %d", mMaxSpeed);
//	log("after: mStunReduction  : %f", mStunReduction);

}



std::string PlayerCharProfile::toString()
{
	std::string info = "";
	
	info += StringUtils::format("charID=%d", getCharID());
	info += StringUtils::format(" name=%s", getName().c_str());
	info += StringUtils::format(" maxSpeed=%d", getMaxSpeed());
	info += StringUtils::format(" acceleration=%f", getAcceleration());
	info += StringUtils::format(" turnSpeed=%d", getTurnSpeed());
	info += StringUtils::format(" stunReduction=%f", getStunReduction());
	info += StringUtils::format(" level=%d", getCurrentLevel());
	info += StringUtils::format(" isLocked=%d", isLocked());
	info += StringUtils::format(" isMax=%d", isMaxLevel());
	info += StringUtils::format(" upgradeFragCount=%d", getUpgradeFragCount());
	info += StringUtils::format(" upgradeMoney=%d", getUpgradeMoney());
	info += StringUtils::format(" collectedFragCount=%d", getCollectedFragCount());
	info += StringUtils::format(" upgradeDesc=%s", getUpgradeDescription().c_str());
	info += StringUtils::format(" unlockDesc=%s", getUnlockDescription().c_str());
	
	return info;
}


std::string PlayerCharProfile::getName()
{
	return mCharGameData == nullptr ? "error" : mCharGameData->getName();
}


// Attribute
int PlayerCharProfile::getMaxSpeed()
{
	return mMaxSpeed;
}

float PlayerCharProfile::getAcceleration()
{
	return mAcceleration;
}

int PlayerCharProfile::getTurnSpeed()
{
	return mTurnSpeed;
}

float PlayerCharProfile::getStunReduction()		//
{
	return mStunReduction;
}

// Level Related
int PlayerCharProfile::getCurrentLevel()
{
	return mPlayerCharData->getCurrentLevel();
}

bool PlayerCharProfile::isLocked()
{
	return getCurrentLevel() == 0;
}

bool PlayerCharProfile::isMaxLevel()
{
	return getCurrentLevel() == kMaxCharLevel;
}


//is new characer
bool PlayerCharProfile::isNewChar()
{
    return mPlayerCharData->getIsNewChar();
}

void PlayerCharProfile::setNewChar(bool isNewChar)
{
    mPlayerCharData->setIsNewChar(isNewChar);
}

// Upgrade
int PlayerCharProfile::getUpgradeFragCount()
{
	if(isMaxLevel()) {
		return 0;
	}
	
	CharUpgradeCost cost = mCharGameData->getUpgradeCostAtLevel(getCurrentLevel()+1);
	return cost.requireFragCount;
	
}

int PlayerCharProfile::getUpgradeMoney()
{
	if(isMaxLevel()) {
		return 0;
	}
	
	CharUpgradeCost cost = mCharGameData->getUpgradeCostAtLevel(getCurrentLevel()+1);
	return cost.requireMoney;
}

int PlayerCharProfile::getCollectedFragCount()
{
	if(mPlayerCharData == nullptr) {
		return 0;
	}
	
	return mPlayerCharData->getCollectedFragment();
}

// Description
std::string PlayerCharProfile::getUpgradeDescription()
{
	return "TODO: Upgrade Description";
}

std::string PlayerCharProfile::getUnlockDescription()
{
	return mCharGameData->getUnlockInfo();
}

int PlayerCharProfile::alterFragmentCount(int change)
{
	if(mPlayerCharData != nullptr) {
		mPlayerCharData->alterFragmentCount(change);
		return getCollectedFragCount();
	}
		
	return -1;
}

void PlayerCharProfile::upgradeToMax()		// just for debugging purpos
{
	// upgrade the Level
	int numUpgrade = 5 - getCurrentLevel();
	for(int i=0; i<numUpgrade; i++) {
		mPlayerCharData->upgradeLevel();
	}
	
	// Save Player Record
	PlayerManager::instance()->savePlayerCharData();
	
	// Update the internal data
	update();
}


void PlayerCharProfile::doUpgradeLogic()		// just for debugging purpos
{
	// upgrade the Level
	mPlayerCharData->upgradeLevel();
	
	// Save Player Record
	PlayerManager::instance()->savePlayerCharData();
	
	// Update the internal data
	update();
}

PlayerCharProfile::UpgradeStatus PlayerCharProfile::upgrade()
{
	
	// Checking for Input
	if(isMaxLevel()) {
		return UpgradeAlreadyMax;
	}
	
	int requireFragment = getUpgradeFragCount();
	int collectFrag = getCollectedFragCount();
	
	if(collectFrag < requireFragment) {
		return UpgradeNotEnoughFragment;
	}
	
	int requireMoney = getUpgradeMoney();
	int myMoney = GameManager::instance()->getMoneyValue(MoneyType::MoneyTypeStar);
	if(myMoney < requireMoney) {
		return UpgradeNotEnoughMoney;
	}
	
	
	// Deduct the money
	if(requireMoney > 0) {
		GameManager::instance()->addMoney(MoneyType::MoneyTypeStar, -requireMoney);
	}
	
	// Deduct the fragment
	mPlayerCharData->alterFragmentCount(-requireFragment);
	
    //set new char to true
	bool isUnlockChar = getCurrentLevel() == 0;
    if(isUnlockChar){
        setNewChar(true);
    }
    
	// upgrade the Level
	doUpgradeLogic();
	
	// Logging
	Analytics::Action logAction = isUnlockChar ? Analytics::act_unlock_char : Analytics::act_upgrade_char;
	Analytics::instance()->logConsume(logAction, getName(), requireMoney, MoneyTypeStar);
	Analytics::instance()->logConsume(logAction, getName(), requireFragment, MoneyTypePuzzle);
	
	//Analytics::instance()->logConsume(Analytics::, <#const std::string &productName#>, <#int value#>)

	
	return UpgradeOk;
}


std::string PlayerCharProfile::infoProfile()		// for Testing Character Selection
{
	std::string info = "";
	
	info += StringUtils::format("speed=%d", getMaxSpeed());
	info += StringUtils::format(" tSpeed=%d", getTurnSpeed());
	info += StringUtils::format(" acce=%5.2f\n", getAcceleration());
	info += StringUtils::format(" stun=%5.2f\n", getStunReduction());
	
	info += StringUtils::format("level=%d", getCurrentLevel());
	info += StringUtils::format(" isLocked=%d", isLocked());
	info += StringUtils::format(" isMax=%d\n", isMaxLevel());
	
	info += StringUtils::format("frag=%d", getCollectedFragCount());
	info += StringUtils::format(" upgrade=[%d, %d]\n", getUpgradeFragCount(), getUpgradeMoney());
	
	info += StringUtils::format("desc1=%s\n", getUpgradeDescription().c_str());
	info += StringUtils::format("desc2=%s\n", getUnlockDescription().c_str());
	
	for(int level=1; level<=kMaxCharLevel; level++) {
		CharUpgradeCost cost = mCharGameData->getUpgradeCostAtLevel(level);
		info += StringUtils::format("Upgrade->Lv%d: ", level);
		info += cost.toString();
		info += "\n";
	}
	
	// TODO: Ability 
	
	return info;
}


void PlayerCharProfile::updateGlobalGameplaySetting(GameplaySetting *playSetting)
{
	if(mCharGameData == nullptr) {
		log("ERROR: PlayerCharProfile: mCharGameData is null");
		return;
	}
	
	Vector<PlayerAbility *> abilityList = getActiveAbilityList();
	
	for(PlayerAbility *ability : abilityList) {
		ability->updateSetting(playSetting, true);
	}
}

void PlayerCharProfile::updateGameplaySetting(GameplaySetting *playSetting)
{
	if(mCharGameData == nullptr) {
		log("ERROR: PlayerCharProfile: mCharGameData is null");
		return;
	}
	
	Vector<PlayerAbility *> abilityList = getActiveAbilityList();
	
	for(PlayerAbility *ability : abilityList) {
		ability->updateSetting(playSetting);
	}
	
}


Vector<PlayerAbility *> PlayerCharProfile::getActiveAbilityList()
{
	int myLevel = getCurrentLevel();
	Vector<PlayerAbility *> result;
	if(myLevel == 0) {
		return result;
	}
	
	
	for(int level=1; level<=myLevel; level++) {
		PlayerAbility *ability = mCharGameData->getAbilityAtLevel(level);
		if(ability == nullptr) {
			log("playerCharProfile: error. ability is null. level=%d char=%d", level, getCharID());
			continue;
		}
		
		result.pushBack(ability);
	}
	
	return result;
}


PlayerAbility *PlayerCharProfile::getAbilityByLevel(int level)
{
	return mCharGameData->getAbilityAtLevel(level);
}


std::string PlayerCharProfile::infoAbilityList()
{
	std::string result = "";
	for(int level=1; level<=5; level++) {
		PlayerAbility *ability = getAbilityByLevel(level);
		if(ability == nullptr) {
			continue;
		}
		
		result += StringUtils::format("lv%d: %s [%s]\n",
									 level,
									 ability->getDesc().c_str(),
									 ability->toString().c_str());
 
	}
	
	
	
	return result;
}
