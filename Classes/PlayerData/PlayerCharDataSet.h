//
//  PlayerCharDataSet.hpp
//  GhostLeg
//
//  Created by Ken Lee on 5/4/2017.
//
//

#ifndef PlayerCharDataSet_hpp
#define PlayerCharDataSet_hpp

#include <stdio.h>

#include <stdio.h>
#include "BasePlayerData.h"
#include "JSONData.h"
#include <map>
#include <string>
#include "PlayerCharData.h"

USING_NS_CC;

class PlayerCharDataSet : public BasePlayerData, public JSONData
{
public:
	CREATE_FUNC(PlayerCharDataSet);
	
public:
	PlayerCharDataSet();
	bool init() { return true; }
	
public:
	void setupMockData();
	virtual std::string toString();
	PlayerCharData *getPlayerCharData(int charID);
	
	// void upgradeLevel();
	int alterFragmentCount(int charID, int change, bool autoSave=true);
	void reset();
	void unlockChar(int charID);
	
	std::string infoPlayerCharDataMap();
	
private:
	Map<int, PlayerCharData *> mPlayerCharDataMap;		// key: charID

private:
	PlayerCharData *createNewPlayerCharData(int charID);
	void addPlayerCharData(PlayerCharData *data);
	
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);	// object value -> JSON
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
#pragma mark - Base Player Data
public:	// abstract methods
	virtual std::string generateDataContent();
	virtual void parseDataContent(const std::string &content);
	virtual void setDefault();

#pragma mark - player selection & character list
public:
	int getSelectedChar();
	void selectChar(int charID);
	
private:
	int mSelected;
	
};

#endif /* PlayerCharDataSet_hpp */
