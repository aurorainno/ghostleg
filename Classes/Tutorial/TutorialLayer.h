//
//  TutorialLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 4/7/2016.
//
//

#ifndef TutorialLayer_hpp
#define TutorialLayer_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC_EXT;
USING_NS_CC;
using namespace cocos2d::ui;

class TutorialNpcDialog;

#include "TutorialData.h"


class TutorialLayer : public LayerColor
{
public:
	typedef std::function<void(Ref *)> CloseCallback;
public:
	CREATE_FUNC(TutorialLayer);
	
	TutorialLayer();
	~TutorialLayer();
	
	CC_SYNTHESIZE(bool, mIsInGameTutorial, IsInGameTutorial);
	CC_SYNTHESIZE(bool, mAutoCleanup, AutoCleanup);
	
	virtual bool init();

	void reset();

	void update(float delta);	// return true if tutorial reach last step
    
    void gaLogTutorialStep(int step);

	bool isDone();
	
	bool isWaitForPlayer();		// Tutorial is start and wait for player instruction
    
    int getCurrentStep();
    TutorialStep *getTutorialStep();
	
    Node *addLoopAnimation(const std::string &csbname, const Vec2 &position, float scale = 1, float rotation = 0);
	Node *addOneTimeAnimation(const std::string &csbname, const Vec2 &position, float scale = 1,bool destroySelf = true,const std::function<void()> &endCallback = NULL, float rotation = 0);
	
	void setTutorialDataList(const Vector<TutorialData *> &dataList);
	
	void setCloseCallback(const CloseCallback &callback);

	
	//
	void setCoachmarkTutorial(const std::string &tutorialName,
							  const std::string &csb,
                              const Vec2 &csbPos,
							  const Vec2 &touchPos,
                              Vec2 touchRect = Vec2::ZERO, float csbRotation = 0, bool showNPC = false, std::string npcMsg = "", bool autoCleanUp = true);

	
private:
	void setNextStep();
	
	TutorialData *getTutorialData(int step);
	bool isLastStep(int step);
	
	void cleanupAnimeNode();
	
	void addTouchListener();
	// Override of touches
	virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);
 
	
	void sendCloseCallback();

	
	
private:
	Node* mCurrentAnimationNode;
	int mCurrentStepValue;
    float mTutorialTimePassed;
	TutorialStep *mStep;
	bool mIsDone;
    bool mIsDrawingSlopeLine;
	std::string mTutorialName;	// for GA Track
	
	CloseCallback mCloseCallback;
	
	Vector<TutorialData *> mTutorialDataList;
	
	
#pragma mark - Dialog Show / Close
public:
	void setDialogVisible(bool flag);
	void showDialog(const std::string &msg);
	void closeDialog();

private:
	void setupNpcDialog();
	
private:
	TutorialNpcDialog *mNpcDialog;
	bool mHasInput;
};


#endif /* TutorialLayer_hpp */
