//
//  TutorialLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 4/7/2016.
//
//

#include "TutorialLayer.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "TutorialManager.h"
#include "ViewHelper.h"

#include "Analytics.h"
#include "StringHelper.h"
#include "GameWorld.h"
#include "GameTouchLayer.h"
#include "TutorialNpcDialog.h"

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

TutorialLayer::TutorialLayer()
: mIsDone(false)
, mCurrentAnimationNode(nullptr)
, mIsInGameTutorial(true)
, mTutorialName("gameTutorial")
, mAutoCleanup(false)
, mCloseCallback(nullptr)
, mNpcDialog(nullptr)
, mCurrentStepValue(0)
{
	mStep = new TutorialStep();
	mStep->setUILayer(this);
}

TutorialLayer::~TutorialLayer()
{
	CC_SAFE_RELEASE(mStep);
}


// on "init" you need to initialize your instance
bool TutorialLayer::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !LayerColor::init() )
	{
		return false;
	}
	
	// For debug
	//setColor(Color3B::BLUE);
	//setOpacity(100);
	
	addTouchListener();
	
	mTutorialTimePassed = 0;
	
	setupNpcDialog();
	
	return true;
}

void TutorialLayer::reset()
{
	if(mIsInGameTutorial) {
		setTutorialDataList(TutorialManager::instance()->getTutorialDataList());
	}
	
	mCurrentStepValue = 0;
    mTutorialTimePassed = 0;
	
	gaLogTutorialStep(0);
	
	
	TutorialData *data = getTutorialData(mCurrentStepValue);
	
	if(data != nullptr) {
		log("debug: %s", data->toString().c_str());
	}
	mStep->setData(data);
	mStep->reset();
}

void TutorialLayer::update(float delta)
{
	if(mIsDone) {
		return;
	}
    
    
    mTutorialTimePassed += delta;
	
	
	mStep->update(delta);
	
	
	if(mStep->isDone() == false) {
		return;
	}
	
	
	// Current Step is done
	if(isLastStep(mCurrentStepValue))
    {
		cleanupAnimeNode();
		
		log("TutorialLayer: all tutorial completed");
		mIsDone = true;
		gaLogTutorialStep(-1);
		
        sendCloseCallback();
		if(mAutoCleanup) {
            removeFromParent();
		}
		
		return;
	}
	
    
	setNextStep();

	return;
}

bool TutorialLayer::isDone()
{
	return mIsDone;
}

void TutorialLayer::addTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(TutorialLayer::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(TutorialLayer::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(TutorialLayer::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(TutorialLayer::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}

bool TutorialLayer::onTouchBegan(cocos2d::Touch* touch, cocos2d::Event*)
{
	log("onTouchBegan: receive touch:%d", mCurrentStepValue);
    if(mStep->isDropLine())
    {
        mIsDrawingSlopeLine = mStep->canSlopeLineStart(touch->getLocation());
    }
	
	mHasInput = false;
	
	return true;
}



void TutorialLayer::onTouchMoved(cocos2d::Touch* touch, cocos2d::Event*)
{
    if(mStep->isDropLine() && mIsDrawingSlopeLine)
    {
        GameWorld::instance()->getGameTouchLayer()->showDrawingSlopeLine(
				mStep->getSlopeLineMidPoint(),touch->getLocation());
    }

}

void TutorialLayer::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event*)
{
	log("onTouchEnded: receive touch end: %d", mCurrentStepValue);
	
	log("onTouchEnded: mHasInput=%d", mHasInput);
	if(! mHasInput) {
		mStep->handleInput(touch->getLocation());
		mHasInput = true;
	}
	
	
	mIsDrawingSlopeLine = false;
	
	update(0);
}



void TutorialLayer::onTouchCancelled(cocos2d::Touch*, cocos2d::Event*)
{
    mIsDrawingSlopeLine = false;
    GameWorld::instance()->getGameTouchLayer()->hideTouchIndicator();
}

void TutorialLayer::setNextStep()
{
	mCurrentStepValue++;
	log("TutorialLayer.setNextStep: starting step %d", mCurrentStepValue);
	
	//TutorialData *data = TutorialManager::instance()->getTutorialData(mCurrentStepValue);
	TutorialData *data = getTutorialData(mCurrentStepValue);
	if(data == nullptr) {
		log("TutorialLayer.setNextStep: data is null for step %d", mCurrentStepValue);
		return;
	}
	
	
	cleanupAnimeNode();
	mStep->setData(data);
	mStep->reset();
    
//    log("mCurrentStepValue:%d ,timeUsed:%d",mCurrentStepValue, (int)mTutorialTimePassed);
    gaLogTutorialStep(mCurrentStepValue);
    
}





bool TutorialLayer::isWaitForPlayer()
{
	return mStep->isWaitForPlayer();
}


Node *TutorialLayer::addLoopAnimation(const std::string &csbname,
									  const Vec2 &position, float scale, float rotation)
{
	log("TutorialLayer: addLoopAnimation: %s", csbname.c_str());
	Node *node = ViewHelper::createAnimationNode(csbname, "active", true, false);
	node->setScale(scale);
	node->setPosition(position);
    node->setRotation(rotation);
	addChild(node);
    mCurrentAnimationNode = node;
	return node;
}

Node * TutorialLayer::addOneTimeAnimation(const std::string &csbname, const Vec2 &position, float scale, bool destroySelf,const std::function<void()> &endCallback,float rotation)
{
	Node *node = ViewHelper::createAnimationNode(csbname, "active", false, destroySelf,endCallback);
    node->setScale(scale);
	node->setPosition(position);
    node->setRotation(rotation);
	addChild(node);
    mCurrentAnimationNode = node;
	return node;
}


int TutorialLayer::getCurrentStep()
{
    return mCurrentStepValue;
}

TutorialStep* TutorialLayer::getTutorialStep(){
    return mStep;
}


void TutorialLayer::gaLogTutorialStep(int step)
{
	Analytics::instance()->logTutorial(mTutorialName, step, mTutorialTimePassed);
}

void TutorialLayer::setTutorialDataList(const Vector<TutorialData *> &dataList)
{
	mTutorialDataList.clear();
	for(TutorialData *data : dataList) {
		mTutorialDataList.pushBack(data);
	}
}

TutorialData *TutorialLayer::getTutorialData(int step)
{
	if(step < 0) {
		step = (int) mTutorialDataList.size() + step;
	}
	
	if(step >= mTutorialDataList.size()) {
		return nullptr;
	}
	
	return mTutorialDataList.at(step);
}

bool TutorialLayer::isLastStep(int step)
{
	return step == mTutorialDataList.size()-1;
}

void TutorialLayer::cleanupAnimeNode()
{
	if(mCurrentAnimationNode){
		mCurrentAnimationNode->removeFromParent();
		mCurrentAnimationNode = nullptr;
	}
}

void TutorialLayer::setCloseCallback(const CloseCallback &callback)
{
	mCloseCallback = callback;
}

void TutorialLayer::sendCloseCallback()
{
	if(mCloseCallback) {
		mCloseCallback(this);
	}
}

void TutorialLayer::setCoachmarkTutorial(const std::string &tutorialName,
										 const std::string &csb, const Vec2 &csbPos, const Vec2 &touchPos,
                                         Vec2 touchRect, float csbRotation, bool showNpc, std::string npcMsg, bool autoCleanUp)
{
	mTutorialDataList.clear();
	
	
	// Define the tutorial Data
	TutorialData *tutorialData = TutorialData::create();
	tutorialData->setStep(0);
	tutorialData->setStartCondition(TutorialData::StartCondition::AutoStart);
	tutorialData->setStartConditionArg(0);
	tutorialData->setDisplayType(TutorialData::DisplayType::DisplayTypeTip);
	tutorialData->setIsLoopAnimation(true);
    tutorialData->setShowNpcDialog(showNpc);
    tutorialData->setDialogMessage(npcMsg);
	
	tutorialData->setEndCondition(TutorialData::EndCondition::TapHitArea);
	tutorialData->setDisplayCsb(csb);
    tutorialData->setDisplayPosition(csbPos);
    tutorialData->setCsbRotation(csbRotation);
	tutorialData->setDisplayPosition(touchPos);
	
	
	tutorialData->setHitAreaByRadius(touchPos, touchRect);
	
	
	mTutorialDataList.pushBack(tutorialData);
	
	mTutorialName = tutorialName;
	
	setAutoCleanup(autoCleanUp);
	setIsInGameTutorial(false);
}


#pragma mark - Dialog Show / Close
void TutorialLayer::showDialog(const std::string &msg)
{
	if(mNpcDialog != nullptr) {
		setDialogVisible(true);
		mNpcDialog->showDialog(msg);
	}
}

void TutorialLayer::closeDialog()
{
	if(mNpcDialog != nullptr) {
		if(mNpcDialog->isVisible() == false) {
			return;
		}
		mNpcDialog->closeDialog();
	}
}

void TutorialLayer::setupNpcDialog()
{
	mNpcDialog = TutorialNpcDialog::create();
	addChild(mNpcDialog);
    mNpcDialog->setVisible(false);
}

void TutorialLayer::setDialogVisible(bool flag)
{
	if(mNpcDialog) {
		mNpcDialog->setVisible(flag);
	}
}
