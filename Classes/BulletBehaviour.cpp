//
//  BulletBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 6/3/2017.
//
//

#include "BulletBehaviour.h"
#include "GameWorld.h"
#include "Enemy.h"
#include "Player.h"
#include "GeometryHelper.h"
#include "StringHelper.h"
#include "ModelLayer.h"
#include "GameSound.h"

BulletBehaviour::BulletBehaviour()
: EnemyBehaviour()
, mIsExploded(false)
, mMagnitude(0)
{
}

void BulletBehaviour::onCreate()
{
    updateProperty();
    getEnemy()->setAnimationEndCallFunc(GameModel::Destroy, [=](){
        getEnemy()->removeFromParent();
    });
    getEnemy()->changeToIdle();
}

void BulletBehaviour::onVisible()
{
//    if(!getEnemy()->getSoundPlayed()) {
//        GameSound::playEnemyAppearSound(getEnemy()->getResID());
//    }
    
}


void BulletBehaviour::onActive()
{
    //    float posX = getEnemy()->getPositionX();
    //
    //    log("onActive! posX: %.0f",posX);
    //    GameWorld::instance()->getGameUILayer()->showCaution(posX);
}


void BulletBehaviour::setNewSpeed(Vec2 oriPos, Vec2 targetPos)
{
    float radian = aurora::GeometryHelper::findAngleRadian(oriPos,targetPos);
    Vec2 newSpeed = aurora::GeometryHelper::resolveVec2(mMagnitude , radian);
    EnemyBehaviour::setSpeed(newSpeed);
//    getEnemy()->setSpeedX(newSpeed.x);
//    getEnemy()->setSpeedY(newSpeed.y);
}


void BulletBehaviour::onUpdate(float delta)
{
    // log("onUpdate!");
    if(mIsExploded){
        return;
    }
    
    Enemy *enemy = getEnemy();
    if(enemy == nullptr) {
        return;
    }
    
    if(enemy->getState() == Enemy::State::Active) {
        enemy->moveFree(delta);
//        if(mAcceleration.x>0 || mAcceleration.y>0){
//            float newSpeedX = enemy->getSpeedX() + mAcceleration.x * delta;
//            float newSpeedY = enemy->getSpeedY() + mAcceleration.y * delta;
//            
//            enemy->setSpeedX(newSpeedX);
//            enemy->setSpeedY(newSpeedY);
//        }
        // enemy->move(delta);
        //		Vec2 speed = enemy->getFinalSpeed();
        //		Vec2 newPos = calcNewPosition(getPosition(), speed.x, speed.y, delta);
        //
        //		enemy->setPosition(newPos);
        
        
    }
}

bool BulletBehaviour::onCollidePlayer(Player *player)
{
    
    Enemy *enemy = getEnemy();	// Error handling
    if(enemy == nullptr) {
        return false;
    }
    
    enemy->setState(Enemy::State::Destroying);
    enemy->setAction(GameModel::Destroy);
    enemy->setAnimationEndCallFunc(GameModel::Destroy, [=](){
        GameWorld::instance()->getModelLayer()->removeEnemy(enemy);
    });
    mIsExploded = true;
    
    return player->handleBeingHitted();
}

void BulletBehaviour::updateProperty()
{
    if(mData == nullptr) {
        log("BulletBehaviour::updateProperty :data is null");
        return;
    }
    
    std::map<std::string, std::string> property = mData->getMapProperty("bulletSetting");
    
    if(property.find("magnitude") == property.end()) {	// not found
        mMagnitude = 0;
    } else {
        mMagnitude = aurora::StringHelper::parseFloat(property["magnitude"]);
    }
    
    mAcceleration = getAcceler();
    
}

std::string BulletBehaviour::toString()
{
    return "BulletBehaviour";
}
