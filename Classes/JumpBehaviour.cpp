//
//  JumpBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 3/3/2017.
//
//

#include "JumpBehaviour.h"
#include "StringHelper.h"
#include "Enemy.h"

#define kFrameRate 60.0

JumpBehaviour::JumpBehaviour()
: EnemyBehaviour()
, mCooldown(0.5)
, mAccumTime(0)
, mStartCountDown(false)
, mIsJumping(false)
, mJumpFrame(35)
, mDistanceTravelled(0)
{}


void JumpBehaviour::onCreate()
{
    updateProperty();
    getEnemy()->setAnimationEndCallFunc(GameModel::Jump, CC_CALLBACK_0(JumpBehaviour::onJump, this));
    
    getEnemy()->setOnFrameEventCallback([=](const std::string &eventName, EventFrame* event){
        // log("enterFrameEvent, eventName=%s",eventName.c_str());
        if(eventName == "JumpStart"){
 //           log("posX:%.2f",getEnemy()->getPositionX());
            mIsJumping = true;
        }else if(eventName == "JumpEnd"){
//            mIsJumping = false;
        }
    });

    getEnemy()->changeToIdle();
   
}

void JumpBehaviour::onVisible()
{
    
}

void JumpBehaviour::onActive()
{
    startJump();
}

void JumpBehaviour::onUpdate(float delta)
{
    if(getEnemy()->getEffectStatus() == EffectStatusTimestop||
       getEnemy()->getState() !=  Enemy::State::Active){
        return;
    }
    
    if(mStartCountDown){
        mAccumTime += delta;
        if(mAccumTime > mCooldown){
            mAccumTime = 0;
            startJump();
        }
    }
    
    if(mIsJumping&&mDistanceTravelled<mDistance){
//        float displacement = mIsLeft ? -mDisplacementPerFrame : mDisplacementPerFrame;
//        getEnemy()->setPositionX(getEnemy()->getPositionX() + displacement);
//        float fps = Director::getInstance()->getFrameRate();
//        getEnemy()->setTimeSpeedByPercent(fps/kFrameRate);
        float displacement = getEnemy()->getSpeedX()*delta;
        float distanceTravelled = mDistanceTravelled + displacement;
        if(distanceTravelled > mDistance){
            displacement -= (distanceTravelled - mDistance);
        }
        if(displacement<0){
            displacement = 0;
        }
        displacement = mIsLeft ? -displacement : displacement;
        getEnemy()->setPositionX(getEnemy()->getPositionX() + displacement);
        mDistanceTravelled += fabsf(displacement);
    }
}

void JumpBehaviour::startJump()
{
//    if(mShouldFlipX){
//        getEnemy()->setFace(mCurrentFace);
//        mCurrentFace = !mCurrentFace;
//    }
    getEnemy()->setAction(GameModel::Jump);
 //   log("animation start/end frame after jump=%.2f,%.2f",getEnemy()->getStartFrame(),getEnemy()->getEndFrame());
    mDisplacementPerFrame =  fabsf(mDistance) /  mJumpFrame;
    if(mDisplacementPerFrame>0){
        getEnemy()->setFace(mIsLeft);
    }
    mStartCountDown = false;
    
    float frameDur = 1.0/kFrameRate;
    float speed = mDistance / (frameDur * mJumpFrame);
    getEnemy()->setSpeedX(speed);
}

void JumpBehaviour::onJump()
{
    getEnemy()->setAction(GameModel::Idle);
    mIsLeft = !mIsLeft;
    mStartCountDown = true;
    mDistanceTravelled = 0;
    mIsJumping = false;
 //   log("enemyID:%d,posX:%.2f",getEnemy()->getObjectID(),getEnemy()->getPositionX());
}

void JumpBehaviour::updateProperty()
{
    if(mData == nullptr) {
        log("JumpBehaviour::updateProperty :data is null");
        return;
    }
    
    std::map<std::string, std::string> property = mData->getMapProperty("jumpSetting");
//    
    mCooldown = aurora::StringHelper::parseFloat(property["cooldown"]);
    mDistance = aurora::StringHelper::parseFloat(property["distance"]);
    mJumpFrame = aurora::StringHelper::parseInt(property["jumpFrame"]);
    

    mIsLeft = getEnemy()->isFaceLeft();
    
}

std::string JumpBehaviour::toString()
{
    return "JumpBehaviour";
}
