//
//  CharacterInfoLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 7/4/2017.
//
//

#ifndef CharacterInfoLayer_hpp
#define CharacterInfoLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "PlayerCharProfile.h"
#include "FBProfilePic.h"
#include "AstrodogClient.h"

USING_NS_CC;
using namespace cocos2d::ui;

class RichTextLabel;
class TutorialLayer;
class NotEnoughMoneyDialog;

class CharacterInfoLayer : public Layer , public AstrodogClientListener
{
public:
    CREATE_FUNC(CharacterInfoLayer);
    
    CC_SYNTHESIZE_RETAIN(PlayerCharProfile*, mPlayerProfile, PlayerProfile);
    
    virtual bool init();
    
    void setupUI(Node* rootNode);
    void update(float delta);
    
    void updateDiamond();
    void updateStars();
    void setupFbProfile();
    
    void setView(PlayerCharProfile* charProfile);
    
    void setUpgradeBtnCallback(std::function<void(int)> callback);
    void setOnExitCallback(std::function<void(int)> callback);
    
    void onEnter() override;
    
    void handleUpgrade(bool isTutorial = false);
    
    void setUpgradeTutorialStepOne();
    void setUpgradeTutorialStepTwo();
    
private:
    void setStatView();
    void setLevelView();
    void setUpgradeView();
    
    void setupStatBar(Node* panel, float value, float maxValue);
    
#pragma mark - AstrodogClient
public:
    virtual void onFBConnect(int status);
    
private:
    Text *mCharNameText;
    TutorialLayer *mTutorialLayer;
    
    Text *mStarText, *mDiamondText;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    Button *mFbConnectBtn;
    
    Layout *mInfoBtn;
    
    Node *mStatPanel;
    Layout *mLevelPanel;
    Node *mUpgradePanel;
    
    Button *mBackBtn;
    RichTextLabel* mInfoText;
    
    Sprite *mCharSprite;
    
    Node *mRootNode;
    Node *mBlockPanel;
    Layout *mTouchPanel;
    std::function<void(int)> mUpgradeBtnCallback;
    std::function<void(int)> mExitCallback;
    NotEnoughMoneyDialog* mNotEnoughResourcesDialog;
    
};


#endif /* CharacterInfoLayer_hpp */
