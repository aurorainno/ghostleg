//
//  LuckyDrawDialog.hpp
//  GhostLeg
//
//  Created by Calvin on 2/2/2017.
//
//

#ifndef LuckyDrawDialog_hpp
#define LuckyDrawDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "FBProfilePic.h"
#include "AstrodogClient.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NotEnoughMoneyDialog;

class LuckyDrawDialog : public Layer , public AstrodogClientListener
{
public:
    
    CREATE_FUNC(LuckyDrawDialog);
    
    LuckyDrawDialog();
    virtual bool init() override;
    
    void onEnter() override;
    void update(float delta);
    
    void updateDiamond();
    void updateStars();
    void setupFbProfile();
    
    void setupUI(Node* node);
    void onExit() override;
    
    void fadeInNextPoster();

    void setOnExitCallback(std::function<void()> callback);
    void setOnFreeDrawCallback(std::function<void()> callback);
    
    bool playVideoAd();
    void handlePlayAdEnd();
    
    void setWatchAdBtn();
	
private:
	void showNotEnoughDialog();

#pragma mark - AstrodogClient
public:
    virtual void onFBConnect(int status);
    

private:
    Text* mDiamondText;
    Text* mStarText;
    
    Button *mFbConnectBtn;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
    
    Button* mDrawBtn;
    Button* mAdBtn;
    Button* mCancelBtn;
    std::vector<Sprite*> mPosterList;
    std::function<void()> mOnExitCallback;
    std::function<void()> mOnFreeDrawCallback;
    int mPosterIndex;
    
    NotEnoughMoneyDialog* mNotEnoughDialog;
    
};


#endif /* LuckyDrawDialog_hpp */
