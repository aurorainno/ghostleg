//
//  Header.h
//  GhostLeg
//
//  Created by Calvin on 24/5/2016.
//
//

#ifndef CoinShopItemView_h
#define CoinShopItemView_h

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>

#include "CommonType.h"
#include <GameProduct.h>

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class CoinShopItemView : public Layer {
public:
    typedef std::function<void(CoinShopItemView* itemview, GameProduct *product)> CoinShopItemViewCallback;
    
    
    CREATE_FUNC(CoinShopItemView);
    
    //
    virtual bool init();
    
    //
    void setProduct(GameProduct* product);
    void setToWatchAdItem(int amount);
    Button* getPurchaseButton();
    void setCallback(const CoinShopItemViewCallback &callback);
    void setPlayAdCallback(const std::function<void()> &callback);
    void updateUI();
    void updateFacebookLikeBtn(bool isLiked);

    std::string  getItemKey();
    
private:
    void setupUI(Node *rootNode);
    void setupPurchaseButton(GameProduct *product);
    void setUpBlueActionItem(GameProduct *product);
    void setItemViewIcon();
    void setDisplayTag(std::string tag);
    void setPurchaseBtnListener();
private:
    Sprite* mIcon;
	Sprite *mItemSprite;//
    Sprite *mDiamondStage, *mStarStage;
    Button* mPurchaseBtn;
    Button* mPlayAdBtn;
    Text* mBlueActionLabel;
    Text* mAlertText;
    Text *mStarAmount, *mDiamondAmount;
    Node *mSpecialInfoPanel,*mInfoPanel,*mBlueActionInfo;
	ui::ImageView *mAlertImage;

    std::string mProductKey;
    std::string mGUIKey;
    GameProduct *mProduct;
    CoinShopItemViewCallback mCallback;
    std::function<void()> mPlayAdCallback;
};



#endif /* Header_h */
