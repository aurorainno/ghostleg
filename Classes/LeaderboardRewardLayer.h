//
//  LeaderboardRewardLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 24/2/2017.
//
//

#ifndef LeaderboardRewardLayer_hpp
#define LeaderboardRewardLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class FBProfilePic;
class LeaderboardReward;

class LeaderboardRewardLayer : public Layer
{
public:
    enum LeaderboardType{
        Local,
        Global
    };
    
    CREATE_FUNC(LeaderboardRewardLayer);
    
    CC_SYNTHESIZE(LeaderboardType, mType, LeaderboardType);
    CC_SYNTHESIZE(int, mPlanetID, PlanetID);
    
    virtual bool init();
    
    void setupUI(Node* rootNode);
    void onEnter();
    
    void setupRewardList(std::vector<LeaderboardReward*> rewardList);
    
private:
    Button* mCancelBtn;
    std::vector<Node*> mRewardViewList;
    
};
#endif /* LeaderboardRewardLayer_hpp */
