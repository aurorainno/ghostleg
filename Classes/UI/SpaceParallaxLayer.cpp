//
//  SpaceParallaxLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 6/5/2016.
//
//

#include "SpaceParallaxLayer.h"
#include "SpriteParallaxSubLayer.h"
#include "AnimationHelper.h"
#include "CsbParallaxSubLayer.h"

//const int kZOrderBg = 1;
//const int kZOrderObjects = 2;
//const int kZOrderStars = 3;


const std::string kDefaultBg = "BGMain1_1.png";
//const std::string kBgFolder = "hires";
const std::string kBgFolder = "lowres";
const float kBgScale = 1.0f;

namespace {
	SpriteParallaxSubLayer *createSpriteLayer(const std::string &spriteName,
											  const Size &parentSize, const Vec2 &speed,
											  float opacity, float scale, float spacing)
	{
		SpriteParallaxSubLayer *layer = new SpriteParallaxSubLayer();
		layer->init(parentSize, speed);
//		layer->setOpacity(opacity);
		
		layer->setSpriteByName(spriteName, scale, spacing);
		
	
		
    
		layer->autorelease();
		
		return layer;
	}
	
	SpriteParallaxSubLayer *createSpriteLayer(const std::string &headerSprite,
											  const std::string &sectionSprite,
											  const Size &parentSize,
											  const Vec2 &speed,
											  float opacity, float scale)
	{
		SpriteParallaxSubLayer *layer = new SpriteParallaxSubLayer();
		layer->init(parentSize, speed);
		//layer->setOpacity(opacity);
		
		layer->setupParallax(headerSprite, sectionSprite, scale);
		
		layer->autorelease();
		
		return layer;
	}

	
	Action *getRepeatFadeInOut(float duration, int fadeOpacity, int originOpacity)
	{
		FadeTo *fade1 = FadeTo::create(duration, fadeOpacity);
		FadeTo *fade2 = FadeTo::create(duration, originOpacity);
		
		Sequence *sequence = Sequence::create(fade1, fade2, nullptr);
		
		return RepeatForever::create(sequence);
	}
	

}

SpaceParallaxLayer::SpaceParallaxLayer()
: mBg1()
, mBg2()
, mBGSpeed(0.0f)
, mSpaceObjectID(1)
, mHasSetup(false)
, mShowRoute(true)
, mMainNode(nullptr)
{
	
}

bool SpaceParallaxLayer::init()
{
	
	Size size = Director::getInstance()->getVisibleSize();
	bool isOkay = ParallaxLayer::init(Color4B::BLACK, size);
	
	if(isOkay == false) {
		return isOkay;
	}
	
	// default setting
	///setBackground(kDefaultBg, kDefaultBg, 1);
	std::string bg = "lowres/bg_forest_bg.png";
	setBackground(bg, bg, 1);
	
	//
	mOffsetY = 0;
	mSpeed = 80.0f;
	mBackgroundLayer = nullptr;
	
	return true;
}

void SpaceParallaxLayer::setup()
{
	if(mHasSetup) {
		return;	// already setup 
	}
	
	setupParallaxSubLayer();
	
	mHasSetup = true;
}


void SpaceParallaxLayer::setBackground(const std::string &bg1, const std::string &bg2, int objectID)
{
	setBg1(kBgFolder + "/" + bg1);
	setBg2(kBgFolder + "/" + bg2);
	setSpaceObjectID(objectID);
}


void SpaceParallaxLayer::onEnter()
{
	ParallaxLayer::onEnter();
	
	setup();
}

void SpaceParallaxLayer::setupDecoLayer()
{
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale;
	finalScale = 0.5f * getScale();
	subLayer = createSpriteLayer("fancy/bg_1_deco.png", parentSize, Vec2(0.0f, 0.1f), 1.0f, finalScale, 80);
	addSubLayer(subLayer);

}


void SpaceParallaxLayer::setupRouteLayer()
{
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale = 0.5f;
	subLayer = createSpriteLayer("fancy/bg_1_line.png", parentSize, Vec2(0.0f, 1.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}
void SpaceParallaxLayer::setupRouteGuide()
{
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale = 0.5f;
	subLayer = createSpriteLayer("route_guide.png", parentSize, Vec2(0.0f, 1.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}

void SpaceParallaxLayer::setupWindLayer()
{
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale;
	finalScale = getScale();
	subLayer = createSpriteLayer("lowres/bg_wind1.png", parentSize, Vec2(0.0f, 3.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}


void SpaceParallaxLayer::setupEdgeLayer()
{
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale;
	finalScale = 0.5f * getScale();
	subLayer = createSpriteLayer("fancy/bg_1_edge.png", parentSize, Vec2(0.0f, 1.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}


void SpaceParallaxLayer::setupParallaxSubLayer()
{
	Size parentSize = this->getContentSize();
	
	
	
	
	SpriteParallaxSubLayer *subLayer;
	
	float finalScale;
	
	
	
	
	// Gradient
	//subLayer = createSpriteLayer("bg_gradient.png", parentSize, Vec2(0, 0), 1, 0.5f, 0);
	//subLayer = createSpriteLayer("astro-tuned.jpg", parentSize, Vec2(0, 0.02f), 1, 1.1f, 0);
	// subLayer = createSpriteLayer("bg.png", parentSize, Vec2(0, 0.02f), 1, 0.5f, 0);
	mBg1 = mBg2 = "lowres/bg_1.png";
	
	finalScale = kBgScale * getScale();
	subLayer = createSpriteLayer(mBg1,
								 mBg2,
								 parentSize, Vec2(0, getBGSpeed()), 1, finalScale);
	
	//subLayer->setTintColor(Color3B::BLUE);
	addSubLayer(subLayer);
	mBackgroundLayer = subLayer;
	//mBackgroundLayer->setVisible(false);
	
	
	setupDecoLayer();
	
	if(getShowRoute()){
		setupRouteLayer();
	}
	
	if(getMainNode() != nullptr) {
		addChild(getMainNode());
	}
	
	setupEdgeLayer();
	
	//setupRouteGuide();
	
	// setupWindLayer();
	
//	std::string csbName = StringUtils::format("Space/SpaceObjectsLayer_%d.csb", mSpaceObjectID);
//	
//	CsbParallaxSubLayer *csbLayer = new CsbParallaxSubLayer();
//	csbLayer->init(parentSize, Vec2(0, 0.1f));
//	csbLayer->setupCsbByName(csbName);
//	addSubLayer(csbLayer);
	
	

//	
//	// Star 1
//	finalScale = 0.5f * getScale();
//	subLayer = createSpriteLayer("scene_bg_star1.png", parentSize, Vec2(0.0f, 0.1f), 0.8f, finalScale, 80);
//	addSubLayer(subLayer);
//	
//	// Star 2
//	finalScale = 0.52f * getScale();
//	subLayer = createSpriteLayer("scene_bg_star2.png", parentSize, Vec2(0.02f, 0.2f), 0.3f, finalScale, 200);
//	subLayer->setTintColor(Color3B::YELLOW);
//	addSubLayer(subLayer);
//
//	// Star 3
//	finalScale = 0.55f * getScale();
//	subLayer = createSpriteLayer("scene_bg_star3.png", parentSize, Vec2(0.05f, 0.35f), 0.3f, finalScale, 200);
//	
//	addSubLayer(subLayer);
//	
	
	
//    Sprite* cloud = Sprite::create("BGMain1_1cloud.png");
//    cloud->setAnchorPoint(Vec2(0,0));
//    cloud->setPosition(Vec2(0,0));
//    cloud->setScale(0.5, 0.5);
//    addChild(cloud);
//    
}

void SpaceParallaxLayer::update(float delta)
{
	ParallaxLayer::update(delta);
    
	mOffsetY += delta * mSpeed;
    
	this->setScrollY(mOffsetY);
}

void SpaceParallaxLayer::setBackgroundTint(const cocos2d::Color3B &color)
{
	if(mBackgroundLayer) {
		mBackgroundLayer->setTintColor(color);
	}
}
