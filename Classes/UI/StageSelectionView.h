//
//  StageSelectionView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#ifndef StageSelectionView_hpp
#define StageSelectionView_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include "PlanetItemView.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class AILPageView;


class HorizontalListView;
class PlanetItemView;

class StageSelectionView : public Layout
{
public:
	typedef std::function<void(Ref *, int plant, bool isRepeat, int currentPage, int maxPage)> SelectPlanetCallback;
    typedef std::function<void(Vec2 scrollOffset)> ViewScrollCallback;
    typedef std::function<void()> ClickUnlockedPlanetCallback;
public:
	CREATE_FUNC(StageSelectionView);
	
	StageSelectionView();
	
	//
	virtual bool init();
	
	
	
#pragma mark - GUI Setup
private:
	void setupStageItemView();
	void setupListView();
	void onPlanetViewClick(int planetID);
	void onPageSelected(int page, bool callback = true);
	
#pragma mark - Internal GUI Data
private:
	HorizontalListView *mListView;
	Size mPreferredSize;
	Size mItemSize;
	

#pragma mark - Navigation & Selection
public:
	void setSelectedPlanet(int planetID);
	void scrollToNextPage();
	void scrollToPreviousPage();
	void scrollToPage(int page);
	

#pragma mark - Getter, Setter and Update Methods
public:
	void updatePlanetData();
	int getSelectedPlanetID();
	PlanetItemView *getPlanetItemViewAtPage(int page);
	int getPageForPlanet(int planetID);
	int getCurrentPage();
	int getLastPage();

private:
	void updatePlanetItemView(PlanetItemView *view);
	
#pragma mark - Callback
public:
	void setSelectPlanetCallback(const SelectPlanetCallback &callback);
    void setOnScrollCallback(const ViewScrollCallback &callback);
    void setOnUnlockedPlanetClickCallback(const ClickUnlockedPlanetCallback &callback);
	
	
	
	
	
	
	
    
	
private:
	
	
	
	
private:
  
	//AILPageView *mPageView;
	Vector<PlanetItemView *> mPlanetViewList;
	int mSelectedPlanet;
	
	SelectPlanetCallback mSelectPlanetCallback;
    ViewScrollCallback mScrollCallback;
    ClickUnlockedPlanetCallback mClickCallback;
};




#endif /* StageSelectionView_hpp */
