//
//  StageParallaxLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 6/5/2016.
//
//

#include "StageParallaxLayer.h"
#include "SpriteParallaxSubLayer.h"
#include "AnimationHelper.h"
#include "CsbParallaxSubLayer.h"
#include "ActionShake.h"
#include "VisibleRect.h"
#include "CommonMacro.h"
//const int kZOrderBg = 1;
//const int kZOrderObjects = 2;
//const int kZOrderStars = 3;


const std::string kBgFolder = "scene";
const float kBgScale = 1.0f;

namespace {
	std::string getBackground1(int stage)
	{
		return StringUtils::format("%s/stage%02d_bg.png", kBgFolder.c_str(), stage);
	}
	std::string getBackground2(int stage)
	{
		return StringUtils::format("%s/stage%02d_bg_loop.png", kBgFolder.c_str(), stage);
	}
	std::string getLineImage(int stage)
	{
		return StringUtils::format("%s/stage%02d_line.png", kBgFolder.c_str(), stage);
	}
	std::string getEdgeImage(int stage)
	{
		return StringUtils::format("%s/stage%02d_edge.png", kBgFolder.c_str(), stage);
	}
	std::string getDecoImage(int stage)
	{
		return StringUtils::format("%s/stage%02d_deco.png", kBgFolder.c_str(), stage);
	}
	
	
	SpriteParallaxSubLayer *createSpriteLayer(const std::string &spriteName,
											  const Size &parentSize, const Vec2 &speed,
											  float opacity, float scale, float spacing)
	{
		SpriteParallaxSubLayer *layer = new SpriteParallaxSubLayer();
		layer->init(parentSize, speed);
		//		layer->setOpacity(opacity);
		
		layer->setSpriteByName(spriteName, scale, spacing);
		
		
		
		
		layer->autorelease();
		
		return layer;
	}
	
	SpriteParallaxSubLayer *createSpriteLayer(const std::string &headerSprite,
											  const std::string &sectionSprite,
											  const Size &parentSize,
											  const Vec2 &speed,
											  float opacity, float scale)
	{
		SpriteParallaxSubLayer *layer = new SpriteParallaxSubLayer();
		layer->init(parentSize, speed);
		//layer->setOpacity(opacity);
		
		layer->setupParallax(headerSprite, sectionSprite, scale);
		
		layer->autorelease();
		
		return layer;
	}
	
	
	Action *getRepeatFadeInOut(float duration, int fadeOpacity, int originOpacity)
	{
		FadeTo *fade1 = FadeTo::create(duration, fadeOpacity);
		FadeTo *fade2 = FadeTo::create(duration, originOpacity);
		
		Sequence *sequence = Sequence::create(fade1, fade2, nullptr);
		
		return RepeatForever::create(sequence);
	}
	
	
}


StageParallaxLayer::StageParallaxLayer()
: mBg1()
, mBg2()
, mBGSpeed(0.5f)
, mSpaceObjectID(1)
, mHasSetup(false)
, mShowRoute(false)
, mShowBackground(true)
, mMainNode(nullptr)
, mStage(1)
{
	
}

bool StageParallaxLayer::init()
{
	
	Size size = Director::getInstance()->getVisibleSize();
	bool isOkay = ParallaxLayer::init(Color4B::BLACK, size);
	
	if(isOkay == false) {
		return isOkay;
	}
	
	// default setting
	///setBackground(kDefaultBg, kDefaultBg, 1);
	std::string bg = "lowres/bg_forest_bg.png";
	setBackground(bg, bg, 1);
	
	//
	mOffsetY = 0;
	mSpeed = 80.0f;
	mBackgroundLayer = nullptr;
	
	return true;
}

void StageParallaxLayer::setup()
{
	if(mHasSetup) {
		return;	// already setup
	}
	
	setupParallaxSubLayer();
	
	mHasSetup = true;
}


void StageParallaxLayer::setBackground(const std::string &bg1, const std::string &bg2, int objectID)
{
	setBg1(kBgFolder + "/" + bg1);
	setBg2(kBgFolder + "/" + bg2);
	setSpaceObjectID(objectID);
}


void StageParallaxLayer::onEnter()
{
	ParallaxLayer::onEnter();
	
	setup();
}

void StageParallaxLayer::setupDecoLayer()
{
	std::string lineImage = getDecoImage(getStage());
	
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale;
	finalScale = 0.5f * getScale();
	subLayer = createSpriteLayer(lineImage, parentSize, Vec2(0.0f, 0.7f), 1.0f, finalScale, 80);
	addSubLayer(subLayer);
	
}


void StageParallaxLayer::setupRouteLayer()
{
	std::string lineImage = getLineImage(getStage());
	
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale = 0.5f;
	subLayer = createSpriteLayer(lineImage, parentSize, Vec2(0.0f, 1.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}
void StageParallaxLayer::setupRouteGuide()
{
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale = 0.5f;
	subLayer = createSpriteLayer("route_guide.png", parentSize, Vec2(0.0f, 1.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}

void StageParallaxLayer::setupEdgeLayer()
{
	std::string image = getEdgeImage(getStage());
	
	SpriteParallaxSubLayer *subLayer;
	Size parentSize = this->getContentSize();
	
	float finalScale;
	finalScale = 0.5f * getScale();
	subLayer = createSpriteLayer(image, parentSize, Vec2(0.0f, 1.0f), 1.0f, finalScale, 0);
	addSubLayer(subLayer);
	
}


void StageParallaxLayer::shake(float duration)
{
    ParallaxLayer::shake(duration);
//	//StageParallaxLayer
//	Shake *shake = Shake::create(duration, 10);
//	mBackgroundLayer->runAction(shake);
}

void StageParallaxLayer::setupParallaxSubLayer()
{
	Size parentSize = this->getContentSize();
	
	
	
	
	SpriteParallaxSubLayer *subLayer;
	
	float finalScale = kBgScale * getScale();
	std::string bg1 = getBackground1(getStage());
	std::string bg2 = getBackground2(getStage());
	
	subLayer = createSpriteLayer(bg1,bg2,
								 parentSize, Vec2(0, getBGSpeed()), 1, finalScale);
	
	addSubLayer(subLayer);
	mBackgroundLayer = subLayer;
	mBackgroundLayer->setVisible(mShowBackground);
	
#if(IS_IOS)
	setupDecoLayer();
#endif
	
	if(getShowRoute()){
		setupRouteLayer();
	}
	
	if(getMainNode() != nullptr) {
		addChild(getMainNode());
	}
	
	//setupEdgeLayer();
}

void StageParallaxLayer::update(float delta)
{
	ParallaxLayer::update(delta);
	
	mOffsetY += delta * mSpeed;
	
	this->setScrollY(mOffsetY);
}

void StageParallaxLayer::setBackgroundTint(const cocos2d::Color3B &color)
{
	if(mBackgroundLayer) {
		mBackgroundLayer->setTintColor(color);
	}
}
