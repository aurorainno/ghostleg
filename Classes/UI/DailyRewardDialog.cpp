//
//  DailyRewardDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/11/2016.
//
//

#include "DailyRewardDialog.h"

#include "ViewHelper.h"
#include "StringHelper.h"
#include "GameSound.h"
#include "CommonMacro.h"
#include "GameManager.h"
#include "RewardManager.h"
#include "Analytics.h"
#include "DailyReward.h"
#include "AnimeNode.h"
#include "BuyNewDogLayer.h"
#include "DogManager.h"
#include "UserGameData.h"
#include "AnimationHelper.h"
#include "MainScene.h"
#include "GameSound.h"

#define kInitDialogScale 0.01
#define kFinalDialogScale 0.5

using namespace cocostudio::timeline;

DailyRewardDialog::DailyRewardDialog()
: mDayPanelMap(),
mReward(nullptr),
mCycleRange(7),
mMode(CollectRewardMode)
{
	
}

// on "init" you need to initialize your instance
bool DailyRewardDialog::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
	{
		return false;
	}
	
    mCycleRange = RewardManager::instance()->getCycleRange();
//    setScale(kInitDialogScale);
    
	// Setup UI
	std::string csb = "DailyRewardDialog.csb";
	setupGUI(csb);

	//setupAnimation(csb);
	
	return true;
}


void DailyRewardDialog::setDialogMode(DailyRewardDialog::DialogMode mode)
{
    mMode = mode;
}

void DailyRewardDialog::onEnter()
{
	Layer::onEnter();
 	
    if(mMode == CollectRewardMode){
//        mInfoText->setVisible(true);
        mCollectButton->setVisible(true);
        mConfirmButton->setVisible(false);
    }else if(mMode == ShowRecordMode){
//        mInfoText->setVisible(false);
        mCollectButton->setVisible(false);
        mConfirmButton->setVisible(true);
    }
    
    AnimationHelper::runPopupAction(mMainPanel, kInitDialogScale,kFinalDialogScale, 0.5f);
	// runAnimation("startup");
}

void DailyRewardDialog::onExit()
{
	Layer::onExit();
}

void DailyRewardDialog::closeDialog()
{
	AnimationHelper::runClosingAction(mMainPanel, 0.3f, false, [&](){
		removeFromParent();
	});
}


void DailyRewardDialog::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	
	FIX_UI_LAYOUT(rootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	mMainPanel = mainPanel;
    
    Node *bgPanel = rootNode->getChildByName("bgPanel");
    mBGPanel = bgPanel;
    
	// Setting the Button
	Button *button;
	
	button = (Button *) mainPanel->getChildByName("collectButton");
	button->addClickEventListener([&](Ref *sender) {
//		GameSound::playSound(GameSound::Sound::Button1);
		onCollectClicked();
	});
	mCollectButton = button;
    
    button = (Button *) mainPanel->getChildByName("confirmButton");
    button->addClickEventListener([&](Ref *sender) {
        GameSound::playSound(GameSound::Sound::Button1);
        closeDialog();
    });
    mConfirmButton = button;
	
	mInfoText = (Text *) mainPanel->getChildByName("infoText");
	
	// Day Panels
	for(int day=1; day <=mCycleRange; day++) {
		std::string nodeName = StringUtils::format("day%dPanel", day);
		
		Node *node = mainPanel->getChildByName(nodeName);
		
		mDayPanelMap.insert(day, node);
	}
    
    setupUIData();
	
}

void DailyRewardDialog::setupUIData()
{
    int lastCollectedDay = RewardManager::instance()->getTotalCollectedDayInTermsOfCycle();
    for(int day = 1; day <= mCycleRange; day++){
        bool isCollected = day <= lastCollectedDay;
        RewardData* data = RewardManager::instance()->getRewardByDay(day);

        setRewardAtDay(day, data);
        setCollectStatus(day,isCollected);
    }

}

void DailyRewardDialog::onCollectClicked()
{
	log("Collect is clicked");
    mCollectButton->setVisible(false);
    mReward = RewardManager::instance()->collectReward();
    RewardManager::instance()->saveData();
    GameSound::playSound(GameSound::PassCheckPoint);
    showRewardCollectAnimation(RewardManager::instance()->getTotalCollectedDayInTermsOfCycle());
}


Node *DailyRewardDialog::getDayPanelAtDay(int day)
{
	return mDayPanelMap.at(day);
}

void DailyRewardDialog::setRewardAtDay(int day, RewardData *data)
{
	if(data == nullptr) {
		log("DailyRewardDialog.setRewardAtDay: data is null");
		return;
	}
	
	Node *panel = getDayPanelAtDay(day);
	if(panel == nullptr) {
		log("DailyRewardDialog.setCollectStatus: panel is null");
		return;
	}
	
	Sprite *rewardSprite = panel->getChildByName<Sprite *>("rewardSprite");
	Text *countText = panel->getChildByName<Text *>("countText");
	

	if(data->getType() == RewardData::RewardTypeStar) {
		// Update the reward content for Star Reward
		
		// the value to reward
		countText->setVisible(true);
		countText->setString(INT_TO_STR(data->getCount()));
		
		
		//
		rewardSprite->setScale(1.4);
		rewardSprite->setTexture("guiImage/ui_general_coins.png");
		
    } else if(data->getType() == RewardData::RewardTypeDiamond) {
		// Update the reward content for Dog Reward
		
        countText->setVisible(true);
        countText->setString(INT_TO_STR(data->getCount()));
        
        
        //
        rewardSprite->setScale(1.4);
        rewardSprite->setTexture("guiImage/ui_general_diamond.png");
    } else if(data->getType() == RewardData::RewardTypeFragment) {
        // Update the reward content for Dog Reward
        
        countText->setVisible(true);
        countText->setString(INT_TO_STR(data->getCount()));
        
        
        //
        rewardSprite->setScale(0.7);
        rewardSprite->setTexture(StringUtils::format("fragment/ui_fragment_%02d.png",data->getObjectID()));
    }
	
	
	
}


void DailyRewardDialog::updateSpriteForDog(Sprite *sprite, int dog)
{
	if(sprite == nullptr) {
		return;
	}
	
	sprite->setScale(0.6);
	
	std::string spriteName = StringUtils::format("dog/dselect_dog%d_unlock.png", dog);
	sprite->setTexture(spriteName);
}

void DailyRewardDialog::setCollectStatus(int day, bool hasCollected)
{
	Node *panel = getDayPanelAtDay(day);
	if(panel == nullptr) {
		log("DailyRewardDialog.setCollectStatus: panel is null");
		return;
	}
	
	Sprite *tickSprite = panel->getChildByName<Sprite *>("tickSprite");
	if(tickSprite == nullptr) {
		log("DailyRewardDialog.setCollectStatus: tickSprite is null");
		return;
	}
	
	tickSprite->setVisible(hasCollected);
    
    Node *bg = panel->getChildByName("collectedBG");
    bg->setVisible(hasCollected);
}

void DailyRewardDialog::showRewardCollectAnimation(int day)
{

    Node *panel = getDayPanelAtDay(day);
    Sprite *tickSprite = panel->getChildByName<Sprite *>("tickSprite");

    Vec2 tickPos = panel->getPosition() + tickSprite->getPosition();
	 
	//ViewHelper::createRedSpot(mMainPanel, centerPos);		// this is for reference check
	
	AnimeNode *animeNode = AnimeNode::create();
	
	animeNode->setup("animation/DailyRewardTickNode.csb");
	animeNode->setStartAnime("start", false, false);
	animeNode->setPosition(tickPos);
	
	
	animeNode->setPlayEndCallback([&](Ref *sender, std::string animeName){
		onCollectAnimationDone();
	});
	
	mMainPanel->addChild(animeNode);
}

void DailyRewardDialog::onCollectAnimationDone()
{
	log("Reward Animation End");
    if(RewardManager::instance()->getTotalCollectedDayInTermsOfCycle() == RewardManager::instance()->getCycleRange()){
        GameSound::playSound(GameSound::CollectOnDaySeven);
    }
    if(mCallback){
        mCallback();
    }
}

void DailyRewardDialog::setCallback(std::function<void ()> callback)
{
    mCallback = callback;
}

Vec2 DailyRewardDialog::getDayPanelCenterPos(int day)
{
	Node *panel = getDayPanelAtDay(day);
	if(panel == nullptr) {
		return Vec2::ZERO;
	}

	
	Vec2 pos = panel->getPosition();
	Size size = panel->getContentSize();
	
	return pos + Vec2(size.width/2, size.height/2);
}

