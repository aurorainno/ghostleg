//
//  DogSelectItemView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/10/2016.
//
//

#include "DogSelectItemView.h"
#include "VisibleRect.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "DogData.h"
#include "DogManager.h"
#include "CircularMaskNode.h"
#include "SuitManager.h"
#include "GameSound.h"
#include "MasteryManager.h"
#include "Mastery.h"
#include "GameManager.h"
#include "RichTextLabel.h"
#include "BuyNewDogLayer.h"
#include "DogSelectionScene.h"
#include "UserGameData.h"
#include "CoinShopScene.h"
#include "DogInfoLayer.h"
#include "AnimationHelper.h"
#include "Constant.h"
#include "Analytics.h"
#include "PlanetManager.h"
#include "NotEnoughCandyDialog.h"

using namespace cocos2d::ui;

DogSelectItemView::DogSelectItemView()
: mDogID(0)
, mDogSelectCallback(0)
, mIsForSuggestion(false)
, mProgressText(nullptr)
, mDogSelectionScene(nullptr)
{
	
}

bool DogSelectItemView::init()
{
	if(Layout::init() == false) {
		return false;
	}
	
	std::string csb = "DogSelectItemView.csb";
	setupGUI(csb);
	
	setVisible(false);
	return true;
}

void DogSelectItemView::setDog(int dogID)
{
	
	mDogID = dogID;
    DogData *data = DogManager::instance()->getDogData(mDogID);
	if(data ==nullptr) {
		return;
	}
    
    mSuitID = data->getSuitID();
    mSuitLevel = data->getSuitLevel();
    mMasteryMap = data->getMasteryMap();
    
    int planetID = PlanetManager::instance()->getPlanetWithDog(dogID);
    PlanetData *planetData = PlanetManager::instance()->getPlanetData(planetID);
    mIsEventDog = planetData->getPlanetType() == PlanetData::Event;
	
	updateUnlockStatus();		// Update the data from DogManager or other Game Manager

	updateDogGUI();				// Update the gui based on the View internal data
    
    setupPurchaseBtn();         //setup the purchase btn
	
	setVisible(true);
}

void DogSelectItemView::updateUnlockStatus()		// update the unlock status
{
    bool isSelected = mDogID == DogManager::instance()->getSelectedDogID();
    bool isUnlocked = DogManager::instance()->getUnlockStatus(mDogID) == DogManager::StatusUnlocked;
    
	mIsSelected = isSelected;
	mIsUnLocked = isUnlocked;
//	mIsReadyToUnlock = false;

	DogManager::instance()->getUnlockProgress(mDogID, mUnlockValue, mUnlockCond);
}

int DogSelectItemView::getDogID()
{
	return mDogID;
}

void DogSelectItemView::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	
	setSizeType(Widget::SizeType::ABSOLUTE);
	setContentSize(rootNode->getContentSize());


	Layout *basePanel = (Layout *) rootNode->getChildByName("basePanel");
	basePanel->addClickEventListener([&](Ref *) {
		onViewClicked();
	});

	DogData *data = DogManager::instance()->getDogData(mDogID);
	if(data != nullptr) {
		mSuitID = data->getSuitID();
		mSuitLevel = data->getSuitLevel();
		
		mMasteryMap = data->getMasteryMap();
	}
	
	
//	Node *mDogIconPanel;
//	Node *mBgSelected;
//	Node *mBgNormal;
//	Sprite *mDogIconSprite;
//	Sprite *mDogSprite;
//	Text *mDogProgressText;
//	Text *mDogInfoText;
//	Text *mDogNameText;

	
	// unlockedBg vs normalBg  : Sprite
	mBgUnlocked = rootNode->getChildByName("unlockedBg");
	mBgNormal = rootNode->getChildByName("normalBg");
	
	// dogSprite : Sprite
	mDogSprite = (Sprite *) rootNode->getChildByName("dogSprite");
	
	// dogIconPanel
	mDogIconPanel = rootNode->getChildByName("dogIconPanel");
	
	// dogIconSprite / progressSprite
	mDogIconSprite = (Sprite *) mDogIconPanel->getChildByName("dogIconSprite");
	mProgressSprite = (Sprite *) mDogIconPanel->getChildByName("progressSprite");
	
	// progressText / Text
//	mProgressText = (Text *) mDogIconPanel->getChildByName("progressText");
	Text *progressText = mDogIconPanel->getChildByName<Text *>("progressText");
	mProgressText = RichTextLabel::createFromText(progressText);
	mProgressText->ignoreContentAdaptWithSize(true);	// ken: must be true, to make align center
	progressText->setVisible(false);		// hide the textbox just for design preview
	
	mProgressText->setTextColor(1, kColorGreen);
	
//    Vec2 pos = mDogIconPanel->getChildByName("progressText")->getPosition();
//    RichTextLabel* label = RichTextLabel::create();
//    label->setScale(0.5);
//    label->setFont("Caviar_Dreams_Bold.ttf", 23);
//    label->setPosition(pos);
//    label->setAnchorPoint(Vec2(0.5,0.5));
//	label->setContentSize(Size(150, 30));		// ken: crash if not setting this, it's strange!!!
//
//    mProgressText = label;
    mDogIconPanel->addChild(mProgressText);
	
	// dogNameText / Text
	mDogNameText = (Text *) rootNode->getChildByName("dogNameText");
    mDogNameText->ignoreContentAdaptWithSize(true);
	
	// dogInfoText / Text (
    mLockedInfoPanel = rootNode->getChildByName("lockedInfoPanel");
	mDogInfoText = (Text *) mLockedInfoPanel->getChildByName("dogInfoText");
    
    mUnlockedInfoPanel = rootNode->getChildByName("unlockedInfoPanel");
    mSuitPanel = mUnlockedInfoPanel->getChildByName("suitPanel");
    mMasteryPanel = mUnlockedInfoPanel->getChildByName("masteryPanel");
    
    mSuitName = mSuitPanel->getChildByName<Text*>("suitName");
    mSuitIcon = mSuitPanel->getChildByName<Sprite*>("suitIcon");
    mSuitLevelIcon = mSuitPanel->getChildByName<Sprite*>("suitLevelIcon");
    
    mMasteryName = mMasteryPanel->getChildByName<Text*>("masteryName");
    mMasteryIcon = mMasteryPanel->getChildByName<Sprite*>("masteryIcon");
    mMasteryLevelIcon = mMasteryPanel->getChildByName<Sprite*>("masteryLevelIcon");
    
    mSelectBtn = rootNode->getChildByName<Button*>("selectBtn");
    mInUseBtn = rootNode->getChildByName<Button*>("InUseBtn");
    mNormalCostBtn = rootNode->getChildByName<Button*>("unlockBtn");
    mEventCostBtn = rootNode->getChildByName<Button*>("eventUnlockBtn");
    
    mSelectBtn->addClickEventListener([&](Ref*){
        if(mDogSelectCallback){
			GameSound::playSound(GameSound::Sound::Button1);
            mDogSelectCallback(mDogID);
			
			// tracking
			
        }
    });
    
    mInfoBtn = rootNode->getChildByName<Button*>("infoButton");
    mInfoBtn->addClickEventListener([&](Ref*){
        onViewClicked();
    });
    
    
    mNewLabel = rootNode->getChildByName<Sprite*>("newLabel");
    AnimationHelper::setMoveUpEffect(mNewLabel, 0.3, 5, false, 0);
    
    setupProgressBar();
}

void DogSelectItemView::setupProgressBar()
{
    CircularMaskNode *node = CircularMaskNode::create();
    node->setPosition(Vec2(mDogIconSprite->getContentSize().width/2,mDogIconSprite->getContentSize().height/2));
    mDogIconSprite->addChild(node);
    
    Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");
//    sprite->setScale(0.5);
//    sprite->setContentSize(sprite->getContentSize() * 0.5);
    node->addChild(sprite);
    
    mProgressBar = node;
}

void DogSelectItemView::setupPurchaseBtn()
{
    DogData *data = DogManager::instance()->getDogData(mDogID);
    int price = data->getPrice();
    Button* buyBtn;
    if(mIsEventDog){
        buyBtn = mEventCostBtn;
		
		if(GameManager::instance()->isEventOn() == false) {
			buyBtn->setVisible(false);
		}
    }else{
        buyBtn = mNormalCostBtn;
    }
    
    Text* costText = buyBtn->getChildByName<Text*>("cost");
    costText->setString(StringUtils::format("%d",price));
    
    buyBtn->addClickEventListener([&](Ref*){
        handlePurchaseDog(true);
		
		
    });
}

bool DogSelectItemView::handlePurchaseDog(bool showUIAfterPurchase, Node* visibleNode)
{
    //handle deduce coins, show BuyDogSuccessLayer, refresh item view
    DogData *data = DogManager::instance()->getDogData(mDogID);
	if(data == nullptr) {
		log("handlePurchaseDog: data is nullptr. dog=%d", mDogID);
		return false;
	}
	
	
    int price = data->getPrice();
	bool canBuy = GameManager::instance()->checkMoneyValue(data->getMoneyType(), price);
	
	
	// Redirect player to shop
	if(canBuy == false) {
		if(mIsEventDog){
			NotEnoughCandyDialog *dialog = NotEnoughCandyDialog::create();
			dialog->setPopScene(true);
			mDogSelectionScene->addChild(dialog);
		}else{
			auto scene = CoinShopSceneLayer::createScene();
			Director::getInstance()->pushScene(scene);
		}
		
		return false;
	}
	
	
	DogManager::instance()->unlock(mDogID);
	
	GameManager::instance()->useMoney(data->getMoneyType(), price);
	

	//
	// event tracking
	std::string dogName = DogManager::instance()->getDogName(mDogID);
	
	if(visibleNode != nullptr) {
		DogInfoLayer *infoLayer = (DogInfoLayer *) visibleNode;
		infoLayer->updateMoney();
	}
	
	//
	if(mDogSelectionScene){
		mDogSelectionScene->hideView();
		
		// Show unlock information
		BuyNewDogLayer* layer = BuyNewDogLayer::create();
		layer->setDog(mDogID);
		layer->setCallback([&,this,layer,showUIAfterPurchase,visibleNode](){
			if(showUIAfterPurchase){
				mDogSelectionScene->showView();
			}
			if(visibleNode){
				visibleNode->setVisible(true);
			}
			layer->removeFromParent();
		});
		
		mDogSelectionScene->addChild(layer);
		
		// refresh information
		mDogSelectionScene->refreshItemView(mDogID);
		mDogSelectionScene->setupTopBar();
	}
	
	    //return true if player has enough coins
    return canBuy;
}

void DogSelectItemView::onViewClicked()
{
	// No need to proceed if for suggestion purpose
	if(mIsForSuggestion == true) {
		return;
	}
	
	
	log("onsViewClicked");
//    if(mDogSelectCallback){
//        mDogSelectCallback(mDogID);
//    }
    if(!mDogSelectionScene){
        log("No dog selection scene found!");
        return;
    }
    
    if(mSuitID == 0 && mMasteryMap.empty()){
        return;
    }
    
//    DogData *data = DogManager::instance()->getDogData(mDogID);
//    int price = data->getPrice();
    
    mDogSelectionScene->hideView();
    
    DogInfoLayer* layer = DogInfoLayer::create();

	
	layer->setDog(mDogID);
    
    layer->setCancelCallBack([&,layer](){
        mDogSelectionScene->showView();
        layer->removeFromParent();
    });
    
    layer->setPurchaseCallBack([&](Ref *sender){
		
		
		Node *infoNode = (Node *) sender;
        if(handlePurchaseDog(false, infoNode)){
            infoNode->setVisible(false);
        }
    });
    
    layer->setSelectCallBack([&,layer](){
        mDogSelectCallback(mDogID);
        layer->setupBtnVisibility();
    });
	
	
    mDogSelectionScene->addChild(layer);

	
}


void DogSelectItemView::updateDogGUI()			// upate the GUI elements
{
	DogData *dogData = DogManager::instance()->getDogData(mDogID);
	
	if(dogData == nullptr) {
		log("updateDogGUI: dogData is null. id=%d", mDogID);
		return;
	}
	
	// update gui element visible
	updateGUIVisibility();
	
	//
	updateUnlockProgress();
	
	// Dog Name
	setDogName(dogData->getName());
	
	// Update Dog Icon
    int resID = dogData->getAnimeID();
	std::string spriteName = StringUtils::format("dog/dselect_dog%d_unlock.png", resID);
	std::string iconSpriteName = StringUtils::format("dog/dselect_dog%d.png", resID);
	mDogSprite->setTexture(spriteName);
	mDogIconSprite->setTexture(iconSpriteName);
    
    //update the info btn position
    float posX = mDogNameText->getPositionX() + mDogNameText->getContentSize().width / 2 + 12;
//   log("dogNameTextWidth: %.2f text posX; %.2f",mDogNameText->getContentSize().width,mDogNameText->getPositionX());
    mInfoBtn->setPositionX(posX);
	
	// info
	setDogInfo(dogData);
}

void DogSelectItemView::testUIState(bool isSelected, bool isUnlock,
							int unlockValue, int unlockCond)
{
	mIsSelected = isSelected;
	mIsUnLocked = isUnlock;
	mUnlockValue = unlockValue;
	mUnlockCond = unlockCond;
	
	updateDogGUI();
}

void DogSelectItemView::updateGUIVisibility()
{
    DogData *data = DogManager::instance()->getDogData(mDogID);
    if(data == nullptr){
        log("cannot get correct dog data!");
        return;
    }
    std::map<std::string, int> condList = data->getUnlockCondList();
    bool isDailyRewardDog = false;
    std::map<std::string, int>::iterator it = condList.find("dailyReward");
    if(it != condList.end()){
        isDailyRewardDog = true;
    }
    
    Button* buyBtn;
    if(mIsEventDog){
        buyBtn = mEventCostBtn;
        mNormalCostBtn->setVisible(false);
    }else{
        buyBtn = mNormalCostBtn;
        mEventCostBtn->setVisible(false);
    }
    
    
	if(mIsSelected) {
        mInUseBtn->setVisible(true);
        mSelectBtn->setVisible(false);
        buyBtn->setVisible(false);
    } else if(mIsUnLocked){
        mSelectBtn->setVisible(true);
        buyBtn->setVisible(false);
        mInUseBtn->setVisible(false);
    }else if(isDailyRewardDog){
        buyBtn->setVisible(false);
        mInUseBtn->setVisible(false);
        mSelectBtn->setVisible(false);
    }else{
        buyBtn->setVisible(true);
        mInUseBtn->setVisible(false);
        mSelectBtn->setVisible(false);
    }
	
	//setup background, info panel and dog icon
	if(mIsUnLocked||mIsEventDog) {
        mBgUnlocked->setVisible(true);
        mBgNormal->setVisible(false);
		mDogSprite->setVisible(true);
		mDogIconPanel->setVisible(false);
        mUnlockedInfoPanel->setVisible(true);
        mLockedInfoPanel->setVisible(false);
	} else {
        mBgUnlocked->setVisible(false);
        mBgNormal->setVisible(true);
		mDogSprite->setVisible(false);
		mDogIconPanel->setVisible(true);
        mUnlockedInfoPanel->setVisible(false);
        mLockedInfoPanel->setVisible(true);
	}
    
    if(DogManager::instance()->isNewDog(mDogID)){
        mNewLabel->setVisible(true);
    }else{
        mNewLabel->setVisible(false);
    }
    
    // setup unlocked dog info (skill info)
    float skillPanelTopY = mUnlockedInfoPanel->getContentSize().height;
    float masteryPosY = skillPanelTopY/2, suitPosY = skillPanelTopY;
    if(mSuitID == 0 && mMasteryMap.empty() == true){
        mMasteryPanel->setVisible(false);
        mSuitPanel->setVisible(false);
        mInfoBtn->setVisible(false);
    }else if(mSuitID && mMasteryMap.empty() == true){
        mMasteryPanel->setVisible(false);
        mSuitPanel->setVisible(true);
        mInfoBtn->setVisible(true);
    }else if(mSuitID == 0 && mMasteryMap.empty() == false){
        mMasteryPanel->setVisible(true);
        mSuitPanel->setVisible(false);
        mInfoBtn->setVisible(true);
        //move mastery panel upward
        masteryPosY = skillPanelTopY;
    }else{
        mMasteryPanel->setVisible(true);
        mSuitPanel->setVisible(true);
        mInfoBtn->setVisible(true);
    }
    mSuitPanel->setPosition(0, suitPosY);
    mMasteryPanel->setPosition(0,masteryPosY);
    
}

void DogSelectItemView::setCallback(DogSelectCallback callBack){
    mDogSelectCallback = callBack;
}

bool DogSelectItemView::hasCallback(){
    return mDogSelectCallback!=nullptr;
}

void DogSelectItemView::setDogName(const std::string &name)
{
	mDogNameText->setString(name);
}

void DogSelectItemView::setDogInfo(DogData* data)
{
	if(data == nullptr) {
		return;
	}
	
    if(mIsUnLocked == false && !mIsEventDog){
		std::string msg = DogManager::instance()->getUnlockMessage(data);
        mDogInfoText->setString(msg);
		return;
    }
	
    if(mSuitID){
        SuitData *suit = GameManager::instance()->getSuitManager()->getSuitData(mSuitID);
		
        if(suit == nullptr) {
            log("Suit is null for suitID=%d dog=%d", mSuitID, mDogID);
        }else{
            std::string suitName = suit->getName();
            std::string suitRes = suit->getItemIcon();
    
            mSuitName->setString(suitName);
            mSuitIcon->setTexture("guiImage/" + suitRes);
            mSuitLevelIcon->setTexture(getLevelIcon(mSuitLevel));
        }
    }
    
    if(!mMasteryMap.empty()){
        int firstMastery = mMasteryMap.begin()->first;
        int firstMasteryLevel = mMasteryMap.begin()->second;
    
        MasteryData *mastery = GameManager::instance()->getMasteryManager()->getMasteryData(firstMastery);
    
        if(mastery == nullptr) {
            log("Mastery is null for masteryID=%d dog=%d", firstMastery, mDogID);
        }else{
            std::string masteryName = mastery->getName();
            std::string masteryRes = mastery->getItemIcon();
    
            mMasteryName->setString(masteryName);
            mMasteryIcon->setTexture("guiImage/" + masteryRes);
            mMasteryLevelIcon->setTexture(getLevelIcon(firstMasteryLevel));
        }
    }
//
//	//set level
//	mSuitLevelIcon->setTexture(getLevelIcon(mSuitLevel));
//	mMasteryLevelIcon->setTexture(getLevelIcon(mMasteryMap.begin()->second));
//        //set name
//        mSuitName->setString(suit->getName());
//        mMasteryName->setString(mastery->getName());
//        //set icon
//	
//	
//    }
//	
//	SuitData* suit = GameManager::instance()->getSuitManager()->getSuitData(mSuitID);
//	
//	int firstMastery = mMasteryMap.begin()->first;
//	
//	MasteryData* mastery = GameManager::instance()->getMasteryManager()->getMasteryData(firstMastery);
//	
	
}

std::string DogSelectItemView::getLevelIcon(int level)
{
    std::string resName = StringUtils::format("guiImage/ui_dog_abilitylevel%02d.png",level);
    return resName;
}

std::string DogSelectItemView::getDogInfo(DogData *data)
{
	if(mIsUnLocked) {
		return data->getInfo();
	} else {
		return DogManager::instance()->getUnlockMessage(data);
	}
}

void DogSelectItemView::updateUnlockProgress()
{
    if(mIsUnLocked || mIsEventDog){
        return;
    }
    
    mUnlockValue = mUnlockValue > mUnlockCond ? mUnlockCond : mUnlockValue;
    
	// Text
//	std::string first = StringUtils::format("%d", mUnlockValue);
//	std::string second = StringUtils::format("/%d", mUnlockCond);

	std::string progressStr = StringUtils::format("<c1>%d</c1> / %d", mUnlockValue, mUnlockCond);
	
//	mProgressText
//	
	//mProgressText->removeAllElement();
	mProgressText->setString(progressStr);
//	mProgressText->add("____xxx");
//	mProgressText->add(first, Color4B(49,216,212,255));
	//mProgressText->add(second);
	
	// ->add(second);
	
	// Progress Indicator
    float percent;
    if(mUnlockValue > mUnlockCond){
        percent = 100;
    }else if(mUnlockCond <= 0){
        percent = 0;
    }else{
        percent = (float) mUnlockValue / mUnlockCond *100;
    }
    
    mProgressBar->updateAngleByPercent(percent);
	
}

void DogSelectItemView::setDogForSuggestion(int dogID)
{
	setDog(dogID);
	
	mIsSelected = false;
	mIsUnLocked = true;
	mIsForSuggestion = true;
	updateDogGUI();
	
	// Hide the select button
	mSelectBtn->setVisible(false);
	mInfoBtn->setVisible(false);
	mNewLabel->setVisible(false);
	
	// Hide the info button 
	
	//
	setVisible(true);
}
