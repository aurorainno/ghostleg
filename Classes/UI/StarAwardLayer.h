//
//  StarAwardLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 18/10/2016.
//
//

#ifndef StarAwardLayer_hpp
#define StarAwardLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class StarAwardLayer : public Layer
{
public:
    CREATE_FUNC(StarAwardLayer);

    virtual bool init() override;
    
    void setupUI(Node* node);
    
    void updateCoinText(int originalCoins , int targetCoins);
    
    virtual void onEnter() override;
    
private:
    Text* mLeftUpperStarText;
    Text* mCenterStarText;
    Button* mOKBtn;
    float mAccumCoins;
};


#endif /* StarAwardLayer_hpp */
