//
//  DogInfoLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 1/11/2016.
//
//

#include "DogInfoLayer.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "SuitManager.h"
#include "GameSound.h"
#include "MasteryManager.h"
#include "GameManager.h"
#include "DogManager.h"
#include "DogData.h"
#include "Suit.h"
#include "Mastery.h"
#include "Analytics.h"
#include "RichTextLabel.h"
#include "Constant.h"
#include "GameRes.h"
#include "StringHelper.h"


DogInfoLayer::DogInfoLayer()
: mPassiveInfoLabel(nullptr)
, mActiveInfoLabel(nullptr)
, mMoneyType(MoneyTypeStar)
{
	
	
}

bool DogInfoLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    
    Node *rootNode = CSLoader::createNode("DogInfoLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
	
	LOG_SCREEN(Analytics::Screen::dog_info);
    
    return true;
}

void DogInfoLayer::setupUI(cocos2d::Node *node)
{
    FIX_UI_LAYOUT(node);
    mStarText = node->getChildByName<Text*>("totalCoinText");
    updateStar();
    
    mActivePanel = node->getChildByName("activePanel");
    
    mPassivePanel = node->getChildByName("passivePanel");
   
	mMoneyIcon = node->getChildByName<Sprite *>("moneyIcon");
	
	
    mPurchaseBtn = mPassivePanel->getChildByName<Button*>("unlockBtn");
    mPurchaseBtn->addClickEventListener([&](Ref*){
		GameSound::playSound(GameSound::Sound::Button1);
        if(mPurchaseCallBack){
            mPurchaseCallBack(this);
        }
		
		
    });
	mButtonMoneyIcon = mPurchaseBtn->getChildByName<Sprite *>("buttonMoneyIcon");
    
    mSelectBtn = mPassivePanel->getChildByName<Button*>("selectBtn");
    mSelectBtn->addClickEventListener([&](Ref*){
		GameSound::playSound(GameSound::Sound::Button1);
        if(mSelectCallBack){
            mSelectCallBack();
        }
		
		
    });
    
    mInUseBtn = mPassivePanel->getChildByName<Button*>("InUseBtn");
    
    mCancelBtn = node->getChildByName<Button*>("cancelBtn");
    mCancelBtn->addClickEventListener([&](Ref*){
		GameSound::playSound(GameSound::Sound::Button1);
        if(mCancelCallBack){
            mCancelCallBack();
        }
    });
	
	setSkillInfoLabel();
}


void DogInfoLayer::configRichTextLabel(RichTextLabel *label)
{
	if(label == nullptr) {
		return;
	}
	
	label->setTextColor(1, kColorGreen);
	label->setTextColor(2, kColorYellow);
	
}

void DogInfoLayer::setSkillInfoLabel()
{
//	mActivePanel = node->getChildByName("activePanel");
//	mPassivePanel = node->getChildByName("passivePanel");

	Text *infoLabel;
	
	// active panel
	infoLabel = mActivePanel->getChildByName<Text*>("skillInfo");
	mActiveInfoLabel = RichTextLabel::createFromText(infoLabel);
	configRichTextLabel(mActiveInfoLabel);
	mActivePanel->addChild(mActiveInfoLabel);
	infoLabel->setVisible(false);
	
	// passive panel
	infoLabel = mPassivePanel->getChildByName<Text*>("skillInfo");
	mPassiveInfoLabel = RichTextLabel::createFromText(infoLabel);
	configRichTextLabel(mPassiveInfoLabel);
	mPassivePanel->addChild(mPassiveInfoLabel);
	infoLabel->setVisible(false);
	
	
}


void DogInfoLayer::setPurchaseCallBack(OnPurchase callback){
    mPurchaseCallBack = callback;
}

void DogInfoLayer::setCancelCallBack(OnCancel callback){
    mCancelCallBack = callback;
}

void DogInfoLayer::setSelectCallBack(OnSelect callback){
    mSelectCallBack = callback;
}

void DogInfoLayer::setLevelBar(cocos2d::Node *panel, int level)
{
    Node* levelPanel = panel->getChildByName("levelPanel");
    if(levelPanel == nullptr){
        log("cannot get levelPanel!");
        return;
    }
    Vector<Node*> levelSpriteList = levelPanel->getChildren();
    for(int i=1;i<levelSpriteList.size();i+=2){
        Sprite* tempA = dynamic_cast<Sprite*>(levelSpriteList.at(i));
        Sprite* tempB = dynamic_cast<Sprite*>(levelSpriteList.at(i+1));
        
        if(!tempA || !tempB){
            continue;
        }
        
        tempA->setVisible(true);
        tempB->setVisible(true);
        
        level--;
        if(level<=0){
            break;
        }
    }
}


void DogInfoLayer::updateMoney()
{
	int value = GameManager::instance()->getMoneyValue(mMoneyType);
	
	
	mStarText->setString(INT_TO_STR(value));
}

void DogInfoLayer::updateStar()
{
    char temp[100];
    sprintf(temp, "%d", GameManager::instance()->getUserData()->getTotalCoin());
    mStarText->setString(temp);
}

void DogInfoLayer::setActive(int activeID, int activeLevel)
{
    SuitData* activeSkill = GameManager::instance()->getSuitManager()->getSuitData(activeID);
    std::string description = activeSkill->getDisplayInfo(activeLevel);
    std::string name = activeSkill->getName();
    
    Sprite* skillName = mActivePanel->getChildByName<Sprite*>("skillName");
    Sprite* skillIcon = mActivePanel->getChildByName<Sprite*>("skillIcon");
    Sprite* skillBG = mActivePanel->getChildByName<Sprite*>("skillBG");
	
    
    skillName->setTexture(StringUtils::format("guiImage/ability_name_%02d.png",activeID));
    skillBG->setTexture(StringUtils::format("guiImage/ability_bg_%02d.png",activeID));
    skillIcon->setTexture(StringUtils::format("guiImage/suit_skill_%02d.png",activeID));
	
	// Text* skillInfo = mActivePanel->getChildByName<Text*>("skillInfo");
	if(mActiveInfoLabel) {
		mActiveInfoLabel->setString(description);
	}

//    setLevelBar(mActivePanel,activeLevel);
}

void DogInfoLayer::setPassive(int passiveID, int passiveLevel)
{
    MasteryData* passiveSkill = GameManager::instance()->getMasteryManager()->getMasteryData(passiveID);
    std::string description = passiveSkill->getDisplayInfo(passiveLevel);
    std::string name = passiveSkill->getName();
    
    Sprite* skillName = mPassivePanel->getChildByName<Sprite*>("skillName");
    Sprite* skillIcon = mPassivePanel->getChildByName<Sprite*>("skillIcon");
    Sprite* skillBG = mPassivePanel->getChildByName<Sprite*>("skillBG");
	

    
    skillName->setTexture(StringUtils::format("guiImage/passive_name_%02d.png",passiveID));
    skillBG->setTexture(StringUtils::format("guiImage/passive_bg_%02d.png",passiveID));
    skillIcon->setTexture(StringUtils::format("guiImage/passive_icon_%02d.png",passiveID));

	// Text* skillInfo = mPassivePanel->getChildByName<Text*>("skillInfo");
	// skillInfo->setString(description);
	if(mPassiveInfoLabel) {
		mPassiveInfoLabel->setString(description);
	}
	
//    setLevelBar(mPassivePanel,passiveLevel);
}

void DogInfoLayer::setPrice(int price)
{
    Text* costText = mPurchaseBtn->getChildByName<Text*>("cost");
    costText->setString(StringUtils::format("%d",price));
}

void DogInfoLayer::setVisible(bool isVisible)
{
    Layer::setVisible(isVisible);
    
    if(isVisible == true){
        setupBtnVisibility();
    }
}

void DogInfoLayer::onEnter()
{
    Layer::onEnter();
    updateMoney();
}

void DogInfoLayer::setDog(int dogID)
{
    mDogID = dogID;
    DogData *data = DogManager::instance()->getDogData(mDogID);
    
    mSuitID = data->getSuitID();
    mSuitLevel = data->getSuitLevel();
    mMasteryMap = data->getMasteryMap();
	
    setActive(mSuitID,mSuitLevel);
    setPassive(mMasteryMap.begin()->first, mMasteryMap.begin()->second);
    setPrice(data->getPrice());
	
	
	setMoneyType(data->getMoneyType());
	
	if(data->getMoneyType() != MoneyTypeStar) {
		if(GameManager::instance()->isEventOn() == false) {
			mPurchaseBtn->setEnabled(false);
		}
		
	}
	
	
    setupBtnVisibility();
}

void DogInfoLayer::setupBtnVisibility()
{
    DogData *data = DogManager::instance()->getDogData(mDogID);
    if(data == nullptr){
        log("cannot get correct dog data!");
        return;
    }
    std::map<std::string, int> condList = data->getUnlockCondList();
    bool isDailyRewardDog = false;
    std::map<std::string, int>::iterator it = condList.find("dailyReward");
    if(it != condList.end()){
        isDailyRewardDog = true;
    }
	
	
	
    if(DogManager::instance()->getSelectedDogID() == mDogID) {
        mInUseBtn->setVisible(true);
        mSelectBtn->setVisible(false);
        mPurchaseBtn->setVisible(false);
    } else if(!DogManager::instance()->isLocked(mDogID)){
        mSelectBtn->setVisible(true);
        mPurchaseBtn->setVisible(false);
        mInUseBtn->setVisible(false);
    }else if(isDailyRewardDog){
        mPurchaseBtn->setVisible(false);
        mInUseBtn->setVisible(false);
        mSelectBtn->setVisible(false);
    }else{
        mPurchaseBtn->setVisible(true);
        mInUseBtn->setVisible(false);
        mSelectBtn->setVisible(false);
		
		
    }

}


void DogInfoLayer::setMoneyType(MoneyType type)
{
	// define the type
	mMoneyType = type;
	
	std::string iconRes = GameRes::getMoneyIcon(type);
	
	// Change the information on the top-left
	mMoneyIcon->setTexture(iconRes);
	
	updateMoney();
	
	// Change the unlock by star/candy icon
	mButtonMoneyIcon->setTexture(iconRes);
}
