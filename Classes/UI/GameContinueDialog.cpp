//
//  GameContinueDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 6/10/2016.
//
//

#include "GameContinueDialog.h"

#include "GameContinueDialog.h"
#include "ViewHelper.h"
#include "GameSound.h"
#include "CommonMacro.h"
#include "AdManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameSound.h"
#include "AdManager.h"
#include "NotEnoughCoinDialog.h"
#include "GameWorld.h"
#include "CoinShopScene.h"
#include "Analytics.h"
#include "Price.h"
#include "CircularMaskNode.h"
#include "NPCMapLogic.h"

using namespace cocostudio::timeline;

#define kCountDownTime 5

GameContinueDialog::GameContinueDialog()
: mReviveCost(300)
, mCounterActive(false)
{
	
}

// on "init" you need to initialize your instance
bool GameContinueDialog::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
	{
		return false;
	}
	
	
	// Setup UI
	std::string csb = "GameContinueDialog.csb";
	setupGUI(csb);
	setupAnimation(csb);
	
	setReviveCost(GameManager::instance()->getReviveCost());
    mRemainTime = kCountDownTime;
    mCounterActive = true;
    this->scheduleUpdate();
	
	return true;
}

void GameContinueDialog::doCallback(GameContinueDialog::GameContinueOption opt)
{
	if(mCallback) {
		mCallback(this, opt);
	}
}

void GameContinueDialog::update(float delta)
{
    if(!mCounterActive){
        return;
    }
    
    mRemainTime = mRemainTime - delta < 0 ? 0 : mRemainTime - delta;
    float percent = mRemainTime / kCountDownTime * 100;
    mClockSprite->updateAngleByPercent(percent);
    if(mRemainTime <= 0){
        GameSound::playSound(GameSound::Sound::Button1);
        Analytics::instance()->logGameContinue("continue_cancel", 0);	// 15 stars
        doCallback(GameContinueOptionExit);
    }
}

void GameContinueDialog::onEnter()
{
	Layer::onEnter();
    mCounterActive = true;
	runAnimation("startup");
    updateMapInfo();
}

void GameContinueDialog::onExit()
{
    mCounterActive = false;
	Layer::onExit();
}

void GameContinueDialog::closeDialog()
{
	this->removeFromParent();
}

void GameContinueDialog::setupAnimation(const std::string &csbName)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
}

void GameContinueDialog::runAnimation(const std::string &name)
{
	if(mTimeline) {
		mTimeline->play(name, false);
	}
}

void GameContinueDialog::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	
	FIX_UI_LAYOUT(rootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	mMainPanel = mainPanel;
	
	// Setting the Button
	Button *button;
	
	button = (Button *) mainPanel->getChildByName("useAdButton");
	button->addClickEventListener([&](Ref *sender) {
		GameSound::playSound(GameSound::Sound::Button1);
		onWatchAdClicked();
	});
	mWatchAdButton = button;
	
	button = (Button *) mainPanel->getChildByName("useStarButton");
	button->addClickEventListener([&](Ref *sender) {
		GameSound::playSound(GameSound::Sound::Button1);
		onReviveWithCoin();
	});
	mUseStarButton = button;
	
	
	
	// ????
    mAlertLayer = rootNode->getChildByName("AlertLayer");
    Button *confirmBtn = mAlertLayer->getChildByName<Button*>("confirmBtn");
    confirmBtn->addClickEventListener([=](Ref*){
        mAlertLayer->setVisible(false);
        mCounterActive = true;
    });
    
    Button *cancelBtn = mAlertLayer->getChildByName<Button*>("cancelBtn");
    cancelBtn->addClickEventListener([=](Ref*){
        doCallback(GameContinueOptionExit);
    });
		
//	button = (Button *) mainPanel->getChildByName("noButton");
//	button->addClickEventListener([&](Ref *sender) {
//		GameSound::playSound(GameSound::Sound::Button1);
//		
//		Analytics::instance()->logGameContinue("continue_cancel", 0);	// 15 stars
//		
//		doCallback(GameContinueOptionExit);
//	});
    
    
    mTotalStarCountText = mainPanel->getChildByName<Text*>("totalStarCount");
    mStarCostText = mUseStarButton->getChildByName<Text*>("count");
    
    updateStars();
    
    Sprite* clockBG = mainPanel->getChildByName<Sprite*>("clockBGSprite");
    
    CircularMaskNode *node = CircularMaskNode::create();
    node->setPosition(Vec2(clockBG->getContentSize().width/2,clockBG->getContentSize().height/2));
    clockBG->addChild(node);
    
    Sprite *sprite = Sprite::create("guiImage/ui_continue_timer_2.png");
    //    sprite->setScale(0.5);
    //    sprite->setContentSize(sprite->getContentSize() * 0.5);
    node->addChild(sprite);
    
    mClockSprite = node;
    mClockSprite->updateAngleByPercent(100);
//	Node *centerPanel = mainPanel->getChildByName("centerPanel");
//	ViewHelper::setWidgetSwallowTouches(centerPanel, false);
//	
//	mResumeButton = (Button *) centerPanel->getChildByName("resumeButton");
//	mHomeButton = (Button *) centerPanel->getChildByName("homeButton");
    mMapInfoText = mainPanel->getChildByName<Text*>("mapInfo");
    mMapInfoText->setVisible(GameManager::instance()->getUserData()->isGmMode());
}

void GameContinueDialog::updateMapInfo()
{
    //if(GameManager::instance()->getUserData()->isGmMode()
    if(mMapInfoText == nullptr || mMapInfoText->isVisible() == false) {
        return;
    }
    
    std::string info = GameWorld::instance()->getMapLogic()->infoMap();
    mMapInfoText->setString(info);
}

void GameContinueDialog::onWatchAdClicked()
{
	if(playVideoAd() == false) {
        mCounterActive = true;
		ViewHelper::showFadeAlert(this, "Sorry, can't play Video now. Try later.", 500);
    //    mAlertLayer->setVisible(true);
	}
}

bool GameContinueDialog::playVideoAd()
{
	 mCounterActive = false;
	
	// Android and iOS ad colony behaviour
	//	in IOS, sequence is rewarded -> finished
	//	in Android, sequence is finished -> rewarded
	
	// Note: We can use Reward for Revive, because it has a daily cap!!
	// Show Video Ad
	bool isOkay = AdManager::instance()->playVideoAd(AdManager::Ad::AdGameContinue, [&](bool isOkay){
		doCallback(GameContinueOptionWatchAd);
//		if(mAlertLayer != nullptr) {
//			mAlertLayer->setVisible(false);
//		}
        mCounterActive = true;
	});
	
	
//	AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
//		doCallback(GameContinueOptionWatchAd);
//        mCounterActive = true;
//	});
//	
//	AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
//		
//	});
//	
//	bool isOkay = AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
	
	Analytics::instance()->logGameContinue("continue_with_ad", 2200);	// 15 stars
	
	return isOkay;
}

void GameContinueDialog::setCallback(const GameContinueCallback &callback)
{
	mCallback = callback;
}


void GameContinueDialog::updateStars()
{
    int totalStars = GameManager::instance()->getMoneyValue(MoneyTypeDiamond);
    mTotalStarCountText->setString(StringUtils::format("%d",totalStars));
}

void GameContinueDialog::showAddCoinDialog()
{
//	NotEnoughCoinDialog *dialog = NotEnoughCoinDialog::create();
//	addChild(dialog);
//	
//	dialog->setCloseCallback([&](bool isUpdated) {
//		if(isUpdated) {
//			GameWorld::instance()->updateCoins();
//		}
//	});
    CoinShopSceneLayer* layer = CoinShopSceneLayer::create();
    layer->setAsScene();
    getParent()->addChild(layer);
    layer->setCloseCallback([=](){
        mCounterActive = true;
        updateStars();
    });
    mCounterActive = false;
}


#pragma mark - Game Continue Star Requirement
//public:
//CC_SYNTHESIZE(int, mReviveCost, ReviveCost);
//private:
//bool haveEnoughCoin();
//
void GameContinueDialog::onReviveWithCoin()
{
	if(haveEnoughCoin() == false) {
		showAddCoinDialog();
		return;
	}
	
	int value = getReviveCost();
	
	// Deduce Total Money
    GameManager::instance()->getUserData()->addDiamond(-value);
	
	//Analytics::instance()->logGameContinue("continue_with_star", value);
	Analytics::instance()->logConsume(Analytics::act_continue_game, "", value, MoneyTypeDiamond);	// will auto fill the planet name
	
	// Callback to tell the game is ready to revive
	doCallback(GameContinueOptionUseStar);
}

bool GameContinueDialog::haveEnoughCoin()
{
	int currentValue = GameManager::instance()->getUserData()->getTotalDiamond();
	
	return currentValue >= getReviveCost();
}

void GameContinueDialog::setReviveCost(Price price)
{
    mReviveCost = price.amount;
    
	mStarCostText->setString(StringUtils::format("%d", mReviveCost));
//	mCostText->setString(StringUtils::format("Use %d", mReviveCost));
}

int GameContinueDialog::getReviveCost()
{
//	int currentCoin = GameManager::instance()->getUserData()->getTotalCoin();
//	if(
	return mReviveCost;
    
}


void GameContinueDialog::hideWatchAd()
{
	mWatchAdButton->setVisible(false);
	
	// move the useStar Button to center
	float y = mMainPanel->getContentSize().height / 2;
	
	mUseStarButton->setPositionY(y);
}
