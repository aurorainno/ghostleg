//
//  PlanetSelectionView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#ifndef PlanetSelectionView_hpp
#define PlanetSelectionView_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include "PlanetItemView.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class AILPageView;
class PlanetItemView;

class PlanetSelectionView : public Layout
{
public:
	typedef std::function<void(Ref *, int plant, bool isRepeat, int currentPage, int maxPage)> SelectPlanetCallback;
    typedef std::function<void(Vec2 scrollOffset)> ViewScrollCallback;
    typedef std::function<void()> ClickUnlockedPlanetCallback;
public:
	CREATE_FUNC(PlanetSelectionView);
	
	PlanetSelectionView();
	
	//
	virtual bool init();
	
	PlanetItemView *getPlanetItemViewAtPage(int page);
	
	void setSelectPlanetCallback(const SelectPlanetCallback &callback);
    void setOnScrollCallback(const ViewScrollCallback &callback);
    void setOnUnlockedPlanetClickCallback(const ClickUnlockedPlanetCallback &callback);
	
	void setSelectedPlanet(int planetID);
	
	void updatePlanetData();
	
	int getSelectedPlanetID();
    
    void scrollToNextPage();
    void scrollToPreviousPage();
    void scrollToPage(int page);
    
    void onPlanetViewClick(int planetID);
	
    int getPageForPlanet(int planetID);
    
    int getCurrentPage();
    int getLastPage();
    
private:
	void setupPlanets();
	void setupPageView();
	
	void updatePlanetItemView(PlanetItemView *view);
	
	void onPageSelected(int page, bool callback = true);
	
private:
  
	AILPageView *mPageView;
	Vector<PlanetItemView *> mPlanetViewList;
	int mSelectedPlanet;
	
	SelectPlanetCallback mSelectPlanetCallback;
    ViewScrollCallback mScrollCallback;
    ClickUnlockedPlanetCallback mClickCallback;
};




#endif /* PlanetSelectionView_hpp */
