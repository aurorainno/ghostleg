//
//  NpcDistanceLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#include "NpcDistanceLayer.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "VisibleRect.h"
#include "NPCOrder.h"

const Color3B kInActiveColor = Color3B::GRAY;
const Color3B kActiveColor = Color3B::RED;

const float kTransitDuration = 0.5f;
const float kShowUpDuration = 2.0f;

const std::string kPassedDotImage = "guiImage/ingame_waypoint_id_on.png";
const std::string kUnPassDotImage = "guiImage/ingame_waypoint_id_off.png";

NpcDistanceLayer::NpcDistanceLayer()
: Layer()
, mMainNode(nullptr)
, mTimeline(nullptr)
, mProgressLine(nullptr)
, mOnCloseCallback(nullptr)
{

}


bool NpcDistanceLayer::init()
{

	if(Layer::init() == false) {
		return false;
	}

	// GUI Position
	float topY = VisibleRect::top().y;
	mHidePos = Vec2(0, topY+10);
	mShowPos = Vec2(0, topY-100); // Director::getInstance()->convertToGL(Vec2(0, 10));

	
	//
	mFirstDotPos = 30;
	int lastDotPos = 320 - 30;
	mTotalLen = lastDotPos - mFirstDotPos;
	
	
	std::string csb = "gui/NpcDistanceLayer.csb";
	setupGUI(csb);
	//setupAnimation(csb);
	
	// Update the initial position (was hidden)
	setPosition(mHidePos);

	return true;
}

void NpcDistanceLayer::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	mMainNode = rootNode;

	//FIX_UI_LAYOUT(rootNode);
	mProgressLine = mMainNode->getChildByName("progressBarImage");
	//dotSprite->setLocalZOrder(90);
	
	Node *textPanel = mMainNode->getChildByName("textPanel");
	textPanel->setLocalZOrder(101);
	
	
	mNpcNode = mMainNode->getChildByName("npcNode");
	mNpcNode->setLocalZOrder(100);
	mNpcIcon = mNpcNode->getChildByName<Sprite *>("npcIcon");
	
}


void NpcDistanceLayer::runAnimation(const std::string &name)
{
	if(mTimeline) {
		mTimeline->play(name, false);
	}
}

void NpcDistanceLayer::setupAnimation(const std::string &csb)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csb);
	runAction(timeline);		// retain is done here!

	mTimeline = timeline;

	mTimeline->setFrameEventCallFunc([&](Frame *frame) {
		EventFrame *event = dynamic_cast<EventFrame*>(frame);
		if(!event) {
			return;
		}

	});
}



void NpcDistanceLayer::setCurrentSpot(int servedNpc, int npcIcon)
{
	
	// define the red line length
	int completeDistance = (servedNpc + 1) * mSpacing;
	
	Size size = mProgressLine->getContentSize();
	size.width = completeDistance * 2;
	
	mProgressLine->setContentSize(size);
	
	//
	for(int i=0; i<servedNpc; i++) {
		Sprite *dot = dotArray.at(i);
		setDotSprite(dot, true);
	}
	
	// Set the NpcIcon  (position)
	float newX = mFirstDotPos + completeDistance;
	mNpcNode->setPositionX(newX);
	
	// Set the NpcIcon  (icon)
	mNpcIcon->setTexture(StringUtils::format("npc/npc_icon_%03d.png", npcIcon));
}

void NpcDistanceLayer::setDotSprite(Sprite *sprite, bool flag)
{
	if(sprite == nullptr) {
		return;
	}
	
	std::string imageName = flag ? kPassedDotImage : kUnPassDotImage;
	
	sprite->setTexture(imageName);
}

void NpcDistanceLayer::setupDots(int numNpc)
{
	// Remove the old dots
	for(Sprite *dotSprite : dotArray)
	{
		dotSprite->removeFromParent();
	}
	
	//
	dotArray.clear();
	
	
	
	//
	mNumNpc = numNpc;
	mSpacing = mTotalLen / (mNumNpc + 1);
	
	Sprite *firstDot = mMainNode->getChildByName<Sprite *>("firstDot");
	Vec2 firstPos = firstDot->getPosition();
	Vec2 pos = Vec2(mFirstDotPos, firstPos.y);

	for(int i=0; i<numNpc; i++) {		// including the last dot
		pos.x += mSpacing;
		
		Sprite *dotSprite = Sprite::create(kUnPassDotImage);
		dotSprite->setAnchorPoint(Vec2(0.5, 0.5));
		dotSprite->setScale(0.3);
		dotSprite->setPosition(pos);
		mMainNode->addChild(dotSprite);
		dotSprite->setLocalZOrder(90);
		
		dotArray.pushBack(dotSprite);
	}
	
//	Sprite *lastDot = mMainNode->getChildByName<Sprite *>("lastDot");	// it is for reference
//	lastDot->setVisible(false);
}

void NpcDistanceLayer::onEnter()
{
	Layer::onEnter();
}


void NpcDistanceLayer::hide()
{
	setPosition(mHidePos);
}

void NpcDistanceLayer::showUp(float duration)
{
	// Hide it first
	hide();
	
	// Define the Animation
	ActionInterval *moveIn = EaseBackOut::create(MoveTo::create(kTransitDuration, mShowPos));
	DelayTime *delay = DelayTime::create(duration);
	ActionInterval *moveOut = EaseBackIn::create(MoveTo::create(kTransitDuration, mHidePos));
	
	CallFunc *callFunc = CallFunc::create([&]{
		if(mOnCloseCallback) {
			mOnCloseCallback();
		}
	});
	
	
	Sequence *sequence = Sequence::create(moveIn, delay, moveOut, callFunc, nullptr);
	
	
	// Run the Action
	runAction(sequence);
	
	
}


void NpcDistanceLayer::showNpcOrder(NPCOrder *order)
{
	//log("DEBUG: showNpcOrder: %s", order->toString().c_str());
	if(order == nullptr) {	// something wrong
		return;
	}

	setCurrentSpot(order->getOrderIndex(), order->getNpcID());
	
	showUp(kShowUpDuration);
}


void NpcDistanceLayer::setOnCloseCallback(const std::function<void()> &callback)
{
	mOnCloseCallback = callback;
}
