//
//  TutorialDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/4/2016.
//
//

#include "PauseDialog.h"
#include "cocostudio/CocoStudio.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "AdManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "NpcMapLogic.h"
#include "Analytics.h"

using namespace cocostudio::timeline;


// on "init" you need to initialize your instance
bool PauseDialog::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
	{
		return false;
	}


	// Setup UI
	Node *rootNode = CSLoader::createNode("PauseDialog.csb");
	addChild(rootNode);

	setupUI(rootNode);
	
	
    
	return true;
}

void PauseDialog::onEnter(){
    Layer::onEnter();
	
 //   AdManager::instance()->showBannerAd("Banner");
	
	updateMapInfo();
	
	LOG_SCREEN(Analytics::scn_pause);
}

void PauseDialog::onExit(){
    AdManager::instance()->hideBannerAd();
    Layer::onExit();
}

void PauseDialog::closeDialog()
{
	this->removeFromParent();
}

void PauseDialog::updateMapInfo()
{
	//if(GameManager::instance()->getUserData()->isGmMode()
	if(mMapInfoText == nullptr || mMapInfoText->isVisible() == false) {
		return;
	}
	
	std::string info = GameWorld::instance()->getMapLogic()->infoMap();
	mMapInfoText->setString(info);
}

void PauseDialog::setupUI(cocos2d::Node *mainPanel)
{
	FIX_UI_LAYOUT(mainPanel);
	
	Node *centerPanel = mainPanel->getChildByName("centerPanel");
	ViewHelper::setWidgetSwallowTouches(centerPanel, false);
	
	mResumeButton = (Button *) centerPanel->getChildByName("resumeButton");
	mHomeButton = (Button *) centerPanel->getChildByName("homeButton");
	
	mMapInfoText = mainPanel->getChildByName<Text *>("mapInfoText");
	mMapInfoText->setVisible(GameManager::instance()->getUserData()->isGmMode());
	
	
//    Node *upperPanel = mainPanel->getChildByName("topPanel");
//    Text *starText = (Text*) upperPanel->getChildByName("coinText");
//    
//    int coin = GameManager::instance()->getUserData()->getTotalCoin();
//    std::string coinStr = StringUtils::format("%d", coin);
//    starText->setString(coinStr);
    
}


void PauseDialog::setResumeListener(const Widget::ccWidgetClickCallback &callback)
{
	if(mResumeButton) {
		mResumeButton->addClickEventListener(callback);
	} else {
		log("mResumeButton is null");
	}
}

void PauseDialog::setBackHomeListener(const Widget::ccWidgetClickCallback &callback)
{
	if(mHomeButton) {
		mHomeButton->addClickEventListener(callback);
	} else {
		log("mHomeButton is null");
	}
}


void PauseDialog::setEndGameListener(const Widget::ccWidgetClickCallback &callback)
{
	if(mHomeButton) {
		mHomeButton->addClickEventListener(callback);
	} else {
		log("mHomeButton is null");
	}
}
