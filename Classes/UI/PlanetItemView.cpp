//
//  PlanetItemView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#include "PlanetItemView.h"
#include "VisibleRect.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "PlanetData.h"
#include "PlanetManager.h"
#include "DogManager.h"

const Color4B kTextColorUnlock = Color4B(255, 255, 255, 255);
const Color4B kTextColorLocked = Color4B(184, 184, 184, 255);
const Color4B kOutlineColorUnlock = Color4B(8, 114, 79, 255);
const Color4B kOutlineColorLocked = Color4B(56, 56, 174, 255);

namespace {
	void updateTextColor(Text *text, bool isLocked)
	{
		Color4B textColor = isLocked ? kTextColorLocked : kTextColorUnlock;
		Color4B outlineColor = isLocked ? kOutlineColorLocked : kOutlineColorUnlock;
		
		text->setTextColor(textColor);
		text->enableOutline(outlineColor, 5);
	}
}

PlanetItemView::PlanetItemView()
: Layout()
, mPlanetSprite(nullptr)
, mNameSprite(nullptr)
, mInfoText(nullptr)
, mIsLocked(true)
, mBestDistance(0)
, mPlanetID(1)
, mPlanetNode(nullptr)
, mPlanetAction(nullptr)
{
	
}


bool PlanetItemView::init()
{
	
	if(Layout::init() == false) {
		return false;
	}
	setSizeType(Widget::SizeType::ABSOLUTE);
	//setContentSize(VisibleRect::getVisibleRect().size);	// OLD GUI
	//setContentSize(Size(160, 160));	// OLD GUI
	
	//ViewHelper::setSolidBackground(this, Color4B::GREEN);
	
	setupGUI("PlanetItemView.csb");
	//setupGUI("StageItemView.csb");
	
	// this view should capture the touch
	setTouchEnabled(false);
	return true;
}


void PlanetItemView::changeContentSize(const Size &newSize)
{
	setContentSize(newSize);
	FIX_UI_LAYOUT(mRootNode);
}

void PlanetItemView::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	mRootNode = rootNode;
	
	setContentSize(mRootNode->getContentSize());
	// FIX_UI_LAYOUT(mRootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	mMainPanel = mainPanel;

	
	
	setupNameGUI();
	
	//
    mTouchPanel = (Layout*)rootNode->getChildByName("touchPanel");
    mTouchPanel->addClickEventListener([&](Ref*){
        if(mCallback){
            mCallback(mPlanetID);
        }
    });
    
	
	mLockPanel = mainPanel->getChildByName("lockPanel");
	mUnlockPanel = mainPanel->getChildByName("unlockPanel");
	
	
	mPlanetSprite = (Sprite *) mainPanel->getChildByName("planetSprite");
	mNameSprite = (Sprite *) mainPanel->getChildByName("nameSprite");
	mInfoText = (Text *) mUnlockPanel->getChildByName("infoText");
	mUnlockInfoText = (Text *) mLockPanel->getChildByName("infoText");
	
	// Planet Node Position (and hide the reference node)
	// will add to it later!
	mPlanetNodeParent = mainPanel->getChildByName("planetNode");
	Node *refNode = mPlanetNodeParent->getChildByName("referenceNode");
	if(refNode) { refNode->removeFromParent(); }
	
	mPlanetNode = nullptr;
	log("DEBUG: csbPos=%f,%f", mPlantCsbPos.x, mPlantCsbPos.y);
}

void PlanetItemView::setupNameGUI()
{
	Node *namePanel = mMainPanel->getChildByName("namePanel");
	
	mStageText = namePanel->getChildByName<Text *>("stageText");
	mStageNameText = namePanel->getChildByName<Text *>("stageNameText");
}

void PlanetItemView::setupPlanet(int planetID, int bestDistance, bool isLocked)
{
	mPlanetID = planetID;
	mBestDistance = bestDistance;
	mIsLocked = isLocked;
	
	//
	updatePlanetVisual();	// CSB & Planet NAme
//	updateInfoText();
//	setUnlockText();
//	setIsLocked(mIsLocked);
}

void PlanetItemView::updatePlanetVisual()
{

	// Update the stage images
	std::string csbFile = StringUtils::format("stage/stage_gui_%03d.csb", mPlanetID);
	setPlanetCsb(csbFile, mIsLocked);
	
	// Update the name
	updateStageName();
	
}
										   


void PlanetItemView::updateStageName()
{
//	PlanetData *data = PlanetManager::instance()->getPlanetData(mPlanetID);
//	std::string name = data == nullptr ? "unknown" : data->getName();

	std::string stage = StringUtils::format("Stage %d", mPlanetID);
	std::string name = PlanetManager::instance()->getPlanetName(mPlanetID);
	
	if(mStageText) {
		mStageText->setString(stage);
		updateTextColor(mStageText, mIsLocked);
	}
	if(mStageNameText) {
		mStageNameText->setString(name);
		updateTextColor(mStageNameText, mIsLocked);
	}
	
	
}

void PlanetItemView::updateTextColorForLockState(bool isLocked)
{
	if(mStageText) {
		updateTextColor(mStageText, mIsLocked);
	}
	if(mStageNameText) {
		updateTextColor(mStageNameText, mIsLocked);
	}
}

void PlanetItemView::setPlanet(int planetID)
{
	mPlanetID = planetID;

	updatePlanetVisual();
}

void PlanetItemView::setBestDistance(int distance)
{
	mBestDistance = distance;
	
	updateInfoText();
}

void PlanetItemView::setIsLocked(bool flag)
{
	mIsLocked = flag;
	
	// Control the visible of the panel
	mLockPanel->setVisible(mIsLocked);
	mUnlockPanel->setVisible(mIsLocked == false);
	
	// updateInfoText();
	setPlanetCsbAnimationForLockState(mIsLocked);
	// updatePlanetSprite();
}


void PlanetItemView::updateInfoText()
{
	std::string info;
	
	//
	std::string unlockHint = DogManager::instance()->getNextUnlockHint();
	if(unlockHint == "") { // no hint, show best distance instead
		info = StringUtils::format("Best distance\n%d metres", mBestDistance);
	} else {
		info = unlockHint;
	}
	
	mInfoText->setString(info);
}

void PlanetItemView::setUnlockText()
{
	int count = PlanetManager::instance()->getUnlockedDogs(mPlanetID);
	int reqCount = 5;
	
	PlanetData *data = PlanetManager::instance()->getPlanetData(mPlanetID);
	std::string name = data == nullptr ? "unknown" : data->getName();
	
	std::string info;
	info = StringUtils::format("Complete the %s team to unlock\n%d/%d members",
							   name.c_str(), count, reqCount);
	mUnlockInfoText->setString(info);
}


void PlanetItemView::setCallback(std::function<void (int)> callback)
{
    mCallback = callback;
}

int PlanetItemView::getPlanetID()
{
	return mPlanetID;
}


void PlanetItemView::testPlanetCsb(const std::string &name, bool isLocked)
{
	setPlanetCsb(name, isLocked);
}

void PlanetItemView::setPlanetCsb(const std::string &csbName, bool isLocked)
{
	if(mPlanetNode) {	// Clean up the old planet node first
		mPlanetNode->removeFromParent();
		mPlanetNode = nullptr;
	}
	
	// Setup the Csb Node
	Node *newNode = CSLoader::createNode(csbName);
	if(newNode == nullptr) {
		return;
	}
	
	// Scaling
    float scale = 1.0;
    newNode->setScale(scale);
	Size size = newNode->getContentSize()*scale;
	
	newNode->setPosition(Vec2(-size.width/2, -size.height/2));
	mPlanetNodeParent->addChild(newNode);
	
	
	mPlanetNode = newNode;
	
	// Setup Animation
	ActionTimeline *timeline = CSLoader::createTimeline(csbName);
	if(timeline == nullptr) {
		log("PlanetItemView. cannot create the timeLine: %s", csbName.c_str());
		return;
	}
	timeline->setTimeSpeed(0.3f);
	
	setPlanetAction(timeline);		// retain take place in the setter
	mPlanetNode->runAction(timeline);

	
	// Modify the state
	setPlanetCsbAnimationForLockState(isLocked);
}

void PlanetItemView::setPlanetCsbAnimation(const std::string &name)
{
	if(mPlanetNode == nullptr) {
		return;
	}
	
	ActionTimeline *timeline = getPlanetAction();
	if(timeline == nullptr) {
		return;
	}
	
	timeline->play(name, false);
	
}

void PlanetItemView::setPlanetCsbAnimationForLockState(bool isLocked)
{
	std::string name = isLocked ? "locked" : "unlock";
	
	setPlanetCsbAnimation(name);
}


std::string PlanetItemView::getPlanetNameImage(bool isLocked)
{
	std::string suffix = "";
	
	if(mPlanetID > 1 && isLocked) {
		suffix = "_l";
	}
	
	return StringUtils::format("planet/planet_%03d_name%s.png", mPlanetID, suffix.c_str());
}


void PlanetItemView::unlockPlanet(const std::function<void()> &callback)
{
	std::string animeName = "active";
	mPlanetAction->setTimeSpeed(2.5f);
	mPlanetAction->setAnimationEndCallFunc(animeName, [=]{
		setPlanetCsbAnimation("unlock");
		
		if(callback != nullptr) {
			callback();
		}
	});
	
	setPlanetCsbAnimation("active");
}
