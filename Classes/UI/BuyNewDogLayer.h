//
//  BuyNewDogLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 1/11/2016.
//
//

#ifndef BuyNewDogLayer_hpp
#define BuyNewDogLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class DogData;

class BuyNewDogLayer : public Layer
{
public:
    typedef std::function<void()> CallbackFunc;
    
    CREATE_FUNC(BuyNewDogLayer);
    
    virtual bool init() override;
    
    void setupUI(Node* node);
    
    void setDog(int dogID);
    void setCallback(CallbackFunc callback);
	
private:
	void attachFireworkEffect(Node *mainNode);
	
private:
    Button* mCancelBtn;
    Button* mOKBtn;
    Text* mDogNameText;
    Sprite* mDogIcon;
    Text* mStarText;
    CallbackFunc mCallback;
    
};
#endif /* BuyNewDogLayer_hpp */
