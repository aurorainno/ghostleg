//
//  BuyNewDogLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 1/11/2016.
//
//

#include "BuyNewDogLayer.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "DogManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "DogData.h"
#include "GameSound.h"
#include "StageParallaxLayer.h"
#include "AnimationHelper.h"
#include "FireworkLayer.h"

bool BuyNewDogLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    
    GameSound::playSound(GameSound::Unlock);
    Node *rootNode = CSLoader::createNode("BuyNewDogLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

void BuyNewDogLayer::setupUI(cocos2d::Node *node)
{
    FIX_UI_LAYOUT(node);
    mStarText = node->getChildByName<Text*>("totalCoinText");
    char temp[100];
    sprintf(temp, "%d", GameManager::instance()->getUserData()->getTotalCoin());
    mStarText->setString(temp);

    // Setup the space background
    ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
    addChild(parallaxLayer);
    parallaxLayer->scheduleUpdate();

	attachFireworkEffect(node);
	
    Node* mainPanel = node->getChildByName("mainPanel");
    
    mDogIcon = mainPanel->getChildByName<Sprite*>("dogSprite");
    mDogNameText = mainPanel->getChildByName<Text*>("dogName");
    
    mOKBtn = mainPanel->getChildByName<Button*>("okBtn");
    mOKBtn->addClickEventListener([&](Ref*){
        if(mCallback){
            mCallback();
            GameSound::stopSound(GameSound::Unlock);
        }
    });
	
	//
	AnimationHelper::addAuraRotateAction(node, 40, 2);
}

void BuyNewDogLayer::setCallback(CallbackFunc callback){
    mCallback = callback;
}

void BuyNewDogLayer::setDog(int dogID)
{
    DogData* data = DogManager::instance()->getDogData(dogID);
    
    int resID = data->getAnimeID();
    std::string spriteName = StringUtils::format("dog/dselect_dog%d_unlock.png", resID);
    mDogIcon->setTexture(spriteName);
    
    mDogNameText->setString(data->getName());
}



void BuyNewDogLayer::attachFireworkEffect(Node *mainNode)
{

	// Add the firework
	Node *attachNode = mainNode->getChildByName("fireworkNode");
	
	if(attachNode->isVisible()) {
		FireworkLayer *firework = FireworkLayer::create();
		firework->setAnchorPoint(Vec2(0.5, 0.5));
		attachNode->addChild(firework);
		firework->startFirework();
	}
	
}




