//
//  TutorialDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/4/2016.
//
//

#ifndef TutorialDialog_hpp
#define TutorialDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;


class TutorialView : public LayerColor
{
public:
	CREATE_FUNC(TutorialView);
	
	typedef std::function<void(Ref*)> EndTutorialCallback;

	void setCallback(const EndTutorialCallback &callback);
	
	//
	virtual bool init();
	
	//	virtual void onEnter();
private:
	void setupTouchListener();
	void setupUI(Node *mainPanel);
	void closeDialog();
	void setupTutorialPage();
	bool isEndOfPage();
	void showNextPage();
	void showPage(int page);
	void handleTutorialEnd();
	void doEndTutorialCallback();
	
private:
	int mNumPage;
	int mPageIndex;
	Sprite *mTutorialSprite;
	EndTutorialCallback mEndCallback;
};

#endif /* TutorialDialog_hpp */
