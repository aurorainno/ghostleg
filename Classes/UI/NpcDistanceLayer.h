//
//  NpcDistanceLayer.hpp
//  GhostLeg
//
//  Generated by Code Generator 'createCocosLayer'
//
//

#ifndef NpcDistanceLayer_hpp
#define NpcDistanceLayer_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class NPCOrder;

class NpcDistanceLayer : public Layer
{
public:
	CREATE_FUNC(NpcDistanceLayer);

	NpcDistanceLayer();
	CC_SYNTHESIZE(Vec2, mHidePos, HidePos);		// note: anchor point at left_bottom
	CC_SYNTHESIZE(Vec2, mShowPos, ShowPos);		// note: anchor point at left_bottom

	virtual bool init() override;

	virtual void onEnter() override;

	void setupDots(int numNpc);
	void setCurrentSpot(int servedNpc, int npcIcon);

	void hide();
	void showUp(float duration=1.0f);
	void showNpcOrder(NPCOrder *order);
	void setOnCloseCallback(const std::function<void()> &callback);
	
	
private:
	Node *mMainNode;
	Node *mProgressLine;
	Vector<Sprite *> dotArray;
	Node *mNpcNode;
	Sprite *mNpcIcon;
	std::function<void ()> mOnCloseCallback;

#pragma mark - GUI & Animation
public:
	void runAnimation(const std::string &name);

private:
	void setupGUI(const std::string &csbName);
	void setupAnimation(const std::string &csb);
	void setDotSprite(Sprite *sprite, bool flag);
	
private:		// data
	cocostudio::timeline::ActionTimeline *mTimeline;

	
	
private:
	int mNumNpc;
	int mTotalLen;
	float mSpacing;
	int mFirstDotPos;
	
//
};




#endif /* LuckyDrawAnime_hpp */
