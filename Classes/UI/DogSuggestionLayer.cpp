//
//  DogSuggestionLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/11/2016.
//
//

#include "DogSuggestionLayer.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "DogSelectItemView.h"
#include "DogManager.h"
#include "DogData.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "BuyNewDogLayer.h"
#include "GameUI.h"
#include "CoinShopScene.h"
#include "Analytics.h"

DogSuggestionLayer::DogSuggestionLayer()
: Layer()
, mMainPanel(nullptr)
, mUnlockInfoView(nullptr)
, mSelectedDog(1)
, mStarValueNode(nullptr)
, mCallback(nullptr)
{
	
}


bool DogSuggestionLayer::init()
{
	
	if(Layer::init() == false) {
		return false;
	}
	
	setupGUI("DogSuggestionLayer.csb");
	
	return true;
}

void DogSuggestionLayer::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	
	FIX_UI_LAYOUT(rootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	mMainPanel = mainPanel;

	// Dog Detail View
	Node *dogDetailNode = mainPanel->getChildByName("dogDetailNode");
	dogDetailNode->setVisible(false);		// the given one is just for reference
	Vec2 dogDetailPos = dogDetailNode->getPosition();
	
	//
	DogSelectItemView *itemView = DogSelectItemView::create();
	itemView->setPosition(dogDetailPos);
	itemView->setDogForSuggestion(3);		// temporailty
	mMainPanel->addChild(itemView);
	mDogInfo = itemView;

	//
	// Buy Button
	mUnlockButton = mainPanel->getChildByName<Button *>("unlockButton");
	mUnlockButton->addClickEventListener([&](Ref *sender){
		unlockDog();
	});
	
	//
	
	// Close Button
	Button *closeButton = mainPanel->getChildByName<Button *>("closeButton");
	closeButton->addClickEventListener([&](Ref *sender){
		doCallback(false);
		closeDialog();
	});
	
	// StarValue
	mStarValueNode = rootNode->getChildByName("starValueNode");
	
	updateStarValue();
}

void DogSuggestionLayer::closeDialog()
{
	removeFromParent();
}

void DogSuggestionLayer::setDog(int dogID)
{
	if(mDogInfo) {
		mDogInfo->setDogForSuggestion(dogID);
	}
	
	// update the price
	DogData *dogData = DogManager::instance()->getDogData(dogID);
	mPrice = dogData == nullptr ? 99999 : dogData->getPrice();
	
	mSelectedDog = dogID;
	
	//
	if(mUnlockButton) {
		std::string title = StringUtils::format("%-10d to unlock", mPrice);
		mUnlockButton->setTitleText(title);
	}
}

void DogSuggestionLayer::unlockDog()
{
	int currentStar = GameManager::instance()->getUserData()->getTotalCoin();
	//log("currentStar=%d mPrice=%d", currentStar, mPrice);
	if(currentStar < mPrice) {
		showCoinShop();
		return;
	}
	
	//
	unlockDogByStar();
}

void DogSuggestionLayer::unlockDogByStar()
{
	log("unlockDog by star");

	// Do the data logic
	DogManager::instance()->unlock(mSelectedDog);
	GameManager::instance()->getUserData()->addCoin(-mPrice);
	
	// event tracking
	std::string dogName = DogManager::instance()->getDogName(mSelectedDog);
	
	// Show the unlock View
	BuyNewDogLayer *newDogLayer = BuyNewDogLayer::create();
	newDogLayer->setDog(mSelectedDog);
	addChild(newDogLayer);
	mUnlockInfoView = newDogLayer;
	newDogLayer->setCallback([&]{
		onUnlockDone();
	});

	
	
	// Hide the mainPanels
	mMainPanel->setVisible(false);
	mStarValueNode->setVisible(false);
	//closeDialog();
}

void DogSuggestionLayer::onUnlockDone()
{
	doCallback(true);
	closeDialog();
}


void DogSuggestionLayer::showCoinShop()
{
	log("redirect to shop");
	auto layer = CoinShopSceneLayer::create();
	addChild(layer);

	layer->setCloseCallback([&]() {
		updateStarValue();
	});
	
}


void DogSuggestionLayer::updateStarValue()
{
	GameUI::updateStarValue(mStarValueNode);
}

void DogSuggestionLayer::doCallback(bool didUnlock)
{
	if(mCallback) {
		mCallback(didUnlock);
	}
	
}

void DogSuggestionLayer::setCallback(const DogSuggestionLayerCallback &callback)
{
	mCallback = callback;
}
