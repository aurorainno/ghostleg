//
//  DogInfoLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 1/11/2016.
//
//

#ifndef DogInfoLayer_hpp
#define DogInfoLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "CommonType.h"

USING_NS_CC;
using namespace cocos2d::ui;

class RichTextLabel;



class DogInfoLayer : public Layer
{
public:
    typedef std::function<void()> OnCancel;
    typedef std::function<void(Ref *)> OnPurchase;
    typedef std::function<void()> OnSelect;
    
    CREATE_FUNC(DogInfoLayer);
	
	DogInfoLayer();
	
    virtual bool init() override;
    
    void setupUI(Node* node);
    
    void setActive(int activeID,int activeLevel);
    void setPassive(int passiveID, int passiveLevel);
    void setPrice(int price);
	
	
	void setMoneyType(MoneyType type);
	
	
    void setPurchaseCallBack(OnPurchase callback);
    void setCancelCallBack(OnCancel callback);
    void setSelectCallBack(OnSelect callback);
    
    void setupBtnVisibility();
    
    void setDog(int dogID);
    
    void updateStar();
	void updateMoney();
    
    virtual void setVisible(bool isVisible) override;
    virtual void onEnter() override;
    
private:
	void setSkillInfoLabel();
    void setLevelBar(Node* panel, int level);
	void configRichTextLabel(RichTextLabel *label);
	
private:
    Button* mCancelBtn;
    Button* mPurchaseBtn;
    Button* mSelectBtn;
    Button* mInUseBtn;
    Node* mActivePanel;
    Node* mPassivePanel;
    OnPurchase mPurchaseCallBack;
    OnCancel mCancelCallBack;
    OnSelect mSelectCallBack;
    Text* mStarText;
	Sprite *mMoneyIcon;
	Sprite *mButtonMoneyIcon;
	
	RichTextLabel *mPassiveInfoLabel;	// info label of passive skill
	RichTextLabel *mActiveInfoLabel;	// info label of active skill

    
    int mDogID;
    std::map<int,int> mMasteryMap;
    int mSuitID, mSuitLevel;
	MoneyType mMoneyType;
};

#endif /* DogInfoLayer_hpp */
