//
//  TutorialDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/4/2016.
//
//

#include "TutorialView.h"
#include "cocostudio/CocoStudio.h"
#include "ViewHelper.h"
#include "CommonMacro.h"

using namespace cocostudio::timeline;


// on "init" you need to initialize your instance
bool TutorialView::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !LayerColor::initWithColor(Color4B(0, 0, 0, 0)))
	{
		return false;
	}
	
	mTutorialSprite = nullptr;
	
	//addTouchListener()
	setupTouchListener();
	
	// Setup UI
	Node *rootNode = CSLoader::createNode("TutorialPageLayer.csb");
	addChild(rootNode);
	
	setupUI(rootNode);
	
	setupTutorialPage();
	
	return true;
}

void TutorialView::setupTouchListener()
{
	ViewHelper::setWidgetSwallowTouches(this, true);
	auto listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	
	auto parent = this;
	
	listener->onTouchBegan = [parent](Touch *touch, Event *event) {
		// log("touch received");
		if(parent->isEndOfPage()) {
			parent->handleTutorialEnd();
		} else {
			parent->showNextPage();
		}
		
		return true;
	};
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
}

void TutorialView::closeDialog()
{
	this->removeFromParent();
}

void TutorialView::setupUI(cocos2d::Node *mainPanel)
{
	FIX_UI_LAYOUT(mainPanel);
	
	Node *bgLayer = mainPanel->getChildByName("bgLayer");
	ViewHelper::setWidgetSwallowTouches(bgLayer, false);
	
	mTutorialSprite = (Sprite *) mainPanel->getChildByName("tutorialSprite");
	mTutorialSprite->setScale(0.45f);
}

void TutorialView::setupTutorialPage()
{
	mNumPage = 3;		// hard code is okay here
	
	showPage(0);
}

bool TutorialView::isEndOfPage()
{
	return mPageIndex == mNumPage-1;
}

void TutorialView::showNextPage()
{
	if(isEndOfPage()) {
		return;
	}
	showPage(mPageIndex+1);
}

void TutorialView::showPage(int page)
{
	mPageIndex = page;
	
	char filename[50];
	sprintf(filename, "tutorial/tutorial%d.jpg", page);
	
	mTutorialSprite->setTexture(filename);
}

void TutorialView::handleTutorialEnd()
{
//	this->closeDialog();
	this->doEndTutorialCallback();
}


void TutorialView::setCallback(const EndTutorialCallback &callback)
{
	mEndCallback = callback;
}

void TutorialView::doEndTutorialCallback()
{
	if(mEndCallback != nullptr) {
		mEndCallback(this);
	}
}