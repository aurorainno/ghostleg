//
//  UnlockDogLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 26/10/2016.
//
//

#include "UnlockDogLayer.h"
//#include "DogManager.h"
//#include "DogData.h"
#include "GameWorld.h"
#include "GameScene.h"
#include "GameSound.h"
#include "FireworkLayer.h"
#include "AnimationHelper.h"
#include "PlayerManager.h"
#include "CharGameData.h"

namespace {
    void setScaleTo(Node *node, float initScale, float finalScale, float duration)
    {
        node->setScale(initScale);
        ScaleTo * scaleto = ScaleTo::create(duration, finalScale);
        node->runAction(scaleto);
    }
}

UnlockDogLayer::UnlockDogLayer()
: mCloseCallback(nullptr)
{
	
}

bool UnlockDogLayer::init()
{
    if(!Layer::init()){
        return false;
    }
    
    Node *rootNode = CSLoader::createNode("UnlockDogLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    Node* mainPanel = rootNode->getChildByName("mainPanel");
    setScaleTo(mainPanel,0.1,1.0,0.2);
    
    return true;
}

void UnlockDogLayer::setupUI(cocos2d::Node *node)
{
    FIX_UI_LAYOUT(node);
    Node *mainPanel = node->getChildByName("mainPanel");
	
	// add FireworkLayer in the mainPanel
	attachFireworkEffect(mainPanel);
	
	
	
	//
	
    mDogNameText = (Text*) mainPanel->getChildByName("charName");
//    mProgressText = (Text*) mainPanel->getChildByName("progress");
//    mInfoText = (Text*) mainPanel->getChildByName("info");
    mDogSprite = (Sprite*) mainPanel->getChildByName("charSprite");
    mConfirmBtn = (Button*) mainPanel->getChildByName("okBtn");
}

void UnlockDogLayer::setView(CharGameData *data){
    std::string name = data->getName();
    std::string spriteName = StringUtils::format("playChar/player_icon_%03d.png", data->getCharID());
	
    mDogNameText->setString(name);
    mDogSprite->setTexture(spriteName);

}



void UnlockDogLayer::setDogQueue(const std::queue<int> &dogQueue)
{
    mDogQueue = dogQueue;
}

void UnlockDogLayer::showNextUnlockInfo()
{
	// New Layer
	UnlockDogLayer *newLayer = UnlockDogLayer::create();
	newLayer->setCloseCallback(mCloseCallback);
	newLayer->setDogQueue(mDogQueue);

	// Show the newLayer
	setVisible(false);
	
	Node *parent = getParent();
	parent->addChild(newLayer);

}

void UnlockDogLayer::callbackClose()
{
	if(mCloseCallback) {
		mCloseCallback();
	}
}

void UnlockDogLayer::setConfirmBtnListener()
{
    mConfirmBtn->addClickEventListener([&,this](Ref*){
        GameSound::playSound(GameSound::Button1);
        if(mDogQueue.size() > 0){		// Still have unlock dog
			showNextUnlockInfo();
        }else{
			callbackClose();
        }
        removeFromParent();
    });
}

void UnlockDogLayer::onEnter()
{
    Layer::onEnter();
    int charID = mDogQueue.front();
    mDogQueue.pop();
    
	CharGameData *data = PlayerManager::instance()->getCharGameData(charID);
    setView(data);
    setConfirmBtnListener();
}


void UnlockDogLayer::setCloseCallback(const std::function<void()> &callback)
{
	mCloseCallback = callback;
}


void UnlockDogLayer::attachFireworkEffect(Node *mainNode)
{
	//
	AnimationHelper::addAuraRotateAction(mainNode, 40, 2);
	
	
	// Add the firework
	Node *attachNode = mainNode->getChildByName("fireworkNode");
	
	if(attachNode->isVisible()) {
		FireworkLayer *firework = FireworkLayer::create();
		firework->setAnchorPoint(Vec2(0.5, 0.5));
		attachNode->addChild(firework);
		firework->startFirework();
	}
	
}
