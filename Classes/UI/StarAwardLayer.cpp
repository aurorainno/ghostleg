//
//  StarAwardLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 18/10/2016.
//
//

#include "StarAwardLayer.h"
#include "CommonMacro.h"

#define kStarRollingFinishTime 1
#define kStarRollingRefreshRate 0.05

bool StarAwardLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    
    Node *rootNode = CSLoader::createNode("AwardStarLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

void StarAwardLayer::setupUI(Node* node)
{
    FIX_UI_LAYOUT(node);
    
    mLeftUpperStarText = (Text*) node->getChildByName("coinText");
    
    Node* centerPanel = node->getChildByName("centerPanel");
    
    mCenterStarText = (Text*) centerPanel->getChildByName("coinText");
    
    mOKBtn = (Button*) centerPanel->getChildByName("confirmBtn");
    mOKBtn->addClickEventListener([&,this](Ref* ref){
        log("address:%p,%p",mOKBtn,ref);
        this->removeFromParent();
    });
}

void StarAwardLayer::onEnter()
{
    Layer::onEnter();
    
    updateCoinText(150,300);
}

void StarAwardLayer::updateCoinText(int originalCoins , int targetCoins){
    mCenterStarText->setString(StringUtils::format("%d",targetCoins));
    mLeftUpperStarText->setString(StringUtils::format("%d",originalCoins));
    mAccumCoins = originalCoins;
    
    
  //  float delayTime = (float) 1/(targetCoins - originalCoins);
    auto delay = DelayTime::create(kStarRollingRefreshRate);
    
    float coinsToAdd = ((float)targetCoins - mAccumCoins) * kStarRollingRefreshRate / kStarRollingFinishTime;
    
    auto updateStarText = CallFunc::create([&,this,targetCoins,coinsToAdd](){
        mAccumCoins += coinsToAdd;
        if(mAccumCoins > targetCoins){
            mAccumCoins = targetCoins;
        }

        mLeftUpperStarText->setString(StringUtils::format("%d",(int)mAccumCoins));
    });
    
    auto checkEndOfAction = CallFunc::create([&,this,targetCoins](){
        if(this->mAccumCoins >= targetCoins){
            this->stopAllActions();
        }
    });

    auto sequence = Sequence::create(updateStarText,checkEndOfAction, delay, nullptr);
    runAction(RepeatForever::create(sequence));
}









