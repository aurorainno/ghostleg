//
//  GmCharSelectionDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 5/10/2016.
//
//

#ifndef GmCharSelectionDialog_hpp
#define GmCharSelectionDialog_hpp

#include <stdio.h>

#include <map>
#include <string>
#include "cocos2d.h"

#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

class GameModel;

USING_NS_CC;
using namespace cocos2d::ui;

class GmDogSelectionDialog : public Layer {
public:
	CREATE_FUNC(GmDogSelectionDialog);
	
	virtual bool init();
	void setupGUI(const std::string &csbName);
  

#pragma mark - Change Char Logic
private:
	void changeChar(int idChange);		// idChange should be 1 : -1

	int getNewCharID(int idChange);
	void updateCharDetail();
    
	
    
#pragma mark - Unlock Char
private:
    void resetAllChar();
	void upgradeAllChar();
    void unlockSelectedChar();
	void upgradeSelectedChar();
    void unlockAllChar();
    void updateUnlockCharBtn();
    
private:
    Button *mUnlockBtn;

#pragma mark - Character GUI information
public:
	void updateCharIcon();
	
private:
	Sprite *mLockedIconSprite;
	Sprite *mUnlockedIconSprite;
	
#pragma mark - Character Text information
private:
	void setupText(Node *mainPanel);
    
	void setCharName(const std::string &name);
	void setCharID(int dogID);
    void setCharUnlockState(bool isUnlocked);
	void setCharStatistics();
	
	void updateTextInfo();
	
	void showSpaceBg();
	void hideSpaceBg();
	
private:
	Text *mNameText;
	Text *mCharIDText;
	Text *mStatText;
    Text *mUnlockText;
	
	
#pragma mark - Update Character Animation

public:
	void updateCharacter();
	
private:
	void setupCharacterNode(Node *refNode);
	
private:
	GameModel *mCharModel;
	Node *mSpaceBgNode;
	Node *mMainPanel;
	int mCharID;

	
#pragma mark - Show Unlock Popup
public:
	void showUnlockCharPopup();
	void showBuyCharPopup();

};




#endif /* GmCharSelectionDialog_hpp */
