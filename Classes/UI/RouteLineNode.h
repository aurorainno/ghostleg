//
//  VerticalLineNode.hpp
//  GhostLeg
//
//	Usage:
//		RouteLineNode *line = RouteLineNode::create();
//		line->setShader(....)
//		line->setThickness(0.5f)
//		line->setColor(Color4F(.....));
//		line->setStart(xx);
//		line->settEnd(xx);
//		line->updateLine()
//
//  Created by Ken Lee on 18/10/2016.
//
//

#ifndef VerticalLineNode_hpp
#define VerticalLineNode_hpp

#include <stdio.h>

#include "cocos2d.h"
// #include "extensions/cocos-ext.h"
#include <string>
#include <vector>
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "VisibleRect.h"
USING_NS_CC;


//
class RouteLineNode : public ui::Layout
{
public:
	CREATE_FUNC(RouteLineNode);
	
	RouteLineNode();

	bool init();
	
	CC_SYNTHESIZE(Vec2, mStart, Start);
	CC_SYNTHESIZE(Vec2, mEnd, End);
	CC_SYNTHESIZE(float, mLength, Length);		// Length of the line
	CC_SYNTHESIZE(float, mThickness, Thickness);
	CC_SYNTHESIZE(Color4F, mLineColor, LineColor);
	CC_SYNTHESIZE(bool, mUseDrawNode, UseDrawNode);
	CC_SYNTHESIZE(float, mSegmentScale, SegmentScale);
	CC_SYNTHESIZE(std::string, mRouteLineImage, RouteLineImage);
	
	
	void setShader(const std::string &shaderName);
	void updateLine();
	
	
	void testLineSegment();
	std::string info();
	
	float calculateDegree(const Vec2 &start, const Vec2 &end);

	virtual void setOpacity(GLubyte opacity);
	virtual GLubyte getOpacity();
	
protected:
	void updateSegmentInfo();
	void updateLineByDrawNode();
	void updateLineBySprite();
	void addLineSegment(const Vec2 &start, float requireLen);
	
private:
	DrawNode *mDrawNode;
	Size mSegmentSize;			// the size of the line segment
	ui::Layout *mPanel;
	int mNumSegment;
	Rect mTextureRect;
	GLubyte mOpacity;
};


#endif /* VerticalLineNode_hpp */
