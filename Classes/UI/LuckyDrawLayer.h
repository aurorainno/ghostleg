//
//  LuckyDrawLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#ifndef LuckyDrawLayer_hpp
#define LuckyDrawLayer_hpp

#include <deque>
#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "LuckyDrawReward.h"
#include "AnimeNode.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class LuckyDrawAnime;
class LuckyDrawReward;

class LuckyDrawLayer : public Layer
{
public:
    enum State{
        Idle,
        Moving
    };
    
    CREATE_FUNC(LuckyDrawLayer);
    
    CC_SYNTHESIZE(bool, mIsFreeDraw, IsFreeDraw);
    
    LuckyDrawLayer();
    
    virtual bool init() override;
    void update(float delta) override;
    void setupUI(Node* rootNode);
    
    void onEnter() override;
    void onExit() override;
    
    void onTap();
    void onAnimationDone();
    
    void setReward(LuckyDrawReward* reward);
    void setOnExitCallback(std::function<void()> callback);
    
    std::string getRaritySpriteByType(LuckyDrawReward::RewardType type);
    std::string getItemSprite(LuckyDrawReward::RewardType type, int itemID);
    
    void setState(State state);
    
    bool playVideoAd();
    
    void handleDoubleUpReward();
    
private:
    Node* mAnimePanel;
    Button* mDoubleUpBtn;
    Layout* mRewardPanel;
    Layout* mTapPanel;
    AnimeNode* mTrack;
    AnimeNode* mTapMeNode;
    LuckyDrawAnime* mAnimeLayer;
    std::function<void()> mOnExit;
    std::deque<AnimeNode*> mBoxQueue;
    State mState;
    float mDistanceTravelled;
    float mTargetDisplacement;
    float mSpeed;
    float mAccumTime;
    float mCoolDown;
    float mLastBoxPosX;
    bool mBoxIsSelected;
    bool mAdIsPlayed;
    
    LuckyDrawReward *mReward;
    
    cocostudio::timeline::ActionTimeline *mTimeline;
};

#endif /* LuckyDrawLayer_hpp */
