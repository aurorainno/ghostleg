//
//  PauseDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/4/2016.
//
//

#ifndef PauseDialog_hpp
#define PauseDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;


class PauseDialog : public LayerColor
{
public:
	CREATE_FUNC(PauseDialog);
	//
	virtual bool init();
	void setResumeListener(const Widget::ccWidgetClickCallback &callback);
	void setBackHomeListener(const Widget::ccWidgetClickCallback &callback);
	void setEndGameListener(const Widget::ccWidgetClickCallback &callback);
	void closeDialog();

    virtual void onEnter();
    virtual void onExit();
    
	//	virtual void onEnter();
private:
	void setupUI(Node *mainPanel);
	void updateMapInfo();
	
private:
	Button *mResumeButton;
	Button *mHomeButton;
	Text *mMapInfoText;
	bool mShowDebugMap;
};

#endif /* PauseDialog_hpp */
