//
//  DailyRewardDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/11/2016.
//
//

#ifndef DailyRewardDialog_hpp
#define DailyRewardDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

#include <time.h>

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class RewardData;

class DailyRewardDialog : public LayerColor
{
public:
    enum DialogMode{
        CollectRewardMode = 0,
        ShowRecordMode,
    };
    
	CREATE_FUNC(DailyRewardDialog);
	
    
    CC_SYNTHESIZE(RewardData*, mReward, Reward);
	DailyRewardDialog();
	
	//
	virtual bool init();
	void closeDialog();
	
	virtual void onEnter();
	virtual void onExit();
	
	void setRewardAtDay(int day, RewardData *data);
	void setCollectStatus(int day, bool hasCollected);
	
	void showRewardCollectAnimation(int day);
    
    void setCallback(std::function<void()> callback);
    
    void setDialogMode(DialogMode mode);
	
	//	virtual void onEnter();
private:
	void setupGUI(const std::string &csbName);
    
    void setupUIData();
	
	void onCollectClicked();
	void onCollectAnimationDone();
	
	Node *getDayPanelAtDay(int day);
	
	void updateSpriteForDog(Sprite *sprite, int dog);
	
	Vec2 getDayPanelCenterPos(int day);
	
private:
	Node *mMainPanel;
    Node *mBGPanel;

	Button *mCollectButton;
    Button *mConfirmButton;
	Text *mInfoText;
    
    DialogMode mMode;
    
    int mCycleRange;
	
    
    
	Map<int, Node *> mDayPanelMap;
    
    std::function<void()> mCallback;
};

#endif /* GameContinueDialog_hpp */

