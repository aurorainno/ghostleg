//
//  GameCenterLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 8/7/2016.
//
//

#ifndef GameCenterLayer_hpp
#define GameCenterLayer_hpp

#include <stdio.h>
#include "cocos2d.h"

class GameCenterManager;

USING_NS_CC;

class GameCenterLayer : public Layer{
public:
    CREATE_FUNC(GameCenterLayer);
    
    virtual bool init();
    void update(float delta);
    void setupUI(Node* rootNode);
};



#endif /* GameCenterLayer_hpp */
