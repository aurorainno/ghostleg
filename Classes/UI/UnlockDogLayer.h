//
//  UnlockDogLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 26/10/2016.
//
//

#ifndef UnlockDogLayer_hpp
#define UnlockDogLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class CharGameData;

class UnlockDogLayer : public Layer
{
public:
    CREATE_FUNC(UnlockDogLayer);
	
	UnlockDogLayer();
    virtual bool init() override;
    
    void setupUI(Node* node);
    
    void setDogQueue(const std::queue<int> &dogQueue);
    
    virtual void onEnter() override;
	
	void setCloseCallback(const std::function<void()> &callback);
	
	
private:
    void setView(CharGameData* data);
    void setConfirmBtnListener();
	void showNextUnlockInfo();
	void callbackClose();
	void attachFireworkEffect(Node *mainNode);
    
private:
    std::queue<int> mDogQueue;
    
    Text* mDogNameText;
    Text* mProgressText;
    Text* mInfoText;
    Button* mConfirmBtn;
    Sprite* mDogSprite;

	std::function<void()> mCloseCallback;
};


#endif /* UnlockDogLayer_hpp */
