//
//  StageSelectionView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#include "StageSelectionView.h"

#include "AILPageView.h"
#include "VisibleRect.h"
#include "PlanetManager.h"
#include "DogSelectionScene.h"
#include "HorizontalListView.h"

#include "AILPageView.h"

//AILPageView *mPageView;
//Vector<PlanetItemView *> mPlanetViewList;
//int mSelectedPlanet;

StageSelectionView::StageSelectionView()
: mPreferredSize(Size(320, 160))
, mItemSize(Size(160, 160))
, mPlanetViewList()
, mSelectedPlanet(0)
, mSelectPlanetCallback()
, mListView(nullptr)
{
	
}

bool StageSelectionView::init()
{
	if(Layout::init() == false) {
		return false;
	}
	
	setSizeType(Widget::SizeType::ABSOLUTE);
	setContentSize(mPreferredSize);
	
	setupListView();
	setupStageItemView();
	
	return true;
}


#pragma mark - GUI Setup
void StageSelectionView::setupListView()
{
	HorizontalListView *listView = HorizontalListView::create();
	listView->setContentSize(mPreferredSize);

	addChild(listView);
	
	// 
	mListView = listView;
}


void StageSelectionView::setupStageItemView()
{
	std::vector<int> planets = PlanetManager::instance()->getPlayablePlanetList();
	
	for(int planetID : planets) {
		int bestDistance = PlanetManager::instance()->getBestDistance(planetID);
		bool isUnlocked = PlanetManager::instance()->isPlanetUnLocked(planetID);
		
		PlanetItemView *view = PlanetItemView::create();
		//view->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		//view->changeContentSize(mItemSize);
		mListView->addItem(view);
		
		view->setupPlanet(planetID, bestDistance, isUnlocked == false);
		view->setCallback(CC_CALLBACK_1(StageSelectionView::onPlanetViewClick, this));
		
		mPlanetViewList.pushBack(view);
		
	}
	
	
	// Setup the Callback and Event Listening
	mListView->addEventListener([&](Ref *ref, HorizontalListView::EventType event) {
		// log("Item Event");
		
		switch(event) {
			case HorizontalListView::EventType::TURNING: {
				HorizontalListView *view = (HorizontalListView *) ref;
				int selected = (int) view->getSelectedIndex();
				
				onPageSelected(selected, true);
			}
		}
	});
	
	mListView->setScrollCallback([&](Ref *ref,  Vec2 scrollOffset){
		if(mScrollCallback){
			mScrollCallback(scrollOffset);
		}
	});
}



void StageSelectionView::onPlanetViewClick(int planetID)
{
	if(PlanetManager::instance()->isPlanetUnLocked(planetID)){
		if(mClickCallback){
			mClickCallback();
		}
	}
}


void StageSelectionView::onPageSelected(int page, bool callback)
{
	PlanetItemView *planetView = getPlanetItemViewAtPage(page);
	if(planetView == nullptr) {
		return;
	}
	
	int planetID = planetView->getPlanetID();
	
	bool isRepeat = planetID == mSelectedPlanet;
	
	mSelectedPlanet = planetID;
	
	
	// Callback
	if(callback) {
		if(mSelectPlanetCallback) {
			int maxPage = mPlanetViewList.size() - 1;
			mSelectPlanetCallback(this, mSelectedPlanet, isRepeat, page, maxPage);
		}
	}
	
}


#pragma mark - Navigation & Selection
void StageSelectionView::setSelectedPlanet(int planetID)
{
	// Find the corresponding page
	int page = getPageForPlanet(planetID);
	log("DEBUG: Page=%d planetID=%d", page, planetID);
	
	
	// TODO the exception case
	if(page < 0) {
		page = 0;		// Force it to page 0
	}
	
	mListView->setSelectedItem(page);		// todo: fix for direct jump
	//mListView->scrollToItem(page);
	mSelectedPlanet = mPlanetViewList.at(page)->getPlanetID();

	log("DEBUG: Page=%d mSelectedPlanet=%d index=%d",
					page, mSelectedPlanet, getCurrentPage());
	
	if(mSelectPlanetCallback) {
		int maxPage = (int)(mPlanetViewList.size() - 1);
		mSelectPlanetCallback(this, mSelectedPlanet, false, page, maxPage);
	}
}

void StageSelectionView::scrollToNextPage()
{
	int page = mListView->getSelectedIndex()+1;
	if(page > (mPlanetViewList.size() - 1)){
		page = mPlanetViewList.size() - 1;
	}
	
	//log("New Page: %d", page);
	mListView->scrollToItem(page);
	//mListView->ui::ListView::scrollToItem(page, Vec2::ANCHOR_MIDDLE, Vec2::ANCHOR_MIDDLE);
}

void StageSelectionView::scrollToPreviousPage()
{
	int page = mListView->getSelectedIndex()-1;
	if(page < 0){
		page = 0;
	}
	//log("New Page: %d", page);
	mListView->scrollToItem(page);
}

void StageSelectionView::scrollToPage(int page)
{
	if(page > (mPlanetViewList.size() - 1)){
		page = mPlanetViewList.size() - 1;
	}else if(page < 0){
		page = 0;
	}
	mListView->scrollToItem(page);
}


#pragma mark - Getter, Setter and Update Methods
void StageSelectionView::updatePlanetItemView(PlanetItemView *view)
{
	//int planetID = view->getPlanetID();
	
	// view->set
	//
}

void StageSelectionView::updatePlanetData()
{
	for(PlanetItemView *view : mPlanetViewList) {
		int planetID = view->getPlanetID();
		
		int bestDistance = PlanetManager::instance()->getBestDistance(planetID);
		bool isUnlocked = PlanetManager::instance()->isPlanetUnLocked(planetID);
		
		view->setupPlanet(planetID, bestDistance, isUnlocked == false);
	}
}



PlanetItemView *StageSelectionView::getPlanetItemViewAtPage(int page)
{
	if(page < 0 || page >= mPlanetViewList.size()){
		return nullptr;
	}
	
	return mPlanetViewList.at(page);
}


int StageSelectionView::getSelectedPlanetID()
{
	return mSelectedPlanet;
}


int StageSelectionView::getPageForPlanet(int planetID)
{
	//int page =
	for(int i=0; i<mPlanetViewList.size(); i++) {
		PlanetItemView *itemView = mPlanetViewList.at(i);
		if(itemView == nullptr) {
			continue;
		}
		
		if(itemView->getPlanetID() == planetID) {
			return i;
		}
	}
	
	return -1;
}

int StageSelectionView::getCurrentPage()
{
	return (int) mListView->getSelectedIndex();
}

int StageSelectionView::getLastPage()
{
	return (int) mPlanetViewList.size() - 1;
}


#pragma mark - Callback



void StageSelectionView::setSelectPlanetCallback(const SelectPlanetCallback &callback)
{
	mSelectPlanetCallback = callback;
}

void StageSelectionView::setOnScrollCallback(const ViewScrollCallback &callback)
{
    mScrollCallback = callback;
}

void StageSelectionView::setOnUnlockedPlanetClickCallback(const ClickUnlockedPlanetCallback &callback)
{
    mClickCallback = callback;
}

