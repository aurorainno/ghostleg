//
//  CheckpointDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/5/2017.
//
//

#include "CheckPointDialog.h"

#include "ViewHelper.h"
#include "CommonMacro.h"
#include "VisibleRect.h"


const float kTransitDuration = 0.5f;
const float kShowUpDuration = 2.0f;
const float kShowPositionY = 100;		// Top Y

CheckPointDialog::CheckPointDialog()
: Layer()
, mMainNode(nullptr)
, mTimeline(nullptr)
, mOnCloseCallback(nullptr)
{
	
}


bool CheckPointDialog::init()
{
	
	if(Layer::init() == false) {
		return false;
	}
	
	std::string csb = "gui/InGameCheckPointLayer.csb";
	setupGUI(csb);
	
	
	// GUI Position
	float topY = VisibleRect::top().y;
	mHidePos = Vec2(0, topY+10);
	float guiBottomY = topY - kShowPositionY - getContentSize().height;
	mShowPos = Vec2(0, guiBottomY); // Director::getInstance()->convertToGL(Vec2(0, 10));
	
	
	
	//setupAnimation(csb);	// no need
	
	// Update the initial position (was hidden)
	setPosition(mHidePos);
	
	return true;
}

void CheckPointDialog::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	mMainNode = rootNode;
	setContentSize(rootNode->getContentSize());

	mScoreText = mMainNode->getChildByName<Text *>("scoreText");
	mTimeText = mMainNode->getChildByName<Text *>("timeText");
	mMoneyText = mMainNode->getChildByName<Text *>("moneyText");
}


void CheckPointDialog::runAnimation(const std::string &name)
{
	if(mTimeline) {
		mTimeline->play(name, false);
	}
}

void CheckPointDialog::setupAnimation(const std::string &csb)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csb);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
	
	mTimeline->setFrameEventCallFunc([&](Frame *frame) {
		EventFrame *event = dynamic_cast<EventFrame*>(frame);
		if(!event) {
			return;
		}
		
	});
}


void CheckPointDialog::onEnter()
{
	Layer::onEnter();
}


void CheckPointDialog::hide()
{
	setPosition(mHidePos);
}

void CheckPointDialog::showUp(float duration)
{
	// Hide it first
	hide();
	
	// Define the Animation
	ActionInterval *moveIn = EaseBackOut::create(MoveTo::create(kTransitDuration, mShowPos));
	DelayTime *delay = DelayTime::create(duration);
	ActionInterval *moveOut = EaseBackIn::create(MoveTo::create(kTransitDuration, mHidePos));
	
	CallFunc *callFunc = CallFunc::create([&]{
		if(mOnCloseCallback) {
			mOnCloseCallback();
		}
	});
	
	
	Sequence *sequence = Sequence::create(moveIn, delay, moveOut, callFunc, nullptr);
	
	
	// Run the Action
	runAction(sequence);
}


void CheckPointDialog::setOnCloseCallback(const std::function<void()> &callback)
{
	mOnCloseCallback = callback;
}


#pragma mark - Setter
void CheckPointDialog::setScore(int score)
{
	mScoreText->setString(StringUtils::format("+%d", score));
}
void CheckPointDialog::setTime(float timeElapse)
{
	mTimeText->setString(StringUtils::format("%.2f", timeElapse));
}
void CheckPointDialog::setMoney(int rewardMoney)
{
	mMoneyText->setString(StringUtils::format("+%d", rewardMoney));
}
