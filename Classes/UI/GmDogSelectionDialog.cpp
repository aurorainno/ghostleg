//
//  GmDogSelectionDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 5/10/2016.
//
//

#include "GmDogSelectionDialog.h"

#include "GameCenterManager.h"
#include "GameManager.h"
#include "CommonMacro.h"
#include "ViewHelper.h"
#include "GameModel.h"
#include "GameRes.h"
#include "PlayerCharProfile.h"
#include "PlayerRecord.h"
#include "GameplaySetting.h"
//#include "BuyNewCharLayer.h"
//#include "UnlockCharLayer.h"
#include "StageParallaxLayer.h"
#include "PlayerManager.h"

bool GmDogSelectionDialog::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init())
	{
		return false;
	}
	
	// Setup UI
	
	setupGUI("GmDogSelectDialog.csb");
	
	mCharID = PlayerManager::instance()->getSelectedChar();
	
	updateCharDetail();
	
	// Setup the space background
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	addChild(parallaxLayer);
	parallaxLayer->setVisible(false);
	mSpaceBgNode = parallaxLayer;
	
	return true;
}


void GmDogSelectionDialog::setupGUI(const std::string &csbName)
{
	// Getting the Root Node
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	FIX_UI_LAYOUT(rootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	mMainPanel = mainPanel;
	
	setupText(mainPanel);
	
	Node *refNode = mainPanel->getChildByName("spriteNode");
	setupCharacterNode(refNode);
	
	mLockedIconSprite = mainPanel->getChildByName<Sprite *>("lockedGUISprite");
	mUnlockedIconSprite = mainPanel->getChildByName<Sprite *>("unlockGUISprite");
	
	
	// Last Button
	Button *lastButton = (Button*)mainPanel->getChildByName("lastButton");
	lastButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED:
			{
				changeChar(-1);
				break;
			}
		}
	});
	
	// Next Button
	Button *nextButton = (Button*)mainPanel->getChildByName("nextButton");
	nextButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED:
			{
				changeChar(1);
				break;
			}
		}
	});
    
    mUnlockBtn = (Button*)mainPanel->getChildByName("unlockButton");
    mUnlockBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED:
            {
                unlockSelectedChar();
                break;
            }
        }
    });

    Button *resetButton = (Button*)rootNode->getChildByName("resetButton");
    resetButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED:
            {
                resetAllChar();
                break;
            }
        }
    });
    
    Button *unlockAllButton = (Button*)rootNode->getChildByName("unlockAllButton");
    unlockAllButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED:
            {
                unlockAllChar();
                break;
            }
        }
    });
	
	Button *upgradeAllButton = (Button*)rootNode->getChildByName("upgradeAllButton");
	upgradeAllButton->addClickEventListener([&](Ref *sender){
		upgradeAllChar();
	});
	
//	unlockAllButton->addOn([&](Ref *sender, Widget::TouchEventType type) {
//		switch(type){
//			case ui::Widget::TouchEventType::ENDED:
//			{
//				unlockAllChar();
//				break;
//			}
//		}
//	});
	
	Button *backBtn = (Button*)rootNode->getChildByName("backButton");
	backBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED:
				removeFromParent();
				break;
		}
	});
	
	Button *popup1Btn = mainPanel->getChildByName<Button *>("popUnlockButton");
	popup1Btn->addClickEventListener([&](Ref *sender){
		showUnlockCharPopup();
	});
	
	Button *popup2Btn = mainPanel->getChildByName<Button *>("popBuyButton");
	popup2Btn->addClickEventListener([&](Ref *sender){
		showBuyCharPopup();
	});
	
	Button *upgradeButton = mainPanel->getChildByName<Button *>("upgradeButton");
	upgradeButton->addClickEventListener([&](Ref *sender){
		upgradeSelectedChar();
	});
}

void GmDogSelectionDialog::resetAllChar()
{
	PlayerManager::instance()->resetPlayerCharData();
	mCharID = PlayerManager::instance()->getSelectedChar();
	
	PlayerManager::instance()->selectChar(mCharID);
	
    updateCharDetail();
}


void GmDogSelectionDialog::updateUnlockCharBtn()
{
	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();

	
	if(profile->isLocked()) {
        mUnlockBtn->setEnabled(true);		// click to unlock
    }else{
        mUnlockBtn->setEnabled(false);
    }
}

void GmDogSelectionDialog::unlockAllChar()
{
    PlayerManager::instance()->unlockAllChar();
    updateCharDetail();
}

void GmDogSelectionDialog::unlockSelectedChar()
{
    PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
    if(profile == nullptr){
        return;
    }
    
    PlayerManager::instance()->forceUnlock(profile->getCharID());
    updateCharDetail();
}


#pragma mark - Change Char Logic
void GmDogSelectionDialog::changeChar(int idChange)
{
	// TODO
	
	int newID = getNewCharID(idChange);
	mCharID = newID;
	
	PlayerManager::instance()->selectChar(newID);

	
	// Update GUI
	updateCharDetail();
}


int GmDogSelectionDialog::getNewCharID(int idChange)
{
	int idMax = PlayerManager::instance()->getTotalCharCount();
	int currentID = PlayerManager::instance()->getSelectedChar();
	
	int newValue = currentID + idChange;
	
	
	// Fix for character 2
	if(newValue == 2) {
		newValue = idChange > 0 ? 3 : 1;
	}
	
	// Find tune boundry case
	if(newValue <= 0) {
		newValue = idMax;
	} else if(newValue > idMax) {
		newValue = 1;
	}
	
	return newValue;
}

void GmDogSelectionDialog::updateCharDetail()
{
//    updateUnlockCharBtn();
	updateTextInfo();
	updateCharacter();
	updateCharIcon();
}

#pragma mark - Update Character Animation

void GmDogSelectionDialog::setupCharacterNode(Node *refNode)
{
	if(refNode == nullptr) {
		log("setupCharacterNode: refNode is nullptr");
		return;
	}
	
	GameModel *model = GameModel::create();
	model->setScale(1.0f);
	model->setPosition(Vec2::ZERO);
	refNode->addChild(model);
	mCharModel = model;

	// Hide the reference Node
	refNode->getChildByName("referenceSprite")->setVisible(false);
	
}

void GmDogSelectionDialog::updateCharacter()
{
	if(mCharModel) {
		std::string name = GameRes::getSelectedCharacterCsb();
		mCharModel->setup(name);
		mCharModel->setAction(GameModel::Action::Idle);
	}
}


#pragma mark - Character Text information
void GmDogSelectionDialog::setupText(Node *mainPanel)
{
	mNameText = (Text *) mainPanel->getChildByName("nameText");
	mCharIDText = (Text *) mainPanel->getChildByName("dogIDText");
	mStatText = (Text *) mainPanel->getChildByName("infoScrollView")->getChildByName("statText");
    mUnlockText = (Text *) mainPanel->getChildByName("unlockText");
}

void GmDogSelectionDialog::updateTextInfo()
{
	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
	
	profile->update();		// update the latest attributes
	
	std::string name = profile == nullptr ? "??" : profile->getName();
	int charID = profile == nullptr ? 0 : profile->getCharID();
	
	
	
	bool unlocked = profile->isLocked() == false;
	
    setCharUnlockState(unlocked);
	setCharID(charID);
	setCharName(name);
	setCharStatistics();
}

void GmDogSelectionDialog::setCharUnlockState(bool isUnlocked)
{
    if(isUnlocked){
        mUnlockText->setString("Unlocked");
    }else{
        mUnlockText->setString("Locked");
    }
}

void GmDogSelectionDialog::setCharName(const std::string &name)
{
	mNameText->setString(name);
}

void GmDogSelectionDialog::setCharID(int dogID)
{
	std::string text = StringUtils::format("%03d", dogID);
	mCharIDText->setString(text);
}

void GmDogSelectionDialog::setCharStatistics()
{
	mStatText->setString("Oh!!!");
	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
	
	std::string content = profile->infoProfile();
	
	content += "----------\n";
	
	GameplaySetting *setting = GameplaySetting::create();
	profile->updateGameplaySetting(setting);
	//profile->up
	content += setting->toString();
	
	
	mStatText->setString(content);
}


#pragma mark - Character GUI information
void GmDogSelectionDialog::updateCharIcon()
{
//	if(mLockedIconSprite) {
//		
//		std::string name = StringUtils::format("dog/dselect_dog%d.png", mCharID);
//		mLockedIconSprite->setTexture(name);
//	}
//	
	if(mUnlockedIconSprite) {
		// XXX: change to resource location
		std::string name = StringUtils::format("dog/dselect_dog%d_unlock.png", mCharID);
		mUnlockedIconSprite->setTexture(name);
	}
}

void GmDogSelectionDialog::showSpaceBg()
{
	mSpaceBgNode->setVisible(true);
}

void GmDogSelectionDialog::hideSpaceBg()
{
	mSpaceBgNode->setVisible(false);
}

void GmDogSelectionDialog::showUnlockCharPopup()
{
//	std::queue<int> dogQueue;
//	dogQueue.push(mCharID);
//	
//	UnlockCharLayer* layer = UnlockCharLayer::create();
//	layer->setCharQueue(dogQueue);
//	addChild(layer);
//	
//	mMainPanel->setVisible(false);
//	showSpaceBg();
//	
//	layer->setCloseCallback([&]{
//		mMainPanel->setVisible(true);
//		hideSpaceBg();
//	});
}

void GmDogSelectionDialog::showBuyCharPopup()
{
//	BuyNewCharLayer *layer = BuyNewCharLayer::create();
//	layer->setChar(mCharID);
//	addChild(layer);
//	
//	Node *myLayer = layer;
//	layer->setCallback([&, myLayer]{
//		myLayer->removeFromParent();
//	});
}


void GmDogSelectionDialog::upgradeSelectedChar()
{
	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
	
	if(profile->isMaxLevel() == false) {
		profile->doUpgradeLogic();
		updateCharDetail();
	}
}


void GmDogSelectionDialog::upgradeAllChar()
{
	PlayerManager::instance()->upgradeAllChar();
	updateCharDetail();
}
