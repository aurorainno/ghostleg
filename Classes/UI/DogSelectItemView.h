//
//  DogSelectItemView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/10/2016.
//
//

#ifndef DogSelectItemView_hpp
#define DogSelectItemView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocos2d::ui;

class DogData;
class CircularMaskNode;
class RichTextLabel;
class DogSelectionSceneLayer;

class DogSelectItemView : public Layout
{
public:
	typedef std::function<void(int dogID)> DogSelectCallback;
public:
	CREATE_FUNC(DogSelectItemView);
	
	DogSelectItemView();
    CC_SYNTHESIZE(DogSelectionSceneLayer*, mDogSelectionScene, DogSelectionScene);
	
	//
	virtual bool init();
	
	void setDogForSuggestion(int dogID);
	
	void setDog(int dogID);
	void updateUnlockStatus();		// update the unlock status
	void updateDogGUI();			// upate the GUI elements
	int getDogID();
	void setSelected(bool flag);
	
	std::string getDogInfo(DogData *data);
	
	void testUIState(bool isSelected, bool isUnlock, int unlockValue=5, int unlockCond=10);
    
    void setCallback(DogSelectCallback callBack);
    bool hasCallback();

private:
	void setupGUI(const std::string &csbName);
	void onViewClicked();

    void setupProgressBar();
	void setDogName(const std::string &name);
	void setDogInfo(DogData* data);
	void updateGUIVisibility();
	void updateUnlockProgress();
    void setupPurchaseBtn();
    
    /**
     * handle Purchase Dog logic
     * @param show dog selection UI after showing the DogUnlockedDialog?
     * @param set which node to be visible after showing the DogUnlockedDialog
     * @return player can buy dog or not
     */
    bool handlePurchaseDog(bool showUIAfterPurchase, Node* visibleNode = nullptr);

    
    std::string getLevelIcon(int level);
	
	
private:		// GUI Elements
	Node *mDogIconPanel;
	Node *mBgUnlocked;
	Node *mBgNormal;
    Node *mLockedInfoPanel, *mUnlockedInfoPanel;
    Node *mSuitPanel, *mMasteryPanel;
    CircularMaskNode *mProgressBar;
	Sprite *mDogIconSprite;
	Sprite *mDogSprite;
	Sprite *mProgressSprite;
    Sprite *mSuitIcon, *mSuitLevelIcon;
    Sprite *mMasteryIcon, *mMasteryLevelIcon;
	RichTextLabel *mProgressText;
	Text *mDogInfoText;
	Text *mDogNameText;
    Text *mSuitName, *mMasteryName;
    Button *mSelectBtn, *mInUseBtn, *mEventCostBtn,*mNormalCostBtn, *mInfoBtn;
    Sprite *mNewLabel;
    
    DogSelectCallback mDogSelectCallback;
	
private:
	int mDogID;
    std::map<int,int> mMasteryMap;
    int mSuitID, mSuitLevel;
	
	bool mIsForSuggestion;
	bool mIsSelected;			// is the dog using
	bool mIsUnLocked;			// is the dog unlocked ??
	bool mIsReadyToUnlock;		// is the dog ready to unlock
    bool mIsEventDog;
	
	int mUnlockCond;			// the required value to unlock
	int mUnlockValue;			// the current value of the unlock Cond
	
};

#endif /* DogSelectItemView_hpp */
