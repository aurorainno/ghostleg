//
//  PlanetItemView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#ifndef PlanetItemView_hpp
#define PlanetItemView_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class PlanetItemView : public Layout
{
public:
	CREATE_FUNC(PlanetItemView);
	CC_SYNTHESIZE(ActionTimeline *, mPlanetAction, PlanetAction);
	
	PlanetItemView();
	
	//
	virtual bool init();

	void setupPlanet(int planetID, int bestDistance, bool isLocked);
//	void setLocked(bool lock);
	
	void setBestDistance(int distance);
	void setPlanet(int planetID);
	void setIsLocked(bool flag);
	int getPlanetID();
	
	void testPlanetCsb(const std::string &name, bool isLocked);
    void setCallback(std::function<void(int)> callback);
	void changeContentSize(const Size &newSize);
	
	void unlockPlanet(const std::function<void()> &callback);
	
private:
	void setupGUI(const std::string &csbName);
	void setupNameGUI();
	
	void updatePlanetVisual();
	void updateInfoText();
	void updateStageName();
	
	void setUnlockText();
	void setPlanetCsb(const std::string &name, bool isLocked);
	void setPlanetCsbAnimationForLockState(bool isLocked);
	void setPlanetCsbAnimation(const std::string &name);
	
	void updateTextColorForLockState(bool isLocked);
	
	std::string getPlanetNameImage(bool isLocked);
	
private:
	Node *mRootNode;
	Sprite *mPlanetSprite;
	Sprite *mNameSprite;
	Text *mInfoText;
	Text *mUnlockInfoText;
	Node *mLockPanel;
	Node *mUnlockPanel;
    Layout *mTouchPanel;
	Vec2 mPlantCsbPos;
	Node *mMainPanel;
	Node *mPlanetNodeParent;		// the parent node hold the planet Node
	Node *mPlanetNode;
    std::function<void(int)> mCallback;
	
	Text *mStageText;			// e.g Stage 1
	Text *mStageNameText;		// e.g Rock Field
	
	bool mIsLocked;
	int mBestDistance;
	int mPlanetID;
};


#endif /* PlanetItemView_hpp */
