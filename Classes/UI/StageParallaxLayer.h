//
//  StageParallaxLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 6/5/2016.
//
//

#ifndef StageParallaxLayer_hpp
#define StageParallaxLayer_hpp

#include <stdio.h>
#include "cocos2d.h"

#include "ParallaxLayer.h"

USING_NS_CC;

class SpriteParallaxSubLayer;

class StageParallaxLayer : public ParallaxLayer
{
public:
	CREATE_FUNC(StageParallaxLayer);
	
	CC_SYNTHESIZE(int, mStage, Stage);
	CC_SYNTHESIZE(std::string, mBg1, Bg1);
	CC_SYNTHESIZE(std::string, mBg2, Bg2);
	CC_SYNTHESIZE(int, mSpaceObjectID, SpaceObjectID);
	CC_SYNTHESIZE(bool, mShowRoute, ShowRoute);
	CC_SYNTHESIZE(bool, mShowBackground, ShowBackground);
	CC_SYNTHESIZE_RETAIN(Node *, mMainNode, MainNode);
	
	StageParallaxLayer();
	
	virtual void onEnter();
	virtual bool init();
	void setup();
	void setBackground(const std::string &bg1, const std::string &bg2, int objectID=1);
	virtual void update(float delta);
	
	CC_SYNTHESIZE(float, mSpeed, Speed);
	CC_SYNTHESIZE(float, mBGSpeed, BGSpeed);
	
	void setBackgroundTint(const cocos2d::Color3B &color);
	
	void setupParallaxSubLayer();
	
	void shake(float duration);
	
	
protected:
	void setupDecoLayer();
	void setupEdgeLayer();
	void setupMainLayer();		// the layer of the game
	
	void setupRouteLayer();
	void setupRouteGuide();		// Guide line of the route, just for debugging
private:
	float mOffsetY;
	bool mHasSetup;

};

#endif /* StageParallaxLayer_hpp */
