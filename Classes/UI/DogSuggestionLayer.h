//
//  DogSuggestionLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/11/2016.
//
//

#ifndef DogSuggestionLayer_hpp
#define DogSuggestionLayer_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class DogSelectItemView;

class DogSuggestionLayer : public Layer
{
public:
	typedef std::function<void(bool didUnlock)> DogSuggestionLayerCallback;
	
public:
	CREATE_FUNC(DogSuggestionLayer);
	
	DogSuggestionLayer();
	
	virtual bool init() override;
	
	void setCallback(const DogSuggestionLayerCallback &callback);
	
	void setDog(int dogID);
	
private:
	void setupGUI(const std::string &csbName);
	void unlockDog();
	void unlockDogByStar();
	void showCoinShop();
	void closeDialog();
	void onUnlockDone();
	void updateStarValue();
	
	void doCallback(bool didUnlock);
	
private:
//	std::vector<int> mPlanetList;
//	int mPlanetID;
//	Button* mOKBtn;
//	Sprite *mNameSprite;
//	Node *mPlanetNodeParent;		// the parent node hold the planet Node
//	Node *mPlanetNode;
	Node *mMainPanel;
	DogSelectItemView *mDogInfo;
	Text *mPriceText;
	ui::Button *mUnlockButton;
	int mSelectedDog;
	int mPrice;
	Node *mUnlockInfoView;
	Node *mStarValueNode;
//
	
	DogSuggestionLayerCallback mCallback;
	
};



#endif /* DogSuggestionLayer_hpp */
