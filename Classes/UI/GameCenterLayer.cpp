//
//  GameCenterLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 8/7/2016.
//
//

#include "GameCenterLayer.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "GameCenterManager.h"
#include "GameManager.h"
#include "CommonMacro.h"

using namespace cocos2d::ui;

bool GameCenterLayer::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init())
    {
        return false;
    }
    
    // Setup UI
    Node *rootNode = CSLoader::createNode("GameCenterScene.csb");
    addChild(rootNode);
    
    setupUI(rootNode);
    scheduleUpdate();
    
    return true;
}


void GameCenterLayer::setupUI(Node* rootNode){
    FIX_UI_LAYOUT(rootNode);
    
    Button* tutorialBtn = rootNode->getChildByName<Button*>("SignInBtn");
    tutorialBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                break;
            }
        }
    });
    
    Button *backBtn = (Button*)rootNode->getChildByName("backBtn");
    backBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED:
                removeFromParent();
                break;
        }
    });

}


void GameCenterLayer::update(float delta){
//    if(mManager->hasSignedIn()){
//        log("Player has signed in to game center/google play!");
//        unscheduleUpdate();
//        removeFromParent();
//    }
}


