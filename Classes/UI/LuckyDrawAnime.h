//
//  LuckyDrawAnime.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#ifndef LuckyDrawAnime_hpp
#define LuckyDrawAnime_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class LuckyDrawAnime : public Layer
{
public:
	typedef std::function<void(Ref *)> LuckyDrawAnimeCallback;
public:
	CREATE_FUNC(LuckyDrawAnime);
	
	LuckyDrawAnime();
	
	virtual bool init() override;
	
	
	virtual void onEnter() override;
	
	void setIdle();			// Turn to idle mode
	void openGacha();		// turn to openGacha	// will do callback
	
	
	void setOpenDoneCallback(const LuckyDrawAnimeCallback &callback);
	
	void playSound(const std::string &soundName);
	
private:
	void onFrameEventReceived(EventFrame *frame);
	
private:
	Node *mMainNode;
	LuckyDrawAnimeCallback mOpenDoneCallback;
	
#pragma mark - GUI & Animation
public:
	void runAnimation(const std::string &name, bool isLoop = false);
	
private:
	void setupGUI(const std::string &csbName);
	void setupAnimation(const std::string &csb);

private:		// data
	cocostudio::timeline::ActionTimeline *mTimeline;
	
};




#endif /* LuckyDrawAnime_hpp */
