//
//  PlanetSelectionView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#include "PlanetSelectionView.h"

#include "AILPageView.h"
#include "VisibleRect.h"
#include "PlanetManager.h"
#include "DogSelectionScene.h"

//AILPageView *mPageView;
//Vector<PlanetItemView *> mPlanetViewList;
//int mSelectedPlanet;

PlanetSelectionView::PlanetSelectionView()
: mPageView(nullptr)
, mPlanetViewList()
, mSelectedPlanet(1)
, mSelectPlanetCallback()
{
	
}

bool PlanetSelectionView::init()
{
	if(Layout::init() == false) {
		return false;
	}
	
	setSizeType(Widget::SizeType::ABSOLUTE);
	setContentSize(VisibleRect::getVisibleRect().size);
	
	setupPageView();
	setupPlanets();
	
	return true;
}


void PlanetSelectionView::setupPageView()
{
	AILPageView *pageView = AILPageView::create();
	pageView->setContentSize(VisibleRect::getVisibleRect().size);
	pageView->setDirection(PageView::Direction::HORIZONTAL);

	addChild(pageView);

	mPageView = pageView;
}

void PlanetSelectionView::scrollToNextPage()
{
    int page = mPageView->getCurrentPageIndex()+1;
    if(page > (mPlanetViewList.size() - 1)){
        page = mPlanetViewList.size() - 1;
    }
    mPageView->scrollToPage(page);
}

void PlanetSelectionView::scrollToPreviousPage()
{
    int page = mPageView->getCurrentPageIndex()-1;
    if(page < 0){
        page = 0;
    }
    mPageView->scrollToPage(page);
}

void PlanetSelectionView::scrollToPage(int page)
{
    if(page > (mPlanetViewList.size() - 1)){
        page = mPlanetViewList.size() - 1;
    }else if(page < 0){
        page = 0;
    }
     mPageView->scrollToPage(page);
}

void PlanetSelectionView::updatePlanetItemView(PlanetItemView *view)
{
	//int planetID = view->getPlanetID();
	
	// view->set
	//
}

void PlanetSelectionView::updatePlanetData()
{
	for(PlanetItemView *view : mPlanetViewList) {
		int planetID = view->getPlanetID();
		
		int bestDistance = PlanetManager::instance()->getBestDistance(planetID);
		bool isUnlocked = PlanetManager::instance()->isPlanetUnLocked(planetID);
		
		view->setupPlanet(planetID, bestDistance, isUnlocked == false);
	}
}

void PlanetSelectionView::setupPlanets()
{
	std::vector<int> planets = PlanetManager::instance()->getPlayablePlanetList();
	
	for(int planetID : planets) {
		int bestDistance = PlanetManager::instance()->getBestDistance(planetID);
		bool isUnlocked = PlanetManager::instance()->isPlanetUnLocked(planetID);
		
		PlanetItemView *view = PlanetItemView::create();
		mPageView->addPage(view);
		
		view->setupPlanet(planetID, bestDistance, isUnlocked == false);
        view->setCallback(CC_CALLBACK_1(PlanetSelectionView::onPlanetViewClick, this));
		
		mPlanetViewList.pushBack(view);
		
	}
	

	mPageView->addEventListener([&](Ref *ref, PageView::EventType event) {
		log("Page Event");
		
		switch(event) {
			case PageView::EventType::TURNING: {
				PageView *view = (PageView *)ref;
				int selectedPage = (int) view->getCurrentPageIndex();
				
                log("page turning");
				onPageSelected(selectedPage, true);
				
				break;
			}
		}
	});
    
    mPageView->setScrollCallback([&](Ref*, Vec2 scrollOffset){
        if(mScrollCallback){
            mScrollCallback(scrollOffset);
        }
    });
}

void PlanetSelectionView::onPlanetViewClick(int planetID)
{
    if(PlanetManager::instance()->isPlanetUnLocked(planetID)){
        if(mClickCallback){
            mClickCallback();
        }
    }else{
        auto scene = Scene::create();
        
        DogSelectionSceneLayer* layer = DogSelectionSceneLayer::create();
        layer->setEnterAtSpecificPlanet(planetID);
        
        scene->addChild(layer);
        Director::getInstance()->pushScene(scene);
    }
}

void PlanetSelectionView::setSelectPlanetCallback(const SelectPlanetCallback &callback)
{
	mSelectPlanetCallback = callback;
}

void PlanetSelectionView::setOnScrollCallback(const ViewScrollCallback &callback)
{
    mScrollCallback = callback;
}

void PlanetSelectionView::setOnUnlockedPlanetClickCallback(const ClickUnlockedPlanetCallback &callback)
{
    mClickCallback = callback;
}

void PlanetSelectionView::onPageSelected(int page, bool callback)
{
	PlanetItemView *planetView = getPlanetItemViewAtPage(page);
	if(planetView == nullptr) {
		return;
	}
	
	int planetID = planetView->getPlanetID();
	
	bool isRepeat = planetID == mSelectedPlanet;
	
	mSelectedPlanet = planetID;
	
	
	// Callback
	if(callback) {
		if(mSelectPlanetCallback) {
            int maxPage = mPlanetViewList.size() - 1;
			mSelectPlanetCallback(this, mSelectedPlanet, isRepeat, page, maxPage);
		}
	}
	
}

PlanetItemView *PlanetSelectionView::getPlanetItemViewAtPage(int page)
{
	if(page < 0 || page >= mPlanetViewList.size()){
		return nullptr;
	}
	
	return mPlanetViewList.at(page);
}


int PlanetSelectionView::getSelectedPlanetID()
{
	return mSelectedPlanet;
}

void PlanetSelectionView::setSelectedPlanet(int planetID)
{
	// Find the corresponding page
	int page = getPageForPlanet(planetID);
	
	// TODO the exception case
	if(page < 0) {
		page = 0;		// Force it to page 0
	}
	
	//mPageView->scrollToPage(page);
	//mPageView->setCurSelectedIndex(page);
	mPageView->setCurrentPageIndex(page);
	
	mSelectedPlanet = mPlanetViewList.at(page)->getPlanetID();
    
    if(mSelectPlanetCallback) {
        int maxPage = mPlanetViewList.size() - 1;
        mSelectPlanetCallback(this, mSelectedPlanet, false, page, maxPage);
    }
}

int PlanetSelectionView::getPageForPlanet(int planetID)
{
	//int page =
	for(int i=0; i<mPlanetViewList.size(); i++) {
		PlanetItemView *itemView = mPlanetViewList.at(i);
		if(itemView == nullptr) {
			continue;
		}
		
		if(itemView->getPlanetID() == planetID) {
			return i;
		}
	}
	
	return -1;
}

int PlanetSelectionView::getCurrentPage()
{
    return (int) mPageView->getCurrentPageIndex();
}

int PlanetSelectionView::getLastPage()
{
    return (int) mPlanetViewList.size() - 1;
}
//
////
//virtual bool init();
//
//
//private:
//void setupPlanets();
//
//private:
//AILPageView *mPageView;
//Vector<PlanetItemView *> mPlanetViewList;
//int mSelectedPlanet;

