//
//  CheckpointDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/5/2017.
//
//

#ifndef CheckpointDialog_hpp
#define CheckpointDialog_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class CheckPointDialog : public Layer
{
public:
	CREATE_FUNC(CheckPointDialog);
	
	CheckPointDialog();
	CC_SYNTHESIZE(Vec2, mHidePos, HidePos);		// note: anchor point at left_bottom
	CC_SYNTHESIZE(Vec2, mShowPos, ShowPos);		// note: anchor point at left_bottom
	
	virtual bool init() override;
	
	virtual void onEnter() override;
	
	void hide();
	void showUp(float duration=1.0f);
	void setOnCloseCallback(const std::function<void()> &callback);
	
	
private:
	Node *mMainNode;
	std::function<void ()> mOnCloseCallback;
	
#pragma mark - GUI & Animation
public:
	void runAnimation(const std::string &name);
	
#pragma mark - Setter
	void setScore(int score);
	void setTime(float timeElapse);
	void setMoney(int rewardMoney);
	
private:
	void setupGUI(const std::string &csbName);
	void setupAnimation(const std::string &csb);
	
private:		// data
	cocostudio::timeline::ActionTimeline *mTimeline;
	
	
	
private:
	//
	Text *mScoreText;
	Text *mTimeText;
	Text *mMoneyText;
};




#endif /* CheckpointDialog_hpp */
