//
//  LuckyDrawAnime.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#include "LuckyDrawAnime.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameUI.h"
#include "Analytics.h"
#include "GameSound.h"
#include "StringHelper.h"

LuckyDrawAnime::LuckyDrawAnime()
: Layer()
, mMainNode(nullptr)
, mTimeline(nullptr)
{
	
}


bool LuckyDrawAnime::init()
{
	
	if(Layer::init() == false) {
		return false;
	}
	
	std::string csb = "gachaGUI/gui_gacha_get.csb";
	setupGUI(csb);
	setupAnimation(csb);
    
	
	return true;
}

void LuckyDrawAnime::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	mMainNode = rootNode;
	
	mMainNode->setScale(0.5f);
    mMainNode->setVisible(false);
	
	FIX_UI_LAYOUT(rootNode);
	
}

void LuckyDrawAnime::runAnimation(const std::string &name, bool isLoop)
{
	if(mTimeline) {
		mTimeline->play(name, isLoop);
        mTimeline->setTimeSpeed(1.0);
	}
}

void LuckyDrawAnime::setupAnimation(const std::string &csb)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csb);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
	
	mTimeline->setFrameEventCallFunc([&](Frame *frame) {
		EventFrame *event = dynamic_cast<EventFrame*>(frame);
		if(!event) {
			return;
		}
		
		onFrameEventReceived(event);
	});
    
}


void LuckyDrawAnime::setIdle()
{
	runAnimation("gacha_inactive");
}

void LuckyDrawAnime::openGacha()
{
	runAnimation("active");
    mMainNode->setVisible(true);
    mTimeline->setAnimationEndCallFunc("active", [=](){
        this->runAnimation("loop",true);
    });
}


void LuckyDrawAnime::onFrameEventReceived(EventFrame *frame) {
	//
	
	std::string str = frame->getEvent();
	log("frameCalled: frameEvent=%s", str.c_str());
	
	if("gachaOpenDone" == str) {
		if(mOpenDoneCallback) {
			mOpenDoneCallback(this);
		}
	}
	
	// Sound
    GameSound::playSoundFile("gacha/gacha_rewardget.mp3");
//	if(aurora::StringHelper::startsWith(str, "sound")) {
//		playSound(str);
//	}
}

void LuckyDrawAnime::onEnter()
{
	Layer::onEnter();
	
	setIdle();
}


void LuckyDrawAnime::setOpenDoneCallback(const LuckyDrawAnimeCallback &callback)
{
	mOpenDoneCallback = callback;
}


void LuckyDrawAnime::playSound(const std::string &soundName)
{
	std::string fullpath = "sound/gacha/" + soundName + ".mp3";

	GameSound::playSoundFile(fullpath);
}
