//
//  TutorialNpcDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 12/5/2017.
//
//

#ifndef TutorialNpcDialog_hpp
#define TutorialNpcDialog_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class TutorialNpcDialog : public Layer
{
public:
	CREATE_FUNC(TutorialNpcDialog);
	
	TutorialNpcDialog();
	
	virtual bool init() override;
	
	virtual void onEnter() override;
	
private:
	Node *mMainNode;
	
#pragma mark - GUI & Animation
public:
	void runAnimation(const std::string &name);

	void showDialog(const std::string &msg="");
	void closeDialog();
	
private:
	void setupGUI(const std::string &csbName);
	void setupAnimation(const std::string &csb);
	
private:		// data
	cocostudio::timeline::ActionTimeline *mTimeline;
	Text *mMessageText;
};



#endif /* TutorialNpcDialog_hpp */
