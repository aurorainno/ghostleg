//
//  TutorialNpcDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 12/5/2017.
//
//

#include "TutorialNpcDialog.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "VisibleRect.h"


TutorialNpcDialog::TutorialNpcDialog()
: Layer()
, mMainNode(nullptr)
, mTimeline(nullptr)
{
	
}


bool TutorialNpcDialog::init()
{
	
	if(Layer::init() == false) {
		return false;
	}
	
	std::string csb = "gui/tutorial/TutorialNpcDialog.csb";
	setupGUI(csb);
	
	setupAnimation(csb);	// no need
	
	return true;
}

void TutorialNpcDialog::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	mMainNode = rootNode;
	setContentSize(rootNode->getContentSize());
	
	
	mMessageText = mMainNode->getChildByName("dialogSprite")->getChildByName<Text *>("dialogMsg");
	
}


void TutorialNpcDialog::runAnimation(const std::string &name)
{
	if(mTimeline) {
		mTimeline->play(name, false);
	}
}

void TutorialNpcDialog::setupAnimation(const std::string &csb)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csb);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
	
	mTimeline->setFrameEventCallFunc([&](Frame *frame) {
		EventFrame *event = dynamic_cast<EventFrame*>(frame);
		if(!event) {
			return;
		}
		
	});
}


void TutorialNpcDialog::onEnter()
{
	Layer::onEnter();
}

void TutorialNpcDialog::showDialog(const std::string &msg)
{
	mMessageText->setString(msg);
	
	runAnimation("showUp");
}

void TutorialNpcDialog::closeDialog()
{
	runAnimation("closeDown");
}

