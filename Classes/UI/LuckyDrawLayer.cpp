//
//  LuckyDrawLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#include "LuckyDrawLayer.h"
#include "CommonMacro.h"
#include "LuckyDrawAnime.h"
#include "LuckyDrawManager.h"
#include "GameSound.h"
#include "AdManager.h"
#include "AnimationHelper.h"
#include "Analytics.h"

#define kMovingTime 0.25

using namespace cocostudio::timeline;

namespace {
    void setScaleTo(Node *node, float initScale, float finalScale, float duration)
    {
        node->setScale(initScale);
        ScaleTo * scaleto = ScaleTo::create(duration, finalScale);
        node->runAction(scaleto);
    }
}


LuckyDrawLayer::LuckyDrawLayer():
mOnExit(0)
,mTargetDisplacement(160)
,mDistanceTravelled(0)
,mCoolDown(1)
,mAccumTime(0)
,mBoxIsSelected(false)
,mReward(nullptr)
{}

bool LuckyDrawLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    mSpeed = mTargetDisplacement / kMovingTime;
    
    Node* rootNode = CSLoader::createNode("LuckyDrawLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    mTapPanel->setVisible(false);
    
    cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline("LuckyDrawLayer.csb");
    runAction(timeline);		// retain is done here!
    
    mTimeline = timeline;
    

    setState(State::Moving);
    scheduleUpdate();
    //
    //    setVisible(false);
    return true;
}

void LuckyDrawLayer::onEnter()
{
    Layer::onEnter();
    mAdIsPlayed = false;
    mTimeline->play("gacha_inactive", true);
	
	LOG_SCREEN(Analytics::scn_luckydraw2);
}

void LuckyDrawLayer::onExit()
{
    Layer::onExit();
    GameSound::stopAllEffect();
}

void LuckyDrawLayer::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    mAnimePanel = rootNode->getChildByName("animePanel");
    mTapPanel =  (Layout*)rootNode->getChildByName("touchPanel");

    mRewardPanel = (Layout*) rootNode->getChildByName("rewardPanel");
 
    mAnimeLayer = LuckyDrawAnime::create();
    mAnimeLayer->setPosition(getContentSize().width/2-2, getContentSize().height/2-2);
    mAnimePanel->addChild(mAnimeLayer);
    
    mAnimeLayer->setOpenDoneCallback([&](Ref*){
        this->onAnimationDone();
    });
    
    mRewardPanel->addClickEventListener([&](Ref*){
        if(mOnExit){
            mOnExit();
        }
        this->removeFromParent();
    });
    
    mDoubleUpBtn = mRewardPanel->getChildByName<Button*>("doubleUpBtn");

    
    
    mTapPanel->addClickEventListener([&](Ref*){ 
        this->onTap();
    });
    
    Vec2 trackPos = rootNode->getChildByName("trackNode")->getPosition();
    AnimeNode *trackNode = AnimeNode::create();
    trackNode->setup("gachaGUI/gui_gacha_track.csb");
    trackNode->setScale(0.5);
//    trackNode->setPosition(trackPos);
    rootNode->getChildByName("trackNode")->addChild(trackNode);
    mTrack = trackNode;
    
    AnimeNode *tapMeNode = AnimeNode::create();
    tapMeNode->setup("gachaGUI/gui_gacha_tapme.csb");
    tapMeNode->setScale(0.5);
    //    trackNode->setPosition(trackPos);
    mTapPanel->getChildByName("tapMe")->addChild(tapMeNode);
    mTapMeNode = tapMeNode;

    
    for(int i=1;i<=4;i++){
        Node* node = rootNode->getChildByName(StringUtils::format("box%02d",i));
        
        AnimeNode *animeNode = AnimeNode::create();
        animeNode->setup("gachaGUI/gui_gacha_box.csb");
        animeNode->setScale(0.5);
 //       animeNode->setPosition(node->getPosition());
        animeNode->setName(node->getName());
        animeNode->setFrameEventCallback([=](EventFrame* event){
            std::string name = event->getEvent();
            if(name == "sfxActive"){
                GameSound::playSoundFile("gacha/gacha_box_active.mp3");
            }else if(name == "sfxOpen"){
                GameSound::playSoundFile("gacha/gacha_boxopen.mp3");
            }
        });

        node->addChild(animeNode);
        
        //        Sprite* sprite = Sprite::create("images/character_box1_off.png");
        //        node->addChild(sprite);
        //        node->retain();
        if(i==4){
            mLastBoxPosX = animeNode->getParent()->getPositionX();
            mLastBoxPosX = node->getPositionX();
        }
        mBoxQueue.push_back(animeNode);
        //       log("posX:%.2f",node->getPositionX());
    }

    mTargetDisplacement = fabsf(mBoxQueue.at(1)->getParent()->getPositionX() - mBoxQueue.at(0)->getParent()->getPositionX());

}

void LuckyDrawLayer::setState(LuckyDrawLayer::State state)
{
    switch (state) {
        case Moving:
            GameSound::playSoundFile("gacha/gacha_track.mp3",false,0.2);
            mTapPanel->setVisible(false);
            mTrack->playAnimation("active", false);
            mState = Moving;
            break;
        
        case Idle:{
            GameSound::playSoundFile("gacha/gacha_boxbounce.mp3");
            mTapPanel->setVisible(true);
            mTapMeNode->playAnimation("active", false);
            for(AnimeNode* boxNode : mBoxQueue){
//                log("name:%s posX:%.2f,posY:%.2f",boxNode->getName().c_str(),boxNode->getPositionX(),boxNode->getPositionY());
                boxNode->playAnimation("stopanime", false);
            }
            AnimeNode* firstBox = mBoxQueue.at(0);
            firstBox->getParent()->setPositionX(mLastBoxPosX);
            firstBox->retain();
            mBoxQueue.pop_front();
            mBoxQueue.push_back(firstBox);
            mState = Idle;
            
            break;
        }
            
        default:
            break;
    }
}

void LuckyDrawLayer::update(float delta)
{
    if(mBoxIsSelected){
        return;
    }
    
    switch (mState) {
        case Moving:{
            float displacement = mSpeed * delta;
            for(Node* boxNode : mBoxQueue){
                boxNode->getParent()->setPositionX(boxNode->getParent()->getPositionX() - displacement);
            }
            mDistanceTravelled += displacement;
            
            if(mDistanceTravelled >= mTargetDisplacement){
                for(AnimeNode* boxNode : mBoxQueue){
                    log("boxNode posX before adjust:%.2f",boxNode->getParent()->getPositionX());
                    boxNode->getParent()->setPositionX(boxNode->getParent()->getPositionX() + (mDistanceTravelled - mTargetDisplacement));
                    log("boxNode posX after adjust:%.2f",boxNode->getParent()->getPositionX());
                    //                  boxNode->playAnimation("", false);
                }
                mDistanceTravelled = 0;
                setState(State::Idle);
            }
            
            break;
        }
            
        case Idle:{
            if(mAccumTime >= mCoolDown){
                mAccumTime = 0;
                setState(Moving);
                break;
            }
            
            mAccumTime += delta;
            
            break;
        }
            
        default:
            break;
    }
}

void LuckyDrawLayer::onTap()
{
    if(mState == Moving || mBoxIsSelected){
        return;
    }
    
    mBoxIsSelected = true;
    mTapPanel->setVisible(false);
    
    AnimeNode* centerBox = mBoxQueue.at(1);
    centerBox->playAnimation("active", false);
    centerBox->setPlayEndCallback([=](Ref* sender, std::string animeName){
        if(animeName == "active"){
            auto reward = LuckyDrawManager::instance()->generateReward();
            LuckyDrawManager::instance()->collectReward(reward,mIsFreeDraw);
            setReward(reward);
            mAnimeLayer->openGacha();
        }
    });
    
}

void LuckyDrawLayer::onAnimationDone()
{
    mRewardPanel->setScale(0.1);
    ScaleTo *scaleto = ScaleTo::create(0.5, 1.0);
    CallFunc *callFunc = CallFunc::create([=]{
        mDoubleUpBtn->addClickEventListener([=](Ref*){
            if(mAdIsPlayed){
                return;
            }
            mAdIsPlayed = playVideoAd();
        });
    });
    
    Sequence *seq = Sequence::create(scaleto,callFunc, NULL);
    mRewardPanel->runAction(seq);

    mRewardPanel->setVisible(true);
}

bool LuckyDrawLayer::playVideoAd()
{
	return AdManager::instance()->playVideoAd(AdManager::AdFreeGacha,
									   [&](bool isFinished){
										   mDoubleUpBtn->setVisible(false);
										   handleDoubleUpReward();
									   });

	// Old Code 
//    // Android and iOS ad colony behaviour
//    //	in IOS, sequence is rewarded -> finished
//    //	in Android, sequence is finished -> rewarded
//    
//    // Note: We can use Reward for Revive, because it has a daily cap!!
//    // Show Video Ad
//    AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
//        mDoubleUpBtn->setVisible(false);
//        handleDoubleUpReward();
//    });
//    
//    AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
//        
//    });
//    
//    bool isOkay = AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
//    
//    //    Analytics::instance()->logGameContinue("continue_with_ad", 2200);	// 15 stars
//    
//    return isOkay;
}

void LuckyDrawLayer::handleDoubleUpReward()
{
    if(mReward == nullptr){
        return;
    }
    
    LuckyDrawManager::instance()->collectRewardForFree(mReward, mIsFreeDraw);
    
    Text* itemText = mRewardPanel->getChildByName<Text*>("rewardText");
    
    int count = mIsFreeDraw ? mReward->getFreeQuantity() : mReward->getQuantity();
    std::string name = mReward->getItemName();
    std::string displayFormat = name + " x %d";
    
    AnimationHelper::popTextEffect(this, itemText, count, count*2, NULL, 0.5, 1.0, displayFormat);

}

void LuckyDrawLayer::setReward(LuckyDrawReward *reward)
{
    if(!mRewardPanel){
        return;
    }
    mReward = reward;
    
    Sprite* raritySprite = mRewardPanel->getChildByName<Sprite*>("raritySprite");
    Sprite* itemSprite = mRewardPanel->getChildByName<Sprite*>("rewardSprite");
    Text* itemText = mRewardPanel->getChildByName<Text*>("rewardText");
    
    LuckyDrawReward::RewardType type = reward->getRewardType();
    int id = reward->getItemID();
    int count = mIsFreeDraw ? reward->getFreeQuantity() : reward->getQuantity();
    std::string name = reward->getItemName();
    
    std::string raritySpriteName = getRaritySpriteByType(type);
    raritySprite->setTexture(raritySpriteName);
    
    std::string itemSpriteName = getItemSprite(type,id);
    itemSprite->setTexture(itemSpriteName);
    if(type == LuckyDrawReward::RewardType::RewardStar){
        itemSprite->setScale(1.0);
    }else if (type == LuckyDrawReward::RewardType::RewardBooster){
        itemSprite->setScale(1.0);
    }else if (type == LuckyDrawReward::RewardType::RewardFragment){
        itemSprite->setScale(1.0);
    }
    
    itemText->setString(name+StringUtils::format(" x %d",count));
}

std::string LuckyDrawLayer::getRaritySpriteByType(LuckyDrawReward::RewardType type)
{
    switch (type) {
        case LuckyDrawReward::RewardType::RewardBooster:
            return "guiImage/gacha_title3.png";
            
        case LuckyDrawReward::RewardType::RewardFragment:
            return "guiImage/gacha_title2.png";
            
        case LuckyDrawReward::RewardType::RewardStar:
            return "guiImage/gacha_title1.png";
            
        default:
            return "";
    }
}

std::string LuckyDrawLayer::getItemSprite(LuckyDrawReward::RewardType type, int itemID)
{
    switch (type) {
        case LuckyDrawReward::RewardType::RewardBooster:{
            if(itemID == 5){
                return "guiImage/ui_booster_item3.png";
            }else if (itemID == 7){
                return "guiImage/ui_booster_item2.png";
            }else if (itemID == 6){
                return "guiImage/ui_booster_item5.png";
            }else if (itemID == 3){
                return "guiImage/ui_booster_item1.png";
            }else if (itemID == 9){
                return "guiImage/ui_booster_item4.png";
            }
        }
            
        case LuckyDrawReward::RewardType::RewardFragment:
            return StringUtils::format("fragment/ui_fragment_%02d.png", itemID);
            
        case LuckyDrawReward::RewardType::RewardStar:
            return "guiImage/ui_shop_item_06.png";
            
        default:
            return "";
    }
}

void LuckyDrawLayer::setOnExitCallback(std::function<void ()> callback)
{
    mOnExit = callback;
}
