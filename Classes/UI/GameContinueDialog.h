//
//  GameContinueDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 6/10/2016.
//
//

#ifndef GameContinueDialog_hpp
#define GameContinueDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

#include <time.h>

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class Price;
class CircularMaskNode;

class GameContinueDialog : public LayerColor
{
public:
	enum GameContinueOption {
		GameContinueOptionWatchAd,
		GameContinueOptionUseStar,
		GameContinueOptionExit,
	};
	
	typedef std::function<void(Ref *, GameContinueOption)> GameContinueCallback;
	
public:
	CREATE_FUNC(GameContinueDialog);
	
	
	GameContinueDialog();
	
	//
	virtual bool init();
	void setCallback(const GameContinueCallback &callback);
	void closeDialog();
    
    void update(float delta);
	
	virtual void onEnter();
	virtual void onExit();
	
	void hideWatchAd();
    
    void updateStars();
	
#pragma mark - Game Continue Star Requirement & reviveWithCoin
public:
	void setReviveCost(Price value);
	int getReviveCost();
	void onReviveWithCoin();
private:
	bool haveEnoughCoin();
	int mReviveCost;
	
	//	virtual void onEnter();
private:
	void setupGUI(const std::string &csbName);
	void setupAnimation(const std::string &csbName);
    
    void updateMapInfo();
	
	void onWatchAdClicked();
	bool playVideoAd();
	
	void doCallback(GameContinueOption opt);
	
	void showAddCoinDialog();
	void runAnimation(const std::string &name);
	
private:
	Button *mWatchAdButton;
	Button *mUseStarButton;
	Button *mExitButton;
	Node *mMainPanel;
    Node *mAlertLayer;
    Text *mStarCountText;
    Text *mTotalStarCountText, *mStarCostText;
    Text *mMapInfoText;
    CircularMaskNode *mClockSprite;
    float mRemainTime;
    bool mCounterActive;
	
	cocostudio::timeline::ActionTimeline *mTimeline;
	
	GameContinueCallback mCallback;
};

#endif /* GameContinueDialog_hpp */
