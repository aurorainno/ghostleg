//
//  VerticalLineNode.cpp
//  GhostLeg
//
//  Created by Ken Lee on 18/10/2016.
//
//

#include "RouteLineNode.h"
#include "ViewHelper.h"
#include "ShaderHelper.h"
#include "StringHelper.h"
#include "GeometryHelper.h"

const float kLineThickness = 0.5f;
const Color4F kMainColor(0.4275,1.0000,0.9882, 1.0);

const char *kRouteImage = "general/route_line.png";

#pragma mark - Main Class implementation
RouteLineNode::RouteLineNode()
: ui::Layout()
, mStart(Vec2::ZERO)
, mEnd(Vec2::ZERO)
, mUseDrawNode(true)
, mDrawNode(nullptr)
, mThickness(kLineThickness)
, mLineColor(kMainColor)
, mSegmentScale(1)
, mOpacity(255)
, mRouteLineImage(kRouteImage)
{
	
}

bool RouteLineNode::init()
{
	if(ui::Layout::init() == false) {
		return false;
	}
	
	setAnchorPoint(Vec2::ZERO);	// same as Layer
	
	// DrawNode
	mDrawNode = DrawNode::create();
	mDrawNode->setPosition(Vec2::ZERO);
	addChild(mDrawNode);
	
	
	// Panenl
	mPanel = ui::Layout::create();
	mPanel->setPosition(Vec2::ZERO);
	mPanel->setClippingEnabled(false);
	addChild(mPanel);
	
	return true;
}

void RouteLineNode::updateLine()
{
	if(mUseDrawNode) {
		updateLineByDrawNode();
	} else {
		updateLineBySprite();
	}
}

void RouteLineNode::updateLineByDrawNode()
{
	mDrawNode->setVisible(true);
	mPanel->setVisible(false);
	ViewHelper::drawLine(mDrawNode, mStart, mEnd, mThickness, mLineColor);
}

void RouteLineNode::setShader(const std::string &shaderName)
{
	
	std::string vsh = "shader/" + shaderName + ".vsh";
	std::string fsh = "shader/" + shaderName + ".fsh";
	
	if(mUseDrawNode) {
		aurora::ShaderHelper::setShaderToDrawNode(mDrawNode, vsh, fsh);
	}
	
}

void RouteLineNode::updateSegmentInfo()
{
	Sprite *sprite = Sprite::create(mRouteLineImage);		// origin size: 26 x 188
	sprite->setScale(mSegmentScale);

	mTextureRect = sprite->getTextureRect();		// origin:0 size: 26x188
	mSegmentSize = sprite->getContentSize() * mSegmentScale;
	
	mLength = mStart.distance(mEnd);
	mNumSegment = (int) ceilf(mLength / mSegmentSize.height);
	
	//log("Debug: mLength=%f mNumSegment=%d", mLength, mNumSegment);
}

void RouteLineNode::addLineSegment(const Vec2 &start, float requireLen)
{
	//updateSegmentInfo();
	
	Sprite *sprite = Sprite::create(mRouteLineImage);		// origin size: 26 x 188
	sprite->setScale(mSegmentScale);
	sprite->setAnchorPoint(Vec2(0.5f, 0));
	sprite->setPosition(start);
	
	if(requireLen < mSegmentSize.height) {
		// need to adjust the TextureRect
		Rect rect = mTextureRect;
		rect.size.height = requireLen / mSegmentScale;
		rect.origin.y = mTextureRect.size.height - rect.size.height;
		
		sprite->setTextureRect(rect);
	}
	
	mPanel->addChild(sprite);
}

void RouteLineNode::testLineSegment()
{
	addLineSegment(Vec2(160, 200), 100);	//
}

std::string RouteLineNode::info()
{
	std::string result = "";
	
	result = StringUtils::format("segmentSize=(%0.4f,%0.4f",
								 mSegmentSize.width, mSegmentSize.height);
	
	
	return result;
}

void RouteLineNode::updateLineBySprite()
{
	mDrawNode->setVisible(false);
	mPanel->setVisible(true);
	
	updateSegmentInfo();
	
	//log("numSegment=%d ", mNumSegment);

	mPanel->removeAllChildren();
	mPanel->setPosition(mStart);
	mPanel->setContentSize(Size(mSegmentSize.width, mLength));		// the panel size is updated
	//ViewHelper::
	
	Vec2 pos = Vec2::ZERO;
	int remainLength = mLength;
	for(int i=0; i<mNumSegment; i++) {
		addLineSegment(pos, remainLength);
		
		pos.y += mSegmentSize.height;
		
		remainLength -= mSegmentSize.height;
	}
	
	// Modify the angle of the panel
	float deg = 90 - aurora::GeometryHelper::findAngleDegree(mStart, mEnd);
	if(deg < 0) {
		deg += 360;
	}
	// log("debug: deg=%f", deg);
	//mPanel->setRotation(45);
	mPanel->setRotation(deg);
}


void RouteLineNode::setOpacity(GLubyte opacity)
{
	mOpacity = opacity;
//	log("opacity=%d", opacity);
	if(mUseDrawNode) {
		mDrawNode->setOpacity(opacity);
	} else {
		for(Node *node : mPanel->getChildren()) {
			node->setOpacity(opacity);
		}
	}
}

GLubyte RouteLineNode::getOpacity()
{
//	log("getOpacity=%d", mOpacity);
	return mOpacity;
}
