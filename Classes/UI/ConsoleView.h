//
//  ConsoleView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 5/10/2016.
//
//

#ifndef ConsoleView_hpp
#define ConsoleView_hpp

#include <stdio.h>

#include "cocos2d.h"
// #include "extensions/cocos-ext.h"
#include <string>
#include <vector>
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"
#include "VisibleRect.h"
USING_NS_CC;

class ConsoleView : public LayerColor
{
public:
	static ConsoleView *create(const Size &contentSize = VisibleRect::getVisibleRect().size);
	
	ConsoleView();
	~ConsoleView();
	
	bool initWithSize(const Size &contentSize);
	
	CC_SYNTHESIZE(Color4B, mBackgroundColor, BackgroundColor);
	CC_SYNTHESIZE(Color3B, mTextColor, TextColor);
	CC_SYNTHESIZE(std::string, mFontName, FontName);
	CC_SYNTHESIZE(int, mFontSize, FontSize);
	
	CC_SYNTHESIZE(int, mHeaderHeight, HeaderHeight);
	CC_SYNTHESIZE(Color4B, mHeaderColor, HeaderColor);
	
	
	void scrollToTop(float duration=0);
	
#pragma mark - logging
public:
	void append(const std::string &msg);
	void append(const char * format, ...);
	void append(const char *format, va_list args);
	
	void setTitle(const std::string &title);
	
	void clear();
	
#pragma mark - Callback
public:
	void setCloseCallback(const std::function<void(ConsoleView *)> &callback);
	
protected:
	void handleClose();
	std::function<void(ConsoleView *)> mCloseCallback;
	
#pragma mark - Setup
protected:
	void setupScrollView();
	void setupHeader();
	
protected:
	void setConsoleContent(const std::string &msg);
	ui::Button *createButton(const std::string &title, const Size &size);
	
protected:
	ui::ScrollView *mScrollView;
	LayerColor *mScrollContentLayer;
	LayerColor *mHeaderLayer;
	ui::Text *mContentText;
	ui::Text *mTitleText;
	
	std::string mContent;
};



#endif /* ConsoleView_hpp */
