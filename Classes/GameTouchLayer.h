//
//  GameTouchLayer.h
//  GhostLeg
//
//  Created by Gavin Chiu on 16/3/16.
//
//

#include <stdio.h>
#include <vector>
#include "cocos2d.h"

#include "XTLayer.h"

#ifndef GameTouchLayer_h
#define GameTouchLayer_h




USING_NS_CC;

class GameTouchLayer : public XTLayer
{
public:
	enum Type {
		Tap,
		Slide,
	};
public:
	typedef std::function<void(Ref *sender,
			const Vec2& start, const Vec2 &end)> GameTouchDrawLineCallback;
	
	
public:
    GameTouchLayer();
    CREATE_FUNC(GameTouchLayer);
    virtual bool init();

	CC_SYNTHESIZE(Type, mType, Type);
	CC_SYNTHESIZE(bool, mEnable, Enable);
	
	void reset();		// cancel every drop but not confirm
	
	void update(float delta);
	
	// Slope mode
    void showDrawingSlopeLine(Vec2 startPoint, Vec2 endPoint);
    void showTouchIndicator(Vec2 touchedPoint);
    void hideTouchIndicator();
    void handleSlopeEnd(const Vec2 &pos);
    Vec2 getIntersectPoint(const Vec2 &initPos,const Vec2 &pos);
    void clearTouchIndicator();

	float getOffsetY();
    void setOffsetY(float y);
	void setDrawLineCallback(const GameTouchDrawLineCallback &callback);

	void resetTouchInfo();
	
	CC_SYNTHESIZE(Vec2, mSlopeLineStartPoint, SlopeLineStartPoint);
    CC_SYNTHESIZE(Vec2, mSlopeLineEndPoint, SlopeLineEndPoint);
	
private:
	void handleDropLine(const Vec2 &start, const Vec2 &end);
	void resetData();
	
private:
    
    DrawNode *mTouchLineLayer;			
    Sprite *mTouchPointSprite;
    Sprite *mEndPointSprite;

private:
	
	void handleSlopeStart(const Vec2 &pos);
	void handleSlopeMove(const Vec2 &pos);
	
	void handleTapLine(const Vec2 &pos);
	
	void addGameMapTouchListener();

	
	void updateLineStartPosition();
	
	virtual bool onTouchBegan(Touch *touch, Event* event);
	
	
	
	// Easy touch callbacks
	virtual void xtTouchesBegan(Vec2 position);
	virtual void xtTouchesMoved(Vec2 position);
	virtual void xtTouchesEnded(Vec2 position);
	virtual void xtTapGesture(Vec2 position);
	virtual void xtDoubleTapGesture(Vec2 position);
	virtual void xtSwipeGesture(XTTouchDirection direction, float distance, float speed);
	
public:
	std::string statInfo();
	
//	virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
//    virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
//    virtual void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
//    virtual void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);
//    
private:
	bool mIsStarted;
    Vec2 mDeltaPos;
	Vec2 mLastTouchPos;
	
	float mScreenYWhenStart;
	float mOffsetYWhenStart;
	float mMapOffsetY;
	
	bool mIsTouching;
	float mTimeTouched;
	bool mHasBraked;
	
	GameTouchDrawLineCallback mDrawLineCallback;
};

#endif /* GameTouchLayer_h */
