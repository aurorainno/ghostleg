//
//  NotEnoughCoinDialog.h
//  GhostLeg
//
//  Created by Calvin on 13/6/2016.
//
//

#ifndef NotEnoughCoinDialog_h
#define NotEnoughCoinDialog_h

#include "cocos2d.h"
#include <stdio.h>
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class NotEnoughCoinDialog : public Layer
{
	

public:
	CREATE_FUNC(NotEnoughCoinDialog);
	
	virtual bool init();
	
	NotEnoughCoinDialog();
	
	void setupUI(Node *rootNode);
	
	
	void closeDialog();
	
	void showRewardVideo();
	
	void rewardStar();
	
	void setCloseCallback(const std::function<void(bool anyUpdate)> &callback);

	bool isAdRewardAvailable();
	
private:
	std::function<void(bool anyUpdate)> mCloseCallback;
	
	bool mIsStarAdded;
   // VideoAdsCallback mCallback;
};

#endif /* NotEnoughCoinDialog_h */
