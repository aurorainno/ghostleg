//
//  MasteryScene.cpp
//  GhostLeg
//
//  Created by Calvin on 17/5/2016.
//
//

#include <stdio.h>
#include "MasteryScene.h"
#include <MasteryItemView.h>
#include <Mastery.h>
#include "GameManager.h"
#include "MasteryManager.h"
#include "cocostudio/CocoStudio.h"
#include "UserGameData.h"
#include "StageParallaxLayer.h"
#include "CommonMacro.h"
#include "GameSound.h"
#include "NotEnoughCoinDialog.h"
#include "AdManager.h"
#include "Analytics.h"

USING_NS_CC;

Scene* MasterySceneLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = MasterySceneLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool MasterySceneLayer::init(){
    
      if ( !Layer::init() )
    {
        return false;
    }
    
    mRootNode = CSLoader::createNode("MasteriesScene.csb");
    mParallaxLayer = StageParallaxLayer::create();
    addChild(mParallaxLayer);
    addChild(mRootNode);
    setupUI();
    
    return true;
}

//void MasterySceneLayer::onEnter(){
//    Layer::onEnter();
//    GameSound::playMusic(GameSound::Title);
//}
//
//void MasterySceneLayer::onExit(){
//    Layer::onExit();
//    GameSound::stop();
//}

//refresh the mastery UI scene
void MasterySceneLayer::refreshPage(){
    for(int i=0;i<mMasteryItems.size();i++){
        MasteryItemView* currentItem = mMasteryItems.at(i);
        int id = currentItem->getMasteryId();
        currentItem->setMastery(GameManager::instance()->getMasteryManager()->getMasteryData(id));
    }
    updateCoins();
    
}

void MasterySceneLayer::update(float delta)
{
    static float offset = 0;
    
    
    mParallaxLayer->setScrollY(offset);
    
    offset += delta * 100;
}

//update the stars
void MasterySceneLayer::updateCoins(){
    char temp[100];
    sprintf(temp, "%d", GameManager::instance()->getUserData()->getTotalCoin());
    mStars->setString(temp);
}

void MasterySceneLayer::onEnter(){
    Layer::onEnter();
 //   AdManager::instance()->showBannerAd("Banner");
    
    scheduleUpdate();
    // logging
    Analytics::instance()->logScreen(Analytics::Screen::mastery);
}

void MasterySceneLayer::onExit(){
    AdManager::instance()->hideBannerAd();
    Layer::onExit();
}

void MasterySceneLayer::setupUI(){
    FIX_UI_LAYOUT(mRootNode);
    
	// Coin Text
    mStars = (Text*) mRootNode->getChildByName("totalCoinText");
    updateCoins();
   	
    Vector<MasteryData *> masteryList = GameManager::instance()->getMasteryManager()->getMasteryArray();
	Node *subPanel = mRootNode->getChildByName("testPanel");
    Button *backBtn = (Button*)mRootNode->getChildByName("backBtn");

	// Panel for MasteryItemView
	float spacingW = 20;
	float spacingH = 20;
	float marginX = (subPanel->getContentSize().width - spacingW - 2 * 125) / 2;	// 125 is MasteryItemView's width
	float marginY = 30;
	
    
	//set lisenter for back button
    backBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
         case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
         case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->popScene();
                break;
        }
    });
    
   
	Vec2 pos = Vec2(marginX, subPanel->getContentSize().height - marginY);
	
	for(int i=0; i<masteryList.size(); i++){
		MasteryData *masteryData = masteryList.at(i);
		
		MasteryItemView *itemView = MasteryItemView::create();
		
        mMasteryItems.pushBack(itemView);
        
		Vec2 leftBottomPos = Vec2(pos.x, pos.y - itemView->getContentSize().height);
		
		itemView->setPosition(leftBottomPos);
		itemView->setMastery(masteryData);
		subPanel->addChild(itemView);
        
    
		
		//setup callback function for upgrade btn
        itemView->setCallback([&](MasteryItemView *view, int masteryID, int upgradeLevel) {
            log("masteryID=%d upgradeLevel=%d", masteryID, upgradeLevel);
            
            if(GameManager::instance()->getMasteryManager()->upgradeMastery(masteryID) == -1){      // TODO: using #define or constant to replace -1
				showAddCoinDialog();
			} else {
				updateCoins();
				view->updateUI();
			}
        });

        
		// Calculate Next view position
		if( i%2 == 0) {	// At left column
			pos.x += itemView->getContentSize().width + spacingW;
			
		} else {		// At right column
			pos.x = marginX;
			pos.y -= itemView->getContentSize().height + spacingH;
		}
	}
}

void MasterySceneLayer::showAddCoinDialog()
{
	NotEnoughCoinDialog *dialog = NotEnoughCoinDialog::create();
	addChild(dialog);
	
	dialog->setCloseCallback([&](bool isUpdated) {
		if(isUpdated) {
			updateCoins();
		}
	});
}


// Note: Handling Upgrade Button
//		1. itemView setCallback (@see void GameOverDialog::setOKListener(const Widget::ccWidgetClickCallback &callback)
//		2. Check enough star  (use ViewHelper::showFadeAlert)
//		3. Do upgrade (MasteryManager::upgraeMastery(masteryID) (ref:
//		4. Refresh the click itemView
//		5. Refresh coin value










