//
//  CreditScene.hpp
//  GhostLeg
//
//  Created by Ken Lee on 7/7/2016.
//
//

#ifndef CreditScene_hpp
#define CreditScene_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "AstrodogClient.h"

USING_NS_CC;

class ParallaxLayer;
class FBProfilePic;

using namespace cocos2d::ui;

class CreditSceneLayer : public Layer, public AstrodogClientListener{
public:
	static Scene* createScene();
	
	CREATE_FUNC(CreditSceneLayer);
	
	virtual bool init();
	virtual void onEnter();
	virtual void onExit();
	virtual void update(float delta);

	void showSecretPanel();
	void hideSecretPanel();
	std::string getInputSecret();
	
private:
	void setupUI(Node *rootNode);
	void setupButtonListener(Node *mainPanel);
	
	void updateVersion();
	void attemptToShowSecret();
	void checkSecret();
	
	void gotoFacebook();
	void gotoTwitter();
    
    void updateDiamond();
    void updateStars();
    void setupFbProfile();
    
    void onFBConnect(int status);
	
private:
	ParallaxLayer *mParallaxLayer;
	Button *mBackButton;
	Node *mSecretPanel;
	TextField *mSecretTextField;
	Button *mVersionButton;
	Button *mSecretConfirmButton;
	Button *mSecretBackButton;
	int mSecretClickCount;
    
    Text* mDiamondText;
    Text* mStarText;
    
    Button* mFbBtn;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
};


#endif /* CreditScene_hpp */
