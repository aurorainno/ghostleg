//
//  NotEnoughCoinDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 13/6/2016.
//
//

#include "cocostudio/CocoStudio.h"
#include "CommonMacro.h"
#include "ui/CocosGUI.h"
#include "NotEnoughCoinDialog.h"
#include "CoinShopScene.h"

#include "AdManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "ViewHelper.h"

#include "Analytics.h"

const int kRewardCoin = 100;

NotEnoughCoinDialog::NotEnoughCoinDialog():
mCloseCallback(),
mIsStarAdded(false)
{
}


bool NotEnoughCoinDialog::init()
{
	if(Layer::init() == false) {
		return false;
	}
	
	Node *rootNode = CSLoader::createNode("NotEnoughCoinsLayer.csb");
	
	setupUI(rootNode);
	
	addChild(rootNode);
	
	return true;
}

void NotEnoughCoinDialog::setupUI(Node *rootNode)
{
	FIX_UI_LAYOUT(rootNode);
	
	//
	ui::Button *backButton = (Button *) rootNode->getChildByName("backBtn");
	ViewHelper::addBackButtonListener(this, backButton, ViewHelper::BackHandleType::RemoveFromParent);
	
	// Handling for Shop
	Button *shopBtn = (Button*) rootNode->getChildByName("shopBtn");
	
	shopBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type){
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				Scene *coinShopScene = CoinShopSceneLayer::createScene();
				Director::getInstance()->pushScene(coinShopScene);
				closeDialog();
			}
		}
	});
	
	Button *movieButton = (Button*) rootNode->getChildByName("movieBtn");
	
	movieButton->addTouchEventListener([&,this](Ref *sender, Widget::TouchEventType type){
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				showRewardVideo();
			}
		}
	});
	
	if(isAdRewardAvailable() == false) {
		movieButton->setVisible(false);
	}
}

bool NotEnoughCoinDialog::isAdRewardAvailable()
{
	return AdManager::instance()->isAdEnabled();
}
//void NotEn

void NotEnoughCoinDialog::closeDialog()
{
	if(mCloseCallback) {
		mCloseCallback(mIsStarAdded);
	}
	
	removeFromParent();
}

void NotEnoughCoinDialog::rewardStar()
{
	int rewardCoin = kRewardCoin;
	GameManager::instance()->getUserData()->addCoin(rewardCoin);		// TODO: Change to not hard code!!!
	mIsStarAdded = true;
}

void NotEnoughCoinDialog::showRewardVideo()
{
	AdManager::instance()->setVideoFinishedCallback([&](bool isPlayed){
		closeDialog();
	});

	AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded) {
		if(isRewarded) {
			rewardStar();
		}
	});
	
	AdManager::instance()->showVideoAd(AdManager::VideoAd::GainStar);
}



void NotEnoughCoinDialog::setCloseCallback(const std::function<void(bool)> &callback)
{
	mCloseCallback = callback;
}
