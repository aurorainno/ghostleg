//
//  CoinShopScene.h
//  GhostLeg
//
//  Created by Calvin on 25/5/2016.
//
//

#ifndef CoinShopScene_h
#define CoinShopScene_h
#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include "CommonType.h"
#include "IAPStatusCode.h"
#include "CoinShopItemView.h"
#include "FBProfilePic.h"
#include "AstrodogClient.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;



class CoinShopSceneLayer : public Layer,  public AstrodogClientListener
{
public:
	enum ItemType {
		ShopItemTypeStar,
		ShopItemTypeCandy,
        ShopItemTypeDiamond,
		ShopItemTypeNoAd,
		ShopItemTypeOther,
	};
public:
    CoinShopSceneLayer();
    static Scene* createScene();
	
	CC_SYNTHESIZE(bool, mIsDialogMode, IsDialogMode);
    
    void setAsScene();
    virtual void onEnter();
    virtual void onExit();
    virtual bool init();
    virtual void update(float delta);
    void refreshPage();
    // implement the "static create()" method manually
    CREATE_FUNC(CoinShopSceneLayer);
    
    void setupCallback();
    void addProductItemTable();
    void showProgress(std::string msg);
    void hideProgress();
    void showErrorMessage(std::string msg); // Sorry shop is not available
    void showAlert(std::string title, std::string info, ItemType type=ShopItemTypeOther);
    void closeAlert();
//    void addBlueActionItems();

    CoinShopItemView * getCoinShopItemByName(std::string name);
	
	void setCloseCallback(const std::function<void()> &callback);
    void setOnPurchaseCallback(const std::function<void()> &callback);
	
	void handleGameProductSuccess(GameProduct *product);
    
    void handleUpdateDiamondAndStars();
    void handleNotEnoughMoney();
    
    void setupFbProfile();
	
private:
    void setupUI();
    void updateCoins();
    void updateDiamond();
	void closeView();
	void updateForEventUI();
	
	void handleProductListLoaded(IAPStatusCode status, const std::string &returnMsg);

#pragma mark - Add Coin Logic
	void addFreeCoin(int amount);
	
#pragma mark - AstrodogClient
public:	// AstrodogClientListener
    virtual void onFBConnect(int status);
	
#pragma mark - Serve Game Product
private:
	void handleRemoveAd(GameProduct *product);
	void handleAddStars(GameProduct *product, int amount);
	void handleAddCandy(GameProduct *product, int amount);
	void handleAddDiamond(GameProduct *product, int amount);
    void handlePlayAdAddStar();
    
private:
    Node *mTopPanel;
    
    Button *mFbConnectBtn;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
    Text* mStarText;
    Text* mDiamondText;
    
    Text *mStars, *mErrmsg;
	
	Sprite *mIconCandyStick;
	Text *mCandy;
    
    Text *mDiamond;
	
    Node *mRootNode, *mProgressLayer, *mAlertLayer;
    Vector<CoinShopItemView *> mCoinShopItems;
    float mTimeElasped;
    bool mErrIsShown;
    std::function<void()> mCloseCallback;		// when using layer directly
    std::function<void()> mOnPurchaseCallback;
    std::function<void()> mOnCloseAlertCallback;
};

#endif /* CoinShopScene_h */
