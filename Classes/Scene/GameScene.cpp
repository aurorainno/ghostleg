//
//  GameScene.cpp
//  GhostLeg
//
//  Created by Ken Lee on 18/3/2016.
//
//

#include "GameScene.h"
#include "MainScene.h"
#include "Constant.h"
#include "CommonMacro.h"
#include "cocostudio/CocoStudio.h"
#include "TDDHelper.h"
#include "GameWorld.h"
#include "AnimationHelper.h"
#include "ViewHelper.h"
#include "GameOverDialog.h"
#include "PauseDialog.h"
#include "Player.h"
#include "GameSound.h"
#include "DebugInfo.h"
#include "StringHelper.h"
#include "StageParallaxLayer.h"
#include "ModelLayer.h"
#include "ItemEffect.h"
#include "GameRes.h"
#include "AdManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "Analytics.h"
#include "DebugInfo.h"
#include "EffectLayer.h"
#include "DeviceHelper.h"
#include "CircularMaskNode.h"
#include "StarAwardLayer.h"
#include "PlanetManager.h"
#include "DogManager.h"
#include "UnlockDogLayer.h"
#include "GameSound.h"
#include "DogSuggestionLayer.h"
#include "VisibleRect.h"
#include "BoosterControl.h"
#include "AnimationHelper.h"
#include "DeliveryCompleteNoticeView.h"
#include "NPCOrder.h"
#include "PlayerManager.h"
#include "NpcDistanceLayer.h"
#include "EndGameScene.h"
#include "NPCRating.h"
#include "StageManager.h"
#include "CheckPointDialog.h"
#include "PlayerGameResult.h"

#define kDistanceTextFontSize 36
#define kDistanceUnitFontSize 20

#define kTimeColorHurryUp	Color4B::RED
#define kTimeColorNormal	Color4B(255, 165, 0, 255)

using namespace cocostudio::timeline;

//
namespace {

}


//
Scene* GameSceneLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	GameSceneLayer *layer = GameSceneLayer::create();
	
	// add layer as a child to scene
	scene->addChild(layer);
	
	
	// return the scene
	return scene;
}

Scene *GameSceneLayer::createTutorialScene(bool startFromTitle)
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// ken: weird logic, need to refactor later
	PlanetManager::instance()->selectPlanet(0);
	StageManager::instance()->setCurrentStage(999);
	
	// 'layer' is an autorelease object
	GameSceneLayer *layer = GameSceneLayer::create();
	
	layer->setTutorialMode(true);
	layer->setTutorialFromTitle(startFromTitle);
	
	// add layer as a child to scene
	scene->addChild(layer);
	
	
	// return the scene
	return scene;
}



// on "init" you need to initialize your instance
bool GameSceneLayer::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}
	
	mTutorialMode = false;
	mTutorialFromTitle = false;
	mCheckPointDialog = nullptr;
	mCoinText = nullptr;
	mStartOnEnter = true;
	mPauseDialog = nullptr;
	mControl = nullptr;
	mLastBestDistance = 0;
	
	// Parallax
//	if(GameManager::instance()->shouldHideBackground()) {
//		mParallaxLayer = nullptr;
//	} else {
	
	
	
	//int stage = PlanetManager::instance()->getSelectedPlanet();
	StageData *stageData = StageManager::instance()->getCurrentStageData();
	int stageID = stageData == nullptr ? 1 : stageData->getStageID();
	int stageBg = stageData == nullptr ? 1 : stageData->getStageBg();
	
	// Setup background
	mParallaxLayer = StageParallaxLayer::create();
	mParallaxLayer->setShowRoute(true);
	mParallaxLayer->setShowBackground(GameManager::instance()->shouldHideBackground() == false);
	mParallaxLayer->setStage(stageBg);
	addChild(mParallaxLayer);
	
	mParallaxLayer->setMainNode(GameWorld::instance());
	
	GameWorld::instance()->setParallaxLayer(mParallaxLayer);
	
	GameWorld::instance()->setEventCallback(
			CC_CALLBACK_3(GameSceneLayer::handleWorldEvent, this));
	//}
	GameWorld::instance()->setStage(stageID);
	GameWorld::instance()->setMapRouteLine(stageBg);
	
	
	
	// Setup UI
	Node *rootNode = CSLoader::createNode("GameScene.csb");
	addChild(rootNode);
	
	setupUI(rootNode);
	
	// the effect layer need some position information
	setupEffectLayerPosition();
	
	// Setup Energy
	setupEnergy();
	
	// Reset the World Logic
	GameWorld::instance()->resetState();
	
	// Setup Planet Detail (Bg & Music)
	setupPlanet();
	
	// Set the booster
	setSelectedBooster(BoosterItemTimeStop);
	
	
	return true;
}


void GameSceneLayer::setupPlanet()
{
	// setup the background
//	int planetID = PlanetManager::instance()->getSelectedPlanet();
//    PlanetData* data = PlanetManager::instance()->getPlanetData(planetID);
//    std::string bg1 = data->getFirstBg();
//    std::string bg2 = data->getSecondBg();
//
//	if(mParallaxLayer != nullptr) {
//		mParallaxLayer->setBGSpeed(0.01f);
//		mParallaxLayer->setBackground(bg1, bg2, planetID);
//	}
//	
	
	// setup the bacground music
	// pick random music from 
}

void GameSceneLayer::setupUI(cocos2d::Node *mainPanel)
{
	FIX_UI_LAYOUT(mainPanel);
	
	//ViewHelper::setWidgetSwallowTouches(mainPanel, false);
	
	Node *bgSprite = mainPanel->getChildByName("bgSprite");
	if(bgSprite != nullptr) {
		bgSprite->setVisible(false);
	}
	
	// Setup the GameWorld
	mWorldNode = mainPanel->getChildByName("worldParent");
	if(mWorldNode != NULL) {
		//ViewHelper::setWidgetSwallowTouches(mWorldNode, false);
		////mWorldNode->addChild(GameWorld::instance());
		//GameWorld::instance()->setEventCallback(
		//				CC_CALLBACK_3(GameSceneLayer::handleWorldEvent, this));
	} else {
		log("wordParent is null");
	}
	
	mRootNode = mainPanel;
	
	// Setup UI Element
	
	// Modify the UI Panel
	Node *uiPanel = mainPanel->getChildByName("uiPanel");
	//ViewHelper::setWidgetSwallowTouches(uiPanel, false);
	mMainUIPanel = uiPanel;
	
	//
	Node *topPanel = uiPanel->getChildByName("topPanel");
	mTopUIPanel = topPanel;
    
    mTimePanel = topPanel->getChildByName("timePanel");
    mNpcPanel = topPanel->getChildByName("npcPanel");
    mBlockPanel = topPanel->getChildByName("blockPanel");
    
    mFinishGameSprite = (Sprite*) uiPanel->getChildByName("finishGameSprite");
	mFinishGameText = uiPanel->getChildByName<Text *>("finishGameText");
	
	// time
	mTimeText = (Text *) mTimePanel->getChildByName("timeText");
    mTimeUnitText = (Text *) mTimePanel->getChildByName("unitText");
    
    mNpcIcon = (Sprite*) mNpcPanel->getChildByName("npcIcon");
    mNpcName = (Text*) mNpcPanel->getChildByName("npcNameText");
    mDistanceText = (Text*) mNpcPanel->getChildByName("distanceText");
	mTimeUsedText = (Text*) mNpcPanel->getChildByName("timeUsedText");
    mMetersText = (Text*) mNpcPanel->getChildByName("distanceUnitText");
    mRewardText = (Text*) mNpcPanel->getChildByName("rewardCountText");
    
    mScoreText = (Text*) topPanel->getChildByName("scoreText");
    mScoreText->setString("0");
	
	mCoinText = (Text *) topPanel->getChildByName("coinText");
    mCoinText->setString("0");
    mInitStarScale = mCoinText->getScale();
	
	mPuzzleText = (Text *) topPanel->getChildByName("puzzleText");
	mPuzzleText->setString("0");
	
	
	
    mDebugText = (Text *) uiPanel->getChildByName("debugText");
    
    mSpeedText = (Text *) uiPanel->getChildByName("speedText");

	// Energy
	Node *energyPanel = mMainUIPanel->getChildByName("energyPanel");
	if(energyPanel) {
//		mEnergyBar = (LoadingBar*) energyPanel->getChildByName("energyBar");
//		mEnergyText = (Text *) energyPanel->getChildByName("energyText");
//		
//		// enemy particle
//		mEnergyParticle = (ParticleSystemQuad *) energyPanel->getChildByName("energyParticle");
//		mEnergyParticle->setVisible(false);
//		mEnergyParticle->setDuration(0.7);
        energyPanel->setVisible(false);
	}
	
	// Tap Hint
	mTapHintPanel = mainPanel->getChildByName("tapHintPanel");
	Node *animeNode = mTapHintPanel->getChildByName("tapAnimeNode");
	cocostudio::timeline::ActionTimeline *timeLine = CSLoader::createTimeline("tapAnime.csb");
	animeNode->runAction(timeLine);		// retain is done here!
	timeLine->play("tap", true);
	mTapHintPanel->setVisible(false);
	
	
    // Pause Button
    mPauseButton = (Button *) uiPanel->getChildByName("pauseButton");
    mPauseButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                //showPauseDialog();
                onPauseGame();
                break;
            }
        }
    });
	
	// CapsuleButton
	mCapsuleButton = (Button *) uiPanel->getChildByName("capsuleButton");
	mCapsuleButton->addClickEventListener([&](Ref *sender) {
		GameWorld::instance()->activateCapsule();
	});
	setupBoosterControl(mCapsuleButton->getPosition());
	
	// Dash Button
	setupDashButton(uiPanel);
	
    // Distance Bar
    mDistanceBarBG = (Sprite*) mTimePanel->getChildByName("distanceBar");
    
    CircularMaskNode *node = CircularMaskNode::create();
    node->setPosition(Vec2(mDistanceBarBG->getContentSize().width/2,mDistanceBarBG->getContentSize().height/2));
    mDistanceBarBG->addChild(node);
    
    Sprite *sprite = Sprite::create("guiImage/ui_meters_1.png");
    //    sprite->setScale(0.5);
    //    sprite->setContentSize(sprite->getContentSize() * 0.5);
    node->addChild(sprite);
    
    mCircularMask = node;
    mCircularMask->updateAngleByPercent(0);
    mTimeDigit = 1;
	
	// Some information
	std::string screenInfo = DebugInfo::infoScreen();
	screenInfo += "LayerSize: " + SIZE_TO_STR(getContentSize()) + "\n";
	screenInfo += "uiPanelSize:" + SIZE_TO_STR(uiPanel->getContentSize()) + "\n";
	screenInfo += "uiPanelPos:" + POINT_TO_STR(uiPanel->getPosition()) + "\n";
	screenInfo += "uiPanelRect:" + RECT_TO_STR(uiPanel->getBoundingBox()) + "\n";
	
	log("GameScreen: screenInfo:\n%s\n", screenInfo.c_str());
	
	if(! GameManager::instance()->isDebugMode()) {
		mDebugText->setVisible(false);
	}
    
    mCandyPanel = topPanel->getChildByName("candyPanel");
	mCandyPanel->setVisible(false);
    
    DeliveryCompleteNoticeView* view = DeliveryCompleteNoticeView::create();
    float posY = Director::getInstance()->getOpenGLView()->getVisibleSize().height;
    view->setPosition(Vec2(0,posY));
    addChild(view);
    mNoticeView = view;
	
	
	// Red Frame for CountDown Effect
	mClockCountDownFrame = mainPanel->getChildByName<ui::ImageView *>("countDownFrame");
	mClockCountDownFrame->setVisible(false);

	
	// Setup TimeExtend Panel
	setupTimeExtendPanel();
	
	// Setup Distance indicator
	setupNpcDistanceLayer();
	
	// The popup when player complete an order
	setupCheckPointDialog();
}

void GameSceneLayer::setTimeUpTextVisibility(bool isVisible)
{
   // mTimeUpSprite->setVisible(isVisible);		// No more!!
}

void GameSceneLayer::setTimeTextVisibility(bool isVisible)
{
    mTimePanel->setVisible(isVisible);
}

void GameSceneLayer::showDeliveryNotice()
{
    mNoticeView->showView();
}

void GameSceneLayer::setDeliveryTips(int tips)
{
    mNoticeView->setBonusTip(tips);
}


void GameSceneLayer::showDeliveryReward()
{
	mNoticeView->showReward();
}


void GameSceneLayer::setDeliveryReward(NPCOrder *order)
{
    mNoticeView->setReward(order);
}

void GameSceneLayer::closeDeliveryNotice()
{
    mNoticeView->closeView();
}

void GameSceneLayer::setDeliveryOnShowRewardCallback(std::function<void ()> callback)
{
    mNoticeView->setOnShowRewardCallback(callback);
}

void GameSceneLayer::setDeliveryOnCloseCallback(std::function<void ()> callback)
{
    mNoticeView->setOnCloseCallback(callback);
}

void GameSceneLayer::setupDashButton(Node *mainPanel)
{
	// Pause Button
	mDashButton = (Button *) mainPanel->getChildByName("dashButton");
	mDashButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type) {
			case ui::Widget::TouchEventType::BEGAN: {
				//showPauseDialog();
				//onPauseGame();
				log("DEBUG: Dash is ON");
				GameWorld::instance()->getPlayer()->setIsDashing(true);
				break;
			}
			case ui::Widget::TouchEventType::ENDED: {
				//showPauseDialog();
				//onPauseGame();
				GameWorld::instance()->getPlayer()->setIsDashing(false);
				log("DEBUG: Dash is OFF");
				break;
			}
		}
	});

}

void GameSceneLayer::resetCandyState()
{
    int planetID = PlanetManager::instance()->getSelectedPlanet();
    PlanetData *data = PlanetManager::instance()->getPlanetData(planetID);
    
    if(data->getPlanetType() == PlanetData::Event){
        mCandyPanel->setVisible(true);
        Text* candyText = mCandyPanel->getChildByName<Text*>("totalCandyText");
        candyText->setString("0");
    }else{
        mCandyPanel->setVisible(false);
    }

}

void GameSceneLayer::onEnter()
{
	Layer::onEnter();
	GameWorld::instance()->setGameUILayer(this);
	GameWorld::instance()->setParallaxLayer(mParallaxLayer);
	
	int stageID = PlanetManager::instance()->getSelectedPlanet();
	GameSound::playStageMusic(stageID);
	
	if(mStartOnEnter == false){
		return;
	}
	
    if(GameManager::instance()->shouldShowSpeed()){
        mSpeedText->setVisible(true);
    }else{
        mSpeedText->setVisible(false);
    }
    
	startGame();
	
	
	// Analytics
	Analytics::instance()->logGameStage();
	Analytics::instance()->resetPlayerHitData();
	LOG_SCREEN(mTutorialMode ? Analytics::scn_tutorial : Analytics::game);
}

void GameSceneLayer::onExit()
{
	GameWorld::instance()->resetState();
	GameWorld::instance()->setGameUILayer(nullptr);
	GameWorld::instance()->setParallaxLayer(nullptr);
	
    
	Layer::onExit();

}


void GameSceneLayer::handleWorldEvent(Ref *sender, GameWorldEvent event, float arg)
{
	switch (event) {
		case EventUpdateTime: {
			setTime(arg);
			updateDebugInfo();
            updateSpeedText();
            //updateScore();
			break;
		}
			
		case EventGameOver: {
			handleGameOver();
			break;
		}
            
        case EventUpdateBarEnergy: {
			updateEnergy((int)arg);		// arg is the latest energy
            break;
        }
			
		case EventUpdateCoin: {
			updateCoins();
            updateCandy();
			break;
		}
			
		case EventTapHintVisibility: {
			if(arg == 1) {
				showTapHint();
			} else {
				hideTapHint();
			}
			
			break;
		}
	}
}

void GameSceneLayer::setRemainDistance(float distance)
{
    const float posXArray[] = {70,80,90,100};
    distance = ceil(fabsf(distance));
    int numOfDigits = distance <= 0 ? 1 : floor (abs(log10 (distance))) + 1;
    float posX = posXArray[numOfDigits-1];
    
    mMetersText->setPositionX(posX);
    mDistanceText->setString(StringUtils::format("%d",(int)distance));
}

//void GameSceneLayer->set

void GameSceneLayer::setNpcInfo(NPCOrder *order)
{
    int npcId = order->getNpcID();
 //   int reward = order->getReward();
    std::string name = order->getName();
    
    std::string iconResName = StringUtils::format("npc/npc_icon_%03d.png",npcId);
    mNpcIcon->setTexture(iconResName);
    
//    mRewardText->setString(StringUtils::format("%d",reward));
    mNpcName->setString(name);
	
	setRemainDistance(order->getDistance());
}

void GameSceneLayer::showNpcAndTimeUI()
{
   // mTimePanel->setVisible(true);
    mNpcPanel->setVisible(true);
}

void GameSceneLayer::hideNpcAndTimeUI()
{
    mTimePanel->setVisible(false);
    mNpcPanel->setVisible(false);
}

void GameSceneLayer::showDialogue(GameSceneLayer::DialogueType type, float delay, float duration)
{
    int dialogId = (int) type;
    std::string resName = StringUtils::format("dialogue/ingame_dialog_%02d.png",dialogId);
    Sprite* dialogSprite = Sprite::create(resName);
    dialogSprite->setAnchorPoint(Vec2(0,1));
    dialogSprite->setPosition(Vec2(33,-2));
    dialogSprite->setScale(0.1);
    dialogSprite->setVisible(false);
    mTopUIPanel->addChild(dialogSprite);
    
    DelayTime* delayAction = DelayTime::create(delay);
    DelayTime* durationAction = DelayTime::create(duration);
    
    ScaleTo* scaleUp = ScaleTo::create(0.2, 0.65);
    ScaleTo* scaleDown = ScaleTo::create(0.05, 0.5);
    Sequence* PopUpAnimation = Sequence::create(scaleUp, scaleDown, NULL);
    
    ScaleTo* scaleUpTwo = ScaleTo::create(0.05, 0.65);
    ScaleTo* scaleDownTwo = ScaleTo::create(0.2, 0.1);
    Sequence* PopInAnimation = Sequence::create(scaleUpTwo, scaleDownTwo, NULL);
    
    CallFunc* turnOnCallback = CallFunc::create([=](){
        dialogSprite->setVisible(true);
    });
    
    CallFunc* turnOffCallback = CallFunc::create([=](){
        dialogSprite->setVisible(false);
    });
    
    RemoveSelf* remove = RemoveSelf::create();
    
    Sequence* finalSequence = Sequence::create(delayAction,turnOnCallback,PopUpAnimation,
                                               durationAction,PopInAnimation,turnOffCallback,
                                               remove,NULL);
    
    dialogSprite->runAction(finalSequence);
}

void GameSceneLayer::setTopPanelVisible(bool flag)
{
	mTopUIPanel->setVisible(flag);
}

void GameSceneLayer::setTime(float remainTime)
{
    if(!mTimeUnitText||!mTimeText){
        return;
    }
    
    float totalTime = GameWorld::instance()->getTotalTime();
    float percentage = (remainTime/totalTime) * 100;
    if(percentage<0){
        percentage = 0;
    }
    
    remainTime = remainTime <= 0 ? 0 : ceil(remainTime);
    
    //play animation when remain time <= 9
    if(remainTime<=9){
        int currentTime;
        sscanf(mTimeText->getString().c_str(),"%d",&currentTime);
        
        if(currentTime != (int)remainTime){
            AnimationHelper::runPopupAndPopbackAction(mTimeText, 0.5, 0.6, 0.4);
        }
    }
    
    
//    //adjust the font size
//    int numOfDigits = remainTime == 0 ? 1 : floor (abs(log10 (remainTime))) + 1;
//    if(mTimeDigit!=numOfDigits){
//        if(numOfDigits > mTimeDigit){
//            mTimeText->setFontSize(mTimeText->getFontSize()*0.8);
//            mTimeUnitText->setFontSize(mTimeUnitText->getFontSize()*0.8);
//        }else if(numOfDigits < mTimeDigit){
//            mTimeText->setFontSize(mTimeText->getFontSize()*1.25);
//            mTimeUnitText->setFontSize(mTimeUnitText->getFontSize()*1.25);
//        }
//        mTimeDigit = numOfDigits;
//    }
//    
    //set the value
	log("DEBUG: remainTime=%f", remainTime);
    mTimeText->setString(StringUtils::format("%.0f", remainTime));
    
    if(!mCircularMask){
        return;
    }
	
    mCircularMask->updateAngleByPercent(percentage);
}

void GameSceneLayer::showBlockCounter(BlockType blockerType, int count)
{
    mBlockPanel->setVisible(true);
    Sprite* iconSprite = mBlockPanel->getChildByName<Sprite*>("iconSprite");
    Text* countText = mBlockPanel->getChildByName<Text*>("countText");
    countText->setString(StringUtils::format("%d",count));
    countText->setVisible(true);
    
    switch (blockerType) {
        case BlockTypeEnemy:
            countText->setVisible(false);
            iconSprite->setTexture("guiImage/ingame_shield1.png");
            
        case BlockTypeLaser:
            iconSprite->setTexture("guiImage/ingame_shield1.png");
            break;
        
        case BlockTypeBullet:
            iconSprite->setTexture("guiImage/ingame_shield2.png");
            break;
        
        case BlockTypeBomb:
            iconSprite->setTexture("guiImage/ingame_shield3.png");
            break;
            
        default:
            iconSprite->setTexture("guiImage/ingame_shield1.png");
            break;
    }
}

void GameSceneLayer::hideBlockCounter()
{
    mBlockPanel->setVisible(false);
}

void GameSceneLayer::updateCoinWithValue(int newTotal, bool showAnimation)
{
	if(mCoinText == nullptr) {
		return;
	}
	
	std::string coinStr = StringUtils::format("%d", newTotal);
	
	if(showAnimation) {
		popValue(mCoinText,coinStr);
	} else {
		mCoinText->setString(coinStr);
	}
}

void GameSceneLayer::updateCoins(bool showAnimation)
{
    int coin = GameWorld::instance()->getPlayerCollectedCoins();
	updateCoinWithValue(coin, showAnimation);
}

void GameSceneLayer::updateCandy(bool showAnimation)
{
    if(!mCandyPanel->isVisible()){
        return;
    }
    
    int candy = GameWorld::instance()->getPlayerCollectedCandy();
    
    Text* candyText = mCandyPanel->getChildByName<Text*>("totalCandyText");
    if(candyText == nullptr) {
        return;
    }
    
    std::string candyStr = StringUtils::format("%d", candy);
    
    if(showAnimation) {
        popValue(candyText,candyStr);
    } else {
        candyText->setString(candyStr);
    }
}




void GameSceneLayer::updatePuzzleCount(bool showAnimation)
{
	updatePuzzleCount(GameWorld::instance()->getPuzzleCount(), showAnimation);
}

void GameSceneLayer::updatePuzzleCount(int newValue, bool showAnimation)
{
	
	int puzzleCount = newValue;
	if(mPuzzleText == nullptr) {
		return;
	}
	
	std::string countStr = StringUtils::format("%d", puzzleCount);
	
	if(showAnimation) {
		popValue(mPuzzleText, countStr);
	} else {
		mPuzzleText->setString(countStr);
	}
}


void GameSceneLayer::updateScore(int score, bool showAnimation)
{
    if(mScoreText==nullptr){
        return;
    }
    
    std::string scoreStr = StringUtils::format("%d", score);
    
    if(showAnimation) {
        popValue(mScoreText,scoreStr);
    } else {
        mScoreText->setString(scoreStr);
    }
    
}


void GameSceneLayer::setPlayerCoin(int coin)
{
	if(mCoinText) {
		std::string coinStr = StringUtils::format("%d", coin);
		popValue(mCoinText,coinStr);
	}
}

void GameSceneLayer::startGame()
{
	
	showGameWorld();
	showGameUI();
    
    // resetCandyState();

	// Revive data
	GameManager::instance()->resetReviveData();
	
    if(mCircularMask){
        mCircularMask->updateAngleByPercent(0); //reset distance bar
    }
	setTimeCounterColor(false);
	
    //reset distance text font size
//    if(mTimeText){
//        mTimeText->setFontSize(kDistanceTextFontSize);
//    }
//    if(mTimeUnitText){
//        mTimeUnitText->setFontSize(kDistanceUnitFontSize);
//    }
    mTimeDigit = 1; //reset no of digit of distance text
    
	GameWorld::instance()->resetState();
	
	// Control whether show tutorial or not
	GameWorld::State firstState;
	if(getTutorialMode()) {
		firstState = GameWorld::State::Tutorial;
		GameWorld::instance()->resetTutorialState();
	} else {
		firstState = GameWorld::State::NewGame;
	}
	
	//GameWorld::instance()->changeState(GameWorld::State::NewGame); // will call state logic
	GameWorld::instance()->changeState(firstState);
	GameWorld::instance()->forceUpdate();
	
	
//	setPlayerCoin(GameWorld::instance()->getPlayer()->getCollectCoin());

	
	GameWorld::instance()->start();
}

void GameSceneLayer::saveCollectedCoins()	// no used!!!
{
    // Save the collected coin
    int collectCoin = GameWorld::instance()->getPlayerCollectedCoins();
//    GameManager::instance()->getUserData()->addCoin(collectCoin);
    GameWorld::instance()->getPlayer()->setCollectCoin(0);
	
	// setPlayerCoin(0);
}


bool GameSceneLayer::canShowAD()
{
	if(AdManager::instance()->isAdEnabled() == false) {
		return false;
	}
	
	bool haveInternet = DeviceHelper::isConnectedToInternet();
	if(haveInternet == false) {
		return false;
	}
	
	return true;
}


void GameSceneLayer::gotoEndGameScene()
{
	Scene *scene = EndGameSceneLayer::createScene();
	Director::getInstance()->replaceScene(scene);
}

void GameSceneLayer::handleGameOver()
{
//    GameSound::clearSound();
	

	mLastBestDistance = GameWorld::instance()->getLastBestDistance();
	
	//
	
	GameWorld::instance()->updatePlayerRecord();
	
	
	// Showing the GameOver using Scene
	if(getTutorialMode()) {
		backToMainSceneFromTutorial();
	} else {
		gotoEndGameScene();
	}
	
	// Showing the GameOver using Dialog
//	// TODO: Unlock Logic
//	std::vector<int> newUnlockList = PlayerManager::instance()->getNewUnlockedCharacters();
//	if(newUnlockList.size() > 0) {
//		handleUnlockNewDogs(newUnlockList);
//    }else{
//        showGameOverDialog();
//    }
//	
//	
//
//	hideGameWorld();
//	hideGameUI();
}

void GameSceneLayer::onGameOverRevive(Ref *sender)		// One more chanc
{
	
	GameSound::playSound(GameSound::Button);
	
	removeDialog();
	showGameWorld();
    
	GameWorld::instance()->revivePlayer();
}

void GameSceneLayer::onGameOverPlayAgain(Ref *sender)
{
    
    GameSound::playSound(GameSound::Button);

    //log("OK button is clicked");
	
	removeDialog();
	
	handleEndGameLogic();
	
	//
	if(canShowAD() && AdManager::instance()->showAdBeforeReplay()) {
		log("Try to show AD before playAgain");
		// Show Video Ad
		AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
			log("Interstitual AD called back");
			startGame();
		});
		
		AdManager::instance()->setVideoRewardedCallback(nullptr);		
		bool canShow = AdManager::instance()->showInterstitualAd();
		
		if(canShow == false) { // Start game if fail to play any ad
			log("Fail to show interstitual AD");
			startGame();
		}
	} else {
		// No need wait for Video Ad
		startGame();
	}
}

void GameSceneLayer::onGameOverShare(Ref *sender)
{
	GameSound::playSound(GameSound::Button);
	
    PlayerGameResult* result = GameWorld::instance()->getGameResult();
    int totalScore =  result->getTotalScore();
    int stageID = PlanetManager::instance()->getSelectedPlanet();
    
    GameManager::instance()->share(totalScore,stageID);
}

void GameSceneLayer::onGameOverBackHome(Ref *sender)
{
    
    GameSound::playSound(GameSound::Button);

	log("OK button is clicked");
	
	removeDialog();
	
	handleEndGameLogic();
	
	Director::getInstance()->popToRootScene();
}


void GameSceneLayer::removeDialog()
{

	if(mDialog != nullptr) {
		mDialog->removeFromParent();
	}
	
	mDialog = nullptr;
}

//void GameSceneLayer::updateScore()
//{
//   // GameWorld::instance()->updateScore();
//}

void GameSceneLayer::updateDebugInfo()
{
	if(mDebugText == nullptr) {
		return;
	}
	
	mDebugText->setString(DebugInfo::instance()->info());
}

void GameSceneLayer::updateSpeedText()
{
    //update player speed info
    if(mSpeedText){
        if(!mSpeedText->isVisible()){
            return;
        }
        int speed = GameWorld::instance()->getPlayer()->getSpeed();
        std::string speedStr = StringUtils::format("speed:%d",speed);
        mSpeedText->setString(speedStr);
    }
}

bool GameSceneLayer::handleUnlockNewDogs(const std::vector<int> newUnlockList)
{
    if(newUnlockList.size() == 0){
        return false;
    }
	
	// prepare the unlock list
    std::queue<int> dogQueue;
    for(int dogID : newUnlockList){
        dogQueue.push(dogID);
    }
    
    UnlockDogLayer* layer = UnlockDogLayer::create();
    
    layer->setDogQueue(dogQueue);
	
	layer->setCloseCallback([&] {
		showGameOverDialog();
//		GameManager::instance()->showRateNotice();
        GameSound::stopSound(GameSound::Unlock);
	});
	
    addChild(layer);
    
    GameSound::playSound(GameSound::Unlock);
	
    return true;
}

void GameSceneLayer::showGameOverDialog()
{
	GameWorld::instance()->getModelLayer()->clearEnemy();

	
    GameOverDialog *dialog = GameOverDialog::create();
    dialog->setOKListener(CC_CALLBACK_1(GameSceneLayer::onGameOverPlayAgain, this));
    dialog->setCancelListener(CC_CALLBACK_1(GameSceneLayer::onGameOverBackHome, this));
    dialog->setReviveListener(CC_CALLBACK_1(GameSceneLayer::onGameOverRevive, this));
    dialog->setShareListener(CC_CALLBACK_1(GameSceneLayer::onGameOverShare, this));
	
	
	dialog->setLastBestDistance(mLastBestDistance);
	
	
    addChild(dialog);
    mDialog = dialog;
}

void GameSceneLayer::showPauseDialog()
{
	PauseDialog *dialog = PauseDialog::create();
	dialog->setResumeListener(CC_CALLBACK_1(GameSceneLayer::onResumeGame, this));
	//dialog->setBackHomeListener(CC_CALLBACK_1(GameSceneLayer::onBackHome, this));
	dialog->setEndGameListener([&](Ref *sender){
		closePauseDialog();
		handleGameOver();
	});
	
	addChild(dialog);
	
	mPauseDialog = dialog;
}

void GameSceneLayer::showCaution(float posX){

}

void GameSceneLayer::popValue(Text* text,const std::string& num){
	
    ScaleTo *scaleTo = ScaleTo::create(0.25, mInitStarScale);
//    auto callback = CallFunc::create([&,this,_num](){
//        this->mCoinText->setString(_num);
//    });
    text->stopAllActions();
    text->setString(num);
    text->setScale(mInitStarScale * 1.7);
    text->runAction(Sequence::create(scaleTo,NULL));
}

void GameSceneLayer::closePauseDialog()
{
	if(mPauseDialog) {
		mPauseDialog->removeFromParent();
	}
}

void GameSceneLayer::onPauseGame()
{
	if(GameWorld::instance()->isGameRunning() == false) {
		return;
	}
	
	GameSound::playSound(GameSound::Button1);
	GameWorld::instance()->changeState(GameWorld::State::Paused);
    GameWorld::instance()->getEffectLayer()->pauseAllCaution();
}

void GameSceneLayer::onResumeGame(Ref *sender)
{
	GameSound::playSound(GameSound::Button1);
	GameWorld::instance()->changeState(GameWorld::State::Running);
	
	resumeClockCountDownEffect();
}

void GameSceneLayer::onBackHome(Ref *sender)
{
	GameSound::playSound(GameSound::Button1);
	//mPauseDialog->closeDialog();
    hideGameUI();
    hideGameWorld();
	Director::getInstance()->popToRootScene();
}


void GameSceneLayer::hideGameUI()
{
	mMainUIPanel->setVisible(false);
}

void GameSceneLayer::showGameUI()
{
	mMainUIPanel->setVisible(true);
}


void GameSceneLayer::showGameWorld()
{
	mWorldNode->setVisible(true);
}

void GameSceneLayer::hideGameWorld()
{
	mWorldNode->setVisible(false);
//    GameWorld::instance()->getEffectLayer()->eraseAllCaution();
}

void GameSceneLayer::updateEnergyText(int current, int max)
{
	char temp[100];
	
	sprintf(temp, "%d / %d", current, max);
//	
//	if(mEnergyText) {
//		mEnergyText->setString(temp);
//	}
}

void GameSceneLayer::setupEnergy()
{
	if(mEnergyBar) {
		mEnergyBar->setPercent(100);
	}
	int max = GameManager::instance()->getUserData()->getEnergyMax();
	updateEnergyText(max, max);
}

void GameSceneLayer::updateEnergy(int current)
{
	float max = GameWorld::instance()->getPlayer()->getTotalEnergy();
	
	float percent = (float) current * 100 / max;
	if(mEnergyBar) {
		//log("updateEnergy: percent=%f", percent);
		mEnergyBar->setPercent(percent);
	}
	
	updateEnergyText(current, max);
}


void GameSceneLayer::setStartOnEnter(bool flag)
{
	mStartOnEnter = flag;
}


// Show & Hide Tap Hint also control the 'touchable' state of other UI as well
void GameSceneLayer::hideTapHint()
{
    if(mPauseButton){
        mPauseButton->setEnabled(true);
    }//
	if(mTapHintPanel) {
		mTapHintPanel->setVisible(false);
	}
}

void GameSceneLayer::showTapHint()
{
    if(mPauseButton){
        mPauseButton->setEnabled(false);
    }//

	if(mTapHintPanel) {
		mTapHintPanel->setVisible(true);
	}
}

void GameSceneLayer::enableCapsuleButton(int itemEffectType)
{
	if(mCapsuleButton == nullptr) {
		return;
	}
	std::string imageName = GameRes::getCapsuleButtonImageName(itemEffectType);
	if(imageName.length() == 0) {
		disableCapsuleButton();
		return;
	}
	
	mCapsuleButton->loadTextureNormal(imageName);
	mCapsuleButton->setVisible(true);
	mCapsuleButton->setEnabled(true);
	
}

void GameSceneLayer::disableCapsuleButton()
{
	if(mCapsuleButton == nullptr) {
		return;
	}
	mCapsuleButton->setVisible(false);
	mCapsuleButton->setEnabled(false);
}


void GameSceneLayer::playEnergyParticle()
{
	if(mEnergyParticle == nullptr) {
		return;
	}
	mEnergyParticle->setVisible(true);
	mEnergyParticle->resetSystem();
}

void GameSceneLayer::setTutorialUI(bool enable)
{
	log("Enable Tutorial UI: flag=%d", enable);
	
	if(enable) {
		mPauseButton->setVisible(false);
        mTimeText->setVisible(false);
        mTimeUnitText->setVisible(false);
        mDistanceBarBG->setVisible(false);
		
//		mDistanceText->setString("Tutorial");
		
	} else {
		mPauseButton->setVisible(true);
        mTimeText->setVisible(true);
        mTimeUnitText->setVisible(true);
        mDistanceBarBG->setVisible(true);
	
//		setPlayerDistance(GameWorld::instance()->getPlayerDistance());
        setTime(GameWorld::instance()->getRemainTime());
	}
	
}

Node* GameSceneLayer::addOneTimeAnimation(const std::string &csbname, const cocos2d::Vec2 &position,
										  float scale, bool destroySelf, bool isRepeat){
    Node *node = ViewHelper::createAnimationNode(csbname, "active", isRepeat, destroySelf);
	
    node->setScale(scale);
    node->setPosition(position);
    addChild(node);
    return node;
}

void GameSceneLayer::handleEndGameLogic()
{
	log("Player end the game!");
	
	// saveCollectedCoins();
}


bool GameSceneLayer::anyUnlockDog()
{
	std::vector<int> newUnlockList = DogManager::instance()->getNewUnlockedDogs();

	return newUnlockList.size() > 0;
}

bool GameSceneLayer::anyDogSuggestion()
{
	return DogManager::instance()->shouldShowSuggestion();
}

void GameSceneLayer::handleUnlockDogInfo()
{
//	bool isOkay = handleUnlockNewDogs();
//	if(isOkay == false){		// exception handling
//		showGameOverDialog();
//	}
}

void GameSceneLayer::handleDogSuggestion()
{
	DogSuggestionLayer *layer = DogSuggestionLayer::create();
	addChild(layer);
	
	int suggestDog = DogManager::instance()->getSuggestedDogID();
	layer->setDog(suggestDog);
	
	layer->setCallback([&](bool didUnlock){
		showGameOverDialog();
	});

}


Vec2 GameSceneLayer::getStarCounterPosition()
{
	Vec2 starPos = VisibleRect::rightTop() + Vec2(-53.5, -53.5);		// TODO: make it not hardcode
	
	return starPos;
}


Vec2 GameSceneLayer::getCandyCounterPosition()
{
	Vec2 topLeft = VisibleRect::leftTop();
	
	Vec2 candyPos = topLeft + Vec2(16, -26);		// TODO: make it not hardcode
	
	return candyPos;
}


BoosterControl *GameSceneLayer::getBoosterControl()
{
    return mControl;
}

void GameSceneLayer::setSelectedBooster(BoosterItemType boosterType)
{
	if(mControl != nullptr) {
		mControl->setBoosterType(boosterType);
	}
}


void GameSceneLayer::setupBoosterControl(const Vec2 &pos)
{
	mControl = BoosterControl::create();
	
	mControl->setBoosterType(BoosterItemNone);
	
	Size controlSize = mControl->getContentSize();
	Vec2 leftBotPos = pos - Vec2(controlSize.width/2, controlSize.height/2);
	
	mControl->setPosition(leftBotPos);
	addChild(mControl);
	
	mControl->setCallback([&](BoosterControl *control) {
		if(control != mControl) {
			return false;
		}
        
        if(GameWorld::instance()->getPlayer()->getIsDying()){
            return false;
        }
		
		BoosterItemType type = control->getBoosterType();
		
		GameWorld::instance()->getPlayer()->activateBooster(type);
		GameWorld::instance()->getGameResult()->addBoosterUse(type);
		
		
		return true;
	});
}

void GameSceneLayer::update(float delta)
{
	if(mControl) {
		mControl->update(delta);
	}
}



#pragma mark - Clock CountDown Effect
void GameSceneLayer::startClockCountDownEffect()
{
	GameSound::playSound(GameSound::CountDown);
	setClockCountDownFrameVisible(true);
	
	setTimeCounterColor(true);
}

void GameSceneLayer::pauseClockCountDownEffect()		// when player pause
{
	GameSound::pauseSound(GameSound::CountDown);
}

void GameSceneLayer::resumeClockCountDownEffect()		// when playe resume
{
	GameSound::resumeSound(GameSound::CountDown);
}

void GameSceneLayer::stopClockCountDownEffect()			// quit game or game over
{
	GameSound::stopSound(GameSound::CountDown);
	setClockCountDownFrameVisible(false);
	setTimeCounterColor(false);
}

void GameSceneLayer::setClockCountDownFrameVisible(bool flag)
{
	if(mClockCountDownFrame == nullptr) {
		return;
	}
	
	if(flag == false) {
		mClockCountDownFrame->setVisible(false);
		return;
	}
	
	mClockCountDownFrame->setVisible(true);
	// TODO: Add the animation
	
//	:setAlphaBlink(Node *node, float duration, int repeatCount,
//				   int opacity1, int opacity2,
	AnimationHelper::setAlphaBlink(mClockCountDownFrame, 0.2, 0, 255, 50);
}


void GameSceneLayer::setTimeCounterColor(bool isHurryUp)
{
	mTimeText->setTextColor(isHurryUp ? kTimeColorHurryUp : kTimeColorNormal);
}


#pragma mark - Related to Game Finish
void GameSceneLayer::setFinishGameTitleVisible(bool flag)
{
	if(mFinishGameSprite == nullptr) {
		return;
	}
	
	mFinishGameSprite->setVisible(flag);
}

#pragma mark - Related to Time Extension
void GameSceneLayer::setupTimeExtendPanel()
{
	mTimeExtendPanel = mRootNode->getChildByName("timeExtendPanel");
	mTimeExtendText = mTimeExtendPanel->getChildByName<Text *>("secondText");
	mTimeExtendPanel->setVisible(false);
}

void GameSceneLayer::showTimeExtended(int extendValue)
{
	if(mTimeExtendText == nullptr) {
		return;
	}
	
	std::string str = StringUtils::format("+%d seconds", extendValue);
	mTimeExtendText->setString(str);
	mTimeExtendPanel->setVisible(true);
	
	
	AnimationHelper::setToastEffect(mTimeExtendPanel, 1.0f, 0.0, 1.0f, 0.1, [=](){
		GameWorld::instance()->addRemainTime((float) extendValue);
		log("DEBUG: World.remainTime=%f", GameWorld::instance()->getRemainTime());
		setTime(GameWorld::instance()->getRemainTime());
	});
	
	
//	static void setToastEffect(cocos2d::Node *targetNode,		// Visible for a short time and then hide
//									 const float &finalScale,
//									 float delay,
//							   float showDuration,
//									 float popDuration,
//									 const std::function<void ()> &callback = nullptr);
}


//private:
//Node *mTimeExtendPanel;
//Text *mTimeExtendText;


#pragma mark - Distance Layer
NpcDistanceLayer *GameSceneLayer::getNpcDistanceLayer()
{
	return mNpcDistanceLayer;
}

void GameSceneLayer::setupNpcDistanceLayer()
{
	mNpcDistanceLayer = NpcDistanceLayer::create();
	
	mRootNode->addChild(mNpcDistanceLayer);

}


#pragma mark - Reset
void GameSceneLayer::reset()
{
	setTopPanelVisible(false);
	setTimeUpTextVisibility(false);
	mTimeExtendPanel->setVisible(false);
	setFinishGameTitleVisible(false);
}


void GameSceneLayer::showNpcDistance(NPCOrder *order)
{
	setTopPanelVisible(false);
	NpcDistanceLayer *layer = getNpcDistanceLayer();
	layer->setOnCloseCallback([&](){
		setTopPanelVisible(true);
	});
	
	layer->showNpcOrder(order);
}


#pragma mark - CheckPoint Reward
void GameSceneLayer::showCheckPointDialog(NPCRating *rating, const std::function<void()> &callback)
{
	if(mCheckPointDialog == nullptr) {
		return;
	}
	
	mCheckPointDialog->setOnCloseCallback(callback);
	mCheckPointDialog->setScore(rating->getScore());
	mCheckPointDialog->setTime(rating->getTimeUsed());
	mCheckPointDialog->setMoney(rating->getTotalReward());
	
	mCheckPointDialog->showUp();
}

void GameSceneLayer::setupCheckPointDialog()
{
	mCheckPointDialog = CheckPointDialog::create();
	
	mRootNode->addChild(mCheckPointDialog);
}



#pragma mark - Effect and Animation
void GameSceneLayer::setupEffectLayerPosition()
{
	EffectLayer *effectLayer = GameWorld::instance()->getEffectLayer();
	
	if(effectLayer != nullptr) {
		// Score Position
		Vec2 scorePosition = mScoreText->convertToWorldSpace(Vec2::ZERO);
		effectLayer->setScorePosition(scorePosition);
		
		Vec2 coinPosition = mCoinText->convertToWorldSpace(Vec2::ZERO);
		effectLayer->setCoinPosition(coinPosition);
	}
}


void GameSceneLayer::runAddScoreAnimation(const int &score, const int &finalScore,
										  const Vec2 &startPos,
										  const std::function<void ()> &callback,
										  const float &duration)
{
	Vec2 scorePosition = mScoreText->convertToWorldSpace(Vec2::ZERO);
	
	
	int fontSize = 15;
	// Create the Text
	std::string str = StringUtils::format("+%d", score);
	ui::Text *text = ViewHelper::createOutlineText(str, fontSize,
												   Color3B::WHITE, 2, Color3B::BLACK);
	
	text->setPosition(startPos);
	addChild(text);
	
	// Run the Animation
	
	ActionInterval *curveMove = AnimationHelper::getCurveAction(
								startPos, scorePosition, duration, 1);
	
	CallFunc *callbackAction = callback == nullptr ? nullptr : CallFunc::create(callback);
	
	RemoveSelf *removeSelf = RemoveSelf::create();

	CallFunc *popupScore = CallFunc::create([&, finalScore]{
		updateScore(finalScore, true);
	});
	
	
	Action *action = Sequence::create(curveMove, removeSelf, popupScore, callbackAction, nullptr);
	
	
	text->runAction(action);
	
}


#pragma mark - Time Used Text
void GameSceneLayer::updateTimeUsed()
{
	float timeUsed = GameWorld::instance()->getTimeElapse();
	
	if(mTimeUsedText != nullptr) {
		std::string str = StringUtils::format("%.2f\"", timeUsed);
		mTimeUsedText->setString(str);
	}
	//
}


#pragma mark - Tutorial
void GameSceneLayer::backToMainSceneFromTutorial()
{
	
	bool useReplace = isTutorialFromTitle();
	
	if(useReplace == false) {		// it is push from the MainScene
		Director::getInstance()->popScene();
		return;
	}
	
	// Create the Main Scene
	auto scene = MainSceneLayer::createScene();
	Director::getInstance()->replaceScene(scene);
	
}

std::string GameSceneLayer::statInfo()
{
	return StringUtils::format("gui=%ld", getChildrenCount());
}
