//
//  SettingScene.hpp
//  GhostLeg
//
//  Created by Calvin on 27/6/2016.
//
//

#ifndef SettingScene_hpp
#define SettingScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "AstrodogClient.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class FBProfilePic;

class SettingSceneLayer : public Layer , public AstrodogClientListener{
public:
    static Scene* createScene();
    virtual bool init();
    CREATE_FUNC(SettingSceneLayer);
    virtual void onEnter();
    virtual void onExit();
    
    void setOnExitCallback(std::function<void()> callback);

private:
    void showProgress(std::string msg);
    void hideProgress();
    void showAlert(std::string title, std::string info);
    void closeAlert();
    
    void updateDiamond();
    void updateStars();
    void setupFbProfile();
	
private:
    void setupUI();
    void setupListener();
    void setupRestoreItemCallBack();
    Node* mRootNode;
    Node  *mProgressLayer, *mAlertLayer;
    Button* mMusicBtn;
    Button* mSEBtn;
    Button* mCreditBtn;
    Button* mBackBtn;
    Button* mRestoreBtn;
    Button* mTutorialBtn;
    Button* mDailyRewardBtn;
    Button* mFbBtn;
    Button* mTopFbBtn;
    
    Button* mFbLikeBtn;
    Button* mTwitterBtn;
    Button* mFeedbackBtn;
    
    Text* mDiamondText;
    Text* mStarText;
    
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
private:
    void onFBConnect(int status);
    std::function<void()> mCallback;
	void gotoTutorialScene();
};


#endif /* SettingScene_hpp */

