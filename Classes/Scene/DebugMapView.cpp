//
//  DebugMapView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/7/2016.
//
//

#include "DebugMapView.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "LevelData.h"

#include "GameManager.h"
#include "UserGameData.h"
#include "GameWorld.h"
#include "ViewHelper.h"
#include "CommonMacro.h"

using namespace cocostudio::timeline;
using namespace cocos2d::ui;

DebugMapView::DebugMapView()
{
	
}

// on "init" you need to initialize your instance
bool DebugMapView::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}
	
	// Setup UI
	Node *rootNode = CSLoader::createNode("DebugMapLayer.csb");
	addChild(rootNode);
	setupUI(rootNode);
	
	updateCurrentMap();
	
	return true;
}


// ui note:
//		panelDialog	Panel	Panel for the Dialog
//		textGameOver	Text	Game Over Title
//		buttonOK		Button	OK Button
//		buttonCancel	Button	Cancel Button
//		scoreText		Text
//		bestScoreText	Text
void DebugMapView::setupUI(cocos2d::Node *rootNode)
{
	// Fine tune layer after layer resizing
	FIX_UI_LAYOUT(rootNode);			// same as above two lines
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	
	// close Button
	Button *backBtn = (Button*) mainPanel->getChildByName("backBtn");
	ViewHelper::addBackButtonListener(this, backBtn, ViewHelper::BackHandleType::RemoveFromParent);
	
	// Scroll of debug map
	ui::ScrollView *scroll = (ui::ScrollView *) mainPanel->getChildByName("mapScrollView");
	setupDebugMapList(scroll);

	// Info Text
	mCurrentMapText = (Text *) mainPanel->getChildByName("currentMapText");
	
	//
	Button *resetBtn = (Button*) mainPanel->getChildByName("resetBtn");
	resetBtn->addClickEventListener([&](Ref *) {
		setDebugMap(-1);
	});
}






void DebugMapView::setupDebugMapList(ui::ScrollView *scrollView)
{
	std::vector<int> maps = MapLevelDataCache::instance()->getAvailableMaps();
	std::sort(maps.begin(), maps.end());   // Sort in
	
	float rowH = 20;
	float colW = 60;
	const int numCol = 5;
	
	// Total height =
	float totalHeight = (maps.size() / numCol + 2) * rowH;
	if(totalHeight < scrollView->getContentSize().height) {
		totalHeight = scrollView->getContentSize().height;
	}
	
	scrollView->setInnerContainerSize(Size(scrollView->getContentSize().width, totalHeight));
	
	//
	
	float column[numCol];
	
	int fontSize = 15;
	float y = totalHeight - rowH / 2;
	
	// initize the column
	for(int i=0; i<numCol; i++) { column[i] = i * colW; }
	
	// Add to scroll
	for(int i=0; i<maps.size(); i++) {
		int mapID = maps[i];
		log("mapID=%d", mapID);
		
		
		
		std::string name = StringUtils::format("%05d", mapID);
		
		Button *button = ViewHelper::createButton(name, fontSize);
		
		float x = column[i % numCol] + colW/2;
		button->setPosition(Vec2(x, y));
		scrollView->addChild(button);
		
		
		Widget::ccWidgetClickCallback callback = [&, mapID](Ref *button) {
			setDebugMap(mapID);
		};
		
		button->setTag(mapID);
		button->addClickEventListener(callback);
		
		// New line handling
		
		if(i % numCol == numCol-1) {
			y -= rowH;
		}
	}
}

void DebugMapView::setDebugMap(int mapID)
{
	GameManager::instance()->setDebugMap(mapID);
	log("change to map %d", mapID);
	
	updateCurrentMap();
}

void DebugMapView::updateCurrentMap()
{
	int currentMap = GameManager::instance()->getDebugMap();
	
	std::string msg = currentMap < 0 ? "No debug map"
					: StringUtils::format("using map %d", currentMap);
	
	mCurrentMapText->setString(msg);
}

