//
//  EndGameScene.hpp
//  GhostLeg
//
//  Created by Ken Lee on 8/5/2017.
//
//

#ifndef EndGameScene_hpp
#define EndGameScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>

USING_NS_CC_EXT;
USING_NS_CC;
using namespace cocos2d::ui;


class EndGameSceneLayer : public Layer
{
public:
	static Scene *createScene();
	
	EndGameSceneLayer();
	
	CREATE_FUNC(EndGameSceneLayer);
	
	virtual bool init();

#pragma mark - Core
public:
	virtual void onEnter();
	virtual void onExit();
	void update(float delta);
	
#pragma mark - GUI Construction
private:
	void setupGUI();
	
#pragma mark - Navigation
public:
	void gotoGameScene();
	void gotoMainScene();

#pragma mark - GameOverDialog
public:
	void showGameOverDialog();
private:
	Node *mDialog;

private:	// handler for different action
	void onGameOverPlayAgain(Ref *sender);	// Retry
	void onGameOverBackHome(Ref *sender);	// Back to main
	void onGameOverShare(Ref *sender);		// Share

#pragma mark - Start New Game
private:
	void startGame();

#pragma mark - Internal Logic
private:
	void handlePuzzleCollection();
	void debugPlayerGameResult();
	void logPlayerGameResult();

#pragma mark - Internal Data
private:
	int mLastBestDistance;
	int mPuzzleCount;
	std::vector<int> mCollectPuzzleList;
};


#endif /* EndGameScene_hpp */
