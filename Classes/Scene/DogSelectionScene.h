//
//  DogSelectionScene.hpp
//  GhostLeg
//
//  Created by Calvin on 13/10/2016.
//
//

#ifndef DogSelectionScene_hpp
#define DogSelectionScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class DogSelectionSceneLayer : public Layer{
    
public:
    static Scene* createScene();
    virtual bool init() override;
    virtual void onEnter() override;
    CREATE_FUNC(DogSelectionSceneLayer);
	
	
	virtual ~DogSelectionSceneLayer();
    
    void onSelectDog(int selectedDogID);
    void refreshPlanetBtnVisibleState();
    void onChangePlanet(int planetID);
    void updateUnlockText();
    void updatePlanetIcon();
    
    void showView();
    void hideView();
    
    void refreshItemView(int dogID);
    void setEnterAtSpecificPlanet(int planetID);
    
    void setupTopBar();
    
    
//    void update(float delta);
//    virtual void onEnter();
//    virtual void onExit();
	
private:
    void setupUI(Node *rootNode);
	void setupDogListView();
    void setupPlanetBtn();
	void setupPlanetList();
    void updateCandyStick();
    void updateIndicatorInfo(int numOfUnlockedDogs);
	
	int getPlanetIDWithChange(int valueChange);	// +1: next planet, -1, last planet
	
private:
    std::vector<int> mPlanetIDList;
    Node *mRootNode;
    Node *mUpperPanel;
	
	Text *mUnlockText;
	Text *mUnlockMsgText;
	
    Node *mDogIndicatorPanel;
    Node *mEventBanner;
	
	ListView *mDogListView;
    Button* mNextBtn;
    Button* mPreviousBtn;
    Sprite* mPlanetSprite;
    
    bool mIsEnteredWithSpecificPlanet;
    int mSelectedDog;
    int mSelectedPlanet;
	
	int mFirstPlanet;		// first planet team, which is venus, id=2
	int mLastPlanet;		// last planet team, which is jupiter, id=5
    
    Vector<Sprite*> mUnlockedDogIndicatorList;
};



#endif /* DogSelectionScene_hpp */
