//
//  MasteryItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 17/5/2016.
//
//

#include <stdio.h>
#include "cocostudio/CocoStudio.h"
#include <MasteryItemView.h>
#include <Mastery.h>
#include "GameManager.h"
#include "MasteryManager.h"
#include "StringHelper.h"
#include "GameSound.h"

USING_NS_CC_EXT;
USING_NS_CC;
using namespace cocos2d::ui;

namespace {
	std::string getLevelString(int level, bool isMax)
	{
		if(isMax) {
			return "max level";
		}
		
		if(level == 0) {
			return "locked";
		}
		
		return "level" + INT_TO_STR(level);
	}
    
    std::string getValueString(float value, bool isPercentage, bool isFloat)
    {
        if(!isFloat){
            std::string result = INT_TO_STR((int)value);
            if(isPercentage)
                result.append("%");
            return result;
        }else{
            char temp[5];
            
            if(!isPercentage)
            sprintf(temp, "%.1f", value);
            else
            sprintf(temp, "%.1f%%", value);
            
            return temp;
        }
    }

}


bool MasteryItemView :: init(){
    
    if ( !Layer::init() )
    {
        return false;
    }
    
    Node *rootNode = CSLoader::createNode("masteries-subView.csb");
    addChild(rootNode);
    setupUI(rootNode);
    
    return true;
}

void MasteryItemView::setupUI(Node *rootNode)
{
	setContentSize(rootNode->getContentSize());
	
    mLevelText = (Text *) rootNode->getChildByName("levelText");
    mTitleText = (Text *) rootNode->getChildByName("titleText");
    mDescriptionText = (Text * ) rootNode->getChildByName("descriptionText");
    mIcon = (Sprite *) rootNode->getChildByName("iconPanel")->getChildByName("icon");
    mUpgradePreviewUnlock = rootNode->getChildByName("upgradeValue_first");
	
	mUpgradePreview = rootNode->getChildByName("upgradeValue_second");
    mNormalUpgradePanel = rootNode->getChildByName("normalPricePanel");
    mMaxUpgradePanel = rootNode->getChildByName("maxLevelPricePanel");
    mUnlockUpgradePanel = rootNode->getChildByName("unlockPricePanel");
    
}


void MasteryItemView::setMastery(MasteryData* mastery){
	if(mastery == nullptr) {
		log("setMastery: mastery is null");
		return;
	}
	
	int masteryID = mastery->getMasteryID();
	
	int currentLevel = GameManager::instance()->getMasteryManager()->getPlayerMasteryLevel(masteryID);
	
	setMastery(mastery, currentLevel);
}



Node *MasteryItemView::getVisibleUpgradePanel(int level, bool isMax)
{
	if(isMax) {				// When Max Level
		mNormalUpgradePanel->setVisible(false);
		mUnlockUpgradePanel->setVisible(false);
		
		mMaxUpgradePanel->setVisible(true);
		
		return mMaxUpgradePanel;
	} else if(level == 0) {	// When Level= 0
		mNormalUpgradePanel->setVisible(false);
		mMaxUpgradePanel->setVisible(false);
		
		mUnlockUpgradePanel->setVisible(true);
		
		return mUnlockUpgradePanel;

	} else {				// When other level
		mMaxUpgradePanel->setVisible(false);
		mUnlockUpgradePanel->setVisible(false);
		
		mNormalUpgradePanel->setVisible(true);
		
		return mNormalUpgradePanel;
		
	}
}

void MasteryItemView::setupUpgradeButton(Node *upgradePanel, bool isMaxLevel)
{
	if(upgradePanel == nullptr) {
		return;
	}
	
	Button *upgradeButton = (Button *)upgradePanel->getChildByName("priceBtn");
	if(upgradeButton == nullptr || isMaxLevel) {
		return;
	}
	
	upgradeButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type) {
            case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
					
			case ui::Widget::TouchEventType::ENDED: {
				notifyUpgrade();
				break;
			
			}
		}
	});
	
}

void MasteryItemView::setMastery(MasteryData* mastery, int currentLevel)
{
	if(mastery == nullptr) {
		log("setMastery: mastery is null");
		return;
	}
	
	mMasteryID = mastery->getMasteryID();
	mPlayerLevel = currentLevel;

	
	// define the next level and isMax flag
    bool isMaxLevel = mastery->getMaxLevel() == currentLevel;
	
    int nextLevel = isMaxLevel ? currentLevel : currentLevel + 1;
	
	// define mastery value and cost
    float currentValue = mastery->getLevelValue(currentLevel);
    float nextValue = mastery->getLevelValue(nextLevel);
    int upgradeCost = mastery->getUpgradePrice(nextLevel);
	

	// title and description
    mTitleText->setString(mastery->getName());
    mDescriptionText->setString(mastery->getInfo());

	// Set the level to GUI
	mLevelText->setString(getLevelString(currentLevel, isMaxLevel));

    // Setup Icon
    if(mTitleText->getString().compare("max storage")==0){
        mIcon->setTexture("masteries_01.png");
    }else if(mTitleText->getString().compare("regeneration")==0){
        mIcon->setTexture("masteries_02.png");
    }else if(mTitleText->getString().compare("double star")==0){
        mIcon->setTexture("masteries_03.png");
    }else if(mTitleText->getString().compare("refinery")==0){
        mIcon->setTexture("masteries_04.png");
    }
    
	// Upgrade Cost
	Node *upgradePanel = getVisibleUpgradePanel(currentLevel, isMaxLevel);
	Text *priceText = (Text *) upgradePanel->getChildByName("priceText");
	if(priceText && isMaxLevel == false) {
		priceText->setString(INT_TO_STR(upgradeCost));
	}
    Text *priceLevelText = (Text*) mNormalUpgradePanel->getChildByName("priceLevelText");
	if(priceLevelText) {
		priceLevelText->setString("upgrade to lvl " + INT_TO_STR(nextLevel));
	}

	// Setting the button handler
	setupUpgradeButton(upgradePanel, isMaxLevel);
	
    // Effective Value
	if(isMaxLevel){
        mUpgradePreviewUnlock->setVisible(true);
        mUpgradePreview->setVisible(false);
        
		Text* valueText = (Text*)mUpgradePreviewUnlock->getChildByName("valueText");
        valueText->setString(getValueString(nextValue, mastery->getPercentFlag(), mastery->getFloatFlag()));
        
    }else{
        mUpgradePreviewUnlock->setVisible(false);
        mUpgradePreview->setVisible(true);

        Text* valueTextCurrent = (Text*)mUpgradePreview->getChildByName("valueTextCurrent");
        valueTextCurrent->setString(getValueString(currentValue,mastery->getPercentFlag(),mastery->getFloatFlag()));
        
        Text* valueTextNext = (Text*)mUpgradePreview->getChildByName("valueTextNext");
        valueTextNext->setString(getValueString(nextValue, mastery->getPercentFlag(),mastery->getFloatFlag()));

    }
    
    
}

int MasteryItemView::getMasteryId(){
    return mMasteryID;
}

void MasteryItemView::setCallback(const MasteryItemViewCallback &callback)
{
	mCallback = callback;
}

void MasteryItemView::notifyUpgrade()
{
	if(! mCallback) {
		return;
	}
	
	mCallback(this, mMasteryID, mPlayerLevel+1);
}

void MasteryItemView::updateUI()
{
	MasteryData *data = GameManager::instance()->getMasteryManager()->getMasteryData(mMasteryID);
	
	setMastery(data);
}



