//
//  GameOverDialog.h
//  TapToJump
//
//  Created by Ken Lee on 19/4/15.
//
//

#ifndef __TapToJump__GameOverDialog__
#define __TapToJump__GameOverDialog__

#include <stdio.h>
#include <string>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "AstrodogClient.h"
#include <queue>
#include "CommonType.h"

USING_NS_CC_EXT;
USING_NS_CC;
using namespace cocos2d::ui;

class GalleryView;
class LeaderboardDialog;
class NPCRating;
class AnimeNode;

// A data class used for display score

class GameOverDialog : public Layer, public AstrodogClientListener
{
	
	
public:
    
	CREATE_FUNC(GameOverDialog);
	
	GameOverDialog();
	
	CC_SYNTHESIZE(int, mLastBestDistance, LastBestDistance);
	
	virtual bool init();
	
	void setOKListener(const Widget::ccWidgetClickCallback &callback);
	void setCancelListener(const Widget::ccWidgetClickCallback &callback);
	void setShareListener(const Widget::ccWidgetClickCallback &callback);
	void setReviveListener(const Widget::ccWidgetClickCallback &callback);
	void setBonusStarListener(int bonusStar);
    
	void hideReviveButton();	// When revive once in one game
	
	void setUnlockHint(const std::string &msg);
	
    virtual void onEnter();
	virtual void onExit();
 
	void updateUnlockHint();
    void updateCoinsGet(bool hasAnimation = false); //animation is hardcoded for rolling to double star
    void updateCandyGet(bool hasAnimation = false);
    
    void updateEndGameResultData();
	
	void setNewRecordEffect(bool flag);
    
    void setLeaderboardGUI(AstrodogLeaderboardType type);
    
    void setupRewardPanel();
    
    void setupCollectedCoins(int coins);
    
    void setPuzzleList(std::vector<int> puzzleList);
    void setupPuzzleUI();
    
    void puzzlePopUp();


#pragma mark - NPCRating
    void setNPCRating(Vector<NPCRating*> ratingList);
    
#pragma mark - Leaderboard
	void uploadLeaderboardScore();
	void loadLeaderboard(AstrodogLeaderboardType type);
	
	void updateLeaderboardRecords(AstrodogRankStatus rankStatus);
	
#pragma mark - AstrodogClient
public:	// AstrodogClientListener
	virtual void onGetRanking(int status, AstrodogLeaderboardType type, int planet,
							  const std::map<std::string, int> &metaData);
	virtual void onSubmitScore(int status);
	virtual void onFBConnect(int status);
		
#pragma mark - Core Function
private:
	void setupUI(Node *mainPanel);
    void updateDistance();
    void updateScore();
	void updatePlayerModel();
	
	void handleRevive();
	void onWatchVideoForBonusStarDone();
    
    
	
private:
    Node *mRootNode;
    Node *mPanelDialog;
    Node *mUpperPanel;
    Node *mLowerPanel;
    Node *mRewardPanel;
    
    Node *mDeliveryScorePanel;
    Node *mClearScorePanel;
	Node *mDistanceScorePanel;
	Node *mCollectScorePanel;
    Node *mKillScorePanel;
    Node *mTotalScorePanel;
	Node *mBonusScorePanel;
	
    Node *mCollectedCoinsPanel;
	
	Text *mDistanceScoreText;
	Text *mCollectScoreText;
	Text *mDeliveryScoreText;
	Text *mClearScoreText;
	Text *mBonusScoreText;
	Text *mKillScoreText;
	Text *mTotalScoreText;
	
	Text *mCollectedCoinsText;

	
	Button *mRetryButton;
	Button *mCancelButton;
	Button *mShareButton;
	Button *mReviveButton;
    Button *mDoubleStarButton;
    Button *mCandyButton;
    
	Text *mDistanceText;
	
	
	Node *mPlayerModelNode;
	Node *nextUnlockMsg;
    ImageView *mNewRecord;
    
    
    
    GalleryView *mGalleryView;
    LeaderboardDialog *mLBDialog;
    
	int mButtonY1;		// The position of Button Row 1 (the revive button)
	int mButtonY2;		// The position of Button Row 2 (others)
	bool mIsReviveRewarded;

	int mBonusStarAmount;
	
    // for double star rolling effect
    float mAccumCoins;
    int mDisplayCoins;
    
    AstrodogLeaderboardType mCurrentLeaderBoardType;
	
	ParticleSystemQuad *mNewRecordParticle;
	Sprite *mNewRecordSprite;
    
	Widget::ccWidgetClickCallback mReviveCallback;
    
    std::vector<int> mPuzzleList;
    std::queue<int> mPuzzleDataQueue;
    std::queue<Node*> mPuzzleNodeQueue;
    std::queue<AnimeNode*> mPuzzleAnimeNodeQueue;
	
// 	Vector<DisplayScore *> mDisplayScoreList;
	
#pragma mark - Score Animation
public:
	void hideAllPopText();
	void displayGameScores();
	void popScoreText(ScoreType scoreType, const std::function<void()> &callback);
private:
	void displayNextScore();
	int getScoreValue(ScoreType scoreType);
	Node *getScorePanel(ScoreType scoreType);
	Text *getScoreText(ScoreType scoreType);
	
	std::vector<ScoreType> mScoreDisplayList;
};

#endif /* defined(__TapToJump__GameOverDialog__) */
