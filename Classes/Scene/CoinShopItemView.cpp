//
//  CoinShopItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 24/5/2016.
//
//

#include "CoinShopItemView.h"
#include "cocostudio/CocoStudio.h"
#include "GameProduct.h"
#include "IAPManager.h"
#include "GameSound.h"
#include "StringHelper.h"

bool CoinShopItemView::init(){
    
    if ( !Layer::init() )
    {
        return false;
    }
    
    Node *rootNode = CSLoader::createNode("CoinShopItemView.csb");
    addChild(rootNode);
    setupUI(rootNode);
    
    return true;

}

void CoinShopItemView::setupUI(Node *rootNode){
    setContentSize(rootNode->getContentSize());
    
    mPurchaseBtn = rootNode->getChildByName<Button*>("purchaseBtn");
    mPlayAdBtn = rootNode->getChildByName<Button*>("watchAdBtn");
    mSpecialInfoPanel = rootNode->getChildByName("infoSpecial");
    mInfoPanel = rootNode->getChildByName("info");
    mBlueActionInfo = rootNode->getChildByName("blueActionInfo");
    mIcon = rootNode->getChildByName<Sprite*>("icon");
    mAlertImage = rootNode->getChildByName<ImageView *>("alertImage");
    mAlertText = rootNode->getChildByName<Text*>("alertText");
	mItemSprite = mInfoPanel->getChildByName<Sprite *>("starSprite");
    
 //   mStarStage = rootNode->getChildByName<Sprite*>("starStage");
 //   mDiamondStage = rootNode->getChildByName<Sprite*>("diamondStage");
    
    mStarAmount = rootNode->getChildByName<Text*>("starAmount");
    mDiamondAmount = rootNode->getChildByName<Text*>("diamondAmount");
}

Button* CoinShopItemView::getPurchaseButton(){
    return mPurchaseBtn;
}

void CoinShopItemView::setupPurchaseButton(GameProduct *product)
{
    
//    if(!isOwned){
//        mPurchaseBtn->setTitleText(price);
//    }
//    else{
//        mPurchaseBtn->setTitleText("Owned");
//        mPurchaseBtn->setEnabled(false);
//        mPurchaseBtn->setOpacity(128);
//        return;
//    }
    Text* iapPriceText = mPurchaseBtn->getChildByName<Text*>("IAPPrice");
    Text* diamondPriceText = mPurchaseBtn->getChildByName<Text*>("diamondPrice");
    Sprite* diamondSprite = mPurchaseBtn->getChildByName<Sprite*>("diamond");
    MoneyType type = product->getCostType();

    if(type == MoneyTypeIAP){
        std::string price = product->getIAPPrice();
        diamondSprite->setVisible(false);
        iapPriceText->setVisible(true);
        diamondPriceText->setVisible(false);
        iapPriceText->setString(price);
    }else if(type == MoneyTypeDiamond){
        std::string price = StringUtils::format("%d",product->getCostValue());
        diamondSprite->setVisible(true);
        iapPriceText->setVisible(false);
        diamondPriceText->setVisible(true);
        diamondPriceText->setString(price);
    }
    
    setPurchaseBtnListener();
}

void CoinShopItemView::setPurchaseBtnListener()
{
    mPurchaseBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::BEGAN: {
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            }
    
            case ui::Widget::TouchEventType::ENDED: {
                if(! mCallback) {
                    return;
                }
                
                mCallback(this,mProduct);
                break;
            }
        }
    });

}

std::string CoinShopItemView::getItemKey(){
    return mProductKey;
}

void CoinShopItemView::setProduct(GameProduct *product){
    
    if(product==nullptr){
        log("CoinShopItemView:setProduct: product is nullptr!");
        return;
    }
    
    mProductKey = product->getName();
    mProduct = product;
    mGUIKey = product->getGUIKey();
    
    std::string displayName = product->getDisplayName();
    std::string displayTag = product->getDisplayTag();
    int amount = product->getAmount();
    
    setItemViewIcon();
    setDisplayTag(displayTag);
    
    
//    if(product->getIsBlueAction())
//    {
//        setUpBlueActionItem(product);
//        return;
//    }

    
	if(displayName.find("star") != std::string::npos) {
//		mInfoPanel->setVisible(true);
//		mSpecialInfoPanel->setVisible(false);
//		mBlueActionInfo->setVisible(false);
//		Text *contentText = mInfoPanel->getChildByName<Text*>("content");
//		Text *titleText = mInfoPanel->getChildByName<Text*>("title");
//		contentText->setString(displayName.substr(5,4));
//		titleText->setString(displayName.substr(10));
//        mStarStage->setVisible(true);
//        mDiamondStage->setVisible(false);
        mStarAmount->setVisible(true);
        mDiamondAmount->setVisible(false);
        mStarAmount->setString(StringUtils::format("%d",amount));
	} else if(displayName.find("candy") != std::string::npos){	// candy
	} else if(displayName.find("diamond") != std::string::npos){	// diamond
//        mStarStage->setVisible(false);
//       mDiamondStage->setVisible(true);
        mStarAmount->setVisible(false);
        mDiamondAmount->setVisible(true);
        mDiamondAmount->setString(StringUtils::format("%d",amount));
    }else{
        mInfoPanel->setVisible(false);
        mSpecialInfoPanel->setVisible(true);
        mBlueActionInfo->setVisible(false);
//        Text *contentText = mSpecialInfoPanel->getChildByName<Text*>("content");
//        contentText->setString(displayName);
    }
    
    
    setupPurchaseButton(product);
    //log("CoinShopItemView::setProduct: product: %s, isConsumable: %d, isOwned: %d",product->getName().c_str(),product->getIsConsumable(),product->getIsOwned());


}



void CoinShopItemView::setItemViewIcon(){
    if(mGUIKey.compare("star1")==0){
        mIcon->setTexture("guiImage/ui_shop_item_04.png");
        mIcon->setPositionY(mIcon->getPositionY()-10);
    }else if(mGUIKey.compare("star2")==0){
        mIcon->setTexture("guiImage/ui_shop_item_05.png");
        mIcon->setPositionY(mIcon->getPositionY()-10);
    }else if(mGUIKey.compare("star3")==0){
        mIcon->setTexture("guiImage/ui_shop_item_06.png");
        mIcon->setPositionY(mIcon->getPositionY()-10);
    }else if(mGUIKey.compare("diamond1")==0){
        mIcon->setTexture("guiImage/ui_shop_item_01.png");
	}else if(mGUIKey.compare("diamond2")==0){
		mIcon->setTexture("guiImage/ui_shop_item_02.png");
    }else if(mGUIKey.compare("diamond3")==0){
        mIcon->setTexture("guiImage/ui_shop_item_03.png");
    }
    
//    else if(mProductKey.compare("coinByFacebookLike")==0){
//        mIcon->setTexture("ui_shop_fb.png");
//    }else if(mProductKey.compare("coinByStoreRating")==0){
//        mIcon->setTexture("ui_shop_rate.png");
//	}else if(mProductKey.compare("candypack1")==0){
//		mIcon->setTexture("guiImage/ui_shop_xmas_item1.png");
//    }else if(mProductKey.compare("diamondpack1")==0){
//        mIcon->setTexture("guiImage/ui_shop_item6.png");
//    }else if(mProductKey.compare("diamondpack2")==0){
//        mIcon->setTexture("guiImage/ui_shop_item7.png");
//    }else if(mProductKey.compare("diamondpack3")==0){
//        mIcon->setTexture("guiImage/ui_shop_item8.png");
//    }
}

void CoinShopItemView::setDisplayTag(std::string tag){
//    if(tag == "") {
//        mAlertImage->setVisible(false);
//	} else if(tag == "special") {
//		//ui_shop_special.png
//		mAlertImage->setVisible(true);
//		mAlertImage->loadTexture("guiImage/ui_shop_special.png");
//		mAlertText->setString("");
//    } else {
//        mAlertText->setString(tag);
//    }
    if(tag.length()>0){
        mAlertText->setString(tag);
        mAlertText->setVisible(true);
    }else{
        mAlertText->setVisible(false);
    }
    
}

void CoinShopItemView::setCallback(const CoinShopItemViewCallback &callback){
    mCallback = callback;
}

void CoinShopItemView::setPlayAdCallback(const std::function<void ()> &callback)
{
    mPlayAdCallback = callback;
}

void CoinShopItemView::setToWatchAdItem(int amount)
{
    mIcon->setTexture("guiImage/ui_shop_item_04.png");
    mIcon->setPositionY(mIcon->getPositionY()-10);

    mAlertText->setVisible(false);
    
    mStarAmount->setVisible(true);
    mDiamondAmount->setVisible(false);
    mStarAmount->setString(StringUtils::format("%d",amount));
    
    mPlayAdBtn->setVisible(true);
    mPurchaseBtn->setVisible(false);
    
    mPlayAdBtn->addClickEventListener([=](Ref*){
        if(mPlayAdCallback){
            mPlayAdCallback();
        }
    });
}




void CoinShopItemView::setUpBlueActionItem(GameProduct *product)
{
    
    mInfoPanel->setVisible(false);
    mSpecialInfoPanel->setVisible(false);
    mBlueActionInfo->setVisible(true);
    
    Text *blueActionLabel = mBlueActionInfo->getChildByName<Text*>("blueActionLabel");
    mPurchaseBtn->loadTextures("ui_shop_ratebtn.png", "ui_shop_ratebtn.png", "ui_shop_ratebtn.png");

    
    if(product->getProductID().compare("coinByFacebookLike")== 0)
    {
        blueActionLabel->setString("like us on Facebook");
        mPurchaseBtn->setTitleText("Like");

    }
    if(product->getProductID().compare("coinByStoreRating")== 0)
    {
        blueActionLabel->setString("rate us on app store");
        mPurchaseBtn->setTitleText("Rate Us");
        
    }
    
    setPurchaseBtnListener();
    
    
}


void CoinShopItemView::updateFacebookLikeBtn(bool isLiked)
{
    if(mPurchaseBtn!= nullptr)
    {
        if(isLiked){mPurchaseBtn->setTitleText("Liked");}
        else {mPurchaseBtn->setTitleText("Like");};
    }

}


