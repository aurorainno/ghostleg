//
//  GameScene.hpp
//  GhostLeg
//
//  Created by Ken Lee on 18/3/2016.
//
//

#ifndef GameScene_hpp
#define GameScene_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include "CommonMacro.h"
#include "CommonType.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class NpcDistanceLayer;
class CheckPointDialog;
class PauseDialog;
class CircularMaskNode;
class StageParallaxLayer;
class BoosterControl;
class NPCOrder;
class NPCRating;
class DeliveryCompleteNoticeView;

class GameSceneLayer : public Layer
{
	friend class GameSceneTest;
	
public:
	enum Animation {
		PopStarValue,
	};
    
    enum DialogueType{
        Waiting = 1,
        OnMyWay = 2,
        Coming  = 3,
        HurryUp = 4,
        Faster  = 5,
        ISeeYou = 6,
        Thanks  = 7
    };
    
public:
	static Scene* createScene();
	static Scene* createTutorialScene(bool startFromTitle = false);
	
	CREATE_FUNC(GameSceneLayer);
	
	CC_SYNTHESIZE(bool, mTutorialMode, TutorialMode);
	SYNTHESIZE_BOOL(mTutorialFromTitle, TutorialFromTitle);
	
	virtual bool init();
	//	void showMainGame();
	virtual void onEnter();
	virtual void onExit();
	//	void handleExit();
	
	// UI Visibility
	void hideGameUI();
	void showGameUI();
	
    //void updateScore();
    
	//
	void updateCoinWithValue(int newTotal, bool showAnimation);
	void updateCoins(bool showAnimation = true);
    void updateCandy(bool showAnimation = true);
	void updatePuzzleCount(bool showAnimation = true);
	void updatePuzzleCount(int newValue, bool showAnimation);
	
    // update by game world
    void updateScore(int score, bool showAnimation = false);
	void update(float delta);

	
	
	// Dialog handling
	void showPauseDialog();
	void closePauseDialog();
    void showGameOverDialog();
	void gotoEndGameScene();
	
	Vec2 getStarCounterPosition();
	Vec2 getCandyCounterPosition();
    
	bool handleUnlockNewDogs(const std::vector<int> dogList);


	void handleWorldEvent(Ref *sender, GameWorldEvent event, float arg);
	void setStartOnEnter(bool flag);
	
	void enableCapsuleButton(int itemEffectType);
	void disableCapsuleButton();
	
    void showCaution(float posX);
    void popValue(Text* text,const std::string& num);
	
	void playEnergyParticle();
	
	void setTutorialUI(bool enable);
    Node *addOneTimeAnimation(const std::string &csbname, const Vec2 &position,
							float scale = 1,bool destroySelf = true, bool isRepeat = false );
	bool canShowAD();
	void handleEndGameLogic();		// Player choose "play again" or "exit"
	
	
	bool anyUnlockDog();
	bool anyDogSuggestion();
	void handleUnlockDogInfo();
	void handleDogSuggestion();
    
    void resetCandyState();
    
    //set customer GUI data
    void setRemainDistance(float distance);
    void setNpcInfo(NPCOrder* order);
	
	void setTopPanelVisible(bool flag);
	
    //pop customer dialogue
    void showDialogue(DialogueType type, float delay, float duration);
    
    //show/hide clock and npc UI
    void hideNpcAndTimeUI();
    void showNpcAndTimeUI();
    
    //show/hide timeUp Text
    void setTimeUpTextVisibility(bool isVisible);
    
    //show/hide timer Text
    void setTimeTextVisibility(bool isVisible);
    
    void showBlockCounter(BlockType blockerType, int count);
    void hideBlockCounter();
    
#pragma mark - deliveryNotice UI
    void showDeliveryNotice();
    void setDeliveryReward(NPCOrder* order);
    void setDeliveryTips(int tips);
    void setDeliveryOnShowRewardCallback(std::function<void()> callback);
    void setDeliveryOnCloseCallback(std::function<void()> callback);
    void closeDeliveryNotice();
	void showDeliveryReward();


#pragma mark - Data
private:	// internal Data
    //customer GUI components
    Sprite *mNpcIcon;
    Text *mNpcName;
    Text *mDistanceText;
	Text *mTimeUsedText;
    Text *mMetersText;
    Text *mRewardText;
    
    Text *mScoreText;
    Text *mSpeedText;
	Text *mTimeText;
    Text *mTimeUnitText;
	Text *mCoinText;
	Text *mPuzzleText;
	Text *mDebugText;
	Text *mEnergyText;
	Node *mRootNode;
	Node *mDialog;
	Node *mTopUIPanel;
    Node *mTimePanel;
    Node *mNpcPanel;
	Node *mMainUIPanel;
	Node *mWorldNode;
    Node *mCandyPanel;
    Node *mBlockPanel;
    Sprite *mDistanceBarBG;
	
    Sprite *mFinishGameSprite;
	Text *mFinishGameText;
    
    CircularMaskNode *mCircularMask;
//    Node *mAnimationNode;
    LoadingBar* mEnergyBar;
	PauseDialog *mPauseDialog;
	StageParallaxLayer *mParallaxLayer;
	Node *mTapHintPanel;
	Button *mPauseButton;
	Button *mCapsuleButton;
	Button *mDashButton;
	ParticleSystemQuad *mEnergyParticle;
    
    DeliveryCompleteNoticeView *mNoticeView;
	bool mStartOnEnter;			// for debugging
	int mTimeDigit;
    float mInitStarScale;
	int mLastBestDistance;
	
private:
	void setupUI(Node *mainPanel);
	void setupDashButton(Node *mainPanel);
	void setTime(float remainTime);
	void startGame();
	void handleGameOver();
    void handleUpdateBarEnergy(int latestEnergy);
	void removeDialog();
	
	void setPlayerCoin(int coin);
	void updateEnergyText(int current, int max);
	void setupEnergy();
	void updateEnergy(int latest);
	
	// GameOver Handler
	void onGameOverPlayAgain(Ref *sender);	// Retry 
	void onGameOverBackHome(Ref *sender);	// Back to main
	void onGameOverShare(Ref *sender);		// Share
	void onGameOverRevive(Ref *sender);		// One more chance

    void saveCollectedCoins();
	
	void updateDebugInfo();
    void updateSpeedText();
	

	//
	void hideTapHint();
	void showTapHint();

	
	// Pause Handling
	void onPauseGame();
	void onResumeGame(Ref *sender);
	void onBackHome(Ref *sender);
	

	// Visibility handling
	void showGameWorld();
	void hideGameWorld();
	
	
	// Planet Background
	void setupPlanet();
	
	// Top Panel
	
public:
    BoosterControl *getBoosterControl();
	void setupBoosterControl(const Vec2 &pos);
	void setSelectedBooster(BoosterItemType boosterType);
private:
	BoosterControl *mControl;


#pragma mark - Related to Game Finish
public:
	void setFinishGameTitleVisible(bool flag);

#pragma mark - Related to Time Extension
public:
	void showTimeExtended(int extendValue);
private:
	void setupTimeExtendPanel();
	
private:
	Node *mTimeExtendPanel;
	Text *mTimeExtendText;

	
	
#pragma mark - Reset
public:
	void reset();
	
	
#pragma mark - Clock CountDown Effect
public:
	void startClockCountDownEffect();
	void pauseClockCountDownEffect();
	void resumeClockCountDownEffect();
	void stopClockCountDownEffect();

	
private:
	void setTimeCounterColor(bool isHurryUp);
	void setClockCountDownFrameVisible(bool flag);
private:
	ui::ImageView *mClockCountDownFrame;		// it is an image

	
#pragma mark - Time Used Text
public:
	void updateTimeUsed();

#pragma mark - Distance Layer 
public:
	NpcDistanceLayer *getNpcDistanceLayer();
	void showNpcDistance(NPCOrder *order);
private:
	void setupNpcDistanceLayer();
private:
	NpcDistanceLayer *mNpcDistanceLayer;


#pragma mark - CheckPoint Reward
public:
	void showCheckPointDialog(NPCRating *rating, const std::function<void()> &callback = nullptr);
	
private:
	void setupCheckPointDialog();
	
private:
	CheckPointDialog *mCheckPointDialog;

#pragma mark - Effect and Animation
private:
	void setupEffectLayerPosition();		// Setup position setting in EffectLayer

public:
	void runAddScoreAnimation(const int &score, const int &finalScore,
							  const Vec2 &startPos,
							  const std::function<void ()> &callback,
							  const float &duration);

public:
	std::string statInfo();

#pragma mark - Tutorial
private:
	void backToMainSceneFromTutorial();
};




#endif /* GameScene_hpp */
