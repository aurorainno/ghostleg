//
//  MainScene.cpp
//  GhostLeg
//
//  Created by Ken Lee on 18/3/2016.
//
//

#include "MainScene.h"


#include "cocostudio/CocoStudio.h"
#include "TDDHelper.h"
#include "GameScene.h"
#include "StageManager.h"
#include "LevelData.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "RateAppHelper.h"
#include "GameManager.h"
#include "CommonMacro.h"
#include "CommonType.h"
#include "UserGameData.h"
#include "TutorialView.h"
#include "ViewHelper.h"
#include "AnimationHelper.h"
#include "DebugInfo.h"
#include "GameModel.h"
#include "MasteryScene.h"
#include "CoinShopScene.h"
#include "SuitScene.h"
#include "GameCenterManager.h"
#include "SuitManager.h"
#include "AdManager.h"
#include "SettingScene.h"
#include "GmScene.h"
#include "Analytics.h"
#include "GameCenterLayer.h"
#include "GameSound.h"
#include "SimpleTDD.h"
#include "StageSelectionView.h"
#include "PlanetManager.h"
#include "Constant.h"
#include "DogSelectionScene.h"
#include "DogManager.h"
#include "LevelData.h"
#include "UpdateChecker.h"
#include "UnlockPlanetLayer.h"
#include "RewardManager.h"
#include "DailyRewardDialog.h"
#include "BuyNewDogLayer.h"
#include "AnimationHelper.h"
#include "CircularMaskNode.h"
#include "DogData.h"
#include "StringHelper.h"
#include "RichTextLabel.h"
#include "StageManager.h"
#include "TimeHelper.h"


#include "PlayerRecord.h"

#include "TutorialLayer.h"
#include "StringHelper.h"


#include "TutorialLayer.h"
#include "StringHelper.h"
#include "PlayerRecord.h"
#include "UIBestDistance.h"
#include "MainSceneInfoLayer.h"
#include "AnimeNode.h"
#include "EventPopUpLayer.h"
#include "LeaderboardDialog.h"
#include "FBProfilePic.h"
#include "ButtonAnimationHelper.h"
#include "LeaderboardScene.h"
#include "BoosterLayer.h"
#include "LuckyDrawDialog.h"
#include "NotEnoughMoneyDialog.h"
#include "StageParallaxLayer.h"
#include "MailboxLayer.h"
#include "CharacterSelectionLayer.h"
#include "PlayerManager.h"
#include "LuckyDrawLayer.h"
#include "TimeHelper.h"

const bool SHOW_COMING_SOON = true;

// Macro
#define LOG_GA(__action__)	LOG_EVENT(Analytics::Category::main, __action__, "", 0)

using namespace cocostudio::timeline;

namespace  {	// local private function
	void setupPlayerSpriteAnimation(Sprite *sprite)
	{
		sprite->setScale(0.5f);
		Vector<SpriteFrame*> animFrames(2);
		char str[100];
		for(int i=1; i<=2; i++) {
			sprintf(str, "alienYellow_walk%d.png", i);
			auto frame = SpriteFrame::create(str, Rect(0,0,70,86));
			animFrames.pushBack(frame);
		}

		auto animation = Animation::createWithSpriteFrames(animFrames, 0.2f, -1);
		auto animate = Animate::create(animation);
		sprite->runAction(animate);
	}

	void showAlert(Node *parent, std::string msg)
	{
		if(parent == nullptr) {
			return;
		}

		Vec2 uiPos(0, 80);
		Vec2 glPos = Director::getInstance()->convertToGL(uiPos);

		ViewHelper::showFadeAlert(parent, msg, glPos.y);
	}
}

int MainSceneLayer::nextDefaultPlanet = -1;

Scene* MainSceneLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	MainSceneLayer *layer = MainSceneLayer::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

void MainSceneLayer::setNextDefaultPlanet(int planetID)
{
    nextDefaultPlanet = planetID;
}

// on "init" you need to initialize your instance
bool MainSceneLayer::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}

	// Clean up
	mLeaderboardTimeStarted = false;
	mCharModel = nullptr;
	mTutorialLayer = nullptr;
    mBestDistanceNode = nullptr;
    mInfoLayer = nullptr;
	mCoachmarkShown = false;
    mLastLeaderboardRemainTime = -1;

	// Setup spritesheet
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("item.plist");

	// initialize
	mLastBackClickTime = 0;

    mBGMAboutToChange = false;

    mPendingforGameCenter = false;

    GameSound::playMusic(GameSound::Title);

	// Setup the space background
    StageParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	parallaxLayer->setBGSpeed(0);
	parallaxLayer->setStage(1);
	parallaxLayer->setShowRoute(false);
    addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();

	// Setup UI
	setupGUI("MainSceneLayer.csb");

	// setupUI(rootNode);

	//TDDHelper::addTestButton(this, Vec2(200, 80));

	//addTestBackButton();
	SimpleTDD::setup(this, Vec2(280, 180), "test");

	// Loading Map
	int selectedPlanet = PlanetManager::instance()->getSelectedPlanet();
	
	MapLevelManager::instance()->setMapCycleForPlanet(selectedPlanet);

    setKeyListener();

	return true;
}

#pragma mark - UI Construction

void MainSceneLayer::setupGUI(const std::string &csbName)
{
	Node *mainPanel = CSLoader::createNode(csbName);
    mRootNode = mainPanel;
	addChild(mainPanel);

	FIX_UI_LAYOUT(mainPanel);

	Node *topPanel = mainPanel->getChildByName("topPanel");

    Node *centerPanel = mainPanel->getChildByName("centerPanel");

	// Binding the Buttons
	mBasePanel = mainPanel->getChildByName("basePanel");
    
	//basePanel->add

	setupTabBar(mainPanel);
	
	// Player Node
	setupPlayerAnimation(mainPanel);

	
	// Buttons Binding
	setupButtons(mainPanel);
	
	// Leaderboard
	setupLeaderboard(mRootNode);
	
		// Star Value
	mTotalCoinText = (Text *) topPanel->getChildByName("totalCoinText");

	
	
	

	

    mPlanetNameText = mainPanel->getChildByName<Text*>("planetName");
    mPlanetInfoText = mainPanel->getChildByName<Text*>("planetInfo");
	
	setupProfilePic(mainPanel);
	
	setupLeaderboardTimeGUI(mainPanel);

//    mBestDistanceNode = UIBestDistance::create();
//    mBestDistanceNode->setPosition(Vec2(getContentSize().width/2 - 20,centerPanel->getPositionY()+70));
//    mBestDistanceNode->setScale(0.53);
//    mRootNode->addChild(mBestDistanceNode);
    
    setupStageSelection();

    mNewLabel = (Sprite*) mTabBar->getChildByName("newLabel");
    AnimationHelper::setMoveUpEffect(mNewLabel, 0.3, 5, false, 0);
//    AnimationHelper::setJellyEffect(mNewLabel, 0.35, 1.3, 0);
    
	// Update the version
	Text *versionText = (Text *) mainPanel->getChildByName("versionText");
	if(versionText) {
		versionText->setString(GameManager::instance()->getVersionString());
	}
    
    
//    mInfoLayer = MainSceneInfoLayer::create();
//    mInfoLayer->setPosition(Vec2(24,100));
//    mInfoLayer->setCallback([&](int planetID){
//        this->enterDogSelectionPage(planetID);
//    });
//    mRootNode->addChild(mInfoLayer);
    
    
    mCandyPanel = topPanel->getChildByName("candyPanel");
    mDiamondPanel = topPanel->getChildByName("diamondPanel");
	
    
	
	setupEventNotice();
	// TODO: separate from this core method
//    mEventPanel = AnimeNode::create();
//    mEventPanel->setup("UI_event_xmas.csb");
//    mEventPanel->playAnimation("active", true);
//    Vec2 pos = mLeftButton->getPosition();
//    mEventPanel->setPosition(Vec2(pos.x + 30,pos.y + 30));
//    mEventPanel->setScale(0.5);
//    mRootNode->addChild(mEventPanel);
//    
//    mEventBtn = mRootNode->getChildByName<Button*>("eventBtn");
//    mEventBtn->addClickEventListener([&](Ref*){
//        this->scrollToPlanet(51);
//    });
}

void MainSceneLayer::setupButtons(Node *mainPanel)
{
	Node *topPanel = mainPanel->getChildByName("topPanel");
	
	// GM Button
	Button *gmButton = (Button *) mainPanel->getChildByName("gmButton");
	gmButton->addClickEventListener([&](Ref *sender){
		gotoGMTool();
	});
	mGmButton = gmButton;
	mGmButton->setVisible(false);
	

	
	// Play
	Button *playButton = (Button *) mBasePanel->getChildByName("playButton");
	playButton->addClickEventListener([&](Ref *sender){
		
		Button *button = (Button *)sender;
		onPlayClicked(button);
	});
	mStartButton = playButton;
	AnimationHelper::setJellyEffect(mStartButton, 1, 1.02, 0);
	
	
	// Facebook Like
	mFbBtn = (Button *) topPanel->getChildByName("fbBtn");
	mFbBtn->addClickEventListener([&](Ref *sender){
		AstrodogClient::instance()->connectFB();
	});
	
	
	//
	mLeftButton = (Button *) mBasePanel->getChildByName("leftButton");
	mLeftButton->addClickEventListener([&](Ref *sender){
        GameSound::playSound(GameSound::ChangeStage);
		switchPlanetByButton(false);
	});
	
	mRightButton = (Button *) mBasePanel->getChildByName("rightButton");
	mRightButton->addClickEventListener([&](Ref *sender){
        GameSound::playSound(GameSound::ChangeStage);
		switchPlanetByButton(true);
	});
	

	// ken: Old GUI
//	mDailyRewardBtn = (Button *) topPanel->getChildByName("dailyRewardButton");
//	mDailyRewardBtn->addClickEventListener([&](Ref *sender){
//		GameSound::playSound(GameSound::Sound::Button1);
//		showDailyRewardDialog(true);
//	});
	
	
	Button *giftButton = (Button *) topPanel->getChildByName("giftButton");
	giftButton->addClickEventListener([&](Ref *sender){
		onRewardMailClicked((Button *) sender);
	});
	
	// Leaderboard
	Button *leaderboardButton = (Button *) mTabBar->getChildByName("leaderboardButton");
	ButtonAnimationHelper::setButtonAnimation(leaderboardButton, 0.02, leaderboardButton->getPosition(), Vec2(0,-5));
	leaderboardButton->addClickEventListener([&](Ref *sender){
		onRankingClicked((Button *) sender);
	});
    
    Node *stageNotCleatNode = mBasePanel->getChildByName("stageNotClearHint");
    mStageNotClearPanel = stageNotCleatNode;
	
	
	
	// Unlock Planet
	mPurchaseBtn = (Button *) mBasePanel->getChildByName("purchaseButton");
	mPurchaseBtn->addClickEventListener([&](Ref *sender){
		
		Button *button = (Button *)sender;
		onUnlockPlanetClicked(button);

	});
	
}

void MainSceneLayer::setupTabBar(Node *mainPanel)
{
	mTabViewLayer = mainPanel->getChildByName("tabView");
	mTabBar = mainPanel->getChildByName("tabBar");
	
	// Dog
	Button *dogButton = (Button *) mTabBar->getChildByName("dogButton");
	ButtonAnimationHelper::setButtonAnimation(dogButton, 0.02, dogButton->getPosition(), Vec2(0,-5));
	dogButton->addClickEventListener([&](Ref *sender){
		GameSound::playSound(GameSound::Sound::Button1);
		gotoDogSelection();
	});
	mDogButton = dogButton;
	
	
	// Shop
	Button *shopButton = (Button *) mTabBar->getChildByName("addStarButton");
	ButtonAnimationHelper::setButtonAnimation(shopButton, 0.02, shopButton->getPosition(), Vec2(0,-5));
	shopButton->addClickEventListener([&](Ref *sender){
		GameSound::playSound(GameSound::Sound::Button1);
		gotoShop();
	});
	
	
	// Option
	
	Button *optionButton = (Button *) mTabBar->getChildByName("optionButton");
	ButtonAnimationHelper::setButtonAnimation(optionButton, 0.02, optionButton->getPosition(), Vec2(0,-5));
	optionButton->addClickEventListener([&](Ref *sender){
		GameSound::playSound(GameSound::Sound::Button1);
		gotoOption();
	});
	

	Button *gachaButton = (Button *) mTabBar->getChildByName("gachaButton");
	ButtonAnimationHelper::setButtonAnimation(gachaButton, 0.02, gachaButton->getPosition(), Vec2(0,-5));
    
    AnimeNode* newAnimation = AnimeNode::create();
    newAnimation->setup("ui_alert_dot.csb");
    newAnimation->setScale(1.5);
    newAnimation->setStartAnime("active",true,false);
    mGachaBtnNewLabel = gachaButton->getChildByName("newLabel");
    mGachaBtnNewLabel->addChild(newAnimation);
    mGachaBtnNewLabel->setVisible(false);
    
	// Scene ...
	gachaButton->addClickEventListener([&](Ref *sender){
		onGachaClicked();
		//       addChild(dialog);
	});
    
    

}

void MainSceneLayer::loadLeaderboard(AstrodogLeaderboardType type)
{
    int planet = mStageSelectionView->getSelectedPlanetID();
    AstrodogClient::instance()->getRanking(type, planet);
    if(mLBDialog){
        mLBDialog->setLeaderboardGUI(type);
        mLBDialog->showLoading();
        mLBDialog->setLeaderboardGUI(type);
        mCurrentLeaderBoardType = type;
    }
    
}

#pragma mark - Stage Logic
int MainSceneLayer::getCurrentStageIndex()
{
	return mStageSelectionView->getCurrentPage();
}

PlanetItemView *MainSceneLayer::getSelectedPlanetItemView()
{
	int selectedIndex = getCurrentStageIndex();
	
	return mStageSelectionView->getPlanetItemViewAtPage(selectedIndex);
}

int MainSceneLayer::getSelectedPlanet()
{
	PlanetItemView *itemView = getSelectedPlanetItemView();
	
	int result = itemView == nullptr ? 0 : itemView->getPlanetID();
	
	log("DEBUG: MainSceneLayer.getSelected: %d", result);
	
	return result;
}

void MainSceneLayer::updateCurrentStageInfo()
{
	int selectedPlanet = getSelectedPlanet();
	//updatePlanetInfoUI(true, selectedPlanet);
	if(mInfoLayer){
		mInfoLayer->setup(selectedPlanet);
	}
}

void MainSceneLayer::setupEventNotice()
{
	
	bool hasEvent = GameManager::instance()->isEventOn();	// event here mean Seasonal Event like Christmas
	
	if(hasEvent == false) {
		mEventPanel = nullptr;
		mEventBtn = nullptr;
		return;
	}

	// The event notice
	mEventPanel = AnimeNode::create();
	mEventPanel->setup("UI_event_xmas.csb");
	mEventPanel->playAnimation("active", true);
	Vec2 pos = mLeftButton->getPosition();
	mEventPanel->setPosition(Vec2(pos.x + 30,pos.y + 30));
	mEventPanel->setScale(0.5);
	mRootNode->addChild(mEventPanel);
	
	// A hidden button to scroll to North Pople
	mEventBtn = mRootNode->getChildByName<Button*>("eventBtn");
	mEventBtn->addClickEventListener([&](Ref*){
		if(GameManager::instance()->isEventOn()) {	// this value may check when user click it
			scrollToPlanet(51);
		}
	});

}

void MainSceneLayer::enterDogSelectionPage(int planetID)
{
    auto scene = Scene::create();
    
    DogSelectionSceneLayer* layer = DogSelectionSceneLayer::create();
    layer->setEnterAtSpecificPlanet(planetID);
    
    scene->addChild(layer);
    Director::getInstance()->pushScene(scene);
}

void MainSceneLayer::updateEventNodeVisibility()
{
    if(mEventPanel){
        int planetID = mStageSelectionView->getSelectedPlanetID();
        PlanetData *data = PlanetManager::instance()->getPlanetData(planetID);
        if(data->getPlanetType() == PlanetData::Event){
            mEventPanel->setVisible(false);
            mEventBtn->setVisible(false);
        }else{
            mEventPanel->setVisible(true);
            mEventBtn->setVisible(true);
        }
    }
}

void MainSceneLayer::showUI()
{
	if(mRootNode) {
        mRootNode->setVisible(true);
	}
}

void MainSceneLayer::hideUI()
{
	if(mRootNode) {
        mRootNode->setVisible(false);
	}
}

void MainSceneLayer::hideMainUI()
{
	if(mBasePanel) {
		mBasePanel->setVisible(false);
	}
	if(mLBDialog) {
		mLBDialog->setVisible(false);
	}
	if(mTabBar) {
		mTabBar->setVisible(false);
	}
}
void MainSceneLayer::showMainUI()
{
	if(mBasePanel) {
		mBasePanel->setVisible(true);
	}
	if(mLBDialog) {
		mLBDialog->setVisible(true);
	}
	if(mTabBar) {
		mTabBar->setVisible(true);
	}
}

void MainSceneLayer::updateBestDistanceNode(int planetID)
{
    if(mBestDistanceNode == nullptr){
        return;
    }
    if(PlanetManager::instance()->isPlanetUnLocked(planetID) == false){
        mBestDistanceNode->setVisible(false);
    }else{
        mBestDistanceNode->setVisible(true);
        mBestDistanceNode->setupBestDistanceText(planetID);
        mBestDistanceNode->runAnimation();
    }
}

//void MainSceneLayer::setInfoPanel(int planetID)
//{
//    bool isUnlocked = PlanetManager::instance()->isPlanetUnLocked(planetID);
//    
//    if(isUnlocked){
//        mLockedInfoPanel->setVisible(false);
//        int infoDogID, infoPlanetID, unlockValue, requiredValue;
//        DogManager::instance()->getNextUnlockDog(infoDogID, infoPlanetID); //infoPlanetID is useless here
//        DogManager::instance()->getUnlockProgress(infoDogID, unlockValue, requiredValue);
//        
//        mInfoPanelPlanetID = PlanetManager::instance()->getPlanetWithDog(infoDogID);
//        DogData *dogData = DogManager::instance()->getDogData(infoDogID);
//		if(dogData == nullptr) {
//			mUnlockInfoPanel->setVisible(false);
//			return;
//		}
//		
//		mUnlockInfoPanel->setVisible(true);
//		
//        infoPlanetID = DogManager::instance()->getBestDistancePlanetID(dogData);
//        PlanetData* planetData = PlanetManager::instance()->getPlanetData(infoPlanetID);
//        
//        //setup dog Icon
//        int resID = dogData->getAnimeID();
//        std::string iconSpriteName = StringUtils::format("dog/dselect_dog%d.png", resID);
//        mDogIconSprite->setTexture(iconSpriteName);
//        
//        //setup progress bar
//        unlockValue = unlockValue > requiredValue ? requiredValue : unlockValue;
//        float percent;
//        if(unlockValue > requiredValue){
//            percent = 100;
//        }else if(requiredValue <= 0){
//            percent = 0;
//        }else{
//            percent = (float) unlockValue / requiredValue *100;
//        }
//        mDogProgressBar->updateAngleByPercent(percent);
//        
//        //setup info text
//        std::string planetName = "";
//        if(planetData){
//            planetName = planetData->getName();
//        }
//        std::string dogName = dogData->getName();
//        std::string progress = StringUtils::format("<c1>%d</c1>/%d",unlockValue,requiredValue);
//
//        std::string infoStr = dogData->getUnlockInfo() + " to unlock <c2>#dog</c2>";
//        aurora::StringHelper::replaceString(infoStr, "#planet", planetName);
//        aurora::StringHelper::replaceString(infoStr, "#v", progress);
//        aurora::StringHelper::replaceString(infoStr, "#dog", dogName);
//        
//        mUnlockInfoText->setTextColor(2, Color4B(255,211,49,255));
//        mUnlockInfoText->setTextColor(1, Color4B(14,255,217,255));
//
//        mUnlockInfoText->setString(infoStr);
//        
//    }else{
//        mUnlockInfoPanel->setVisible(false);
//        mLockedInfoPanel->setVisible(true);
//        PlanetData* planetData = PlanetManager::instance()->getPlanetData(planetID);
//        mInfoPanelPlanetID = planetID;
//        std::string planetName = "";
//        if(planetData){
//            planetName = planetData->getName();
//        }
//        Text* planetText = mLockedInfoPanel->getChildByName<Text*>("planetName");
//        planetText->setString(planetName);
//    }
//}

void MainSceneLayer::setupStageSelection()
{
	
	ui::Layout *parent = mBasePanel->getChildByName<ui::Layout *>("stageSelectPanel");
	if(parent == nullptr) {
		return;
	}
	parent->setBackGroundColorType(ui::Layout::BackGroundColorType::NONE);

	// Getting the Current selected Planet
	//PlanetManager::instance()->checkAndFixSelectedPlanet();
	int planetID = PlanetManager::instance()->getSelectedPlanet();
	
	
	// Create the View
	StageSelectionView *selectionView = StageSelectionView::create();
	selectionView->setPosition(Vec2::ZERO);
	parent->addChild(selectionView);

	mStageSelectionView = selectionView;

	
	// Setup callback
	mStageSelectionView->setSelectPlanetCallback([&](Ref *ref, int planetID, bool isRepeat, int currentPage, int maxPage) {
		onPlanetSelected(planetID, isRepeat, currentPage, maxPage);
	});
    
    mStageSelectionView->setOnScrollCallback([&](Vec2 offset){
        this->onPlanetScrolled(offset);
    });

    mStageSelectionView->setOnUnlockedPlanetClickCallback([&](){
        this->startGame();
    });
	
	
	// 
    mStageSelectionView->setSelectedPlanet(planetID);

}


void MainSceneLayer::gotoFacebookPage()
{
	log("Go to facebook");
	Application::getInstance()->openURL(kFacebookPageLink);
	
	LOG_GA(Analytics::click_fb);
}

void MainSceneLayer::gotoOption()
{
//	auto scene = SettingSceneLayer::createScene();
//	Director::getInstance()->pushScene(scene);
    SettingSceneLayer *layer = SettingSceneLayer::create();
    layer->setOnExitCallback([&](){
        mTabBar->setVisible(true);
    });
    removeOldMenuItem();
    addNewMenuItem(layer);
    mTabBar->setVisible(false);
    
	LOG_GA(Analytics::click_setting);
}

void MainSceneLayer::setInProgressNotice(Button *button)
{
	if(button == nullptr) {
		return;
	}

	Node *mainLayer = this;
	button->addTouchEventListener([&, mainLayer](Ref *sender, Widget::TouchEventType type) {
		showAlert(mainLayer, "Sorry, We are working on this feature!");
		// auto scene = MasterySceneLayer::createScene();
		// Director::getInstance()->pushScene(scene);
	});
}

void MainSceneLayer::checkNewFreeGacha()
{
    mGachaBtnNewLabel->setVisible(false);
    
    int lastPlayAdTime =  GameManager::instance()->getUserData()->getGachaLastPlayAdTime();
    int currentTime = TimeHelper::getCurrentTimeStampInSecSinceMay2017();
    
    int timeDiff = currentTime - lastPlayAdTime;
    if(timeDiff < 0){
        //unexpected behaviour, maybe user cheating
        return;
    }
    
    if(timeDiff > kPlayAdCountDownInSec){
        mGachaBtnNewLabel->setVisible(true);
    }

}

// OLD Logic
//void MainSceneLayer::setupUI(cocos2d::Node *mainPanel)
//{
//	FIX_UI_LAYOUT(mainPanel);
//
//
//	setupPlayerAnimation(mainPanel);
//
//	Node *centerPanel = mainPanel->getChildByName("centerPanel");
//	Node *titlePanel = mainPanel->getChildByName("titlePanel");
//
//	// Best and Last distance
//	mBestScoreText = (Text *) titlePanel->getChildByName("bestScoreText");
//	mTotalCoinText = (Text *) mainPanel->getChildByName("totalCoinText");
//
////	Sprite *playerSprite = (Sprite *)centerPanel->getChildByName("playerSprite");
////	setupPlayerSpriteAnimation(playerSprite);
//
//	Button *slopeModeButton = (Button *) mainPanel->getChildByName("slopeModeButton");
//	slopeModeButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//        switch(type) {
//            case ui::Widget::TouchEventType::BEGAN: {
//                break;
//            }
//
//            case ui::Widget::TouchEventType::ENDED: {
//				GameWorld::instance()->setIfAllowSlopeMode(true);
//				GameSound::playSound(GameSound::Button);
//				startGame();
//                break;
//            }
//        }
//    });
//	if(!GameManager::instance()->isDebugMode()) {
//		slopeModeButton->setVisible(false);
//	}
//
//
//
//
//
//
//    Button *leaderBoardBtn = (Button *) mainPanel->getChildByName("leaderBoardBtn");
//
//    leaderBoardBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//        switch(type) {
//            case ui::Widget::TouchEventType::BEGAN:{
//                GameSound::playSound(GameSound::Sound::Button1);
//                break;
//            }
//            case ui::Widget::TouchEventType::ENDED: {
//                  GameManager::instance()->getGameCenterManager()->showBestDistanceLeaderBoard();
//                break;
//            }
//        }
//    });
//
//    Button *optionBtn = (Button *) mainPanel->getChildByName("optionBtn");
//	optionBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//		switch(type) {
//            case ui::Widget::TouchEventType::BEGAN:{
//                GameSound::playSound(GameSound::Sound::Button1);
//                break;
//            }
//			case ui::Widget::TouchEventType::ENDED: {
//				auto scene = SettingSceneLayer::createScene();
//				Director::getInstance()->pushScene(scene);
//				break;
//			}
//		}
//	});
//
//    Button *masteriesBtn = (Button *) mainPanel->getChildByName("masteriesBtn");
//    masteriesBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//        switch(type) {
//            case ui::Widget::TouchEventType::BEGAN:{
//                GameSound::playSound(GameSound::Sound::Button1);
//                break;
//            }
//            case ui::Widget::TouchEventType::ENDED: {
//				auto scene = MasterySceneLayer::createScene();
//				Director::getInstance()->pushScene(scene);
//                break;
//            }
//        }
//    });
//
//
//    Button *addStarBtn= (Button *) mainPanel->getChildByName("addStarBtn");
//
//    addStarBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//        switch(type) {
//            case ui::Widget::TouchEventType::BEGAN:{
//                GameSound::playSound(GameSound::Sound::Button1);
//                break;
//            }
//            case ui::Widget::TouchEventType::ENDED: {
//                auto scene = CoinShopSceneLayer::createScene();
//                Director::getInstance()->pushScene(scene);
//                break;
//            }
//        }
//    });
//
//	Button *suitsBtn = (Button *) mainPanel->getChildByName("suitsBtn");
//	suitsBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//		switch(type) {
//            case ui::Widget::TouchEventType::BEGAN:{
//                GameSound::playSound(GameSound::Sound::Button1);
//                break;
//            }
//			case ui::Widget::TouchEventType::ENDED: {
//				auto scene = SuitSceneLayer::createScene();
//				Director::getInstance()->pushScene(scene);
//				break;
//			}
//		}
//	});
//
//	Button *gmBtn = (Button *) mainPanel->getChildByName("gmBtn");
//	gmBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//		switch(type) {
//			case ui::Widget::TouchEventType::ENDED: {
//				auto scene = GmSceneLayer::createScene();
//				Director::getInstance()->pushScene(scene);
//				break;
//			}
//		}
//	});
//	mGmButton = gmBtn;
//	mGmButton->setVisible(false);
//
//
//	//
//	ViewHelper::setWidgetSwallowTouches(centerPanel, false);
//	ViewHelper::setWidgetSwallowTouches(mainPanel->getChildByName("Title"), false);
//
//	//
//	//
//    auto listener = EventListenerTouchOneByOne::create();
//    listener->setSwallowTouches(true);
//
//    listener->onTouchBegan = [&](Touch *touch, Event *event) {
//        log("Start is clicked");
//		handleTouchLayer();
//		return true;
//    };
//    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, mainPanel);
//
//
//
//
//
//}


void MainSceneLayer::update(float delta){

//    if(mPendingforGameCenter){
//        if(mManager->hasSignedIn()){
//            log("Player has signed in to game center/google play!");
//            mManager->showBestDistanceLeaderBoard();
//            mPendingforGameCenter = false;
//            unscheduleUpdate();
//        }
//    }

	if(mTutorialLayer) {
		mTutorialLayer->update(delta);
	}
	
	updateLeaderboardTime(delta);
}

void MainSceneLayer::setupPlayerAnimation(Node *mainPanel)
{
	Node *playerNode = mBasePanel->getChildByName("playerNode");

	if(!playerNode) {
		return;
	}

	GameModel *player = GameModel::create();
//	player->setup("character1.csb");
//	player->setAction(GameModel::Action::Idle);
	player->setScaleX(-player->getScaleX());
	playerNode->addChild(player);
	mCharModel = player;
}

void MainSceneLayer::scrollToPlanet(int planetID)
{
    if(mStageSelectionView->getPageForPlanet(planetID) == mStageSelectionView->getCurrentPage()){
        return;
    }
    
    mStageSelectionView->scrollToPage(mStageSelectionView->getPageForPlanet(planetID));
//    mStartButton->setOpacity(100);
    setPlayButtonOpacity(100);
    if(mInfoLayer){
        mInfoLayer->setVisible(false);
    }
}

void MainSceneLayer::checkNewDogs()
{
    if(DogManager::instance()->hasNewDog()){
        mNewLabel->setVisible(true);
    }else{
        mNewLabel->setVisible(false);
    }
}

bool MainSceneLayer::checkNewPlanets()
{
    std::vector<int> newPlanetList = PlanetManager::instance()->getNewUnlockedPlanets();
    if(newPlanetList.size() == 0){
		return false;
	}

	hideUI();
    GameSound::playSound(GameSound::Unlock);
    UnlockPlanetLayer* layer = UnlockPlanetLayer::create();
    layer->setCloseCallback([&](int planetID){
		GameSound::stopSound(GameSound::Unlock);
		this->showUI();
        this->scrollToPlanet(planetID);
	});
	
	layer->setPlanetList(newPlanetList);
	layer->setParentLayer(this);
    addChild(layer);
	
	return true;
}

void MainSceneLayer::removeOldMenuItem()		// clear TabViewContent
{
    for(int i=0;i<mMenuItemList.size();i++){
        Node* menuItemNode = mMenuItemList.at(i);
        if(!menuItemNode){
            continue;
        }
        if(!menuItemNode->getParent()){
            continue;
        }
        
        if(menuItemNode->getParent() == mTabViewLayer){
            menuItemNode->removeFromParent();
        }
    }
    mMenuItemList.clear();
}

void MainSceneLayer::addNewMenuItem(Node* item)	// Add TabViewContent
{
    mMenuItemList.pushBack(item);
    mTabViewLayer->addChild(item);
}

void MainSceneLayer::onEnter()
{
    log("MainScene: onEnter");
	Layer::onEnter();
	
	//
   
    mTabBar->setVisible(true);
	AdManager::instance()->hideBannerAd();

    if(mBGMAboutToChange){
        GameSound::playMusic(GameSound::Title);
        mBGMAboutToChange = false;
    }
    
    if(mLeaderboardTimePanel->isVisible()){
        mLeaderboardTimePanel->stopAllActions();
        mLeaderboardTimePanel->setPositionY(570);
        MoveBy* moveBy = MoveBy::create(0.5, Vec2(0, -40));
        mLeaderboardTimePanel->runAction(moveBy);
    }

    //daily reward
    showDailyRewardDialog();
    
    checkNewFreeGacha();
    
    if(GameManager::instance()->getPlayerRecord()->getPlayCount()>15){
        GameManager::instance()->showRateNotice(true);
    }
	

	// The stage information
	updateCurrentStageInfo();		// note: the 
	

	// 
	AstrodogClient::instance()->setListener(this);
    loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
    setupFBProfile();

    
	updateGUIData();	// Update the latest star value
	updateCharacter();
    
    //check new dog(s)
    checkNewDogs();			// the "NEW" tag at the dog button

    bool newPlanetUnlocked = checkNewPlanets();		// Popup New Planet dialog
    
    if(nextDefaultPlanet != -1){
        scrollToPlanet(nextDefaultPlanet);
        nextDefaultPlanet = -1;
    }
	
    updatePlanetState();

    LOG_SCREEN(Analytics::Screen::main_screen);
 

	// Visibility of GM Button
	mGmButton->setVisible(GameManager::instance()->getUserData()->isGmMode());

	DebugInfo::showState(false);			// Hiding the stat


	// Check the version update
	checkNewVersion();
    
    //Gavin 2016/11/14
    //Move GameCenterManger setup to MainScene onEnter().
    GameManager::instance()->getGameCenterManager()->setup();


	if(newPlanetUnlocked == false) {
//		handleCoachmarkAction();
	}
    
    updateEventNodeVisibility();
    
    showSelectCharCoachmark();
	
	scheduleUpdate();
}

void MainSceneLayer::showDailyRewardDialog(bool isCheckRecord)
{
    bool isPlayed = GameManager::instance()->getUserData()->isCoachmarkPlayed("selectChar");
    if(!isPlayed){
        return;
    }

    if(RewardManager::instance()->checkDailyRewardStatus(RewardManager::DailyRewardStatusNoNeed) && !isCheckRecord){
        log("Reward already collected today!");
        // showEventPopUp();
        return;
    }else if(RewardManager::instance()->checkDailyRewardStatus(RewardManager::DailyRewardStatusIncreaseCycle)){
        RewardManager::instance()->increaseCycle();
    }else if(RewardManager::instance()->checkDailyRewardStatus(RewardManager::DailyRewardStatusNeedReset)){
        RewardManager::instance()->resetCollectedReward();
    }
    
    DailyRewardDialog *dialog = DailyRewardDialog::create();
    if(!isCheckRecord){
        dialog->setCallback([&,dialog](){
            // showEventPopUp();
            if(dialog->getReward()){
                RewardData* reward = dialog->getReward();
                
                if(reward->getType() == RewardData::RewardTypeDiamond){
 /*                  
                    BuyNewDogLayer* layer = BuyNewDogLayer::create();
                    layer->setDog(reward->getObjectID());
                    layer->setCallback([&,layer](){
                        layer->removeFromParent();
                    });
                    this->addChild(layer);
                    DogManager::instance()->unlock(reward->getObjectID());
                    checkNewDogs();
  */
                    int rewardDiamond = reward->getCount();
                    int originalDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
                    int finalDiamond = originalDiamond + rewardDiamond;
                    GameManager::instance()->getUserData()->addDiamond(reward->getCount());
                    Text* diamondtext = mDiamondPanel->getChildByName<Text*>("totalDiamondText");
                    AnimationHelper::popTextEffect(this, diamondtext, originalDiamond,finalDiamond);
					
					Analytics::instance()->logCollect(Analytics::from_daily_reward, rewardDiamond, MoneyTypeDiamond);
					
                }else if (reward->getType() == RewardData::RewardTypeStar){
					
					int rewardCoin = reward->getCount();
                    int originalCoin = GameManager::instance()->getUserData()->getTotalCoin();
                    int finalCoin = originalCoin + rewardCoin;
                    GameManager::instance()->getUserData()->addCoin(reward->getCount());
                    AnimationHelper::popTextEffect(this, mTotalCoinText, originalCoin,finalCoin);
					
					
					Analytics::instance()->logCollect(Analytics::from_daily_reward, rewardCoin, MoneyTypeStar);
                }else if (reward->getType() == RewardData::RewardTypeFragment){
                    PlayerManager::instance()->addPlayerFragment(reward->getObjectID(), reward->getCount());
					Analytics::instance()->logCollect(Analytics::from_daily_reward, reward->getCount(), MoneyTypePuzzle);
                }
            }
            dialog->closeDialog();
        });
    }else{
        dialog->setDialogMode(DailyRewardDialog::ShowRecordMode);
    }
    
    addChild(dialog);

}


void MainSceneLayer::showEventPopUp()
{
    if(GameManager::instance()->isEventPopUpShown()){
        return;
    }
    
    EventPopUpLayer *layer = EventPopUpLayer::create();
    layer->setCallback([&](Node* node){
        this->scrollToPlanet(51);
        if(node){
            node->removeFromParent();
        }
    });
    addChild(layer);
}

void MainSceneLayer::onExit()
{
	unscheduleUpdate();
	DebugInfo::showState(true);

	if(mBGMAboutToChange) {
        GameSound::stop();
	}

	hideTutorial();		// hide the showing tutorial !!
    removeOldMenuItem();

	Layer::onExit();
}

void MainSceneLayer::handleTouchLayer()
{
	GameWorld::instance()->setIfAllowSlopeMode(false);
	GameSound::playSound(GameSound::Button2);
	startGame();
}

void MainSceneLayer::updateCharacter()
{
	if(mCharModel) {
		std::string name = GameRes::getSelectedCharacterCsb();
        
        std::string fullPath = FileUtils::getInstance()->fullPathForFilename(name);
        
        if(!FileUtils::getInstance()->isFileExist(fullPath)){
            name = GameRes::getCharacterCsbName(1);
        }


		mCharModel->setup(name);
		mCharModel->setAction(GameModel::Action::Idle);
	}
}

void MainSceneLayer::updateGUIData()
{
	int bestDistance = GameManager::instance()->getUserData()->getBestDistance();
	int totalCoin = GameManager::instance()->getUserData()->getTotalCoin();

	// log("DEBUG: score=%d coin=%d", bestDistance, totalCoin);

	// setBestScore(GameManager::instance()->getUserData()->getBestScore());
    updateTotalCoin();
    updateCandyStick();
    updateDiamond();
}

void MainSceneLayer::setBestScore(int score)
{
	char temp[100];
	sprintf(temp, "best distance %dm", score);
	if(mBestScoreText) {
		mBestScoreText->setString(temp);
	}
}


void MainSceneLayer::updateTotalCoin()
{
	char temp[100];
	sprintf(temp, "%d", GameManager::instance()->getUserData()->getTotalCoin());
	if(mTotalCoinText) {
		mTotalCoinText->setString(temp);
	}
}

void MainSceneLayer::updateCandyStick()
{
    if(mCandyPanel == nullptr){
        return;
    }
    
    if(!GameManager::instance()->isEventOn()){
        mCandyPanel->setVisible(false);
        return;
    }
    
    mCandyPanel->setVisible(true);
    Text* candyText = mCandyPanel->getChildByName<Text*>("totalCandyText");
    int numOfCandy = GameManager::instance()->getUserData()->getTotalCandy();
    candyText->setString(StringUtils::format("%d",numOfCandy));
}

void MainSceneLayer::updateDiamond()
{
    if(mDiamondPanel == nullptr){
        return;
    }

    Text* diamondText = mDiamondPanel->getChildByName<Text*>("totalDiamondText");
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    diamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void MainSceneLayer::gotoGameScene()
{


	auto scene = GameSceneLayer::createScene();

	//float duration = 0.5;
	//Director::getInstance()->pushScene(TransitionSplitRows::create(duration, scene ));
	Director::getInstance()->pushScene(scene);
}

void MainSceneLayer::showTutorial()
{
	TutorialView *view = TutorialView::create();

	view->setCallback([&] (Ref *) {
		GameManager::instance()->getUserData()->markTutorialPlayed(true);

		gotoGameScene();		// note: an extra touch is called if close the tutorialView immediately
								//
	});

	addChild(view);
	mTutorialView = view;
}

void MainSceneLayer::hideTutorial()
{
	if(!mTutorialView) {
		return;
	}
	mTutorialView->removeFromParent();
	mTutorialView = nullptr;
}

void MainSceneLayer::startGame()
{
	mCoachmarkShown = false;	// reset coachmark

    mBGMAboutToChange = true;

	gotoGameScene();

	LOG_GA(Analytics::start_game);
}



void MainSceneLayer::setKeyListener()
{
	auto keyboardListener = EventListenerKeyboard::create();
	// keyboardListener->onKeyPressed = CC_CALLBACK_2(TitleLayer::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(MainSceneLayer::keyReleased, this);

	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(
																						  keyboardListener, this);
}
void MainSceneLayer::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	// log("keyReleased: keyCode=%d", keyCode);

	if(keyCode == EventKeyboard::KeyCode::KEY_ESCAPE) {
		// Director::getInstance()->end();
		handleExit();
	}

}


void MainSceneLayer::handleExit()
{
	time_t nowTime;
	time(&nowTime);

	time_t diff = nowTime - mLastBackClickTime;

	log("time diff=%ld", diff);

	if(diff < 5) {		// Less that 5 seconds
		// Execute the exit
		Director::getInstance()->end();
		//log("EXIT NOW!!");
	} else {
		mLastBackClickTime = nowTime;

		showAlert(this, "Click back again to exit");
	}
}


void MainSceneLayer::gotoDogSelection()
{
	// TODO
	auto scene = CharacterSelectionLayer::createScene();
	Director::getInstance()->pushScene(scene);
	
	LOG_GA(Analytics::click_dogmenu);
}

void MainSceneLayer::gotoShop()
{
	//
	hideMainUI();
	
    CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
    layer->setCloseCallback([&](){
        showMainUI();
    });
    layer->setOnPurchaseCallback([&](){
        this->updateDiamond();
        this->updateTotalCoin();
    });
    
    removeOldMenuItem();
    addNewMenuItem(layer);
    
	LOG_GA(Analytics::click_coin_shop);
}

void MainSceneLayer::gotoGMTool()
{
	auto scene = GmSceneLayer::createScene();
	Director::getInstance()->pushScene(scene);
	
	LOG_GA(Analytics::click_gmtool);
}


void MainSceneLayer::setPlayButtonEnable(bool isEnable)
{
//	if(mStartButton) {
//		mStartButton->setEnabled(flag);
//	}
    mStartButton->setOpacity(255);
    mPurchaseBtn->setOpacity(255);
    if(isEnable){
        mStartButton->setVisible(true);
        mPurchaseBtn->setVisible(false);
    }else{
        mStartButton->setVisible(false);
        mPurchaseBtn->setVisible(true);
        
        int planetID = mStageSelectionView->getSelectedPlanetID();
        PlanetData* data = PlanetManager::instance()->getPlanetData(planetID);
        
        std::string name = data->getName();
        int price = data->getPrice();
        
        Text* priceText = mPurchaseBtn->getChildByName<Text*>("price");
        priceText->setString(StringUtils::format("%d",price));
//        Text* nameText = mPurchaseBtn->getChildByName<Text*>("planetName");
//        nameText->setString("unlock "+name);
    }
}

void MainSceneLayer::setPlayButtonOpacity(GLbyte byte)
{
    mStartButton->setOpacity(byte);
    mPurchaseBtn->setOpacity(byte);
}

void MainSceneLayer::onPlanetScrolled(cocos2d::Vec2 offset)
{
//    log("offset=%.2f",-offset.x);
    float centerX = 320 * mStageSelectionView->getCurrentPage();
    float modScroll;
    
    if(centerX != 0.0){
        modScroll = -offset.x > centerX ? fmod(-offset.x, centerX) : fmod(centerX, -offset.x);
    }else{
        modScroll = -offset.x;
    }
	log("modScroll=%.2f",modScroll);
    
    if(modScroll < 5.0){
        return;
    }
	
//	int opacity = 150 * modScroll / 150;
 //   mStartButton->setOpacity(100);
//    mUnlockInfoPanel->setVisible(false);
//    mLockedInfoPanel->setVisible(false);
//    mBestDistanceNode->setVisible(false);
    setPlayButtonOpacity(100);
	
    if(mInfoLayer){
        mInfoLayer->setVisible(false);
    }
}

void MainSceneLayer::onPlanetSelected(int planetID, bool isRepeat, int currentPage, int maxPage) // isRepeat=true if select again
{
//	mStartButton->setOpacity(255);
//	mStartButton->setVisible(true);
    
    setPlayButtonOpacity(255);
    
    if(mInfoLayer){
        mInfoLayer->setup(planetID);
    }
    
    
    loadLeaderboard(mCurrentLeaderBoardType);
	

	if(isRepeat == false) {	// a new planet is selected
        mSelectedPlanet = planetID;
		bool isUnLock = PlanetManager::instance()->isPlanetUnLocked(planetID);
		if(isUnLock) {
			setPlayButtonEnable(true);

			// Update the choice in PlanetManager
			PlanetManager::instance()->selectPlanet(planetID);
			StageManager::instance()->setCurrentStage(planetID);

			// Update the Level Manager
			MapLevelManager::instance()->setMapCycleForPlanet(planetID);
		} else {
			setPlayButtonEnable(false);
		}
	}
    updateChangePlanetButton(currentPage,maxPage);
    updateEventNodeVisibility();
	
	LOG_GA(Analytics::change_planet);
}

void MainSceneLayer::updateChangePlanetButton(int currentPage, int maxPage)
{
    mRightButton->setVisible(true);
    mLeftButton->setVisible(true);

    if(currentPage <= 0){
        mLeftButton->setVisible(false);
    }else if(currentPage >= maxPage){
        mRightButton->setVisible(false);
    }
}

void MainSceneLayer::updatePlanetState()
{
	int planetID = mStageSelectionView->getSelectedPlanetID();
	log("updatePlanetState: planetID=%d", planetID);

	bool isUnLock = PlanetManager::instance()->isPlanetUnLocked(planetID);

	mStageSelectionView->updatePlanetData();

	setPlayButtonEnable(isUnLock);
}



#pragma mark - Rate Notice


void MainSceneLayer::checkNewVersion()
{
	UpdateChecker::instance()->setGUIFile("UpdateNoticeLayer.csb");
	UpdateChecker::instance()->startChecking();
}



void MainSceneLayer::showUnlockPlanetCoachmark()
{
	GameManager::instance()->getUserData()->setCoachmarkPlayed("visitPlanet", true);
	mCoachmarkShown = true;
	
	
	// Start showing
	TutorialLayer *tutorialLayer = TutorialLayer::create();
	
	tutorialLayer->setColor(Color3B::BLACK);
	tutorialLayer->setOpacity(200);
	
	tutorialLayer->setContentSize(getContentSize());
	
	tutorialLayer->setLocalZOrder(10000);	// Very high
	tutorialLayer->setCloseCallback([&](Ref *sender){
		mTutorialLayer = nullptr;
		switchPlanetByButton(true);
	});
	
//	tutorialLayer->setCoachmarkTutorial("visitPlanet",
//										"tutorial_unlock_planet.csb",
//										getPlanetButtonPos());
	
	
	//
	tutorialLayer->reset();
	addChild(tutorialLayer);
	mTutorialLayer = tutorialLayer;
	
	mTutorialLayer->update(0);
}


void MainSceneLayer::showSelectCharCoachmark()
{
	
	bool isPlayed = GameManager::instance()->getUserData()->isCoachmarkPlayed("selectChar");
    if(isPlayed){
        return;
    }
//	mCoachmarkShown = true;
	
	
	// Start showing
	TutorialLayer *tutorialLayer = TutorialLayer::create();
//	tutorialLayer->setColor(Color3B::BLACK);
//	tutorialLayer->setOpacity(200);

	tutorialLayer->setContentSize(getContentSize());
	
	tutorialLayer->setLocalZOrder(10000);	// Very high
	tutorialLayer->setCloseCallback([&](Ref *sender){
		mTutorialLayer = nullptr;
		gotoDogSelection();
	});
	
    Vec2 dogBtnPos = mDogButton->convertToWorldSpaceAR(Vec2::ZERO);
    Vec2 btnSize = mDogButton->getContentSize() * mDogButton->getScale();
    Vec2 csbPos = Vec2(dogBtnPos.x + 20, dogBtnPos.y + 8);
	tutorialLayer->setCoachmarkTutorial("selectChar",
										"tutorial/tutorial_point.csb",
										 csbPos,dogBtnPos,btnSize,-120);
	
	
	//
	tutorialLayer->reset();
	addChild(tutorialLayer);
	mTutorialLayer = tutorialLayer;
	
	mTutorialLayer->update(0);
}

Vec2 MainSceneLayer::getDogButtonPos()
{
	return mDogButton->getPosition();
}

Vec2 MainSceneLayer::getPlanetButtonPos()
{
	
	
	Vec2 nodePos = mRightButton->getPosition();
	
	return nodePos;
	//return convertToWorldSpace(mRightButton->getPosition());
}

void MainSceneLayer::handleCoachmarkAction()
{
	CoachmarkAction action = determineCoachmarkAction();
	
	switch(action) {
		case CoachmarkActionSelectChar: {
			showSelectCharCoachmark();
			break;
		}
			
		case CoachmarkActionVisitPlanet: {
			showUnlockPlanetCoachmark();
			break;
		}
			
		default: {
			break;	// do nothing
		}
	}
}

MainSceneLayer::CoachmarkAction MainSceneLayer::determineCoachmarkAction()
{
	UserGameData *userData = GameManager::instance()->getUserData();
	
	if(mCoachmarkShown) {
		return CoachmarkActionNone;
	}
	
	if(userData->getPlayCount() <= 1) {	// First time play
		return CoachmarkActionNone;
	}

	
	if(userData->isCoachmarkPlayed("selectChar") == false) {
		return CoachmarkActionSelectChar;
	}

	if(userData->isCoachmarkPlayed("visitPlanet") == false) {
		return CoachmarkActionVisitPlanet;
	}
	

	
	return CoachmarkActionNone;
}



void MainSceneLayer::switchPlanetByButton(bool isRight)
{
	GameSound::playSound(GameSound::Sound::Button1);
	if(isRight) {
		mStageSelectionView->scrollToNextPage();
	} else {
		mStageSelectionView->scrollToPreviousPage();
	}
    
    setPlayButtonOpacity(100);
//	mStartButton->setOpacity(100);
//	mUnlockInfoPanel->setVisible(false);
//	mLockedInfoPanel->setVisible(false);
    if(mInfoLayer){
        mInfoLayer->setVisible(false);
    }
//    mBestDistanceNode->setVisible(false);
//    updatePlanetInfoUI(false, 0);
}


void MainSceneLayer::popStageNotClearText()
{
    Text* content = mStageNotClearPanel->getChildByName<Text*>("content");
    int stageID = getSelectedPlanet();
    content->setString(StringUtils::format("clear stage %d first",stageID-1));
    mStageNotClearPanel->stopAllActions();
    mStageNotClearPanel->setVisible(true);
    mStageNotClearPanel->setOpacity(255);
    mStageNotClearPanel->setScale(0.1);
    
    ScaleTo* scaleToLarge = ScaleTo::create(0.2, 1.1);
    ScaleTo* scaleToNormal = ScaleTo::create(0.1, 1.0);
    FadeOut* fadeOut = FadeOut::create(0.2);
    DelayTime* delayTime = DelayTime::create(2);
    CallFunc* callFunc = CallFunc::create([=](){
        mStageNotClearPanel->setVisible(false);
    });
    Sequence* sequence = Sequence::create(scaleToLarge,scaleToNormal,delayTime,fadeOut,callFunc, NULL);
    mStageNotClearPanel->runAction(sequence);
}

#pragma mark - Button handler
void MainSceneLayer::onRewardMailClicked(Button *button)
{
	GameSound::playSound(GameSound::Sound::Button1);
	MailboxLayer* layer = MailboxLayer::create();
	layer->setCloseCallback([&](){
		mTabBar->setVisible(true);
        updateGUIData();
	});
	
	mTabBar->setVisible(false);
	removeOldMenuItem();
	addNewMenuItem(layer);
	

}

void MainSceneLayer::onRankingClicked(Button *button)
{
	GameSound::playSound(GameSound::Sound::Button1);
	
//	if(SHOW_COMING_SOON){
//		showComingSoonPopup();
//		return;
//	}
	
	
	//
//	LeaderboardLayer* layer = LeaderboardLayer::create();
//	layer->setCloseCallback([&](){
//		mTabBar->setVisible(true);
//	});
//	
//	removeOldMenuItem();
//	addNewMenuItem(layer);
//	mTabBar->setVisible(false);
    
    Scene* scene = LeaderboardLayer::createScene();
    Director::getInstance()->pushScene(scene);
	
	
	LOG_GA(Analytics::click_leaderboard);
}

void MainSceneLayer::onPlayClicked(Button *button)
{
	
	//Button *button = (Button *)sender;
	if(button->getOpacity() < 255) {
		return;
		
	}
	GameSound::playSound(GameSound::Sound::Button1);
	
	
	///
	BoosterLayer* layer = BoosterLayer::create();
	layer->setStartCallback([&](){
		startGame();
	});
    layer->setOnEnterCallback([=](){
        layer->setupDistrictUI(mSelectedPlanet);
    });
    
    addChild(layer);
	
	//
	
//	removeOldMenuItem();
//	addNewMenuItem(layer);
}

void MainSceneLayer::onGachaClicked()
{
	
//	if(SHOW_COMING_SOON){
//		showComingSoonPopup();
//		return;
//	}
	
	GameSound::playSound(GameSound::Sound::Button1);
    auto scene = Scene::create();
    LuckyDrawDialog *layer = LuckyDrawDialog::create();
    layer->setOnExitCallback([](){
        Director::getInstance()->popScene();
    });
    scene->addChild(layer);
    Director::getInstance()->pushScene(scene);
//	LuckyDrawDialog* dialog = LuckyDrawDialog::create();
//	dialog->setOnExitCallback([&](){
//		mTabBar->setVisible(true);
//	});
//	removeOldMenuItem();			// ???
//	addNewMenuItem(dialog);
//	mTabBar->setVisible(false);

}

void MainSceneLayer::onCharacterBoxClicked()
{
	
}


void MainSceneLayer::onUnlockPlanetClicked(Button *button)
{
	if(button->getOpacity() < 255) {
		return;
	}
	
	int planetID = getSelectedPlanet();
	
	PlanetManager::UnlockResult result = PlanetManager::instance()->unlockNewPlanet(planetID);
	
	if(result == PlanetManager::UnlockFail) {	// Weird case
		return;
	}
    
    if(result == PlanetManager::UnlockFailStageNotClear){
        popStageNotClearText();
        return;
    }
	
	if(result == PlanetManager::UnlockFailNoMoney) {
		NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
		dialog->setMode(NotEnoughMoneyDialog::StarMode);
		addChild(dialog);
		return;
	}
	
	
    GameSound::playSound(GameSound::UnlockStage);
	PlanetItemView *view = mStageSelectionView->getPlanetItemViewAtPage(mStageSelectionView->getCurrentPage());
	view->unlockPlanet([&](){
		//button->setOpacity(255);

		// Unlock Animation Done
		updatePlanetState();
	});
	
	updateTotalCoin();
	button->setOpacity(100);
	
	// ken: old code
	
//	
//	
//	// Refactor: getSelectedPlanet
//	PlanetItemView *view = mStageSelectionView->getPlanetItemViewAtPage(
//									mStageSelectionView->getCurrentPage());
//	int planetID = view->getPlanetID();
//	
//	// Refactor: unlockPlanet
//	PlanetData* data = PlanetManager::instance()->getPlanetData(planetID);
//	int userCoin = GameManager::instance()->getUserData()->getTotalCoin();
//	int price = data->getPrice();
//	
//	if(price>userCoin){
//		NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
//		dialog->setMode(NotEnoughMoneyDialog::StarMode);
//		addChild(dialog);
//	}else{
//		PlanetManager::instance()->unlockPlanet(planetID);
//		PlanetManager::instance()->selectPlanet(planetID);
//		GameManager::instance()->getUserData()->addCoin(-price);
//		
//		//pop out unlock planet dialog
//		hideUI();
//		GameSound::playSound(GameSound::Unlock);
//		UnlockPlanetLayer* layer = UnlockPlanetLayer::create();
//		layer->setCloseCallback([&](int planetID){
//			GameSound::stopSound(GameSound::Unlock);
//			this->showUI();
//			this->scrollToPlanet(planetID);
//		});
//		
//		std::vector<int> newPlanetList{};
//		newPlanetList.push_back(planetID);
//		layer->setPlanetList(newPlanetList);
//		//           layer->setParentLayer(this);
//		addChild(layer);
//		
//		
//		updateTotalCoin();
//		updatePlanetState();
//	}
}

void MainSceneLayer::onOtherClicked()
{
	
}

#pragma mark - Facebook Related
void MainSceneLayer::setupFBProfile()
{
	if(AstrodogClient::instance()->isFBConnected()){
		//
		AstrodogUser* user = AstrodogClient::instance()->getUser();
		
		
		
		//
		mFbBtn->setVisible(false);
		mProfilePicFrame->setVisible(true);
		mFbName->setVisible(true);
		
		//mFbName->setString(user->getName());
		//std::string testStr = "Bérangère";
		ViewHelper::setUnicodeText(mFbName, user->getName());
		
		
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
//            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }
		mProfilePic->setWithFBId(user->getFbID());
	}else{
		mFbBtn->setVisible(true);
		mFbName->setVisible(false);
		mProfilePicFrame->setVisible(false);
	}
}


void MainSceneLayer::onFBConnect(int status)
{
	if(status != STATUS_SUCCESS){
		return;
	}
	
	setupFBProfile();
	loadLeaderboard(mCurrentLeaderBoardType);
	
}

void MainSceneLayer::setupProfilePic(Node *mainPanel)
{
	Node *topPanel = mainPanel->getChildByName("topPanel");
	
	mFbName = (Text *) topPanel->getChildByName("fbName");
	mProfilePic = FBProfilePic::create();
	mProfilePic->defineSize(Size(47, 47));
	mProfilePic->setAnchorPoint(Vec2::ZERO);
	mProfilePic->setPosition(Vec2(2,2));
	
	// mProfilePic->setPlaceHolderImage("guiImage/default_profilepic.png");

	mProfilePicFrame = (Sprite *) topPanel->getChildByName("profilePicFrame");
	mProfilePicFrame->addChild(mProfilePic);
	mProfilePic->setLocalZOrder(-1);

}




#pragma mark - Leaderboard
void MainSceneLayer::setupLeaderboard(Node *rootNode)
{
	// Create the LeaderboardDialog
	mLBDialog = LeaderboardDialog::create();
	mLBDialog->setLeaderboardUIType(LeaderboardDialog::LeaderboardUIType::Mainscene);
	
	mLBDialog->setAnchorPoint(Vec2(0.5,0));
	
	//    Size leaderboardSize = mLBDialog->getContentSize();
	//    float height = 170, width = 295;
	//    Size reSize = Size(width,height);
	//    mLBDialog->setContentSize(reSize);
	
	mLBDialog->setPosition(Vec2(160,150));
	
	mLBDialog->setFriendBtnCallback([&](){
		loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardFriend);
	});
	
	mLBDialog->setNationalBtnCallback([&](){
		loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
	});
	
	mLBDialog->setGlobalBtnCallback([&](){
		loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardGlobal);
	});
	
	mLBDialog->setFbBtnCallback([&](){
		AstrodogClient::instance()->connectFB();
	});
	
	Node *leaderboardView = mRootNode->getChildByName("leaderboardView");
	leaderboardView->addChild(mLBDialog);
	
}

void MainSceneLayer::onGetRanking(int status, AstrodogLeaderboardType type, int planet,
								  const std::map<std::string, int> &metaData)
{
	// stopLoading
	
	if(status != STATUS_SUCCESS) {
		return;
	}
	
	int currentPlanet = mStageSelectionView->getSelectedPlanetID();
	if(type != mCurrentLeaderBoardType || currentPlanet!=planet){
		return;
	}
	//
	// updateLeaderboard(type, planet);
	updateLeaderboardRecords();
}

void MainSceneLayer::updateLeaderboardRecords()
{
	AstrodogLeaderboardType type = mCurrentLeaderBoardType;
	
	int planet = mStageSelectionView->getSelectedPlanetID();
	
	AstrodogLeaderboard *board = AstrodogClient::instance()->getLeaderboard(type, planet);
	if(board == nullptr) {
		log("MainSceneLayer: Fail to get the leaderboard");
		setupLeaderboardEndTime(0);
		return;
	}
	
	setupLeaderboardEndTime(board->getEndTime());
	
	// TODO update the GUI
	mLBDialog->updateWithLeaderboard(board,planet);
	// mLBDialog->updateLBListView(board->getRecordList());
	
	//
	//log("Leaderboard: recordCount=%ld", board->recordList.size());
	for(AstrodogRecord *record : board->recordList) {
		log("  %s", record->toString().c_str());
	}
}


#pragma mark - Coming Soon
void MainSceneLayer::showComingSoonPopup()
{
	Node *mainNode = CSLoader::createNode("gui/ComingSoonLayer.csb");
	
	addChild(mainNode);

	Button *button = mainNode->getChildByName<Button *>("okayButton");
	if(button) {
		button->addClickEventListener([=](Ref *sender){
			mainNode->removeFromParent();
		});
	}
}


#pragma mark - Leaderboard count down
void MainSceneLayer::updateLeaderboardTime(float delta)
{
	if(mLeaderboardTimeStarted == false) {
		return;
	}
	
	if(mLeaderboardTimePanel == nullptr || mLeaderboardTimePanel->isVisible() == false) {
		return;
	}
	
    
	long long remainMillis = mLeaderboardEndTime - TimeHelper::getCurrentTimeStampInMillis();
    if(mLastLeaderboardRemainTime!=-1&&mLastLeaderboardRemainTime - remainMillis < 60000){
        return;
    }
    
    mLastLeaderboardRemainTime = remainMillis;
	setLeaderboardTime(remainMillis);
}

void MainSceneLayer::setupLeaderboardEndTime(long long endTime)
{
	mLeaderboardEndTime = endTime;
	mLeaderboardTimeStarted = true;
	
	if(mLeaderboardEndTime <= 0) {
		setLeaderboardTimeVisible(false);
		return;
	}
	
	setLeaderboardTimeVisible(true);
	
	updateLeaderboardTime(0);
}

void MainSceneLayer::setLeaderboardTimeVisible(bool flag)
{
	if(mLeaderboardTimePanel != nullptr) {
        if(!mLeaderboardTimePanel->isVisible() && flag == true){
            mLeaderboardTimePanel->stopAllActions();
            mLeaderboardTimePanel->setPositionY(570);
            MoveBy* moveBy = MoveBy::create(0.5, Vec2(0, -40));
            mLeaderboardTimePanel->runAction(moveBy);
        }
        
	   mLeaderboardTimePanel->setVisible(flag);
	}	
}

void MainSceneLayer::setLeaderboardTime(long long remainMillis)
{
	if(mLeaderboardTimePanel == nullptr) {
		log("MainSceneLayer.setLeaderBoardTime: panel is null");
		return;
	}
	if(mLeaderboardTimeText == nullptr) {
		log("MainSceneLayer.setLeaderBoardTime: text is null");
		return;
	}
	
	if(remainMillis <= 0){
		setLeaderboardTimeVisible(false);
		return;
	}
	
	
	mLeaderboardTimePanel->setVisible(true);
	
	std::string timeStr = TimeHelper::getRemainTimeString(remainMillis);
	
	mLeaderboardTimeText->setString(timeStr);

//	std::map<std::string, int> result = TimeHelper::parseMillis(remainMillis);
//	int days = result["days"]
//	int hours = result["hours"], minutes = result["minutes"];
//	
//	if(days>0){
//		mFirstUnit->setVisible(true);
//		mFirstValue->setVisible(true);
//		mSecondValue->setVisible(false);
//		std::string displayStr = StringUtils::format("%d",days);
//		mFirstValue->setString(displayStr);
//	}else{
//		mFirstUnit->setVisible(false);
//		mFirstValue->setVisible(false);
//		mSecondValue->setVisible(true);
//		std::string displayStr = StringUtils::format("%02d:%02d",hours,minutes);
//		mSecondValue->setString(displayStr);
//	}
}

void MainSceneLayer::setupLeaderboardTimeGUI(Node *mainPanel)
{
	mLeaderboardTimePanel = nullptr;
	mLeaderboardTimeText = nullptr;
	if(mainPanel == nullptr) {
		return;
	}
	
	mLeaderboardTimePanel = mainPanel->getChildByName("leaderboardTimePanel");
    
	mLeaderboardTimeText = mLeaderboardTimePanel->getChildByName<Text *>("leaderboardTimeText");
	
	mLeaderboardTimePanel->setVisible(false);
	
	mLeaderboardEndTime = 0;
	
}

