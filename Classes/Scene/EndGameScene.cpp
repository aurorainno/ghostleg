//
//  EndGameScene.cpp
//  GhostLeg
//
//  Created by Ken Lee on 8/5/2017.
//
//

#include "EndGameScene.h"
#include "GameOverDialog.h"
#include "GameWorld.h"
#include "GameScene.h"
#include "PlayerGameResult.h"

#include "CommonType.h"
#include "Constant.h"
#include "CommonMacro.h"
#include "AnimationHelper.h"
#include "ViewHelper.h"
#include "StringHelper.h"
#include "GameSound.h"
#include "GameRes.h"
#include "AdManager.h"
#include "GameManager.h"
#include "Analytics.h"
#include "TimeMeter.h"

#include "cocostudio/CocoStudio.h"




#include "StageParallaxLayer.h"
#include "UserGameData.h"

#include "DeviceHelper.h"
#include "VisibleRect.h"

#include "GameOverDialog.h"
#include "StageManager.h"
#include "PlayerManager.h"
#include "PlayerGameResult.h"
#include "PlanetManager.h"
#include "BoosterLayer.h"

Scene* EndGameSceneLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	EndGameSceneLayer *layer = EndGameSceneLayer::create();
	
	// add layer as a child to scene
	scene->addChild(layer);
	
	// return the scene
	return scene;
}

EndGameSceneLayer::EndGameSceneLayer()
: mDialog(nullptr)
{
	
}


// on "init" you need to initialize your instance
bool EndGameSceneLayer::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}
	
	// debugPlayerGameResult();
	
	
	// Logging
	mLastBestDistance = GameWorld::instance()->getLastBestDistance();
	
	handlePuzzleCollection();
	
	logPlayerGameResult();
	
	showGameOverDialog();
	
	return true;
}

void EndGameSceneLayer::logPlayerGameResult()
{
	PlayerGameResult *result = GameWorld::instance()->getGameResult();
	if(result == nullptr) {
		return;
	}
	
	int collectCoin = result->getStarCollected();
	Analytics::instance()->logCollect(Analytics::from_game, collectCoin, MoneyTypeStar);
	
	int puzzleCount = (int)mCollectPuzzleList.size();
	Analytics::instance()->logCollect(Analytics::from_game, puzzleCount, MoneyTypePuzzle);
	
	BoosterItemType item = result->getUsedBooster();
	if(item != BoosterItemNone) {
		Analytics::instance()->logUseBooster(item, result->getUsedBoosterCount());
	}
}

void EndGameSceneLayer::debugPlayerGameResult()
{
	PlayerGameResult *result = GameWorld::instance()->getGameResult();
	log("EndGame: playerResult=%s", result->toString().c_str());
}

#pragma mark - Core
void EndGameSceneLayer::onEnter()
{
	Layer::onEnter();
	
	GameSound::stopAllEffect();
	
	
	TimeMeter::instance()->showSummary();
}

void EndGameSceneLayer::onExit()
{
	Layer::onExit();
}

void EndGameSceneLayer::update(float delta)
{
	
}

#pragma mark - GUI Construction
void EndGameSceneLayer::setupGUI()
{
	
}

#pragma mark - Navigation
void EndGameSceneLayer::gotoGameScene()
{
	Scene *gameScene = GameSceneLayer::createScene();
	Director::getInstance()->replaceScene(gameScene);
}
void EndGameSceneLayer::gotoMainScene()
{
	Director::getInstance()->popScene();
}



#pragma mark - GameOverDialog

void EndGameSceneLayer::showGameOverDialog()
{
	
	
	
	GameOverDialog *dialog = GameOverDialog::create();
	dialog->setOKListener(CC_CALLBACK_1(EndGameSceneLayer::onGameOverPlayAgain, this));
	dialog->setCancelListener(CC_CALLBACK_1(EndGameSceneLayer::onGameOverBackHome, this));
	dialog->setShareListener(CC_CALLBACK_1(EndGameSceneLayer::onGameOverShare, this));
    dialog->setPuzzleList(mCollectPuzzleList);
//
//	

	// dialog->setLastBestDistance(mLastBestDistance);
	
	// Add to the scene
	if(mDialog != nullptr) {
		mDialog->removeFromParent();
	}
	addChild(dialog);
	mDialog = dialog;
}





void EndGameSceneLayer::onGameOverPlayAgain(Ref *sender)
{
	
	LOG_EVENT(Analytics::Category::end_game, Analytics::Action::play_again,"",0);
	
	
	GameSound::playSound(GameSound::Button);
	
	
	//
	if(AdManager::instance()->canShowReplayAd()
	   && AdManager::instance()->showAdBeforeReplay()) {
		log("Try to show AD before playAgain");
		// Show Video Ad
		AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
			log("Interstitual AD called back");
			startGame();
		});
		
		AdManager::instance()->setVideoRewardedCallback(nullptr);
		bool canShow = AdManager::instance()->showInterstitualAd();
		
		if(canShow == false) { // Start game if fail to play any ad
			log("Fail to show interstitual AD");
			startGame();
		}
	} else {
		// No need wait for Video Ad
		startGame();
	}
}

void EndGameSceneLayer::onGameOverShare(Ref *sender)
{
	LOG_EVENT(Analytics::Category::end_game, Analytics::Action::share,"",0);
	
	GameSound::playSound(GameSound::Button);
	
    PlayerGameResult* result = GameWorld::instance()->getGameResult();
    int totalScore =  result->getTotalScore();
    int stageID = PlanetManager::instance()->getSelectedPlanet();
    
	GameManager::instance()->share(totalScore,stageID);
}

void EndGameSceneLayer::onGameOverBackHome(Ref *sender)
{
	
	LOG_EVENT(Analytics::Category::end_game, Analytics::Action::back_main,"",0);
	
	
	GameSound::playSound(GameSound::Button);
	
	log("OK button is clicked");
	
	gotoMainScene();
}


#pragma mark - Start New Game
void EndGameSceneLayer::startGame()
{
    BoosterLayer* layer = BoosterLayer::create();
    layer->setStartCallback([&](){
        gotoGameScene();;
    });
    
    layer->setOnEnterCallback([=](){
        layer->setupDistrictUI(PlanetManager::instance()->getSelectedPlanet());
    });
    
    addChild(layer);
//	gotoGameScene();
}


#pragma mark - Internal Logic
void EndGameSceneLayer::handlePuzzleCollection()
{
	// Update the count
	mPuzzleCount = GameWorld::instance()->getPuzzleCount();
	
	// Roll the
	mCollectPuzzleList = StageManager::instance()->rollRewardPuzzle(mPuzzleCount);
	
	// log("PuzzleCollected: %s", VECTOR_TO_STR(mCollectPuzzleList).c_str());
	
	// Reward the puzzle
	PlayerManager::instance()->addPlayerFragments(mCollectPuzzleList);
	

}
