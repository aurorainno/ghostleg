//
//  TitleScene.cpp
//  GhostLeg
//
//	Scene Navigation          -P-> GameScene (Tutorial) -> Pop
//		Title  -P-> MainScene -P-> GameScene -R-> EndGame -> Pop
//			   -P-> GameScene ->
//
//  Created by Ken Lee on 19/5/2017.
//
//

#include "TitleScene.h"
#include "AnimeNode.h"

#include "ViewHelper.h"
#include "CommonMacro.h"
#include "VisibleRect.h"
#include "MainScene.h"
#include "GameScene.h"
#include "GameSound.h"
#include "LevelData.h"
#include "StageManager.h"
#include "GameManager.h"
#include "SimpleTDD.h"
#include "Analytics.h"
#include "ItemManager.h"

Scene* TitleSceneLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	TitleSceneLayer *layer = TitleSceneLayer::create();
	
	// add layer as a child to scene
	scene->addChild(layer);
	
	// return the scene
	return scene;
}

TitleSceneLayer::TitleSceneLayer()
: Layer()
, mMainNode(nullptr)
, mTimeline(nullptr)
{
	
}


bool TitleSceneLayer::init()
{
	
	if(Layer::init() == false) {
		return false;
	}
	
	std::string csb = "gui/TitleScene.csb";
	setupGUI(csb);
	
	//setupAnimation(csb);	// no need
	
	
	// Take a lot of time
	MapLevelManager::instance()->loadConfig();
	// MapLevelDataCache::instance()->loadAllMap();
	StageManager::instance()->setup();

	
	//
//	bool flag = GameManager::instance()->shouldShowTutorial();
//	log("DEBUG: show tutorial=%d", flag);
//
	
	return true;
}

void TitleSceneLayer::setupGUI(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	mMainNode = rootNode;
	setContentSize(rootNode->getContentSize());
	
	Layout *touchPanel = mMainNode->getChildByName<Layout *>("touchPanel");
	
	//typedef std::function<void(Ref*,Widget::TouchEventType)> ccWidgetTouchCallback;
	touchPanel->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type){
		log("DEBUG: touchPanel is touched");
		
		if(Widget::TouchEventType::BEGAN == type) {
			onPlayerTouched();
		}
	});
	
	AnimeNode *animeNode = AnimeNode::create();
	
	animeNode->setup("splashscreen/splashscreen.csb");
	//animeNode->setStartAnime("active", false, false);
	animeNode->setPosition(VisibleRect::center());
	animeNode->setScale(0.5f);
	animeNode->setTimeSpeed(2.0f);
	addChild(animeNode);
	mAnimeNode = animeNode;
	
	mAnimeNode->setPlayEndCallback([&](Ref *sender, std::string animeName){
		if(animeName == "active") {
			mAnimeNode->setTimeSpeed(1.0f);
			mAnimeNode->playAnimation("tap", true, false);
		}
	});
	
	

	
	SimpleTDD::setup(this, Vec2(280, 180), "test");
}


void TitleSceneLayer::runAnimation(const std::string &name)
{
	if(mTimeline) {
		mTimeline->play(name, true);
	}
}

void TitleSceneLayer::setupAnimation(const std::string &csb)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csb);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
	
	mTimeline->setFrameEventCallFunc([&](Frame *frame) {
		EventFrame *event = dynamic_cast<EventFrame*>(frame);
		if(!event) {
			return;
		}
		
	});
}


void TitleSceneLayer::onEnter()
{
	Layer::onEnter();
	// runAnimation("active");
	
	mAnimeNode->playAnimation("active", false, false);
    
    GameSound::playMusic(GameSound::Title);
    
	
	LOG_SCREEN(Analytics::scn_title_screen);
	
	// Preload sound for game
	preloadData();
}

void TitleSceneLayer::preloadData()
{
	GameSound::preloadGameSound();
	ItemManager::instance()->preloadCsb();
}

void TitleSceneLayer::onPlayerTouched()
{
    GameSound::playSound(GameSound::ChangeStage);
	bool flag = GameManager::instance()->shouldShowTutorial();
	if(flag) {
		gotoTutorialScene();
	} else {
		gotoMainScene();
	}
}

void TitleSceneLayer::gotoMainScene()
{
	auto scene = MainSceneLayer::createScene();
	
	Director::getInstance()->pushScene(scene);
}

void TitleSceneLayer::gotoTutorialScene()
{
	auto scene = GameSceneLayer::createTutorialScene(true);
	
	Director::getInstance()->pushScene(scene);
}
