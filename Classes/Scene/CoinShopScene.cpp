//
//  CoinShopScene.cpp
//  GhostLeg
//
//  Created by Calvin on 25/5/2016.
//
//

#include "CoinShopScene.h"
#include "StageParallaxLayer.h"
#include "cocostudio/CocoStudio.h"
#include "CommonMacro.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "IAPManager.h"
#include "GameSound.h"
#include "StringHelper.h"
#include "Analytics.h"
#include "Constant.h"
#include "MainScene.h"
#include "NotEnoughMoneyDialog.h"
#include "AdManager.h"
#include "AnimationHelper.h"
#include "StringHelper.h"
#include "ViewHelper.h"

const int freeStarAmount = 500;

namespace {
	std::string getIconSpriteByItemType(CoinShopSceneLayer::ItemType type)
	{
		switch (type) {
			case CoinShopSceneLayer::ShopItemTypeStar	: return "guiImage/star_normal.png";
			case CoinShopSceneLayer::ShopItemTypeCandy	: return "guiImage/icon_candystick.png";
            case CoinShopSceneLayer::ShopItemTypeDiamond	: return "guiImage/diamond.png";
			default: return "";
		}
		
	}
}

#pragma mark - Main Code


CoinShopSceneLayer::CoinShopSceneLayer():
mIsDialogMode(true),
mOnPurchaseCallback(0)
{}

Scene* CoinShopSceneLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = CoinShopSceneLayer::create();
    layer->setIsDialogMode(false);
    layer->setAsScene();
    
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


void CoinShopSceneLayer::setAsScene()
{
    mTopPanel->setVisible(true);
	
    StageParallaxLayer *parallaxLayer = StageParallaxLayer::create();
    parallaxLayer->setBGSpeed(0);
    parallaxLayer->setStage(1);
    parallaxLayer->scheduleUpdate();
    
    // add layer as a child to scene
    addChild(parallaxLayer,-1);
}

bool CoinShopSceneLayer::init(){
    
    if ( !Layer::init() )
    {
        return false;
    }
	
	mIsDialogMode = true;
    	
	// Main UI
	mRootNode = CSLoader::createNode("CoinShopScene.csb");
    addChild(mRootNode);
	setupUI();
	
	
	// Make UI animation
    scheduleUpdate();
    mErrIsShown = false;
    mTimeElasped = 0;
	
    return true;
}

void CoinShopSceneLayer::onEnter(){
    Layer::onEnter();
    //GameSound::playMusic(GameSound::Title);
    //	updateCandy();
    updateCoins();
    updateDiamond();
    setupFbProfile();


    // logging
	LOG_SCREEN(Analytics::scn_coin_shop);
}


void CoinShopSceneLayer::onExit(){
    Layer::onExit();

}

void CoinShopSceneLayer::setupUI(){
    FIX_UI_LAYOUT(mRootNode);
    
    // Coin Text
//    mStars = (Text*) mRootNode->getChildByName("totalCoinText");
//    updateCoins();
   	mTopPanel = mRootNode->getChildByName("topPanel");
    
    mStarText = (Text*) mTopPanel->getChildByName("totalCoinText");
    mDiamondText = mTopPanel->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mFbConnectBtn = (Button*) mTopPanel->getChildByName("fbBtn");
    mFbName = (Text *) mTopPanel->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) mTopPanel->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
    mFbConnectBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });
    
    Button *backBtn = (Button*)mRootNode->getChildByName("backBtn");
    mProgressLayer = mRootNode->getChildByName("ProgressLayer");
    mErrmsg = mRootNode->getChildByName<Text*>("err_msg");
    mAlertLayer = mRootNode->getChildByName("AlertLayer");
    Button *confirmBtn = (Button*)mAlertLayer->getChildByName("confirmBtn");
    
//    mIconCandyStick = (Sprite *) mRootNode->getChildByName("iconCandyStick");
//    mCandy = (Text *) mRootNode->getChildByName("totalCandyText");
//    
//    mDiamond = (Text *) mRootNode->getChildByName("totalDiamondText");
    
    //	Sprite *mIconCandyStick;
    //	Text *mCandy;
    
    
    mAlertLayer->setVisible(false);
    mErrmsg->setVisible(false);
    
    //set lisenter for back button
    backBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:{
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            }
            case ui::Widget::TouchEventType::ENDED: {
                closeView();
//                if(mIsDialogMode){
//                    this->removeFromParent();
//                }else{
//                    Director::getInstance()->popScene();
//                }
//                break;
            }
        }
    });
    
    //set lisenter for alert button
    confirmBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:{
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            }
            case ui::Widget::TouchEventType::ENDED: {
                mAlertLayer->setVisible(false);
                if(mOnCloseAlertCallback){
                    mOnCloseAlertCallback();
                }
                mOnCloseAlertCallback = NULL;
                break;
            }
        }
    });
    
    
//    Button *restoreBtn = (Button*)mRootNode->getChildByName("restoreBtn");
//    restoreBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
//        switch(type){
//            case ui::Widget::TouchEventType::BEGAN:{
//                GameSound::playSound(GameSound::Sound::Button1);
//                break;
//            }
//            case ui::Widget::TouchEventType::ENDED: {
//                GameManager::instance()->getIAPManager()->IAPManager::restorePurchasedItems();
//                showProgress("Restoring your items....");
//                break;
//            }
//        }
//    });
    
    //setup callback for IAR event
    setupCallback();
    
    
    if(!GameManager::instance()->getIAPManager()->isReady()) {
        GameManager::instance()->getIAPManager()->updateProductInfo();
    } else {
        hideProgress();
        addProductItemTable();
    }
    
    
 //   updateForEventUI();
}

void CoinShopSceneLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void CoinShopSceneLayer::updateCoins()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}


CoinShopItemView *CoinShopSceneLayer::getCoinShopItemByName(std::string name){
    for(int i=0;i<mCoinShopItems.size();i++){
		if(name == mCoinShopItems.at(i)->getItemKey()) {
			return mCoinShopItems.at(i);
		}
    }
	
    return nullptr;
}

void CoinShopSceneLayer::addProductItemTable()
{
//    Button *restoreBtn = (Button*)mRootNode->getChildByName("restoreBtn");
//    restoreBtn->setVisible(true);
    
    //Node *subPanel = mRootNode->getChildByName("subPanel");
    ui::ScrollView *subPanel = (ui::ScrollView *) mRootNode->getChildByName("subPanel");

    
    
    Vector<GameProduct *> productList = GameManager::instance()->getIAPManager()->getProductArray();

    float spacingH = 8;
    // float marginY = 5;
	float totalHeight = 0;
	
	CoinShopItemView::CoinShopItemViewCallback callback = [&](CoinShopItemView* itemview,GameProduct* product)
    {
		
//		showProgress("Purchase\nin progress...");
		IAPStatusCode result = GameManager::instance()->getIAPManager()->buyProduct(product);
        switch (result) {
            case IAP_WAIT_CALLBACK:
                showProgress("Purchase\nin progress...");
                break;
            
            case IAP_SUCCESS:
                this->handleGameProductSuccess(product);
                break;
                
            case IAP_NOT_ENOUGH_MONEY:
                handleNotEnoughMoney();
                break;
                
            default:
                break;
        }
		Analytics::instance()->logScreen(Analytics::Screen::appstore);
		
	};
	
    
    
//    Vec2 pos = Vec2(0, subPanel->getInnerContainerSize().height);// ItemView Height=60 width=275


//    if( GameManager::instance()->getUserData()->hasLikedFacebook() == false)
//    {
//        subPanel->setInnerContainerSize(Size(subPanel->getContentSize().width, 480)); //2 -> 560
//        pos = Vec2(0, subPanel->getInnerContainerSize().height);
//        
//                
//        CoinShopItemView::CoinShopItemViewCallback facebooLikeCallback = [&](CoinShopItemView* itemview,std::string productKey)
//        {
//            if( GameManager::instance()->getUserData()->hasLikedFacebook() == false)
//            {
//                CCApplication::getInstance()->openURL(kFacebookPageLink);
//                GameManager::instance()->getUserData()->setLikedFacebook(true);
//                itemview->updateFacebookLikeBtn(true);
//                GameManager::instance()->getUserData()->addCoin(100);
//                updateCoins();
//                showAlert(" Thank you!", "100 star is added");
//            }
//
//        };
//        
//        
//        GameProduct *likeFacebook =  GameProduct::create();
//        likeFacebook->setName("coinByFacebookLike");//product ID'
//        likeFacebook->setDisplayName("like us facebook la");
//        likeFacebook->setIsBlueAction(true);
//        likeFacebook->setProductID("coinByFacebookLike");
//        CoinShopItemView *itemView = CoinShopItemView::create();
//        pos.y -= itemView->getContentSize().height;
//        itemView->setPosition(pos);
//        itemView->setProduct(likeFacebook);
//        itemView->setCallback(facebooLikeCallback);
//        subPanel->addChild(itemView);
//        mCoinShopItems.pushBack(itemView);	// Save the view
//        pos.y -= spacingH;
//    
//    }
    
    
    


	for(GameProduct *product : productList)
    {
		if(GameManager::instance()->getIAPManager()->isProductAvailable(product) == false) {
			continue;
		}		// Something wrong!!!
		
        std::string nodeName = product->getGUIKey() + "_node";
        Node* itemNode = mRootNode->getChildByName(nodeName);
        if(!itemNode){
            continue;
        }
        
        CoinShopItemView *itemView = CoinShopItemView::create();
		itemView->setProduct(product);
		itemView->setCallback(callback);
        mCoinShopItems.pushBack(itemView);
        itemNode->addChild(itemView);
 
        /*
        ParticleSystemQuad* emitter;
        if(product->getDisplayName().find("diamond")!=std::string::npos){
            emitter = ParticleSystemQuad::create("particle_shop_diamond.plist");
        }else if(product->getDisplayName().find("star")!=std::string::npos){
            emitter = ParticleSystemQuad::create("particle_shop_star.plist");
        }
        emitter->setScale(0.25);
        Vec2 pos = Vec2(itemView->getContentSize().width/2,itemView->getContentSize().height/2 + 40);
        emitter->setPosition(pos);
        
        itemView->addChild(emitter);
        */
        
//		pos.y -= itemView->getContentSize().height;	// move the yPos to the bottom of the view
//		
//		itemView->setPosition(pos);
//		itemView->setCallback(callback);
//        subPanel->addChild(itemView);
//        mCoinShopItems.pushBack(itemView);	// Save the view
//		
//		pos.y -= spacingH;
//		
//		totalHeight += (spacingH + itemView->getContentSize().height);
    }
	
	
	//add watch ad itemView
    Node* itemNode = mRootNode->getChildByName("freeStar_node");
    if(!itemNode){
        return;
    }
    
    CoinShopItemView *itemView = CoinShopItemView::create();
    itemView->setToWatchAdItem(freeStarAmount);
    itemView->setPlayAdCallback([=]{
        handlePlayAdAddStar();
    });
    mCoinShopItems.pushBack(itemView);
    itemNode->addChild(itemView);

}

void CoinShopSceneLayer::handlePlayAdAddStar()
{
	
	bool isOkay = AdManager::instance()->playVideoAd(AdManager::AdFreeCoin,
			 [&](bool isFinished){
				 addFreeCoin(freeStarAmount);
				 
				 
				 if(mOnPurchaseCallback){
					 mOnPurchaseCallback();
				 }
				 
				 std::string valueStr = mStarText->getString();
				 int oriValue = aurora::StringHelper::parseInt(valueStr);
				 int finalValue = oriValue + freeStarAmount;
				 
				 mOnCloseAlertCallback = [=]{
					 mStarText->stopAllActions();
					 mStarText->setScale(0.5);
					 AnimationHelper::popTextEffect(this, mStarText, oriValue, finalValue);
				 };
				 
				 showAlert("Reward Success", INT_TO_STR(freeStarAmount), ShopItemTypeStar);
				 
			 }
		);
	
	if(isOkay == false) {
		showErrorMessage("Fail to earn free coin. Try later");
	}
	
//    AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
//        GameManager::instance()->getUserData()->addCoin(freeStarAmount);
//        if(mOnPurchaseCallback){
//            mOnPurchaseCallback();
//        }
//        
//        std::string valueStr = mStarText->getString();
//        int oriValue = aurora::StringHelper::parseInt(valueStr);
//        int finalValue = oriValue + freeStarAmount;
//        
//        mOnCloseAlertCallback = [=]{
//            mStarText->stopAllActions();
//            mStarText->setScale(0.5);
//            AnimationHelper::popTextEffect(this, mStarText, oriValue, finalValue);
//        };
//        
//        showAlert("Purchase Success", INT_TO_STR(freeStarAmount), ShopItemTypeStar);
//
//    });
//    
//    AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
//        
//    });
//    
//    AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
    
    //    Analytics::instance()->logGameContinue("continue_with_ad", 2200);	// 15 stars
}


void CoinShopSceneLayer::handleProductListLoaded(IAPStatusCode status, const std::string &msg)
{
	hideProgress();
	
	if(status == IAP_LOAD_PRICE_FAIL) {
		showErrorMessage(msg);
		return;
	}
	
	if(status != IAP_SUCCESS) {		// something wrong
		log("unknown status: %d", status);
		return;
	}
	
	
	if(!mErrIsShown){		// if already shown the err msg, i.e. already timeout, then don't add product to the view.
		addProductItemTable();
	}
}


void CoinShopSceneLayer::setupCallback()
{
    IAPManager *instance = GameManager::instance()->getIAPManager();
    
    instance->setUpdateProductCallback([&](IAPStatusCode status, std::string msg){
		handleProductListLoaded(status, msg);
    });
    
    instance->setPurchaseCallback(
			[&](GameProduct *product, IAPStatusCode status, std::string msg) {
                hideProgress();
                
                if(status==IAP_SUCCESS){
					handleGameProductSuccess(product);

				} else if (status== IAP_PURCHASE_CANCEL) {
					// Nothing to do
                } else if (status==IAP_ERROR_PURCHASE_FAIL) {
                    // Nothing to do, because Apple SDK already prompt the error
				} else {
					showAlert("Fail to purchase",  msg);
				}
            });
    
    
    instance->setRestoreItemCallback(
			[&](IAPStatusCode status, std::string msg) {
				hideProgress();
				 if(status==IAP_SUCCESS)
				 {
					 
					 CoinShopItemView *view = getCoinShopItemByName("noad");
					 if(view)
					 {
						 Button* btn = view->getPurchaseButton();
						 btn->setTitleText("Owned");
						 btn->setEnabled(false);
						 btn->setOpacity(128);
					 }
					 
					 showAlert("Your item is restored.",  msg);
				 }
				 else if (status==IAP_NO_ITEM_TO_RESTORE)
				 {
					 showAlert("No items can be restored.",  msg);
				 }
				 else
				 {
					 showAlert("Failed to restore your items.",  msg);
				 }
				
				 
			  });

}

void CoinShopSceneLayer::updateForEventUI()
{
	bool showCandy = GameManager::instance()->isEventOn();
	
	mIconCandyStick->setVisible(showCandy);
	mCandy->setVisible(showCandy);
	
	if(showCandy) {
//		updateCandy();
	}
}


void CoinShopSceneLayer::showProgress(std::string msg){
    if(mProgressLayer){
		mProgressLayer->setPositionX(0);
        mProgressLayer->setVisible(true);
        Text* message = mProgressLayer->getChildByName<Text*>("message");
        message->setString(msg);
    }
}

void CoinShopSceneLayer::hideProgress(){
    if(mProgressLayer)
        mProgressLayer->setVisible(false);
}

void CoinShopSceneLayer::showErrorMessage(std::string msg){
    if(mErrmsg){
        mErrIsShown = true;
        mErrmsg->setString(msg);
        mErrmsg->setVisible(true);
    }
}

void CoinShopSceneLayer::showAlert(std::string title, std::string info, ItemType type)
{
    if(mAlertLayer == nullptr){
		return;
	}
	
	mAlertLayer->setPositionX(0);
	
//	// Define the title
	Text *titleText = mAlertLayer->getChildByName<Text*>("title");
	titleText->setString(title);
//	
//	// Define the content
//	Text *infoText = mAlertLayer->getChildByName<Text*>("info");
//	Node *starPanel = mAlertLayer->getChildByName("buyStarPanel");
//	
//	if(type == ShopItemTypeStar || type == ShopItemTypeCandy || type == ShopItemTypeDiamond) {
//		starPanel->setVisible(true);
//		infoText->setVisible(false);
//		starPanel->getChildByName<Text*>("amount")->setString(info);
//		
//		Sprite *iconSprite = starPanel->getChildByName<Sprite *>("iconSprite");
//		iconSprite->setTexture(getIconSpriteByItemType(type));
//		
//	} else {
//		starPanel->setVisible(false);
//		infoText->setVisible(true);
//		infoText->setString(info);
//	}

	mAlertLayer->setVisible(true);
}

void CoinShopSceneLayer::closeAlert(){
    if(mAlertLayer){
        mAlertLayer->setVisible(false);
    }
}


void CoinShopSceneLayer::update(float delta){
	mTimeElasped += delta;
	
    if(mTimeElasped > 8.0){
		// Timeout handling
        if(GameManager::instance()->getIAPManager()->isReady() == false){
            hideProgress();
            showErrorMessage("Sorry, shop\nis not\navailable now.");
        }
        mTimeElasped = 0;
        unscheduleUpdate();
    }
}

void CoinShopSceneLayer::setCloseCallback(const std::function<void()> &callback)
{
	mCloseCallback = callback;
}

void CoinShopSceneLayer::setOnPurchaseCallback(const std::function<void ()> &callback)
{
    mOnPurchaseCallback = callback;
}

void CoinShopSceneLayer::closeView()
{
	if(mIsDialogMode){
		if(mCloseCallback) {
			mCloseCallback();
		}
		
		removeFromParent();
	}else{
		Director::getInstance()->popScene();
	}
}

#pragma mark - Serve Game Product

void CoinShopSceneLayer::handleGameProductSuccess(GameProduct *product)
{
	log("handleGameProductSuccess: handling product=%s", product->toString().c_str());
	if(product == nullptr) {
		log("serverGameProduct: product is nullptr");
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(product->getAction(), ':');
	
	std::string action = tokens[0];
	std::string valueStr = tokens.size() <= 1 ? "" : tokens[1];
	int value = STR_TO_INT(valueStr);
	
    GameSound::playSound(GameSound::Purchase);
	
	if("addStar" == action) {
		handleAddStars(product, value);
	} else if("addCandy" == action) {
		handleAddCandy(product, value);
    } else if("addDiamond" == action) {
		log("handleGameProductSuccess: before addDiamond");
		
        handleAddDiamond(product, value);
		
		log("handleGameProductSuccess: addDiamond done");
    } else if("removeAd" == action) {
//		handleRemoveAd(product);
	}
}

void CoinShopSceneLayer::handleRemoveAd(GameProduct *product)
{
	log("Serve RemoveAd");
	if(product->getIsOwned() == false){
		return;
	}
	
	CoinShopItemView *view = getCoinShopItemByName(product->getName());
	Button *btn = view->getPurchaseButton();
	btn->setTitleText("Owned");
	btn->setEnabled(false);
	btn->setOpacity(128);
}

void CoinShopSceneLayer::handleAddStars(GameProduct *product, int amount)
{
	log("Serve AddStars");
	
//	GameSound::playSound(GameSound::Sound::Button4);
	
//	updateCoins();
//    handleUpdateDiamondAndStars();
    
    updateDiamond();
    if(mOnPurchaseCallback){
        mOnPurchaseCallback();
    }
    
    std::string valueStr = mStarText->getString();
    int oriValue = aurora::StringHelper::parseInt(valueStr);
    int finalValue = oriValue + amount;
    
    mOnCloseAlertCallback = [=]{
        mStarText->stopAllActions();
        mStarText->setScale(0.5);
        AnimationHelper::popTextEffect(this, mStarText, oriValue, finalValue);
    };
	
	// logging
	Analytics::instance()->logCollect(Analytics::from_diamond, amount, MoneyTypeStar);
	Analytics::instance()->logConsume(Analytics::act_buy_coin,
						product->getName(), product->getCostValue(), MoneyTypeDiamond);
	
	
	//
	showAlert("Purchase Success", INT_TO_STR(amount), ShopItemTypeStar);
	
	
	
//	
//	if(product->getIsConsumable()){
//		if(product->getDisplayName().find("star")!=std::string::npos){
//			std::string amount = product->getDisplayName().substr(5,4);
//			
//		}else{
//			showAlert("Purchase Success",  product->getDisplayName() + " is added");
//		}
//	}else{
//		showAlert("Purchase Success",  product->getDisplayName() + " is unlocked");
//		if(product->getIsOwned()){
//			Button* btn = view->getPurchaseButton();
//			btn->setTitleText("Owned");
//			btn->setEnabled(false);
//			btn->setOpacity(128);
//		}
//	}
}

void CoinShopSceneLayer::handleAddCandy(GameProduct *product, int amount)
{
	
	GameSound::playSound(GameSound::Sound::Button4);
	
//	updateCandy();
    handleUpdateDiamondAndStars();
	
	showAlert("Purchase Success", INT_TO_STR(amount), ShopItemTypeCandy);
}

void CoinShopSceneLayer::handleAddDiamond(GameProduct *product, int amount)
{
 //   GameSound::playSound(GameSound::Sound::Button4);
    
//    updateDiamond();
//     handleUpdateDiamondAndStars();
    updateCoins();
    if(mOnPurchaseCallback){
        mOnPurchaseCallback();
    }
    
    std::string valueStr = mDiamondText->getString();
    int oriValue = aurora::StringHelper::parseInt(valueStr);
    int finalValue = oriValue + amount;
    
    mOnCloseAlertCallback = [=]{
        mDiamondText->stopAllActions();
        mDiamondText->setScale(0.5);
        AnimationHelper::popTextEffect(this, mDiamondText, oriValue, finalValue);
    };
    
	
	// logging
	Analytics::instance()->logCollect(Analytics::from_iap, amount, MoneyTypeDiamond);
	
    showAlert("Purchase Success", INT_TO_STR(amount), ShopItemTypeDiamond);
}


void CoinShopSceneLayer::handleUpdateDiamondAndStars()
{
//    if(!getParent()){
//        return;
//    }
//    MainSceneLayer* layer = dynamic_cast<MainSceneLayer*>(getParent());
//    if(layer){
//        layer->updateDiamond();
//        layer->updateTotalCoin();
//    }
    updateDiamond();
    updateCoins();
   if(mOnPurchaseCallback){
        mOnPurchaseCallback();
   }
}

void CoinShopSceneLayer::handleNotEnoughMoney()
{
    NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
    dialog->setMode(NotEnoughMoneyDialog::DiamondMode);
    //dialog->hideShopButton();
	dialog->setAlreadyInShop(true);
    addChild(dialog);
}


void CoinShopSceneLayer::setupFbProfile()
{
    
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mFbConnectBtn->setVisible(false);
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        //mFbName->setString(user->getName());
		ViewHelper::setUnicodeText(mFbName, user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
//        mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void CoinShopSceneLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}


#pragma mark - Add Coin Logic
void CoinShopSceneLayer::addFreeCoin(int amount)
{
	GameManager::instance()->getUserData()->addCoin(amount);
	Analytics::instance()->logCollect(Analytics::from_free_coin, amount, MoneyTypeStar);
	
}
