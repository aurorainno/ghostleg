//
//  TitleScene.hpp
//  GhostLeg
//
//  Created by Ken Lee on 19/5/2017.
//
//

#ifndef TitleScene_hpp
#define TitleScene_hpp

#include <stdio.h>


#include <stdio.h>


#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class AnimeNode;

class TitleSceneLayer : public Layer
{
public:
	static Scene* createScene();
public:
	CREATE_FUNC(TitleSceneLayer);
	
	TitleSceneLayer();
	
	virtual bool init() override;
	
	virtual void onEnter() override;

private:
	void onPlayerTouched();
	void gotoMainScene();
	void gotoTutorialScene();
	
private:
	Node *mMainNode;

#pragma mark - Preload
	void preloadData();
	
#pragma mark - GUI & Animation
public:
	void runAnimation(const std::string &name);
		
private:
	void setupGUI(const std::string &csbName);
	void setupAnimation(const std::string &csb);
	
private:		// data
	cocostudio::timeline::ActionTimeline *mTimeline;
	AnimeNode *mAnimeNode;
};



#endif /* TitleScene_hpp */
