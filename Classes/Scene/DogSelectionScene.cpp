//
//  DogSelectionScene.cpp
//  GhostLeg
//
//  Created by Calvin on 13/10/2016.
//
//

#include "DogSelectionScene.h"
#include "StageParallaxLayer.h"
#include "cocostudio/CocoStudio.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "DogSelectItemView.h"
#include "DogManager.h"
#include "GameSound.h"
#include "PlanetManager.h"
#include "Analytics.h"
#include "GameManager.h"
#include "UserGameData.h"


const float kListItemSpacing = 5.0f;		// pixel, spacing

Scene* DogSelectionSceneLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = DogSelectionSceneLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

DogSelectionSceneLayer::~DogSelectionSceneLayer()
{
	log("DogSelectionSceneLayer is removed");
}

bool DogSelectionSceneLayer::init(){
    if ( !Layer::init() )
    {
        return false;
    }
	
	// define the first & last planet
	setupPlanetList();		// assume
	
    // Add background first
    ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	
    addChild(parallaxLayer);
    parallaxLayer->scheduleUpdate();
    
    Node *rootNode = CSLoader::createNode("DogSelectionScene.csb");
    addChild(rootNode);
    setupUI(rootNode);
	
    mIsEnteredWithSpecificPlanet = false;
	LOG_SCREEN(Analytics::Screen::dog_select);
	
    return true;
}

void DogSelectionSceneLayer::setupPlanetList()
{
	
    mPlanetIDList.clear();
	
	std::vector<int> tempPlanetIDList = PlanetManager::instance()->getDogTeamList();
    for(int planetID : tempPlanetIDList){
		mPlanetIDList.push_back(planetID);
	}
	
}

void DogSelectionSceneLayer::setupUI(Node* rootNode){
    FIX_UI_LAYOUT(rootNode);
    mRootNode = rootNode;
	
	Node *upperPanel = rootNode->getChildByName("upperPanel");
    mUpperPanel = upperPanel;
	
	// Back Button
    Button* backBtn = (Button*) upperPanel->getChildByName("backBtn");
    backBtn->addClickEventListener([&](Ref*){
		GameSound::playSound(GameSound::Sound::Button1);
        Director::getInstance()->popScene();
    });
	
    mNextBtn = (Button*) upperPanel->getChildByName("nextBtn");
    mPreviousBtn = (Button*) upperPanel->getChildByName("previousBtn");
 
    mPlanetSprite = (Sprite*) upperPanel->getChildByName("planetSprite");
	
    mDogIndicatorPanel = rootNode->getChildByName("dogIndicatorPanel");
    for(int i=1;i<=5;i++){
        std::string name = StringUtils::format("dogUnlockedIcon%02d",i);
        Sprite* sprite = mDogIndicatorPanel->getChildByName<Sprite*>(name);
        mUnlockedDogIndicatorList.pushBack(sprite);
    }
    
    mEventBanner = rootNode->getChildByName("eventBanner");
    
	// Dog ListView
	mDogListView = (ListView *) rootNode->getChildByName("dogListView");
	mDogListView->setScrollBarEnabled(false);
//	mDogListView->setDirection(ui::ScrollView::Direction::NONE);
}

void DogSelectionSceneLayer::onEnter()
{
    Layer::onEnter();
//     GameSound::playMusic(GameSound::Title);
    
    setupDogListView();
    setupPlanetBtn();
//    updateUnlockText();
    setupTopBar();
    updatePlanetIcon();
}

void DogSelectionSceneLayer::hideView()
{
    mRootNode->setVisible(false);
}

void DogSelectionSceneLayer::showView()
{
    mRootNode->setVisible(true);
}

void DogSelectionSceneLayer::setEnterAtSpecificPlanet(int planetID)
{
    mIsEnteredWithSpecificPlanet = true;
    mSelectedPlanet = planetID;
}

void DogSelectionSceneLayer::setupPlanetBtn()
{
    refreshPlanetBtnVisibleState();
    
    mNextBtn->addClickEventListener([&](Ref*){
		GameSound::playSound(GameSound::Sound::Button1);
		
		int newPlanet = getPlanetIDWithChange(1);
        PlanetData* data = PlanetManager::instance()->getPlanetData(newPlanet);
        if(data){
            onChangePlanet(data->getPlanetID());
        }
    });
    
    mPreviousBtn->addClickEventListener([&](Ref*){
		GameSound::playSound(GameSound::Sound::Button1);
		
		int newPlanet = getPlanetIDWithChange(-1);
        PlanetData* data = PlanetManager::instance()->getPlanetData(newPlanet);
        if(data){
            onChangePlanet(data->getPlanetID());
        }
    });
}

void DogSelectionSceneLayer::refreshPlanetBtnVisibleState()
{
    mPreviousBtn->setVisible(true);
    mNextBtn->setVisible(true);
//    
//    if(!PlanetManager::instance()->getPlanetData(mSelectedPlanet - 1)){
//        mPreviousBtn->setVisible(false);
//    }else if(!PlanetManager::instance()->getPlanetData(mSelectedPlanet + 1)){
//        mNextBtn->setVisible(false);
//    }
}

void DogSelectionSceneLayer::setupDogListView()
{
    mSelectedDog = DogManager::instance()->getSelectedDogID();
    
    if(!mIsEnteredWithSpecificPlanet){
        mSelectedPlanet = PlanetManager::instance()->getPlanetWithDog(mSelectedDog);
    }
    
	mDogListView->removeAllItems();
	mDogListView->setItemsMargin(kListItemSpacing);//spacing between item
   
    PlanetData *planet = PlanetManager::instance()->getPlanetData(mSelectedPlanet);
    std::vector<int> dogList = planet->getDogList();

    for(int i=0;i<5;i++){
        DogSelectItemView *dogView = DogSelectItemView::create();
        dogView->setDogSelectionScene(this);
        mDogListView->pushBackCustomItem(dogView);
    }
    
    int index = 0;
    Vector<Widget*>& widgetList = mDogListView->getItems();
    
    for(int dogID : dogList) {
        DogSelectItemView *dogView = (DogSelectItemView *) widgetList.at(index);
		
		dogView->setDog(dogID);
        
        dogView->setCallback([&](int dogID){
            onSelectDog(dogID);
        });
        
        index++;
	}
}

void DogSelectionSceneLayer::setupTopBar()
{
    float offsetY_A = 73, offsetY_B = 120;
    PlanetData *planetData = PlanetManager::instance()->getPlanetData(mSelectedPlanet);
    int numOfUnlockedDogs = PlanetManager::instance()->getUnlockedDogs(mSelectedPlanet);
    
    if(planetData->getPlanetType() == PlanetData::Event){
		bool showFlag = GameManager::instance()->isEventOn();
		int offset = showFlag ? offsetY_B : offsetY_A;
		
        mEventBanner->setVisible(showFlag);
        mDogIndicatorPanel->setVisible(false);
        mDogListView->setPositionY(getContentSize().height - offset);
		
		
		
		
        updateCandyStick();
    }else if(planetData->getPlanetType() != PlanetData::Normal ||
       numOfUnlockedDogs == 5){
        mEventBanner->setVisible(false);
        mDogIndicatorPanel->setVisible(false);
        mDogListView->setPositionY(getContentSize().height - offsetY_A);
    }else{
        mEventBanner->setVisible(false);
        mDogIndicatorPanel->setVisible(true);
        mDogListView->setPositionY(getContentSize().height - offsetY_B);
        
        updateIndicatorInfo(numOfUnlockedDogs);
    }
    
}


void DogSelectionSceneLayer::updateIndicatorInfo(int numOfUnlockedDogs)
{
    //set Text
    Text* numText = mDogIndicatorPanel->getChildByName<Text*>("numberText");
    numText->setString(StringUtils::format("%d",numOfUnlockedDogs));
    //set dog Icon
    for(int i=0;i<5;i++){
        Sprite* sprite = mUnlockedDogIndicatorList.at(i);
        if(sprite){
            if(i<numOfUnlockedDogs){
                sprite->setVisible(true);
            }else{
                sprite->setVisible(false);
            }
        }
    }
    //set planet icon
    std::string iconName;
    if(mSelectedPlanet == 2){
        iconName = "planet/planet_venus.png";
    }else if (mSelectedPlanet == 3){
        iconName = "planet/planet_mars.png";
    }else if (mSelectedPlanet == 4){
        iconName = "planet/planet_saturn.png";
    }else if(mSelectedPlanet == 5){
        iconName = "planet/planet_jupiter.png";
    }
    Sprite* planetSprite = mDogIndicatorPanel->getChildByName<Sprite*>("planetIcon");
    planetSprite->setTexture(iconName);

}

void DogSelectionSceneLayer::updateCandyStick()
{
    Text* candy = mEventBanner->getChildByName<Text*>("numOfCandy");
    int numOfCandy = GameManager::instance()->getUserData()->getTotalCandy();
    std::string numOfCandyStr = StringUtils::format("%d",numOfCandy);
    candy->setString(numOfCandyStr);
}



void DogSelectionSceneLayer::updateUnlockText()
{
    int unlockedTotal = PlanetManager::instance()->getUnlockedDogs(mSelectedPlanet);
    mUnlockText->setString(StringUtils::format("%d",unlockedTotal));
}

void DogSelectionSceneLayer::updatePlanetIcon()
{
	
	PlanetData *planetData = PlanetManager::instance()->getPlanetData(mSelectedPlanet);
	
	// Banner Image
	std::string resName = "";
	bool showUnlockMsg = false;
	
	if(planetData != nullptr) {
		resName = "guiImage/";
		if(planetData->getBannerImage() != "") {
			resName += planetData->getBannerImage();
		} else {
			resName += StringUtils::format("ui_team_planet_%03d.png", planetData->getPlanetID());
		}
		
		showUnlockMsg = planetData->getPlanetType() == PlanetData::Normal;
	}
	
	// Show/Hide the unlock msg
//	mUnlockMsgText->setVisible(showUnlockMsg);
//	mUnlockText->setVisible(showUnlockMsg);

	// Update the banner image
    mPlanetSprite->setTexture(resName);
}

void DogSelectionSceneLayer::onChangePlanet(int planetID)
{
    mSelectedPlanet = planetID;
    refreshPlanetBtnVisibleState();
//    updateUnlockText();
    
    PlanetData *planet = PlanetManager::instance()->getPlanetData(mSelectedPlanet);
    std::vector<int> dogList = planet->getDogList();
    
    Vector<Widget*>& widgetList = mDogListView->getItems();
    
    int dogID = 0, index = 0;
    
    for(Widget *widget : widgetList) {
        DogSelectItemView *dogView = (DogSelectItemView *) widget;
        
        if(index >= dogList.size()){
            dogView->setVisible(false);
        }else{
            dogID = dogList.at(index);
            dogView->setDog(dogID);
            //log("dogID:%d",dogID);
            
            if(!dogView->hasCallback()){
                dogView->setCallback([&,this](int dogID){
                    this->onSelectDog(dogID);
                });
            }
        }
        
        index++;
    }

    updatePlanetIcon();
    setupTopBar();
}

void DogSelectionSceneLayer::onSelectDog(int selectedDogID)
{
    bool isUnlocked = DogManager::instance()->getUnlockStatus(selectedDogID) == DogManager::StatusUnlocked;

    if(!isUnlocked || selectedDogID == mSelectedDog){
        return;
    }
    
    DogManager::instance()->selectDog(selectedDogID);
    DogManager::instance()->removeNewDog(selectedDogID);
 
    Vector<Widget*>& widgetList = mDogListView->getItems();
    
    for(Widget *widget : widgetList) {
        DogSelectItemView *dogView = (DogSelectItemView *) widget;
        if(!dogView->isVisible()){
            break;
        }
        
        int dogID = dogView->getDogID();
        
        if(dogID == selectedDogID || dogID == mSelectedDog){
            dogView->setDog(dogID);
        }
    }
    
    mSelectedDog = selectedDogID;
}

void DogSelectionSceneLayer::refreshItemView(int dogID)
{
    Vector<Widget*>& widgetList = mDogListView->getItems();
    
    for(Widget *widget : widgetList) {
        DogSelectItemView *dogView = (DogSelectItemView *) widget;
        if(!dogView->isVisible()){
            break;
        }
        
        int currentDogID = dogView->getDogID();
        
        if(dogID == currentDogID){
            dogView->setDog(dogID);
        }
    }
}


int DogSelectionSceneLayer::getPlanetIDWithChange(int valueChange)
{
//	int newID = mSelectedPlanet + valueChange;
//	if(newID < mFirstPlanet) {
//		newID = mLastPlanet;
//	}else if(newID > mLastPlanet) {
//		newID = mFirstPlanet;
//	}
    
    int index = 0;
    for(int i=0; i < mPlanetIDList.size() ;i++)
    {
        if(mPlanetIDList.at(i) == mSelectedPlanet){
            index = i;
             break;
        }
    }
    
    index += valueChange;
    if(index<0){
        index = (int)mPlanetIDList.size()-1;
    }else if(index>=mPlanetIDList.size()){
        index = 0;
    }
	
	return mPlanetIDList.at(index);
}
