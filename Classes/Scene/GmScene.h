//
//  GmScene.hpp
//  GhostLeg
//
//  Created by Ken Lee on 30/6/2016.
//
//

#ifndef GmScene_hpp
#define GmScene_hpp

#include <stdio.h>


#include <stdio.h>
#include <time.h>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "CommonType.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

// Gm: Game Master!
class GmSceneLayer : public Layer
{
public:
    
	static Scene *createScene();
	
	virtual bool init();
    virtual void onEnter();
	
	CREATE_FUNC(GmSceneLayer);
	
private:
	void setupUI();
	
	void updateUI();

	int getProperty(const std::string &name);

	void showDebugMapView();
	
	void showPlayerRecord();
	void clearPlayerRecord();
	void showDogSelectDialog();
	
	void toggleCollision();
	void updateCollisionButton();
	void toggleBackground();		// note: it's faster if no background
	void updateBackgroundButton();
	void toggleStat();
	
	void resetCoachmark();

	void toggleAnalytics();
	void updateAnalyticsButton();
    

	void updateToggleButtonLabel(Button *button, bool flag, const std::string &name);
private:
	Node *mRootNode;
	CheckBox *mCollisionCheck;
	Button *mCollisionButton;
	Button *mBackgroundButton;
	Button *mAnalyticsButton;
};

#endif /* GmScene_hpp */
