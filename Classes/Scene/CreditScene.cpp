//
//  CreditScene.cpp
//  GhostLeg
//
//  Created by Ken Lee on 7/7/2016.
//
//

#include "CreditScene.h"

#include "GameManager.h"
#include "UserGameData.h"
#include "UserGameData.h"
#include "CommonMacro.h"
#include "AdManager.h"
#include "ViewHelper.h"
#include "cocostudio/CocoStudio.h"
#include "Constant.h"
#include "Analytics.h"
#include "FBProfilePic.h"

const std::string kSecret = "ladygaga";

#define ALERT_Y_POS	 Director::getInstance()->convertToGL(Vec2(0, 80)).y

Scene* CreditSceneLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	auto layer = CreditSceneLayer::create();
	
	// add layer as a child to scene
	scene->addChild(layer);
	
	// return the scene
	return scene;
}

bool CreditSceneLayer::init(){
	if ( !Layer::init() )
	{
		return false;
	}
	
	Node *rootNode = CSLoader::createNode("CreditScene.csb");
	addChild(rootNode);
	setupUI(rootNode);
	
	AstrodogClient::instance()->setListener(this);
	updateVersion();
	
	LOG_SCREEN(Analytics::Screen::credit);
	
	return true;
	
}


void CreditSceneLayer::updateVersion()
{
	if(mVersionButton) {
		std::string version = GameManager::instance()->getVersionString();
		mVersionButton->setTitleText(version);
	}
}

void CreditSceneLayer::setupUI(Node *rootNode){
	FIX_UI_LAYOUT(rootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	
	mBackButton = (Button *) mainPanel->getChildByName("backBtn");
	mVersionButton = (Button *) mainPanel->getChildByName("versionBtn");
	mSecretPanel = (Node *) rootNode->getChildByName("secretPanel");
	mSecretTextField = (TextField *) mSecretPanel->getChildByName("secretTextField");
	mSecretConfirmButton = (Button *) mSecretPanel->getChildByName("confirmBtn");
	mSecretBackButton = (Button *) mSecretPanel->getChildByName("secretBackBtn");
	
	mSecretPanel->setVisible(false);
	
	// Setting Buttons
	setupButtonListener(mainPanel);
	
    mStarText = (Text*) rootNode->getChildByName("totalCoinText");
    mDiamondText = rootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mFbBtn = (Button*) rootNode->getChildByName("fbBtn");
    mFbName = (Text *) rootNode->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) rootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);


//	Button *mBackButton;
//	Node *mSecretPanel;
//	Text *mSecretText;
//	Button *mVersionButton;
//	Button *mSecretConfirmButton;
//	Button *mSecretBackButton;
//
//	Node* musicPanel = mRootNode->getChildByName("musicPanel");
//	Node* SEPanel = mRootNode->getChildByName("SEPanel");
//	
//	mCreditsBtn = mRootNode->getChildByName<Button*>("credits");
//	mBackBtn = mRootNode->getChildByName<Button*>("backBtn");
//	
//	if(GameManager::instance()->getUserData()->isBGMOn()){
//		mMusicBtn = musicPanel->getChildByName<Button*>("ONBtn");
//	}else{
//		mMusicBtn = musicPanel->getChildByName<Button*>("OFFBtn");
//	}
//	
//	if(GameManager::instance()->getUserData()->isSEOn()){
//		mSEBtn = SEPanel->getChildByName<Button*>("ONBtn");
//	}else{
//		mSEBtn = SEPanel->getChildByName<Button*>("OFFBtn");
//	}
//	
//	mMusicBtn->setVisible(true);
//	mSEBtn->setVisible(true);
}

void CreditSceneLayer::onEnter(){
	Layer::onEnter();
	
	// AdManager::instance()->showBannerAd("Banner");
    setupFbProfile();
    updateDiamond();
    updateStars();
    
	scheduleUpdate();
	
	mSecretClickCount = 0;
}

void CreditSceneLayer::onExit(){
//	AdManager::instance()->hideBannerAd();
	
	Layer::onExit();
}

void CreditSceneLayer::update(float delta)
{
//	static float offset = 0;
	
	
//	mParallaxLayer->setScrollY(offset);
	
//	offset += delta * 100;
}


std::string CreditSceneLayer::getInputSecret()
{
	if(mSecretTextField == nullptr) {
		return "";
	}
	
	return mSecretTextField->getString();
}

void CreditSceneLayer::showSecretPanel()
{
	mSecretPanel->setVisible(true);
	mSecretPanel->setPositionX(0);
}

void CreditSceneLayer::hideSecretPanel()
{
	mSecretPanel->setVisible(false);
	mSecretPanel->setPositionX(getContentSize().width);
}


void CreditSceneLayer::attemptToShowSecret()
{
	mSecretClickCount++;
	
	if(mSecretClickCount == 4) {
		ViewHelper::showFadeAlert(this, "3 more clicks!", ALERT_Y_POS);
	}
	
	if(mSecretClickCount >= 7) {
		showSecretPanel();
	}
}

void CreditSceneLayer::checkSecret()
{
	if(kSecret != getInputSecret()) {
		ViewHelper::showFadeAlert(this, "Don't tell the secret to other", ALERT_Y_POS);
		return;
	}
	
	
	ViewHelper::showFadeAlert(this, "Gm mode unlocked", ALERT_Y_POS);
	GameManager::instance()->getUserData()->setGmModel(true);
	
	hideSecretPanel();
}

void CreditSceneLayer::setupButtonListener(Node *mainPanel)
{
//	ViewHelper::addBackButtonListener(this, mBackButton, ViewHelper::BackHandleType::PopScene);
    
    mBackButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                this->removeFromParent();
            }
        }
    });

    
	
	mSecretBackButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type) {
			case ui::Widget::TouchEventType::ENDED: {
				hideSecretPanel();
			}
		}
	});
	
	
	mVersionButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type) {
			case ui::Widget::TouchEventType::ENDED: {
				log("Debug versionButton is clicked");
				attemptToShowSecret();
			}
		}
	});
	
	mSecretConfirmButton->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type) {
			case ui::Widget::TouchEventType::ENDED: {
				log("Debug versionButton is clicked");
				checkSecret();
			}
		}
	});

	Button *button;
	button = (Button *) mainPanel->getChildByName("fbButton");
	button->addClickEventListener([&](Ref *sender) {
		gotoFacebook();
	});
	
	button = (Button *) mainPanel->getChildByName("twitterButton");
	button->addClickEventListener([&](Ref *sender) {
		gotoTwitter();
	});
	/// panel->get
}


void CreditSceneLayer::gotoFacebook()
{
	CCApplication::getInstance()->openURL(kFacebookPageLink);
}

void CreditSceneLayer::gotoTwitter()
{
	CCApplication::getInstance()->openURL(kTwitterLink);
}

void CreditSceneLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void CreditSceneLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void CreditSceneLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        mFbName->setString(user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }
        mProfilePic->setWithFBId(user->getFbID());
    }else{
        mFbBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void CreditSceneLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}

