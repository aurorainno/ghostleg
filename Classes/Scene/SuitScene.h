//
//  SuitScene.h
//  GhostLeg
//
//  Created by Calvin on 13/6/2016.
//
//

#ifndef SuitScene_h
#define SuitScene_h
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "SuitItemView.h"

class ParallaxLayer;

class SuitSceneLayer : public Layer{
    
public:
    static Scene* createScene();
    virtual bool init();
    CREATE_FUNC(SuitSceneLayer);
    void refreshPage();
    void update(float delta);
    virtual void onEnter();
    virtual void onExit();

private:
    void setupUI();
    void updateCoins();
	void showAddCoinDialog();
	void showVideoAd(int suitID);
	
private: // data
    Node *mRootNode;
    ParallaxLayer* mParallaxLayer;
    cocos2d::ui::ScrollView* mScrollView;
    Text *mStars;
    Vector<SuitItemView*> mSuitItemViewList;
    
};

#endif /* SuitScene_h */
