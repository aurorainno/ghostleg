//
//  SettingScene.cpp
//  GhostLeg
//
//  Created by Calvin on 27/6/2016.
//
//

#include "SettingScene.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "CommonMacro.h"
#include "StageParallaxLayer.h"
#include "cocostudio/CocoStudio.h"
#include "GameSound.h"
#include "AdManager.h"
#include "Analytics.h"
#include "CreditScene.h"
#include "IAPManager.h"
#include "DailyRewardDialog.h"
#include "AstrodogClient.h"
#include "MainScene.h"
#include "FBProfilePic.h"
#include "GameScene.h"
#include "Constant.h"
#include "ViewHelper.h"

Scene* SettingSceneLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SettingSceneLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool SettingSceneLayer::init(){
    if ( !Layer::init() )
    {
        return false;
    }
    
    mRootNode = CSLoader::createNode("Option.csb");
    addChild(mRootNode);
    setupUI();
    setupListener();
    setupRestoreItemCallBack();
    AstrodogClient::instance()->setListener(this);
    
    return true;

}

void SettingSceneLayer::setupRestoreItemCallBack()
{
    IAPManager *instance = GameManager::instance()->getIAPManager();
    instance->setRestoreItemCallback([&](IAPStatusCode status, std::string msg){
        hideProgress();
        if(status==IAP_SUCCESS){
            showAlert("Your item is restored.",  msg);
        }else if (status==IAP_NO_ITEM_TO_RESTORE){
            showAlert("No items can be restored.",  msg);
        }else{
            showAlert("Failed to restore your items.",  msg);
        }
    });
}

void SettingSceneLayer::setupUI(){
    FIX_UI_LAYOUT(mRootNode);
    
    Node* musicPanel = mRootNode->getChildByName("musicPanel");
    Node* SEPanel = mRootNode->getChildByName("SEPanel");
    
    mCreditBtn = mRootNode->getChildByName<Button*>("creditBtn");
    mTutorialBtn = mRootNode->getChildByName<Button*>("tutorialBtn");
    mBackBtn = mRootNode->getChildByName<Button*>("backBtn");
    mRestoreBtn = mRootNode->getChildByName<Button*>("restoreBtn");
    mDailyRewardBtn = mRootNode->getChildByName<Button*>("dailyRewardBtn");
    mFbBtn = mRootNode->getChildByName<Button*>("fbBtn");
    mFbLikeBtn = mRootNode->getChildByName<Button*>("fbLikeBtn");
    mTwitterBtn = mRootNode->getChildByName<Button*>("twitterButton");
    mFeedbackBtn = mRootNode->getChildByName<Button*>("feedbackBtn");
    
    Node* topPanel = mRootNode->getChildByName("topPanel");
    mStarText = (Text*) topPanel->getChildByName("totalCoinText");
    mDiamondText = topPanel->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mTopFbBtn = (Button*) topPanel->getChildByName("fbBtn");
    mFbName = (Text *) topPanel->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) topPanel->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
  

    
    bool isFbConnected = AstrodogClient::instance()->isFBConnected();
    std::string name = isFbConnected ? "connected" : "connect";
    Text* btnText = mFbBtn->getChildByName<Text*>("content");
    btnText->setString(name);
    
    if(GameManager::instance()->getUserData()->isBGMOn()){
        mMusicBtn = musicPanel->getChildByName<Button*>("ONBtn");
    }else{
        mMusicBtn = musicPanel->getChildByName<Button*>("OFFBtn");
    }
    
    if(GameManager::instance()->getUserData()->isSEOn()){
        mSEBtn = SEPanel->getChildByName<Button*>("ONBtn");
    }else{
        mSEBtn = SEPanel->getChildByName<Button*>("OFFBtn");
    }
    
    mMusicBtn->setVisible(true);
    mSEBtn->setVisible(true);
    
    mProgressLayer = mRootNode->getChildByName("ProgressLayer");
    
    mAlertLayer = mRootNode->getChildByName("AlertLayer");
    Button *confirmBtn = (Button*)mAlertLayer->getChildByName("confirmBtn");
    //set lisenter for alert button
    confirmBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:{
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            }
            case ui::Widget::TouchEventType::ENDED: {
                mAlertLayer->setVisible(false);
                break;
            }
        }
    });

}

void SettingSceneLayer::onEnter(){
    Layer::onEnter();
    
    setupFbProfile();
    updateDiamond();
    updateStars();
    
//    AdManager::instance()->showBannerAd("Banner");
    
    // logging
    Analytics::instance()->logScreen(Analytics::Screen::setting);
	
	
}

void SettingSceneLayer::onExit(){
    AdManager::instance()->hideBannerAd();
    if(mCallback){
        mCallback();
    }
    
    Layer::onExit();
}

void SettingSceneLayer::setupListener(){
    log("current BGM state: %d",GameManager::instance()->getUserData()->isBGMOn());
    log("current SE state: %d",GameManager::instance()->getUserData()->isSEOn());
    
    
    mRestoreBtn = (Button*)mRootNode->getChildByName("restoreBtn");
    mRestoreBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:{
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            }
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getIAPManager()->IAPManager::restorePurchasedItems();
                showProgress("Restoring your items....");
                break;
            }
        }
    });
    
    mMusicBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                bool state = GameManager::instance()->getUserData()->isBGMOn();
                GameManager::instance()->getUserData()->setBGMState(!state);
                mMusicBtn->setVisible(false);
                if(state == true){
                    Button* offBtn = mRootNode->getChildByName("musicPanel")->getChildByName<Button*>("OFFBtn");
                    mMusicBtn = offBtn;
                    GameSound::stop();
                }else{
                    Button* onBtn = mRootNode->getChildByName("musicPanel")->getChildByName<Button*>("ONBtn");
                    mMusicBtn = onBtn;
                    GameSound::playMusic(GameSound::Title);
                }
                mMusicBtn->setVisible(true);
                this->setupListener();
                break;
            }
        }
    });
    
    mSEBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                bool state = GameManager::instance()->getUserData()->isSEOn();
                GameManager::instance()->getUserData()->setSEState(!state);
                mSEBtn->setVisible(false);
                if(state == true){
                    Button* offBtn = mRootNode->getChildByName("SEPanel")->getChildByName<Button*>("OFFBtn");
                    mSEBtn = offBtn;
                }else{
                    Button* onBtn = mRootNode->getChildByName("SEPanel")->getChildByName<Button*>("ONBtn");
                    mSEBtn = onBtn;
                }
                mSEBtn->setVisible(true);
                this->setupListener();
                break;
            }
        }
    });

    mBackBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            case ui::Widget::TouchEventType::ENDED:
 //               Director::getInstance()->popScene();
                this->removeFromParent();
                break;
        }
    });

	mCreditBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
            case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
			case ui::Widget::TouchEventType::ENDED:
//				auto scene = CreditSceneLayer::createScene();
//				Director::getInstance()->pushScene(scene);
                auto layer = CreditSceneLayer::create();
                addChild(layer);
				break;
		}
	});
    
    mTutorialBtn->addClickEventListener([&](Ref *sender) {
		gotoTutorialScene();
    });
    
    mDailyRewardBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            case ui::Widget::TouchEventType::ENDED:
                //GameManager::instance()->resetTutorial();
                DailyRewardDialog *dialog = DailyRewardDialog::create();
                dialog->setDialogMode(DailyRewardDialog::DialogMode::ShowRecordMode);
                addChild(dialog);
                break;
        }
    });
    
    mFbBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            case ui::Widget::TouchEventType::ENDED:
                bool isConnected = AstrodogClient::instance()->isFBConnected();
                if(!isConnected){
                    AstrodogClient::instance()->connectFB();
                }
                break;
        }
    });
    
    mTopFbBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });
    
    mTwitterBtn->addClickEventListener([=](Ref*){
        CCApplication::getInstance()->openURL(kTwitterLink);
    });
    
    mFbLikeBtn->addClickEventListener([=](Ref*){
        CCApplication::getInstance()->openURL(kFacebookPageLink);
    });

    mFeedbackBtn->addClickEventListener([=](Ref*){
        CCApplication::getInstance()->openURL(kFeedbackLink);
    });


}

void SettingSceneLayer::showProgress(std::string msg){
    if(mProgressLayer){
        mProgressLayer->setPositionX(0);
        mProgressLayer->setVisible(true);
        Text* message = mProgressLayer->getChildByName<Text*>("message");
        message->setString(msg);
    }
}

void SettingSceneLayer::hideProgress(){
    if(mProgressLayer)
        mProgressLayer->setVisible(false);
}

void SettingSceneLayer::showAlert(std::string title, std::string info){
    if(mAlertLayer){
        mAlertLayer->setPositionX(0);
        mAlertLayer->getChildByName<Text*>("title")->setString(title);
        mAlertLayer->getChildByName<Text*>("info")->setString(info);
        mAlertLayer->setVisible(true);
    }
}

void SettingSceneLayer::closeAlert(){
    if(mAlertLayer){
        mAlertLayer->setVisible(false);
    }
}

void SettingSceneLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void SettingSceneLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void SettingSceneLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
		
		ViewHelper::setUnicodeText(mFbName, user->getName());
		
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
//        mTopFbBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void SettingSceneLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}

void SettingSceneLayer::setOnExitCallback(std::function<void ()> callback)
{
    mCallback = callback;
}


void SettingSceneLayer::gotoTutorialScene()
{
	auto scene = GameSceneLayer::createTutorialScene();
	
	Director::getInstance()->pushScene(scene);
	
}
