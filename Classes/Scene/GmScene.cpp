//
//  GmScene.cpp
//  GhostLeg
//
//  Created by Ken Lee on 30/6/2016.
//
//

#include "cocostudio/CocoStudio.h"
#include "CommonMacro.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameModel.h"
#include "StringHelper.h"
#include "ViewHelper.h"
#include "GmScene.h"
#include "DebugMapView.h"
#include "Analytics.h"
#include "SuitManager.h"
#include "MasteryManager.h"
#include "AdManager.h"
#include "ConsoleView.h"
#include "VisibleRect.h"
#include "GmDogSelectionDialog.h"
#include "DogManager.h"
#include "DogData.h"
#include "PlayerRecord.h"
#include "PlanetManager.h"
#include "ChangeMapSpeedDialog.h"
#include "GameWorld.h"
#include "AstrodogClient.h"
#include "PluginFacebook/PluginFacebook.h"


namespace {	// handly local method
	void resetDiamond()
	{
		int currentValue = GameManager::instance()->getUserData()->getTotalDiamond();
		GameManager::instance()->getUserData()->addDiamond(-currentValue);

	}
	
}

Scene *GmSceneLayer::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();
	
	// 'layer' is an autorelease object
	auto layer = GmSceneLayer::create();
	
	// add layer as a child to scene
	scene->addChild(layer);
	
	// return the scene
	return scene;
}

bool GmSceneLayer::init()
{
	
	if ( !Layer::init() )
	{
		return false;
	}
	
	mRootNode = CSLoader::createNode("GmScene.csb");
	addChild(mRootNode);
	
	setupUI();
	updateUI();
	
	return true;
}

void GmSceneLayer::onEnter()
{
    Layer::onEnter();
    Analytics::instance()->logScreen(Analytics::Screen::gm);
}


void GmSceneLayer::setupUI()
{
	FIX_UI_LAYOUT(mRootNode);
	
	//
	Node *mainPanel = mRootNode->getChildByName("mainPanel");
	
	Node *scrollPanel = mainPanel->getChildByName("mainScrollView");
	
	Node *buttonParent = scrollPanel;
	
	
	//
	Button *button;
	
	button = (Button *) buttonParent->getChildByName("mapBtn");
	button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				showDebugMapView();
				break;
			}
		}
	});
	
	button = (Button *) buttonParent->getChildByName("resetTutorialBtn");
	button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				GameManager::instance()->resetTutorial();
				break;
			}
		}
	});
	
	button = (Button *) buttonParent->getChildByName("resetGmBtn");
	button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				GameManager::instance()->getUserData()->setGmModel(false);
				break;
			}
		}
	});
	
	// Change the collision behaviour
	button = (Button *) buttonParent->getChildByName("toggleCollisionBtn");
	button->addClickEventListener([&](Ref *sender) {
		toggleCollision();
	});
	
	mCollisionButton = button;
	updateCollisionButton();
	
	// Change the background behaviour
	button = (Button *) buttonParent->getChildByName("backgroundBtn");
	button->addClickEventListener([&](Ref *sender) {
		toggleBackground();
	});
	
	mBackgroundButton = button;
	updateBackgroundButton();
	
	// Change the background behaviour
	button = (Button *) buttonParent->getChildByName("toggleStatBtn");
	button->addClickEventListener([&](Ref *sender) {
		toggleStat();
	});
	

    button = (Button *) buttonParent->getChildByName("addStar");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getUserData()->addCoin(1000);
                //DogData *data = DogManager::instance()->getSelectedDogData();
				
                // PlayerRecord *record = GameManager::instance()->getPlayerRecord();
                // record->setPlayerRecord(PlayerRecord::KeyStarTotal, 1000);
                // record->setDogRecord(PlayerRecord::KeyStarTotal, data->getDogID(), 1000);
                
                break;
            }
        }
    });
	
    button = (Button *) buttonParent->getChildByName("addDiamond");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getUserData()->addDiamond(100);
                
                break;
            }
        }
    });
    
    button = (Button *) buttonParent->getChildByName("resetStar");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                int currentCoins = GameManager::instance()->getUserData()->getTotalCoin();
                GameManager::instance()->getUserData()->addCoin(-currentCoins);
                break;
            }
        }
    });
    
    button = (Button *) buttonParent->getChildByName("resetCandy");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
//                int currentCandy = GameManager::instance()->getUserData()->getTotalCandy();
//                GameManager::instance()->getUserData()->addCandy(-currentCandy);
				resetDiamond();
				
//				std::string appLinkUrl = "https://fb.me/1831229720537597";
//				std::string previewImageUrl = "http://aurorainnoapps.com:9280/static-doc//monsterEx/appIcon.png";
//				
//				sdkbox::PluginFacebook::inviteFriends(appLinkUrl, previewImageUrl);
				
                break;
            }
        }
    });
    
    button = (Button *) buttonParent->getChildByName("resetFB");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                AstrodogClient::instance()->resetUserFbData();
                break;
            }
        }
    });
    
    button = (Button *) buttonParent->getChildByName("resetCoachmark");
    button->addClickEventListener([&](Ref *sender) {
		resetCoachmark();
    });
	
    button = (Button *) buttonParent->getChildByName("toggleAnalytics");
    button->addClickEventListener([&](Ref *sender) {
		toggleAnalytics();
    });
	mAnalyticsButton = button;
	updateAnalyticsButton();
	
	
    
    button = (Button *) buttonParent->getChildByName("resetNoAds");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                bool isNoAd = AdManager::instance()->isNoAd();
                AdManager::instance()->setNoAd(false);
                break;
            }
        }
    });
    
    button = (Button *) buttonParent->getChildByName("resetBestDis");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getUserData()->clearRecord();
                break;
            }
        }
    });
	
	button = (Button *) buttonParent->getChildByName("showRecordBtn");
	button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				showPlayerRecord();
				break;
			}
		}
	});
	
	button = (Button *) buttonParent->getChildByName("resetRecordBtn");
	button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				clearPlayerRecord();
				break;
			}
		}
	});

	button = (Button *) buttonParent->getChildByName("selectDogBtn");
	button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				showDogSelectDialog();
				break;
			}
		}
	});
    
    button = (Button *) buttonParent->getChildByName("resetPlanetBtn");
    button->addClickEventListener([&](Ref *sender) {
		PlanetManager::instance()->lockAllPlanet();
    });

	button = (Button *) buttonParent->getChildByName("unlockPlanetBtn");
	button->addClickEventListener([&](Ref *sender) {
		PlanetManager::instance()->unlockAllPlanet();
	});
    
    button = (Button *) buttonParent->getChildByName("adjustSpeedBtn");
    button->addClickEventListener([&](Ref *sender) {
        ChangeMapSpeedDialog* dialog = ChangeMapSpeedDialog::create();
        addChild(dialog);
    });
    
    button = (Button *) buttonParent->getChildByName("showSpeedBtn");
    button->addClickEventListener([&](Ref *sender) {
        GameManager::instance()->toggleShowSpeed();
    });

    button = (Button *) buttonParent->getChildByName("toggleEvent");
    button->addClickEventListener([&](Ref *sender) {
        GameManager::instance()->setEventOnOff(!GameManager::instance()->isEventOn());
    });

	// toggleHitboxBtn
	button = (Button *) buttonParent->getChildByName("toggleHitbox");
	button->addClickEventListener([&](Ref *sender) {
		bool flag = GameModel::isShowingHitbox();
		GameModel::showHitbox(!flag);
		//bool flag = Game
		//GameManager::instance()->setEventOnOff(!GameManager::instance()->isEventOn());
	});

    button = (Button *) buttonParent->getChildByName("resetPlayAdTime");
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getUserData()->setGachaLastPlayAdTime(0);
                break;
            }
        }
    });
    
	//showDogSelectDialog
	
	// Setup Button Button
	Button *backBtn = (Button*) mainPanel->getChildByName("backBtn");
	ViewHelper::addBackButtonListener(this, backBtn, ViewHelper::BackHandleType::PopScene);
}

int GmSceneLayer::getProperty(const std::string &name)
{
	if("collision" == name) {
		int value = GameManager::instance()->getDebugProperty("noCollision");
		
		return value == 1 ? 0 : 1;		// reverse
	} else if("debugMap" == name) {
		return GameManager::instance()->getDebugMap();
	}
	
	return 0;
}
	   

void GmSceneLayer::updateUI()
{
	
	
	
}

void GmSceneLayer::showDebugMapView()
{
	DebugMapView *view = DebugMapView::create();
	
	addChild(view);
}

void GmSceneLayer::showPlayerRecord()
{
	ConsoleView *consoleView = ConsoleView::create();
	
	addChild(consoleView);

	consoleView->clear();
	consoleView->append(GameManager::instance()->infoPlayerRecord());
    int candy = GameManager::instance()->getUserData()->getTotalCandy();
    consoleView->append(StringUtils::format("candy=%d",candy));
}

void GmSceneLayer::clearPlayerRecord()
{
	GameManager::instance()->clearPlayerRecord();
	DogManager::instance()->resetSuggestRecord();
	
	ViewHelper::showFadeAlert(this, "Player Record is cleared", VisibleRect::center().y);
}

void GmSceneLayer::showDogSelectDialog()
{
	GmDogSelectionDialog *dialog = GmDogSelectionDialog::create();
	addChild(dialog);
}

void GmSceneLayer::updateCollisionButton()
{
	std::string key = "noCollision";
	int value = GameManager::instance()->getDebugProperty(key);
	
	bool collision = value == 0;	//
	std::string title = collision ? "Collision ON" : "Collision OFF";
	
	mCollisionButton->setTitleText(title);
}

void GmSceneLayer::toggleCollision()
{
	std::string key = "noCollision";
	int oldValue = GameManager::instance()->getDebugProperty(key);
	int newValue = oldValue == 1 ? 0 : 1;		// toggle
	
	GameManager::instance()->setDebugProperty(key, newValue);
	
	
	updateCollisionButton();
	
	
	
//	log("debug: collisionFlag=%d", collisionFlag);
//	mCollisionCheck->setSelected(collisionFlag);
//	
//	int value = type == CheckBox::EventType::SELECTED ? 0 : 1;
//	GameManager::instance()->setDebugProperty("noCollision", value)s
}


void GmSceneLayer::toggleBackground()
{
	std::string key = "noBackground";
	int oldValue = GameManager::instance()->getDebugProperty(key);
	int newValue = oldValue == 1 ? 0 : 1;		// toggle
	
	log("noBackground: old=%d new=%d", oldValue, newValue);
	
	GameManager::instance()->setDebugProperty(key, newValue);
	
	
	updateBackgroundButton();
}

void GmSceneLayer::updateToggleButtonLabel(Button *button, bool flag, const std::string &name)
{
	std::string title = name + " " + ((flag) ? "ON" : "OFF");
	
	button->setTitleText(title);
}

void GmSceneLayer::updateBackgroundButton()
{
	std::string key = "noBackground";
	int value = GameManager::instance()->getDebugProperty(key);
	
	log("noBackground: %d", value);
	
	bool haveBg = value == 0;	//
	
	updateToggleButtonLabel(mBackgroundButton, haveBg, "Background");
}

void GmSceneLayer::toggleStat()
{
	bool current = Director::getInstance()->isDisplayStats();
	
	Director::getInstance()->setDisplayStats(! current);
	
}


void GmSceneLayer::toggleAnalytics()
{
	bool isEnabled = Analytics::instance()->getEnabled();
	
	Analytics::instance()->setEnabled(!isEnabled);
	
	updateAnalyticsButton();
}

void GmSceneLayer::updateAnalyticsButton()
{
	bool isEnabled = Analytics::instance()->getEnabled();
	updateToggleButtonLabel(mAnalyticsButton, isEnabled, "Analytics");
}


void GmSceneLayer::resetCoachmark()
{
	GameManager::instance()->getUserData()->resetCoachmark();
}
