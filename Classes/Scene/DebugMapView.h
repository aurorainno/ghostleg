//
//  DebugMapView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/7/2016.
//
//

#ifndef DebugMapView_hpp
#define DebugMapView_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC_EXT;
USING_NS_CC;
using namespace cocos2d::ui;


class DebugMapView : public Layer
{
public:
	CREATE_FUNC(DebugMapView);
	
	DebugMapView();
	
	virtual bool init();
	
	
private:
	void setupUI(Node *mainPanel);

	void setupDebugMapList(ui::ScrollView *scrollView);
	
	void setDebugMap(int mapID);

	void updateCurrentMap();
	
private:
	Text *mCurrentMapText;
};


#endif /* DebugMapView_hpp */
