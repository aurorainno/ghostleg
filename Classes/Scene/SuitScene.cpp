//
//  SuitScene.cpp
//  GhostLeg
//
//  Created by Calvin on 13/6/2016.
//
//

#include "SuitScene.h"
#include "cocostudio/CocoStudio.h"
#include "StageParallaxLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "CommonMacro.h"
#include "Suit.h"
#include "SuitManager.h"
#include "NotEnoughCoinDialog.h"
#include "AdManager.h"
#include "Analytics.h"
#include "GameSound.h"

Scene* SuitSceneLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = SuitSceneLayer::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool SuitSceneLayer::init(){
    
    if ( !Layer::init() )
    {
        return false;
    }
    
    mRootNode = CSLoader::createNode("SuitScene.csb");
    mParallaxLayer = StageParallaxLayer::create();
    addChild(mParallaxLayer);
    addChild(mRootNode);
    setupUI();
    
    return true;
}


void SuitSceneLayer::update(float delta)
{
    static float offset = 0;
    
    
    mParallaxLayer->setScrollY(offset);
    
    offset += delta * 100;
}

//update the stars
void SuitSceneLayer::updateCoins(){
    char temp[100];
    sprintf(temp, "%d", GameManager::instance()->getUserData()->getTotalCoin());
    mStars->setString(temp);
}

void SuitSceneLayer::onEnter(){
    Layer::onEnter();
	
//	AdManager::instance()->showBannerAd("Banner");
    
    scheduleUpdate();
    
    LOG_SCREEN(Analytics::Screen::suit);
    
}

void SuitSceneLayer::onExit(){
	
    AdManager::instance()->hideBannerAd();
	
	Layer::onExit();
}

void SuitSceneLayer::setupUI(){
    FIX_UI_LAYOUT(mRootNode);
    mStars = (Text*) mRootNode->getChildByName("totalCoinText");
    updateCoins();
   	
    Vector<SuitData*> suitList = GameManager::instance()->getSuitManager()->getSuitArray();
    mScrollView = (ScrollView*)mRootNode->getChildByName("ScrollView");
    Button *backBtn = (Button*)mRootNode->getChildByName("backBtn");
    backBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN:
                GameSound::playSound(GameSound::Sound::Button1);
                break;
            case ui::Widget::TouchEventType::ENDED:
                Director::getInstance()->popScene();
                break;
        }
    });
    
    float spacingH = 20;
    float marginY = 30;
    float scrollViewHeight = mScrollView->getInnerContainerSize().height;
    float posY = scrollViewHeight - marginY;
    float posX = 0.0;
    
    log("suitListSize: %d\n",(int)suitList.size());
    for(SuitData* suit : suitList){
        SuitItemView* suitItemView = SuitItemView::create();
        
        mSuitItemViewList.pushBack(suitItemView);
        
        suitItemView->setView(suit);
        
        posX = (getContentSize().width - suitItemView->getContentSize().width)/2;
        posY -= suitItemView->getContentSize().height;
        
        log("suitScene: suitItemView.width: %.2f, height: %.2f\n",suitItemView->getContentSize().width,
            suitItemView->getContentSize().height);
        
        suitItemView->setPosition(Vec2(posX,posY));
        
        //TODO: itemView setCallback, show notEnoughCoinDialog
        suitItemView->setUpgradeCallback([&](SuitItemView *view, int suitID, int upgradeLevel) {
            if(suitID == 2 && upgradeLevel==1){

				showVideoAd(suitID);
				
//				ViewHelper::
				
//                AdManager::instance()->showVideoAd("VideoGainStar", [&,this](int){
//                    log("WacthAds Btn Callback is called.");
//                    GameManager::instance()->getSuitManager()->upgradeSuit(2);
//                    this->updateCoins();
//                    this->refreshPage();
//                });
            }else{
                if(GameManager::instance()->getSuitManager()->upgradeSuit(suitID) == -1){      // TODO: using #define or constant to
					showAddCoinDialog();
                }
                updateCoins();
                refreshPage();
            }
        });
        
        suitItemView->setSelectCallback([&](SuitItemView *view, int suitID){
            GameManager::instance()->getSuitManager()->selectPlayerSuit(suitID);
            refreshPage();
        });
        
        
        mScrollView->addChild(suitItemView);
        
        posY-=spacingH;
    }
}


void SuitSceneLayer::showVideoAd(int suitID)
{
	//                AdManager::instance()->showVideoAd("VideoGainStar", [&,this](int){
	//                    log("WacthAds Btn Callback is called.");
	//                    GameManager::instance()->getSuitManager()->upgradeSuit(2);
	//                    this->updateCoins();
	//                    this->refreshPage();
	
	AdManager::instance()->setVideoFinishedCallback([&, suitID](bool isPlayed){
		GameManager::instance()->getSuitManager()->upgradeSuit(suitID);
		refreshPage();
	});
	AdManager::instance()->setVideoRewardedCallback(nullptr);
	
	AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);	// ken: should add a new ad for this
}



void SuitSceneLayer::refreshPage(){
    for(SuitItemView* currentItem : mSuitItemViewList){
        int id = currentItem->getSuitId();
        currentItem->setView(GameManager::instance()->getSuitManager()->getSuitData(id));
    }
    updateCoins();
}

void SuitSceneLayer::showAddCoinDialog()
{
	NotEnoughCoinDialog *dialog = NotEnoughCoinDialog::create();
	addChild(dialog);
	
	dialog->setCloseCallback([&](bool isUpdated) {
		if(isUpdated) {
			updateCoins();
		}
	});
}


