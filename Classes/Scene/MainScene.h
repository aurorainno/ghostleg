//
//  MainScene.hpp
//  GhostLeg
//
//  Created by Ken Lee on 18/3/2016.
//
//

#ifndef MainScene_hpp
#define MainScene_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include "AstrodogClient.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class TutorialView;
class GameModel;
class GameCenterManager;
class StageSelectionView;
class CircularMaskNode;
class RichTextLabel;
class TutorialLayer;
class UIBestDistance;
class MainSceneInfoLayer;
class AnimeNode;
class LeaderboardDialog;
class FBProfilePic;
class PlanetItemView;

class MainSceneLayer : public Layer , public AstrodogClientListener
{
public:
	enum CoachmarkAction {
		CoachmarkActionNone,	// do nothing
		CoachmarkActionSelectChar,
		CoachmarkActionVisitPlanet,
	};
	
public:
	static Scene *createScene();
    static int nextDefaultPlanet;
    
    static void setNextDefaultPlanet(int planetID);
	
	CREATE_FUNC(MainSceneLayer);
	
	virtual bool init();
	void gotoGameScene();
	virtual void onEnter();
	virtual void onExit();
	void update(float delta);
	
    void showUI();
    void hideUI();
	void hideMainUI();
	void showMainUI();
	
    void scrollToPlanet(int planetID);
	
	void showUnlockPlanetCoachmark();
	void showSelectCharCoachmark();
    
    void updateTotalCoin();
    void updateCandyStick();
    void updateDiamond();
    
    // void updatePlanetInfoUI(bool isVisible, int planetID);
    
    void popStageNotClearText();
	
//	void handleExit();

	
#pragma mark - GUI Construction
private:
	void setupGUI(const std::string &csb);

	void setupUI(Node *mainPanel);
	void setupButtons(Node *mainPanel);
	
	
	void setupPlayerAnimation(Node *mainPanel);
	
    void updateChangePlanetButton(int currentPage, int maxPage);
    
	void addTestBackButton();
	
	void gotoFacebookPage();
	void gotoOption();
	
	void gotoDogSelection();
	void gotoShop();
	void gotoGMTool();
	
	void setBestScore(int score);

	void updateGUIData();
	
	void startGame();
	void showTutorial();
	void hideTutorial();
	void handleExit();
	void setInProgressNotice(Button *button);
	void handleTouchLayer();
	void updateCharacter();
	void updatePlanetState();
	
	void onPlanetSelected(int planetID, bool isRepeat, int currentPage, int maxPage);	// isRepeat=true if select again
    void onPlanetScrolled(Vec2 offset);
	void setPlayButtonEnable(bool flag);
    void setPlayButtonOpacity(GLbyte byte);

	void checkNewVersion();
    void checkNewDogs();
    bool checkNewPlanets();
    
    void showDailyRewardDialog(bool isCheckRecord = false);
    void showEventPopUp();
    
    void enterDogSelectionPage(int planetID);
	
	Vec2 getDogButtonPos();
	Vec2 getPlanetButtonPos();
	
	void handleCoachmarkAction();
	CoachmarkAction determineCoachmarkAction();
	
	void switchPlanetByButton(bool isRight);
    
    void updateBestDistanceNode(int planetID);
    
	
	void setupEventNotice();
    void updateEventNodeVisibility();
    
    void checkNewFreeGacha();
    
#pragma mark - Common
	

#pragma mark - Button handler
private:
	void onPlayClicked(Button *button);
	void onGachaClicked();
	void onCharacterBoxClicked();
	void onUnlockPlanetClicked(Button *button);
	void onOtherClicked();
	void onRankingClicked(Button *button);
	void onRewardMailClicked(Button *button);
	

	
#pragma mark - Leaderboard
	
#pragma mark - Keyboard handling
	void setKeyListener();
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);

#pragma mark - Internal Data
private:
    Node* mRootNode;
	Node *mBasePanel;
    Node* mTabViewLayer;
    Layout* mUnlockInfoPanel;
    Layout* mLockedInfoPanel;
    GameCenterManager* mManager;
    bool mPendingforGameCenter;
	Button *mStartButton;
	Button *mGmButton;
	Button *mDogButton;
    Button *mLeftButton, *mRightButton;
    Button *mDailyRewardBtn;
    Button *mEventBtn;
    Button *mFbBtn;
    Button *mPurchaseBtn;
	Text *mBestScoreText;
	Text *mTotalCoinText;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    Node *mTabBar;
    Node *mStageNotClearPanel;
    
    Node *mGachaBtnNewLabel;
    
    Sprite *mNewLabel;
	time_t mLastBackClickTime;
	TutorialView *mTutorialView;
    bool mBGMAboutToChange;
	GameModel *mCharModel;
	
    Sprite* mDogIconSprite;
    CircularMaskNode* mDogProgressBar;
    RichTextLabel* mUnlockInfoText;
    int mInfoPanelPlanetID;
	bool mCoachmarkShown;
    MainSceneInfoLayer* mInfoLayer;
    Node* mCandyPanel;
    Node* mDiamondPanel;
    AnimeNode* mEventPanel;
    LeaderboardDialog* mLBDialog;
    AstrodogLeaderboardType mCurrentLeaderBoardType;
    
    Text* mPlanetNameText;
    Text* mPlanetInfoText;
    
	TutorialLayer *mTutorialLayer;
    UIBestDistance *mBestDistanceNode;
    
    Vector<Node*> mMenuItemList;
    int mSelectedPlanet;
    
#pragma mark - AstrodogClient
public:	// AstrodogClientListener
	

#pragma mark - Coming Soon
public:
	void showComingSoonPopup();

#pragma mark - Stage Selection
private:
	void setupStageSelection();
	int getCurrentStageIndex();
	PlanetItemView *getSelectedPlanetItemView();
	int getSelectedPlanet();
	
private:
	void updateCurrentStageInfo();
private:
	StageSelectionView *mStageSelectionView;
	
	
#pragma mark - Leaderboard
private:
	void setupLeaderboard(Node *rootNode);
	void loadLeaderboard(AstrodogLeaderboardType type);
	void updateLeaderboardRecords();
	virtual void onGetRanking(int status, AstrodogLeaderboardType type, int planet,
							  const std::map<std::string, int> &metaData);

#pragma mark - FB connect
public:
	virtual void onFBConnect(int status);
private:
	void setupFBProfile();
	void setupProfilePic(Node *mainPanel);
	
#pragma mark - TabBar Handling
private:
	void setupTabBar(Node *mainPanel);
	void addNewMenuItem(Node *item);
	void removeOldMenuItem();

	
#pragma mark - Leaderboard count down
public:
	void setupLeaderboardEndTime(long long endTime);
	
	void setLeaderboardTime(long long remainMillis);
	void setLeaderboardTimeVisible(bool flag);
	
private:
	void updateLeaderboardTime(float delta);
	void setupLeaderboardTimeGUI(Node *mainPanel);
	
private:
    long long mLastLeaderboardRemainTime;
	Node *mLeaderboardTimePanel;
	Text *mLeaderboardTimeText;
	long long mLeaderboardEndTime;
	bool mLeaderboardTimeStarted;
};


#endif /* MainScene_hpp */
