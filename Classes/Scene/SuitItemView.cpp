//
//  SuitItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 13/6/2016.
//
//

#include <stdio.h>
#include "SuitItemView.h"
#include "Suit.h"
#include "SuitManager.h"
#include "GameManager.h"
#include "cocostudio/CocoStudio.h"
#include "StringHelper.h"
#include <iomanip> 
#include "GameSound.h"

using namespace std;

bool SuitItemView :: init(){
    
    if ( !Layer::init() )
    {
        return false;
    }
    
    mRootNode = CSLoader::createNode("SuitItemView.csb");
    addChild(mRootNode);
    setupUI(mRootNode);
    
    return true;
}

void SuitItemView::setupUI(Node *rootNode)
{
    setContentSize(rootNode->getContentSize());
    
    Node* infoPanel = rootNode->getChildByName("Info");
    mCurrentLevelPanel = infoPanel->getChildByName("CurrentValuePanel");
    mNextLevelPanel = infoPanel->getChildByName("NextValuePanel");
    
    mInfo = infoPanel->getChildByName<Text*>("InfoText");
    
    mCurrentItemIcon = mCurrentLevelPanel->getChildByName<Sprite*>("ValueIcon");
    mCurrentLevelValue = mCurrentLevelPanel->getChildByName<Text*>("ValueText");
    
    mNextItemIcon = mNextLevelPanel->getChildByName<Sprite*>("ValueIcon");
    mNextLevelValue = mNextLevelPanel->getChildByName<Text*>("ValueText");
    
    mCharIcon = rootNode->getChildByName<Sprite*>("Icon");
    mTitlePanel = rootNode->getChildByName("TitlePanel");

}

void SuitItemView::setView(SuitData *suit){
    int suitID = suit->getSuitID();
    
    int currentLevel = GameManager::instance()->getSuitManager()->getPlayerSuitLevel(suitID);
    
    if(suit == nullptr) {
        log("setSuit: suit is null");
        return;
    }
    
    mSuitID = suit->getSuitID();
    mSuitLevel = currentLevel;
    
    
    // define the next level and isMax flag
    bool isMaxLevel = suit->getMaxLevel() == currentLevel;
    
    int nextLevel = isMaxLevel ? currentLevel : currentLevel + 1;
    
    // define suit value and cost
    float currentValue = suit->getLevelValue(currentLevel);
    float nextValue = suit->getLevelValue(nextLevel);
    int upgradeCost = suit->getUpgradePrice(nextLevel);
    
    //set title panel data
    Text* title = mTitlePanel->getChildByName<Text*>("Title");
    Sprite* locked = mTitlePanel->getChildByName<Sprite*>("locked");
    Text* level = mTitlePanel->getChildByName<Text*>("Level");
    
    title->setString(suit->getName());
    
    if(currentLevel==0){
        locked->setVisible(true);
        level->setVisible(false);
    }else{
        locked->setVisible(false);
        level->setVisible(true);
        level->setString("lv"+INT_TO_STR(currentLevel));
    }
    
    mInfo->setString(suit->getInfo());
    
    //set level value
    if(suit->getSuitID()==1){
        mCurrentLevelPanel->setVisible(false);
        mNextLevelPanel->setVisible(false);
    }else{
        if(currentLevel==0||isMaxLevel){
            mCurrentLevelPanel->setVisible(true);
            mNextLevelPanel->setVisible(false);
            
            mCurrentItemIcon->setTexture(suit->getItemIcon());
            mCurrentLevelValue->setString(aurora::StringHelper::getFormattedString(suit->getLevelValue(nextLevel),suit->getDisplayFormat()));
        }else{
            mCurrentLevelPanel->setVisible(true);
            mNextLevelPanel->setVisible(true);
            
            mCurrentItemIcon->setTexture(suit->getItemIcon());
            mCurrentLevelValue->setString(aurora::StringHelper::getFormattedString(suit->getLevelValue(currentLevel),suit->getDisplayFormat()));
            
            mNextItemIcon->setTexture(suit->getItemIcon());
            mNextLevelValue->setString(aurora::StringHelper::getFormattedString(suit->getLevelValue(nextLevel),suit->getDisplayFormat()));
        }
    }
    
    mCharIcon->setTexture(suit->getCharIcon());
    
    setupBtn(suit,isMaxLevel);
   
}


void SuitItemView::setUpgradeCallback(const SuitItemViewUpgradeCallback &callback)
{
    mUpgradeCallback = callback;
}

void SuitItemView::setSelectCallback(const SuitItemViewSelectCallback &callback){
    mSelectCallback = callback;
}

void SuitItemView::setupBtn(SuitData* suit,bool isMaxLevel){
    bool setCallback = true;
    Button* upgradeBtn;
    mUpgradeBtn = mRootNode->getChildByName<Button*>("priceBtn");
    mWatchAdsBtn = mRootNode->getChildByName<Button*>("watchAdsBtn");
    mMaxBtn = mRootNode->getChildByName<Button*>("maxBtn");
    mUseBtn = mRootNode->getChildByName<Button*>("useBtn");
    mSelectedBtn = mRootNode->getChildByName<Button*>("selectedBtn");
    
    if(suit->getSuitID()==2&&mSuitLevel==0){
        mUpgradeBtn->setVisible(false);
        mMaxBtn->setVisible(false);
        mWatchAdsBtn->setVisible(true);
        upgradeBtn = mWatchAdsBtn;
    }else if(isMaxLevel){
        mUpgradeBtn->setVisible(false);
        mMaxBtn->setVisible(true);
        mWatchAdsBtn->setVisible(false);
        setCallback = false;
    }else{
        mUpgradeBtn->setVisible(true);
        mMaxBtn->setVisible(false);
        mWatchAdsBtn->setVisible(false);
        Text* price = mUpgradeBtn->getChildByName<Text*>("priceText");
        price->setString(INT_TO_STR(suit->getUpgradePrice(mSuitLevel+1)));
        upgradeBtn = mUpgradeBtn;
    }
    
    if(setCallback){
        Text* text = upgradeBtn->getChildByName<Text*>("priceLevelText");
        if(GameManager::instance()->getSuitManager()->getPlayerSuitLevel(suit->getSuitID())==0){
            log("suitItemView: levelText.width %.2f",text->getContentSize().width);
            text->setString(" unlock");
        }else{
            text->setString("upgrade");
        }
        
        upgradeBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
            switch(type) {
                case ui::Widget::TouchEventType::BEGAN:
                    GameSound::playSound(GameSound::Sound::Button1);
                    break;
                case ui::Widget::TouchEventType::ENDED: {
                    if(mUpgradeCallback)
                        mUpgradeCallback(this, mSuitID, mSuitLevel+1);
                    break;
                
                }
            }
        });
    }
    //handle use and selected btn
    if(mSuitID==GameManager::instance()->getSuitManager()->getSelectedSuit()){
        mSelectedBtn->setVisible(true);
        mUseBtn->setVisible(false);
    }else{
        mSelectedBtn->setVisible(false);
        mUseBtn->setVisible(true);
        mUseBtn->setEnabled(true);
        mUseBtn->setOpacity(255);
        if(GameManager::instance()->getSuitManager()->getPlayerSuitLevel(mSuitID)==0){
            mUseBtn->setEnabled(false);
            mUseBtn->setOpacity(80);
        }else{
            mUseBtn->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
                switch(type) {
                    case ui::Widget::TouchEventType::BEGAN:
                        GameSound::playSound(GameSound::Sound::Select);
                        break;
                    case ui::Widget::TouchEventType::ENDED: {
                        if(mSelectCallback)
                            mSelectCallback(this, mSuitID);
                        break;
                        
                    }
                }
            });
        }
    }
    


}
