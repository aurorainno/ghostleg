//
//  SuitItemView.h
//  GhostLeg
//
//  Created by Calvin on 13/6/2016.
//
//

#ifndef SuitItemView_h
#define SuitItemView_h
#include "cocos2d.h"
#include "ui/CocosGUI.h"


using namespace cocos2d;
using namespace cocos2d::ui;

class SuitData;

class SuitItemView : public Layer{
public:
    typedef std::function<void(SuitItemView *view, int suitID, int level)> SuitItemViewUpgradeCallback;
    typedef std::function<void(SuitItemView *view, int suitID)> SuitItemViewSelectCallback;
    
    CREATE_FUNC(SuitItemView);
    
    //
    virtual bool init();
    
    //
    void setView(SuitData* suit);
    void setUpgradeCallback(const SuitItemViewUpgradeCallback &callback);
    void setSelectCallback(const SuitItemViewSelectCallback &callback);
    int getSuitId(){return mSuitID;}
        
private:
    void setupUI(Node *rootNode);
    void setupBtn(SuitData* suit,bool isMax);
    
    Button *mUpgradeBtn, *mWatchAdsBtn, *mMaxBtn;
    Button *mSelectedBtn, *mUseBtn;
    Text* mCurrentLevelValue;
    Text* mNextLevelValue;
    Text* mInfo;
    Sprite *mCharIcon, *mCurrentItemIcon, *mNextItemIcon;
    Node *mRootNode, *mTitlePanel, *mCurrentLevelPanel, *mNextLevelPanel;
    
    int mSuitID;
    int mSuitLevel;
    
    SuitItemViewUpgradeCallback mUpgradeCallback;
    SuitItemViewSelectCallback mSelectCallback;
    
};


#endif /* SuitItemView_h */
