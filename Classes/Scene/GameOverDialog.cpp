//
//  GameOverDialog.cpp
//  TapToJump
//
//  Created by Ken Lee on 19/4/15.
//
//

#include "GameOverDialog.h"

#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "StageParallaxLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameWorld.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "GameModel.h"
#include "GameCenterManager.h"
#include "AdManager.h"
#include "Analytics.h"
#include "DeviceHelper.h"
#include "DogManager.h"
#include "ConsoleView.h"
#include "PlanetManager.h"
#include "PlayerRecord.h"
#include "GameSound.h"
#include "AnimationHelper.h"
#include "UnlockDogLayer.h"
#include "DogSelectionScene.h"
#include "GameScene.h"
#include "LeaderboardItemView.h"
#include "LeaderboardDialog.h"
#include "GalleryView.h"
#include "NPCRating.h"
#include "NPCRatingItemView.h"
#include "NPCManager.h"
#include "AnimeNode.h"
#include "PlayerGameResult.h"
#include "DataHelper.h"

using namespace cocostudio::timeline;


// Local Method
namespace {
	void setFadeIn(Node *node, float delayTime, float fadeTime)
	{
		
		auto delay = delayTime <= 0 ? nullptr : DelayTime::create(delayTime);
		auto fadeIn = FadeIn::create(fadeTime);
		
		Action *action;
		
		if(delay == nullptr) {
			action = fadeIn;
		} else {
			action = Sequence::create(delay, fadeIn, nullptr);
		}
		
		node->setOpacity(0);
		node->runAction(action);
	}
	
	void setMoveIn(Node *node, float delayTime, float moveTime, const Vec2 &pos)
	{
		
		auto delay = delayTime <= 0 ? nullptr : DelayTime::create(delayTime);
		auto moveIn = MoveTo::create(moveTime, pos);
		
		Action *action;
		
		if(delay == nullptr) {
			action = moveIn;
		} else {
			action = Sequence::create(delay, moveIn, nullptr);
		}
		
		node->runAction(action);
	}
	
	void updateDistanceText(Text *text, int distance)
	{
		if(text == nullptr) {
			return;
		}
		
		char temp[100];
		sprintf(temp, "%dM", distance);
		
		text->setString(temp);
	}
}

GameOverDialog::GameOverDialog()
: mReviveCallback(nullptr)
, mButtonY1(0)
, mButtonY2(0)
, mAccumCoins(0)
, mDisplayCoins(0)
, mBonusStarAmount(0)
, mIsReviveRewarded(false)
, mNewRecord(nullptr)
, nextUnlockMsg(nullptr)
, mCandyButton(nullptr)
, mLastBestDistance(0)
{
	
}

// on "init" you need to initialize your instance
bool GameOverDialog::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}
	
	mRetryButton = nullptr;
	mCancelButton = nullptr;
    
	// Setup UI
	Node *rootNode = CSLoader::createNode("GameOverDialog.csb");
	addChild(rootNode);
	setupUI(rootNode);

	

	// update the coin collected
//    updateCoinsGet();
//    
//    // update the candy collected
//    //updateCandyGet();		// ken: no need now
//	
//	//update score
////    updateDistance();
//    updateScore();

	hideAllPopText();
	
    //updateEndGameResultData();		// ken: old code
    
	//setup mock data for npc rating:
    Vector<NPCRating*> ratingList = NPCManager::instance()->getRatingList();
    setNPCRating(ratingList);
    
    
//	updatePlayerModel();
	
	// update unlock hint
	
//	updateUnlockHint();
	return true;
}

void GameOverDialog::onEnter()
{
    Layer::onEnter();
    Node* panelDialog = mRootNode->getChildByName("panelDialog");
    setFadeIn(panelDialog, 0.0, 0.2f);
//	AdManager::instance()->showBannerAd("Banner");
	
	displayGameScores();
	
    setupRewardPanel();
    LOG_SCREEN(Analytics::Screen::game_over);
	
	AstrodogClient::instance()->setListener(this);
	uploadLeaderboardScore();
}


void GameOverDialog::onExit(){
//	AdManager::instance()->hideBannerAd();
	
	AstrodogClient::instance()->removeListener();
	
	Layer::onExit();
}


void GameOverDialog::setNewRecordEffect(bool flag)
{
	if(flag) {
		mNewRecordSprite->setVisible(true);
		mNewRecordParticle->setVisible(true);
		mNewRecordParticle->resetSystem();
		
		AnimationHelper::setMoveUpEffect(mNewRecordSprite, 0.3, 8, false, 0);
		
		GameSound::playSound(GameSound::VoiceAmazing);
	} else {
		mNewRecordSprite->setVisible(false);
		mNewRecordParticle->setVisible(false);
		mNewRecordParticle->stopSystem();
	}
}
// ui note:
//		panelDialog	Panel	Panel for the Dialog
//		textGameOver	Text	Game Over Title
//		buttonOK		Button	OK Button
//		buttonCancel	Button	Cancel Button
//		scoreText		Text
//		bestScoreText	Text
void GameOverDialog::setupUI(cocos2d::Node *mainPanel)
{
	Node *node, *panelDialog, *upperPanel, *lowerPanel;
	
	// Fine tune layer after layer resizing
	FIX_UI_LAYOUT(mainPanel);			// same as above two lines
	
    mRootNode = mainPanel;
	//
	panelDialog = mainPanel->getChildByName("panelDialog");
    mPanelDialog = panelDialog;
		
    int selectedPlanet = PlanetManager::instance()->getSelectedPlanet();
    PlanetData* data  = PlanetManager::instance()->getPlanetData(selectedPlanet);
    if(data->getPlanetType() == PlanetData::Event){
        upperPanel = panelDialog->getChildByName("eventUpperPanel");

        mCandyButton = (Button*) upperPanel->getChildByName("collectedCandyLabel")->getChildByName("candyBtn");
        mCandyButton->addClickEventListener([&](Ref*){
            auto scene = Scene::create();
            
            DogSelectionSceneLayer* layer = DogSelectionSceneLayer::create();
            layer->setEnterAtSpecificPlanet(51); //hardcode: event planetID = 51
            
            scene->addChild(layer);
            Director::getInstance()->pushScene(scene);
            
            GameSceneLayer* uiLayer = GameWorld::instance()->getGameUILayer();
            uiLayer->setStartOnEnter(false);
        });
    }else{
        upperPanel = panelDialog->getChildByName("upperPanel");
    }
    upperPanel->setVisible(true);
    
    lowerPanel = panelDialog->getChildByName("lowerPanel");
	
    mUpperPanel = upperPanel;
    mLowerPanel = lowerPanel;
	// Binding the UI component to local variable
	
//	mPlayerModelNode = panelDialog->getChildByName("playerModelNode");
	
//    mDistanceText = (Text *) upperPanel->getChildByName("distanceText");
    
//    mBestScoreText = (Text *) upperPanel->getChildByName("bestScoreLabel")
//											->getChildByName("bestScoreText");
	
    mDeliveryScorePanel = upperPanel->getChildByName("deliveryScorePanel");
	mDeliveryScoreText = (Text *) mDeliveryScorePanel->getChildByName("scoreText");
	
	
    mClearScorePanel = upperPanel->getChildByName("clearScorePanel");					// Clear Stage (mTipPanel)
    mClearScoreText = (Text *) mClearScorePanel->getChildByName("scoreText");
	
	mCollectScorePanel = upperPanel->getChildByName("collectScorePanel");
	mCollectScoreText = (Text*) mCollectScorePanel->getChildByName<Text*>("scoreText");
	
	mDistanceScorePanel = upperPanel->getChildByName("distanceScorePanel");
	mDistanceScoreText = (Text*) mDistanceScorePanel->getChildByName<Text*>("scoreText");
	
	
    mKillScorePanel = upperPanel->getChildByName("killScorePanel");
    mKillScoreText = (Text *) mKillScorePanel->getChildByName("scoreText");
    
	mBonusScorePanel = upperPanel->getChildByName("bonusScorePanel");
    mBonusScoreText = (Text*) mBonusScorePanel->getChildByName<Text*>("scoreText");

	mTotalScorePanel = upperPanel->getChildByName("totalScorePanel");
	mTotalScoreText = (Text*) mTotalScorePanel->getChildByName<Text*>("scoreText");

	
	
    mRewardPanel = panelDialog->getChildByName("rewardPanel");
    
    mCollectedCoinsPanel = mRewardPanel->getChildByName("collectedCoinPanel");
    mCollectedCoinsText = mCollectedCoinsPanel->getChildByName<Text*>("coinText");
    
    
    
//    mNewRecord = (ImageView *) upperPanel->getChildByName("bestScoreLabel")
//                                            ->getChildByName("newRecord");
//    mNewRecord->setVisible(false);
	
	// For New Record
//	mNewRecordSprite = upperPanel->getChildByName("scoreLabel")
//						->getChildByName<Sprite *>("newRecordSprite");
    
    
//	mNewRecordParticle = mainPanel->getChildByName<ParticleSystemQuad *>("newRecordParticle");
	
//	setNewRecordEffect(false);
	
	//	// Bind node to corresponding class variable
    
    GalleryView* view = GalleryView::create();
    view->setTargetPosition(Vec2::ZERO);
    view->setPeriod(2);
    view->setRotationTime(0.5);
    mGalleryView = view;
    
    mLBDialog = LeaderboardDialog::create();
    mLBDialog->setLeaderboardUIType(LeaderboardDialog::LeaderboardUIType::Endgame);
    
    mLBDialog->setAnchorPoint(Vec2(0,0));
    
//    Size leaderboardSize = mLBDialog->getContentSize();
//    float height = leaderboardSize.height * 0.1;
//    Size reSize = Size(leaderboardSize.width,height);
//    mLBDialog->setContentSize(reSize);
    
 //   mLBDialog->setPosition(Vec2(0,getContentSize().height*0.2));
    
    mLBDialog->setFriendBtnCallback([&](){
        setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardFriend);
        loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardFriend);
    });
    
    mLBDialog->setNationalBtnCallback([&](){
        setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
        loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
    });
    
    mLBDialog->setGlobalBtnCallback([&](){
        setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardGlobal);
        loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardGlobal);
    });
    
    mLBDialog->setFbBtnCallback([&](){
        AstrodogClient::instance()->connectFB();
    });
    
    Node* lbPanel = panelDialog->getChildByName("leaderboardPanel");
    lbPanel->addChild(mLBDialog);
    //show national lb in default
    setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
    
    Node* puzzlePanel = mRewardPanel->getChildByName("puzzlePanel");
    Vector<Node*> puzzleList = puzzlePanel->getChildren();
    for(Node* puzzleNode : puzzleList){
        mPuzzleNodeQueue.push(puzzleNode);
    }
    
    
	mRetryButton = (Button *) lowerPanel->getChildByName("retryButton");
	mCancelButton = (Button *) lowerPanel->getChildByName("exitButton");
	mReviveButton = (Button *) lowerPanel->getChildByName("onemoreButton");
	mShareButton = (Button *) lowerPanel->getChildByName("shareButton");
//    mDoubleStarButton = (Button*) upperPanel->getChildByName("collectedCoinsLabel")->getChildByName("starBtn");
//	
//	int bonusStar = GameWorld::instance()->getBonusStar();
//	log("debug: bonusStar: value=%d", bonusStar);
//    if(bonusStar > 0){
//        setBonusStarListener(bonusStar);
//    }else{
//        mDoubleStarButton->setVisible(false);
//    }
	
	// Next Unlock Message
//	nextUnlockMsg = upperPanel->getChildByName("nextUnlockMsg");
	
	// Setting the button listener
	if(mReviveButton) {
		mReviveButton->addClickEventListener([&](Ref *){
			handleRevive();
		});
	}
	
	// Mark the y position of the buttons
	mButtonY1 = (mReviveButton == nullptr) ? 0 : mReviveButton->getPositionY();
	mButtonY2 = (mRetryButton == nullptr) ? 0 : mRetryButton->getPositionY();
}

void GameOverDialog::setupRewardPanel()
{
    int collectedCoins = GameWorld::instance()->getGameResult()->getStarCollected();
    if(collectedCoins <= 0 && mPuzzleList.empty()){
        mRewardPanel->setVisible(false);
        return;
    }
    setupCollectedCoins(collectedCoins);
    setupPuzzleUI();
}

void GameOverDialog::setupCollectedCoins(int coins)
{
    if(coins<=0){
        mCollectedCoinsPanel->setVisible(false);
        return;
    }
    
    mCollectedCoinsText->setString(StringUtils::format("%d",coins));
}

void GameOverDialog::setupPuzzleUI()
{
    for(int puzzleID : mPuzzleList){
		
		if(mPuzzleNodeQueue.empty()) {
			break;
		}
		if(puzzleID == 0) {
			continue;
		}
		
        Node* puzzleNode = mPuzzleNodeQueue.front();
		
		
		
        mPuzzleNodeQueue.pop();
        
//        if(puzzleID > 4){
//            puzzleID = 4; //hardcode now since not enough fragment!
//        }
        
        Sprite* fragmentSprite = Sprite::create(StringUtils::format("fragment/ui_fragment_%02d.png",puzzleID));
        fragmentSprite->setScale(0.5);
        fragmentSprite->setVisible(false);
        puzzleNode->addChild(fragmentSprite);
        
        AnimeNode* animeNode = AnimeNode::create();
        animeNode->setup("item_result_puzzle.csb");
        animeNode->setStartAnime("idle",true,false);
        animeNode->setFrameEventCallback([=](EventFrame* event){
            if(event->getEvent() == "show"){
                fragmentSprite->setVisible(true);
                puzzlePopUp();
            }
            
            if(event->getEvent() == "explode"){
                GameSound::playSound(GameSound::PuzzleExplode);
            }
            
        });
        puzzleNode->addChild(animeNode);
        
        mPuzzleDataQueue.push(puzzleID);
        mPuzzleAnimeNodeQueue.push(animeNode);
    }
}

void GameOverDialog::setPuzzleList(std::vector<int> puzzleList)
{
    mPuzzleList = puzzleList;
}

void GameOverDialog::puzzlePopUp(){
    if(mPuzzleDataQueue.empty() || mPuzzleAnimeNodeQueue.empty()){
        mLowerPanel->setVisible(true);
        return;
    }
    
    int puzzleID = mPuzzleDataQueue.front();
    mPuzzleDataQueue.pop();
    
//    if(puzzleID > 4){ // hardcode now since not enough fragment
//        puzzleID = 4;
//    }
    
    AnimeNode* puzzleAnimeNode = mPuzzleAnimeNodeQueue.front();
    mPuzzleAnimeNodeQueue.pop();
    
    puzzleAnimeNode->playAnimation("consume", false, true);
    
}


void GameOverDialog::updateEndGameResultData()
{
    PlayerGameResult* result = GameWorld::instance()->getGameResult();
    
    int money = result->getDeliveryScore();
//    int bonus = NPCManager::instance()->getNoCrashScore();
    int stageClear = result->getClearStageScore();
    int swipeOutScore = result->getKillScore();
    int bonusScore = result->getBonusScore();
    int collectedCoins = result->getStarCollected();
    int totalScore = result->getTotalScore();
	
	mDeliveryScoreText->setVisible(true);
    AnimationHelper::popTextEffect(this, mDeliveryScoreText, 0, money, [=]{
        mClearScorePanel->setVisible(true);
        AnimationHelper::popTextEffect(this, mClearScoreText, 0, stageClear, [=]{
            mKillScorePanel->setVisible(true);
 //           mTotalScorePanel->setVisible(true);
             AnimationHelper::popTextEffect(this, mKillScoreText, 0, swipeOutScore, [=]{
                  mBonusScorePanel->setVisible(true);
                  AnimationHelper::popTextEffect(this, mBonusScoreText, 0, bonusScore, [=]{

					  
					  
                    mTotalScorePanel->setVisible(true);
                    AnimationHelper::popTextEffect(this, mTotalScoreText, 0, totalScore, [=]{
                        this->puzzlePopUp();
                          
                    }, 0.3, 1.0);
                      
                  }, 0.3, 1.0);

            }, 0.3, 1.0);
            
        }, 0.3, 1.0);
        
    }, 0.3, 1.0);
    
    
    
//    mBonusText->setString(StringUtils::format("%d",bonus));
//    mTipText->setString(StringUtils::format("%d",tips));
//    mClientText->setString(StringUtils::format("%dx%d",clientCount,clientScore));
//    mScoreText->setString(StringUtils::format("%d",totalScore));
}

void GameOverDialog::onWatchVideoForBonusStarDone()
{
	GameWorld::instance()->collectCoin(mBonusStarAmount);
	GameWorld::instance()->updateCoins(false);
	
	updateCoinsGet(true);
	
	mDoubleStarButton->setVisible(false);		// hide the StarButton
	
	Analytics::instance()->logCollect(Analytics::bonus_star, mBonusStarAmount);
}

void GameOverDialog::setBonusStarListener(int bonusStar)
{
    if(mDoubleStarButton == nullptr) {
		 log("mDoubleStarButton is null");
		return;
	}
	
	mBonusStarAmount = bonusStar;
	
	mDoubleStarButton->addClickEventListener([&](Ref*){
		// click sound
		GameSound::playSound(GameSound::Sound::Button1);
			
		
		// Callback handling
		AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
			onWatchVideoForBonusStarDone();
		});
            
		AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
                
		});
            
		bool isOkay = AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
            
		if(isOkay == false) {
			float y = Director::getInstance()->convertToGL(Vec2(0, 80)).y;
			ViewHelper::showFadeAlert(this, "Fail to play video, try again!", (int) y);
		}
	});
	
}

void GameOverDialog::setNPCRating(Vector<NPCRating*> ratingList)
{
    std::vector<NPCRatingItemView*> itemList{};
    for(NPCRating* rating : ratingList){
        NPCRatingItemView* itemView = NPCRatingItemView::create();
        itemView->setItemView(rating);
        itemList.push_back(itemView);
    }
    
    mGalleryView->setNodeList(itemList);
    Node* galleryPanel = mPanelDialog->getChildByName("npcRatingPanel");
    galleryPanel->addChild(mGalleryView);
}

void GameOverDialog::setOKListener(const Widget::ccWidgetClickCallback &callback)
{
	if(mRetryButton) {
		mRetryButton->addClickEventListener(callback);
	} else {
		log("mOKButton is null");
	}
}

void GameOverDialog::setCancelListener(const Widget::ccWidgetClickCallback &callback)
{
	if(mCancelButton) {
		mCancelButton->addClickEventListener(callback);
	} else {
		log("mCancelButton is null");
	}
}

void GameOverDialog::setShareListener(const Widget::ccWidgetClickCallback &callback)
{
	if(mShareButton) {
		mShareButton->addClickEventListener(callback);
	} else {
		log("mShareButton is null");
	}
}

void GameOverDialog::setReviveListener(const Widget::ccWidgetClickCallback &callback)
{
	mReviveCallback = callback;
}

void GameOverDialog::updateDistance()
{
	
	int travelledDistance = GameWorld::instance()->getPlayerDistance();
	updateDistanceText(mDistanceText, travelledDistance);

	// Always send the score because the leaderboard contain "Today" / "Week" / "All Time"
    
//	GameManager::instance()->getGameCenterManager()->sendBestDistance(lastScore);

//	int planetID = PlanetManager::instance()->getSelectedPlanet();
//	int bestScore = PlanetManager::instance()->getBestDistance(planetID);
//						//GameManager::instance()->getPlayerRecord()->getPlanetRecord(
//						//				PlayerRecord::KeyBestDistance, planetID);
//	
//	updateDistanceText(mBestScoreText, bestScore);
//	
//	log("debug: newBest=%d lastBest=%d", bestScore, mLastBestDistance);
	
//	bool isNewRecord = bestScore > mLastBestDistance;
//	setNewRecordEffect(isNewRecord);
}

void GameOverDialog::setLeaderboardGUI(AstrodogLeaderboardType type)
{
    mLBDialog->setLeaderboardGUI(type);
    mCurrentLeaderBoardType = type;
}

void GameOverDialog::updateScore()
{
    if(mTotalScoreText == nullptr){
        return;
    }
    
    int score = GameWorld::instance()->getPlayerScore();
    std::string scoreStr = StringUtils::format("%d", score);
    mTotalScoreText->setString(scoreStr);
    
    GameManager::instance()->getUserData()->updateScore(score);
}

void GameOverDialog::updateCandyGet(bool hasAnimation)
{
    /*
    if(mCollectedCandyText == nullptr){
        return;
    }
    int candyGetted = GameWorld::instance()->getPlayerCollectedCandy();
    if(hasAnimation){
        AnimationHelper::popTextEffect(this, mCollectedCandyText, candyGetted/2, candyGetted);
    }else{
        char temp[100];
        sprintf(temp, "%d", candyGetted);
        mCollectedCandyText->setString(temp);
    }
     */
}

void GameOverDialog::updateCoinsGet(bool hasAnimation)
{
    int coinsGetted = GameWorld::instance()->getPlayerCollectedCoins();
    if(mCollectedCoinsText == nullptr) {
        return;
    }
	
    if(hasAnimation){
        AnimationHelper::popTextEffect(this, mCollectedCoinsText, coinsGetted/2, coinsGetted);
    }else{
        char temp[100];
        sprintf(temp, "%d", coinsGetted);
        mCollectedCoinsText->setString(temp);
    }
    
}


void GameOverDialog::updatePlayerModel()
{
	if(! mPlayerModelNode) {
		return;
	}

	GameModel *model = GameModel::create();
	model->setup(GameRes::getSelectedCharacterCsb());
    model->setAction(GameModel::Action::Climb);
	model->setScale(1.5);
    model->setFace(false);
	mPlayerModelNode->addChild(model);
	
}

void GameOverDialog::handleRevive()
{
	mIsReviveRewarded = false;
	

	auto parent = this;
	
	
	bool debugRevive = false;		// ken: just use if you don't show the revive video!
    
    
    if(debugRevive)
    {
		if(mReviveCallback) {
			mReviveCallback(this);
		}
		return;
	}
	
	bool isOkay = AdManager::instance()->playVideoAd(AdManager::Ad::AdGameContinue,
							[&](bool isOkay){
								if(mReviveCallback) {
										mReviveCallback(this);
								}
	});
	
	
  	
//	// Android and iOS ad colony behaviour
//	//	in IOS, sequence is rewarded -> finished
//	//	in Android, sequence is finished -> rewarded
//	
//	// Note: We can use Reward for Revive, because it has a daily cap!!
//	// Show Video Ad
//	AdManager::instance()->setVideoFinishedCallback([&, parent](bool isFinished){
//		if(mReviveCallback) {
//			mReviveCallback(this);
//		}
//	});
//	
//	AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
//		
//	});
//	
//	bool isOkay = AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
	
	if(isOkay == false) {
		float y = Director::getInstance()->convertToGL(Vec2(0, 80)).y;
		ViewHelper::showFadeAlert(parent, "Fail to play video, try again!", (int) y);
	}
	
    // logging
    Analytics::instance()->logScreen(Analytics::Screen::revive_video);
}


void GameOverDialog::hideReviveButton()
{
	// Hidden the button
	if(mReviveButton) {
		mReviveButton->setVisible(false);
		mReviveButton->setEnabled(false);
	}
	
	
	// Move the Retry button up
	Button *buttons[3] = {mRetryButton, mCancelButton, mShareButton};
	for(int i=0; i<3; i++) {
		Button *button = buttons[i];
		if(button) {
			button->setPositionY(mButtonY1);
		}
	}
		
	
	
}

void GameOverDialog::updateUnlockHint()
{
	std::string info = DogManager::instance()->getNextUnlockHint();

	setUnlockHint(info);
}

void GameOverDialog::setUnlockHint(const std::string &msg)
{
	if(nextUnlockMsg == nullptr) {
		return;
	}
	
	
	if(msg == "") {
		nextUnlockMsg->setVisible(false);
		return;
	}
	
	nextUnlockMsg->setVisible(true);
	
	Text *msgText = nextUnlockMsg->getChildByName<Text *>("msgText");
	
	if(msgText) {
		msgText->setString(msg);
	}
	//
}


#pragma mark - Leaderboard
void GameOverDialog::uploadLeaderboardScore()
{
	int score = GameWorld::instance()->getPlayerScore();
	int planet = PlanetManager::instance()->getSelectedPlanet();
	
	AstrodogClient::instance()->submitScore(planet, score);
	
	
}

void GameOverDialog::loadLeaderboard(AstrodogLeaderboardType type)
{
	int planet = PlanetManager::instance()->getSelectedPlanet();
	AstrodogClient::instance()->getRanking(type, planet);
    if(mLBDialog){
        mLBDialog->showLoading();
    }
}

void GameOverDialog::updateLeaderboardRecords(AstrodogRankStatus rankStatus)
{
	AstrodogLeaderboardType type = mCurrentLeaderBoardType;
	
	int planet = PlanetManager::instance()->getSelectedPlanet();

	AstrodogLeaderboard *board = AstrodogClient::instance()->getLeaderboard(type, planet);
	if(board == nullptr) {
		log("GameOverDialog: Fail to get the leaderboard");
		return;
	}
	
	// log("rankStatus: status=%d", rankStatus);
	
	// TODO update the GUI
	mLBDialog->updateWithLeaderboard(board,planet);
	mLBDialog->updateRankStatus(rankStatus);
	
	//
//	log("Leaderboard: recordCount=%ld", board->recordList.size());
//	for(AstrodogRecord *record : board->recordList) {
//		log("  %s", record->toString().c_str());
//	}
}

#pragma mark - AstrodogClient
// Listener
void GameOverDialog::onGetRanking(int status, AstrodogLeaderboardType type, int planet,
								  const std::map<std::string, int> &metaData)
{
	// stopLoading
	if(status != STATUS_SUCCESS) {
		return;
	}
    
    if(type != mCurrentLeaderBoardType){
        return;
    }
	//
	// updateLeaderboard(type, planet);
	int value = metaData.at("rankStatus");
	AstrodogRankStatus rankStatus = (AstrodogRankStatus) value;
	updateLeaderboardRecords(rankStatus);
}

void GameOverDialog::onSubmitScore(int status)
{
    if(status!=STATUS_ERROR){
        loadLeaderboard(AstrodogLeaderboardCountry);
    }
}

void GameOverDialog::onFBConnect(int status)
{
    if(status!=STATUS_SUCCESS){
        return;
    }
    
    loadLeaderboard(mCurrentLeaderBoardType);
}



#pragma mark - Score Animation
void GameOverDialog::hideAllPopText()
{
	//mMoneyPanel->setVisible(false);
	mClearScorePanel->setVisible(false);
	mDeliveryScorePanel->setVisible(false);
	mCollectScorePanel->setVisible(false);
	mDistanceScorePanel->setVisible(false);
	mKillScorePanel->setVisible(false);
	mBonusScorePanel->setVisible(false);
	mTotalScorePanel->setVisible(false);
	
	

}

void GameOverDialog::popScoreText(ScoreType scoreType, const std::function<void()> &callback)
{
//	void AnimationHelper::popTextEffect(Node* node,cocos2d::ui::Text *text, int originValue, int finalValue,
//										std::function<void()> callback,
//										float finishTime, float targetScale, std::string displayFormat)
//	{

	
	Text *text = getScoreText(scoreType);
	
	if(text == nullptr) {
		callback();
	}
	
	Node *panel = getScorePanel(scoreType);
	
	if(panel == nullptr) {
		callback();
	}

	
	int score = getScoreValue(scoreType);
	
	panel->setVisible(true);
	
    AnimationHelper::popTextEffect(this, text, 0, score, callback, 0.4, 1.0, "%d", GameSound::ResultScore);
}

int GameOverDialog::getScoreValue(ScoreType scoreType)
{
	PlayerGameResult* result = GameWorld::instance()->getGameResult();
	
	switch(scoreType) {
		case ScoreTypeCollect		: return result->getCollectScore();
		case ScoreTypeAllClear		: return result->getClearStageScore();
		case ScoreTypeDistance		: return result->getDistanceScore();
		case ScoreTypeDelivery		: return result->getDeliveryScore();
		case ScoreTypeKill			: return result->getKillScore();
		case ScoreTypeBonus			: return result->getBonusScore();
		case ScoreTypeTotal			: return result->getTotalScore();
		default						: return 0;
	}
}

Node *GameOverDialog::getScorePanel(ScoreType scoreType)
{
	switch(scoreType) {
		case ScoreTypeCollect		: return mCollectScorePanel;
		case ScoreTypeAllClear		: return mClearScorePanel;
		case ScoreTypeDistance		: return mDistanceScorePanel;
		case ScoreTypeDelivery		: return mDeliveryScorePanel;
		case ScoreTypeKill			: return mKillScorePanel;
		case ScoreTypeBonus			: return mBonusScorePanel;
		case ScoreTypeTotal			: return mTotalScorePanel;
		default						: return nullptr;
	}
}
Text *GameOverDialog::getScoreText(ScoreType scoreType)
{
	switch(scoreType) {
		case ScoreTypeCollect		: return mCollectScoreText;
		case ScoreTypeAllClear		: return mClearScoreText;
		case ScoreTypeDistance		: return mDistanceScoreText;
		case ScoreTypeDelivery		: return mDeliveryScoreText;
		case ScoreTypeKill			: return mKillScoreText;
		case ScoreTypeBonus			: return mBonusScoreText;
		case ScoreTypeTotal			: return mTotalScoreText;
		default						: return nullptr;
	}
}

void GameOverDialog::displayNextScore()
{
	// Check agains the displayList
	
	// if empty, stop the recursion and show the puzzle
	if(mScoreDisplayList.size() == 0) {
		puzzlePopUp();
		return;
	}
	
	ScoreType displayScoreType;
	aurora::DataHelper::dequeueVector(mScoreDisplayList, displayScoreType);
	
	popScoreText(displayScoreType, [&](){
		displayNextScore();
	});
}

void GameOverDialog::displayGameScores()
{
	mScoreDisplayList.clear();
	mScoreDisplayList.push_back(ScoreTypeDelivery);
	mScoreDisplayList.push_back(ScoreTypeAllClear);
//	mScoreDisplayList.push_back(ScoreTypeCollect);
	mScoreDisplayList.push_back(ScoreTypeDistance);
	mScoreDisplayList.push_back(ScoreTypeKill);
	mScoreDisplayList.push_back(ScoreTypeBonus);
	mScoreDisplayList.push_back(ScoreTypeTotal);
	
	displayNextScore();
}
