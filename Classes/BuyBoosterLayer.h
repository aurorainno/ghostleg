//
//  BuyBoosterLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 24/1/2017.
//
//

#ifndef BuyBoosterLayer_hpp
#define BuyBoosterLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "CommonType.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class RichTextLabel;
class NotEnoughMoneyDialog;
class FBProfilePic;

class BuyBoosterLayer : public Layer
{
public:
    CREATE_FUNC(BuyBoosterLayer);
    
    BuyBoosterLayer();
    
    virtual bool init() override;
    virtual void onEnter() override;
    void setupUI(Node* rootNode);
    
    void setupView(BoosterItemType type);
    void setupNotEnoughDialog();
    void onAmountChange(int amountToAdd);
    void onPurchase();
    
    void updateTotalCoin();
    void setupFBProfile();
    void updateDiamond();
    
    void setPurchaseSuccessCallback(std::function<void(int)> callback);
    
    std::string getBoosterBgResNameByType(BoosterItemType type);
    std::string getBoosterIconResNameByType(BoosterItemType type);

	void logPurchase(MoneyType moneyType, int price, int amount);
    
private:
    BoosterItemType mMyItemType;
    int mCurrentAmount;
    int mOriginalPrice;
    MoneyType mMoneyType;
    
    Node* mMainPanel;
    Node* mTopPanel;
    Button *mNextBtn, *mPreviousBtn;
    Button* mCancelBtn;
    Button* mPurchaseBtn;
    Text* mInfoLabel;
    Node* mCountPanel;
    Text* mCountText;
    Text* mPriceText;
    Text* mItemName;
    Sprite* mItemIconSprite;
    Sprite *mStarSprite, *mDiamondSprite;
    NotEnoughMoneyDialog *mNotEnoughDialog;
    Text *mTotalCoinText;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    Node* mDiamondPanel;
    
    std::function<void(int)> mPurchaseSuccessCallback;
    
};


#endif /* BuyBoosterLayer_hpp */
