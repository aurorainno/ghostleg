//
//  NPCRatingItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 15/3/2017.
//
//

#include "NPCRatingItemView.h"
#include "NPCRating.h"
#include "NPCManager.h"

bool NPCRatingItemView::init()
{
    if(Layout::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("NPCRatingItemView.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}


void NPCRatingItemView::setupUI(cocos2d::Node *rootNode)
{
    setContentSize(rootNode->getContentSize());
 //   this->setAnchorPoint(Vec2(0.5,0.5));
    mRootNode = rootNode;
    
    mNpcSprite = rootNode->getChildByName<Sprite*>("npcSprite");
    mCommentText = rootNode->getChildByName<Text*>("commentText");
    mEmojiSprite = rootNode->getChildByName<Sprite*>("emojiSprite");
    
    mTimeText = rootNode->getChildByName<Text*>("timeText");
    mTimeUnit = rootNode->getChildByName<Text*>("timeUnit");

}

void NPCRatingItemView::setItemView(NPCRating *data)
{
    CCASSERT(data!=nullptr,"NPCRating is null!");
    
    int npcID = data->getNpcID();
    int rating = data->getRating();
    std::string comment = NPCManager::instance()->getRatingComment(rating);
    float timeUsed = data->getTimeUsed();
    
    std::string iconResName = StringUtils::format("npc/npc_icon_%03d.png",npcID);
    mNpcSprite->setTexture(iconResName);
    
    iconResName = StringUtils::format("Emoji/imgame_emoji_%d.png",rating);
    mEmojiSprite->setTexture(iconResName);
    
    mCommentText->setString(comment);
    
    mTimeText->setString(StringUtils::format("%.2f",timeUsed));
  
    mTimeUnit->setPositionX(mTimeText->getPositionX() + mTimeText->getContentSize().width * mTimeText->getScaleX() + 5);
//    setRatingGUI(data->getRating());
}

void NPCRatingItemView::setRatingGUI(int rating)
{
    int count = rating;
    Node* ratingPanel = mRootNode->getChildByName("ratingPanel");
    for(Node* node : ratingPanel->getChildren()){
        Sprite* starSprite = dynamic_cast<Sprite*>(node);
        if(!starSprite){
            continue;
        }
        
        std::size_t found = starSprite->getName().find("star");
        if (found!=std::string::npos){
            bool isVisible = count > 0;
            starSprite->setVisible(isVisible);
            count--;
        }
    }
}


