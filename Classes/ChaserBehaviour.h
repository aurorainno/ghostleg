//
//  ChaserBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 2/5/2017.
//
//

#ifndef ChaserBehaviour_hpp
#define ChaserBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

class GameWorld;

class ChaserBehaviour : public EnemyBehaviour
{
public:
    
    CREATE_FUNC(ChaserBehaviour);
    
    ChaserBehaviour();
    
    bool init() { return true; }
    
    std::string toString();

public:	// property
	CC_SYNTHESIZE(int, mInitialSpeed, InitialSpeed);
	CC_SYNTHESIZE(int, mMaxSpeed, MaxSpeed);
	CC_SYNTHESIZE(bool, mInitWithPlayerSpeed, InitWithPlayerSpeed);
	CC_SYNTHESIZE(int, mAcceleration, Acceleration);
	
	
	CC_SYNTHESIZE(int, mMinStunSpeed, MinStunSpeed);
	CC_SYNTHESIZE(int, mMaxStunSpeed, MaxStunSpeed);
	CC_SYNTHESIZE(int, mIncStunSpeed, IncStunSpeed);
	CC_SYNTHESIZE(float, mSpeedRatio, SpeedRatio);
	CC_SYNTHESIZE(float, mBrakeSpeedRatio, BrakeSpeedRatio);
	
public:
    virtual void onCreate();
    virtual void onVisible();
	virtual void onInActive();
    virtual void onActive();
    virtual void onUpdate(float delta);
	virtual bool shouldInActive();
    
    bool onCollidePlayer(Player *player);
    
    void updateProperty();
	void increaseSpeedThreshold();	
	
private:
	void setupSpeed();
	void updateSpeed(float delta);
	void updateSpeedWithPlayer(float delta);
	float adjustYPosition(float y, float playerY);
	
	
	
private:
    bool mStartChasing;
	bool mReachMax;				// Already reach the max speed
//    float mAccelY;
    float mSpeedY;
//    float mMaxSpeedY;
	float mInitialMaxSpeed;		// MIN(playerMax and MaxSpeed)
	float mSpeedThreshold;
	float mSpeedWhenStun;
	bool mUsingStunSpeed;
	
};

#endif /* ChaserBehaviour_hpp */
