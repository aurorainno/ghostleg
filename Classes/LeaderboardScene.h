//
//  LeaderboardLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 19/1/2017.
//
//

#ifndef LeaderboardScene_hpp
#define LeaderboardScene_hpp


#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "AstrodogClient.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class FBProfilePic;
class LeaderboardItemView;

class LeaderboardLayer : public Layer, public AstrodogClientListener
{
public:
    enum DialogMode{
        LayerMode = 0,
        SceneMode
    };
    
    static Scene *createScene();
    
    CREATE_FUNC(LeaderboardLayer);
	
	CC_SYNTHESIZE(DialogMode, mMode, DialogMode);
    
    LeaderboardLayer();
    
    virtual bool init();
    virtual void onEnter();
    virtual void update(float delta) override;
    
    void setupUI();
    
    void loadLeaderboard(AstrodogLeaderboardType type);
    void updateLBListView(Vector<AstrodogRecord *> recordList);
    
    void setupRankStatus(int newRank, LeaderboardItemView* itemView);
    
    void onLBTypeChangeGUIUpdate();
    void updateBtnAndBorderGUI(AstrodogLeaderboardType type);
    void setupBanner(AstrodogLeaderboardType type);
    
    void updatePlanetBtnGUI(int selectedPlanet);
    void onPlanetSelected(int planetID);
    
    void updateDiamond();
    void updateStars();
    
    void setupFbProfile();
    void onFbBtnClicked();
    
    void startLoading();
    ListView *getListView();
    
    void updateLeaderboardRecords();
    void updateLeaderboardRemainTime();
    
    
    void setCloseCallback(std::function<void()> callback);
	void showInviteDialog();
#pragma mark - AstrodogClient
public:	// AstrodogClientListener
    virtual void onGetRanking(int status, AstrodogLeaderboardType type, int planet,
							  const std::map<std::string, int> &metaData);
    virtual void onFBConnect(int status);
    
private:
    Node *mRootNode;
    
    Button *mCancelBtn;
    
    Button *mFbConnectBtn;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
    Button *mFriendBtn, *mNationalBtn, *mGlobalBtn;
    Node *mFriendBorder, *mNationalBorder, *mGlobalBorder;
    
    Text *mLoadingText;
    
    ListView *mListView;
    
    Sprite* mBannerBg;
    Sprite* mBannerDiamondSprite;
    Button *mBannerBtn, *mBannerFbBtn;
    Text* mBannerText;
    
    Text* mStarText;
    Text* mDiamondText;
    
    Node* mCounterPanel;
    Text *mFirstValue, *mFirstUnit, *mSecondValue;
    
    AstrodogLeaderboardType mCurrentLBType;
    
    std::function<void()> mCloseCallback;
    
	
    
    int mCurrentPlanet;
    long long mLeaderboardEndTime;
    bool mIsStartCounting;
    float mAccumTime;
    
};

#endif /* LeaderboardScene_hpp */
