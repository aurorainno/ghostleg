//
//  SnowBall.hpp
//  GhostLeg
//
//  Created by Calvin on 7/12/2016.
//
//

#ifndef SnowBall_hpp
#define SnowBall_hpp

#include <stdio.h>
#include "Item.h"
#include "Enemy.h"
#include "RouteMoveLogic.h"

//class
//MapLine *line = MapLine::create(MapLine::eTypeSlope, 25, 125, 100, 200);
class MapLine;

class SnowBall : public Item
{
public:
    enum Action {
        Start,
        Loop,
        Destroy
    };
    
#pragma mark - Public static Method
    CREATE_FUNC(SnowBall);
    
#pragma mark - Public Method and Properties
    SnowBall();
    ~SnowBall();
    
    CC_SYNTHESIZE(float, mVelocity, Velocity);
    CC_SYNTHESIZE(float, mAcceler, Acceler);
	CC_SYNTHESIZE(float, mMaxVelocity, MaxVelocity);
    CC_SYNTHESIZE(float, mDuration, Duration);
    
    virtual bool init();	// virtual because different derived class have different init logic
    virtual bool use();
    virtual void update(float delta);
	
	void setupMoveLogic();
	void setupMoveLogic(Dir dir, MapLine *currentLine);
    
    void setAction(SnowBall::Action action);
    void setAnimationEndCallFunc(SnowBall::Action action, std::function<void()> func);
	virtual bool isWeapon();
	
private:
	CC_SYNTHESIZE_RETAIN(RouteMoveLogic *, mMoveLogic, MoveLogic);
    
private:
    bool shouldRemove();
    void handleCollision();
	void updateVelocity(float delta);
	
	
    bool isDestoryed;


};

#endif /* SnowBall_hpp */
