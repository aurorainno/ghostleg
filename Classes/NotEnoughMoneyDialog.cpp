//
//  NotEnoughMoneyDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 1/2/2017.
//
//

#include "NotEnoughMoneyDialog.h"
#include "CommonMacro.h"
#include "CoinShopScene.h"
#include "LuckyDrawDialog.h"

NotEnoughMoneyDialog::NotEnoughMoneyDialog()
: mAlreadyInShop(false)
, mRedirectByScene(true)
, mAutoremove(true)
, mCallback(NULL)
{
}

bool NotEnoughMoneyDialog::init()
{
    if(!Layer::init()){
        return false;
    }
    
    Node* rootNode = CSLoader::createNode("gui/NotEnoughMoneyDialog.csb");
    setupUI(rootNode);
    addChild(rootNode);
	mAlreadyInShop = false;
    
    return true;
}

void NotEnoughMoneyDialog::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    Node *mainPanel = rootNode->getChildByName("mainPanel");
    
    mShopBtn = mainPanel->getChildByName<Button*>("shopBtn");
    mDrawBtn = mainPanel->getChildByName<Button*>("drawBtn");
    mCancelBtn = mainPanel->getChildByName<Button*>("cancelBtn");
    
    mStarSprite = mainPanel->getChildByName<Sprite*>("starSprite");
    mDiamondSprite = mainPanel->getChildByName<Sprite*>("diamondSprite");
    mPuzzleSprite = mainPanel->getChildByName<Sprite*>("puzzleSprite");

    mShopBtn->addClickEventListener([=](Ref*){
		if(isAlreadyInShop()) {
			this->removeFromParent();
			return; // No need to the ShopScene
		}
		redirectToShop();
    });
    
    mDrawBtn->addClickEventListener([=](Ref*){
        redirectToLuckDraw();
    });
    
    mCancelBtn->addClickEventListener([=](Ref*){
        if(mAutoremove)
            this->removeFromParent();
        else
            this->setVisible(false);
    });
	
	// Calculate SingleButton Position
	Vec2 buttonPos = mShopBtn->getPosition();
	buttonPos.y = (mShopBtn->getPositionY() + mCancelBtn->getPositionY())/2;
	mSingleButtonPos = buttonPos;
}

void NotEnoughMoneyDialog::setMode(NotEnoughMoneyDialog::DialogMode mode)
{
    switch (mode) {
        case StarMode:{
            mPuzzleSprite->setVisible(false);
            mDiamondSprite->setVisible(false);
            mStarSprite->setVisible(true);
            mDrawBtn->setVisible(false);
            mShopBtn->setVisible(true);
            break;
        }
            
        case DiamondMode:{
            mPuzzleSprite->setVisible(false);
            mDiamondSprite->setVisible(true);
            mStarSprite->setVisible(false);
            mDrawBtn->setVisible(false);
            mShopBtn->setVisible(true);
            break;
        }
            
        case PuzzleMode:{
            mPuzzleSprite->setVisible(true);
            mDiamondSprite->setVisible(false);
            mStarSprite->setVisible(false);
            mDrawBtn->setVisible(true);
            mShopBtn->setVisible(false);
            break;
        }
            
        default:
            break;
    }
}

void NotEnoughMoneyDialog::hideShopButton()
{
    mShopBtn->setVisible(false);
}


void NotEnoughMoneyDialog::redirectToShop()
{
	if(mRedirectByScene) {
		auto scene = CoinShopSceneLayer::createScene();
		Director::getInstance()->pushScene(scene);
		
        if(mAutoremove)
            removeFromParent();
        
	} else {
		Node *parent = getParent();
		if(parent == nullptr) {
			log("NotEnoughMoneyDialog: parent is null");
		} else {
			CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
			layer->setIsDialogMode(true);
            if(!mAutoremove){
                layer->setCloseCallback([=]{
                    if(mCallback){
                        mCallback();
                    }
                });
            }
			parent->addChild(layer);
		}
        
        if(mAutoremove)
            removeFromParent();
	}
}

void NotEnoughMoneyDialog::redirectToLuckDraw()
{
    Node *parent = getParent();
    if(parent == nullptr) {
        log("NotEnoughMoneyDialog: parent is null");
    } else {
        LuckyDrawDialog *layer = LuckyDrawDialog::create();
        if(!mAutoremove){
            layer->setOnExitCallback([=]{
                if(mCallback){
                    mCallback();
                }
            });
        }
//        layer->setIsDialogMode(true);
        parent->addChild(layer);
    }
    
    if(mAutoremove)
        removeFromParent();
}

void NotEnoughMoneyDialog::setCallbackAfterRedirect(std::function<void ()> callback)
{
    mCallback = callback;
}
