//
//  UIBestDistance.cpp
//  GhostLeg
//
//  Created by Calvin on 2/12/2016.
//
//

#include "UIBestDistance.h"
#include "PlanetManager.h"

UIBestDistance::UIBestDistance(){}

bool UIBestDistance::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Node::init())
    {
        return false;
    }
    
    // Setup UI
    std::string csb = "UI_Distance.csb";
    setupGUI(csb);
    setupAnimation(csb);
    
    return true;
}

void UIBestDistance::setupGUI(const std::string &csbName)
{
    Node *rootNode = CSLoader::createNode(csbName);
    addChild(rootNode);
    
    mBestDistanceText = rootNode->getChildByName<Text*>("distance value");
    
}

void UIBestDistance::setupAnimation(const std::string &csbName)
{
    cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
    runAction(timeline);		// retain is done here!
    
    mTimeLine = timeline;
}

void UIBestDistance::runAnimation()
{
    if(mTimeLine) {
        mTimeLine->play("active", false);
    }
}

void UIBestDistance::setupBestDistanceText(int planetID)
{
    int bestDistance = PlanetManager::instance()->getBestDistance(planetID);
    std::string bestDistanceStr = StringUtils::format("%dm",bestDistance);
    
    mBestDistanceText->setString(bestDistanceStr);
}
