//
//  NPCOrderItemView.hpp
//  GhostLeg
//
//  Created by Calvin on 13/3/2017.
//
//

#ifndef NPCOrderItemView_hpp
#define NPCOrderItemView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "AstrodogClient.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NPCOrder;

class NPCOrderItemView : public Layout
{
public:
    CREATE_FUNC(NPCOrderItemView);
    virtual bool init();
	
	NPCOrderItemView();
	~NPCOrderItemView();
	
    void setupUI(Node* rootNode);
    
    void setNPCOrder(NPCOrder* data);
    
    void setCallback(std::function<void(NPCOrder*)> callback);
	
	//virtual void
    
private:
    void setRatingGUI(int difficulty);
	void updateFragmentDetail(NPCOrder *orderData);
	
private:
    Layout *mTouchLayer;
    Node *mRootNode;
    Sprite* mIcon;
    Text* mDistanceText;
    Text* mTimerText;
    Text* mRewardText;
    
    Text* mDistanceUnitText;
    Text* mTimerUnitText;
    Text* mBonusRewardText;
    Text* mBonusTimeText;
    
    Node* mFragPanel;
    Sprite* mCharFragmentSprite;
    Text* mCollectedFragText;
    Text* mRewardFragText;
    LoadingBar* mProgressBar;
    
    std::function<void(NPCOrder*)> mCallback;
    
    NPCOrder *mNPCOrder;
};


#endif /* NPCOrderItemView_hpp */
