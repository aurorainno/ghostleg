//
//  NPCOrderItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 13/3/2017.
//
//

#include "NPCOrderItemView.h"
#include "NPCOrder.h"
#include "StringHelper.h"
#include "AnimationHelper.h"
#include "PlayerManager.h"

#define kOriDistanceUnitPosX 110
#define kOriTimeUnitPosX 205
#define kOriBonusTimePosX 233
#define kOriBonusRewardPosX 130

bool NPCOrderItemView::init()
{
    if(Layout::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("NPCOrderItemView.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

NPCOrderItemView::NPCOrderItemView()
: mNPCOrder(nullptr)
{
	
}

NPCOrderItemView::~NPCOrderItemView()
{
	CC_SAFE_RELEASE_NULL(mNPCOrder);
}

void NPCOrderItemView::setupUI(cocos2d::Node *rootNode)
{
    setContentSize(rootNode->getContentSize());
    this->setAnchorPoint(Vec2(0.5,0.5));
    mRootNode = rootNode;
    
    mIcon = rootNode->getChildByName<Sprite*>("npcSprite");
    mDistanceText = rootNode->getChildByName<Text*>("distanceText");
    mTimerText = rootNode->getChildByName<Text*>("timeText");
    mRewardText = rootNode->getChildByName<Text*>("rewardText");
    
    mDistanceUnitText = rootNode->getChildByName<Text*>("distanceUnitText");
    mTimerUnitText = rootNode->getChildByName<Text*>("timeUnitText");
    
    mBonusRewardText = rootNode->getChildByName<Text*>("bonusRewardText");
    mBonusTimeText = rootNode->getChildByName<Text*>("bonusTimeText");
    
    mFragPanel = rootNode->getChildByName("charFragPanel");
    mCharFragmentSprite = mFragPanel->getChildByName<Sprite*>("charSmallIcon");
    mRewardFragText = mFragPanel->getChildByName<Text*>("rewardFragText");
    mCollectedFragText = mFragPanel->getChildByName<Text*>("collectedFragText");
    mProgressBar = mFragPanel->getChildByName<LoadingBar*>("progressBar");
    
    
    mTouchLayer = rootNode->getChildByName<Layout*>("touchPanel");
    mTouchLayer->addTouchEventListener([=](Ref*,TouchEventType type){
        switch (type) {
            case TouchEventType::BEGAN:{
                this->stopAllActions();
                this->setScale(1.1);
                break;
            }
                
            case TouchEventType::CANCELED:{
                this->setScale(1.0);
                AnimationHelper::setJellyEffect(this, 1, 1.02, 0);
                break;
            }
                
            case TouchEventType::ENDED:{
                if(mCallback){
                    mCallback(mNPCOrder);
                }
                //
				
				// ken: usually the GUI will be removed.
				// this->setScale(1.0);
                //AnimationHelper::setJellyEffect(this, 1, 1.02, 0);
                break;
            }
                
            default:
                break;
        }
    });
    
    AnimationHelper::setJellyEffect(this, 1, 1.02, 0);
}

void NPCOrderItemView::setNPCOrder(NPCOrder *data)
{
    CC_ASSERT(data!=nullptr);
    
    int npcID = data->getNpcID();
    int distance = data->getDistance();
    //int time = data->getTimeLimit();
    // int reward = data->getReward();
//    int diff = data->getDifficulty();
    
    mNPCOrder = data;
	CC_SAFE_RETAIN(mNPCOrder);
    
    std::string iconResName = StringUtils::format("npc/npc_icon_%03d.png",npcID);
    mIcon->setTexture(iconResName);
    
    int numOfDigit = floor(log10(distance)) + 1;
    mDistanceText->setString(StringUtils::format("%d",distance));
    mDistanceUnitText->setPositionX(mDistanceText->getPositionX() + mDistanceText->getContentSize().width * mDistanceText->getScaleX() + 5);
	
    int baseTime = data->getBaseTimeLimit();
    numOfDigit = floor(log10(baseTime)) + 1;
    
	mTimerText->setString(StringUtils::format("%d",baseTime));
    
    if(data->getBonusTimeLimit()>0){
        mBonusTimeText->setVisible(true);
        mBonusTimeText->setPositionX(kOriBonusTimePosX + (numOfDigit-1) * 16);
        mBonusTimeText->setString(StringUtils::format("%d",data->getBonusTimeLimit()));
    }else{
        mBonusTimeText->setVisible(false);
    }
    
    mTimerUnitText->setPositionX(mTimerText->getPositionX() + mTimerText->getContentSize().width * mTimerText->getScaleX() + 5);

    
    int baseReward = data->getBaseReward();
    numOfDigit = floor(log10(baseReward)) + 1;
    
	mRewardText->setString(StringUtils::format("%d", baseReward));
	
	//
    if(data->getBonusReward()>0){
        mBonusRewardText->setVisible(true);
        mBonusRewardText->setPositionX(kOriBonusRewardPosX + (numOfDigit-1) * 16);
        mBonusRewardText->setString(StringUtils::format("%d",data->getBonusReward()));
    }else{
        mBonusRewardText->setVisible(false);
    }
    
    mRewardFragText->setString(StringUtils::format("x %d",data->getFragReward()));
	

	// Show the fragment information
	updateFragmentDetail(data);
}

void NPCOrderItemView::updateFragmentDetail(NPCOrder *data)
{
	int charID = data->getFragChar();
	if(charID <= 0) {		// No rewarding fragment
		mFragPanel->setVisible(false);
		return;
	}
	
	
	PlayerCharProfile *profile = PlayerManager::instance()->getPlayerCharProfile(charID);
	if(profile == nullptr) {
		log("ERROR: NPCOrderItemView::updateFragmentDetail: profile is null. charID=%d", charID);
		
		mFragPanel->setVisible(false);
		return;
	}
	
	
	if(profile->isMaxLevel()) {		// No more fragment need if already MAX
		mFragPanel->setVisible(false);
		return;
	}
	
	
	mFragPanel->setVisible(true);
	
	float collectedFrag = profile->getCollectedFragCount();
	float requiredFrag = profile->getUpgradeFragCount();
	if(requiredFrag == 0) {	// prevent divide by zero
		requiredFrag = 1;
	}
	
	// To percentage
	float percent = collectedFrag * 100 / requiredFrag;
	if(percent > 100) {
		percent = 100;
	}
	
	
	mProgressBar->setPercent(percent);
	mCollectedFragText->setString(StringUtils::format("%.0f/%.f",
										collectedFrag, requiredFrag));
	
	mCharFragmentSprite->setTexture(StringUtils::format("fragment/ui_fragment_%02d.png",charID));	
}

void NPCOrderItemView::setRatingGUI(int difficulty)
{
    int count = difficulty;
    Node* ratingPanel = mRootNode->getChildByName("ratingPanel");
    for(Node* node : ratingPanel->getChildren()){
        Sprite* starSprite = dynamic_cast<Sprite*>(node);
        if(!starSprite){
            continue;
        }
        std::size_t found = starSprite->getName().find("star");
        if (found!=std::string::npos){
            bool isVisible = count > 0;
            starSprite->setVisible(isVisible);
            count--;
        }
    }
}

void NPCOrderItemView::setCallback(std::function<void (NPCOrder*)> callback)
{
    mCallback = callback;
}


