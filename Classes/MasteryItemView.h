//
//  MasteryItemView.h
//  GhostLeg
//
//  Created by Calvin on 17/5/2016.
//
//

#ifndef MasteryItemView_h
#define MasteryItemView_h

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>

#include "CommonType.h"
#include <MasteryItemView.h>
#include <Mastery.h>

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class MasteryItemView : public Layer {
public:
	typedef std::function<void(MasteryItemView *view, int masteryID, int level)> MasteryItemViewCallback;
	
	
    CREATE_FUNC(MasteryItemView);
	
	//
    virtual bool init();
	
	//
    void setMastery(MasteryData* mastery);
	void setMastery(MasteryData* mastery, int playerLevel);
	
	void setCallback(const MasteryItemViewCallback &callback);
	void updateUI();
    
    int getMasteryId();
    
private:
    void setupUI(Node *rootNode);
	
	Node *getVisibleUpgradePanel(int level, bool isMax);
	void notifyUpgrade();
	void setupUpgradeButton(Node *upgradePanel, bool isMaxLevel);
    
private:
    Text* mLevelText;
    Text* mTitleText;
    Text* mDescriptionText;
    Sprite* mIcon;
    Node *mNormalUpgradePanel,*mMaxUpgradePanel,*mUnlockUpgradePanel;
    Node *mUpgradePreviewUnlock,*mUpgradePreview;
	
	//
	int mMasteryID;
	int mPlayerLevel;
	MasteryItemViewCallback mCallback;
};


#endif /* MasteryItemView_h */
