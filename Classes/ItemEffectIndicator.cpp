//
//  ItemEffectIndicator.cpp
//  GhostLeg
//
//  Created by Calvin on 27/1/2017.
//
//

#include "ItemEffectIndicator.h"
#include "CircularMaskNode.h"
#include "CommonMacro.h"
#include "EffectLayer.h"
#include "GameWorld.h"
#include "Player.h"

ItemEffectIndicator::ItemEffectIndicator():
Layout(),
mRemainTime(0),
mOriTime(0),
mMyType(None),
mOnFinishCallback(NULL)
{
}

bool ItemEffectIndicator::init()
{
    if(Layout::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("ItemEffectIndicator.csb");
    setupUI(rootNode);
    addChild(rootNode);
    //
    setVisible(false);
    return true;
}

void ItemEffectIndicator::setupUI(Node* rootNode)
{
    setContentSize(rootNode->getContentSize());
    cocos2d::ui::Helper::doLayout(this);
    
    mIcon = rootNode->getChildByName<Sprite*>("icon");
    Node* coolDownNode = rootNode->getChildByName("coolDownNode");
    
    CircularMaskNode *node = CircularMaskNode::create();
    node->setPosition(Vec2(getContentSize().width/2,getContentSize().height/2));
    coolDownNode->addChild(node);
    
 //   Sprite *sprite = Sprite::create("guiImage/ingame_booster_2.png");
    //    sprite->setScale(0.5);
    //    sprite->setContentSize(sprite->getContentSize() * 0.5);
 //   node->addChild(sprite);
    
    mCoolDownMask = node;
    mCoolDownMask->updateAngleByPercent(100);

}

std::string ItemEffectIndicator::getIconResByType(ItemEffectIndicator::EffectType type)
{
    switch (type) {
        case TimeStop:
            return "guiImage/ingame_booster_2.png";
            
        case Magnet:
            return "guiImage/ingame_booster_1.png";
            
        case Shield:
            return "guiImage/ingame_booster_3.png";
        
        case SpeedUp:
            return "guiImage/ingame_booster_4.png";
            
        case CashOut:
            return "guiImage/ingame_booster_5.png";
        
        default:
            return "";
    }
}

std::string ItemEffectIndicator::getCoolDownResByType(ItemEffectIndicator::EffectType type)
{
    switch (type) {
        case TimeStop:
            return "guiImage/ingame_booster_2_1.png";
            
        case Magnet:
            return "guiImage/ingame_booster_1_1.png";
            
        case Shield:
            return "guiImage/ingame_booster_3_1.png";
            
        case SpeedUp:
            return "guiImage/ingame_booster_4_1.png";
            
        case CashOut:
            return "guiImage/ingame_booster_5_1.png";
            
        default:
            return "";
    }
}

void ItemEffectIndicator::config(EffectType type, float time)
{
    mMyType = type;
    mOriTime = time;
    mRemainTime = mOriTime;
    
    std::string iconName = getIconResByType(type);
    mIcon->setTexture(iconName);
    
    std::string cooldownName = getCoolDownResByType(type);
    Sprite* coolDownSprite = Sprite::create(cooldownName);
    coolDownSprite->setScale(0.5);
    coolDownSprite->setAnchorPoint(Vec2(0.5,0.5));
//    coolDownSprite->setContentSize(coolDownSprite->getContentSize() * 0.5);
    mCoolDownMask->addChild(coolDownSprite);
    
}

ItemEffectIndicator::EffectType ItemEffectIndicator::getEffectType()
{
    return mMyType;
}

void ItemEffectIndicator::activate()
{
    setVisible(true);
    mRemainTime = mOriTime;
    mCoolDownMask->updateAngleByPercent(100);
    scheduleUpdate();
}

void ItemEffectIndicator::update(float delta)
{
    // mRemainTime -= delta;
	mRemainTime = GameWorld::instance()->getPlayer()->getItemEffectRemainTime(mMyType);
	// log("Effect Indicator: remainTime=%f", mRemainTime);
	
    mCoolDownMask->updateAngleByPercent(mRemainTime/mOriTime*100.0);
    
    if(mRemainTime<=0){
        setVisible(false);
        unscheduleUpdate();
        if(mOnFinishCallback){
            mOnFinishCallback(this);
        }
    }
}
//
//bool ItemEffectIndicator::isMatchWithEffect(EffectLayer::BoosterType type)
//{
//    switch (type) {
//			
//        default:
//            break;
//    }
//}

bool ItemEffectIndicator::isMatchWithEffect(int type)
{
    switch (type) {
        case EffectLayer::SpeedUp:
            return mMyType == SpeedUp;
            
        case EffectLayer::Magnet:
            return mMyType == Magnet;
            
        case EffectLayer::CashOut:
            return mMyType == CashOut;
			
		case EffectLayer::Shield:
			return mMyType == Shield;
			
		case EffectLayer::TimeStop:
			return mMyType == TimeStop;

        default:
			return false;
    }
}


void ItemEffectIndicator::setOnFinishCallback(std::function<void(ItemEffectIndicator*)> callback)
{
    mOnFinishCallback = callback;
}




