//
//  MailItemView.hpp
//  GhostLeg
//
//  Created by Calvin on 17/2/2017.
//
//

#ifndef MailItemView_hpp
#define MailItemView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "AstrodogClient.h"

USING_NS_CC;
using namespace cocos2d::ui;

class RichTextLabel;

class MailItemView : public Layout
{
public:
    CREATE_FUNC(MailItemView);
    virtual bool init();
    
    void setupUI(Node* rootNode);
    
    void setView(AstrodogMail* data);

    void setOnReceiveCallback(std::function<void(int)> callback);
    
    CC_SYNTHESIZE(Sprite *, mLargeDiamond, LargeDiamond);
    CC_SYNTHESIZE(Sprite *, mLargeStar, LargeStar);
    
private:
    ImageView *mBGImageOn, *mBGImageOff;
    Sprite *mIcon;
    Text *mTitleText;
    RichTextLabel *mMessageText;
    Text *mRewardCountText;
    Sprite *mDiamondSprite , *mStarSprite, *mCharSprite;
    Button *mReceiveBtn, *mDoneBtn;
    Text* mTimeText;
    
    int mMailID;
    std::function<void(int)> mOnReceiveCallback;

};


#endif /* MailItemView_hpp */
