//
//  LeaderboardRewardManager.hpp
//  GhostLeg
//
//  Created by Calvin on 27/2/2017.
//
//

#ifndef LeaderboardRewardManager_hpp
#define LeaderboardRewardManager_hpp

#include <stdio.h>
#include "LeaderboardReward.h"
#include "cocos2d.h"

USING_NS_CC;

using namespace std;

class LeaderboardRewardManager
{
public: // Type and Enum
    enum LeaderboardRewardType{
        Local,
        Global
    };
    
    static LeaderboardRewardManager *instance();
    
private:
    LeaderboardRewardManager();
    ~LeaderboardRewardManager();
    
#pragma mark - General
public:
    void setup();
    
    map<LeaderboardRewardType,map<int,vector<LeaderboardReward*>>> getRewardMap();
    
private:
    void loadRewardData();
    
    map<int,vector<LeaderboardReward*>> mRewardMapWithPlanetID;
    map<LeaderboardRewardType,map<int,vector<LeaderboardReward*>>> mRewardMap;

};

#endif /* LeaderboardRewardManager_hpp */
