//
//  CharacterScrollView.hpp
//  GhostLeg
//
//  Created by Calvin on 16/2/2017.
//
//

#ifndef CharacterScrollView_hpp
#define CharacterScrollView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;

class CharacterScrollView : public ui::ScrollView
{
    CREATE_FUNC(CharacterScrollView);
    virtual bool init() override;
};


#endif /* CharacterScrollView_hpp */
