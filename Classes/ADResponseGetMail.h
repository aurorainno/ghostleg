//
//  ADResponseGetMail.hpp
//  GhostLeg
//
//  Created by Calvin on 20/2/2017.
//
//

#ifndef ADResponseGetMail_hpp
#define ADResponseGetMail_hpp

#include <stdio.h>
#include "AstrodogData.h"
#include "ADResponse.h"

class ADResponseGetMail : public ADResponse
{
public:
    CREATE_FUNC(ADResponseGetMail);
    
public:
    ADResponseGetMail();
    
    
    CC_SYNTHESIZE_RETAIN(AstrodogMailbox *, mMailbox, Mailbox);
    
    virtual std::string toString();
    
    virtual bool parseJSON(const rapidjson::Value &jsonValue);
};
#endif /* ADResponseGetMail_hpp */
