//
//  CharacterItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 14/2/2017.
//
//

#include "CharacterItemView.h"
#include "DogManager.h"
#include "DogData.h"
#include "AnimationHelper.h"
#include "PlayerCharProfile.h"
#include "PlayerManager.h"
#include "AnimeNode.h"
#include "GameRes.h"

bool CharacterItemView::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("CharacterItemView.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}


void CharacterItemView::setupUI(cocos2d::Node *rootNode)
{
    setContentSize(rootNode->getContentSize());
    
    mLockedPanel = rootNode->getChildByName("lockedPanel");
    mUnlockedPanel = rootNode->getChildByName("unlockedPanel");
    
    
    
    Button* selectBtn = mUnlockedPanel->getChildByName<Button*>("selectBtn");
    Node* animeRootNode = selectBtn->getChildByName("animeNode");
    
    AnimeNode *animeNode = AnimeNode::create();
    animeNode->setup("gui_character_select.csb");
    animeNode->setStartAnime("active", true, false);
    animeNode->setScale(2);
    animeRootNode->addChild(animeNode);
    
    Node* progressAnimeNode = mLockedPanel->getChildByName<LoadingBar*>("progressBar")->getChildByName("animeNode");
    
    AnimeNode *progressAnime = AnimeNode::create();
    progressAnime->setup("gui_character_bar.csb");
    progressAnime->setStartAnime("active", true, false);
    progressAnime->setScale(2);
    progressAnime->setScaleX(2.1);
    progressAnimeNode->addChild(progressAnime);
    progressAnimeNode->setVisible(false);
    
    Node* charAnimeNode = mUnlockedPanel->getChildByName("charFrame")->getChildByName("charAnime");
    
    AnimeNode *charAnime = AnimeNode::create();
    charAnime->setScale(1.8);
    mCharAnimeNode = charAnime;
    charAnimeNode->addChild(charAnime);
    charAnimeNode->setVisible(false);
    
    
    mRootNode = rootNode;
}

Vec2 CharacterItemView::getCharCenterPos()
{
    Node* bgSprite = mUnlockedPanel->getChildByName("normalBG");
    Vec2 pos = bgSprite->convertToWorldSpaceAR(Vec2::ZERO);
    return pos;
}

Vec2 CharacterItemView::getCharTouchArea()
{
    Node* bgSprite = mUnlockedPanel->getChildByName("normalBG");
    Vec2 size = bgSprite->getContentSize() * bgSprite->getScale();
    return size;
}

void CharacterItemView::setView(PlayerCharProfile* charProfile)
{
    setPlayerProfile(charProfile);
    
    mCharAnimeNode->stopAnimation();
    mCharAnimeNode->setup(GameRes::getCharacterCsbName(charProfile->getCharID()));
    mCharAnimeNode->playAnimation("idle", true);
    
    if(charProfile->isLocked()){
        setLockedView();
    }else{
        setUnlockedView();
    }
}

void CharacterItemView::setLockedView()
{
    mRootNode->setContentSize(Size(getContentSize().width,117));
    ui::Helper::doLayout(mRootNode);
    setContentSize(Size(getContentSize().width,117));

    mLockedPanel->setVisible(true);
    mUnlockedPanel->setVisible(false);
    
    mTouchPanel = mLockedPanel->getChildByName<Layout*>("touchPanel");
    mTouchPanel->setTouchEnabled(true);
    mTouchPanel->setSwallowTouches(false);
    
    Sprite* charSprite = mLockedPanel->getChildByName("charFrame")->getChildByName<Sprite*>("charSprite");
    Text* nameText = mLockedPanel->getChildByName<Text*>("nameText");
    Text* progressText = mLockedPanel->getChildByName<Text*>("progressText");
    LoadingBar* progressBar = mLockedPanel->getChildByName<LoadingBar*>("progressBar");
    Node* animeNode = progressBar->getChildByName("animeNode");
    
    std::string name = mPlayerProfile->getName();
    int collectedFrag = mPlayerProfile->getCollectedFragCount();
    int requiredFrag = mPlayerProfile->getUpgradeFragCount();
    
    nameText->setString(name);
    if(collectedFrag<requiredFrag){
        animeNode->setVisible(false);
        progressText->setString(StringUtils::format("%d/%d",collectedFrag,requiredFrag));
        mTouchPanel->addTouchEventListener([=](Ref*,Widget::TouchEventType type){
            if(mTouchCallback){
                mTouchCallback(mPlayerProfile->getCharID(), type);
            }
        });
    }else{
        animeNode->setVisible(true);
        progressText->stopAllActions();
        progressText->setString(StringUtils::format("%d/%d",collectedFrag,requiredFrag));
        FadeOut* fadeOut = FadeOut::create(1);
        FadeIn* fadeIn = FadeIn::create(1);
        DelayTime* delay = DelayTime::create(0.5);
        
        CallFunc* changeToNumber = CallFunc::create([=](){
            progressText->setString(StringUtils::format("%d/%d",collectedFrag,requiredFrag));
        });
        
        CallFunc* changeToUnlock = CallFunc::create([=](){
            progressText->setString(StringUtils::format("unlock"));
        });
        
        Sequence* sequence = Sequence::create(delay, fadeOut, changeToUnlock, fadeIn,
                                              delay->clone(), fadeOut->clone(), changeToNumber,  fadeIn->clone(), NULL);
        
        RepeatForever* repeat = RepeatForever::create(sequence);
        progressText->runAction(repeat);
         
        mTouchPanel->addTouchEventListener([=](Ref*,Widget::TouchEventType type){
            if(mUnlockCallback){
                mUnlockCallback(mPlayerProfile->getCharID(), type);
            }
        });
    }
    progressBar->setPercent((float) collectedFrag/requiredFrag*100.0f);
    charSprite->setTexture(StringUtils::format("playChar/player_icon_%03d.png",mPlayerProfile->getCharID()));
}

void CharacterItemView::setUnlockedView()
{
    mRootNode->setContentSize(Size(getContentSize().width,137));
    ui::Helper::doLayout(mRootNode);
    setContentSize(Size(getContentSize().width,137));
    
    ui::Helper::doLayout(this);
    
    mLockedPanel->setVisible(false);
    mUnlockedPanel->setVisible(true);
    
    mTouchPanel = mUnlockedPanel->getChildByName<Layout*>("touchPanel");
    mTouchPanel->setTouchEnabled(true);
    mTouchPanel->setSwallowTouches(false);
    
    mTouchPanel->addTouchEventListener([=](Ref*,Widget::TouchEventType type){
        if(mTouchCallback){
            mTouchCallback(mPlayerProfile->getCharID(), type);
        }
    });
    
    Text* nameText = mUnlockedPanel->getChildByName<Text*>("nameText");
    Text* levelText = mUnlockedPanel->getChildByName<Text*>("levelText");
    Button* selectBtn = mUnlockedPanel->getChildByName<Button*>("selectBtn");
    Sprite* newLabel = mUnlockedPanel->getChildByName<Sprite*>("newLabel");
    Node* animeRootNode = selectBtn->getChildByName("animeNode");
    Sprite* charSprite = mUnlockedPanel->getChildByName("charFrame")->getChildByName<Sprite*>("charSprite");
    charSprite->setTexture(StringUtils::format("playChar/player_icon_%03d.png",mPlayerProfile->getCharID()));
    
    Node* charAnimeNode = mUnlockedPanel->getChildByName("charFrame")->getChildByName("charAnime");
    
    if(mPlayerProfile->isNewChar()){
        newLabel->setVisible(true);
        MoveBy* moveBy = MoveBy::create(0.5, Vec2(0,10));
        RepeatForever* repeat = RepeatForever::create(Sequence::create(moveBy, moveBy->reverse(), NULL));
        newLabel->runAction(repeat);
    }else{
        newLabel->setVisible(false);
    }
    
    
    if(PlayerManager::instance()->getSelectedChar() == mPlayerProfile->getCharID()){
        selectBtn->setEnabled(false);
        animeRootNode->setVisible(true);
        charSprite->setVisible(false);
        charAnimeNode->setVisible(true);
    }else{
        charSprite->setVisible(true);
        charAnimeNode->setVisible(false);
        animeRootNode->setVisible(false);
        selectBtn->setEnabled(true);
        selectBtn->addClickEventListener([=](Ref*){
            if(mBtnCallback){
                mBtnCallback(mPlayerProfile->getCharID());
            }
        });
    }
    
    nameText->setString(mPlayerProfile->getName());
    levelText->setString(StringUtils::format("%d",mPlayerProfile->getCurrentLevel()));
    

    
}

Vec2 CharacterItemView::getCharIconWorldPos()
{
    Node* charSprite = mLockedPanel->getChildByName("charFrame")->getChildByName("charSprite");
    Vec2 pos = charSprite->convertToWorldSpaceAR(Vec2::ZERO);
    return pos;
}

void CharacterItemView::setBtnCallback(std::function<void (int)> callback)
{
    mBtnCallback = callback;
}

void CharacterItemView::setTouchCallback(std::function<void (int, Widget::TouchEventType)> callback)
{
    mTouchCallback = callback;
}

void CharacterItemView::setUnlockCallback(std::function<void (int, Widget::TouchEventType)> callback)
{
    mUnlockCallback = callback;
}
