//
//  MasteryManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/5/2016.
//
//

#include "MasteryManager.h"
#include "Mastery.h"
#include "Mock.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "ViewHelper.h"
#include "Player.h"
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include "StringHelper.h"
#include "Analytics.h"
#include "DogManager.h"
#include "DogData.h"

#define kKeyPlayerMastery	"ghostleg.masteryData"

#define kMasteryIDMaxStorage		1
#define kMasteryIDRegeneration		2
#define kMasteryIDDoubleStar		3
#define kMasteryIDRefinery			4


MasteryManager::MasteryManager()
{
	
}

void MasteryManager::loadPlayerMastery()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("MasteryManager.loadPlayerMastery: userDefault not available");
		return;
	}
	
	mPlayerMasteryArray.clear();
	
	std::string content = userData->getStringForKey(kKeyPlayerMastery, "");
	
	if(content == "") {
		return;
	}

	std::stringstream ss(content);
	std::string to;
    
	while(std::getline(ss, to, '\n')){

		PlayerMastery *mastery = new PlayerMastery();
		
		mastery->parse(to);
		
		if(mastery->getMasteryID() > 0) {
			mPlayerMasteryArray.pushBack(mastery);	// retain happen here!
		}
		
		mastery->release();
	}
}

void MasteryManager::savePlayerMastery()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("MasteryManager.savePlayerMastery: userDefault not available");
		return;
	}
	
	std::string playerData = "";
	for(int i=0; i<mPlayerMasteryArray.size(); i++) {
		PlayerMastery *data = mPlayerMasteryArray.at(i);
		playerData += data->toString();
		playerData += "\n";
	}
	
	userData->setStringForKey(kKeyPlayerMastery, playerData);
}

/***
 *
 *  JSONData *jsonData = loadJSONData("mastery.json");
 *  for( .....) 
 *   {
 *      MasteryData *data = parseMasteryFromJSON(....)		// autorelease
 *       mMasteryArray.pushBack(data);
 *  }
 *
 */
MasteryData* MasteryManager::generateSingleMasteryFromJSON(const rapidjson::Value &masteryJSON){
    
    MasteryData *data = MasteryData::create();

    data->setMasteryID(masteryJSON["masteryID"].GetInt());
    data->setName(masteryJSON["name"].GetString());
    data->setInfo(masteryJSON["info"].GetString());
    data->setMaxLevel(masteryJSON["maxLevel"].GetInt());
    data->setFloatFlag(masteryJSON["isFloat"].GetBool());
    data->setPercentFlag(masteryJSON["isPercentage"].GetBool());
    data->setItemIcon(masteryJSON["valueIcon"].GetString());

    for(int j=0;j<masteryJSON["price"].Size();j++){
        data->addLevelPrice(masteryJSON["price"][j].GetInt());
        //printf("%i, ",masteryJSON["price"][j].GetInt());
    }
    
    log("\nlevelValue:");
    for(int k=0;k<masteryJSON["value"].Size();k++){
        data->addLevelValue(masteryJSON["value"][k].GetDouble());
        //printf("%.1f, ",masteryJSON["value"][k].GetDouble());
    }
    
    return data;
}

void MasteryManager::loadMasteryData()
{
    
    std::string content = FileUtils::getInstance()->getStringFromFile("json/mastery.json");
    
    log("mastery json content:\n%s", content.c_str());
    rapidjson::Document masteryArray;
    masteryArray.Parse(content.c_str());
    if (masteryArray.HasParseError())
    {
        CCLOG("GetParseError %d\n", masteryArray.GetParseError());
        return;
    }

    mMasteryArray.clear();

    for(rapidjson::SizeType i=0;i<masteryArray.Size();i++){
        const rapidjson::Value& currentMastery = masteryArray[i];
        MasteryData *data = generateSingleMasteryFromJSON(currentMastery);
        mMasteryArray.pushBack(data);
    }
	
}

Vector<MasteryData *> MasteryManager::getMasteryArray()
{
	return mMasteryArray;
}


PlayerMastery *MasteryManager::getPlayerMastery(int masteryID)
{
	for(int i=0; i<mPlayerMasteryArray.size(); i++) {
		PlayerMastery *data = mPlayerMasteryArray.at(i);
		if(data->getMasteryID() == masteryID) {
			return data;
		}
	}

	return nullptr;
}

void MasteryManager::setPlayerMasteryLevel(int masteryID, int level)
{
	PlayerMastery *mastery = getPlayerMastery(masteryID);
	
	if(mastery != nullptr) {
		mastery->setMasteryLevel(level);
		return;
	}

	// Make a new one
	mastery = new PlayerMastery();
	mastery->setMasteryID(masteryID);
	mastery->setMasteryLevel(level);
	
	mPlayerMasteryArray.pushBack(mastery);
	
	
	mastery->release();
}


int MasteryManager::getPlayerMasteryLevel(int masteryID)
{
	PlayerMastery *mastery = getPlayerMastery(masteryID);
	
	return mastery == nullptr ? 0 : mastery->getMasteryLevel();
}

int MasteryManager::upgradeMastery(int masteryID)
{
	
	MasteryData *masteryData = getMasteryData(masteryID);
    
	if(masteryData == nullptr) {
		return -3;
	}
	
	int currentLevel = getPlayerMasteryLevel(masteryID);
	if(currentLevel == masteryData->getMaxLevel()) {
		return -2;
	}
	
	int playerCoin = GameManager::instance()->getUserData()->getTotalCoin();
	int requiredCoin = masteryData->getUpgradePrice(currentLevel + 1);
    log("MasteryManager::upgradeMastery: playerCoin=%d,requiredCoin=%d",playerCoin,requiredCoin);
	
	if(playerCoin < requiredCoin) {
    		return -1;
	}

	// Save the mastery after level up
	setPlayerMasteryLevel(masteryID, currentLevel+1);
	savePlayerMastery();
	
	// Save the consumed coin
	GameManager::instance()->getUserData()->addCoin(-requiredCoin);
	
	// GA Log
	// Analytics::instance()->logMastery(masteryID, currentLevel+1, requiredCoin);
	
    
	return 0;
}

MasteryData *MasteryManager::getMasteryData(int masteryID)
{
	for(int i=0; i<mMasteryArray.size(); i++) {
		MasteryData *data = mMasteryArray.at(i);
		if(data->getMasteryID() == masteryID) {
			return data;
		}
	}
	
	return nullptr;
}




std::string MasteryManager::infoMasteryArray()
{
	std::string count = StringUtils::format("%zd", mMasteryArray.size());
	std::string result = "Mastery Count: " + count;
	
	result += "\n";
	for(int i=0; i<mMasteryArray.size(); i++) {
		MasteryData *data = mMasteryArray.at(i);
		
		result += data->toString();
		result += "\n";
	}
	
	return result;
}


void MasteryManager::resetAllMastery()
{
	for(int i=0; i<mPlayerMasteryArray.size(); i++) {
		PlayerMastery *mastery = mPlayerMasteryArray.at(i);
		
		mastery->setMasteryLevel(0);
	}
}

std::string MasteryManager::infoPlayerMasteryArray()
{
	std::string count = StringUtils::format("%zd", mPlayerMasteryArray.size());
	std::string result = "Player Mastery Count: " + count;
	
	result += "\n";
	for(int i=0; i<mPlayerMasteryArray.size(); i++) {
		PlayerMastery *data = mPlayerMasteryArray.at(i);
		
		result += data->toString();
		result += "\n";
	}
	
	return result;
}

// The current value of the player mastery (base on current level)
float MasteryManager::getCurrentMasteryValue(int masteryID)
{
	MasteryData *mastery = getMasteryData(masteryID);
	if(mastery == nullptr) {
		return 0;
	}
	
	// int currentLevel = getPlayerMasteryLevel(masteryID);
	int currentLevel = getDogMasteryLevel(masteryID);			// 
	
	return mastery->getLevelValue(currentLevel);
}

void MasteryManager::setPlayerMasteryAttribute(Player *player)
{
	if(player == nullptr) {
		log("error:setPlayerMasteryAttribute: player is null");
		return;
	}

	float totalEnergy = getCurrentMasteryValue(kMasteryIDMaxStorage);
	float consumeRate = getCurrentMasteryValue(kMasteryIDRefinery);
	float regenerateRate = getCurrentMasteryValue(kMasteryIDRegeneration);
	float doubleStarRate = getCurrentMasteryValue(kMasteryIDDoubleStar);
	
	
	player->setTotalEnergy(totalEnergy);
	player->setConsumeRate(consumeRate);
	player->setRegenerateRate(regenerateRate);
	player->setDoubleStarRate(doubleStarRate);
}

std::string MasteryManager::getMasteryName(int masteryID)
{
	MasteryData *mastery = getMasteryData(masteryID);
	if(mastery == nullptr) {
		return StringUtils::format("mastery_%d", masteryID);
	}
	
	return mastery->getName();
}

int MasteryManager::getDogMasteryLevel(int masteryID)
{
	DogData *data = DogManager::instance()->getSelectedDogData();
	
	return data == nullptr ? 1 : data->getMasteryLevel(masteryID);
}

