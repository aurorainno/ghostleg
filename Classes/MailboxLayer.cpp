//
//  MailboxLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 17/2/2017.
//
//

#include "MailboxLayer.h"
#include "CommonMacro.h"
#include "MailItemView.h"
#include "FBProfilePic.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "StageParallaxLayer.h"
#include "AnimationHelper.h"
#include "PlayerManager.h"
#include "Analytics.h"

const Vec2 kStarAnimationFinalPos = Vec2(148.5,541.5);
const Vec2 kDiamondAnimationFinalPos = Vec2(239.5,541.5);

bool MailboxLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    AstrodogClient::instance()->setListener(this);
    
    StageParallaxLayer *parallaxLayer = StageParallaxLayer::create();
    parallaxLayer->setBGSpeed(0);
    addChild(parallaxLayer);
    parallaxLayer->scheduleUpdate();

    
    Node* rootNode = CSLoader::createNode("MailBoxLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

void MailboxLayer::onEnter()
{
    Layer::onEnter();
    
//    Vector<AstrodogMail*> mailList = AstrodogClient::instance()->getMailbox()->getSortedMailList();
//    if(mailList.empty()){
//        mInProgressLayer->setVisible(true);
//        AstrodogClient::instance()->loadMailbox();
//    }else{
//        loadMailListView(mailList);
//    }
    
    mInProgressLayer->setVisible(true);
    AstrodogClient::instance()->loadMailbox();
    
    updateStars();
    updateDiamond();
    setupFbProfile();
	
	LOG_SCREEN(Analytics::scn_inbox);
}

void MailboxLayer::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    mStarText = (Text*) rootNode->getChildByName("totalCoinText");
    mDiamondText = rootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mFbConnectBtn = (Button*) rootNode->getChildByName("fbBtn");
    mFbName = (Text *) rootNode->getChildByName("fbName");
    
    mEmptyHint = (Text*) rootNode->getChildByName("emptyHint");
    
    mProfilePicFrame = (Sprite *) rootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
    mFbConnectBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });

    
    mCancelBtn = rootNode->getChildByName<Button*>("backBtn");
    mCancelBtn->addClickEventListener([&](Ref*){
        if(mCloseCallback){
            mCloseCallback();
        }
        this->removeFromParent();
    });
    
    mListView = rootNode->getChildByName<ListView*>("listView");
    mInProgressLayer = rootNode->getChildByName("InProgressLayer");
}

void MailboxLayer::setCloseCallback(std::function<void ()> callback)
{
    mCloseCallback = callback;
}

void MailboxLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void MailboxLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void MailboxLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        mFbName->setString(user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
//        mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void MailboxLayer::loadMailListView(Vector<AstrodogMail*> mailList)
{
    if(mailList.empty()){
        mEmptyHint->setVisible(true);
        return;
    }
    
    for(AstrodogMail* mail : mailList){
        MailItemView *mailView = MailItemView::create();
        mailView->setView(mail);
        mailView->setOnReceiveCallback([=](int mailID){
            //updateMailItemView(mailID);
            mInProgressLayer->setVisible(true);
            mSelectedMailID = mailID;
            AstrodogClient::instance()->receivedMail(mailID);
 //           AstrodogMail* mail = AstrodogClient::instance()->getMailbox()->getMailByID(mSelectedMailID);
 //           AstrodogAttachment attachment = mail->getAttachment();
 //           playAnimation(attachment);
        });
        mMailItemViewMap.insert(mail->getMailID(), mailView);
        mListView->pushBackCustomItem(mailView);
    }
}

void MailboxLayer::updateMailItemView(int mailID)
{
    MailItemView* view = mMailItemViewMap.at(mailID);
    if(!view){
        return;
    }
    AstrodogMail* mail = AstrodogClient::instance()->getMailbox()->getMailByID(mailID);
    view->setView(mail);
}

void MailboxLayer::playAnimation(AstrodogAttachment attachment)
{
    Sprite* target;
    MailItemView* view =  mMailItemViewMap.at(mSelectedMailID);
    if(attachment.type == AstrodogAttachment::TypeDiamond){
        target = Sprite::create("guiImage/ui_general_diamond.png");
    }else if(attachment.type == AstrodogAttachment::TypeStar){
        target = Sprite::create("guiImage/ui_general_coins.png");
    }else{
        return;
    }
    
    Vec2 startPos = view->convertToWorldSpace(view->getLargeStar()->getPosition());
    Vec2 endPos = attachment.type == AstrodogAttachment::TypeDiamond ? kDiamondAnimationFinalPos : kStarAnimationFinalPos;
    target->setPosition(startPos);
    target->setScale(0.5);
//    ActionInterval* action = AnimationHelper::getCurveAction(startPos, endPos, 1.0);
    MoveTo *moveTo = MoveTo::create(0.4, endPos);
    CallFunc *func = CallFunc::create([=](){
        log("Animation end!");
        target->setVisible(false);
        int oriAmount;
        if(attachment.type == AstrodogAttachment::TypeDiamond){
            oriAmount = GameManager::instance()->getMoneyValue(MoneyType::MoneyTypeDiamond);
            oriAmount -= attachment.amount;
            AnimationHelper::popTextEffect(this, mDiamondText, oriAmount, oriAmount + attachment.amount, [=]{
                updateDiamond();
                updateStars();
            });
        }else if(attachment.type == AstrodogAttachment::TypeStar){
            oriAmount = GameManager::instance()->getMoneyValue(MoneyType::MoneyTypeStar);
            AnimationHelper::popTextEffect(this, mStarText, oriAmount, oriAmount + attachment.amount, [=]{
                updateDiamond();
                updateStars();
            });
        }

    });
    Sequence *seq = Sequence::create(moveTo,func,RemoveSelf::create(), NULL);
    target->runAction(seq);
    addChild(target);
}

void MailboxLayer::onMailReceived(int status)
{
    mInProgressLayer->setVisible(false);
    if(status!=STATUS_SUCCESS){
        return;
    }
    
    AstrodogMail* mail = AstrodogClient::instance()->getMailbox()->getMailByID(mSelectedMailID);
    AstrodogAttachment att = mail->getAttachment();
    
    updateMailItemView(mSelectedMailID);
//    updateDiamond();
//    updateStars();
 
	
	// bug fixing for crash in the following function on 1-6-2017
    playAnimation(att);
}

void MailboxLayer::onMailboxLoaded(int status)
{
    mInProgressLayer->setVisible(false);
    if(status!=STATUS_SUCCESS){
        return;
    }
    
    Vector<AstrodogMail*> mailList = AstrodogClient::instance()->getMailbox()->getSortedMailList();
    loadMailListView(mailList);
}

void MailboxLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}

