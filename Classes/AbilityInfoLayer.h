//
//  AbilityInfoLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 10/4/2017.
//
//

#ifndef AbilityInfoLayer_hpp
#define AbilityInfoLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "PlayerCharProfile.h"
#include "Analytics.h"

class FBProfilePic;

USING_NS_CC;
using namespace cocos2d::ui;

class RichTextLabel;

class AbilityInfoLayer : public Layer
{
public:
    CREATE_FUNC(AbilityInfoLayer);
    
    CC_SYNTHESIZE_RETAIN(PlayerCharProfile*, mPlayerProfile, PlayerProfile);
    
    virtual bool init();
    void onEnter();
    
    void updateDiamond();
    void updateStars();
    void setupFbProfile();
    
    void setupUI(Node* rootNode);
    
    void setView(PlayerCharProfile* charProfile);
    
    
private:
    void setRichTextContent(RichTextLabel *richText, std::string content);
    

private:
    Node *mRootNode;
    Text *mNameText;
    
    Text *mStarText, *mDiamondText;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
};


#endif /* AbilityInfoLayer_hpp */
