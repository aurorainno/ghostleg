//
//  CharacterSelectionLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 13/2/2017.
//
//

#ifndef CharacterSelectionLayer_hpp
#define CharacterSelectionLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "AstrodogClient.h"
#include "CharacterItemView.h"

USING_NS_CC;
using namespace cocos2d::ui;

class FBProfilePic;
class GameModel;
class DogData;
class TutorialLayer;

class CharacterSelectionLayer : public Layer, public AstrodogClientListener
{
public:
    CharacterSelectionLayer();
    
    static Scene* createScene();
    CREATE_FUNC(CharacterSelectionLayer);
    virtual bool init();
    
    void setupUI();
    void onEnter();
    
    void update(float delta);
    
    
    void updateDiamond();
    void updateStars();
    void setupFbProfile();
    
    void setCloseCallback(std::function<void()> callback);
    
    void setScrollView(Vector<PlayerCharProfile*> lockedList, Vector<PlayerCharProfile*> unlockedList);
    void setPanel(Vector<PlayerCharProfile*> profileList, std::string panelName);
    
    void refreshItemView(int charID);
    void resetView();
    
    void handleUnlockNewChar(int charID, Sprite* charIcon);
    
    void showSelectCharCoachmark();

#pragma mark - AstrodogClient
public:
    virtual void onFBConnect(int status);
    
private:
    Node* mRootNode;
    Node* mMainPanel;
    Node* mCoverPanel;
    
    Text* mDiamondText;
    Text* mStarText;
    
    Button *mFbConnectBtn;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
    Button* mCancelBtn;
    TutorialLayer *mTutorialLayer;
    Sprite *mUnlockSprite;
    
    ui::ScrollView *mScrollView;
    
    std::function<void()> mCloseCallback;
    
    Map<int,CharacterItemView*> mItemViewMap;
    
    bool mIsScene;
    float mContentSizeHeight;
    float mOriScrollViewPositionY;
};

#endif /* CharacterSelectionLayer_hpp */
