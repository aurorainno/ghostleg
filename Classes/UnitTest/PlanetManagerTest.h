#ifdef ENABLE_TDD
//
//  PlanetManagerTest.h
//
//
#ifndef __TDD_PlanetManagerTest__
#define __TDD_PlanetManagerTest__

// Include Header

#include "TDDBaseTest.h"
#include "ui/CocosGUI.h"

// Class Declaration
class PlanetManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testGameInfo();
	void testIsTypeMatched();
	void testParseTypeStr();
	void testPlanetData();
	void testChangePlanet();
	void testGameData();
	void testLock();
	void testUnLock();
	void testUnLockAll();
	void testDogTeamList();
	void testUnlockPlanetLayer();
private:
	ui::Text *mPlanetText;
	
};

#endif

#endif
