#ifdef ENABLE_TDD
//
//  StringHelperTest.m
//	TDD Framework 
//
//
#include "StringHelperTest.h"
#include "TDDHelper.h"
#include "StringHelper.h"
#include "URLHelper.h"
//#include <curl/curl.h>

void StringHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void StringHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void StringHelperTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testContainsNonAlpha);
	ADD_TEST(testUrlEscape);
	ADD_TEST(testFormatDecimal);
	ADD_TEST(testEndsWith);
	ADD_TEST(testCharVecToString);
	ADD_TEST(testMapToString);
	ADD_TEST(testVectorToString);
	ADD_TEST(testFromGeometry);
	ADD_TEST(testSplit);
	ADD_TEST(testParseInt);
    ADD_TEST(testReplaceString);
    ADD_TEST(testContainsCJK);
}

#pragma mark -
#pragma mark Sub Test Definition
void StringHelperTest::subTest()
{
	log("this is a sample subTest");
}

void StringHelperTest::testUrlEscape()
{
	std::vector<std::string> testNames;
	
	testNames.push_back("陳一心");
	testNames.push_back("Ken Lee");
	testNames.push_back("Hello World");
	testNames.push_back("一拳Chiu人");

	for(std::string str : testNames) {
		std::string encoded = UriEncode(str);
		std::string decoded = UriDecode(encoded);
	
		log("DEBUG: str=[%s] length=%d", str.c_str(), (int) str.length());
		log("DEBUG: encode=[%s] decode=[%s]", encoded.c_str(), decoded.c_str());
	}
//
//	CURL *curl = curl_easy_init();
//	if(curl) {
//	
//		char *output = curl_easy_escape(curl, str.c_str(), str.length());
//		if(output) {
//			printf("Encoded: %s\n", output);
//			curl_free(output);
//		}
//		
//		curl_easy_cleanup(curl);
//	}
}

void StringHelperTest::testContainsNonAlpha()
{
	std::vector<std::string> testList;
	
	testList.push_back("PLAY 1234");
	testList.push_back("ABCD EFGG");
	testList.push_back("測試用中文");
	testList.push_back("ぁいぶをれ");
	testList.push_back("ㅖ한ㅍㅎㅛ");
	testList.push_back("ϐϕθ");
	testList.push_back("Bérangère");
	
	
	for(std::string str : testList) {
		bool result = aurora::StringHelper::containNonAlpha(str);
		
		log("%s: %d", str.c_str(), result);
	}
	
}


void StringHelperTest::testContainsCJK()
{
	std::vector<std::string> testList;
	
	testList.push_back("ABCD EFGG");
	testList.push_back("測試用中文");
    testList.push_back("ぁいぶをれ");
    testList.push_back("ㅖ한ㅍㅎㅛ");
    testList.push_back("ϐϕθ");
	testList.push_back("Bérangère");
    
  	
	for(std::string str : testList) {
		bool isCJK = aurora::StringHelper::containCJKChar(str);

		log("%s: %d", str.c_str(), isCJK);
	}
    
}

void StringHelperTest::testFromGeometry()
{
	Rect rect = Rect(10, 20, 30, 40);
	Size size = rect.size;
	Point point = rect.origin;
	
	std::string rectStr = aurora::StringHelper::rectToStr(rect);
	std::string sizeStr = aurora::StringHelper::sizeToStr(size);
	std::string pointStr = aurora::StringHelper::pointToStr(point);
	
	log("DEBUG: rect=%s size=%s point=%s",
				rectStr.c_str(), sizeStr.c_str(), pointStr.c_str());

}

void StringHelperTest::testSplit()
{
	std::vector<std::string> tokens = aurora::StringHelper::split("star:1234", ':');
	
	
	for(int i=0; i<tokens.size(); i++) {
		std::string value = tokens[i];
		
		log("%d: %s", i, value.c_str());
	}
}



void StringHelperTest::testParseInt()
{
	std::string valueStr = "1234";
	
	int value = aurora::StringHelper::parseInt(valueStr);
	
	log("str=%s value=%d", valueStr.c_str(), value);
	
	//StringUtils::
}

void StringHelperTest::testVectorToString()
{
	std::vector<int> intVector;
	intVector.push_back(1);
	intVector.push_back(3);
	intVector.push_back(5);
	
	std::vector<float> floatVector;
	floatVector.push_back(1.3);
	floatVector.push_back(2.5);
	floatVector.push_back(3.8);
	
	log("intVector: %s", aurora::StringHelper::vectorToString(intVector).c_str());
	log("floatVector: %s", aurora::StringHelper::vectorToString(floatVector).c_str());
	
	log("vector: %s", VECTOR_TO_STR(intVector).c_str());
}

void StringHelperTest::testMapToString()
{
	std::map<std::string, std::string> map;
	map["prop1"] = "Apple";
	map["prop2"] = "testing";
	map["prop3"] = "Hello";

	log("mapList:\n%s", aurora::StringHelper::mapToString(map).c_str());
}

void StringHelperTest::testCharVecToString()
{
	std::vector<char> charVector;
	charVector.push_back('a');
	charVector.push_back('b');
	charVector.push_back('c');

	std::string result = aurora::StringHelper::charVectorToString(charVector);
	
	log("%s", result.c_str());
}

void StringHelperTest::testReplaceString()
{
    std::string s1 = "this is a #abc test";
    std::string sym = "#abc";
    
    aurora::StringHelper::replaceString(s1, sym, "sub");
    log("%s",s1.c_str());
    
    std::string str = StringUtils::format("%f", 3.05000);
    str.erase ( str.find_last_not_of('0') + 1, std::string::npos );
    log("%s",str.c_str());
}

void StringHelperTest::testFormatDecimal()
{
	const int nCount = 5;
	
	float testValues[nCount] = {
		0,
		1.5,
		2,
		3,
		2.1203,
	};
	
	for(float value : testValues) {
		std::string str = aurora::StringHelper::formatDecimal(value);
		
		log("value=%f str=[%s]", value, str.c_str());
	}
	
}

void StringHelperTest::testEndsWith()
{
	const int nCount = 6;
	
	float testValues[nCount] = {
		0,
		1.5,
		2,
		3,
		2.1203,
		2.0f/3,
	};
	
	for(float value : testValues) {
		std::string str = aurora::StringHelper::formatDecimal(value);
		
		log("value=%f str=[%s]", value, str.c_str());
	}
	
}

#endif
