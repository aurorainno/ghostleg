#ifdef ENABLE_TDD
//
//  AdManagerTest.m
//	TDD Framework 
//
//
#include "AdManagerTest.h"
#include "TDDHelper.h"
#include "AdManager.h"
#include "ViewHelper.h"
#include "VisibleRect.h"

void AdManagerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
//	mVideoList.push_back("VideoRevive");
//	mVideoList.push_back("VideoReward");
//	mVideoList.push_back("VideoInter");
//	
	mVideoKeyList.push_back((int) AdManager::VideoAd::OneMoreChance);
	mVideoKeyList.push_back((int) AdManager::VideoAd::PlayAgain);
	mVideoKeyList.push_back((int) AdManager::VideoAd::GainStar);
	
	mVideoToPlay = mVideoKeyList[0];
}


void AdManagerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void AdManagerTest::defineTests()
{
    ADD_TEST(subTest);
	ADD_TEST(testPlayAd);
	ADD_TEST(testfindSelectedProviderAndAd);
	ADD_TEST(testAdReadiness);
	ADD_TEST(testInterAd);
	ADD_TEST(testFindDefaultVideoProvider);
	ADD_TEST(showTestBanner);
	ADD_TEST(toggleNoAd);
	ADD_TEST(bannerAdTest);
	ADD_TEST(changeVideo);
    ADD_TEST(videoAdTest);
    ADD_TEST(hideAdTest);
}

#pragma mark -
#pragma mark Sub Test Definition

void AdManagerTest::testPlayAd()
{
	static int counter = 0;
	
	std::vector<AdManager::Ad> adList;
	adList.push_back(AdManager::Ad::AdPlayAgain);
	adList.push_back(AdManager::Ad::AdGameContinue);
	adList.push_back(AdManager::Ad::AdFreeCoin);
	adList.push_back(AdManager::Ad::AdFreeGacha);
	adList.push_back(AdManager::Ad::AdDoubleReward);
	
	
	AdManager::Ad currentAd = adList[counter % adList.size()];
	
	log("Try to play Ad=%d", currentAd);
	
	bool isOkay = AdManager::instance()->playVideoAd(currentAd, [=](bool isOkay){
		log("Finish playing videoAd=%d isOkay=%d", currentAd, isOkay);
	});
	
	log("  status=%d", isOkay);
	
	counter++;
}

void AdManagerTest::testfindSelectedProviderAndAd()
{
//	AdManager::instance();
//	void AdManager::findSelectedProviderAndAd(const AdManager::Ad &toPlayAd,
//											  AdManager::Provider &outProvider,
//											  AdManager::VideoAd &outAd)
	
//	AdPlayAgain			= 200,			// Interstitial
//	AdGameContinue		= 201,
//	AdFreeCoin			= 202,
//	AdFreeGacha			= 203,
//	AdDoubleReward		= 204,			// double the quantity of Gacha

	std::vector<AdManager::Ad> adList;
//	adList.push_back(AdManager::Ad::AdPlayAgain);
//	adList.push_back(AdManager::Ad::AdGameContinue);
	adList.push_back(AdManager::Ad::AdFreeCoin);
//	adList.push_back(AdManager::Ad::AdFreeGacha);
//	adList.push_back(AdManager::Ad::AdDoubleReward);
	
	for(AdManager::Ad ad : adList) {
		AdManager::Provider provider;
		AdManager::VideoAd videoAd;
		
		AdManager::instance()->findSelectedProviderAndAd(ad, provider, videoAd);
		
		log("Ad: %d  provider=%d videoAd=%d", ad, provider, videoAd);
	}
}


void AdManagerTest::subTest(){
    AdManager *instance = AdManager::instance();
    instance->init();
}

void AdManagerTest::testAdReadiness()
{
	AdHandler *handler;
	
	handler = AdManager::instance()->getAdHandler(AdManager::Admob);
	log("Admob Info:\n%s\n", handler->infoAdList().c_str());
	
	handler = AdManager::instance()->getAdHandler(AdManager::AdColony);
	log("Adcolony Info:\n%s\n", handler->infoAdList().c_str());
}


void AdManagerTest::testInterAd()
{
	AdManager::instance()->showInterstitualAd();
}

void AdManagerTest::testFindDefaultVideoProvider()
{
	
	
	
	std::vector<AdManager::VideoAd> videoList;
	
	videoList.push_back(AdManager::OneMoreChance);
	videoList.push_back(AdManager::PlayAgain);
	
	for(int i=0; i<videoList.size(); i++) {
		AdManager::VideoAd video = videoList[i];
		
		AdManager::Provider provider = AdManager::instance()->findDefaultVideoProvider(video);
		
		log("video=%d provider=%d", video, provider);
	}
}



void AdManagerTest::toggleNoAd()
{
	bool isNoAd = AdManager::instance()->isNoAd();
	if(isNoAd) {
		// Turn on
		AdManager::instance()->setNoAd(false);
		ViewHelper::showFadeAlert(this, "Ad is turned ON", 200);
	} else {
		AdManager::instance()->setNoAd(true);
		ViewHelper::showFadeAlert(this, "Ad is turned OFF", 200);
	}
	
}

void AdManagerTest::showTestBanner()
{
	log("Testing banner Ad");
	AdManager::instance()->showBannerAd("home");
}

void AdManagerTest::bannerAdTest()
{
	log("Calling banner Ad");
    AdManager::instance()->showBannerAd("Banner");
}


void AdManagerTest::changeVideo()
{
	static int idx = 0;
	
	int videoIdx = idx % mVideoKeyList.size();
	
//	mVideoName = mVideoList[videoIdx];
//
	mVideoToPlay = mVideoKeyList[videoIdx];
	std::string msg = StringUtils::format("Player video [%d", mVideoToPlay);
	ViewHelper::showFadeAlert(this, msg, VisibleRect::center().y);
	
	
	idx++;
}

void AdManagerTest::videoAdTest(){
	
	
	AdManager::instance()->setVideoFinishedCallback([](bool isFinished){
		log("Video is played");
	});
	
	AdManager::instance()->setVideoRewardedCallback([](bool isRewarded){
		log("Video is rewarded");
	});
	
	bool isOk = AdManager::instance()->showVideoAd((AdManager::VideoAd) (mVideoToPlay));
	log("showVideoAd: video=%d isOkay=%d", mVideoToPlay, isOk);
	
	if(!isOk) {
		ViewHelper::showFadeAlert(this, "Fail to play video ad", VisibleRect::center().y);
	}
}

void AdManagerTest::hideAdTest(){
    AdManager::instance()->hideBannerAd("BannerDialog");
}

#endif
