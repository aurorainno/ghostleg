#ifdef ENABLE_TDD
//
//  AndroidHelperTest.h
//
//
#ifndef __TDD_AndroidHelperTest__
#define __TDD_AndroidHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class AndroidHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testVoidStatic();
	void testGetString();
	void testSetterMethod();
}; 

#endif

#endif
