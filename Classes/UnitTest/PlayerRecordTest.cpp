#ifdef ENABLE_TDD
//
//  PlayerRecordTest.m
//	TDD Framework 
//
//
#include "PlayerRecordTest.h"
#include "TDDHelper.h"
#include "PlayerRecord.h"
#include "PlayerGameResult.h"
#include "GameManager.h"
#include "PlayerCharData.h"

void PlayerRecordTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void PlayerRecordTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

void PlayerRecordTest::setupGameResult(PlayerGameResult &result,
				int distance, int star, int dog, int planet, int kill)
{
	result.setDistance(distance);
	result.setStarCollected(star);
	result.setDogPlayed(dog);
	result.setPlanetPlayed(planet);
	result.setKill(kill);

	//	result.setDis
//	result.
}

#pragma mark -
#pragma mark List of Sub Tests

void PlayerRecordTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testIsStageClear);
	ADD_TEST(testParsePlayerCharData);
	ADD_TEST(testPlayerGameData);
	ADD_TEST(testSetDogRecord);
	ADD_TEST(testUpdateWithResult);
	ADD_TEST(testSetGlobalRecord);
	ADD_TEST(testClearRecord);
}

#pragma mark -
#pragma mark Sub Test Definition
void PlayerRecordTest::testIsStageClear()
{
	int start=1;
	int end=5;
	
	for(int stageID=start; stageID <= end; stageID++) {
		bool isClear = GameManager::instance()->isStageClear(stageID);
		
		log("stage: %d  isClear=%d", stageID, isClear);
	}
	
}


void PlayerRecordTest::testParsePlayerCharData()
{
	PlayerCharData *data = PlayerCharData::create();
	data->setCharID(1);
	data->setCollectedFragment(10);
	data->setCurrentLevel(1);
	
	std::string json = data->toJSONContent();
	
	log("JSON: %s", json.c_str());
	
	PlayerCharData *newData = PlayerCharData::create();
	newData->parseJSONContent(json);
	
	log("JSON(parsed): %s", newData->toString().c_str());
	
}

void PlayerRecordTest::testPlayerGameData()
{
	PlayerRecord *record = PlayerRecord::create();

	log("GameData: %s", record->infoPlayerCharData().c_str());
}

void PlayerRecordTest::subTest()
{
	log("this is a sample subTest");

	PlayerRecord *record = PlayerRecord::create();
	
	log("key=%s", PlayerRecord::KeyBestDistance);
	
	record->setRecordData("hello", 123);
	record->setRecordData("hello_aaaa", 2123);
	
	log("%s", record->toString().c_str());
	
	std::string json = record->toJSONContent();
	log("json:\n%s\n", json.c_str());
	
	record->save();
	
	PlayerRecord *loadedData = PlayerRecord::create();
	
	loadedData->load();
	
	log("%s", loadedData->toString().c_str());
}

void PlayerRecordTest::testSetDogRecord()
{
	PlayerRecord *record = PlayerRecord::create();
	
	int distance, star, dogID;
	
	// Round 1:
	distance = 100; star = 10; dogID=1;
	
	record->setDogRecord(PlayerRecord::KeyPlayCount, dogID, 1);
	record->setDogRecord(PlayerRecord::KeyStarTotal, dogID, star);
	record->setDogRecord(PlayerRecord::KeyStarInOneGame, dogID, star);
	record->setDogRecord(PlayerRecord::KeyBestDistance, dogID, distance);
	
	log("Round1: %s", record->toString().c_str());
	
	
	// Round 2:
	distance = 130; star = 15; dogID=1;
	
	record->setDogRecord(PlayerRecord::KeyPlayCount, dogID, 1);
	record->setDogRecord(PlayerRecord::KeyStarTotal, dogID, star);
	record->setDogRecord(PlayerRecord::KeyStarInOneGame, dogID, star);
	record->setDogRecord(PlayerRecord::KeyBestDistance, dogID, distance);
	
	log("Round2: %s", record->toString().c_str());

	
	// Round 3:
	distance = 80; star = 7; dogID=1;
	
	record->setDogRecord(PlayerRecord::KeyPlayCount, dogID, 1);
	record->setDogRecord(PlayerRecord::KeyStarTotal, dogID, star);
	record->setDogRecord(PlayerRecord::KeyStarInOneGame, dogID, star);
	record->setDogRecord(PlayerRecord::KeyBestDistance, dogID, distance);
	
	log("Round3: %s", record->toString().c_str());
}

void PlayerRecordTest::testUpdateWithResult()
{
	PlayerGameResult result;
	PlayerRecord *record = PlayerRecord::create();
	
	setupGameResult(result, 100, 3, 1, 1, 5);
	
	record->updatePlayerRecordWithResult(&result);
	log("Round1: %s", record->toString().c_str());	

	
	setupGameResult(result, 120, 30, 1, 1, 3);
	
	record->updatePlayerRecordWithResult(&result);
	log("Round2: %s", record->toString().c_str());

	
	setupGameResult(result, 20, 60, 2, 1, 0);
	
	record->updatePlayerRecordWithResult(&result);
	log("Round3: %s", record->toString().c_str());
	
	setupGameResult(result, 20, 60, 2, 3, 0);
	
	record->updatePlayerRecordWithResult(&result);
	log("Round4: %s", record->toString().c_str());
}

void PlayerRecordTest::testClearRecord()
{
	GameManager::instance()->clearPlayerRecord();
	
	log("record:\n%s\n", GameManager::instance()->infoPlayerRecord().c_str());
}

void PlayerRecordTest::testSetGlobalRecord()
{
	log("Record (Before): %s", GameManager::instance()->infoPlayerRecord().c_str());
	
	
	// Testing Data
	PlayerGameResult result;
	
	int distance = RandomHelper::random_int(50, 200);
	int star = RandomHelper::random_int(10, 30);
	int dog = RandomHelper::random_int(1, 3);
	int planet = RandomHelper::random_int(1, 2);
	
	setupGameResult(result, distance, star, dog, planet, 10);
	
	log("GameResult=%s", result.toString().c_str());
	
	
	//
	GameManager::instance()->updatePlayerRecord(&result);
	log("Record (After): %s", GameManager::instance()->infoPlayerRecord().c_str());
	
}

#endif
