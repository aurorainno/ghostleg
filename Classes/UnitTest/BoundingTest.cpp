#ifdef ENABLE_TDD
//
//  BoundingTest.m
//	TDD Framework 
//
//	http://www.flipcode.com/archives/2D_OBB_Intersection.shtml
//	http://blog.csdn.net/hortor_iphone/article/details/6653403
//	http://api.cocos.com/dc/d7a/classcocos2d_1_1_o_b_b.html
#include "BoundingTest.h"
#include "TDDHelper.h"
#include "StringHelper.h"
#include "Player.h"
#include "VisibleRect.h"
#include "Enemy.h"
#include "EnemyFactory.h"

#define kTagBox	12345

void BoundingTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	// EnemyFactory::instance()->loadData();
	
	addTouchListener();
	
	Player *player = Player::create();
	player->setPosition(VisibleRect::center());
	addChild(player);
	
	mPlayer = player;
	
	addEnemies();
}


void BoundingTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

void BoundingTest::clearAll()
{
	
	Vector<Node *> nodes = getChildren();
	for(int i=0; i<nodes.size(); i++) {
		Node *node = nodes.at(i);
		
		if(node->getTag() == kTagBox) {
			node->removeFromParent();
		}
	}
}

void BoundingTest::addEnemies()
{
	mEnemyList.clear();
	
	Enemy *enemy;
	Vec2 pos;
	
	enemy = EnemyFactory::instance()->createEnemy(2);
	enemy->update(0);
	enemy->changeToActive();
	pos = VisibleRect::center();	//+ Vec2(-30, -50);
	enemy->setPosition(pos);
	
	addChild(enemy);
	
	mEnemyList.pushBack(enemy);
}

Layout *BoundingTest::addBox(const Vec2 &point, const Size &size, const Color3B &color)
{
	Layout *box = Layout::create();
	
	box->setAnchorPoint(Vec2(0.5f, 0.5f));
	box->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	box->setBackGroundColor(color);
	box->setContentSize(size);
	box->setPosition(point);
	box->setTag(kTagBox);
	
	addChild(box);
	
	float dotSize = 2;
	LayerColor *redDot = LayerColor::create(Color4B::RED, dotSize, dotSize);
	Vec2 pos = Vec2(size.width/2, size.height/2);
	redDot->setPosition(pos);
	box->addChild(redDot);
	
	
	
	return box;
}



void BoundingTest::movePlayer(const Vec2 &pos)
{
	if(mPlayer == nullptr) {
		return;
	}
	
	Vec2 finalPos = pos + Vec2(0, 20);

	mPlayer->setPosition(finalPos);
	
	checkCollision();
}

void BoundingTest::checkCollision()
{
	
	if(mPlayer == nullptr) {
		return;
	}
	
	int idx = 0;
	for(Enemy *enemy : mEnemyList)
	{
		if(mPlayer->isCollide(enemy)) {
			log("player collide with enemy %d", idx);
		}
		idx++;
	}
	
	
}



#pragma mark - Touch handling

void BoundingTest::addTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(BoundingTest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(BoundingTest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(BoundingTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(BoundingTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
}

bool BoundingTest::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event)
{
	log("onTouchBegan: %s", POINT_TO_STR(touch->getLocation()).c_str());

	movePlayer(touch->getLocation());
	
	return true;
}

void BoundingTest::onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event)
{
	log("onTouchBegan: %s", POINT_TO_STR(touch->getLocation()).c_str());
	
	movePlayer(touch->getLocation());
}

void BoundingTest::onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event)
{
}

void BoundingTest::onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event)
{
}



#pragma mark -
#pragma mark List of Sub Tests

void BoundingTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(BoundingTest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void BoundingTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	
	Layout *box1 = addBox(Vec2(0, 0), Size(50, 50), Color3B::BLUE);
	
	log("box1=%s", RECT_TO_STR(box1->getBoundingBox()).c_str());
	
	Layout *box2 = addBox(Vec2(100, 200), Size(50, 50), Color3B::GREEN);
	box2->setRotation(50);
	
	log("box2=%s", RECT_TO_STR(box2->getBoundingBox()).c_str());
	
}


#endif
