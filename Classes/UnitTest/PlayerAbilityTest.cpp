#ifdef ENABLE_TDD
//
//  PlayerAbilityTest.m
//	TDD Framework
//
//
#include "PlayerAbilityTest.h"
#include "TDDHelper.h"
#include "PlayerAbilityFactory.h"
#include "PlayerAbility.h"
#include "PlayerCharProfile.h"
#include "GameplaySetting.h"
#include "StageManager.h"
#include "DogManager.h"
#include "GameWorld.h"
#include "StringHelper.h"
#include "DogData.h"
#include "CharGameData.h"
#include "PlayerCharData.h"

#include "Player.h"
#include "ItemManager.h"
#include "ItemEffect.h"
#include "ItemEffectFactory.h"

namespace {
	PlayerCharProfile *createProfileWithAbility(const std::vector<std::string> &abilityList)
	{
		PlayerCharProfile *profile = PlayerCharProfile::create();
		
		
		
		PlayerCharData *playerData = PlayerCharData::create();
		playerData->setCurrentLevel(5);  // All levels
		profile->setPlayerCharData(playerData);
		
		CharGameData *gameData = CharGameData::create();
		gameData->parseAbilityList(abilityList);
		profile->setCharGameData(gameData);
		
		return profile;
	}
	
	
	GameplaySetting *createSetting(const std::vector<std::string> &abilityList, bool isGlobal)
	{
//		int remain = abilityList.size();
		
//		profile->updateGlobalGameplaySetting(globalSetting);
//		GameplaySetting *playerSetting = GameplaySetting::create();
//		profile->updateGameplaySetting(playerSetting);
		
		GameplaySetting *setting = GameplaySetting::create();
//		profile->updateGlobalGameplaySetting(globalSetting);
		std::vector<std::string> abilities;
		
		int total = (int) abilityList.size();
		int counter = 0;
		int totalCount = 0;
		for(std::string ability : abilityList) {
			counter++;
			totalCount++;
			abilities.push_back(ability);
			
			
			if(counter == 5 || total == totalCount) { // every 5 or the last ability
				//log("DEBUG: abilities=%s", VECTOR_TO_STR(abilities).c_str());
				// there are 5 abilities now
				PlayerCharProfile *profile = createProfileWithAbility(abilities);
				if(isGlobal) {
					profile->updateGlobalGameplaySetting(setting);
				} else {
					profile->updateGameplaySetting(setting);
				}
				
				// Reset
				abilities.clear();
				counter = 0;
				
				log("DEBUG: profile=%s", profile->infoAbilityList().c_str());
			}
		}
		
		
		return setting;
	}

}

void PlayerAbilityTest::setUp()
{
	log("TDD Setup is called");
}


void PlayerAbilityTest::tearDown()
{
	log("TDD tearDown is called");
}

void PlayerAbilityTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PlayerAbilityTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PlayerAbilityTest::defineTests()
{
	ADD_TEST(testBonusAttribute);
	ADD_TEST(testLongerBooster);
	ADD_TEST(testPowerUp);
	ADD_TEST(testSlowerMissile);
	ADD_TEST(testMainAttribute);
	ADD_TEST(testAddingSetting);
	ADD_TEST(testGlobalGameplaySetting);
	ADD_TEST(testMockAbility);
	ADD_TEST(testAbilityDesc);
	ADD_TEST(testSample);
	ADD_TEST(testAttributeBuff);
	ADD_TEST(testBonusReward);
	ADD_TEST(testGameplaySetting);
}

#pragma mark -
#pragma mark Sub Test Definition
void PlayerAbilityTest::testMainAttribute()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("maxSpeed/0.1");
	abilityList.push_back("maxSpeed/0.5");
	abilityList.push_back("stunReduction/0.1");
	abilityList.push_back("maxSpeed/0.4");
	abilityList.push_back("maxSpeed/0.1/global");
	abilityList.push_back("stunReduction/0.2/global");
	
	GameplaySetting *globalSetting = createSetting(abilityList, true);
	GameplaySetting *playerSetting = createSetting(abilityList, false);
	playerSetting->add(globalSetting);
	
	log("Global Setting:\n%s", globalSetting->toString().c_str());
	
	log("Player Setting:\n%s", playerSetting->toString().c_str());
	
	
	//
	playerSetting->setMaxSpeed(200);
	playerSetting->setStunReduction(0.5);
	playerSetting->setTurnSpeed(300);
	playerSetting->setAcceleration(100);
	
	
//	GameplaySetting::Attribute attributes[2] = {
//		GameplaySetting::Attribute::MaxSpeed,
//		GameplaySetting::Attribute::StunReduction
//	};
//	
//	for()
	float speed = playerSetting->getMainAttribute(GameplaySetting::MaxSpeed);
	float stunReduction = playerSetting->getMainAttribute(GameplaySetting::StunReduction);
	
	log("speed=%f", speed);			// expect: 200 + (0.5 + 0.1)% = 200 + 0.6% = 200+ 120 = 320
	log("stun=%f", stunReduction);	// expect: 0.5 + (0.1 + 0.2)% = 0.5 + 0.3% = 0.5 + 0.15 = 0.65
}


void PlayerAbilityTest::testAddingSetting()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("maxSpeed/0.1");
	abilityList.push_back("maxSpeed/0.5");
	abilityList.push_back("maxSpeed/0.1/global");
	abilityList.push_back("extraReward/100/global");
	
	PlayerCharProfile *profile = createProfileWithAbility(abilityList);
	
	log("gameData detail: %s", profile->getCharGameData()->toString().c_str());
	
	GameplaySetting *globalSetting = GameplaySetting::create();
	profile->updateGlobalGameplaySetting(globalSetting);
	
	GameplaySetting *playerSetting = GameplaySetting::create();
	profile->updateGameplaySetting(playerSetting);
	
	log("Before Adding Global:\n%s\n", playerSetting->toString().c_str());
	
	playerSetting->add(globalSetting);
	
	log("After Adding Global:\n%s\n", playerSetting->toString().c_str());
}

void PlayerAbilityTest::testGlobalGameplaySetting()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("maxSpeed/0.1");
	abilityList.push_back("maxSpeed/0.5");
	abilityList.push_back("stunReduction/0.3");
	abilityList.push_back("maxSpeed/0.4");
	abilityList.push_back("maxSpeed/0.15/global");
	abilityList.push_back("stunReduction/0.1/global");
	
	GameplaySetting *globalSetting = createSetting(abilityList, true);
	GameplaySetting *playerSetting = createSetting(abilityList, false);
	
	log("Global Setting:\n%s", globalSetting->toString().c_str());
	
	log("Player Setting:\n%s", playerSetting->toString().c_str());

}


void PlayerAbilityTest::testMockAbility()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("maxSpeed/0.1");
	abilityList.push_back("maxSpeed/0.2");
	abilityList.push_back("maxSpeed/0.3");
	abilityList.push_back("maxSpeed/0.4");
	abilityList.push_back("maxSpeed/0.4/global");
	
	
	PlayerCharProfile *profile = createProfileWithAbility(abilityList);
	
	log("gameData detail: %s", profile->getCharGameData()->toString().c_str());
	
}

void PlayerAbilityTest::testSample()
{
	log("this is a sample subTest");
	
	PlayerAbility *ability = PlayerAbilityFactory::instance()->create("bonusTip/1");
	log("ability=%s", ability->toString().c_str());
}

void PlayerAbilityTest::testGameplaySetting()
{
	PlayerAbility *ability = PlayerAbilityFactory::instance()->create("bonusTip/1");
	log("ability=%s", ability->toString().c_str());
	
	GameplaySetting *setting = GameplaySetting::create();
	ability->updateGameplaySetting(setting);
	
	log("GameSetting:%s\n", setting->toString().c_str());
}


void PlayerAbilityTest::testBonusReward()
{
	DogManager::instance()->selectDog(1);
	DogData *dog = DogManager::instance()->getSelectedDogData();
	log("%s", dog->toString().c_str());
	log("dog: %s", dog->infoAbilityList().c_str());
	
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	
	StageManager::instance()->setGameplaySetting(setting);
	// StageManager::instance()->updateGameplaySetting();		// Setting are changed
	
	
	log("GameSetting:%s\n", setting->toString().c_str());

	
	// 
}


void PlayerAbilityTest::testBonusAttribute()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("attribute/maxSpeed,0.1");
	abilityList.push_back("attribute/stunReduction,0.1");
	abilityList.push_back("attribute/acceleration,0.1");
	abilityList.push_back("attribute/turnSpeed,0.1");
	abilityList.push_back("none");

	
	GameplaySetting *playerSetting = createSetting(abilityList, false);
	
	log("Player Setting:\n%s", playerSetting->toString().c_str());
	
	
}




void PlayerAbilityTest::testAttributeBuff()
{
	Vector<PlayerAbility *> abilityList;
	
	// Setting the list
	PlayerAbility *ability;
	
	// maxSpeed percentage
	ability = PlayerAbilityFactory::instance()->create("attribute");
	ability->parse("maxSpeed,0.1");
	abilityList.pushBack(ability);
	
	//
	GameplaySetting *setting = GameplaySetting::create();
	
	for(PlayerAbility *ability : abilityList) {
		ability->updateGameplaySetting(setting);
	}
	
	
	
	log("GameSetting:%s\n", setting->toString().c_str());
	
	
	//
}


void PlayerAbilityTest::testAbilityDesc()
{
//	Vector<PlayerAbility *> abilityList;
//	
//	// Setting the list
//	PlayerAbility *ability;
	std::vector<std::string> abilityList;
	
	abilityList.push_back("doubleStarChance/30");
	abilityList.push_back("extraReward/20");
	abilityList.push_back("extraTime/1");
	abilityList.push_back("bonusTip/50");
	abilityList.push_back("ratingBonus/5,20");

	for(std::string abilityStr : abilityList) {
		PlayerAbility *ability = PlayerAbilityFactory::instance()->create(abilityStr);
		log("%s: %s", abilityStr.c_str(), ability->getDesc().c_str());
	}
}


#pragma mark - Booster & PowerUp
void PlayerAbilityTest::testSlowerMissile()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("booster/slowerMissile,0.1");
	abilityList.push_back("booster/slowerMissile,0.5");
	abilityList.push_back("booster/slowerCyclone,0.2");
	abilityList.push_back("booster/slowerCyclone,0.4");
	abilityList.push_back("none");
	
	
	SpeedData missileSpeed;
	SpeedData cycloneSpeed;
	
	GameWorld::instance()->getPlayer()->setSpeed(200);
	
//	SpeedData getMissileSpeedData();
//	SpeedData getCycloneSpeedData();
	// ---------------
	log("Without Ability");
	log("------------------------");
	GameWorld::instance()->setGameplaySetting(GameplaySetting::create());
	ItemManager::instance()->setupBoosterGameSetting();
	
	missileSpeed = ItemManager::instance()->getMissileSpeedData();
	cycloneSpeed = ItemManager::instance()->getCycloneSpeedData();
	log("missileSpeed=%s", SPEED_DATA_STR(missileSpeed).c_str());
	log("cycloneSpeed=%s", SPEED_DATA_STR(cycloneSpeed).c_str());
	

	
	// ---------------
	log("With Ability");
	log("------------------------");
	GameplaySetting *setting = createSetting(abilityList, false);
	GameWorld::instance()->setGameplaySetting(setting);
	ItemManager::instance()->setupBoosterGameSetting();
	
	missileSpeed = ItemManager::instance()->getMissileSpeedData();
	cycloneSpeed = ItemManager::instance()->getCycloneSpeedData();
	log("missileSpeed=%s", SPEED_DATA_STR(missileSpeed).c_str());
	log("cycloneSpeed=%s", SPEED_DATA_STR(cycloneSpeed).c_str());
	
	

}



void PlayerAbilityTest::testPowerUp()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("powerUp/longerSpeedUp,1");		// 1 sec
	abilityList.push_back("powerUp/fasterSpeedUp,0.5");		// 50%
	abilityList.push_back("powerUp/longerCashOut,2");		// 2 sec
	abilityList.push_back("powerUp/longerMagnet,3");		// 3 sec
	
	std::vector<ItemEffect::Type> effectList;
	effectList.push_back(ItemEffect::Type::ItemEffectSpeedUp);
	effectList.push_back(ItemEffect::Type::ItemEffectCashOut);
	effectList.push_back(ItemEffect::Type::ItemEffectStarMagnet);

	// ---------------
	log("Without Ability");
	log("------------------------");

	// Without any ability
	GameWorld::instance()->setGameplaySetting(GameplaySetting::create());
	for(ItemEffect::Type effectType : effectList) {
		// Effect
		ItemEffect *effect = ItemEffectFactory::createEffectByType(effectType);
		effect->setIsPowerUp(true);
		
		ItemManager::instance()->defineItemEffectProperty(effect);
	
		log("%s", effect->toString().c_str());
	}
	
	
	
	// ---------------
	log("With Ability");
	log("------------------------");
	
	GameplaySetting *setting = createSetting(abilityList, false);
	GameWorld::instance()->setGameplaySetting(setting);
	log("GameplaySetting: %s", setting->toString().c_str());
	
	// Without any ability
	for(ItemEffect::Type effectType : effectList) {
		// Effect
		ItemEffect *effect = ItemEffectFactory::createEffectByType(effectType);
		effect->setIsPowerUp(true);
		
		ItemManager::instance()->defineItemEffectProperty(effect);
		
		log("%s", effect->toString().c_str());
	}
	
	
}

void PlayerAbilityTest::testLongerBooster()
{
	std::vector<std::string> abilityList;
	abilityList.push_back("booster/longerTimeStop,1");		// 1 sec
	abilityList.push_back("booster/longerTimeStop,3");		// 3 sec
	abilityList.push_back("booster/longerShield,2");		// 2 sec
	abilityList.push_back("booster/longerShield,3");		// 3 sec
	abilityList.push_back("none");
	
	std::vector<ItemEffect::Type> effectList;
	effectList.push_back(ItemEffect::Type::ItemEffectTimeStop);
	effectList.push_back(ItemEffect::Type::ItemEffectShield);
	
	
	// ---------------
	log("Without Ability");
	log("------------------------");
	
	// Without any ability
	GameWorld::instance()->setGameplaySetting(GameplaySetting::create());
	for(ItemEffect::Type effectType : effectList) {
		// Effect
		ItemEffect *effect = ItemEffectFactory::createEffectByType(effectType);
		ItemManager::instance()->defineItemEffectProperty(effect);
		
		log("%s", effect->toString().c_str());
	}
	
	
	
	// ---------------
	log("With Ability");
	log("------------------------");
	
	GameplaySetting *setting = createSetting(abilityList, false);
	GameWorld::instance()->setGameplaySetting(setting);
	log("GameplaySetting: %s", setting->toString().c_str());
	
	// Without any ability
	for(ItemEffect::Type effectType : effectList) {
		// Effect
		ItemEffect *effect = ItemEffectFactory::createEffectByType(effectType);
		
		ItemManager::instance()->defineItemEffectProperty(effect);
		
		log("%s", effect->toString().c_str());
	}
	

}


#endif
