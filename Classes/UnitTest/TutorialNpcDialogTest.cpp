#ifdef ENABLE_TDD
//
//  TutorialNpcDialogTest.m
//	TDD Framework
//
//
#include "TutorialNpcDialogTest.h"
#include "TDDHelper.h"
#include "TutorialNpcDialog.h"

void TutorialNpcDialogTest::setUp()
{
	log("TDD Setup is called");
	
	mDialog = TutorialNpcDialog::create();
	addChild(mDialog);
}


void TutorialNpcDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void TutorialNpcDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void TutorialNpcDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void TutorialNpcDialogTest::defineTests()
{
	ADD_TEST(testShow);
	ADD_TEST(testClose);
}

#pragma mark -
#pragma mark Sub Test Definition
void TutorialNpcDialogTest::testShow()
{
	mDialog->showDialog("Testing 1\nTesting 2");
}

void TutorialNpcDialogTest::testClose()
{
	mDialog->closeDialog();
}

#endif
