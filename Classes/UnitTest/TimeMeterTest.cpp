#ifdef ENABLE_TDD
//
//  TimeMeterTest.m
//	TDD Framework
//
//
#include "TimeMeterTest.h"
#include "TDDHelper.h"
#include "TimeMeter.h"
#include <chrono>
#include <iostream>

void TimeMeterTest::setUp()
{
	log("TDD Setup is called");
}


void TimeMeterTest::tearDown()
{
	log("TDD tearDown is called");
}

void TimeMeterTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void TimeMeterTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void TimeMeterTest::defineTests()
{
	ADD_TEST(testTime);
	ADD_TEST(testSummary);
	ADD_TEST(testHighTime);
	ADD_TEST(testStartStop);
}

#pragma mark -
#pragma mark Sub Test Definition
void TimeMeterTest::testTime()
{
	auto timeNow = std::chrono::system_clock::now().time_since_epoch();
	
	std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow);
	
	long long timeMillis = ms.count();
	
	log("timeMillis: %lld", timeMillis);
}

void TimeMeterTest::testHighTime()
{
//	std::chrono::time_point<system_clock> highClock = std::chrono::high_resolution_clock::now();
//	std::chrono::duration<double> time = highClock;
//	
//	log("time: %f", time);
}


void TimeMeterTest::testStartStop()
{
	for(int x=1; x<=3; x++) {
		int nLoop = 1000000 * x;
		
		std::string key = StringUtils::format("%dSqrt", nLoop);
		TimeMeter::instance()->start(key);
		
		
		for(int i=0; i<nLoop; i++) {
			sqrt(1.0f);
		}
		
		TimeMeter::instance()->stop();
	}
	
	
}

void TimeMeterTest::testSummary()
{
	for(int x=1; x<=3; x++) {
		int nLoop = 1000000 * x;
		
		std::string key = StringUtils::format("%dSqrt", nLoop);
		TimeMeter::instance()->start(key);
		
		
		for(int i=0; i<nLoop; i++) {
			sqrt(1.0f);
		}
		
		TimeMeter::instance()->stop();
	}

	log("Summary:\n%s\n", TimeMeter::instance()->summary().c_str());
	
}

#endif
