#ifdef ENABLE_TDD
//
//  AstrodogMailTest.h
//
//
#ifndef __TDD_AstrodogMailTest__
#define __TDD_AstrodogMailTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class AstrodogMailTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testObjects();
};

#endif

#endif
