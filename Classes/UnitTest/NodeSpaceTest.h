#ifdef ENABLE_TDD
//
//  NodeSpaceTest.h
//
//
#ifndef __TDD_NodeSpaceTest__
#define __TDD_NodeSpaceTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class NodeSpaceTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
