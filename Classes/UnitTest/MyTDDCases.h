//
//  MyTDDCases.h
//  Cocos2dxTDDLib
//
//	This header should be included by TDDCases.h only
//  Created by Ken Lee on 25/5/14.
//
//


// Add the Header here!
#include "SettingSceneTest.h"
#include "WebViewTest.h"
#include "RateAppTest.h"
#include "AdManagerTest.h"
#include "SuitSceneTest.h"
#include "TDDSample.h"
#include "KeyTest.h"
#include "SpriteTest.h"
#include "DrawNodeTest.h"
#include "GameMapTest.h"
#include "RandomHelperTest.h"
#include "MovableModelTest.h"
#include "GameWorldTest.h"
#include "Vec2Test.h"
#include "GeometryHelperTest.h"
#include "MapDataGeneratorTest.h"
#include "StdDataTest.h"
#include "MainSceneTest.h"
#include "DialogTest.h"
#include "ItemTest.h"
#include "ModelLayerTest.h"
#include "TiledTest.h"
#include "LevelDataLoaderTest.h"
#include "GameSoundTest.h"
#include "TutorialViewTest.h"
#include "DebugMapTest.h"
#include "StringHelperTest.h"
#include "ResponsiveUITest.h"
#include "PauseDialogTest.h"
#include "ParallaxTest.h"
#include "GameModelTest.h"
#include "ModelGalleryTest.h"
#include "ShaderHelperTest.h"
#include "ParticleTest.h"
#include "JSONTest.h"
#include "MasteryTest.h"
#include "CocosViewTest.h"
#include "GameUITest.h"
#include "GameSceneTest.h"
#include "MasterySceneTest.h"
#include "IAPManagerTest.h"
#include "BoundingTest.h"
#include "GameCenterTest.h"
#include "EnemyBehaviourTest.h"
#include "ItemEffectTest.h"
#include "EnemyTest.h"
#include "SuitManagerTest.h"
#include "GeometryVisualTest.h"
#include "EffectLayerTest.h"
#include "TouchTest.h"
#include "GameTouchLayerTest.h"
#include "ShareHelperTest.h"
#include "AnimationHelperTest.h"
#include "PlayerTest.h"
#include "ShareImageTest.h"
#include "GmSceneTest.h"
#include "TutorialTest.h"
#include "ViewHelperTest.h"
#include "AnalyticsTest.h"
#include "CreditTest.h"
#include "AdColonyTest.h"
#include "AdHandlerTest.h"
#include "UserDefaultTest.h"
#include "AndroidHelperTest.h"
#include "DeviceHelperTest.h"
#include "PlayerRecordTest.h"
#include "DogManagerTest.h"
#include "ConsoleViewTest.h"
#include "GmDogSelectDialogTest.h"

#include "PlayTimeTest.h"

#include "GameContinueDialogTest.h"
#include "UIPageViewTest.h"
#include "PlanetItemViewTest.h"
#include "PlanetManagerTest.h"
#include "DogSelectItemViewTest.h"
#include "DogSelectSceneTest.h"
#include "ListViewTest.h"
#include "StencilTest.h"
#include "MaskNodeTest.h"
#include "AwardStarLayerTest.h"
#include "RouteLineTest.h"
#include "CocosViewTest.h"
#include "AnimeGalleryTest.h"
#include "UnlockDogLayerTest.h"
#include "FBAnalyticsTest.h"
#include "ReviewTest.h"
#include "HttpTest.h"
#include "UpdateCheckerTest.h"
#include "DogSuggestionLayerTest.h"
#include "BatchTest.h"
#include "RenderTest.h"
#include "DailyRewardTest.h"
#include "DailyRewardDialogTest.h"
#include "AnimeNodeTest.h"

#include "UIRichTextTest.h"
#include "RichTextParserTest.h"
#include "FireworkTest.h"
#include "DogInfoLayerTest.h"
#include "UIBestDistanceTest.h"
#include "TutorialLayerTest.h"

#include "EnemyFactoryTest.h"
#include "GameMapTest.h"
#include "SnowBallTest.h"
#include "MoveLogicTest.h"
#include "CoinShopTest.h"
#include "GameResTest.h"
#include "FacebookTest.h"
#include "ADResponseGetRankingTest.h"
#include "AstrodogClientTest.h"
#include "AstrodogDataTest.h"
#include "AstrodogClientUITest.h"
#include "AsyncSpriteTest.h"
#include "FBProfilePicTest.h"

#include "TimeHelperTest.h"

#include "ItemManagerTest.h"
#include "BoosterDataTest.h"
#include "BoosterControlTest.h"

#include "LeaderboardUITest.h"
#include "LuckyDrawLayerTest.h"
#include "ItemEffectIndicatorTest.h"
#include "LuckyDrawManagerTest.h"

#include "MapDataTest.h"
#include "HitboxTest.h"
#include "AstrodogMailTest.h"
#include "MailBoxLayerTest.h"

#include "StageParallaxTest.h"
#include "BehaviourTest.h"
#include "NPCOrderUITest.h"
#include "NPCRatingUITest.h"

#include "NPCTest.h"
#include "StageManagerTest.h"

#include "NPCMapLogicTest.h"

#include "CommonDialogTest.h"

#include "PackageBoxTest.h"
#include "EmojiAnimeNodeTest.h"
#include "DeliveryCompleteNoticeViewTest.h"
#include "NPCManagerTest.h"
#include "MotionStreakTest.h"
#include "PlayerAbilityTest.h"
#include "BoosterEffectTest.h"
#include "NPCChanceTest.h"
#include "CharGameDataTest.h"
#include "PlayerManagerTest.h"
#include "PositionRecorderTest.h"
#include "HorizontalListViewTest.h"
#include "StageSelectionViewTest.h"
#include "SpeedEffectTest.h"
#include "DataHelperTest.h"
#include "NpcDistanceTest.h"
#include "EndGameTest.h"
#include "CheckPointDialogTest.h"
#include "TutorialNpcDialogTest.h"
#include "TitleSceneTest.h"
#include "AdmobTest.h"
#include "FacebookNativeTest.h"
#include "NotEnoughMoneyDialogTest.h"
#include "InMobiTest.h"
#include "RenderItemTest.h"
#include "LeaderboardLayerTest.h"
#include "TimeMeterTest.h"

//#include#
// NOTE!!! The above line is used to generate new unit test, must not be removed.

// Define
TDD_CASES
{
	TEST(NotEnoughMoneyDialogTest),
	TEST(TimeMeterTest),
	TEST(LeaderboardLayerTest),
	TEST(RenderItemTest),
	TEST(InMobiTest),
	TEST(TitleSceneTest),
	TEST(FacebookNativeTest),
	TEST(AdmobTest),
	TEST(TutorialNpcDialogTest),
	TEST(CheckPointDialogTest),
	TEST(EndGameTest),
	TEST(NpcDistanceTest),
	TEST(SpeedEffectTest),
	TEST(DataHelperTest),
	TEST(StageSelectionViewTest),
	TEST(HorizontalListViewTest),
	TEST(PositionRecorderTest),
	TEST(PlayerManagerTest),
	TEST(PlayerAbilityTest),
	TEST(CharGameDataTest),
	TEST(BoosterEffectTest),
	TEST(NPCChanceTest),
	TEST(MotionStreakTest),
	TEST(NPCManagerTest),
    TEST(DeliveryCompleteNoticeViewTest),
	TEST(NPCMapLogicTest),
	TEST(EmojiAnimeNodeTest),
	TEST(PackageBoxTest),
	TEST(CommonDialogTest),
	TEST(StageManagerTest),
    TEST(NPCRatingUITest),
    TEST(NPCOrderUITest),

    TEST(MailBoxLayerTest),
	TEST(NPCTest),
	TEST(StageParallaxTest),
	TEST(BehaviourTest),
	TEST(AstrodogMailTest),
    TEST(ItemEffectIndicatorTest),
	TEST(MapDataTest),
	TEST(HitboxTest),
	TEST(LuckyDrawManagerTest),
    TEST(BoosterControlTest),
	TEST(LeaderboardUITest),
	TEST(LuckyDrawLayerTest),
    TEST(TimeHelperTest),
    TEST(UIBestDistanceTest),
	TEST(BoosterDataTest),
	TEST(ItemManagerTest),
	TEST(FBProfilePicTest),
	TEST(AsyncSpriteTest),
	TEST(AstrodogClientUITest),
	TEST(AstrodogDataTest),
	TEST(AstrodogClientTest),
	TEST(ADResponseGetRankingTest),
	TEST(FacebookTest),
	TEST(CoinShopTest),
	TEST(GameResTest),
	TEST(SnowBallTest),
	TEST(MoveLogicTest),
	TEST(EnemyFactoryTest),
    TEST(UIRichTextTest),
    TEST(UnlockDogLayerTest),
	TEST(TutorialLayerTest),
	TEST(RichTextParserTest),
	TEST(DogInfoLayerTest),
	TEST(DailyRewardTest),
	TEST(FireworkTest),
	TEST(AnimeNodeTest),
	TEST(DailyRewardDialogTest),
	TEST(DogSuggestionLayerTest),
	TEST(UpdateCheckerTest),
	TEST(RenderTest),
	TEST(HttpTest),
	TEST(BatchTest),
	TEST(ReviewTest), 
    TEST(AwardStarLayerTest),
	TEST(FBAnalyticsTest),
	TEST(AnimeGalleryTest),
    TEST(PlayTimeTest),
	TEST(ListViewTest),
	TEST(RouteLineTest),
	TEST(CocosViewTest),
	TEST(MaskNodeTest),
	TEST(StencilTest),
	TEST(DogSelectSceneTest),
    TEST(SettingSceneTest),
	TEST(DogSelectItemViewTest),
	TEST(PlanetManagerTest),
	TEST(PlanetItemViewTest),
	TEST(UIPageViewTest), 
	TEST(GameContinueDialogTest),
	TEST(GmDogSelectDialogTest),
	TEST(ConsoleViewTest),
	TEST(DogManagerTest),
	TEST(DeviceHelperTest),
	TEST(PlayerRecordTest),
    TEST(WebViewTest),
	TEST(AndroidHelperTest),
	TEST(UserDefaultTest),
	TEST(AdHandlerTest),
	TEST(AdColonyTest),
	TEST(CreditTest),
	TEST(AnalyticsTest),
	TEST(ViewHelperTest),
    TEST(RateAppTest),
	TEST(TutorialTest),
    TEST(AdManagerTest),
	TEST(GmSceneTest),
	TEST(ModelGalleryTest),
	TEST(GameTouchLayerTest),
	TEST(SuitSceneTest),
    TEST(EffectLayerTest),
	TEST(ShareHelperTest),
	TEST(PlayerTest),
	TEST(ShareImageTest),
	TEST(AnimationHelperTest),
	TEST(TouchTest),
	TEST(JSONTest),
	TEST(GeometryVisualTest),
	TEST(EnemyTest),
	TEST(ItemEffectTest),
	TEST(SuitManagerTest),
	TEST(EnemyBehaviourTest),
	TEST(IAPManagerTest),
	TEST(BoundingTest),
    TEST(GameCenterTest),
	TEST(GameSceneTest),
	TEST(GameUITest),
	TEST(MasterySceneTest),
	TEST(MasteryTest),
	TEST(DebugMapTest),
	TEST(CocosViewTest),
	TEST(ParticleTest),
	TEST(ShaderHelperTest),
	TEST(GameModelTest),
	TEST(ParallaxTest), 
	TEST(PauseDialogTest),
	TEST(ResponsiveUITest),
	TEST(StringHelperTest),
	TEST(KeyTest),
	TEST(ItemTest),
	TEST(TutorialViewTest),
	TEST(GameSoundTest),
	TEST(LevelDataLoaderTest),
	TEST(TiledTest),
	TEST(DialogTest),
	TEST(ModelLayerTest),
	TEST(MainSceneTest),
	TEST(StdDataTest),
	TEST(MapDataGeneratorTest),
	TEST(Vec2Test),
	TEST(GeometryHelperTest),
	TEST(GameWorldTest),
	TEST(MovableModelTest),
	TEST(RandomHelperTest),
	TEST(GameMapTest),
	TEST(TDDSample),
	TEST(SpriteTest),
	TEST(DrawNodeTest),
//#testcase#
// NOTE!!! The above line is used to generate new unit test, must not be removed.
};


