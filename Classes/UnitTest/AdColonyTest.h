#ifdef ENABLE_TDD
//
//  AdColonyTest.h
//
//
#ifndef __TDD_AdColonyTest__
#define __TDD_AdColonyTest__

// Include Header

#include "TDDTest.h"

class AdColonyHandler;

// Class Declaration 
class AdColonyTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testVideo1(Ref *sender);
	void testVideo2(Ref *sender);
	
private:
	AdColonyHandler *mHandler;
}; 

#endif

#endif
