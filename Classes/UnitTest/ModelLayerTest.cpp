#ifdef ENABLE_TDD
//
//  ModelLayerTest.m
//	TDD Framework 
//
//
#include "ModelLayerTest.h"
#include "TDDHelper.h"
#include "Item.h"
#include "ModelLayer.h"
#include "Coin.h"
#include "Player.h"
#include "Star.h"
#include "ViewHelper.h"
#include "EnemyFactory.h"
#include "Enemy.h"
#include "Missile.h"
#include "VisibleRect.h"
#include "StringHelper.h"
#include "GameWorld.h"
#include "Star.h"

void ModelLayerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void ModelLayerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ModelLayerTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(ModelLayerTest::step);
	SUBTEST(ModelLayerTest::testMissile);
	SUBTEST(ModelLayerTest::testAddItem);
	SUBTEST(ModelLayerTest::testMoveUp);
	SUBTEST(ModelLayerTest::testVisibleStar);
	SUBTEST(ModelLayerTest::testMoveDown);
	SUBTEST(ModelLayerTest::testGetNearestEnemy);
	SUBTEST(ModelLayerTest::testShowAlertNearPlayer);
	SUBTEST(ModelLayerTest::testMovingStar);
	
}

#pragma mark -
#pragma mark Sub Test Definition
void ModelLayerTest::testAddItem(Ref *sender)
{
	log("this is a sample subTest");

	ModelLayer *layer = ModelLayer::create();
	addChild(layer);
	
	Coin *coin;
    coin = Coin::create(Coin::SmallCoin);
	coin->setPosition(Vec2(0, 100));
	layer->addItem(coin);
	
	coin = Coin::create(Coin::SmallCoin);
	coin->setPosition(Vec2(50, 100));
	layer->addItem(coin);

	coin = Coin::create(Coin::SmallCoin);
	coin->setPosition(Vec2(150, 100));
	layer->addItem(coin);

	log("initial list");
	log("layerInfo: %s", layer->info().c_str());
	
	
	// remove last coin
	layer->removeItem(coin);

	log("remove last coin");
	log("layerInfo: %s", layer->info().c_str());

	// remove all coins
	layer->removeAllItems();
	
	log("remove all");
	log("layerInfo: %s", layer->info().c_str());

	
	mModelLayer = layer;
	for(int y=0; y<500; y+=100) {
        Star *star = Star::create(Star::SmallStar);
		star->setPosition(Vec2(50, y));
		
		mModelLayer->addItem(star);
	}
	
	
}

void ModelLayerTest::testMoveUp(Ref *sender)
{
	mCameraY += 30;
	mModelLayer->setOffsetY(mCameraY);
	log("CameraY=%f", mCameraY);
}

void ModelLayerTest::testMoveDown(Ref *sender)
{
	mCameraY -= 30;
	mModelLayer->setOffsetY(mCameraY);
	log("CameraY=%f", mCameraY);
}

void ModelLayerTest::testShowAlertNearPlayer(Ref *sender)
{
	Player *player = mModelLayer->getPlayer();
	player->setPosition(Vec2(50, 150));

	Vec2 alertPos = player->getPosition();
	alertPos.y += 50;
	
	ViewHelper::showFadeAlert(mModelLayer, "Double Star!!", alertPos);	
}

// Testing Step:
//		Create the modelLayer
//		Add the player
//		Add some enemy
//		Call the method
//		Verify the result
void ModelLayerTest::testGetNearestEnemy(Ref *sender)
{
	const int nEnemy = 3;
	Vec2 enemyPos[nEnemy] = {
		Vec2(100, 100),
		Vec2(150, 180),
		Vec2(200, 300),
	};
	
	
	// Prepare the layer and enemy
	ModelLayer *layer = ModelLayer::create();
	for(int i=0; i<nEnemy; i++) {
		Vec2 pos = enemyPos[i];
		
		Enemy *enemy = EnemyFactory::instance()->createEnemy(1);
		enemy->changeState(Enemy::State::Active);
		enemy->setPosition(pos);
		layer->addEnemy(enemy);
	}
	
	// Define the player
	Player *player = Player::create();
	player->setPosition(Vec2(150, 150));
	layer->setPlayer(player);
	
	// Get the result
	Vector<Enemy *> nearest = layer->getNearestEnemy(300, 20);
	
	log("Nearest enemy: cound=%ld", nearest.size());
	
	Vector<Enemy *>::iterator it = nearest.begin();
	for(; it != nearest.end(); it++) {
		Enemy *e = *it;
		log("%s", e->toString().c_str());
	}
	
}


void ModelLayerTest::testMissile(Ref *sender)
{
	const int nEnemy = 3;
	Vec2 enemyPos[nEnemy] = {
		Vec2(100, 500),
		Vec2(150, 500),
		Vec2(200, 500),
	};
	
	
	// Prepare the layer and player
	ModelLayer *layer = ModelLayer::create();
	addChild(layer);
	GameWorld::instance()->setModelLayer(layer);
	mModelLayer = layer;
	
	
	// Prepare enemy
	Enemy *targetEnemy = nullptr;
	for(int i=0; i<nEnemy; i++) {
		Vec2 pos = enemyPos[i];
		
		Enemy *enemy = EnemyFactory::instance()->createEnemy(1);
		enemy->changeState(Enemy::State::Active);
		enemy->setPosition(pos);
		layer->addEnemy(enemy);
		
		targetEnemy = enemy;	// the last one
	}
	
	// Prepare the missile
	Missile *missile = Missile::create();
	missile->setPosition(VisibleRect::center());
	missile->setTargetEnemy(targetEnemy);
	
	layer->addItem(missile);
	
	log("missile.box=%s", RECT_TO_STR(missile->getBoundingBox()).c_str());
	
}

void ModelLayerTest::step(Ref *sender)
{
	if(mModelLayer) {
		mModelLayer->update(0.1);
	}
}



void ModelLayerTest::testMovingStar(Ref *sender)
{
	const int nStar = 3;
	Vec2 starPos[nStar] = {
		Vec2(100, 100),
		Vec2(150, 180),
		Vec2(200, 300),
	};
	
	
	// Prepare the layer and player
	ModelLayer *layer = ModelLayer::create();
	addChild(layer);
	GameWorld::instance()->setModelLayer(layer);
	mModelLayer = layer;
	
	
	// Prepare enemy
	Star *targetStar = nullptr;
	for(int i=0; i<nStar; i++) {
		Vec2 pos = starPos[i];
		
        Star *star = Star::create(Star::SmallStar);
		star->setPosition(pos);
		layer->addItem(star);
		
		targetStar = star;	// the last one
	}
	
	targetStar->setMoveToPlayer(true);
}

void ModelLayerTest::testVisibleStar(Ref *sender)
{
	const int nStar = 5;
	Vec2 starPos[nStar] = {
		Vec2(100, 100),
		Vec2(150, 180),
		Vec2(200, 300),
		Vec2(200, 1300),
		Vec2(200, 900),
	};
	
	
	
	ModelLayer *layer = ModelLayer::create();
	for(int i=0; i<nStar; i++) {
		Vec2 pos = starPos[i];
		
		Star *star = Star::create(Star::SmallStar);
		star->setPosition(pos);
		layer->addItem(star);
	}
	
	
	Vector<Collectable *> starList = layer->getVisibleCollectableList();
	for(Collectable *star : starList) {
		
		log("star=%s", star->toString().c_str());
	}
}

#endif
