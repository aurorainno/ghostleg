#ifdef ENABLE_TDD
//
//  UIPageViewTest.h
//
//
#ifndef __TDD_UIPageViewTest__
#define __TDD_UIPageViewTest__

// Include Header

#include "TDDBaseTest.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

// Class Declaration
class UIPageViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	cocos2d::ui::Layout *createPlanetGUI(int planetID);
	
	void modifyPageOpacity(ui::PageView *pageView, int page, int opacity);
	
private:
	void testSample();
	void testWithPlanet();
	void testPlanet();
	void testAILPageView();
	void testSizedPageSize();
};

#endif

#endif
