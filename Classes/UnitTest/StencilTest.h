#ifdef ENABLE_TDD
//
//  StencilTest.h
//
//
#ifndef __TDD_StencilTest__
#define __TDD_StencilTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class StencilTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

	void drawPie(DrawNode *node, const Vec2 &origin, float radius, float angle);
private:
	void testDrawPie();
	void testSample();
};

#endif

#endif
