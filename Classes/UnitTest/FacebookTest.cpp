#ifdef ENABLE_TDD
//
//  FacebookTest.m
//	TDD Framework
//
//
#include "FacebookTest.h"
#include "TDDHelper.h"
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include "network/HttpRequest.h"
#include "network/HttpClient.h"
#include "JSONHelper.h"

using namespace sdkbox;

class SpriteEx : public Sprite
{
public:
    static SpriteEx* createWithUrl(const std::string& url) {
        SpriteEx* sprite = new SpriteEx();
        sprite->autorelease();
        sprite->init();
        sprite->updateWithUrl(url);
        return sprite;
    }
    static SpriteEx* create() {
        SpriteEx* sprite = new SpriteEx();
        sprite->autorelease();
        sprite->init();
        return sprite;
    }
    
    virtual bool init() {
        _image_size.x = 50;
        _image_size.y = 50;
        return Sprite::init();
    }
    
    void setSize(Vec2 s) {
        _image_size = s;
    }
    
    void updateWithUrl(const std::string& url) {
        network::HttpRequest* request = new network::HttpRequest();
        request->setUrl(url.data());
        request->setRequestType(network::HttpRequest::Type::GET);
        request->setResponseCallback([=](network::HttpClient* client, network::HttpResponse* response) {
            if (!response->isSucceed()) {
                CCLOG("ERROR, remote sprite load image failed");
                return ;
            }
            
            std::vector<char> *buffer = response->getResponseData();
            Image img;
            img.initWithImageData(reinterpret_cast<unsigned char*>(&(buffer->front())), buffer->size());
            
            if (1)
            {
                // save image file to device.
                std::string path = FileUtils::getInstance()->getWritablePath()+"p.png";
                log("save image path = %s", path.data());
                bool ret = img.saveToFile(path);
                log("save file %s", ret ? "success" : "failure");
                
                this->initWithFile(path);
            } else {
                
                // create sprite with texture
                Texture2D *texture = new Texture2D();
                texture->autorelease();
                texture->initWithImage(&img);
                
                this->initWithTexture(texture);
                if (0 != _image_size.x) {
                    auto size = getContentSize();
                    setScaleX(_image_size.x/size.width);
                    setScaleY(_image_size.y/size.height);
                }
            }
        });
        network::HttpClient::getInstance()->send(request);
        request->release();
    }
    
private:
    cocos2d::Vec2 _image_size;
};


void FacebookTest::setUp()
{
    log("TDD Setup is called");
    
    auto winsize = Director::getInstance()->getWinSize();
    mIconSprite = SpriteEx::create();
    mIconSprite->setPosition(winsize / 2);
    addChild(mIconSprite);
}


void FacebookTest::tearDown()
{
    log("TDD tearDown is called");
}

void FacebookTest::willRunTest(const std::string &name)
{
    log("Before run %s", name.c_str());
}

void FacebookTest::didRunTest(const std::string &name)
{
    log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void FacebookTest::defineTests()
{
    ADD_TEST(testSample);
	ADD_TEST(testAPI);
	ADD_TEST(testInviteFriends);
    ADD_TEST(testGetUserData);
    ADD_TEST(testLogOut);
    ADD_TEST(testGetFriends);
    ADD_TEST(testIsLoggedIn);
}

#pragma mark -
#pragma mark Sub Test Definition
void FacebookTest::testSample()
{
    log("this is a sample subTest");
    std::vector<std::string> permissions;
    permissions.push_back(sdkbox::FB_PERM_READ_PUBLIC_PROFILE);
    permissions.push_back(sdkbox::FB_PERM_READ_USER_FRIENDS);
    permissions.push_back(sdkbox::FB_PERM_READ_EMAIL);
    
    sdkbox::PluginFacebook::login(permissions);
    //    FacebookListener* listener = new FacebookListener();
    PluginFacebook::setListener(this);
    
}

// Reference:
//		docs.sdkbox.com/en/plugins/facebook/v3-cpp/#methods
//			static void requestInvitableFriends ( const FBAPIParam & ) ;
//				 inviteFriendsWithInviteIds
//					inviteFriends
void FacebookTest::testInviteFriends()
{
//	sdkbox::PluginFacebook::login();	??

	
	// using:
	//		https://developers.facebook.com/quickstarts/1810253282635241/?platform=app-links-host
	std::string appLinkUrl = "https://fb.me/1831229720537597";
	std::string previewImageUrl = "http://aurorainnoapps.com:9280/static-doc//monsterEx/appIcon.png";

	PluginFacebook::inviteFriends(appLinkUrl, previewImageUrl);
}

void FacebookTest::testGetUserData()
{
    bool isLoggedIn = PluginFacebook::isLoggedIn();
    std::string userID = sdkbox::PluginFacebook::getUserID();
    std::string token = sdkbox::PluginFacebook::getAccessToken();
    log("userID=%s, token=%s, isLoggedIn=%d",userID.c_str(),token.c_str(),isLoggedIn);
}

void FacebookTest::testLogOut()
{
    PluginFacebook::logout();
}

void FacebookTest::testGetFriends()
{
    PluginFacebook::fetchFriends();
}

void FacebookTest::testIsLoggedIn()
{
    log("isLoggedIn: %d",PluginFacebook::isLoggedIn());
}

void FacebookTest::onInviteFriendsResult(bool result, const std::string &msg)
{
    log("====onInviteFriends===\nresult=%d msg=\n[%s]\n", result, msg.c_str());
}

void FacebookTest::onLogin(bool isLogin, const std::string& msg)
{
    log("onLoggedIn: isLogin=%d, msg=\n[%s]\n", isLogin, msg.c_str());
}

void FacebookTest::onFetchFriends(bool ok, const std::string& msg)
{
    const std::vector<sdkbox::FBGraphUser>& friends = PluginFacebook::getFriends();
    for (int i = 0; i < friends.size(); i++)
    {
        const sdkbox::FBGraphUser& user = friends.at(i);
        log("##FB> -------------------------------");
        log("##FB>> %s", user.uid.data());
        log("##FB>> %s", user.firstName.data());
        log("##FB>> %s", user.lastName.data());
        log("##FB>> %s", user.name.data());
        log("##FB>> %s", user.isInstalled ? "app is installed" : "app is not installed");
        log("##FB");
    }
}

void FacebookTest::testAPI()
{
	//std::string myFacebookID = sdkbox::
	std::string userID =PluginFacebook::getUserID();
	std::string accessToken = PluginFacebook::getAccessToken();
	log("DEBUG: userID=[%s] token=[%s]", userID.c_str(), accessToken.c_str());
	
	FBAPIParam param;
	//param["redirect"] = "false";
	std::string url = "/me";
	PluginFacebook::setListener(this);
	PluginFacebook::api(url, "GET", param, url);
	
}

void FacebookTest::onGetUserInfo( const sdkbox::FBGraphUser& userInfo )
{
    log("Facebook id:'%s' name:'%s' last_name:'%s' first_name:'%s' email:'%s' installed:'%d'",
          userInfo.getUserId().c_str(),
          userInfo.getName().c_str(),
          userInfo.getFirstName().c_str(),
          userInfo.getLastName().c_str(),
          userInfo.getEmail().c_str(),
          userInfo.isInstalled ? 1 : 0
          );
    
    sdkbox::FBAPIParam params;
    params["redirect"] = "false";
    params["type"] = "small";
    std::string url(userInfo.getUserId() + "/picture");
    PluginFacebook::api(url, "GET", params, "__fetch_picture_tag__");
}

void FacebookTest::onAPI(const std::string& tag, const std::string& jsonData)
{
    log("##FB onAPI: tag -> %s, json -> %s", tag.c_str(), jsonData.c_str());
	
	if("/me" == tag) {
		rapidjson::Document doc;
		doc.Parse(jsonData.c_str());
		
		std::string myName = aurora::JSONHelper::getString(doc, "name", "");
		log("MyName=%s", myName.c_str());
	}
	
    if (tag == "__fetch_picture_tag__") {
        
        rapidjson::Document doc;
        doc.Parse(jsonData.c_str());
        
        if (doc.HasParseError())
        {
            log("GetParseError %d\n", doc.GetParseError());
            return;
        }

        std::string url = doc["data"]["url"].GetString();
        log("picture's url = %s", url.data());
        
        mIconSprite->updateWithUrl(url);
    }
}

void FacebookTest::onSharedSuccess(const std::string& message){}
void FacebookTest::onSharedFailed(const std::string& message){}
void FacebookTest::onSharedCancel(){}
void FacebookTest::onPermission(bool isLogin, const std::string& msg){}
void FacebookTest::onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends ){}
void FacebookTest::onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg ){}




#endif
