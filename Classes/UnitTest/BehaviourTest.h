#ifdef ENABLE_TDD
//
//  BehaviourTest.h
//
//
#ifndef __TDD_BehaviourTest__
#define __TDD_BehaviourTest__

// Include Header

#include "TDDBaseTest.h"

class Enemy;
class GameModel;
class Player;
class Star;

// Class Declaration
class BehaviourTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	
	void setupModels();
	void setupPlayer();
	void setupItem();
	
	Enemy *createEnemy(int enemyID);
	
	void setupTouchListener();
	bool onTouchBegan(Touch *touch, Event *event);
	void onTouchEnded(Touch *touch, Event *event);
	void onTouchMoved(Touch *touch, Event *event);
	void onTouchCancelled(Touch *touch, Event *event);

	void update(float delta);
private:
	void testSample();
	
	
private:
	Layer *mMainLayer;
	std::vector<Enemy *> mEnemyList;
	Player *mPlayer;
	Star *mStar;
	Vec2 mLastPos;
};

#endif

#endif
