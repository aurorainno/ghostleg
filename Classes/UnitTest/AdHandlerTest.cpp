#ifdef ENABLE_TDD
//
//  AdHandlerTest.m
//	TDD Framework 
//
//
#include "AdHandlerTest.h"
#include "TDDHelper.h"
#include "AdmobHandler.h"
#include "AdColonyHandler.h"
#include "AdManager.h"

#include <vector>
#include <string>

void AdHandlerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	useAdmob(nullptr);
	
}


void AdHandlerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	CC_SAFE_RELEASE(mAdHandler);
}

#pragma mark -
#pragma mark List of Sub Tests

void AdHandlerTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(AdHandlerTest::testVideoAd);
	SUBTEST(AdHandlerTest::testBannerAd);
	SUBTEST(AdHandlerTest::testRewardAd);
	SUBTEST(AdHandlerTest::useAdmob);
	SUBTEST(AdHandlerTest::useAdcolony);
	SUBTEST(AdHandlerTest::testIsReady);
}

#pragma mark -
#pragma mark Sub Test Definition
void AdHandlerTest::useAdmob(Ref *sender)
{
	AdmobHandler *handler = new AdmobHandler();
	handler->setup();
	setAdHandler(handler);
	handler->release();
}

void AdHandlerTest::useAdcolony(Ref *sender)
{
	AdColonyHandler *handler = new AdColonyHandler();
	handler->setup();
	setAdHandler(handler);
	handler->release();
	
}

void AdHandlerTest::testIsReady(Ref *sender)
{
	std::vector<std::string> testList;
	
	testList.push_back("VideoReward");
	testList.push_back("VideoInter");
	testList.push_back("Banner");
	
	for(int i=0; i<testList.size(); i++) {
		std::string name = testList[i];
		
		bool isReady = getAdHandler()->isAdReady(name);
		
		log("name=%s isReady=%d", name.c_str(), isReady);
	}
}

void AdHandlerTest::testVideoAd(Ref *sender)
{
	log("Testing testVideoAd");
	
	getAdHandler()->setVideoFinishedCallback([](bool isPlayed){
		log("video finished. isPlayed=%d", isPlayed);
	});
	
	getAdHandler()->setVideoRewardedCallback([](bool isRewarded){
		log("video Rewarded. isRewarded=%d", isRewarded);
	});
	
	std::string name = "VideoReward";
	log("Try to show %s", name.c_str());
	getAdHandler()->showVideo(name);
}


void AdHandlerTest::testRewardAd(Ref *sender)
{
	
	std::string adName = getAdHandler()->getAdName(AdManager::VideoAd::OneMoreChance);
	
	log("Testing testAd: %s", adName.c_str());
	
	getAdHandler()->setVideoFinishedCallback([](bool isPlayed){
		log("video finished. isPlayed=%d", isPlayed);
	});
	
	getAdHandler()->setVideoRewardedCallback([](bool isRewarded){
		log("video Rewarded. isRewarded=%d", isRewarded);
	});
	
	getAdHandler()->showVideo(adName);
}


void AdHandlerTest::testBannerAd(Ref *sender)
{
	log("this is a sample subTest");
	

	getAdHandler()->showBanner("Banner");
	
	
}

#endif
