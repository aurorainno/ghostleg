#ifdef ENABLE_TDD
//
//  IAPManagerTest.h
//
//
#ifndef __TDD_IAPManagerTest__
#define __TDD_IAPManagerTest__

// Include Header

#include "TDDBaseTest.h"

class CoinShopSceneLayer;

// Class Declaration 
class IAPManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void subTest();
	void testGetMoneyDetail();
	void testAddCandySuccess();
	void testAddStarSuccess();
	void testPurchaseStar();
	void testPurchaseNoAd();
	void testActionAddCandy();
	void testActionAddStar();
    void testActionAddDiamond();
	void testActionRemoveAd();
    void testProductItemView();
    void testCoinShopScene();
    void testShowErrMsg();

    void testShowAlert();

	void testLoadProducts();
	void testUpdateProductInfo();
	void testResetConsumable();

private:
	CoinShopSceneLayer *mShopLayer;
	
}; 

#endif

#endif
