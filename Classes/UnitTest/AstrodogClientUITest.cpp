#ifdef ENABLE_TDD
//
//  AstrodogClientUITest.m
//	TDD Framework
//
//
#include "AstrodogClientUITest.h"
#include "TDDHelper.h"
#include "ConsoleView.h"
#include "VisibleRect.h"
#include "StringHelper.h"

void AstrodogClientUITest::setUp()
{
	log("TDD Setup is called");
	//
	for(int i=1; i<5; i++) { mPlanetList.push_back(i); }
	mBoardTypeList.push_back(AstrodogLeaderboardGlobal);
	mBoardTypeList.push_back(AstrodogLeaderboardCountry);
	mBoardTypeList.push_back(AstrodogLeaderboardFriend);
	
//	std::vector<AstrodogLeaderboardType> mBoardTypeList;
//
	
	// Data Setup
	mBoardType = AstrodogLeaderboardGlobal;
	mPlanet = 1;
	mScore = 50;

	
	// Setting Up the ConsoleView
	Size leaderboardSize = Size(320, 200);
	
	mConsoleView = ConsoleView::create(leaderboardSize);
	
	Vec2 pos = VisibleRect::leftTop() - Vec2(0, leaderboardSize.height);
	mConsoleView->setPosition(pos);
	addChild(mConsoleView);
	mConsoleView->setTitle("Leaderboard");
	
	// Information Box (for LeaderboardType & planet)
	mInfoText = ui::Text::create("", "", 13);
	mInfoText->setAnchorPoint(Vec2(0, 1));
	mInfoText->setContentSize(Size(300, 30));
	mInfoText->setPosition(pos + Vec2(10, -40));
	addChild(mInfoText);
	updateInfo();
	
	
	// Setup Listener
	AstrodogClient::instance()->setListener(this);
	
	
	// Some intial data
	addContent("Welcome to Astrodog Leaderboard");
}


void AstrodogClientUITest::tearDown()
{
	log("TDD tearDown is called");
	AstrodogClient::instance()->removeListener();
}

void AstrodogClientUITest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AstrodogClientUITest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}

#pragma mark - ConsoleView
void AstrodogClientUITest::setContent(const std::string &content)
{
	mConsoleView->clear();
	mConsoleView->append(content);
}

void AstrodogClientUITest::addContent(const std::string &content)
{
	mConsoleView->append(content);
}

#pragma mark - Information Box
void setInfo(const std::string &content)
{
	
}

#pragma mark - AstrodogClientListener
void AstrodogClientUITest::onGetRanking(int status, AstrodogLeaderboardType type,
										int planet, const std::map<std::string, int> &metaData)
{
	log("onGetRanking");
	showLeaderboard();
}

void AstrodogClientUITest::onSubmitScore(int status)
{
	updateInfo();
}

void AstrodogClientUITest::onFBConnect(int status)
{
	updateInfo();
}

#pragma mark - Core Functions
void AstrodogClientUITest::updateInfo()
{
	std::string info = AstrodogClient::instance()->getUserInfo() + "\n";
	
	info += StringUtils::format("type=%d planet=%d score=%d\n", mBoardType, mPlanet, mScore);
	
	info += "server=" + AstrodogClient::instance()->getCurrentServerURL();
	
	mInfoText->setString(info);
}

void AstrodogClientUITest::showLeaderboard()
{
	AstrodogLeaderboard *board = AstrodogClient::instance()->getLeaderboard(mBoardType, mPlanet);
	
	if(board == nullptr) {
		std::string msg = StringUtils::format("Leaderboard not available: type=%d planet=%d",
											  mBoardType, mPlanet);
		setContent(msg);
		return;
	}
	
	std::string content = "";
	
	
	for(AstrodogRecord *record : board->recordList) {
		//
		content += StringUtils::format("%5d: ", record->getRank());
		content += record->getName();
		content += " [" + record->getCountry() + "]";
		content += " score=" + INT_TO_STR(record->getScore());
		content += " uid=" + INT_TO_STR(record->getUid());
		
		content += "\n";
	}
	
	setContent(content);
	
}


#pragma mark -
#pragma mark List of Sub Tests

void AstrodogClientUITest::defineTests()
{
	ADD_TEST(loadLeaderboard);
	ADD_TEST(testFacebook);
	ADD_TEST(changeBoard);
	ADD_TEST(changePlanet);
	ADD_TEST(submitScore);
	ADD_TEST(scoreAdd20);
	ADD_TEST(scoreMinus20);
	ADD_TEST(updateInfo);
	ADD_TEST(logoutFB);
}

#pragma mark -
#pragma mark Sub Test Definition
void AstrodogClientUITest::loadLeaderboard()
{
	AstrodogClient::instance()->getRanking(mBoardType, mPlanet);
	setContent("Loading.....");
}

void AstrodogClientUITest::changeBoard()
{
	static int index = 0;
	
	//
	mBoardType = mBoardTypeList[index];
	updateInfo();
	
	// Next
	index++;
	if(index >= mBoardTypeList.size()) {
		index = 0;
	}
}

void AstrodogClientUITest::changeScore(int change)
{
	int newScore = mScore + change;
	if(newScore < 0) {
		newScore = 0;
	}
	
	mScore = newScore;
	
	updateInfo();
}

void AstrodogClientUITest::changePlanet()
{
	static int index = 0;
	
	//
	mPlanet = mPlanetList[index];
	updateInfo();
	
	// Next
	index++;
	if(index >= mPlanetList.size()) {
		index = 0;
	}
	
}


void AstrodogClientUITest::scoreAdd20()
{
	changeScore(20);
}

void AstrodogClientUITest::scoreMinus20()
{
	changeScore(-20);
}

void AstrodogClientUITest::submitScore()
{
	AstrodogClient::instance()->submitScore(mPlanet, mScore);
}

void AstrodogClientUITest::testFacebook()
{
	AstrodogClient::instance()->connectFB();
}

void AstrodogClientUITest::logoutFB()
{
	AstrodogClient::instance()->disconnectFB();
}


#endif
