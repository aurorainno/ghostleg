#ifdef ENABLE_TDD
//
//  HttpTest.h
//
//
#ifndef __TDD_HttpTest__
#define __TDD_HttpTest__

// Include Header

#include "TDDBaseTest.h"
#include "network/HttpClient.h"

using namespace cocos2d::network;

// Class Declaration
class HttpTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void onHttpRequestCompleted(HttpClient *sender, HttpResponse *response);
private:
	void testSample();
};

#endif

#endif
