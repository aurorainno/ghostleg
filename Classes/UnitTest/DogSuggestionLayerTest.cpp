#ifdef ENABLE_TDD
//
//  DogSuggestionLayerTest.m
//	TDD Framework
//
//
#include "DogSuggestionLayerTest.h"
#include "TDDHelper.h"
#include "DogSuggestionLayer.h"
#include "StageParallaxLayer.h"

void DogSuggestionLayerTest::setUp()
{
	log("TDD Setup is called");

	StageParallaxLayer *parallax = StageParallaxLayer::create();
	addChild(parallax);

	mLayer = nullptr;
}


void DogSuggestionLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void DogSuggestionLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DogSuggestionLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DogSuggestionLayerTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testChangeDog);
}

#pragma mark -
#pragma mark Sub Test Definition
void DogSuggestionLayerTest::testSample()
{
	log("this is a sample subTest");
	
	DogSuggestionLayer *layer = DogSuggestionLayer::create();
	addChild(layer);
	mLayer = layer;
	
	layer->setCallback([&](bool didUnlock){
		log("callback: didUnlock=%d", didUnlock);
	});
}

void DogSuggestionLayerTest::testChangeDog()
{
	if(mLayer) {
		mLayer->setDog(10);
	}

}

#endif
