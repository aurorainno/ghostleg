#ifdef ENABLE_TDD
//
//  LuckyDrawLayerTest.m
//	TDD Framework
//
//
#include "LuckyDrawLayerTest.h"
#include "TDDHelper.h"
#include "LuckyDrawAnime.h"
#include "LuckyDrawLayer.h"
#include "LuckyDrawManager.h"

void LuckyDrawLayerTest::setUp()
{
	log("TDD Setup is called");
}


void LuckyDrawLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void LuckyDrawLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void LuckyDrawLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void LuckyDrawLayerTest::defineTests()
{
    ADD_TEST(testSample);
	ADD_TEST(testCreateAnime);
	ADD_TEST(testOpen);
	ADD_TEST(testSound);
}

#pragma mark -
#pragma mark Sub Test Definition
void LuckyDrawLayerTest::testCreateAnime()
{
	LuckyDrawAnime *layer = LuckyDrawAnime::create();
	addChild(layer);
	
	mAnime = layer;
	
}

void LuckyDrawLayerTest::testSample()
{
    LuckyDrawManager::instance()->setup();
    LuckyDrawReward *reward = LuckyDrawManager::instance()->generateReward();
    
    LuckyDrawLayer *layer = LuckyDrawLayer::create();
    layer->setReward(reward);
    addChild(layer);

}

void LuckyDrawLayerTest::testOpen()
{
    if(!mAnime){
        return;
    }
    
	mAnime->openGacha();
	mAnime->setOpenDoneCallback([&](Ref *) {
		log("Gacha is opened");
	});
}

void LuckyDrawLayerTest::testSound()
{
	static int index = 0;
	
	std::vector<std::string> soundNameList;
	soundNameList.push_back("sound1");
	soundNameList.push_back("sound2");
	
	
	if(mAnime) {
		std::string soundName = soundNameList[index];
		log("sound: %s", soundName.c_str());
		mAnime->playSound(soundName);
	}
	
	
	//
	index++;
	if(index >= soundNameList.size()) {
		index = 0;
	}
	
}

#endif
