#ifdef ENABLE_TDD
//
//  CheckPointDialogTest.h
//
//
#ifndef __TDD_CheckPointDialogTest__
#define __TDD_CheckPointDialogTest__

// Include Header

#include "TDDBaseTest.h"

class CheckPointDialog;

// Class Declaration
class CheckPointDialogTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	
private:
	CheckPointDialog *mDialog;
};

#endif

#endif
