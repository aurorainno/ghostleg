#ifdef ENABLE_TDD
//
//  UserDefaultTest.m
//	TDD Framework 
//
//
#include "UserDefaultTest.h"
#include "TDDHelper.h"

void UserDefaultTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void UserDefaultTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void UserDefaultTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(UserDefaultTest::testUserCounter);
}

#pragma mark -
#pragma mark Sub Test Definition
void UserDefaultTest::testUserCounter(Ref *sender)
{
	log("this is a sample subTest");
	
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("UserGameData:loadData: userDefault not available");
		return;
	}
	
	int counter = userData->getIntegerForKey("testCounter", 0);
	
	log("Loaded Counter=%d", counter);
	
	counter++;
	
	log("Save Counter to %d", counter);
	userData->setIntegerForKey("testCounter", counter);
	
}


#endif
