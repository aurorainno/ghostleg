#ifdef ENABLE_TDD
//
//  FacebookTest.h
//
//
#ifndef __TDD_FacebookTest__
#define __TDD_FacebookTest__

// Include Header

#include "TDDBaseTest.h"
#include "PluginFacebook/PluginFacebook.h"

// Class Declaration
class SpriteEx;

class FacebookTest : public TDDBaseTest, public sdkbox::FacebookListener
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testGetUserData();
    void testLogOut();
    void testGetFriends();
	void testInviteFriends();
    void testIsLoggedIn();
	void testAPI();
	
    SpriteEx* mIconSprite;

private:
    virtual void onLogin(bool isLogin, const std::string& msg);
    virtual void onSharedSuccess(const std::string& message) ;
    virtual void onSharedFailed(const std::string& message) ;
    virtual void onSharedCancel() ;
    virtual void onAPI(const std::string& key, const std::string& jsonData) ;
    virtual void onPermission(bool isLogin, const std::string& msg) ;
    virtual void onFetchFriends(bool ok, const std::string& msg) ;
    virtual void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends ) ;
    virtual void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg );
    virtual void onInviteFriendsResult( bool result, const std::string& msg );
	
    
    virtual void onGetUserInfo( const sdkbox::FBGraphUser& userInfo ) ;

};

#endif

#endif
