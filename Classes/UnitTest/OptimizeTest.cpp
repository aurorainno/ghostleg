#ifdef ENABLE_TDD
//
//  OptimizeTest.m
//	TDD Framework
//
//
#include "OptimizeTest.h"
#include "TDDHelper.h"

void OptimizeTest::setUp()
{
	log("TDD Setup is called");
}


void OptimizeTest::tearDown()
{
	log("TDD tearDown is called");
}

void OptimizeTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void OptimizeTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void OptimizeTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void OptimizeTest::testSample()
{
	log("this is a sample subTest");
}


#endif
