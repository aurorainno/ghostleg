#ifdef ENABLE_TDD
//
//  WebViewTest.h
//
//
#ifndef __TDD_WebViewTest__
#define __TDD_WebViewTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class WebViewTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
    void WebViewLayerTest(Ref* sender);
}; 

#endif

#endif
