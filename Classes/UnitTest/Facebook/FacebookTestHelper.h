//
//  FacebookTestHelper.h
//  GhostLeg
//
//  Created by Ken Lee on 25/5/2017.
//
//

#ifndef FacebookTestHelper_h
#define FacebookTestHelper_h

#include "cocos2d.h"
#include <string>


// Implementation at FacebookTestHelper.mm
//
class FacebookTestHelper	// rename iOSNativeHelper later
{
public:
	static void testFacebookInvite();
	static void testAccessToken();
};


#endif /* FacebookTestHelper_h */
