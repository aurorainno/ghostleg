//
//  FacebookTestHelper.m
//  GhostLeg
//
//  Created by Ken Lee on 25/5/2017.
//
//


#pragma mark - Objective C
#import "AppController.h"
#import "RootViewController.h"
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

//#import <FBSDKCoreKit/

@interface iOSFacebookTestHelper : NSObject
{
	
}
@end

@implementation iOSFacebookTestHelper
+ (void)checkNative {
//  FBSDKServerConfiguration *configuration = [FBSDKServerConfigurationManager cachedServerConfiguration];
//  BOOL useNativeDialog = [configuration useNativeDialogForDialogName:FBSDKDialogConfigurationNameAppInvite];
//  BOOL isInstalled = [FBSDKInternalUtility isFacebookAppInstalled]);
}

+ (void)testFacebookInvite {
	// Reference:
	//			https://developers.facebook.com/docs/app-invites/ios
	
	
	NSLog(@"DEBUG: testFacebookInvite!");
	
	
	//
	if ([FBSDKAccessToken currentAccessToken]) {
		// User is logged in, do work such as go to next view controller.
		NSLog(@"AccessToken=%@", [FBSDKAccessToken currentAccessToken]);
	} else {
		NSLog(@"Cannot get the accessToken");
	}
	
	
	//
	NSString *appLink = @"https://fb.me/1831229720537597";
	NSString *previewImage = @"http://aurorainnoapps.com:9280/static-doc//monsterEx/appIcon.png";

	
	FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
	content.appLinkURL = [NSURL URLWithString:appLink];
	//optionally set previewImageURL
	content.appInvitePreviewImageURL = [NSURL URLWithString:previewImage];
	content.destination = FBSDKAppInviteDestination::FBSDKAppInviteDestinationFacebook;
	
	AppController *appController = (AppController *)[[UIApplication sharedApplication] delegate];
	// Present the dialog. Assumes self is a view controller
	// which implements the protocol `FBSDKAppInviteDialogDelegate`.
	[FBSDKAppInviteDialog showFromViewController:appController.viewController
									 withContent:content
										delegate:nullptr];
}



+ (void)testAccessToken {
	// Reference:
	//			https://developers.facebook.com/docs/app-invites/ios
	
	
	NSLog(@"DEBUG: testAccessToken!");
	
	
	//
	if ([FBSDKAccessToken currentAccessToken]) {
		// User is logged in, do work such as go to next view controller.
		NSLog(@"AppID=%@", [FBSDKAccessToken currentAccessToken].appID);
		NSLog(@"userID=%@", [FBSDKAccessToken currentAccessToken].userID);
		NSLog(@"AccessToken=%@", [FBSDKAccessToken currentAccessToken].tokenString);
		//NSLog(@"userID=%@", [FBSDKAccessToken currentAccessToken].user
		
		
	} else {
		NSLog(@"Cannot get the accessToken");
	}
}

@end;





#pragma mark - C Code
#include "FacebookTestHelper.h"

void FacebookTestHelper::testFacebookInvite()
{
	[iOSFacebookTestHelper testFacebookInvite];
}


void FacebookTestHelper::testAccessToken()
{
	[iOSFacebookTestHelper testAccessToken];
}
