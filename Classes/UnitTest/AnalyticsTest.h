#ifdef ENABLE_TDD
//
//  AnalyticsTest.h
//
//
#ifndef __TDD_AnalyticsTest__
#define __TDD_AnalyticsTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class AnalyticsTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testLogScreen(Ref *sender);
	void testLogEvent(Ref *sender);
}; 

#endif

#endif
