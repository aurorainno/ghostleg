#ifdef ENABLE_TDD
//
//  DogManagerTest.m
//	TDD Framework 
//
//
#include "DogManagerTest.h"
#include "TDDHelper.h"
#include "DogManager.h"
#include "Player.h"
#include "MasteryManager.h"
#include "DogData.h"
#include "GameManager.h"
#include "StringHelper.h"
#include "PlanetManager.h"

void DogManagerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	DogManager::instance()->loadData();
}


void DogManagerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}


#pragma mark -
#pragma mark Helper Method



void DogManagerTest::showSelectedDogInfo()
{
	DogData *selectedDog = DogManager::instance()->getSelectedDogData();
	log("Dog Data: %s", selectedDog->toString().c_str());
	
	Player *player = Player::create();

	GameManager::instance()->getMasteryManager()->setPlayerMasteryAttribute(player);
	
	log("Player: %s", player->infoAttribute().c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DogManagerTest::defineTests()
{
	ADD_TEST(testMock);
	ADD_TEST(testParseAbility);
	ADD_TEST(testInfoDogData);
	ADD_TEST(testRarityAndRank);
	ADD_TEST(testGetNextUnlock);
	ADD_TEST(testGetUnlockProgressMessage);
	ADD_TEST(testSortLockedDog);
	ADD_TEST(testResetSuggestion);
	ADD_TEST(testGetUnlockMessage);
	ADD_TEST(testParseUnlock);
	ADD_TEST(testChangeDog);
    ADD_TEST(testEndGameUnlockDog);
	ADD_TEST(testGetUnlockProgress);
	ADD_TEST(testGetDogList);
	ADD_TEST(testDogInfoText);
	ADD_TEST(testShouldShowSuggestion);
}

#pragma mark -
#pragma mark Sub Test Definition
void DogManagerTest::testParseAbility()
{
	std::vector<std::string> abilityStrList;
	
	abilityStrList.push_back("bonusTip/30");		// 30 percent tips
	abilityStrList.push_back("ratingBonus/2,50");	// +50 for rating=2
	abilityStrList.push_back("none");
	
	DogData *dogData = DogData::create();

	dogData->parseAbilityList(abilityStrList);
	
	log("DogData:\n%s\n", dogData->infoAbilityList().c_str());
}


void DogManagerTest::testRarityAndRank()
{
	// Rank
	std::vector<std::string> rankList;
	rankList.push_back("classic");
	rankList.push_back("premium");
	
	for(std::string str : rankList) {
		DogData::DogRank rank = DogData::parseRank(str);
		std::string rankStr = DogData::getRankString(rank);
		
		log("%s: rank=%d (%s)", str.c_str(), rank, rankStr.c_str());
	}
	
	
	// Rarity
	std::vector<std::string> rarityList;
	rarityList.push_back("rare");
	rarityList.push_back("superRare");
	
	for(std::string str : rarityList) {
		DogData::DogRarity rarity = DogData::parseRarity(str);
		std::string rarityStr = DogData::getRarityString(rarity);
		
		log("%s: rarity=%d (%s)", str.c_str(), rarity, rarityStr.c_str());
	}

}


void DogManagerTest::testSortLockedDog()
{
	std::vector<int> lockedDogs = PlanetManager::instance()->getLockedDogList(2);
	log("lockedDogs: %s", VECTOR_TO_STR(lockedDogs).c_str());
	
	DogManager::instance()->sortLockedDogList(lockedDogs);
	log("afterSort: lockedDogs: %s", VECTOR_TO_STR(lockedDogs).c_str());
}


void DogManagerTest::testGetUnlockProgress()
{
	for(int dogID=1; dogID<=7; dogID++) {
		int now;
		int need;
		
		DogManager::instance()->getUnlockProgress(dogID, now, need);
		
		log("DEBUG: dogID=%d now=%d need=%d", dogID, now, need);
	}
	
	
}

void DogManagerTest::testInfoDogData()
{
	log("DogData Details:\n%s", DogManager::instance()->infoDogData().c_str());
}


void DogManagerTest::testMock()
{
	log("this is a sample subTest");
	
	DogManager::instance()->setupMock();
	
	log("DogData Details:\n%s", DogManager::instance()->infoDogData().c_str());
}

void DogManagerTest::testChangeDog()
{
	static int counter = 0;

	//
	
	int dogID = (counter % DogManager::instance()->getTotalDogCount()) + 1;
	
	DogManager::instance()->selectDog(dogID);
	
	log("selectedDog=%d", DogManager::instance()->getSelectedDogID());
	showSelectedDogInfo();
	
	// Change
	counter++;
}

void DogManagerTest::testEndGameUnlockDog()
{
    DogManager::instance()->getNewUnlockedDogs();
}

void DogManagerTest::testShouldShowSuggestion()
{
	int count = 30;
	
	DogManager::instance()->resetSuggestRecord();
	for(int i=0; i<count; i++) {
		bool flag = DogManager::instance()->shouldShowSuggestion(i);
	
		
		log("%d: showSuggest=%d", i, flag);
	}
	
	int suggestDog = DogManager::instance()->getSuggestedDogID();
	
	log("suggestDog=%d", suggestDog);
	
}

void DogManagerTest::testGetDogList()
{
	std::vector<int> result = DogManager::instance()->getDogListOrderByTeam(DogManager::SearchTypeLocked);
	
	log("DogList: %s", VECTOR_TO_STR(result).c_str());
}

void DogManagerTest::testResetSuggestion()
{
	DogManager::instance()->resetSuggestRecord();
}

void DogManagerTest::testDogInfoText()
{
	std::string result = DogManager::instance()->getUnlockInfo("kill #v wolves in #p", 100, 2);
	
	log("result=%s", result.c_str());
}

void DogManagerTest::testParseUnlock()
{
	std::vector<std::string> testList;
	testList.push_back("test_planet003");
	testList.push_back("test_planet004");
	testList.push_back("test_dog002");
	testList.push_back("testing");
	
	
	for(std::string cond : testList) {
		int planet;
		int dog;
		std::string mainKey;
		
		DogManager::instance()->parseUnlockCond(cond, planet, dog, mainKey);
		log("cond=[%s] planet=%d dog=%d mainKey=%s", cond.c_str(), planet, dog, mainKey.c_str());
	}
}

void DogManagerTest::testGetUnlockMessage()
{
	
	for(int dogID = 2; dogID <=3; dogID++) {
		DogData *data = DogManager::instance()->getDogData(dogID);
		
		std::string msg = DogManager::instance()->getUnlockMessage(data);
	
		log("dog=%d msg=%s", dogID, msg.c_str());
	}
	
}

void DogManagerTest::testGetNextUnlock()
{
	int unlockDog;
	int unlockPlanet;
	bool haveAny = DogManager::instance()->getNextUnlockDog(unlockDog, unlockPlanet);
	
	log("unlockDog=%d unlockPlanet=%d haveAny=%d", unlockDog, unlockPlanet, haveAny);
}

void DogManagerTest::testGetUnlockProgressMessage()
{
	int planetID = 005;		//"Jupiter";
	
	for(int dogID = 2; dogID <=20; dogID++) {
		std::string msg = DogManager::instance()->getUnlockProgressMessage(dogID, planetID);
		
		log("dog=%d msg=%s", dogID, msg.c_str());
	}
	
}

#endif

