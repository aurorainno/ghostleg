#ifdef ENABLE_TDD
//
//  AnimeNodeTest.h
//
//
#ifndef __TDD_AnimeNodeTest__
#define __TDD_AnimeNodeTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class AnimeNodeTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testStartAnime();
};

#endif

#endif
