#ifdef ENABLE_TDD
//
//  FBAnalyticsTest.m
//	TDD Framework
//
//
#include "FBAnalyticsTest.h"
#include "TDDHelper.h"
#include "FBAnalytics.h"

void FBAnalyticsTest::setUp()
{
	log("TDD Setup is called");
}


void FBAnalyticsTest::tearDown()
{
	log("TDD tearDown is called");
}

void FBAnalyticsTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void FBAnalyticsTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void FBAnalyticsTest::defineTests()
{
	ADD_TEST(testLogEvent);
}

#pragma mark -
#pragma mark Sub Test Definition
void FBAnalyticsTest::testLogEvent()
{
	//log("this is a sample subTest");
	FBAnalytics::logEvent("Testing from UnitTest");
}


#endif
