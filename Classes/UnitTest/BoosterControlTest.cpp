#ifdef ENABLE_TDD
//
//  BoosterControlTest.m
//	TDD Framework
//
//
#include "BoosterControlTest.h"
#include "TDDHelper.h"
#include "BoosterControl.h"
#include "VisibleRect.h"

void BoosterControlTest::setUp()
{
	log("TDD Setup is called");
}


void BoosterControlTest::tearDown()
{
	log("TDD tearDown is called");
}

void BoosterControlTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void BoosterControlTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void BoosterControlTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testCallback);
	ADD_TEST(testBoosterType);
}

#pragma mark -
#pragma mark Sub Test Definition
void BoosterControlTest::testSample()
{
	log("this is a sample subTest");
	BoosterControl *control = BoosterControl::create();
	control->setPosition(VisibleRect::center());
	
	addChild(control);
}

void BoosterControlTest::testBoosterType()
{
	//log("this is a sample subTest");
	std::vector<BoosterItemType> typeArray;
	typeArray.push_back(BoosterItemMissile);
	typeArray.push_back(BoosterItemSnowball);
	typeArray.push_back(BoosterItemDoubleCoin);
	typeArray.push_back(BoosterItemUnbreakable);
	typeArray.push_back(BoosterItemTimeStop);
	
	Vec2 pos = Vec2(100, 100);
	
	for(BoosterItemType type : typeArray) {
		BoosterControl *control = BoosterControl::create();
		control->setBoosterType(type);
		control->setPosition(pos);
		addChild(control);
		
		pos.y += 80;
	}
}

void BoosterControlTest::testCallback()
{
	log("this is a sample subTest");
	BoosterControl *control = BoosterControl::create();
	control->setPosition(VisibleRect::center());
	control->setBoosterType(BoosterItemTimeStop);
	
	control->setCallback([&](BoosterControl *control) {
		if(control == nullptr) {
			log("input control is null");
			return false;
		}
		
		BoosterItemType type = control->getBoosterType();
		log("boosterType: %d", type);
		
		return true;
	});
	
	addChild(control);
	
	control->scheduleUpdate();
}


#endif
