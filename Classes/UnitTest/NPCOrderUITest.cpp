#ifdef ENABLE_TDD
//
//  NPCOrderUITest.m
//	TDD Framework
//
//
#include "NPCOrderUITest.h"
#include "TDDHelper.h"
#include "NPCOrderItemView.h"
#include "NPCOrderSelectDialog.h"
#include "NPCOrder.h"
#include "StageManager.h"
#include "GameplaySetting.h"

void NPCOrderUITest::setUp()
{
	log("TDD Setup is called");
}


void NPCOrderUITest::tearDown()
{
	log("TDD tearDown is called");
}

void NPCOrderUITest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NPCOrderUITest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NPCOrderUITest::defineTests()
{
	ADD_TEST(testNPCOrderItemView);
    ADD_TEST(testNPCOrderSelectDialog);
}

#pragma mark -
#pragma mark Sub Test Definition
void NPCOrderUITest::testNPCOrderItemView()
{
	log("this is a sample subTest");
	
	NPCOrderData *orderData = StageManager::instance()->getNpcOrderData(101);
	GameplaySetting *setting = GameplaySetting::create();
	
	
    NPCOrder* order = new NPCOrder();
	order->setup(orderData, setting);

	log("DEBUG: order=%s", order->toString().c_str());
	log("DEBUG: reward=%d", order->getReward());
    
    NPCOrderItemView* itemView = NPCOrderItemView::create();
    itemView->setNPCOrder(order);
    Vec2 size = Director::getInstance()->getOpenGLView()->getVisibleSize();
    itemView->setPosition(size/2);
    addChild(itemView);
}

void NPCOrderUITest::testNPCOrderSelectDialog()
{
    Vector<NPCOrder*> orderList{};
    
	NPCOrder* order1 = NPCOrder::create();
    order1->setNpcID(1);
    order1->setReward(300);
    order1->setTimeLimit(120);
    order1->setDifficulty(1);
    orderList.pushBack(order1);

	NPCOrder* order2 = NPCOrder::create();
    order2->setNpcID(3);
    order2->setReward(5500);
    order2->setTimeLimit(500);
    order2->setDifficulty(3);
    orderList.pushBack(order2);

    NPCOrderSelectDialog* dialog = NPCOrderSelectDialog::create();
    dialog->setItemView(orderList);
    
    dialog->setListener(this);
    addChild(dialog);
	
	mDialog = dialog;
    
}

void NPCOrderUITest::onRefreshNPC()
{
    log("onRefreshNPC is called!");
}

void NPCOrderUITest::onNPCOrderSelected(NPCOrder *order)
{
    log("npc%03d order is choosen!",order->getNpcID());
	mDialog->removeFromParent();
	mDialog = nullptr;
}

void NPCOrderUITest::onDialogClosed()
{
    log("dialog is closed!");
}


#endif
