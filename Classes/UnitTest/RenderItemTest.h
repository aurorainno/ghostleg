#ifdef ENABLE_TDD
//
//  RenderItemTest.h
//
//
#ifndef __TDD_RenderItemTest__
#define __TDD_RenderItemTest__

// Include Header

#include "TDDBaseTest.h"

class StageParallaxLayer;

class Star;

// Class Declaration
class RenderItemTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	//
	void addCoin(const Vec2 &pos);
	void consumeAllCoin();
	
	void addMagnet(const Vec2 &pos);
	void consumeAllMagnet();

	void addCashout(const Vec2 &pos);
	void consumeAllCashout();

	void consumeAllItem(Vector<Node *> &itemList);
	
private:
	void clearAll();
	
	void testAddCoin();
	void testConsumeAllCoin();
	
	void testAddMagnet();
	void testConsumeAllMagnet();
	
	void testAddCashout();
	void testConsumeAllCashout();
	
	void addPuzzle();
	void consumeAllPuzzle();
	
	void addSpeedUp();
	void consumeAllSpeedUp();

private:
	StageParallaxLayer *mParallaxLayer;
	Vector<Node *> mCoinList;
	Vector<Node *> mMagnetList;
	Vector<Node *> mCashoutList;
	Vector<Node *> mPuzzleList;
	Vector<Node *> mSpeedupList;
	
	
};

#endif

#endif
