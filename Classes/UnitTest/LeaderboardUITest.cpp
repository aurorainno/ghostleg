#ifdef ENABLE_TDD
//
//  LeadboardUITest.m
//	TDD Framework
//
//
#include "LeaderboardUITest.h"
#include "TDDHelper.h"
#include "LeaderboardItemView.h"
#include "LeaderboardDialog.h"
#include "AstrodogData.h"
#include "VisibleRect.h"
#include "AstrodogClient.h"

void LeaderboardUITest::setUp()
{
	log("TDD Setup is called");
}


void LeaderboardUITest::tearDown()
{
	log("TDD tearDown is called");
}

void LeaderboardUITest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void LeaderboardUITest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void LeaderboardUITest::defineTests()
{
	ADD_TEST(testDialog);
	ADD_TEST(testMainLeaderboard);
	ADD_TEST(testItemView);
}

#pragma mark -
#pragma mark Sub Test Definition

void LeaderboardUITest::testMainLeaderboard()
{
	//Asr
	LeaderboardDialog *dialog = LeaderboardDialog::create();
	dialog->setLeaderboardUIType(LeaderboardDialog::LeaderboardUIType::Mainscene);
	addChild(dialog);
	
	AstrodogLeaderboardType type = AstrodogLeaderboardGlobal;
	int planet = 1;
	
	AstrodogLeaderboard *leaderboard = AstrodogClient::instance()->getLeaderboard(type, planet);
	dialog->updateWithLeaderboard(leaderboard,planet);
	
	
	dialog->updateRankStatus(AstrodogRankUp);
}


void LeaderboardUITest::testDialog()
{
	//Asr
	LeaderboardDialog *dialog = LeaderboardDialog::create();
	dialog->setLeaderboardUIType(LeaderboardDialog::LeaderboardUIType::Endgame);
	addChild(dialog);
	
	AstrodogLeaderboardType type = AstrodogLeaderboardGlobal;
	int planet = 1;
	
	AstrodogLeaderboard *leaderboard = AstrodogClient::instance()->getLeaderboard(type, planet);
	dialog->updateWithLeaderboard(leaderboard,planet);
	
	
	dialog->updateRankStatus(AstrodogRankUp);
}

void LeaderboardUITest::testItemView()
{
	log("this is a sample subTest");
	
	
	AstrodogRecord *record = AstrodogRecord::create();
	record->setFbID("1234567");
	record->setName("Testing");
	record->setScore(99999);
	record->setCountry("CA");
	record->setRank(111);
	
	std::vector<AstrodogRankStatus> allStatus;
	
	allStatus.push_back(AstrodogRankUp);
	allStatus.push_back(AstrodogRankDown);
	allStatus.push_back(AstrodogRankNone);
	
	Vec2 pos = Vec2(25, 200);
	
	for(AstrodogRankStatus status : allStatus) {
		LeaderboardItemView *itemView = LeaderboardItemView::create();
		itemView->setPosition(pos);
	
		addChild(itemView);
		itemView->setItemView(record);
		itemView->setRankStatus(status);
	
		pos.y += 60;
	}
	
	
	//itemView->setRankUpVisible(true);
	
	//bool isThisPlayer = itemView->setItemView(recordList.at(i));	// ken: this is not good, violate, SRP
	
}


#endif
