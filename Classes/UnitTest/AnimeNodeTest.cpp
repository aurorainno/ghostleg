#ifdef ENABLE_TDD
//
//  AnimeNodeTest.m
//	TDD Framework
//
//
#include "AnimeNodeTest.h"
#include "TDDHelper.h"
#include "AnimeNode.h"
#include "VisibleRect.h"
#include "ViewHelper.h"

void AnimeNodeTest::setUp()
{
	log("TDD Setup is called");
}


void AnimeNodeTest::tearDown()
{
	log("TDD tearDown is called");
}

void AnimeNodeTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AnimeNodeTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AnimeNodeTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testStartAnime);
}

#pragma mark -
#pragma mark Sub Test Definition
void AnimeNodeTest::testSample()
{
	log("this is a sample subTest");
	AnimeNode *animeNode = AnimeNode::create();
	
	animeNode->setup("animation/DailyRewardTickNode.csb");
	
	animeNode->setPosition(VisibleRect::center());
	
	addChild(animeNode);
	
	animeNode->setPlayEndCallback([&](Ref *sender, std::string animeName){
		log("Animation play end. name=%s", animeName.c_str());
	});
	
	animeNode->playAnimation("start", false, true);
}

void AnimeNodeTest::testStartAnime()
{
	log("this is a sample subTest");
	
	
	
	AnimeNode *animeNode = AnimeNode::create();
	
	animeNode->setup("animation/DailyRewardTickNode.csb");
	animeNode->setStartAnime("start", false, true);
	animeNode->setPosition(VisibleRect::center());
	
	
	animeNode->setPlayEndCallback([&](Ref *sender, std::string animeName){
		log("Animation play end. name=%s", animeName.c_str());
	});

	
	addChild(animeNode);
	
	ViewHelper::createRedSpot(this, VisibleRect::center());
}


#endif
