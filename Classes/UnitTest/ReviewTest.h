#ifdef ENABLE_TDD
//
//  ReviewTest.h
//
//
#ifndef __TDD_ReviewTest__
#define __TDD_ReviewTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class ReviewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testShow();
	void testSetMessage();
};

#endif

#endif
