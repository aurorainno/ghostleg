#ifdef ENABLE_TDD
//
//  BatchTest.h
//
//
#ifndef __TDD_BatchTest__
#define __TDD_BatchTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class BatchTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

private:
	void addTestSprite(const Vec2 &pos);
	
private:
	void testSprite();
	void testSprLayout();
	void testRouteLine();
};

#endif

#endif
