#ifdef ENABLE_TDD
//
//  NPCRatingUITest.m
//	TDD Framework
//
//
#include "NPCRatingUITest.h"
#include "NPCRatingItemView.h"
#include "NPCRating.h"
#include "GalleryView.h"
#include "TDDHelper.h"

void NPCRatingUITest::setUp()
{
	log("TDD Setup is called");
}


void NPCRatingUITest::tearDown()
{
	log("TDD tearDown is called");
}

void NPCRatingUITest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NPCRatingUITest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NPCRatingUITest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(testGalleryView);
}

#pragma mark -
#pragma mark Sub Test Definition
void NPCRatingUITest::testSample()
{
	log("this is a sample subTest");
    
    NPCRating *dataObject = new NPCRating();
    dataObject->setNpcID(1);
    dataObject->setRating(4);
    
    NPCRatingItemView* itemView = NPCRatingItemView::create();
    itemView->setItemView(dataObject);
    
    Size size = Director::getInstance()->getVisibleSize();
    itemView->setPosition(Vec2(size.width/2,size.height/2));
    
    addChild(itemView);
    
}

void NPCRatingUITest::testGalleryView()
{
    GalleryView* view = GalleryView::create();
    Size size = Director::getInstance()->getVisibleSize();
    view->setTargetPosition(Vec2(size.width/2,size.height/2));
    view->setPeriod(2);
    view->setRotationTime(0.5);
    
    std::vector<NPCRatingItemView*> itemList{};
    for(int i=1;i<=5;i++){
        NPCRating *dataObject = new NPCRating();
        dataObject->setNpcID(i);
        dataObject->setRating(i);
        
        NPCRatingItemView* itemView = NPCRatingItemView::create();
        itemView->setItemView(dataObject);
        itemView->setAnchorPoint(Vec2(0.5,0.5));
        itemList.push_back(itemView);
    }
    
    view->setNodeList(itemList);
    
    addChild(view);
    
}


#endif
