#ifdef ENABLE_TDD
//
//  Vec2Test.m
//	TDD Framework 
//
//
#include "Vec2Test.h"
#include "TDDHelper.h"

#include "cocos2d.h"

namespace {
	bool isLineIntersect(const Vec2 &point1, const Vec2 &point2, const Vec2 &check) {
		if(check.x != point1.x) {
			return false;
		}
		
		return check.y >= point1.y && check.y <= point2.y;
	}
}

USING_NS_CC;

void Vec2Test::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void Vec2Test::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void Vec2Test::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(Vec2Test::subTest);
	SUBTEST(Vec2Test::testIntersect);
}

#pragma mark -
#pragma mark Sub Test Definition
void Vec2Test::subTest(Ref *sender)
{
	log("this is a sample subTest");
	Vec2 point1 = Vec2(30, 50);
	Vec2 point2 = Vec2(30, 80);
	
	Vec2 checkPoint1 = Vec2(30, 60);
	Vec2 checkPoint2 = Vec2(30, 100);
	
	Vec2 result1 = Vec2::getIntersectPoint(point1, point2, checkPoint1, checkPoint1);
	Vec2 result2 = Vec2::getIntersectPoint(point1, point2, checkPoint2, checkPoint2);
	
	log("result1=%f,%f", result1.x, result1.y);
	log("result2=%f,%f", result2.x, result2.y);
}


void Vec2Test::testIntersect(Ref *sender)
{
	log("this is a sample subTest");
	Vec2 point1 = Vec2(30, 50);
	Vec2 point2 = Vec2(30, 80);
	
	Vec2 checkPoint1 = Vec2(30, 60);
	Vec2 checkPoint2 = Vec2(30, 100);

	
	
	
	bool result1 = isLineIntersect(point1, point2, checkPoint1);
	bool result2 = isLineIntersect(point1, point2, checkPoint2);
//	Vec2 result2 = isLineIntersect(point1, point2, checkPoint2, checkPoint2);
	
	log("result1=%d", result1);
	log("result2=%d", result2);
//	log("result2=%f,%f", result2.x, result2.y);
}



#endif
