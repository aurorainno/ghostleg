#ifdef ENABLE_TDD
//
//  HitboxTest.h
//
//
#ifndef __TDD_HitboxTest__
#define __TDD_HitboxTest__

// Include Header

#include "TDDBaseTest.h"
#include "Circle.h"

class Enemy;
class GameModel;
class Player;
class Star;

// Class Declaration
class HitboxTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

	void setupModels();
	void setupPlayer();
	void setupItem();
	
	Enemy *createEnemy(int enemyID);

	void highlightHitBox(GameModel *model);
	void highlightRect(Rect &rect);
	void highlightCircle(Circle &circle);
	
	void setupTouchListener();
	bool onTouchBegan(Touch *touch, Event *event);
	void onTouchEnded(Touch *touch, Event *event);
	void onTouchMoved(Touch *touch, Event *event);
	void onTouchCancelled(Touch *touch, Event *event);

	
private:
	void moveEnemy();
	void moveLayer();
	void checkHitBox();
	void stepEnemy();
	void testHitboxInfo();
	void showHitbox();
	void hideHitbox();
	void clearHitbox();
	void testFourFlip();
	void testDifferentHitbox();
	
private:
	Layer *mMainLayer;
	Layer *mHitBoxLayer;
	Enemy *mEnemy;
	std::vector<Enemy *> mEnemyList;
	Player *mPlayer;
	Star *mStar;
	Vec2 mLastPos;
};

#endif

#endif
