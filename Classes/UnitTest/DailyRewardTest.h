#ifdef ENABLE_TDD
//
//  DailyRewardTest.h
//
//
#ifndef __TDD_DailyRewardTest__
#define __TDD_DailyRewardTest__

// Include Header

#include "TDDBaseTest.h"

class DailyReward;

// Class Declaration
class DailyRewardTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	void addMockData(DailyReward &dailyReward);
private:
	void testAddReward();
	void testGetRewardAtDay();
	void testManager();
	void testDailyRewardDetail();
    void testCollectReward();
    void addOneDay();
	void testLoadData();
    void testSave();
    void testReset();
    
private:
    int mDay;
    
};

#endif

#endif
