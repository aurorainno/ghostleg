#ifdef ENABLE_TDD
//
//  BehaviourTest.m
//	TDD Framework
//
//
#include "GameWorld.h"
#include "ModelLayer.h"
#include "BehaviourTest.h"
#include "TDDHelper.h"
#include "Enemy.h"
#include "EnemyFactory.h"
#include "VisibleRect.h"
#include "StringHelper.h"
#include "Player.h"
#include "GameModel.h"
#include "ui/CocosGUI.h"
#include "Star.h"

Enemy *BehaviourTest::createEnemy(int enemyID)
{
	Enemy *enemy = EnemyFactory::instance()->createEnemy(enemyID);
	if(enemy == nullptr) {
		return nullptr;
	}
	
	enemy->setPosition(Vec2(100, 400));
	//enemy->setupScaleAndFlip(0.5, 0.5, true, false);
	enemy->setupHitboxes();
	
	return enemy;
}


void BehaviourTest::setupPlayer()
{
	// Player
	Player *player = GameWorld::instance()->getPlayer();
	//player->reset();
	player->setPosition(Vec2(100, 100));
	mPlayer = player;
	mPlayer->setSpeed(0);
	mPlayer->setMaxSpeed(0);
	mPlayer->setAcceleration(0);
	mPlayer->resetSpeed();
	
	GameWorld::instance()->setIsBraking(true);
}

void BehaviourTest::setupItem()
{
	
	// Star
//	mStar = Star::create();
//	mStar->setPosition(VisibleRect::center() + Vec2(50, 50));
//	mMainLayer->addChild(mStar);
}

void BehaviourTest::setupModels()
{
	mEnemyList.clear();
	
	//int enemyID = 118;
	//int enemyID = 510;
	//int enemyID = 112;
	//int enemyID = 201;
	//int enemyID = 513;	// Laser Enemy
	//int enemyID = 515;	// Laser Enemy
	//int enemyID = 30;		// Jumping Model
	//int enemyID = 7;		// Crab (4, 7)
	//int enemyID = 310;		// Volcano
	//int enemyID = 80;		// Green Bullet
	//int enemyID = 81;		// Bomb
	int enemyID = 515;		// Sudden Move
	
	Enemy *enemy = createEnemy(enemyID);
	if(enemy != nullptr) {
		GameWorld::instance()->getModelLayer()->addEnemy(enemy);
	}
	
	setupPlayer();
	setupItem();
	
	scheduleUpdate();
}

void BehaviourTest::setupTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(BehaviourTest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(BehaviourTest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(BehaviourTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(BehaviourTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}

bool BehaviourTest::onTouchBegan(Touch *touch, Event *event)
{
	mLastPos = touch->getLocation();
	
	return true;
}

void BehaviourTest::onTouchEnded(Touch *touch, Event *event)
{
}

void BehaviourTest::onTouchMoved(Touch *touch, Event *event)
{
	Vec2 newPos = touch->getLocation();
	
	
	Vec2 diff = newPos - mLastPos;
	mLastPos = newPos;
	
	// Move Player by diff
	Vec2 oldPos = mPlayer->getPosition();
	mPlayer->setPosition(oldPos + diff);
	
	// Hitbox test
	bool flag;
	
	for(Enemy *enemy : mEnemyList) {
		flag = mPlayer->isCollideModel(enemy);
		log("player hit enemy: %d", flag);
	}
	
	flag = mPlayer->isCollideModel(mStar);
	log("player hit star: %d", flag);
}

void BehaviourTest::onTouchCancelled(Touch *touch, Event *event)
{
	
}
					   
void BehaviourTest::update(float delta)
{
	GameWorld::instance()->getModelLayer()->update(delta);
//	for(Enemy *enemy : mEnemyList) {
//		enemy->update(delta);
//	}
}


void BehaviourTest::setUp()
{
	GameWorld *world = GameWorld::instance();
	addChild(world);
	
	world->setUserTouchEnable(false);
	
	setupModels();
	
	// Touch Handling
	setupTouchListener();
}


void BehaviourTest::tearDown()
{
	log("TDD tearDown is called");
}

void BehaviourTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void BehaviourTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void BehaviourTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void BehaviourTest::testSample()
{
	log("this is a sample subTest");
}


#endif
