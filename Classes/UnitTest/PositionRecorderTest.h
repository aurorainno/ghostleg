#ifdef ENABLE_TDD
//
//  PositionRecorderTest.h
//
//
#ifndef __TDD_PositionRecorderTest__
#define __TDD_PositionRecorderTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class PositionRecorderTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testRecordPosition();
	void testNearestSpawnPosition();
};

#endif

#endif
