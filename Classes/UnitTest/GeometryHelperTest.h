#ifdef ENABLE_TDD
//
//  GeometryHelperTest.h
//
//
#ifndef __TDD_GeometryHelperTest__
#define __TDD_GeometryHelperTest__

// Include Header

#include "TDDBaseTest.h"

class MapLine;

// Class Declaration 
class GeometryHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();

private:
	void testCalculateNewPos(MapLine *mapLine, float speed);
	
private:
	void testGetNodeFinalScale();
	void subTest();
	void testAngle();
	void testFindAngle();
	void testResolveVec2();
	void testCalcNewTracePos();
}; 

#endif

#endif
