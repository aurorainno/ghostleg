#ifdef ENABLE_TDD
//
//  SpriteTest.h
//
//
#ifndef __TDD_SpriteTest__
#define __TDD_SpriteTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class SpriteTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testSample(Ref *sender);
	void testSetTexture(Ref *sender);
	void testUsingSpriteSheet(Ref *sender);
	void testSpriteWithSize(Ref *sender);
}; 

#endif

#endif
