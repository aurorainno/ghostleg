#ifdef ENABLE_TDD
//
//  AdmobTest.m
//	TDD Framework
//
//
#include "AdmobTest.h"
#include "TDDHelper.h"
#include "AdmobHandler.h"
#include "PluginAdMob/PluginAdMob.h"

void AdmobTest::setUp()
{
	log("TDD Setup is called");
	// Information
	
	log("PluginAdmobVersion: %s", sdkbox::PluginAdMob::getVersion().c_str());
	// Admob version: GoogleMobileAds.framework 
	
	//
	mHandler = new AdmobHandler();
	mHandler->setup();
	
	mHandler->setVideoFinishedCallback([&](bool flag) {
		log("Video Done: okay=%d", flag);
	});
	
	mHandler->setVideoRewardedCallback([&](bool flag) {
		log("Video Rewarded: okay=%d", flag);
	});

}


void AdmobTest::tearDown()
{
	log("TDD tearDown is called");
}

void AdmobTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AdmobTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AdmobTest::defineTests()
{
	ADD_TEST(testBanner);
	ADD_TEST(hideBanner);
	ADD_TEST(testVideo);
	ADD_TEST(testInter);
	ADD_TEST(testVideo1);
	ADD_TEST(testVideo2);
}

#pragma mark -
#pragma mark Sub Test Definition
void AdmobTest::testBanner()
{
	mHandler->showBanner("testBanner");
}

void AdmobTest::testVideo()
{
	mHandler->showVideo("testVideo");
}


void AdmobTest::hideBanner()
{
	mHandler->hideBanner("testBanner");
}
						
void AdmobTest::testInter()
{
	//mHandler->showVideo("testInter");
}

void AdmobTest::testVideo1()
{
	//mHandler->showVideo("video1");
	sdkbox::PluginAdMob::show("video1");
}

void AdmobTest::testVideo2()
{
	sdkbox::PluginAdMob::show("video2");
	//mHandler->showVideo("video2");
	
}

#endif
