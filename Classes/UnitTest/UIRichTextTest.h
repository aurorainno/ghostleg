#ifdef ENABLE_TDD
//
//  UIRichTextTest.h
//
//
#ifndef __TDD_UIRichTextTest__
#define __TDD_UIRichTextTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class UIRichTextTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testSetString();
	void testCreateFromText();
};

#endif

#endif
