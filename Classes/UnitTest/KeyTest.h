#ifdef ENABLE_TDD
//
//  KeyTest.h
//
//
#ifndef __TDD_KeyTest__
#define __TDD_KeyTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class KeyTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	void keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	void keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
