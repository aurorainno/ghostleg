#ifdef ENABLE_TDD
//
//  EffectLayerTest.m
//	TDD Framework 
//
//
#include "EffectLayerTest.h"
#include "TDDHelper.h"
#include "EffectLayer.h"
#include "VisibleRect.h"
#include "GameRes.h"
#include "GameWorld.h"
#include "GameScene.h"
#include "ModelLayer.h"
#include "ViewHelper.h"
#include "ItemEffect.h"


void EffectLayerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	addChild(GameWorld::instance());
	mEffectLayer = GameWorld::instance()->getEffectLayer();
	mEffectLayer->reset();

    ModelLayer* modelLayer = GameWorld::instance()->getModelLayer();
    mPlayer = modelLayer->getPlayer();

	
	mOffset = 0;
	mSpeedY = 0;
	
	scheduleUpdate();

}


void EffectLayerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	unscheduleUpdate();
}

void EffectLayerTest::setCamera(float y)
{
	mEffectLayer->setOffsetY(mOffset);
	GameWorld::instance()->setCameraY(y);
}

#pragma mark -
#pragma mark List of Sub Tests

void EffectLayerTest::update(float dt)
{
	if(mSpeedY != 0) {
		mOffset += (dt * mSpeedY);
		setCamera(mOffset);
	}
}

void EffectLayerTest::defineTests()
{
	ADD_TEST(testShowCoinEffect);
	ADD_TEST(showWindEffect);
	ADD_TEST(hideWindEffect);
	ADD_TEST(testCoinAnimation);
	ADD_TEST(testShowCoinEffect);
	ADD_TEST(testRunAddScoreAnimation);
    ADD_TEST(testDeduceEnergy);
	ADD_TEST(testCandyEffect);
	ADD_TEST(testStarEffect);
	ADD_TEST(testShowCollectItemEffect);
	ADD_TEST(moveUp);
	ADD_TEST(moveDown);
	ADD_TEST(autoUp);
	ADD_TEST(testAddScrollLayer);
	ADD_TEST(testAddScreenLayer);
	ADD_TEST(testParticle);
	ADD_TEST(testCollectCapsule2);
	ADD_TEST(testCollectCapsule);
	ADD_TEST(testExplode);
	ADD_TEST(testShieldBreak);
    ADD_TEST(testDoubleStar);
    ADD_TEST(testCaution);
	
}

#pragma mark -
#pragma mark Sub Test Definition
void EffectLayerTest::showWindEffect()
{
	if(mEffectLayer) {
		mEffectLayer->showWindEffect();
	}
}

void EffectLayerTest::hideWindEffect()
{
	if(mEffectLayer) {
		mEffectLayer->hideWindEffect();
	}
}

void EffectLayerTest::testCoinAnimation()
{
	if(mEffectLayer) {
		std::function<void()> callback = []() {
			log("Coin Animation Done");
		};
		
		mEffectLayer->runAddCoinAnimation(VisibleRect::center(), callback, 0.8f);
	}
}

void EffectLayerTest::moveUp()
{
	mSpeedY = 0;
	if(mEffectLayer) {
		mOffset += 50;
		setCamera(mOffset);
	}
}

void EffectLayerTest::moveDown()
{
	mSpeedY = 0;
	if(mEffectLayer) {
		mOffset -= 50;
		setCamera(mOffset);
	}
}

void EffectLayerTest::testRunAddScoreAnimation()
{
	
	if(mEffectLayer) {

		std::function<void()> callback = []() {
			log("Score Animation Done");
		};

		mEffectLayer->runAddScoreAnimation(100, VisibleRect::center(), callback, 0.8f);
	}
	
}

void EffectLayerTest::autoUp()
{
	if(mSpeedY == 0) {
		mSpeedY = 50;
	} else {
		mSpeedY = 0;
	}
}

void EffectLayerTest::testExplode()
{
	if(mEffectLayer) {
		mEffectLayer->showEffect(GameRes::MissileExplode, VisibleRect::center());
	}
}


void EffectLayerTest::testShieldBreak()
{
	if(mEffectLayer) {
		mEffectLayer->showEffect(GameRes::ShieldBreak, VisibleRect::center());
	}
}


void EffectLayerTest::testDoubleStar(){
    if(mEffectLayer) {
//        Vec2 pos = mPlayer->getPosition();
//        pos.y+=40;
        mEffectLayer->showDoubleStarEffect();
    }
}

void EffectLayerTest::testCaution()
{
    GameWorld::instance()->getGameUILayer()->showCaution(30);
}


void EffectLayerTest::testAddScreenLayer()
{
	auto label = Label::createWithTTF("Screen!!!", "arial.ttf", 30);
	label->setPosition(VisibleRect::center());
	
	mEffectLayer->addNodeToScreenLayer(label);
	
	
	DelayTime *delay = DelayTime::create(1);
	FadeOut *fadeOut = FadeOut::create(0.5);
	RemoveSelf *remove = RemoveSelf::create();
	
	Sequence *sequence = Sequence::create(delay, fadeOut, remove, nullptr);
	label->runAction(sequence);

}

void EffectLayerTest::testAddScrollLayer()
{
	auto label = Label::createWithTTF("Note on Map!!!", "arial.ttf", 30);
	label->setPosition(VisibleRect::center());
	
	mEffectLayer->addNodeToScrollLayer(label);
	
	
	DelayTime *delay = DelayTime::create(1);
	FadeOut *fadeOut = FadeOut::create(0.5);
	RemoveSelf *remove = RemoveSelf::create();
	
	Sequence *sequence = Sequence::create(delay, fadeOut, remove, nullptr);
	label->runAction(sequence);
	
}

void EffectLayerTest::testDeduceEnergy()
{
    mEffectLayer->showDeduceEnergyEffect(10);
}

void EffectLayerTest::testCollectCapsule()
{
	int itemEffect = ItemEffect::Type::ItemEffectMissile;
	
	std::function<void()> callback = [itemEffect]() {
		log("Try to trigger effect=%d", itemEffect);
	};
	
	mEffectLayer->showCollectCapsuleEffect(itemEffect, Vec2(100, 300), callback);
}

void EffectLayerTest::testCollectCapsule2()
{
	int itemEffect = ItemEffect::Type::ItemEffectMissile;
	
	std::function<void()> callback = [itemEffect]() {
		log("Try to trigger effect=%d", itemEffect);
	};
	
	mEffectLayer->showCollectCapsuleEffect(itemEffect, Vec2(100, 300), callback);
}

void EffectLayerTest::testParticle()
{
	Vec2 pos = Vec2(VisibleRect::center().x, 50);
	mEffectLayer->showScreenParticle("particle_slowapply.plist", pos);
}

void EffectLayerTest::testShowCollectItemEffect()
{
	std::string image = "guiImage/item_candystick.png";
	
	mEffectLayer->showCollectItemEffect(image, 0.4f, Vec2(50, 200), Vec2(20, 540),
						[&]{
							log("testShowCollectItemEffect: item collected");
						 });
}


void EffectLayerTest::testCandyEffect()
{
	mEffectLayer->showCollectManyCandyEffect(5, Vec2(20, 100));
}

void EffectLayerTest::testStarEffect()
{
	mEffectLayer->showCollectManyStarEffect(5, Vec2(20, 100));
}

void EffectLayerTest::testShowCoinEffect()
{
	mEffectLayer->showConsumeCoinEffect(VisibleRect::center());
}

#endif

