#ifdef ENABLE_TDD
//
//  ConsoleViewTest.h
//
//
#ifndef __TDD_ConsoleViewTest__
#define __TDD_ConsoleViewTest__

// Include Header

#include "TDDBaseTest.h"

class ConsoleView;

// Class Declaration
class ConsoleViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testAppend();

private:
	ConsoleView *mConsoleView;
};

#endif

#endif
