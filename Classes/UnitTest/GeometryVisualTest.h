#ifdef ENABLE_TDD
//
//  GeometryVisualTest.h
//
//
#ifndef __TDD_GeometryVisualTest__
#define __TDD_GeometryVisualTest__

// Include Header

#include "TDDTest.h"
#include <vector>

// Class Declaration 
class GeometryVisualTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void redraw();
	void update(float delta);
	
private:
	void subTest(Ref *sender);
	void testUpdateLogic(Ref *sender);
	void testHomingLogic(Ref *sender);
	
private: // data
	DrawNode *mDrawNode;
	std::vector<Rect> mRectArray;
	std::vector<Rect *> mRectRefArray;
	Rect mRect1;
	Rect mRect2;
	Rect mRectMoving;
	
	// Update Logic
	typedef std::function<void(float delta)> UpdateLogic;
	UpdateLogic mUpdateLogic;
};

#endif

#endif
