#ifdef ENABLE_TDD
//
//  AstrodogMailTest.m
//	TDD Framework
//
//
#include "AstrodogMailTest.h"
#include "TDDHelper.h"
#include "AstrodogData.h"

void AstrodogMailTest::setUp()
{
	log("TDD Setup is called");
}


void AstrodogMailTest::tearDown()
{
	log("TDD tearDown is called");
}

void AstrodogMailTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AstrodogMailTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AstrodogMailTest::defineTests()
{
	ADD_TEST(testObjects);
}

#pragma mark -
#pragma mark Sub Test Definition
void AstrodogMailTest::testObjects()
{
	AstrodogAttachment attachment;
	attachment.type = AstrodogAttachment::TypeDiamond;
	attachment.amount = 5;
	
	// Mail
	AstrodogMail mail;
	
	mail.setMailID(1234);
	mail.setTitle("Hello");
	mail.setMessage("Fun fun fun fun!");
	mail.setAttachment(attachment);
	mail.setCreateTime(12312312l);
	
	log("Mail: [%s]", mail.toString().c_str());
	
	
	// Mailbox
	
	AstrodogMailbox mailbox;
	mailbox.setupSampleData();
	
	log("Mailbox:\n%s\n", mailbox.toString().c_str());
	
}


#endif
