#ifdef ENABLE_TDD
//
//  NpcDistanceTest.m
//	TDD Framework
//
//
#include "NpcDistanceTest.h"
#include "TDDHelper.h"
#include "NpcDistanceLayer.h"
#include "NPCOrder.h"

void NpcDistanceTest::setUp()
{
	log("TDD Setup is called");
	mDistanceLayer = nullptr;
	mOrderIndex = 0;
}


void NpcDistanceTest::tearDown()
{
	log("TDD tearDown is called");
}

void NpcDistanceTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NpcDistanceTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NpcDistanceTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testShowUp);
	ADD_TEST(testNextNpc);
}

#pragma mark -
#pragma mark Sub Test Definition
void NpcDistanceTest::testSample()
{
	log("this is a sample subTest");
	NpcDistanceLayer *layer = NpcDistanceLayer::create();
	
	layer->setPosition(Vec2(0, 300));
	layer->setupDots(8);
	layer->setCurrentSpot(0, 1);
	
	addChild(layer);
}

void NpcDistanceTest::testNextNpc()
{
	setupDistanceLayer();
	
	NPCOrder *order = NPCOrder::create();
	order->setNpcID(1);
	order->setOrderIndex(mOrderIndex);
	
	mDistanceLayer->showNpcOrder(order);
	
	mOrderIndex++;
	
	

}

void NpcDistanceTest::setupDistanceLayer()
{
	if(mDistanceLayer == nullptr) {
		mDistanceLayer = NpcDistanceLayer::create();
		mDistanceLayer->setupDots(5);
		mDistanceLayer->setCurrentSpot(0, 1);
		
		mDistanceLayer->setOnCloseCallback([&]{
			log("DistanceLayer: onClose!!!");
		});
		
		addChild(mDistanceLayer);
	}

}

void NpcDistanceTest::testShowUp()
{
	setupDistanceLayer();
	
	mDistanceLayer->showUp(1.0);
}



#endif
