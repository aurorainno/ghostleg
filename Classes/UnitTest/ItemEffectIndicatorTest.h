#ifdef ENABLE_TDD
//
//  ItemEffectIndicatorTest.h
//
//
#ifndef __TDD_ItemEffectIndicatorTest__
#define __TDD_ItemEffectIndicatorTest__

// Include Header

#include "TDDBaseTest.h"

class ItemEffectIndicator;

// Class Declaration
class ItemEffectIndicatorTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testActivate();
    
private:
    ItemEffectIndicator* mIndicator;
};

#endif

#endif
