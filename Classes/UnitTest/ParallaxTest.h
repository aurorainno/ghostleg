#ifdef ENABLE_TDD
//
//  ParallaxTest.h
//
//
#ifndef __TDD_ParallaxTest__
#define __TDD_ParallaxTest__

// Include Header

#include "TDDBaseTest.h"

class SpriteParallaxSubLayer;
class ParallaxLayer;
class CsbParallaxSubLayer;

// Class Declaration 
class ParallaxTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void addParallaxLayer();
	void testSpaceScene();
	void testSpaceLayer();
	void subTest();
	void moveUp();
	void moveDown();
	void moveLeft();
	void moveRight();
	void testForest();
	void testScrollingSpace();
	void testBackgroundTint();
	void testTintParallax();
	void testHeaderParallax();
	void testCsbParallax();
	
	void togglePlay();
	void updateScroll();
	
	void update(float delta);
private:
	float mScrollY;
	float mScrollX;
	bool mIsPlaying;
	
	SpriteParallaxSubLayer *mTestSubLayer;
	CsbParallaxSubLayer *mCsbSubLayer;
	ParallaxLayer *mParallaxLayer;
}; 

#endif

#endif
