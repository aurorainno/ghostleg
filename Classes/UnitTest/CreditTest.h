#ifdef ENABLE_TDD
//
//  CreditTest.h
//
//
#ifndef __TDD_CreditTest__
#define __TDD_CreditTest__

// Include Header

#include "TDDTest.h"

class CreditSceneLayer;

// Class Declaration 
class CreditTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testScene(Ref *sender);
	void testOpenClose(Ref *sender);
	void testInputSecret(Ref *sender);

private:
	CreditSceneLayer *mLayer;

};

#endif

#endif
