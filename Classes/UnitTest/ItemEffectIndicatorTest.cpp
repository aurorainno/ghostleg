#ifdef ENABLE_TDD
//
//  ItemEffectIndicatorTest.m
//	TDD Framework
//
//
#include "ItemEffectIndicatorTest.h"
#include "ItemEffectIndicator.h"
#include "TDDHelper.h"
#include "CommonType.h"

void ItemEffectIndicatorTest::setUp()
{
	log("TDD Setup is called");
    mIndicator = nullptr;
}


void ItemEffectIndicatorTest::tearDown()
{
	log("TDD tearDown is called");
}

void ItemEffectIndicatorTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void ItemEffectIndicatorTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void ItemEffectIndicatorTest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(testActivate);
}

#pragma mark -
#pragma mark Sub Test Definition
void ItemEffectIndicatorTest::testSample()
{
	log("this is a sample subTest");
    ItemEffectIndicator* indicator = ItemEffectIndicator::create();
    mIndicator = indicator;
    mIndicator->config(ItemEffectIndicator::TimeStop, 5);
    Size size = getContentSize();
    mIndicator->setPosition(Size(size.width/2,size.height/2));
    addChild(mIndicator);
}

void ItemEffectIndicatorTest::testActivate()
{
    if(mIndicator){
        mIndicator->activate();
    }
}


#endif
