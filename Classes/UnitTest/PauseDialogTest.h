#ifdef ENABLE_TDD
//
//  PauseDialogTest.h
//
//
#ifndef __TDD_PauseDialogTest__
#define __TDD_PauseDialogTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class PauseDialogTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
