#ifdef ENABLE_TDD
//
//  UIRichTextTest.m
//	TDD Framework
//
//
#include "UIRichTextTest.h"
#include "RichTextLabel.h"
#include "TDDHelper.h"
#include "VisibleRect.h"

void UIRichTextTest::setUp()
{
	log("TDD Setup is called");
}


void UIRichTextTest::tearDown()
{
	log("TDD tearDown is called");
}

void UIRichTextTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void UIRichTextTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void UIRichTextTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testSetString);
	ADD_TEST(testCreateFromText);
}

#pragma mark -
#pragma mark Sub Test Definition
void UIRichTextTest::testSample()
{
	log("this is a sample subTest");
    RichTextLabel* richText = RichTextLabel::create();
    richText->ignoreContentAdaptWithSize(false);
    richText->setAnchorPoint(Vec2(0,1));
    richText->setContentSize(Size(300, 100));
    richText->setPosition(Vec2(VisibleRect::left().x, VisibleRect::top().y-30));
    addChild(richText);
    richText->setFont("Helvetica", 26);
    richText->add("save the ")->add("building", Color4B(255, 0, 0, 80));
    richText->addNewLine();
    richText->add("save all the ")->add("cats cats cats cats cats cats cats cats cats cats", Color4B(0, 255, 0, 255));
    
    log("width&height:%.2f,%.2f",richText->getVirtualRendererSize().width,richText->getVirtualRendererSize().height);

}

void UIRichTextTest::testSetString()
{
	
	std::string str = "Hello this is <c1>color</c1> <c2>highlight</c2> demo.\n"
						"This is the <c2>2nd</c2> line.";
	
	RichTextLabel *richText = RichTextLabel::create();
	//
	richText->setFont("Caviar_Dreams_Bold.ttf", 15);
	richText->setTextColor(1, Color4B::BLUE);
	richText->setTextColor(2, Color4B::GREEN);
	
	
	richText->ignoreContentAdaptWithSize(false);
	richText->setAnchorPoint(Vec2(0.5, 0.5));		// default
	//richText->setAnchorPoint(Vec2(0, 0));

	richText->setContentSize(Size(300, 100));
	
	richText->setPosition(VisibleRect::center());
	
	addChild(richText);
	
	richText->setString(str);
	
	richText->setFont("Helvetica", 26);
	
}



void UIRichTextTest::testCreateFromText()
{
	Vec2 pos1 = VisibleRect::center() + Vec2(0, 50);
	Vec2 pos2 = VisibleRect::center() - Vec2(0, 50);
	
	Text *text = Text::create();
	text->setPosition(pos1);
	text->setAnchorPoint(Vec2(0.5, 0.5));
	text->setFontName("Caviar_Dreams_Bold.ttf");
	text->setTextColor(Color4B::WHITE);
	text->setFontSize(30.0f);
	text->setScale(0.5f);
	text->setString("Testing Testing");
	
	addChild(text);
	
	RichTextLabel *richText = RichTextLabel::createFromText(text);
	richText->setTextColor(1, Color4B::GREEN);
	richText->setTextColor(2, Color4B::ORANGE);
	
	//
	richText->setPosition(pos2);
	richText->setString("<c2>Hello</c2> <c1>World</c1> ABC\nTesting!");
	
	//
	addChild(richText);
	
	//Text *text = Rich
	
//	std::string str = "Hello this is <c1>color</c1> <c2>highlight</c2> demo.\n"
//	"This is the <c2>2nd</c2> line.";
//	
//	RichTextLabel *richText = RichTextLabel::create();
//	//
//	richText->setFont("Caviar_Dreams_Bold.ttf", 15);
//	richText->setTextColor(1, Color4B::BLUE);
//	richText->setTextColor(2, Color4B::GREEN);
//	
//	
//	richText->ignoreContentAdaptWithSize(false);
//	richText->setAnchorPoint(Vec2(0.5, 0.5));		// default
//	//richText->setAnchorPoint(Vec2(0, 0));
//	
//	richText->setContentSize(Size(300, 100));
//	
//	richText->setPosition(VisibleRect::center());
//	
//	addChild(richText);
//	
//	richText->setString(str);
//	
//	richText->setFont("Helvetica", 26);
	
}


#endif
