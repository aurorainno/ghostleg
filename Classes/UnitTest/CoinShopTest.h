#ifdef ENABLE_TDD
//
//  CoinShopTest.h
//
//
#ifndef __TDD_CoinShopTest__
#define __TDD_CoinShopTest__

// Include Header

#include "TDDBaseTest.h"

class CoinShopSceneLayer;

// Class Declaration
class CoinShopTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testAddCandySuccess();
	void testAddStarSuccess();
	void testCoinShopScene();
	void testShowAlert();
	
private:
	CoinShopSceneLayer *mShopLayer;
};

#endif

#endif
