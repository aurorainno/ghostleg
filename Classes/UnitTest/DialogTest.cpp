#ifdef ENABLE_TDD
//
//  DialogTest.m
//	TDD Framework 
//
//
#include "DialogTest.h"
#include "TDDHelper.h"
#include "GameOverDialog.h"

void DialogTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void DialogTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void DialogTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(DialogTest::testGameOver);
	SUBTEST(DialogTest::testHideRevive);
	SUBTEST(DialogTest::testUnlockMsg);
	SUBTEST(DialogTest::testGameOverNewScore);
}


#pragma mark -
#pragma mark Helper 
GameOverDialog *DialogTest::createGameOverDialog()
{
	GameOverDialog *dialog = GameOverDialog::create();
	
	dialog->setOKListener([] (Ref *) {
		log("ok is called");	// Retry
	});
	
	dialog->setCancelListener([] (Ref *) {
		log("cancel is called"); // Back to Main
	});
	
	dialog->setShareListener([] (Ref *) {
		log("share is called");
	});
	dialog->setReviveListener([] (Ref *) {
		log("OneMoreChance is called");
	});

	
	return dialog;
}


#pragma mark -
#pragma mark Sub Test Definition
void DialogTest::testHideRevive(Ref *sender)
{
	log("this is a sample subTest");
	
	GameOverDialog *dialog = createGameOverDialog();
	addChild(dialog);
	
	dialog->hideReviveButton();
}

void DialogTest::testGameOver(Ref *sender)
{
	log("this is a sample subTest");
	
	GameOverDialog *dialog = createGameOverDialog();
	addChild(dialog);
}

void DialogTest::testGameOverNewScore(Ref *sender)
{
	log("this is a sample subTest");
	
	GameOverDialog *dialog = createGameOverDialog();
	
	dialog->setLastBestDistance(-10);	// will trigger setBestScoreEffect
	
	addChild(dialog);
	
	
}


void DialogTest::testUnlockMsg(Ref *sender)
{
	GameOverDialog *dialog = createGameOverDialog();
	addChild(dialog);
	//dialog->setUnlockHint("");
	dialog->setUnlockHint("Testing Testing");
	
}

#endif
