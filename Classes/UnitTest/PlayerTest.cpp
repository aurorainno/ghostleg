#ifdef ENABLE_TDD
//
//  PlayerTest.m
//	TDD Framework 
//
//
#include "PlayerTest.h"
#include "TDDHelper.h"
#include "Player.h"
#include "VisibleRect.h"
#include "CommonType.h"
#include "StageParallaxLayer.h"

void PlayerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	// Setup the space background
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();


	Player *player = Player::create();
	player->setPosition(VisibleRect::center());
	addChild(player);

	setPlayer(player);
	
	mDialogType = (int) Player::DialogType::DialogNone;
	
	mIsEffectOn = false;
	
	

}


void PlayerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void PlayerTest::defineTests()
{
	ADD_TEST(changeFace);
	ADD_TEST(testBlockAnimation);
	ADD_TEST(testSpeedupEffect);
	ADD_TEST(testMagnetEffect);
	ADD_TEST(changeDialog);
	ADD_TEST(testCloseDialog);
	ADD_TEST(testActivateBooster);
	ADD_TEST(testGivingBox)
	ADD_TEST(testReviving);
	
	ADD_TEST(testDying);
	ADD_TEST(step);
	ADD_TEST(testSlowEffect);
	ADD_TEST(testPackageBox);
}


#pragma mark -
#pragma mark Sub Test Definition
void PlayerTest::testActivateBooster()
{
	getPlayer()->activateBooster(BoosterItemMissile);
}

void PlayerTest::step()
{
	if(getPlayer()) {
		getPlayer()->update(0.3f);
	}
}


void PlayerTest::testBlockAnimation()
{
	std::string csbName = "particle_block.csb";
	//getPlayer()->setEffectStatus(EffectStatusSlow, 0.5f);
	
	getPlayer()->setFace(true);
	getPlayer()->addCsbAnimeByName(csbName, GameModel::ObjectNode, 0.5,Vec2::ZERO);
	getPlayer()->playCsbAnime(csbName, "active", true);
}

void PlayerTest::changeFace()
{
	bool isLeft = getPlayer()->isFaceLeft();
	getPlayer()->setFace(! isLeft);
}

void PlayerTest::testSlowEffect()
{
	getPlayer()->setEffectStatus(EffectStatusSlow, 0.5f);
}

void PlayerTest::testReviving()
{
	log("this is a sample subTest");
	

	if(getPlayer()) {
		getPlayer()->setReviving();
	}
}


void PlayerTest::testDying()
{
	if(getPlayer() == nullptr) {
		return;
	}
	
	getPlayer()->setPosition(Vec2(100, 80));

	
	Vec2 pos = Vec2(100, 300);
	createTestButton("Die", pos, [&](Ref *){
		getPlayer()->showDyingAnimation();
	});
	
	pos.x += 100;
	createTestButton("Restore", pos, [&](Ref *){
		getPlayer()->stopDyingAnimation();
	});
}

void PlayerTest::testPackageBox()
{
	Player *player = getPlayer();
	if(player == nullptr) {
		return;
	}
	
	player->setPackageVisible(true);
	//player->updatePackageBoxAnimation();

}


void PlayerTest::testGivingBox()
{
	Player *player = getPlayer();
	if(player == nullptr) {
		return;
	}
	
	player->setPackageVisible(true);
	player->showGivingPackage([&](){
		log("GivingBox Done");
	});
	//player->updatePackageBoxAnimation();
	

}


void PlayerTest::testShowDialog()
{
	if(mPlayer) {
		mPlayer->showDialog((Player::DialogType) mDialogType, 0, 3);
	}

}

void PlayerTest::changeDialog()
{
	mDialogType++;
	if(mDialogType > (int) Player::DialogThankYou) {
		mDialogType = Player::DialogNone;
	}

	testShowDialog();
}

void PlayerTest::testCloseDialog()
{
	if(mPlayer) {
		mPlayer->closeDialog();
	}
}

void PlayerTest::testMagnetEffect()
{
	if(mPlayer == nullptr) {
		return;
	}
	std::string csbName = "particle_magnet.csb";
	
	
	if(mIsEffectOn) {
		mPlayer->removeCsbAnimeByName(csbName);
		mIsEffectOn = false;
	} else {
		mPlayer->addCsbAnimeByName(csbName, false, 0.8f);
		mPlayer->playCsbAnime(csbName, "active", true);
		mIsEffectOn = true;
	}
	
}


void PlayerTest::testSpeedupEffect()
{
	if(mPlayer == nullptr) {
		return;
	}
	std::string csbName = "particle_speedup.csb";
	
	
	if(mIsEffectOn) {
		mPlayer->removeCsbAnimeByName(csbName);
		mIsEffectOn = false;
	} else {
		mPlayer->addCsbAnimeByName(csbName, false, 0.8f);
		mPlayer->playCsbAnime(csbName, "active", true);
		mIsEffectOn = true;
	}
	
}



#endif
