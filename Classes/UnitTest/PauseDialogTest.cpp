#ifdef ENABLE_TDD
//
//  PauseDialogTest.m
//	TDD Framework 
//
//
#include "PauseDialogTest.h"
#include "TDDHelper.h"
#include "PauseDialog.h"

void PauseDialogTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void PauseDialogTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void PauseDialogTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(PauseDialogTest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void PauseDialogTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	
	PauseDialog *dialog = PauseDialog::create();
	addChild(dialog);
}


#endif
