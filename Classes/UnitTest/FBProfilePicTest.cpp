#ifdef ENABLE_TDD
//
//  FBProfilePicTest.m
//	TDD Framework
//
//
#include "FBProfilePicTest.h"
#include "TDDHelper.h"
#include "FBProfilePic.h"

void FBProfilePicTest::setUp()
{
	log("TDD Setup is called");
	
	mProfilePic = FBProfilePic::create();
	mProfilePic->defineSize(Size(50, 50));
	//mProfilePic->setScale(0.5f);			// set the sprite just by scale
	mProfilePic->setPosition(Vec2(250, 200));
	
	addChild(mProfilePic);
	
}


void FBProfilePicTest::tearDown()
{
	log("TDD tearDown is called");
}

void FBProfilePicTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void FBProfilePicTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void FBProfilePicTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testDifferentSize);
	ADD_TEST(setProfilePic);
	ADD_TEST(clearProfilePic);
	ADD_TEST(clearCache);
}

#pragma mark -
#pragma mark Sub Test Definition
void FBProfilePicTest::testSample()
{
	//std::string fbid = "100009392333076";
	std::string fbid = "111111";
	
	FBProfilePic *sprite = FBProfilePic::create();
	sprite->setContentSize(Size(100, 100));
	sprite->setPosition(Vec2(60, 300));
	
	sprite->setPlaceHolderImage("guiImage/default_profilepic.png");
	
	sprite->setWithFBId(fbid);
	
	addChild(sprite);
}

void FBProfilePicTest::testDifferentSize()
{
	std::vector<FBProfilePic *> profileList;
	
	Vec2 pos = Vec2(10, 200);
	int size = 20;
	
	for(int i=0; i<5; i++) {
		
		FBProfilePic *sprite = FBProfilePic::create();
		sprite->setContentSize(Size(size, size));
		sprite->setPosition(pos);
		sprite->setAnchorPoint(Vec2(0, 0));
		//sprite->setPlaceHolderImage("guiImage/default_profilepic.png");
		
		addChild(sprite);
		
		pos.y += (size) + 10;
		size += 20;
		
		profileList.push_back(sprite);
	}

	//std::string fbid = "100009392333076";
	std::string fbid = "155843647958143";		// startupbeat
	for(FBProfilePic *profilepic : profileList) {
		profilepic->setWithFBId(fbid);
	}
	
}

void FBProfilePicTest::setProfilePic()
{
	mProfilePic->setWithFBId("155843647958143");
}

void FBProfilePicTest::clearCache()
{
	FBProfilePic::clearProfilePicCache("155843647958143");
}

void FBProfilePicTest::clearProfilePic()
{
	mProfilePic->setWithFBId("");
}

#endif
