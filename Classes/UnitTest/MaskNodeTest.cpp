#ifdef ENABLE_TDD
//
//  MaskNodeTest.m
//	TDD Framework
//
//
#include "MaskNodeTest.h"
#include "TDDHelper.h"
#include "CircularMaskNode.h"
#include "VisibleRect.h"

void MaskNodeTest::setUp()
{
	log("TDD Setup is called");
	
	CircularMaskNode *node = CircularMaskNode::create();
	node->setPosition(VisibleRect::center() + Vec2(0, 100));
	//node
	
	Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");

	node->addChild(sprite);
	
	addChild(node);
	
	
	mMaskNode = node;
	mMaskNode->updateAngleByPercent(1);
}


void MaskNodeTest::tearDown()
{
	log("TDD tearDown is called");
}

void MaskNodeTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void MaskNodeTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void MaskNodeTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testAddAngle);
}

#pragma mark -
#pragma mark Sub Test Definition
void MaskNodeTest::testSample()
{
	log("this is a sample subTest");
	
	
	
	
	CircularMaskNode *node = CircularMaskNode::create();
	node->setPosition(VisibleRect::center());
	
	Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");
    sprite->setScale(sprite->getScale()*0.5);
	node->addChild(sprite);
	
	addChild(node);
}

void MaskNodeTest::testAddAngle()
{
	static float percent = 0;
	
	mMaskNode->updateAngleByPercent(percent);
	
	
	percent += 20;
}
#endif
