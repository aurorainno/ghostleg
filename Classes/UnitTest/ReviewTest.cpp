#ifdef ENABLE_TDD
//
//  ReviewTest.m
//	TDD Framework
//
//
#include "ReviewTest.h"
#include "TDDHelper.h"
#include "PluginReview/PluginReview.h"
#include "RateAppHelper.h"

void ReviewTest::setUp()
{
	log("TDD Setup is called");
	RateAppHelper::getInstance()->init();
	
}


void ReviewTest::tearDown()
{
	log("TDD tearDown is called");
}

void ReviewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void ReviewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void ReviewTest::defineTests()
{
	ADD_TEST(testShow);
	ADD_TEST(testSetMessage);
}

#pragma mark -
#pragma mark Sub Test Definition
void ReviewTest::testShow()
{
	log("this is a sample subTest");
	sdkbox::PluginReview::show(true);
}

void ReviewTest::testSetMessage()
{
	//sdkbox::PluginReview::setTitle("");
}

#endif
