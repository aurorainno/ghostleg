#ifdef ENABLE_TDD
//
//  StageSelectionViewTest.h
//
//
#ifndef __TDD_StageSelectionViewTest__
#define __TDD_StageSelectionViewTest__

// Include Header

#include "TDDBaseTest.h"

class StageSelectionView;

// Class Declaration
class StageSelectionViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void nextStage();
	void lastStage();
	
private:
	StageSelectionView *mSelectionView;
};

#endif

#endif
