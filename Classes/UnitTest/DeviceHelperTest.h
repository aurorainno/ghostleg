#ifdef ENABLE_TDD
//
//  DeviceHelperTest.h
//
//
#ifndef __TDD_DeviceHelperTest__
#define __TDD_DeviceHelperTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class DeviceHelperTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testExternalPath(Ref *sender);
	void testConnectInternet(Ref *sender);
    void testGetBuildNumber(Ref *sender);
    void testGetCountryCode(Ref *sender);
}; 

#endif

#endif
