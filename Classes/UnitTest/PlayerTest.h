#ifdef ENABLE_TDD
//
//  PlayerTest.h
//
//
#ifndef __TDD_PlayerTest__
#define __TDD_PlayerTest__

// Include Header

#include "TDDBaseTest.h"

class Player;

// Class Declaration 
class PlayerTest : public TDDBaseTest
{
protected:
	CC_SYNTHESIZE(Player *, mPlayer, Player);
	
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testShowDialog();
	void changeDialog();
	void testCloseDialog();
	void testActivateBooster();
	void testReviving();
	void testDying();
	void testSlowEffect();
	void testPackageBox();
	void testGivingBox();
	void testMagnetEffect();
	void testSpeedupEffect();
	void testBlockAnimation();
	void changeFace();
	void step();
	
private:
	int mDialogType;
	bool mIsEffectOn;
};

#endif

#endif
