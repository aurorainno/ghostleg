#ifdef ENABLE_TDD
//
//  ShaderHelperTest.h
//
//
#ifndef __TDD_ShaderHelperTest__
#define __TDD_ShaderHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class ShaderHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testNormalNode();
	void testShaderNode();
}; 

#endif

#endif
