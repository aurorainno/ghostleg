#ifdef ENABLE_TDD
//
//  EnemyTest.m
//	TDD Framework 
//
//
#include "EnemyTest.h"
#include "TDDHelper.h"
#include "Enemy.h"
#include "EnemyFactory.h"
#include "VisibleRect.h"
#include "cocostudio/CocoStudio.h"
#include "ViewHelper.h"

void EnemyTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	//setEnemy(1);
}


void EnemyTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

Enemy *EnemyTest::createEnemy(int enemyID)
{
	Enemy *enemy = EnemyFactory::instance()->createEnemy(enemyID);
	if(enemy) {
		enemy->setPosition(VisibleRect::center());
		enemy->update(0);
		enemy->changeToActive();
	}
	return enemy;
}

void EnemyTest::setEnemy(int enemyID)
{
	mEnemyID = enemyID;
	
	
	if(mEnemy) {
		mEnemy->removeFromParent();
		mEnemy = nullptr;
	}
	
	
	Vec2 pos = VisibleRect::center();
	
	Enemy *enemy = EnemyFactory::instance()->createEnemy(mEnemyID);
	if(enemy) {
		enemy->setPosition(pos);
		addChild(enemy);
		enemy->update(0);
		enemy->changeToActive();
	}
	
	mEnemy = enemy;
}

#pragma mark -
#pragma mark List of Sub Tests

void EnemyTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testDirection);
	ADD_TEST(testSetupScaleAndFlip);
	ADD_TEST(testFlip);
	ADD_TEST(testScale2);
	ADD_TEST(testScale);
	ADD_TEST(testGivenEnemy);
	ADD_TEST(testEnemySize1);
	ADD_TEST(wakeUpAlert);
	ADD_TEST(changeEnemy);
	ADD_TEST(step);
	ADD_TEST(hitEnemy);
	ADD_TEST(testOpacity);
}

#pragma mark -
#pragma mark Sub Test Definition
void EnemyTest::subTest()
{
	log("this is a sample subTest");
}

void EnemyTest::testDirection()
{
	Enemy *enemy;
	
	//int testEnemy = 111;		// obstacle
	//int testEnemy
	int testEnemy = 4;
	
	DrawNode *drawNode = DrawNode::create();
	addChild(drawNode);
	
	Vec2 pos = Vec2(VisibleRect::center().x, 100);
	
	// Scale normal
	enemy = createEnemy(testEnemy);
	enemy->setPosition(pos);
	enemy->setFace(false);
	addChild(enemy);
	ViewHelper::drawAxis(drawNode, pos, Color4B::RED);
		
	pos.y += 120;
	
	// Flipped
	enemy = createEnemy(testEnemy);
	enemy->setFace(true);
	enemy->setPosition(pos);
	addChild(enemy);
	ViewHelper::drawAxis(drawNode, pos, Color4B::BLUE);
	
}

void EnemyTest::testSetupScaleAndFlip()
{
	Enemy *enemy;
	
	//int testEnemy = 111;		// obstacle
	int testEnemy = 202;		// obstacle
	//int testEnemy
	//int testEnemy = 4;
	
	DrawNode *drawNode = DrawNode::create();
	addChild(drawNode);
	
	Vec2 pos = Vec2(VisibleRect::center().x, 100);
	//Vec2 pos = Vec2(10, 100);
	
	
	std::vector<bool> flipXArray;
	std::vector<bool> flipYArray;
	
	flipXArray.push_back(false); flipYArray.push_back(false);
	flipXArray.push_back(false); flipYArray.push_back(true);
	flipXArray.push_back(true); flipYArray.push_back(false);
	flipXArray.push_back(true); flipYArray.push_back(true);
	
	float scaleX=0.6f; float scaleY = 0.4f;
	//float scaleX=1.0f; float scaleY = 1.0f;
	
	for(int i=0; i<flipXArray.size(); i++) {
		bool flipX = flipXArray[i];
		bool flipY = flipYArray[i];
		
		enemy = createEnemy(testEnemy);
		enemy->setupScaleAndFlip(scaleX, scaleY, flipX, flipY);
		enemy->setPosition(pos);
		
		addChild(enemy);
		
		ViewHelper::drawAxis(drawNode, pos, Color4B::RED);
		
		pos.y += 120;
	}
}

void EnemyTest::testFlip()
{
	Enemy *enemy;
	
	DrawNode *drawNode = DrawNode::create();
	addChild(drawNode);
	
	Vec2 pos = Vec2(VisibleRect::center().x, 100);
	
	
	std::vector<bool> flipXArray;
	std::vector<bool> flipYArray;
	
	flipXArray.push_back(false); flipYArray.push_back(false);
	flipXArray.push_back(false); flipYArray.push_back(true);
	flipXArray.push_back(true); flipYArray.push_back(false);
	flipXArray.push_back(true); flipYArray.push_back(true);
	
	
	for(int i=0; i<flipXArray.size(); i++) {
		bool flipX = flipXArray[i];
		bool flipY = flipYArray[i];
		
		enemy = createEnemy(111);
		enemy->setScaleX(0.5);
		enemy->setScaleY(0.5);
		enemy->setFlipX(flipX);
		enemy->setFlipY(flipY);
		enemy->setPosition(pos);
		
		addChild(enemy);
		
		
		ViewHelper::drawAxis(drawNode, pos, Color4B::RED);
		
		pos.y += 80;
	}
}

void EnemyTest::testScale2()
{
	Enemy *enemy;
	
	DrawNode *drawNode = DrawNode::create();
	addChild(drawNode);
	
	Vec2 pos = Vec2(VisibleRect::center().x, 100);
	
	
	std::vector<float> scaleXArray;
	std::vector<float> scaleYArray;
	
	scaleXArray.push_back(1.0f); scaleYArray.push_back(-1.0f);
	scaleXArray.push_back(-1.3f); scaleYArray.push_back(-0.5f);
	scaleXArray.push_back(-0.3f); scaleYArray.push_back(1.5f);
	
	for(int i=0; i<scaleXArray.size(); i++) {
		float scaleX = scaleXArray[i];
		float scaleY = scaleYArray[i];
		
		enemy = createEnemy(1);
		enemy->setScaleX(scaleX);
		enemy->setScaleY(scaleY);
		enemy->setPosition(pos);
		
		addChild(enemy);
		
		
		ViewHelper::drawAxis(drawNode, pos, Color4B::RED);
		
		pos.y += 80;
	}
}


void EnemyTest::testScale()
{
	Enemy *enemy;
	
	DrawNode *drawNode = DrawNode::create();
	addChild(drawNode);

	Vec2 pos = VisibleRect::center();
	
	
	std::vector<float> scaleXArray;
	std::vector<float> scaleYArray;
	
	scaleXArray.push_back(1.0f); scaleYArray.push_back(1.0f);
	scaleXArray.push_back(1.3f); scaleYArray.push_back(0.5f);
	scaleXArray.push_back(0.3f); scaleYArray.push_back(1.5f);
	
	for(int i=0; i<scaleXArray.size(); i++) {
		float scaleX = scaleXArray[i];
		float scaleY = scaleYArray[i];
		
		enemy = createEnemy(111);
		enemy->setScaleX(scaleX);
		enemy->setScaleY(scaleY);
		enemy->setPosition(pos);
		
		addChild(enemy);

		
		ViewHelper::drawAxis(drawNode, pos, Color4B::RED);
	}
}

void EnemyTest::testEnemySize1()
{
	//set
	const int nEnemy = 10;
	int enemyList[nEnemy] = {1, 8, 13, 2, 3, 4, 5, 6, 7, 12};
	//int enemyList[nEnemy] = {1, 8, 13, 2, 3, 4};
	
	float x1 = VisibleRect::center().x - 80;
	float x2 = VisibleRect::center().x + 80;
	float y = VisibleRect::top().y - 60;

	for(int i=0; i<nEnemy; i++) {
		int enemyID = enemyList[i];
		
		Enemy *enemy = Enemy::create();
		enemy->setResource(enemyID);
		//enemy->setAction(GameModel::Action::Walk);
		enemy->setVisible(true);
		
		float x = i % 2 == 0 ? x1 : x2;
		enemy->setPosition(Vec2(x, y));
		
		addChild(enemy);
		
		Rect rect = enemy->getBoundingBox();
		log("res: %d\tsize: %4.2f %4.2f", enemyID, rect.size.width, rect.size.height);
		
		if((i % 2) == 1) {
			y -= 80;
		}
	}
	
	
}

void EnemyTest::wakeUpAlert()
{
	log("Wake up Enemy");
	
	setEnemy(2);
//	
//	createTestButton("WakeUp", Vec2(100, 100),												   
//						[&](Ref* sender) {
//							mEnemy->showWakeupAlert();
//						}
//					);
}

void EnemyTest::testGivenEnemy()
{
	setEnemy(4);
}

void EnemyTest::changeEnemy()
{
	int nextEnemy = ((mEnemyID + 1) % 10) + 1;
	setEnemy(nextEnemy);
}

void EnemyTest::step()
{
	if(mEnemy) {
		mEnemy->update(0.3);
	}
	
}

void EnemyTest::hitEnemy()
{
	if(mEnemy) {
		mEnemy->beHit();
		mEnemy->showKnockoutEffect(Vec2(10, 10));
	}
	
}

void EnemyTest::testOpacity()
{
	setEnemy(1);
	
	mEnemy->setOpacity(100);
}

#endif
