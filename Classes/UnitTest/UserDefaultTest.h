#ifdef ENABLE_TDD
//
//  UserDefaultTest.h
//
//
#ifndef __TDD_UserDefaultTest__
#define __TDD_UserDefaultTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class UserDefaultTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testUserCounter(Ref *sender);
}; 

#endif

#endif
