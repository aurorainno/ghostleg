#ifdef ENABLE_TDD
//
//  AdColonyTest.m
//	TDD Framework 
//
//
#include "AdColonyTest.h"
#include "TDDHelper.h"
#include "AdColonyHandler.h"

void AdColonyTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	mHandler = new AdColonyHandler();
	mHandler->setup();
}


void AdColonyTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	delete mHandler;
}

#pragma mark -
#pragma mark List of Sub Tests

void AdColonyTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	
	
	SUBTEST(AdColonyTest::testVideo1);
	SUBTEST(AdColonyTest::testVideo2);
}

#pragma mark -
#pragma mark Sub Test Definition
void AdColonyTest::testVideo1(Ref *sender)
{
	log("Video1: %d", mHandler->isAdReady("video1"));
	mHandler->showVideo("video1");
	
}

void AdColonyTest::testVideo2(Ref *sender)
{
	log("Video2: %d", mHandler->isAdReady("video2"));
	mHandler->showVideo("video2");
}


#endif
