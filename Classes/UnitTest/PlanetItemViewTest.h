#ifdef ENABLE_TDD
//
//  PlanetItemViewTest.h
//
//
#ifndef __TDD_PlanetItemViewTest__
#define __TDD_PlanetItemViewTest__

// Include Header

#include "TDDBaseTest.h"

class PlanetItemView;

// Class Declaration
class PlanetItemViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testChangePlanet();
	void testPlanetSelect();
	void testSetDefaultPlanet();
	void testPlanetCsb();
	void testUpgrade();
	void testAllItemView();

private:
	PlanetItemView *mPlanetView;
};

#endif

#endif
