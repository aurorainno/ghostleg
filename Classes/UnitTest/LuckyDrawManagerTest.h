#ifdef ENABLE_TDD
//
//  LuckyDrawManagerTest.h
//
//
#ifndef __TDD_LuckyDrawManagerTest__
#define __TDD_LuckyDrawManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class LuckyDrawManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testParseJSON();
	void testInfoRewardList();
	void testRoll();
};

#endif

#endif
