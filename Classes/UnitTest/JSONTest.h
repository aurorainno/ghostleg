#ifdef ENABLE_TDD
//
//  JSONTest.h
//
//
#ifndef __TDD_JSONTest__
#define __TDD_JSONTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class JSONTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void subTest();
    void subTest2();
    void masteryTest();
    void printMastery(const rapidjson::Value& mastery);
	void testGetJsonStrMap();
	void testJSONData();
    void testAstrodogUserData();
}; 

#endif

#endif
