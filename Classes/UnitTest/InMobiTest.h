#ifdef ENABLE_TDD
//
//  InMobiTest.h
//
//
#ifndef __TDD_InMobiTest__
#define __TDD_InMobiTest__

// Include Header

#include "TDDBaseTest.h"

class AdHandler;

// Class Declaration
class InMobiTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testVideo2();
	void testHandlerVideo();
	
private:
	AdHandler *mAdHandler;
};

#endif

#endif
