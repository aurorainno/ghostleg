#ifdef ENABLE_TDD
//
//  DrawNodeTest.m
//	TDD Framework 
//
//
#include "DrawNodeTest.h"
#include "TDDHelper.h"

void DrawNodeTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	mDrawNode = DrawNode::create();
	addChild(mDrawNode);
}


void DrawNodeTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void DrawNodeTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(DrawNodeTest::testLine);
	SUBTEST(DrawNodeTest::testRect);
}

#pragma mark -
#pragma mark Sub Test Definition
void DrawNodeTest::testLine(Ref *sender)
{
	log("this is a sample subTest");
	
	DrawNode *drawNode = DrawNode::create();
	
	drawNode->setPosition(Vec2(100, 100));
	
	drawNode->drawLine(Vec2(0, 0), Vec2(100, 100), Color4F::RED);
	
	addChild(drawNode);
}


void DrawNodeTest::testRect(Ref *sender)
{
	static Vec2 pos(50, 50);
	Size size(50, 50);
	
	Rect rect = Rect(pos, size);
	
	Vec2 origin = Vec2(rect.getMinX(), rect.getMinY());
	Vec2 dest = Vec2(rect.getMaxX(), rect.getMaxY());
	
	mDrawNode->clear();
	mDrawNode->drawRect(origin, dest, Color4F::RED);
	
	pos.x += 5;
	pos.y += 10;
}


#endif
