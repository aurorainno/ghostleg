#ifdef ENABLE_TDD
//
//  ShareHelperTest.h
//
//
#ifndef __TDD_ShareHelperTest__
#define __TDD_ShareHelperTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class ShareHelperTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testSimpleShare(Ref *sender);
	void testShareScore(Ref *sender);
}; 

#endif

#endif
