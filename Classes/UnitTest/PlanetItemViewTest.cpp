#ifdef ENABLE_TDD
//
//  PlanetItemViewTest.m
//	TDD Framework
//
//
#include "PlanetItemViewTest.h"
#include "TDDHelper.h"
#include "PlanetItemView.h"
#include "PlanetSelectionView.h"


void PlanetItemViewTest::setUp()
{
	log("TDD Setup is called");
	mPlanetView = nullptr;
}


void PlanetItemViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void PlanetItemViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PlanetItemViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PlanetItemViewTest::defineTests()
{
	ADD_TEST(testUpgrade);
	ADD_TEST(testPlanetSelect);
	ADD_TEST(testAllItemView);
	ADD_TEST(testSample);
	ADD_TEST(testPlanetCsb);
	ADD_TEST(testChangePlanet);
	ADD_TEST(testSetDefaultPlanet);
}

#pragma mark -
#pragma mark Sub Test Definition
void PlanetItemViewTest::testSample()
{
	log("this is a sample subTest");

	PlanetItemView *view = PlanetItemView::create();
	
	addChild(view);
	
	view->setupPlanet(1, 100, false);
	
	mPlanetView = view;
}

void PlanetItemViewTest::testChangePlanet()
{
	if(mPlanetView == nullptr) {
		return;
	}
	
	static int planetID = 1;
	int distanceArray[4] = {
		0, 100, 30, -1
	};

	
	int distance = distanceArray[planetID];
	bool isLocked = distance < 0;
	
	
	//
	mPlanetView->setPlanet(planetID);
	mPlanetView->setBestDistance(distance);
	mPlanetView->setIsLocked(isLocked);
	
	// PlanetID
	planetID++;
	if(planetID > 3) {
		planetID = 1;
	}
}

void PlanetItemViewTest::testPlanetSelect()
{
	PlanetSelectionView *view = PlanetSelectionView::create();
	
	addChild(view);
	
	view->setSelectPlanetCallback([&](Ref *ref, int planetID, bool isRepeat, int currentPage, int maxPage) {
		log("planetID=%d isRepeat=%d", planetID, isRepeat);
	});
}

void PlanetItemViewTest::testSetDefaultPlanet()
{
	PlanetSelectionView *view = PlanetSelectionView::create();
	
	addChild(view);
	
	view->setSelectedPlanet(2);
}

void PlanetItemViewTest::testPlanetCsb()
{
	PlanetItemView *view = PlanetItemView::create();
	
	addChild(view);
	
	
	//view->testPlanetCsb("planetGUI/planet_gui_001.csb", true);
	view->testPlanetCsb("planetGUI/planet_gui_003.csb", false);
}


void PlanetItemViewTest::testAllItemView()
{
	log("this is a sample subTest");
	
	//void PlanetItemView::setupPlanet(int planetID, int bestDistance, bool isLocked)
	
	float y = 500;	// Director::getInstance()->convertToGL(Vec2(0, 160)).y;
	float x1 = 80;
	float x2 = 160;
	
	for(int planetID=1; planetID<=5; planetID++) {
		PlanetItemView *view;
		
		
		view = PlanetItemView::create();
		view->setupPlanet(planetID, 100, true);
		view->setPosition(Vec2(x1, y));
		addChild(view);
		
		view = PlanetItemView::create();
		view->setupPlanet(planetID, 100, false);
		view->setPosition(Vec2(x2, y));
		addChild(view);
		
		y -= 200;
	}
	
	
	
	//mPlanetView = view;
}


void PlanetItemViewTest::testUpgrade()
{
	mPlanetView->unlockPlanet([&](){
		log("DEBUG: planet is unlocked");
	});
}


#endif
