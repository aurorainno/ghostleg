#ifdef ENABLE_TDD
//
//  DailyRewardTest.m
//	TDD Framework
//
//
#include "DailyRewardTest.h"
#include "TDDHelper.h"
#include "DailyReward.h"
#include "RewardManager.h"
#include "TimeHelper.h"

void DailyRewardTest::setUp()
{
	log("TDD Setup is called");
    mDay = TimeHelper::getCurrentTimeStampInDays();
}


void DailyRewardTest::tearDown()
{
	log("TDD tearDown is called");
}

void DailyRewardTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DailyRewardTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DailyRewardTest::defineTests()
{
	ADD_TEST(testAddReward);
	ADD_TEST(testGetRewardAtDay);
	ADD_TEST(testDailyRewardDetail);
	ADD_TEST(testManager);
	ADD_TEST(testLoadData);
    ADD_TEST(testCollectReward);
    ADD_TEST(addOneDay);
    ADD_TEST(testSave);
    ADD_TEST(testReset);
}


void DailyRewardTest::testLoadData()
{
	RewardManager::instance()->loadData();
	
	log("%s", RewardManager::instance()->toString().c_str());
}

void DailyRewardTest::addMockData(DailyReward &dailyReward)
{
	
	RewardData *data;
	
	
	dailyReward.setCycleRange(5);
	
	// Setting the default
	//
	for(int d=1; d<=5; d++) {
		RewardData *data = RewardData::create(RewardData::RewardTypeStar, 0, d * 200);		// Day 1
		dailyReward.setDefaultReward(d, data);
	}
	
	//
	int day = 1;
	
	data = RewardData::create(RewardData::RewardTypeStar, 0, 100);		// Day 1
	dailyReward.addReward(day++, data);
	
	data = RewardData::create(RewardData::RewardTypeStar, 0, 500);		// Day 2
	dailyReward.addReward(day++, data);

	data = RewardData::create(RewardData::RewardTypeStar, 0, 600);		// Day 3
	dailyReward.addReward(day++, data);
	
	data = RewardData::create(RewardData::RewardTypeStar, 0, 800);		// Day 4
	dailyReward.addReward(day++, data);
	

}

#pragma mark -
#pragma mark Sub Test Definition
void DailyRewardTest::testAddReward()
{
	log("this is a sample subTest");
	
	DailyReward dailyReward;
	addMockData(dailyReward);
	
	log("%s", dailyReward.toString().c_str());
	
	dailyReward.cleanup();
	
	log("%s", dailyReward.toString().c_str());
}

void DailyRewardTest::testGetRewardAtDay()
{
	DailyReward dailyReward;
	addMockData(dailyReward);

	for(int day=1; day<=30; day++) {
		RewardData *reward = dailyReward.getRewardAtDay(day);
		
		log("day %d: %s", day, reward->toString().c_str());
	}
}


void DailyRewardTest::testDailyRewardDetail()
{
	DailyReward dailyReward;
	addMockData(dailyReward);
	
	log("%s", dailyReward.toString().c_str());
}

void DailyRewardTest::testManager()
{
	RewardManager::instance()->setup();
	
	log("%s", RewardManager::instance()->toString().c_str());
}

void DailyRewardTest::addOneDay()
{
    mDay++;
}

void DailyRewardTest::testCollectReward()
{
    if(RewardManager::instance()->checkDailyRewardStatus(RewardManager::DailyRewardStatusNoNeed)){
        log("Reward already collected today!");
        return;
    }else if(RewardManager::instance()->checkDailyRewardStatus(RewardManager::DailyRewardStatusIncreaseCycle)){
        RewardManager::instance()->increaseCycle();
    }else if(RewardManager::instance()->checkDailyRewardStatus(RewardManager::DailyRewardStatusNeedReset)){
        RewardManager::instance()->resetCollectedReward();
    }
    
    RewardManager::instance()->collectReward(mDay);
    
    log("%s",RewardManager::instance()->toString().c_str());
}

void DailyRewardTest::testSave()
{
    RewardManager::instance()->saveData();
}

void DailyRewardTest::testReset()
{
    RewardManager::instance()->resetAll();
}

#endif
