#ifdef ENABLE_TDD
//
//  StdDataTest.m
//	TDD Framework 
//
//
#include "StdDataTest.h"
#include "TDDHelper.h"
#include <stdio.h>
#include <vector>
#include "StringHelper.h"

USING_NS_CC;

void StdDataTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void StdDataTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void StdDataTest::defineTests()
{
	ADD_TEST(testVectorCopy);
	ADD_TEST(testSwap);
	ADD_TEST(testStdMap);
	ADD_TEST(testEraseVector);
	ADD_TEST(testEraseCocosVector);
}

#pragma mark -
#pragma mark Sub Test Definition

void StdDataTest::testStdMap()
{
	std::map<int, int> testMap;
	
	testMap[1] = 5;
	
	log("Before: %s", aurora::StringHelper::mapToString(testMap).c_str());
	
	int value = testMap[10];		// this will create an entry to testMap
	
	log("After: %s", aurora::StringHelper::mapToString(testMap).c_str());
	log("value=%d", value);
}

void StdDataTest::testEraseVector()
{
	static const int arr[] = {1, 2, 3, 4, 5, 6};
	std::vector<int> vec (arr, arr + sizeof(arr)/sizeof(arr[0]));
	
	log("data=%s", VECTOR_TO_STR(vec).c_str());
	
	std::vector<int>::iterator it = vec.begin();
	for(; it != vec.end(); it++) {
		int value = *it;
		log("debug: %d", value);
		if(value % 2 == 0) {
			vec.erase(it);
		}
	}
	
	log("after erase: data=%s", VECTOR_TO_STR(vec).c_str());
//	
//	vec.erase(vec.begin());				// 1
//	vec.erase(vec.begin() + 2);			// 3		// will be change after the last erase
//	vec.erase(vec.begin() + 4);			// 5
//	
//	log("after erase: data=%s", VECTOR_TO_STR(vec).c_str());
}

void StdDataTest::testEraseCocosVector()
{
	Vector<Node *> vec;
	for(int i=0; i<6; i++) {
		Node *node = Node::create();
		node->setTag(i);
		vec.pushBack(node);
	}
	
	// log("data=%s", VECTOR_TO_STR(vec).c_str());
	
	Vector<Node *>::iterator it = vec.begin();
	for(; it != vec.end(); it++) {
		Node *node = *it;
		int value = node->getTag();
		log("debug: %d", value);
		if(value % 2 == 0) {
			vec.erase(it);
		}
	}
	
	
	
	// log("after erase: data=%s", VECTOR_TO_STR(vec).c_str());
	//
	//	vec.erase(vec.begin());				// 1
	//	vec.erase(vec.begin() + 2);			// 3		// will be change after the last erase
	//	vec.erase(vec.begin() + 4);			// 5
	//
	//	log("after erase: data=%s", VECTOR_TO_STR(vec).c_str());
}



void StdDataTest::testEraseVectorBug()
{
	static const int arr[] = {1, 2, 3, 4, 5, 6};
	std::vector<int> vec (arr, arr + sizeof(arr)/sizeof(arr[0]));
	
	log("data=%s", VECTOR_TO_STR(vec).c_str());
	
	vec.erase(vec.begin());				// 1
	vec.erase(vec.begin() + 2);			// 3		// will be change after the last erase
	vec.erase(vec.begin() + 4);			// 5
	
	log("after erase: data=%s", VECTOR_TO_STR(vec).c_str());
}

void StdDataTest::testSwap()
{
	log("this is a sample subTest");
	
	static const int arr[] = {1, 2, 3, 4, 5, 6};
	std::vector<int> vec (arr, arr + sizeof(arr)/sizeof(arr[0]));

	//vec.swa
//	std::swap_ranges(vec.begin(), vec.begin(), vec.begin()+1);
	CC_SWAP(vec[2], vec[3], int);
	for(int i=0; i<vec.size(); i++){
		log("%d", vec[i]);
	}
}

void StdDataTest::testVectorCopy()
{
	
	static const int arr[] = {1, 2, 3, 4, 5, 6};
	std::vector<int> vec1 (arr, arr + sizeof(arr)/sizeof(arr[0]));
	
	std::vector<int> vec2 = vec1;		// copy happen here
	
	vec1.erase(vec1.begin());			// the first one is removed

	log("VEC1: %s", VECTOR_TO_STR(vec1).c_str());
	log("VEC2: %s", VECTOR_TO_STR(vec2).c_str());
}



#endif
