#ifdef ENABLE_TDD
//
//  AnimeGalleryTest.h
//
//
#ifndef __TDD_AnimeGalleryTest__
#define __TDD_AnimeGalleryTest__

// Include Header

#include "TDDBaseTest.h"

class GameModel;

// Class Declaration
class AnimeGalleryTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

protected:
	void setupBackground();
	void setupModel();
	void setupTouchControl();
	void setAnimationByIndex(int index);
	void setActionByIndex(int index);
	void setAnimation(const char *name);
	void setAction(int actionValue);
	void setNextAnimation();
	void setNextAction();
	
	
	void update(float delta);
private:
	void testSample();
	
	
private:
	int mAction;
	GameModel *mModel;
	int mSelectedAnimation;		// index in the array 
	int mSelectedAction;		// index in the array
};

#endif

#endif
