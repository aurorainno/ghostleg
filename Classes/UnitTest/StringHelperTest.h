#ifdef ENABLE_TDD
//
//  StringHelperTest.h
//
//
#ifndef __TDD_StringHelperTest__
#define __TDD_StringHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class StringHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void subTest();
	void testContainsNonAlpha();
	void testUrlEscape();
	void testContainsCJK();
	void testFromGeometry();
	void testFormatDecimal();
	void testSplit();
	void testEndsWith();
	void testParseInt();
	void testVectorToString();
	void testMapToString();
	void testCharVecToString();
    void testReplaceString();
}; 

#endif

#endif
