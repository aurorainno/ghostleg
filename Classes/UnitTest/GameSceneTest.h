#ifdef ENABLE_TDD
//
//  GameSceneTest.h
//
//
#ifndef __TDD_GameSceneTest__
#define __TDD_GameSceneTest__

// Include Header

#include "TDDBaseTest.h"

class GameSceneLayer;

// Class Declaration 
class GameSceneTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testAddCoinWithAnime();
	void testAddScoreWithAnime();
	void testCheckPointDialog();
	void testShowPuzzleCount();
	void testGameOver();
	void testShowNpcIndicator();
	void testShowTimeExtend();
	void testGameLayer();
	void testSetTutorialUI();
	void testHideTapHint();
	void testCapsuleButton();
	void testEnergyParticle();
	void testTopGUI();

	void testStartCountDown();
	void testStopCountDown();
	void testPauseCountDown();		// Also resume
	
	
private:
	GameSceneLayer *mGameLayer;
	
	bool mIsPaused;
};

#endif

#endif
