#ifdef ENABLE_TDD
//
//  GeometryHelperTest.m
//	TDD Framework 
//
//
#include "GeometryHelperTest.h"
#include "TDDHelper.h"
#include "GeometryHelper.h"
#include "MapLine.h"
#include "StringHelper.h"
#include "VisibleRect.h"

void GeometryHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void GeometryHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void GeometryHelperTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testAngle);
	ADD_TEST(testGetNodeFinalScale);
	ADD_TEST(testFindAngle);
	ADD_TEST(testResolveVec2);
	ADD_TEST(testCalcNewTracePos);
}

#pragma mark -
#pragma mark Sub Test Definition
void GeometryHelperTest::testAngle()
{
	float startX = 160; float startY = 200;
	Vec2 startPoint = Vec2(startX, startY);
	std::vector<Vec2> endPointList;
	endPointList.push_back(Vec2(210, 280));
	endPointList.push_back(Vec2(110, 280));
	endPointList.push_back(Vec2(210, 120));
	endPointList.push_back(Vec2(110, 120));
	
	
	for(Vec2 endPoint : endPointList) {
		//float rawAngle = startPoint.getAngle(endPoint);
		float angleRad = aurora::GeometryHelper::findAngleRadian(startPoint, endPoint);
		float angleDeg = aurora::GeometryHelper::findAngleDegree(startPoint, endPoint);
		float cocosDeg = aurora::GeometryHelper::findCocosDegree(startPoint, endPoint);
		
		std::string start = POINT_TO_STR(startPoint);
		std::string end = POINT_TO_STR(endPoint);
		
		log("start=%s end=%s rad=%f deg=%f ccDeg=%f", start.c_str(), end.c_str(),
						angleRad, angleDeg, cocosDeg);
	}
}


void GeometryHelperTest::testGetNodeFinalScale()
{
	// Data Setup
	LayerColor *blueLayer = LayerColor::create(Color4B::BLUE, 300, 300);
	blueLayer->setScale(0.8, 0.8);
	blueLayer->setPosition(Vec2(10, 10));
	addChild(blueLayer);
	
	LayerColor *greenLayer = LayerColor::create(Color4B::GREEN, 300, 300);
	greenLayer->setPosition(Vec2(10, 10));
	greenLayer->setScale(0.5, 0.5);
	blueLayer->addChild(greenLayer);
	
	
	Vec2 scaleBlue = aurora::GeometryHelper::getNodeFinalScale(blueLayer, this);
	Vec2 scaleGreen = aurora::GeometryHelper::getNodeFinalScale(greenLayer, this);
	
	
	log("scaleBlue=%s", POINT_TO_STR(scaleBlue).c_str());
	log("scaleGreen=%s", POINT_TO_STR(scaleGreen).c_str());
}

void GeometryHelperTest::subTest()
{
	log("this is a sample subTest");
	// Testing Vertical Case
	Vec2 point1 = Vec2(30, 50);
	Vec2 point2 = Vec2(30, 80);
	
	Vec2 checkPoint1 = Vec2(30, 60);
	//Vec2 checkPoint2 = Vec2(30, 100);
	Vec2 checkPoint2 = Vec2(30, 80);

	// Testing Horizontal Case
	Vec2 point3 = Vec2(30, 50);
	Vec2 point4 = Vec2(100, 50);
	
	Vec2 checkPoint3 = Vec2(40, 50);
	Vec2 checkPoint4 = Vec2(110, 50);
	Vec2 checkPoint5 = Vec2(30, 50);

	bool result1 = aurora::GeometryHelper::isLineIntersect(point1, point2, checkPoint1);
	bool result2 = aurora::GeometryHelper::isLineIntersect(point1, point2, checkPoint2);
	log("result1=%d", result1);
	log("result2=%d", result2);

	bool result3 = aurora::GeometryHelper::isLineIntersect(point3, point4, checkPoint3);
	bool result4 = aurora::GeometryHelper::isLineIntersect(point3, point4, checkPoint4);
	bool result5 = aurora::GeometryHelper::isLineIntersect(point3, point4, checkPoint5);
	log("result3=%d", result3);
	log("result4=%d", result4);
	log("result5=%d", result5);
	
	
	
}

//    4 --- 3
//    |     |
//    |     |
//    1 --- 2
void GeometryHelperTest::testFindAngle()
{
	Vec2 point1 = Vec2(50, 50);
	Vec2 point2 = Vec2(90, 50);
	Vec2 point3 = Vec2(90, 100);
	Vec2 point4 = Vec2(50, 100);
	Vec2 pointMid = point1.getMidpoint(point3);
	
	float rad12 = aurora::GeometryHelper::findAngleRadian(point1, point2);
	float deg12 = aurora::GeometryHelper::findAngleDegree(point1, point2);
	
	float rad13 = aurora::GeometryHelper::findAngleRadian(point1, point3);
	float deg13 = aurora::GeometryHelper::findAngleDegree(point1, point3);
	
	float rad14 = aurora::GeometryHelper::findAngleRadian(point1, point4);
	float deg14 = aurora::GeometryHelper::findAngleDegree(point1, point4);
	
	log("rad12=%f(%f) rad13=%f(%f) rad14=%f(%f)", deg12, rad12, deg13, rad13, deg14, rad14);
	
	float rad1 = aurora::GeometryHelper::findAngleRadian(pointMid, point1);
	float deg1 = aurora::GeometryHelper::findAngleDegree(pointMid, point1);
	
	float rad2 = aurora::GeometryHelper::findAngleRadian(pointMid, point2);
	float deg2 = aurora::GeometryHelper::findAngleDegree(pointMid, point2);
	
	float rad3 = aurora::GeometryHelper::findAngleRadian(pointMid, point3);
	float deg3 = aurora::GeometryHelper::findAngleDegree(pointMid, point3);
	
	float rad4 = aurora::GeometryHelper::findAngleRadian(pointMid, point4);
	float deg4 = aurora::GeometryHelper::findAngleDegree(pointMid, point4);
	
	log("rad1=%f(%f) rad2=%f(%f) rad3=%f(%f) rad4=%f(%f)",
				deg1, rad1, deg2, rad2, deg3, rad3, deg4, rad4);
}


//         |
//         |
//   ______|________
//         |
//         |
//		   |
//
void GeometryHelperTest::testResolveVec2()
{
	float angles[4] = {
			45, 135, 225, 315
	};
	
	float dist = 100;
	for(int i=0; i<4; i++) {
		float angleDegree = angles[i];
		float angleRad = MATH_DEG_TO_RAD(angleDegree);
		
		Vec2 result = aurora::GeometryHelper::resolveVec2(dist, angleRad);
		
		log("%f: result=%f,%f", angleDegree, result.x, result.y);
	}
	
//	Vec2 result = aurora::GeometryHelper::resolveVec2(100, angle);
//	
//
}

void GeometryHelperTest::testCalculateNewPos(MapLine *mapLine, float speed)
{
	log("test=[%s] speed=[%f]", mapLine->toString().c_str(), speed);

	for(float delta=0.0f; delta <= 1.0f; delta += 0.1f) {
		Vec2 start = mapLine->getStartPoint();
		Vec2 end = mapLine->getEndPoint();
		
		
		Vec2 pos = aurora::GeometryHelper::calculateNewTracePosition(start, end, speed, delta);
		
		log("delta=%f  pos=%s", delta, POINT_TO_STR(pos).c_str());
	}
}

void GeometryHelperTest::testCalcNewTracePos()
{
	float speed = 50.0f;
	// Case: My Pos =
	Vector<MapLine *> testLineList;
	testLineList.pushBack(MapLine::create(MapLine::eTypeSlope, 10, 10, 100, 100));  // Slope line
	testLineList.pushBack(MapLine::create(MapLine::eTypeSlope, 10, 10, 100, 10));
	
	for(MapLine *line : testLineList)
	{
		testCalculateNewPos(line, speed);
	}
	
	
//	for(float delta=0.0f); delta <= 1.0f; delta += 0.1f) {
//		Vec2 start =
//	aurora::GeometryHelper::calculateNewTracePosition(
//													  , <#const cocos2d::Vec2 &targetPos#>, <#float velocity#>, <#float timeDelta#>)
//	}

	
	
	
}


#endif
