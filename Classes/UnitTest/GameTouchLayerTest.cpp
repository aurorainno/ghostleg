#ifdef ENABLE_TDD
//
//  GameTouchLayerTest.m
//	TDD Framework 
//
//
#include "GameTouchLayerTest.h"
#include "TDDHelper.h"
#include "GameTouchLayer.h"
#include "GameMap.h"
#include "StringHelper.h"

const float kSpeed = 50;

void GameTouchLayerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");

	GameMap *map = GameMap::create();
	addChild(map);
	mMap = map;
	
	//
	mScrollY = 0;
	
	
	// Touch Layer
	GameTouchLayer *touchLayer = GameTouchLayer::create();
	addChild(touchLayer);
	touchLayer->setDrawLineCallback([&](Ref *sender, const Vec2 &start, const Vec2 &end){
		handleDrawLine(start, end);
	});
	mTouchLayer = touchLayer;
}


void GameTouchLayerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	
}


void GameTouchLayerTest::handleDrawLine(const Vec2 &start, const Vec2 &end)
{
	log("DrawLine: [%s] -> [%s]", POINT_TO_STR(start).c_str(), POINT_TO_STR(end).c_str());
	mMap->addSlopeLine(start, end);
}

void GameTouchLayerTest::update(float delta)
{
	mScrollY += kSpeed * delta;
	updateMap();
}

void GameTouchLayerTest::updateMap()
{
	mMap->setOffsetY(mScrollY);
	mTouchLayer->setOffsetY(mScrollY);
}

#pragma mark -
#pragma mark List of Sub Tests

void GameTouchLayerTest::defineTests()
{
	ADD_TEST(play);
	ADD_TEST(pause);
	ADD_TEST(moveUp);
	ADD_TEST(moveDown);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameTouchLayerTest::play()
{
	scheduleUpdate();
	
}

void GameTouchLayerTest::pause()
{
	unscheduleUpdate();
	
}

void GameTouchLayerTest::moveUp()
{
	mScrollY += kSpeed * 5;
	updateMap();
}


void GameTouchLayerTest::moveDown()
{
	float newScroll = mScrollY - kSpeed * 5;
	if(newScroll < 0) {
		newScroll = 0;
	}
	
	updateMap();
}


void GameTouchLayerTest::subTest()
{
	log("this is a sample subTest");
}


#endif
