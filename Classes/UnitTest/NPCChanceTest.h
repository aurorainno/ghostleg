#ifdef ENABLE_TDD
//
//  NPCChanceTest.h
//
//
#ifndef __TDD_NPCChanceTest__
#define __TDD_NPCChanceTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class NPCChanceTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testLoad();
	void testRollNpc();
};

#endif

#endif
