#ifdef ENABLE_TDD
//
//  MainSceneTest.m
//	TDD Framework 
//
//
#include "MainSceneTest.h"
#include "TDDHelper.h"
#include "MainScene.h"
#include "GameScene.h"

void MainSceneTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	mMainLayer = nullptr;
	mGameLayer = nullptr;
}


void MainSceneTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void MainSceneTest::defineTests()
{
	ADD_TEST(hideMainUI);
	ADD_TEST(showMainUI);
	ADD_TEST(testSetLeaderboardTime);
	ADD_TEST(testComingSoon);
	ADD_TEST(testShowUnlockPlanet);
	ADD_TEST(testShowUnlockDog);
	ADD_TEST(testMainScene);
	ADD_TEST(testGameScene);
}

#pragma mark -
#pragma mark Sub Test Definition
void MainSceneTest::testSetLeaderboardTime()
{
	long long remain = 35 * 3600 + 112;
	if(mMainLayer == nullptr) {
		testMainScene();
	}
	
	
	mMainLayer->setLeaderboardTime(remain);
}


void MainSceneTest::testComingSoon()
{
	if(mMainLayer == nullptr) {
		testMainScene();
	}
	mMainLayer->showComingSoonPopup();
}

void MainSceneTest::testMainScene()
{
	log("this is a sample subTest");
	
	MainSceneLayer *layer = MainSceneLayer::create();
	
	addChild(layer);
	
	mMainLayer = layer;
}

void MainSceneTest::hideMainUI()
{
	if(mMainLayer) {
		mMainLayer->hideMainUI();
	}
}

void MainSceneTest::showMainUI()
{
	if(mMainLayer) {
		mMainLayer->showMainUI();
	}
}


void MainSceneTest::testGameScene()
{
	log("this is a sample subTest");
	
	GameSceneLayer *layer = GameSceneLayer::create();
	
	layer->setStartOnEnter(false);
	
	addChild(layer);
	
	mGameLayer = layer;
}

void MainSceneTest::testShowUnlockPlanet()
{
	MainSceneLayer *layer = MainSceneLayer::create();
	addChild(layer);
	
	mMainLayer = layer;
	
	mMainLayer->showUnlockPlanetCoachmark();
}


void MainSceneTest::testShowUnlockDog()
{
	MainSceneLayer *layer = MainSceneLayer::create();
	addChild(layer);
	
	mMainLayer = layer;
	
//	mMainLayer->showUnlockDogCoachmark();
}

#endif
