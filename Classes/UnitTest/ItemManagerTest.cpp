#ifdef ENABLE_TDD
//
//  ItemManagerTest.m
//	TDD Framework
//
//
#include "ItemManagerTest.h"
#include "TDDHelper.h"
#include "PlayerBooster.h"
#include "ItemManager.h"
#include "GameManager.h"

void ItemManagerTest::setUp()
{
	log("TDD Setup is called");
}


void ItemManagerTest::tearDown()
{
	log("TDD tearDown is called");
}

void ItemManagerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void ItemManagerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void ItemManagerTest::defineTests()
{
	ADD_TEST(testBuy);
	ADD_TEST(testGetInfo);
	ADD_TEST(testLoadBoosterData);
	ADD_TEST(testSaveLoad);
	ADD_TEST(testBoosterData);
	ADD_TEST(testPlayerBoosterJSON);
	ADD_TEST(testPlayerBoosterValue);
}

#pragma mark -
#pragma mark Sub Test Definition
void ItemManagerTest::testBuy()
{
	bool isOkay;
	
	GameManager::instance()->addMoney(MoneyTypeDiamond, 100);
	
	log("current money: %s", GameManager::instance()->infoMoney().c_str());
	
	isOkay = ItemManager::instance()->buyBooster(BoosterItemTimeStop, 2);
	log("Buy Timestop: status=%d", isOkay);
	
	log("after buy timeStop: %s", GameManager::instance()->infoMoney().c_str());
	
	isOkay =ItemManager::instance()->buyBooster(BoosterItemMissile, 1);
	log("Buy missile: status=%d", isOkay);
	
	log("after buy missile: %s", GameManager::instance()->infoMoney().c_str());
}

void ItemManagerTest::testGetInfo()
{
	std::vector<BoosterItemType> typeArray;
	typeArray.push_back(BoosterItemTimeStop);
	typeArray.push_back(BoosterItemMissile);
	typeArray.push_back(BoosterItemSnowball);
	typeArray.push_back(BoosterItemDoubleCoin);
	typeArray.push_back(BoosterItemUnbreakable);
	
	
	for(BoosterItemType type : typeArray) {
		Price price = ItemManager::instance()->getBoosterPrice(type);
		std::string name = ItemManager::instance()->getBoosterName(type);
		std::string info = ItemManager::instance()->getBoosterInfo(type);
		
		log("type=%d price=%s name=[%s] info=[%s]", (int)type, price.toString().c_str(),
						name.c_str(), info.c_str());
	}
}
void ItemManagerTest::testLoadBoosterData()
{
	ItemManager::instance()->loadBoosterData();
	
	log("ItemManager:\n%s", ItemManager::instance()->infoBoosterDataSet().c_str());
}

void ItemManagerTest::testPlayerBoosterJSON()
{
	log("this is a sample subTest");
	
	PlayerBooster *booster = PlayerBooster::create();
	booster->setBoosterCount(BoosterItemMissile, 2);
	booster->setBoosterCount(BoosterItemDoubleCoin, 5);
	
	std::string json = booster->toJSONContent();
	log("JSON:\n%s\n", json.c_str());
	
	
	PlayerBooster *parseBooster = PlayerBooster::create();
	parseBooster->parseJSONContent(json);
	log("booster=%s", parseBooster->toString().c_str());
	
}

void ItemManagerTest::testPlayerBoosterValue()
{
	PlayerBooster *booster = PlayerBooster::create();
	booster->setBoosterCount(BoosterItemMissile, 2);
	booster->setBoosterCount(BoosterItemDoubleCoin, 5);
	
	
	log("Origin: %s", booster->toString().c_str());
	
	booster->addBooster(BoosterItemMissile);
	booster->deduceBooster(BoosterItemDoubleCoin);
	
	log("After Change: %s", booster->toString().c_str());
}

void ItemManagerTest::testSaveLoad()
{
	log("After Load: %s", ItemManager::instance()->toString().c_str());
	
	ItemManager::instance()->addBoosterCount(BoosterItemMissile, 3);
	ItemManager::instance()->addBoosterCount(BoosterItemDoubleCoin, 3);
	
	
	ItemManager::instance()->deduceBoosterCount(BoosterItemDoubleCoin);
	
	
	ItemManager::instance()->savePlayerData();
	
	ItemManager::instance()->loadPlayerData();
    
	
	log("After Save & Load: %s", ItemManager::instance()->toString().c_str());
}

void ItemManagerTest::testBoosterData()
{
	ItemManager::instance()->setupMockData();
	
	log("BoosterData: %s\n", ItemManager::instance()->infoBoosterDataSet().c_str());
}

#endif
