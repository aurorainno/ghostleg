#ifdef ENABLE_TDD
//
//  DataHelperTest.m
//	TDD Framework
//
//
#include "DataHelperTest.h"
#include "TDDHelper.h"
#include "DataHelper.h"
#include "StringHelper.h"

void DataHelperTest::setUp()
{
	log("TDD Setup is called");
}


void DataHelperTest::tearDown()
{
	log("TDD tearDown is called");
}

void DataHelperTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DataHelperTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DataHelperTest::defineTests()
{
	ADD_TEST(testDequeue);
	ADD_TEST(testDequeuePointer);
}

#pragma mark -
#pragma mark Sub Test Definition
void DataHelperTest::testDequeue()
{
	std::vector<int> queueVector;
	
	for(int i=0; i<13; i++){
		queueVector.push_back(i);
	}
	
	while(! queueVector.empty()) {
		std::vector<int> result;
		
		// DEQUEUE_VECTOR(queueVector, result, 5, int);
		aurora::DataHelper::dequeueVector(queueVector, result, 5);
		
		log("RESULT: [%s]", VECTOR_TO_STR(result).c_str());
	}
	
}

void DataHelperTest::testDequeuePointer()
{
	std::vector<Node *> queueVector;
	
	for(int i=0; i<13; i++){
		Node *node = Node::create();
		node->setName(StringUtils::format("Node-%d", i));
		queueVector.push_back(node);
	}
	
	while(! queueVector.empty()) {
		std::vector<Node *> result;
		
		// DEQUEUE_VECTOR(queueVector, result, 5, int);
		aurora::DataHelper::dequeueVector(queueVector, result, 5);
		
		std::string resultStr = "";
		for(Node *node : result) {
			resultStr += node->getName() + " ";
		}
		log("%s", resultStr.c_str());
	}
	
}



#endif
