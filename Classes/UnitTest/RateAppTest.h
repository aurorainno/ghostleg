#ifdef ENABLE_TDD
//
//  RateAppTest.h
//
//
#ifndef __TDD_RateAppTest__
#define __TDD_RateAppTest__

// Include Header

#include "TDDTest.h"
#include "RateAppHelper.h"
// Class Declaration 
class RateAppTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
    void resetRateNoticeFlag(Ref* sender);
    RateAppHelper* rateAppInstance;
}; 

#endif

#endif
