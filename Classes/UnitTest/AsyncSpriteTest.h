#ifdef ENABLE_TDD
//
//  AsyncSpriteTest.h
//
//
#ifndef __TDD_AsyncSpriteTest__
#define __TDD_AsyncSpriteTest__

// Include Header

#include "TDDBaseTest.h"

class AsyncSprite;

// Class Declaration
class AsyncSpriteTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testPlaceHolder();
	void testImageURL();
	void testRemoveImage();
	void testLoadAndRemove();
	
private:
	AsyncSprite *mSprite;
};

#endif

#endif
