#ifdef ENABLE_TDD
//
//  AndroidHelperTest.m
//	TDD Framework 
//
//
#include "AndroidHelperTest.h"
#include "TDDHelper.h"
#include "AndroidHelper.h"
#include "Constant.h"

void AndroidHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void AndroidHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void AndroidHelperTest::defineTests()
{
	ADD_TEST(testVoidStatic);
	ADD_TEST(testSetterMethod);
	ADD_TEST(testGetString);
}

#pragma mark -
#pragma mark Sub Test Definition
void AndroidHelperTest::testVoidStatic()
{
	//
	AndroidHelper::callVoidStaticMethod(ANDROID_CLASS(util.TestHelper), "testLog");
	//AndroidHelper::callVoidStaticMethod("com.aurorainno.GhostLeg.util.TestHelper", "testLog");
}

void AndroidHelperTest::testSetterMethod()
{
	//
	AndroidHelper::callVoidStaticMethod(ANDROID_CLASS(util.TestHelper), "testLogMessage", "Hello");
}

void AndroidHelperTest::testGetString()
{
	//
	std::string result = AndroidHelper::getStaticVoidMethodResult(
							ANDROID_CLASS(util.TestHelper), "testGetString");
	
	log("result from Android: %s", result.c_str());
}


#endif
