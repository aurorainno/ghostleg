#ifdef ENABLE_TDD
//
//  AsyncSpriteTest.m
//	TDD Framework
//
//
#include "AsyncSpriteTest.h"
#include "TDDHelper.h"
#include "AsyncSprite.h"
#include "VisibleRect.h"

void AsyncSpriteTest::setUp()
{
	log("TDD Setup is called");
	mSprite = nullptr;
}


void AsyncSpriteTest::tearDown()
{
	log("TDD tearDown is called");
}

void AsyncSpriteTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AsyncSpriteTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AsyncSpriteTest::defineTests()
{
	ADD_TEST(testImageURL);
	ADD_TEST(testLoadAndRemove);
	ADD_TEST(testRemoveImage);
	ADD_TEST(testPlaceHolder);
}

#pragma mark -
#pragma mark Sub Test Definition
void AsyncSpriteTest::testPlaceHolder()
{
	AsyncSprite *sprite = AsyncSprite::create();
	sprite->setContentSize(Size(100, 100));
	sprite->setPosition(Vec2(60, 300));
	
	sprite->setPlaceHolderImage("guiImage/btn_character.png");
	
	addChild(sprite);
}

void AsyncSpriteTest::testImageURL()
{
	// std::string url = "https://pbs.twimg.com/profile_images/621211556337053696/zHjs6E9l.png";
	
	std::string url = "https://graph.facebook.com/100009392333076/picture?type=large&width=100&height=100";
	AsyncSprite *sprite = AsyncSprite::create();
	sprite->setContentSize(Size(100, 100));
	sprite->setPosition(Vec2(60, 300));

	sprite->setPlaceHolderImage("guiImage/btn_character.png");
	sprite->loadFromURL(url);
	
	addChild(sprite);
	
	mSprite = sprite;
}


void AsyncSpriteTest::testRemoveImage()
{
	if(mSprite != nullptr) {
		mSprite->removeFromParent();		// Destruction happen here!!
		mSprite = nullptr;
	}
}

void AsyncSpriteTest::testLoadAndRemove()
{
	testImageURL();
	testRemoveImage();
}

#endif
