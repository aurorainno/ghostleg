#ifdef ENABLE_TDD
//
//  MovableModelTest.h
//
//
#ifndef __TDD_MovableModelTest__
#define __TDD_MovableModelTest__

// Include Header

#include "TDDTest.h"

class MovableModel;

// Class Declaration 
class MovableModelTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
	void toggleRun(Ref *sender);
	void testEnemyTexture(Ref *sender);
	void testEnemyFactory(Ref *sender);

private:
	MovableModel *mModel;
	bool mRunning;
};

#endif

#endif
