#ifdef ENABLE_TDD
//
//  NPCMapLogicTest.m
//	TDD Framework
//
//
#include "NPCMapLogicTest.h"
#include "TDDHelper.h"
#include "TDDHelper.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "ModelLayer.h"
#include "Player.h"
#include "Item.h"
#include "LevelData.h"
#include "DebugInfo.h"
#include "ItemEffect.h"
#include "EnemyFactory.h"
#include "Enemy.h"
#include "ViewHelper.h"
#include "StageManager.h"
#include "StringHelper.h"
#include "NPCMapLogic.h"
#include "StageParallaxLayer.h"

void NPCMapLogicTest::setUp()
{
	log("TDD Setup is called");
	
	log("TDD Setup is called");
	log("Please write somethings");
	
	
	setupParallax();
	
	GameWorld::instance()->getPlayer()->setPositionY(50);
	GameWorld::instance()->moveCameraToPlayer();
	
	// Setup the Game World
	
	mParallaxLayer->setMainNode(GameWorld::instance());
	GameWorld::instance()->setParallaxLayer(mParallaxLayer);
	
	addChild(GameWorld::instance());
	//GameWorld::instance()->start();
	
	// 
	// StageManager::instance()->setupMockData();
	
	int mapOffset = GameWorld::instance()->getCameraY();
	log("Map Offset: %d", mapOffset);
	
	mMapLogic = new NPCMapLogic();
	
	std::vector<int> maps = MapLevelDataCache::instance()->getAvailableMaps();
	std::sort(maps.begin(), maps.end());   // Sort in
	log("AvailableMaps: %s", VECTOR_TO_STR(maps).c_str());
}

void NPCMapLogicTest::setupParallax()
{
	// Setup background
	mParallaxLayer = StageParallaxLayer::create();
	mParallaxLayer->setShowRoute(true);
	mParallaxLayer->setStage(1);
	addChild(mParallaxLayer);
}


void NPCMapLogicTest::tearDown()
{
	log("TDD tearDown is called");
	delete mMapLogic;
}

void NPCMapLogicTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NPCMapLogicTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NPCMapLogicTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(addNpcMap);
	ADD_TEST(testAddPackageBox);
	ADD_TEST(moveUp);
	ADD_TEST(moveDown);
	ADD_TEST(setNpcMap1);
}

void NPCMapLogicTest::addNpcMap()
{
	mMapLogic->setTestMode(NPCMapLogic::NoGameMap);
	mMapLogic->setupWithNPC(1);
	GameWorld::instance()->getModelLayer()->update(0);
	
}


void NPCMapLogicTest::setNpcMap1()
{
	mMapLogic->setTestMode(NPCMapLogic::NoTest);
	
	mMapLogic->setupWithNPC(101);
	GameWorld::instance()->getModelLayer()->update(0);
	
}

void NPCMapLogicTest::testAddPackageBox()
{
	Vec2 pos = mMapLogic->getDropPosition();
	GameWorld::instance()->getModelLayer()->addPackageBox(pos, [&]{
		log("Packaged Delivered!");
	});
}

void NPCMapLogicTest::movePlayer(int offset)
{
	Player *player = GameWorld::instance()->getPlayer();
	
	//
	Vec2 position = player->getPosition();
	position += Vec2(0, offset);
	player->setPosition(position);
	GameWorld::instance()->moveCameraToPlayer();
	
	GameWorld::instance()->getModelLayer()->update(0);
	mMapLogic->updateWorldData(0);
	//
	int mapOffset = GameWorld::instance()->getCameraY();
	log("Map Offset: %d", mapOffset);
	log("Map DebugData : %s", mMapLogic->infoDebugData().c_str());
	log("Is At NPC map: %d", mMapLogic->isAtNpcMap());
	
}

#pragma mark -
#pragma mark Sub Test Definition
void NPCMapLogicTest::testSample()
{
	log("this is a sample subTest");
	
}

void NPCMapLogicTest::moveUp()
{
	movePlayer(80);
}

void NPCMapLogicTest::moveDown()
{
	movePlayer(-80);
}


#endif
