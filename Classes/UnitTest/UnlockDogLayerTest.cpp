#ifdef ENABLE_TDD
//
//  UnlockDogLayerTest.m
//	TDD Framework
//
//
#include "UnlockDogLayerTest.h"
#include "UnlockDogLayer.h"
#include "TDDHelper.h"
#include "FireWork.h"
#include "VisibleRect.h"
#include "BuyNewDogLayer.h"
#include "StageParallaxLayer.h"

void UnlockDogLayerTest::setUp()
{
	log("TDD Setup is called");
	
	// Setup the space background
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();
}


void UnlockDogLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void UnlockDogLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void UnlockDogLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void UnlockDogLayerTest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(testFireWork);
	ADD_TEST(testBuyUnlock);
}

#pragma mark -
#pragma mark Sub Test Definition
void UnlockDogLayerTest::testSample()
{
	clearNodes();
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();
	
	log("this is a sample subTest");
    std::queue<int> dogQueue;
    dogQueue.push(1);
    dogQueue.push(2);
    
    UnlockDogLayer* layer = UnlockDogLayer::create();
    layer->setDogQueue(dogQueue);
    addChild(layer);
	
	layer->setCloseCallback([&] {
		log("UnlockDog: unlock dog popup closed!!");
	});
}

void UnlockDogLayerTest::testFireWork()
{
    FireWork* fireWork = FireWork::create();
    fireWork->setup("firework.csb","new");
    fireWork->setPosition(VisibleRect::center());
    addChild(fireWork);
}

void UnlockDogLayerTest::testBuyUnlock()
{
	BuyNewDogLayer *layer = BuyNewDogLayer::create();
	
	addChild(layer);
	
}

#endif
