#ifdef ENABLE_TDD
//
//  FireworkTest.h
//
//
#ifndef __TDD_FireworkTest__
#define __TDD_FireworkTest__

// Include Header

#include "TDDBaseTest.h"

class FireworkLayer;

// Class Declaration
class FireworkTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void activateNextEmitter();
	void startFirework();
	void stopFirework();
	void changeInterval();

private:
	FireworkLayer *mFirework;
};

#endif

#endif
