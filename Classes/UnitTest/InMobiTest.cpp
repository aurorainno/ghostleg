#ifdef ENABLE_TDD
//
//  InMobiTest.m
//	TDD Framework
//
//

#include "InMobiTest.h"
#include "TDDHelper.h"
#include "AdHandler.h"

#if IS_IOS
#include "PluginInMobi/PluginInMobi.h"
#include "InMobiHandler.h"
#endif

void InMobiTest::setUp()
{
	log("TDD Setup is called");
	
	bool useHandler = true;
#if IS_IOS
	//
	if(useHandler) {
		mAdHandler = new InMobiHandler();
		mAdHandler->setup();
		
	
	} else {
		sdkbox::PluginInMobi::setLogLevel(sdkbox::PluginInMobi::SBIMSDKLogLevel::kIMSDKLogLevelDebug);
		sdkbox::PluginInMobi::init();
		mAdHandler = nullptr;
	}
#endif
	
	

	//
}


void InMobiTest::tearDown()
{
	log("TDD tearDown is called");
	CC_SAFE_DELETE(mAdHandler);
}

void InMobiTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void InMobiTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void InMobiTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testVideo2);
	ADD_TEST(testHandlerVideo);
}

#pragma mark -
#pragma mark Sub Test Definition
void InMobiTest::testSample()
{
#if IS_IOS
	log("this is a sample subTest");
	
	std::string ad = "video1";
	sdkbox::PluginInMobi::loadInterstitial(ad);
	if (sdkbox::PluginInMobi::isInterstitialReady(ad)) {
		log("Plugin InMobi interstitial ad is ready");
	
		sdkbox::PluginInMobi::showInterstitial(ad);
	} else {
		log("Plugin InMobi interstitial ad is not ready");
	}
#endif
}


void InMobiTest::testVideo2()
{
	log("this is a sample subTest");
#if IS_IOS
	std::string ad = "video2";
	sdkbox::PluginInMobi::loadInterstitial(ad);
	if (sdkbox::PluginInMobi::isInterstitialReady(ad)) {
		log("Plugin InMobi interstitial ad is ready");
		
		sdkbox::PluginInMobi::showInterstitial(ad);
	} else {
		log("Plugin InMobi interstitial ad is not ready");
	}
#endif
}

void InMobiTest::testHandlerVideo()
{
	if(mAdHandler == nullptr) {
		log("handler is null");
		return;
	}
	
	mAdHandler->setVideoFinishedCallback([&](bool isOkay){
		log("Ad is called");
	});
	
	bool flag = mAdHandler->isAdReady("video1");
	log("isReady: flag=%d", flag);
	
	mAdHandler->showVideo("video1");
}

#endif
