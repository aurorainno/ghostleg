#ifdef ENABLE_TDD
//
//  HorizontalListViewTest.h
//
//
#ifndef __TDD_HorizontalListViewTest__
#define __TDD_HorizontalListViewTest__

// Include Header

#include "TDDBaseTest.h"

class HorizontalListView;

// Class Declaration
class HorizontalListViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testItemList2();
	void testCallback();
	
private:
	HorizontalListView *mListView;
};

#endif

#endif
