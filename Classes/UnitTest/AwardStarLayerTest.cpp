#ifdef ENABLE_TDD
//
//  AwardStarLayerTest.m
//	TDD Framework
//
//
#include "AwardStarLayerTest.h"
#include "TDDHelper.h"
#include "StarAwardLayer.h"
#include "AdManager.h"
#include "ViewHelper.h"

void AwardStarLayerTest::setUp()
{
	log("TDD Setup is called");
}


void AwardStarLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void AwardStarLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AwardStarLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AwardStarLayerTest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(testAd);
}

#pragma mark -
#pragma mark Sub Test Definition
void AwardStarLayerTest::testSample()
{
	log("this is a sample subTest");
    StarAwardLayer* layer = StarAwardLayer::create();
    addChild(layer);
}

void AwardStarLayerTest::testAd()
{
    // Android and iOS ad colony behaviour
    //	in IOS, sequence is rewarded -> finished
    //	in Android, sequence is finished -> rewarded
    
    // Note: We can use Reward for Revive, because it has a daily cap!!
    // Show Video Ad
    AdManager::instance()->setVideoFinishedCallback([&](bool isFinished){
        
        StarAwardLayer* layer = StarAwardLayer::create();
        addChild(layer);
    });
    
    AdManager::instance()->setVideoRewardedCallback([&](bool isRewarded){
        
    });
    
    bool isOkay = AdManager::instance()->showVideoAd(AdManager::VideoAd::OneMoreChance);
    
    if(isOkay == false) {
        float y = Director::getInstance()->convertToGL(Vec2(0, 80)).y;
        ViewHelper::showFadeAlert(this, "Fail to play video, try again!", (int) y);
    }
}


#endif
