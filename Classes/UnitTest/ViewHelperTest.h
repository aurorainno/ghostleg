#ifdef ENABLE_TDD
//
//  ViewHelperTest.h
//
//
#ifndef __TDD_ViewHelperTest__
#define __TDD_ViewHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class ViewHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testAlert();
	void testOutlineText();
	void testCreateAnime();
	void testLoopAnime();
	void testBearingCoordinate();
	void testDrawPie();
	void testDrawLine();
	void testMaskTexture();
}; 

#endif

#endif
