#ifdef ENABLE_TDD
//
//  TiledTest.h
//
//
#ifndef __TDD_TiledTest__
#define __TDD_TiledTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class TiledTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
