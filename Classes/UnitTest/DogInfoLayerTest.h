#ifdef ENABLE_TDD
//
//  DogInfoLayerTest.h
//
//
#ifndef __TDD_DogInfoLayerTest__
#define __TDD_DogInfoLayerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class DogInfoLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testAbilityInfoPage();
	void testXmasDog();
	void testSetMoneyType();
};

#endif

#endif
