#ifdef ENABLE_TDD
//
//  MasterySceneTest.m
//	TDD Framework 
//
//
#include "MasterySceneTest.h"
#include "TDDHelper.h"
#include "MasteryItemView.h"
#include "Mastery.h"
#include "GameManager.h"
#include "MasteryManager.h"
#include "StringHelper.h"
#include "MasteryScene.h"
#include "Player.h"
#include "UserGameData.h"
#include "NotEnoughCoinDialog.h"

void MasterySceneTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void MasterySceneTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void MasterySceneTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(MasterySceneTest::simpleTest);
	SUBTEST(MasterySceneTest::testDiffLevel);
	SUBTEST(MasterySceneTest::testMasteryScene);
	SUBTEST(MasterySceneTest::testCallback);
    SUBTEST(MasterySceneTest::testNotEnoughDialog);
}

#pragma mark -
#pragma mark Sub Test Definition
void MasterySceneTest::simpleTest(Ref *sender)
{
	log("this is a sample subTest");
	
	MasteryItemView *itemView = MasteryItemView::create();
	
	itemView->setPosition(Vec2(100, 100));
	
	addChild(itemView);
	
	MasteryData *mastery = GameManager::instance()->getMasteryManager()->getMasteryData(2);
	
	itemView->setMastery(mastery);
	
	log("ContentSize: %s", SIZE_TO_STR(itemView->getContentSize()).c_str());
}

void MasterySceneTest::testDiffLevel(Ref *sender)
{
	int levelArray[3] = {0, 1, 5};
	
	
	Vec2 pos = Vec2(50, 0);
	
	int masteryID = 2;
	
	
	for(int i=0; i<3; i++) {
		int level = levelArray[i];

		MasteryItemView *itemView = MasteryItemView::create();
		
		itemView->setPosition(pos);
		
		addChild(itemView);
		
		MasteryData *mastery = GameManager::instance()->getMasteryManager()->getMasteryData(masteryID);
		
		itemView->setMastery(mastery, level);

		pos.y += itemView->getContentSize().height + 5;
	}
	
}


void MasterySceneTest::testMasteryScene(Ref *sender)
{
    
    GameManager::instance()->getUserData()->addCoin(9999);

	MasterySceneLayer *layer = MasterySceneLayer::create();
    
    mLayer = layer;
    
    auto button = Button::create();
    button->setColor(Color3B::GREEN);
    button->setTitleText("Reset");
    
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getMasteryManager()->resetAllMastery();
                GameManager::instance()->getUserData()->addCoin(-GameManager::instance()->getUserData()->getTotalCoin());
                mLayer->refreshPage();
                break;
            }
        }
    });

    button->setPosition(Vec2(30,80));
	addChild(layer);
    addChild(button);

}


void MasterySceneTest::testCallback(Ref *sender)
{
	MasteryItemView *itemView = MasteryItemView::create();
	
	itemView->setPosition(Vec2(100, 100));
	
	addChild(itemView);
	
	MasteryData *mastery = GameManager::instance()->getMasteryManager()->getMasteryData(2);
	
	itemView->setMastery(mastery);
	
	itemView->setCallback([&](MasteryItemView *view, int masteryID, int upgradeLevel) {
		log("masteryID=%d upgradeLevel=%d", masteryID, upgradeLevel);
		
		GameManager::instance()->getMasteryManager()->upgradeMastery(masteryID);
		
		view->updateUI();
	});
	
}

void MasterySceneTest::testNotEnoughDialog(Ref *sender)
{

	NotEnoughCoinDialog *dialog = NotEnoughCoinDialog::create();
	addChild(dialog);

	dialog->setCloseCallback([](bool isUpdated) {
		log("is star reward!! isUpdate=%d", isUpdated);
	});

}



#endif
