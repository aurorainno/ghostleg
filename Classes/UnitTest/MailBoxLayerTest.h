#ifdef ENABLE_TDD
//
//  MailBoxLayerTest.h
//
//
#ifndef __TDD_MailBoxLayerTest__
#define __TDD_MailBoxLayerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class MailBoxLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testItemView();
};

#endif

#endif
