#ifdef ENABLE_TDD
//
//  AdHandlerTest.h
//
//
#ifndef __TDD_AdHandlerTest__
#define __TDD_AdHandlerTest__

// Include Header

#include "TDDTest.h"

#include "AdHandler.h"

// Class Declaration 
class AdHandlerTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void useAdmob(Ref *sender);
	void useAdcolony(Ref *sender);
	
	void testRewardAd(Ref *sender);
	void testIsReady(Ref *sender);
	void testVideoAd(Ref *sender);
	void testBannerAd(Ref *sender);

private:
	CC_SYNTHESIZE_RETAIN(AdHandler *, mAdHandler, AdHandler);

};

#endif

#endif
