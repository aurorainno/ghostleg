#ifdef ENABLE_TDD
//
//  GameModelTest.m
//	TDD Framework 
//
//
#include "GameModelTest.h"
#include "TDDHelper.h"
#include "GameModel.h"
#include "VisibleRect.h"
#include "Player.h"
#include "Enemy.h"
#include "EnemyFactory.h"
#include "ViewHelper.h"
#include "ItemEffectUILayer.h"
#include <string>
#include "TimeMeter.h"

// Cocos Studio
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
using namespace cocostudio::timeline;

USING_NS_CC;

#pragma mark - Action Manager


#pragma mark - Testing Code

void GameModelTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("player.plist");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("monster.plist");
	
	mActionManager = new ActionManager();
	
	testParticle();
}


void GameModelTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	mActionManager->release();
}

#pragma mark -
#pragma mark List of Sub Tests

void GameModelTest::defineTests()
{
	ADD_TEST(testPerformance);
	ADD_TEST(addSampleModel);
	ADD_TEST(toggleFace);
	ADD_TEST(testItemEffectUI);
	ADD_TEST(testAddIndicator);
	ADD_TEST(testRemoveIndicator);
	ADD_TEST(testUpdateIndicator);
	ADD_TEST(testPlayCsbOnce);
	ADD_TEST(testStatusIcon);
	ADD_TEST(testAllEnemyStatus);
	ADD_TEST(testAddCsbAnime);
	ADD_TEST(testPlayerVisualEffect);
	ADD_TEST(testOpacity);
	ADD_TEST(testParticle);
	ADD_TEST(testPlayAction);
	ADD_TEST(testActionManager);
	ADD_TEST(testUpdate);
	ADD_TEST(testPauseResume);
	ADD_TEST(testBoundingBox);
	ADD_TEST(testLoader);
	ADD_TEST(testSpriteSheet);
	ADD_TEST(changeAnime);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameModelTest::testPerformance()
{
	int nLoop = 10;
	
	TimeMeter::instance()->reset();
	
	for(int i=0; i<nLoop; i++) {
		TimeMeter::instance()->start("gameMode");
		GameModel *model = GameModel::create();
		model->setup("monster02.csb");
		TimeMeter::instance()->stop("gameMode");
	}
	
	TimeMeter::instance()->showSummary();
}
void GameModelTest::addSampleModel()
{
	log("this is a sample subTest");
	GameModel *model = GameModel::create();
	model->setScale(0.5f);
	model->setPosition(Vec2(100, 100));
	
	model->setup("monster02.csb");
	model->setAction(GameModel::Action::Walk);
	
	addChild(model);
	
	mModel = model;
}


void GameModelTest::testBoundingBox()
{
	log("testBoundingBox");
	GameModel *model = GameModel::create();
	model->setup("monster1.csb");

	//
	log("1st Position: %s", model->info().c_str());
	
	model->setPosition(100, 50);
	
	log("2nd Position: %s", model->info().c_str());
	
	// different scale
	model->setScale(0.5f);
	model->setup("monster1.csb");
	log("after scale: %s", model->info().c_str());

}

void GameModelTest::changeAnime()
{
	GameModel::Action actions[3] = {GameModel::Action::Walk, GameModel::Action::Climb, GameModel::Action::Idle};
	
	static int index = 0;
	
	int modIndex = index % 3;
	GameModel::Action action = actions[modIndex];

	if(mModel) {
		mModel->setAction(action);
	}
	
	index++;
}


void GameModelTest::testLoader()
{
	//std::string csbName = "monster2.csb";
	std::string csbName = "player1.csb";
	
	
	
	Node *rootNode = CSLoader::createNode(csbName);
	if(rootNode == nullptr) {
		log("GameModel. cannot create the node: %s", csbName.c_str());
		return;
	}
	rootNode->setPosition(Vec2(100, 100));
	
	addChild(rootNode);

}

void GameModelTest::testSpriteSheet()
{
	//std::string csbName = "monster2.csb";
	Sprite *sprite = Sprite::createWithSpriteFrameName("p_0_die_00.png");
	sprite->setPosition(Vec2(100, 100));
	
	addChild(sprite);
}



void GameModelTest::testActionManager()
{
	//std::string csbName = "monster2.csb";
	std::string csbName = "player1.csb";
	
	GameModel *model = GameModel::create();
	model->setup(csbName);
	model->setAction(GameModel::Action::Climb);
	model->setPosition(Vec2(150, 200));
	addChild(model);
	
	model->setActionManager(mActionManager);
	
	
}



void GameModelTest::testParticle()
{
	std::string csbName = "item02.csb";
	
	GameModel *model = GameModel::create();
	model->setup(csbName);
	model->setAction(GameModel::Action::Climb);
	model->setPosition(Vec2(150, 200));
	addChild(model);
	
	mModel = model;
	mModel->setAction(GameModel::Action::Consume);

}



void GameModelTest::testUpdate()
{
	mActionManager->update(0.5f);
}

void GameModelTest::testPauseResume()
{
	if(mModel == nullptr) {
		return;
	}
	
	static bool doPause = true;
	
	if(doPause) {
		mModel->pause();
	} else {
		mModel->resume();
	}
	
	
	doPause = !doPause;
}


void GameModelTest::testPlayAction()
{
	if(mModel) {
		log("Running the Consume!");
		mModel->setAction(GameModel::Action::Consume);
	}
}


void GameModelTest::testOpacity()
{
	log("this is a sample subTest");
	GameModel *model = GameModel::create();
	model->setScale(0.5f);
	model->setPosition(Vec2(100, 100));
	
	model->setup("monster02.csb");
	
	addChild(model);
	
	mModel = model;
	
	model->setOpacity(50);
}


void GameModelTest::testPlayerVisualEffect()
{
	// Setup Model
	int nModel = 3;
	std::vector<GameModel *> modelList;
	
	Vec2 pos = Vec2(100, 500);
	for(int i=0; i<nModel; i++) {
		Player *model = Player::create();
		model->setPosition(pos);
		
		addChild(model);

		pos.y -= 80;
		modelList.push_back(model);
	}
	
	//
	modelList[0]->addParticleEffect(GameRes::Particle::Invulernable);		// Invulernable
	modelList[1]->addParticleEffect(GameRes::Particle::Shield);		// Invulernable
	modelList[2]->addParticleEffect(GameRes::Particle::ShieldBreak);
	
	
//	modelList[1]->addSpriteEffectByName("Shield.png", true, 1.6f);				// Shield
//	modelList[2]->addSpriteEffectByName("m_bounding.png", true, 1.6f);				// Shield
//	
	
	
}


void GameModelTest::testStatusIcon()
{
	// Setup Model
	int nModel = 3;
	std::vector<GameModel *> modelList;
	
	Vec2 pos = Vec2(100, 500);
	for(int i=0; i<nModel; i++) {
		Enemy *model = EnemyFactory::instance()->createEnemy(1);
		model->setPosition(pos);
		model->changeState(Enemy::State::Idle);
		
		addChild(model);
		
		pos.y -= 80;
		modelList.push_back(model);
	}
	

	modelList[0]->setStatusIconByName("status_slow.png");
//	modelList[0]->setStatusIconByName("status_timestop.png");
//	modelList[1]->addSpriteEffectByName("Shield.png", true, 1.6f);				// Shield
//	modelList[2]->addSpriteEffectByName("m_bounding.png", true, 1.6f);				// Shield
}




void GameModelTest::testAllEnemyStatus()
{
	// Setup Model
	int nModel = 13;
	std::vector<GameModel *> modelList;
	
	Vec2 pos = Vec2(100, 500);
	for(int i=0; i<nModel; i++) {
		int monsterID = i+1;
		if(monsterID >= 10 && monsterID <= 11) {	// skip
			continue;
		}
		
		Enemy *model = EnemyFactory::instance()->createEnemy(monsterID);
		model->setPosition(pos);
		model->changeState(Enemy::State::Idle);
		
		// Set the Status
		//model->setEffectStatus(EffectStatusTimestop);
		//model->setEffectStatus(EffectStatusSlow);
		model->setEffectStatus(EffectStatusShield);
		
		// Just set Status Icon (visually)
		// model->setStatusIconByName("status_slow.png");
		addChild(model);
		
		if((i % 2) == 1) {
			pos.x = 100;
			pos.y -= 80;
		} else {
			pos.x += 100;
		}
		modelList.push_back(model);
	}

}


void GameModelTest::testAddCsbAnime()
{
	Player *player = Player::create();
	player->setPosition(VisibleRect::center());
	addChild(player);
	
	//std:string csbName = "status_magnet.csb";
	std:string csbName = "status_shield.csb";
	player->addCsbAnimeByName(csbName, false, 3.0f, Vec2(0, 50));
	
	player->playCsbAnime(csbName, "active", true);
//	player->removeCsbAnimeByName(csbName);
}


void GameModelTest::testPlayCsbOnce()
{
	Enemy *model = EnemyFactory::instance()->createEnemy(3);
	model->setPosition(VisibleRect::center());
	model->changeState(Enemy::State::Idle);
	
	addChild(model);
	
	std::function<void()> callback = [] {
		log("playCsb is done!!");
	};
	
	std::string csbName = "ui_alert.csb";
	model->addCsbAnimeByName(csbName, true, 3.0f, Vec2(0, 60));
	model->playCsbAnime(csbName, "active", false, true, callback);
	
}


void GameModelTest::testAddIndicator()
{
	ItemEffectUILayer *testUI = ItemEffectUILayer::create();
	
	testUI->configAsTimer("icon_invulnerable.png", "## sec", 10.0f);
	testUI->setVisible(true);
	
//	testUI->setPosition(VisibleRect::center());
//	addChild(testUI);
//	ViewHelper::createRedSpot(this, VisibleRect::center());
	
	
	
//	ItemEffectUILayer *layer = ItemEffectUILayer::create();
//	
//	layer->configAsTimer("icon_invulnerable.png", "## sec", 10.0f);
//	layer->setVisible(true);
//
	LayerColor *colorBox = LayerColor::create(Color4B::RED, 200, 50);
	
	mModel->addCustomIndicator(testUI, Vec2(0, 100), 2.0f, false);
	
	mIndicator = testUI;
//	addChild(model);
}

void GameModelTest::testUpdateIndicator()
{
	static float value = 0.5f;

	mIndicator->setValue(value);
	
	value += 0.1f;
}

void GameModelTest::testRemoveIndicator()
{
	
	mModel->removeCustomIndicator();
	mIndicator = nullptr;
	//	addChild(model);
}

void GameModelTest::testItemEffectUI()
{
	ItemEffectUILayer *testUI;
 
	Vec2 pos = VisibleRect::center();
	pos.y = 100;
	
	testUI = ItemEffectUILayer::create();
	testUI->configAsTimer("icon_invulnerable.png", "## sec", 10.0f);
	testUI->setVisible(true);
	testUI->setPosition(pos);
	addChild(testUI);
	
	pos.y += 80;
	
	testUI = ItemEffectUILayer::create();
	testUI->configAsTimer("icon_invulnerable.png", "#", 10.0f);
	testUI->setVisible(true);
	testUI->setPosition(pos);
	addChild(testUI);

	pos.y += 80;
	
	testUI = ItemEffectUILayer::create();
	testUI->configAsTimer("star.png", "#", 10.0f);
	testUI->setVisible(true);
	testUI->setPosition(pos);
	addChild(testUI);

	
}

void GameModelTest::toggleFace()
{
	if(mModel == nullptr) {
		return;
	}
	
	bool faceLeft = mModel->isFaceLeft();
	
	mModel->setFace(! faceLeft);
}
#endif
