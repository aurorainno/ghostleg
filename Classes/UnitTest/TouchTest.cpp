#ifdef ENABLE_TDD
//
//  TouchTest.m
//	TDD Framework 
//
//
#include "TouchTest.h"
#include "TDDHelper.h"
#include "ViewHelper.h"
#include "XTLayer.h"
#include "StringHelper.h"
#include "VisibleRect.h"

#pragma mark - Extended XTLayer

class DoubleTouchLayer : public XTLayer
{
public:
	CREATE_FUNC(DoubleTouchLayer);
	
	virtual bool init() {
		if(XTLayer::init() == false) {
			return false;
		}

		// No tap 
		xtDoubleTapTime(0);
		xtLongTapTime(0);
//		
//		xtTapTime(150);
//		xtDoubleTapTime(150);
//		
		return true;
	}
	
	// Easy touch callbacks
	virtual void xtTouchesBegan(Vec2 position){
		//log("TouchBegin: pos=%s", POINT_TO_STR(position).c_str());
	}
	
	
	virtual void xtTouchesMoved(Vec2 position){
		//log("TouchMoved: pos=%s", POINT_TO_STR(position).c_str());
	}
	
	virtual void xtTouchesEnded(Vec2 position){
		//log("TouchEnd: pos=%s", POINT_TO_STR(position).c_str());
	}
	
	// Gesture Callbacks
	virtual void xtTapGesture(Vec2 position) {
		log("Tap: pos=%s", POINT_TO_STR(position).c_str());
	}
	
	virtual void xtDoubleTapGesture(Vec2 position) {
		log("Double Tap: pos=%s", POINT_TO_STR(position).c_str());
	}
	
	virtual void xtSwipeGesture(XTTouchDirection direction, float distance, float speed)
	{
		log("Swipe is called");
	}
};


///
void TouchTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void TouchTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void TouchTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(TouchTest::testOneByOne);
	SUBTEST(TouchTest::testDoubleTap);
}



void TouchTest::addOneByOneListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(TouchTest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(TouchTest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(TouchTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(TouchTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}


#pragma mark -
#pragma mark Touch Handling

bool TouchTest::onTouchBegan(Touch* touch, Event* event)
{

	Vec2 point = touch->getLocation();
	
	//touch->get
	//event->
	
	
	log("TouchBegin");
	//std::string msg = StringUtils::
	
	return true;
	
}

void TouchTest::onTouchEnded(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();
	
	log("TouchEnded");
}

void TouchTest::onTouchMoved(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();
	
	log("TouchMoved");
}

void TouchTest::onTouchCancelled(Touch* touch, Event* event)
{
	//cocos2d::log("touch cancelled");
	log("TouchCancelled");
}

#pragma mark -
#pragma mark Sub Test Definition
void TouchTest::testOneByOne(Ref *sender)
{
	addOneByOneListener();
}

void TouchTest::testDoubleTap(Ref *sender)
{
	DoubleTouchLayer *touchLayer = DoubleTouchLayer::create();
	
	addChild(touchLayer);
}

#endif
