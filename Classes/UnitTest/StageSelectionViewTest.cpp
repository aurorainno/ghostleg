#ifdef ENABLE_TDD
//
//  StageSelectionViewTest.m
//	TDD Framework
//
//
#include "StageSelectionViewTest.h"
#include "TDDHelper.h"
#include "StageSelectionView.h"
#include "ViewHelper.h"
#include "StringHelper.h"

void StageSelectionViewTest::setUp()
{
	log("TDD Setup is called");
}


void StageSelectionViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void StageSelectionViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void StageSelectionViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void StageSelectionViewTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(lastStage);
	ADD_TEST(nextStage);
	
}

#pragma mark -
#pragma mark Sub Test Definition
void StageSelectionViewTest::testSample()
{
	
	StageSelectionView *view = StageSelectionView::create();
	view->setPosition(Vec2(0, 250));
	addChild(view);
	
	
	ViewHelper::setSolidBackground(view, Color4B::YELLOW);
	
	view->setSelectedPlanet(0);
	
	mSelectionView = view;
	

	// callback setting
//	typedef std::function<void(Ref *, int plant, bool isRepeat, int currentPage, int maxPage)> SelectPlanetCallback;
//	typedef std::function<void(Vec2 scrollOffset)> ViewScrollCallback;
//	typedef std::function<void()> ClickUnlockedPlanetCallback;

	mSelectionView->setSelectPlanetCallback(
			[&](Ref *, int stageID, bool isRepeat, int currentPage, int maxPage)
			{
				log("stageID=%d isRepeat=%d currPage=%d pageBound=%d",
						stageID, isRepeat, currentPage, maxPage);
			});
	
	mSelectionView->setOnScrollCallback(
				[&](Vec2 scrollOffset) {
					log("scroll=%s", POINT_TO_STR(scrollOffset).c_str());
				}
			);
	
	mSelectionView->setOnUnlockedPlanetClickCallback(
				[&]() {
					log("Unlock Clicked");
				});
	
//	void setSelectPlanetCallback(const SelectPlanetCallback &callback);
//	void setOnScrollCallback(const ViewScrollCallback &callback);
//	void setOnUnlockedPlanetClickCallback(const ClickUnlockedPlanetCallback &callback);

}

void StageSelectionViewTest::nextStage()
{
	mSelectionView->scrollToNextPage();
}

void StageSelectionViewTest::lastStage()
{
	mSelectionView->scrollToPreviousPage();
}

#endif
