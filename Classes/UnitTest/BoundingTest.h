#ifdef ENABLE_TDD
//
//  BoundingTest.h
//
//
#ifndef __TDD_BoundingTest__
#define __TDD_BoundingTest__

// Include Header

#include "TDDTest.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include <time.h>

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class Player;
class Enemy;

// Class Declaration 
class BoundingTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void addTouchListener();
	virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
	virtual void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);

private:
	void clearAll();
	Layout *addBox(const Vec2 &point, const Size &size, const Color3B &color);

	void movePlayer(const Vec2 &pos);

	void addEnemies();
	
	void checkCollision();
	
private:
	void subTest(Ref *sender);
	

private:
	Player *mPlayer;
	Vector<Enemy *> mEnemyList;
};

#endif

#endif
