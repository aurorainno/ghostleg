#ifdef ENABLE_TDD
//
//  ItemManagerTest.h
//
//
#ifndef __TDD_ItemManagerTest__
#define __TDD_ItemManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class ItemManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testBuy();
	void testGetInfo();
	void testLoadBoosterData();
	void testPlayerBoosterJSON();
	void testPlayerBoosterValue();
	void testSaveLoad();
	void testBoosterData();
	//void test
};

#endif

#endif
