#ifdef ENABLE_TDD
//
//  GmSceneTest.h
//
//
#ifndef __TDD_GmSceneTest__
#define __TDD_GmSceneTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class GmSceneTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testScene(Ref *sender);
	void testDebugMap(Ref *sender);
}; 

#endif

#endif
