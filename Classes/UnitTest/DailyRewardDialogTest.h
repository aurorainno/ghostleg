#ifdef ENABLE_TDD
//
//  DailyRewardDialogTest.h
//
//
#ifndef __TDD_DailyRewardDialogTest__
#define __TDD_DailyRewardDialogTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class DailyRewardDialogTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testCallback();
	void testCollectStatus();
	void testSetRewardData();
	void testRewardAnimation();
};

#endif

#endif
