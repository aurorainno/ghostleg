#ifdef ENABLE_TDD
//
//  LevelDataLoaderTest.m
//	TDD Framework 
//
//
#include "LevelDataLoaderTest.h"
#include "TDDHelper.h"
#include "LevelData.h"
#include "Constant.h"
#include "StringHelper.h"
#include "Player.h"
#include "VisibleRect.h"
#include "GameWorld.h"

void LevelDataLoaderTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	
	setMenuSize(VisibleRect::getVisibleRect().size);
	alignMenu(eTDDTopCenter);
}


void LevelDataLoaderTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void LevelDataLoaderTest::defineTests()
{
	ADD_TEST(testNpcMap);
	ADD_TEST(testData);
	ADD_TEST(testGetMap);
	ADD_TEST(testChangePlanet);
	ADD_TEST(testLoadAllMap);
	ADD_TEST(testSpeedUp2);
	ADD_TEST(testGetSection);
	ADD_TEST(testMapGeneration);
	ADD_TEST(testGetMapList);
	ADD_TEST(testSpeedUp);
	ADD_TEST(testNextMapCycle);
	ADD_TEST(testNeedNewMap);
	ADD_TEST(testNextMapCycle);
	ADD_TEST(testCycleInfo);
	ADD_TEST(testLoadLevel);
	ADD_TEST(testLoad);
	ADD_TEST(testMapCache);
	ADD_TEST(testRandomLine);
	ADD_TEST(testLoadConfig);
}

#pragma mark -
#pragma mark Sub Test Definition

void LevelDataLoaderTest::testNpcMap()
{
	//MapLevelDataCache::instance()->loadMap();
	// sprintf(filename, , mapID);
	std::string filename = "mapData/npc100.dat";
	
	MapLevelData *map = MapLevelDataCache::instance()->loadMap(filename);
	
	if(map == nullptr) {
		log("No map data for map=1");
	} else {
		log("Map Data: %s\n", map->toString().c_str());
	}
}


void LevelDataLoaderTest::testData()
{
	log("this is a sample subTest");

	LevelItemData itemData;
	LevelLineData lineData;
	LevelEnemyData enemyData;
	
	lineData.parse("id=3 type=1 xLine=1 y=300");
	enemyData.parse("type=111 x=196 y=509 flipX=1 flipY=0 scaleX=1.670000 scaleY=0.727273");
	itemData.parse("type=200 x=0 y=475");

	log("lineData=%s", lineData.toString().c_str());
	log("enemyData=%s", enemyData.toString().c_str());
	log("itemData=%s", itemData.toString().c_str());
	
}


void LevelDataLoaderTest::testLoad()
{
	MapLevelData mapData;
	bool isOkay = mapData.load("map1.dat");
	
	if(isOkay == false) {
		log("Fail to load map");
		return;
	}
	
	log("MapContent:\n%s\n", mapData.toString().c_str());
}


void LevelDataLoaderTest::testGetMap()
{
	MapLevelDataCache::instance()->loadAllMap();
	
	MapLevelData *map = MapLevelDataCache::instance()->getMap(10000);
	
	if(map == nullptr) {
		log("No map data for map=1");
	} else {
		log("Map Data: %s\n", map->toString().c_str());
	}
}


void LevelDataLoaderTest::testMapCache()
{
	MapLevelDataCache::instance()->loadAllMap();
	
	MapLevelData *map = MapLevelDataCache::instance()->getMap(10001);
	
	if(map == nullptr) {
		log("No map data for map=1");
	} else {
		log("Map Data: %s\n", map->toString().c_str());
	}
}

void LevelDataLoaderTest::testRandomLine()
{
	MapLevelData *map = MapLevelDataCache::instance()->getMap(0);
	
	if(map == nullptr) {
		log("No map data for map=1");
		return;
	}
	
	// getRandomLineData(Vector<LevelLineData *> &outLineArray);
	Vector<LevelLineData *> randomLines;
	
	
	map->getRandomLineData(randomLines);
	
	for(int i=0; i<randomLines.size(); i++) {
		LevelLineData *line = randomLines.at(i);
		
		log("%s", line->toString().c_str());
	}
	
}

void LevelDataLoaderTest::testLoadConfig()
{
	MapLevelDataCache::instance()->loadConfig();
	
	log("Result:\n%s\n", MapLevelDataCache::instance()->infoConfig().c_str());
	log("maxLevel=%d\n", MapLevelDataCache::instance()->getMaxMapLevel());
	
	
}


void LevelDataLoaderTest::testLoadLevel()
{
	MapLevelManager::instance()->loadConfig();
	
	log("Section:\n%s\n", MapLevelManager::instance()->infoSectionData().c_str());
	log("Cycle:\n%s\n", MapLevelManager::instance()->infoCycleData().c_str());
}

void LevelDataLoaderTest::testChangePlanet()
{
	for(int planet = 1; planet<=5; planet++) {
		log("Change planet to %d", planet);
		
		MapLevelManager::instance()->setMapCycleForPlanet(planet);
		log("Cycle:\n%s\n", MapLevelManager::instance()->infoCycleData().c_str());
	}
	
	
}



void LevelDataLoaderTest::testGetSection()
{
	MapLevelManager::instance()->loadConfig();
	
	MapSection *section = MapLevelManager::instance()->getSectionByID(1);
	log("Section:\n%s\n", section->toString().c_str());
	
	for(int i=0; i<5; i++) {
		int map = section->getRandomMap();
		log("%d: map=%d", i, map);
	}
	
}

void LevelDataLoaderTest::testCycleInfo()
{
	MapLevelManager::instance()->loadConfig();

	
	MapCycleInfo info;
	info.setCycle(0);
	info.setStartDistance(0);
	info.setEndDistance(kMapHeight * 20);
	
	int distance = 100;
	for(int i=0; i<30; i++) {
		int map = info.getMapID(distance);
		
		log("distance: %d map: %d", distance, map);
		
		distance += kMapHeight;
	}
	
	
//	
//	MapSection *section = MapLevelManager::instance()->getSectionByID(1);
//	log("Section:\n%s\n", section->toString().c_str());
//	
//	for(int i=0; i<5; i++) {
//		int map = section->getRandomMap();
//		log("%d: map=%d", i, map);
//	}
	
}


void LevelDataLoaderTest::testNeedNewMap()
{
	MapLevelManager::instance()->loadConfig();

	MapLevelManager::instance()->setup(100);
	
	int distance = 100;
	
	for(int i=0; i<30; i++) {
		bool flag = MapLevelManager::instance()->needNewMap(distance);
		int cycle = MapLevelManager::instance()->getCurrentCycle();
		
		log("distance=%d newMap=%d cycle=%d", distance, flag, cycle);
		
		if(flag) {
			int lastDistance = MapLevelManager::instance()->getGeneratedDistance();
			MapLevelManager::instance()->setGeneratedDistance(lastDistance + kMapHeight);
		}
		
		distance += kMapHeight / 2;
	}
}


void LevelDataLoaderTest::testNextMapCycle()
{
	MapLevelManager::instance()->loadConfig();
	
	MapLevelManager::instance()->setup(100);
	
	for(int i=0; i<30; i++) {
		log("%s", MapLevelManager::instance()->getCurrentCycleInfo()->toString().c_str());
		
		MapLevelManager::instance()->setNextMapCycle();
	}
	
	
}



void LevelDataLoaderTest::testMapGeneration()
{
	MapLevelManager::instance()->loadConfig();
	
	MapLevelManager::instance()->setup(100);
	
	int distance = 100;
	
	for(int i=0; i<200; i++) {
		bool flag = MapLevelManager::instance()->needNewMap(distance);
		
		if(flag) {
			int startDistance = MapLevelManager::instance()->getGeneratedDistance();
			
			MapLevelManager::instance()->updateMapCycle(startDistance);		// will cause next cycle if need
			
			MapCycleInfo *currentCycle = MapLevelManager::instance()->getCurrentCycleInfo();
			int mapID = currentCycle->getMapID(startDistance);
			
			log("distance: %d cycle=%d mapID=%d", startDistance, currentCycle->getCycle(), mapID);
			
			// Assume Map is generated!!
			
			
			MapLevelManager::instance()->setGeneratedDistance(startDistance + kMapHeight);
		}
		
		distance += kMapHeight / 2;
	}
}

void LevelDataLoaderTest::testGetMapList()
{
	MapLevelManager::instance()->loadConfig();
	
	std::vector<int> mapList;
	
	MapLevelManager::instance()->getMapList(mapList);
	
	log("map: %s", VECTOR_TO_STR(mapList).c_str());
}


void LevelDataLoaderTest::testLoadAllMap()
{
	MapLevelManager::instance()->loadConfig();
	MapLevelDataCache::instance()->loadAllMap();
	
	MapLevelData *map = MapLevelDataCache::instance()->getNpcMap(100);
	log("map: %s", map->toString().c_str());
}


void LevelDataLoaderTest::testSpeedUp2()
{
	MapLevelManager::instance()->setMapCycleForPlanet(5);
	MapLevelManager::instance()->setup(100);
	
	int distance = 100;

	Player *player = GameWorld::instance()->getPlayer();
	//player->reset();
	player->setPositionY(distance);
	
	log("Player Speed: %d distance=%f", player->getSpeed(), player->getPositionY());
	
	
	for(int i=0; i<50; i++) {
		bool flag = MapLevelManager::instance()->needNewMap(distance);
	
		log("distance=%d needNewMap=%d", distance, flag);
		
		if(flag) {
			int startDistance = MapLevelManager::instance()->getGeneratedDistance();
			MapLevelManager::instance()->updateMapCycle(startDistance);		// will cause next cycle if need
			MapLevelManager::instance()->setGeneratedDistance(startDistance + kMapHeight);
		}
		
		log("Player Speed: %d distance=%f", player->getSpeed(), player->getPositionY());
		
		distance += 100;
		player->setPositionY(distance);
	}
	
//
//	bool needSpeedUp(
//	
//	distance += 30;
//	
//	
//
//	if(MapLevelManager::instance()->needSpeedUp(player->getPositionY())) {
//		MapLevelManager::instance()->updatePlayerSpeed(player);
//	}

}



void LevelDataLoaderTest::testSpeedUp()
{
	MapLevelManager::instance()->setup(100);
	
	int distance = 100;
	
	Player *player = Player::create();
	
	for(int i=0; i<200; i++) {
		bool flag = MapLevelManager::instance()->needNewMap(distance);
		
		if(flag) {
			int startDistance = MapLevelManager::instance()->getGeneratedDistance();
			MapLevelManager::instance()->updateMapCycle(startDistance);		// will cause next cycle if need
			MapLevelManager::instance()->setGeneratedDistance(startDistance + kMapHeight);
		}
		
		
		
		
		
		
		float y = player->getPositionY();
		
		// Check the position and speed
		log("y=%f vx=%f vy=%f", y, player->getSpeedX(), player->getSpeedY());

		
		MapCycleInfo *currentCycle = MapLevelManager::instance()->getCurrentCycleInfo();
		log("distance: %d cycle=%s", distance, currentCycle->toString().c_str());
		
		if(MapLevelManager::instance()->needSpeedUp(player->getPositionY())) {
			MapLevelManager::instance()->updatePlayerSpeed(player);
		}
		
		// Moving the player and map
		float moveY = kMapHeight / 2;
		player->setPositionY(y + moveY);
		
		distance += moveY;
		
		
		
	}

	
	
}

#endif

