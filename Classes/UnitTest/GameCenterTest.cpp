#ifdef ENABLE_TDD
//
//  GameCenterTestTest.m
//	TDD Framework 
//
//
#include "GameCenterTest.h"
#include "TDDHelper.h"
#include "GameManager.h"
#include "GameCenterManager.h"
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "GameCenterLayer.h"

void GameCenterTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void GameCenterTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void GameCenterTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(GameCenterTest::subTest);
	SUBTEST(GameCenterTest::testSignin);
	SUBTEST(GameCenterTest::testSubmitDistance);
	SUBTEST(GameCenterTest::testBestDistance);
    SUBTEST(GameCenterTest::testGameCenterLayer);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameCenterTest::subTest(Ref *sender)
{
    GameCenterManager *gc = GameManager::instance()->getGameCenterManager();
	
    log("GameCenterTest:isConnected : %d", sdkbox::PluginSdkboxPlay::isConnected());
	
	gc->sendBestDistance(10);
	gc->showLeaderboard("Best Distance");
}

void GameCenterTest::testGameCenterLayer(cocos2d::Ref *sender){
	GameCenterLayer* layer = GameCenterLayer::create();
	addChild(layer);
}

void GameCenterTest::testSubmitDistance(Ref *sender)
{
	GameManager::instance()->getGameCenterManager()->sendBestDistance(77);
}

void GameCenterTest::testBestDistance(Ref *sender)
{
	GameManager::instance()->getGameCenterManager()->showBestDistanceLeaderBoard();
}

void GameCenterTest::testSignin(Ref *sender)
{
	sdkbox::PluginSdkboxPlay::signin();
}

#endif
