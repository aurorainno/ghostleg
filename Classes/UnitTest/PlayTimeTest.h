#ifdef ENABLE_TDD
//
//  PlayTimeTest.h
//
//
#ifndef __TDD_PlayTimeTest__
#define __TDD_PlayTimeTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class PlayTimeTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void updatePlayTimeRecord();
    void setLastPlayTime();
};

#endif

#endif
