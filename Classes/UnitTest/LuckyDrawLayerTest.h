#ifdef ENABLE_TDD
//
//  LuckyDrawLayerTest.h
//
//
#ifndef __TDD_LuckyDrawLayerTest__
#define __TDD_LuckyDrawLayerTest__

// Include Header

#include "TDDBaseTest.h"

class LuckyDrawAnime;

// Class Declaration
class LuckyDrawLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
    void testSample();
	void testCreateAnime();
	void testOpen();
	void testSound();

private:
	LuckyDrawAnime *mAnime;
};

#endif

#endif
