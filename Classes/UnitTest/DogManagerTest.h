#ifdef ENABLE_TDD
//
//  DogManagerTest.h
//
//
#ifndef __TDD_DogManagerTest__
#define __TDD_DogManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class DogManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	void showSelectedDogInfo();
	
	
private:
	void testInfoDogData();
	void testRarityAndRank();
	void testMock();
	void testChangeDog();
	void testSortLockedDog();
	void testGetUnlockMessage();
    void testEndGameUnlockDog();
	void testGetUnlockProgress();
	void testShouldShowSuggestion();
	void testGetDogList();
	void testResetSuggestion();
	void testDogInfoText();
	void testParseUnlock();
	void testGetNextUnlock();
	void testGetUnlockProgressMessage();
	void testParseAbility();
}; 

#endif

#endif
