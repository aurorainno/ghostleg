#ifdef ENABLE_TDD
//
//  CharGameDataTest.m
//	TDD Framework
//
//
#include "CharGameDataTest.h"
#include "TDDHelper.h"

void CharGameDataTest::setUp()
{
	log("TDD Setup is called");
}


void CharGameDataTest::tearDown()
{
	log("TDD tearDown is called");
}

void CharGameDataTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void CharGameDataTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void CharGameDataTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void CharGameDataTest::testSample()
{
	log("this is a sample subTest");
}


#endif
