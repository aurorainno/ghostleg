#ifdef ENABLE_TDD
//
//  ShareImageTest.h
//
//
#ifndef __TDD_ShareImageTest__
#define __TDD_ShareImageTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class ShareImageTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testImage(Ref *sender);
	void testSaveImage(Ref *sender);
	void testWriteToFile(Ref *sender);
	
    Sprite *mCharacter;
}; 

#endif

#endif
