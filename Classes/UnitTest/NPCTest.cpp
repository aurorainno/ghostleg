#ifdef ENABLE_TDD
//
//  NPCTest.m
//	TDD Framework
//
//
#include "NPCTest.h"
#include "TDDHelper.h"
#include "NPC.h"
#include "VisibleRect.h"

void NPCTest::setUp()
{
	log("TDD Setup is called");
	
	NPC *npc = NPC::create();
	npc->setNpc(1);
	npc->setPosition(Vec2(100, 100));
	addChild(npc);
	npc->update(0);
	mNpc = npc;
	
	mNpcEmoji = NPC::EmojiWaiting;
}


void NPCTest::tearDown()
{
	log("TDD tearDown is called");
}

void NPCTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NPCTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NPCTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testDifferentFace);
	ADD_TEST(testNPCList);
	ADD_TEST(testEmoji);
	ADD_TEST(changeEmoji);
}

#pragma mark -
#pragma mark Sub Test Definition
void NPCTest::testSample()
{
	log("this is a sample subTest");
//	Enemy *enemy = Enemy::create();
//	enemy->setObjectID(nextObjectID());
	NPC *npc = NPC::create();
	npc->setNpc(1);
	npc->setPosition(VisibleRect::center());
	addChild(npc);
	npc->update(0);
}

void NPCTest::testNPCList()
{
	
	int startID = 1;
	int endID = 7;
	
	Vec2 pos = Vec2(150, 50);
	
	for(int npcID=startID; npcID <= endID; npcID++) {
		NPC *npc = NPC::create();
		npc->setNpc(npcID);
		npc->setPosition(pos);
		addChild(npc);
		npc->update(0);
		
		pos += Vec2(0, 80);
	}
	//	Enemy *enemy = Enemy::create();
	//	enemy->setObjectID(nextObjectID());
	
}

void NPCTest::testDifferentFace()
{
	log("this is a sample subTest");
	//	Enemy *enemy = Enemy::create();
	//	enemy->setObjectID(nextObjectID());
	NPC *npc;
 
	npc = NPC::create();
	npc->setNpc(2);
	npc->setPosition(Vec2(100, 100));
	npc->setFace(true);
	addChild(npc);
	npc->update(0);
	
	npc = NPC::create();
	npc->setNpc(2);
	npc->setPosition(Vec2(100, 150));
	npc->setFace(false);
	addChild(npc);
	npc->update(0);
}

void NPCTest::testEmoji()
{
	mNpc->setEmojiVisible(true);
	mNpc->setEmoji(mNpcEmoji);
}

void NPCTest::changeEmoji()
{
	mNpcEmoji = (NPC::Emoji)(mNpcEmoji+1);
	if(mNpcEmoji > NPC::Emoji5) {
		mNpcEmoji = NPC::EmojiWaiting;
	}
	
	testEmoji();
}

#endif
