#ifdef ENABLE_TDD
//
//  FireworkTest.m
//	TDD Framework
//
//
#include "FireworkTest.h"
#include "TDDHelper.h"
#include "FireworkLayer.h"
#include "VisibleRect.h"

void FireworkTest::setUp()
{
	log("TDD Setup is called");
	
	FireworkLayer *firework = FireworkLayer::create();
	
	addChild(firework);
	
	mFirework = firework;
	
	setBackgroundColor(Color3B::BLACK);
	
	// Move the firework to top
	mFirework->setAnchorPoint(Vec2(0, 1));
	mFirework->setPosition(VisibleRect::leftTop());
}


void FireworkTest::tearDown()
{
	log("TDD tearDown is called");
}

void FireworkTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void FireworkTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void FireworkTest::defineTests()
{
	ADD_TEST(activateNextEmitter);
	ADD_TEST(changeInterval);
	ADD_TEST(startFirework);
	ADD_TEST(stopFirework);
}

#pragma mark -
#pragma mark Sub Test Definition
void FireworkTest::activateNextEmitter()
{
	static int i = 0;
	
	mFirework->activateEmitter(i);
	
	i = (i + 1) % mFirework->getEmitterCount();
}

void FireworkTest::startFirework()
{
	mFirework->startFirework();
}

void FireworkTest::changeInterval()
{
	float current = mFirework->getInterval();
	float newValue = current + 0.2f;
	
	log("Firework interval changed to %f", newValue);
	mFirework->setInterval(newValue);
	
	mFirework->stopFirework();
	mFirework->startFirework();
}


void FireworkTest::stopFirework()
{
	mFirework->stopFirework();
}

#endif
