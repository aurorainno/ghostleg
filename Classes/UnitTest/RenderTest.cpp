#ifdef ENABLE_TDD
//
//  RenderTest.m
//	TDD Framework
//
//
#include "RenderTest.h"
#include "TDDHelper.h"
#include "VisibleRect.h"
#include "Star.h"
#include "GameWorld.h"
#include "Enemy.h"
#include "EnemyFactory.h"
#include "Player.h"

void RenderTest::setUp()
{
	log("TDD Setup is called");
	GameWorld::instance()->setGameUILayer(nullptr);
}


void RenderTest::tearDown()
{
	log("TDD tearDown is called");
}

void RenderTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void RenderTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void RenderTest::defineTests()
{
	ADD_TEST(testAddStar);
	ADD_TEST(testAddEnemy);
	ADD_TEST(testRandomEnemy);
	ADD_TEST(testAddPlayer);
	ADD_TEST(testConsumeStar);
}


Vec2 RenderTest::getRandomPosition()
{
	float x = RandomHelper::random_real(0.0f, VisibleRect::right().x);
	float y = RandomHelper::random_real(0.0f, VisibleRect::top().y);
	
	return Vec2(x, y);
}


void RenderTest::addEnemy(int enemyID, const Vec2 &pos)
{
	
	Enemy *enemy = EnemyFactory::instance()->createEnemy(enemyID);
	if(enemy) {
		enemy->setPosition(pos);
		enemy->updateChildrenZOrder();
		addChild(enemy);
		enemy->update(0);
		enemy->changeToActive();
	}
}
#pragma mark -
#pragma mark Sub Test Definition
void RenderTest::testAddStar()
{
    Star *star = Star::create(Star::SmallStar);
	star->setPosition(getRandomPosition());
	
	addChild(star);
}


void RenderTest::testConsumeStar()
{
	Vector<Node *> children = getChildren();
	
	for(Node *node : children) {
		Star *star = dynamic_cast<Star *>(node);
		if(star == nullptr) {
			continue;
		}
		star->use();
	}
}


void RenderTest::testAddEnemy()
{
	addEnemy(1, getRandomPosition());
}

void RenderTest::testRandomEnemy()
{
	int enemyID = RandomHelper::random_int(1, 9);
	addEnemy(enemyID, getRandomPosition());
}

void RenderTest::testAddPlayer()
{
	Player *player = Player::create();
	player->setPosition(getRandomPosition());
	addChild(player);
}

#endif
