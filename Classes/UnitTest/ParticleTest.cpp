#ifdef ENABLE_TDD
//
//  ParticleTest.m
//	TDD Framework 
//
//
#include "ParticleTest.h"
#include "TDDHelper.h"
#include "CommonMacro.h"
#include "VisibleRect.h"
#include "StageParallaxLayer.h"
#include "StringHelper.h"

void ParticleTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	// Setup the space background
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();
	
	setupTouchListener();
}


void ParticleTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

// http://developalia.com/cocostudio-code-connection-frame-event/

void ParticleTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testSpeedyParticle);
	ADD_TEST(testFireParticle);
	ADD_TEST(testStatus);
	ADD_TEST(setOneTimeParticle);
	ADD_TEST(playParticle);
	ADD_TEST(stopParticle);
	ADD_TEST(playAnimation);
}

#pragma mark -
#pragma mark Sub Test Definition
void ParticleTest::subTest()
{
	log("this is a sample subTest");
	Node *node = CSLoader::createNode("testing_layer.csb");
	
	addChild(node);
	
	const Vector<Node *> &children = node->getChildren();
	
	for(int i=0; i<children.size(); i++) {
		Node *node = children.at(i);
		
		log("Node: %s", node->getName().c_str());
	}
	
	//node->get
//	Node *mainLayer = node->getChildByName("Layer");
//	
	mParticle = (ParticleSystemQuad *) node->getChildByName("testParticle");
//	//ParticleSystemQuad *particle
	ActionTimeline *timeline = CSLoader::createTimeline("testing_layer.csb");
	setTimeline(timeline);
	
	if(timeline) {
		runAction(timeline);
		timeline->setFrameEventCallFunc(CC_CALLBACK_1(ParticleTest::onFrameEvent, this));
	}
}

void ParticleTest::onFrameEvent(Frame *frame)
{
	
	// log("onFrameEvent: is called");
	EventFrame* evnt = dynamic_cast<EventFrame*>(frame);
	if(!evnt) {
		return;
	}
	
	std::string str = evnt->getEvent();
	if (str == "startParticle")
	{
		//log("startParticle event is trigger");
		Node *node = evnt->getNode();
		// log("nodeName=%s", node->getName().c_str());
		ParticleSystemQuad * particle = (ParticleSystemQuad *)node;
		particle->resetSystem();
	}
}

void ParticleTest::playParticle()
{
	if(mParticle) {
		mParticle->resetSystem();
	} else {
		log("Particle is null");
	}
	
}

void ParticleTest::stopParticle()
{
	if(mParticle) {
		mParticle->stopSystem();
	} else {
		log("Particle is null");
	}
	
}

void ParticleTest::playAnimation()
{
	mTimeline->play("showStar", false);
}


void ParticleTest::setOneTimeParticle()
{
	std::string filename = "particle/particle_firework1.plist";
	ParticleSystemQuad *particle = ParticleSystemQuad::create(filename);
	particle->setPosition(VisibleRect::center());
	particle->setScale(0.4f);
	addChild(particle);
	
	mParticle = particle;
}
void ParticleTest::testStatus()
{
	log("Particle: isActive=%d isPaused=%d", mParticle->isActive(), mParticle->isPaused());
}

void ParticleTest::testFireParticle()
{
	Node *particle = ParticleFire::createWithTotalParticles(30);
	particle->setPosition(VisibleRect::center());
	particle->setScale(0.5f);
	addChild(particle);
}

void ParticleTest::testSpeedyParticle()
{
	std::string filename = "particle/particle_speedy.plist";
	ParticleSystemQuad *particle = ParticleSystemQuad::create(filename);
	particle->setPosition(VisibleRect::center());
	particle->setScale(0.4f);
	addChild(particle);
	
	mParticle = particle;
}


void ParticleTest::moveParticle(const Vec2 &diff)
{
	if(mParticle == nullptr) {
		return;
	}
	Vec2 position = mParticle->getPosition();
	position += diff;
	
	mParticle->setPosition(position);
}


void ParticleTest::setupTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(ParticleTest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(ParticleTest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(ParticleTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(ParticleTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}

bool ParticleTest::onTouchBegan(Touch *touch, Event *event)
{
	mLastPos = touch->getLocation();
	
	return true;
}

void ParticleTest::onTouchEnded(Touch *touch, Event *event)
{
}

void ParticleTest::onTouchMoved(Touch *touch, Event *event)
{
	Vec2 newPos = touch->getLocation();
	Vec2 diff = newPos - mLastPos;
	mLastPos = newPos;
	
	log("MovementDiff: %s", POINT_TO_STR(diff).c_str());
	moveParticle(diff);
}

void ParticleTest::onTouchCancelled(Touch *touch, Event *event)
{
	
}




#endif
