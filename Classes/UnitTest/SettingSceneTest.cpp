#ifdef ENABLE_TDD
//
//  SettingSceneTest.m
//	TDD Framework 
//
//
#include "SettingSceneTest.h"
#include "TDDHelper.h"
#include "SettingScene.h"

void SettingSceneTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void SettingSceneTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void SettingSceneTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(SettingSceneTest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void SettingSceneTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	
	auto settingLayer = SettingSceneLayer::create();
    addChild(settingLayer);
}


#endif
