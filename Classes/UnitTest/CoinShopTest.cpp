#ifdef ENABLE_TDD
//
//  CoinShopTest.m
//	TDD Framework
//
//
#include "CoinShopTest.h"
#include "TDDHelper.h"
#include "IAPManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "IAPStatusCode.h"
#include "VisibleRect.h"
#include "CoinShopItemView.h"
#include "CoinShopScene.h"


void CoinShopTest::setUp()
{
	log("TDD Setup is called");
	mShopLayer = nullptr;
}


void CoinShopTest::tearDown()
{
	log("TDD tearDown is called");
}

void CoinShopTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void CoinShopTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void CoinShopTest::defineTests()
{
	ADD_TEST(testCoinShopScene);
	ADD_TEST(testAddCandySuccess);
	ADD_TEST(testAddStarSuccess);
	ADD_TEST(testShowAlert);
}

#pragma mark -
#pragma mark Sub Test Definition
void CoinShopTest::testAddCandySuccess()
{
	// Add some star
	GameManager::instance()->getUserData()->addCandy(500);
	
	//
	std::string prodName = "candypack1";
	GameProduct *product = GameManager::instance()->getIAPManager()->getGameProductByName(prodName);
	mShopLayer->handleGameProductSuccess(product);
}

void CoinShopTest::testAddStarSuccess()
{
	// Add some star
	GameManager::instance()->getUserData()->addCoin(1000);
	
	//
	std::string prodName = "starpack1";
	GameProduct *product = GameManager::instance()->getIAPManager()->getGameProductByName(prodName);
	mShopLayer->handleGameProductSuccess(product);
}


void CoinShopTest::testCoinShopScene()
{
	CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
	layer->setPosition(VisibleRect::leftBottom());
	
	addChild(layer);
	
	mShopLayer = layer;
}


void CoinShopTest::testShowAlert()
{
	CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
	layer->setPosition(VisibleRect::leftBottom());
	
	addChild(layer);
	
	mShopLayer = layer;
	
	
	//mShopLayer->showAlert("Test Title", "10000", CoinShopSceneLayer::ShopItemTypeStar);
	mShopLayer->showAlert("Test Candy", "10000", CoinShopSceneLayer::ShopItemTypeCandy);
	
}

#endif
