#ifdef ENABLE_TDD
//
//  AstrodogClientTest.h
//
//
#ifndef __TDD_AstrodogClientTest__
#define __TDD_AstrodogClientTest__

// Include Header

#include "TDDBaseTest.h"
#include "AstrodogClient.h"

// Class Declaration
class AstrodogClientTest : public TDDBaseTest, AstrodogClientListener
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	virtual void onGetRanking(int status, AstrodogLeaderboardType type, int planet,
							  const std::map<std::string, int> &metaData);
    virtual void onSubmitScore(int status);
    virtual void onFBConnect(int status);
    
private:
	void testUrlEncode();
	void testRemainTime();
	void testUserInfo();
	void testGetRankPosition();
	void testInfoLeaderboard();
	void testLoadAllLeaderboard();
	void testLBSaveLoad();
	void testGetRanking();
	void testSubmitScore();
	void testLeaderboard();
    void testAstrodogRecord();
	void testSubmitScoreNewUser();
	void testSubmitFBOldUser();
	void testSubmitFBNewUser();
    void testConnectFB();
    void testURLHelper();
	void testResetUser();
};

#endif

#endif
