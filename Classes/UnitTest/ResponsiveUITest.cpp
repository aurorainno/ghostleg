#ifdef ENABLE_TDD
//
//  ResponsiveUITest.m
//	TDD Framework 
//
//
#include "ResponsiveUITest.h"
#include "TDDHelper.h"

// For cocos studio
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "DebugInfo.h"
#include "StringHelper.h"

using namespace cocos2d::ui;

void ResponsiveUITest::setUp()
{
	log("TDD Setup is called");
	log("Screen Information:\n%s\n", DebugInfo::infoScreen().c_str());
	
	
}


void ResponsiveUITest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ResponsiveUITest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(ResponsiveUITest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void ResponsiveUITest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	
	Node *rootNode = CSLoader::createNode("UiTestScene.csb");
	
	rootNode->setContentSize(this->getContentSize());
	cocos2d::ui::Helper::doLayout(rootNode);
	
	addChild(rootNode);
	
	Node *topPanel = rootNode->getChildByName("topPanel");
	Node *centerPanel = rootNode->getChildByName("centerPanel");
	
	
	std::string sizeRoot = SIZE_TO_STR(rootNode->getContentSize());
	std::string sizeTop = SIZE_TO_STR(topPanel->getContentSize());
	std::string sizeCenter = SIZE_TO_STR(centerPanel->getContentSize());
	
	
	std::string debugInfo = "rootSize: " + sizeRoot + "\n";
	debugInfo += "sizeTop: " + sizeTop + "\n";
	debugInfo += "sizeCenter: " + sizeCenter + "\n";
	
	log("DEBUG:\n%s\n", debugInfo.c_str());
}


#endif
