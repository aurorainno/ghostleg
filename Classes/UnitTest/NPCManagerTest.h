#ifdef ENABLE_TDD
//
//  NPCManagerTest.h
//
//
#ifndef __TDD_NPCManagerTest__
#define __TDD_NPCManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class NPCManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testAddNpcRating();
	void testNpcRatingWithSetting();
	void testGetRatingList();
};

#endif

#endif
