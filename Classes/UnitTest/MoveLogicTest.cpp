#ifdef ENABLE_TDD
//
//  MoveLogicTest.m
//	TDD Framework
//
//
#include "MoveLogicTest.h"
#include "TDDHelper.h"
#include "RouteMoveLogic.h"
#include "StringHelper.h"
#include "MapLine.h"
#include "GameMap.h"

void MoveLogicTest::setUp()
{
	log("TDD Setup is called");
	mLogic = nullptr;
	
	// Testing Map
	mGameMap = GameMap::create();
	mGameMap->retain();
	
	setupGameMap();
	
	
	// Testing Sprite
	mTestSprite = Sprite::create("star.png");
	mTestSprite->setScale(0.3f);
	addChild(mTestSprite);
}


void MoveLogicTest::tearDown()
{
	log("TDD tearDown is called");
	
	CC_SAFE_RELEASE(mGameMap);
}

void MoveLogicTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void MoveLogicTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


void MoveLogicTest::setMoveLogic(RouteMoveLogic *logic)
{
	
	if(logic != nullptr) {
		logic->retain();
	}
	
	//
	if(mLogic != nullptr) {
		mLogic->release();
	}

	mLogic = logic;
	
}

void MoveLogicTest::setupGameMap()
{
	//mGameMap->s
	mLineX[0]= mGameMap->getVertLineX(0);
	mLineX[1] = mGameMap->getVertLineX(1);
	mLineX[2] = mGameMap->getVertLineX(2);
	
	
	mGameMap->addSlopeLine(Vec2(mLineX[0], 250), Vec2(mLineX[1], 200));
	mGameMap->addSlopeLine(Vec2(mLineX[0], 320), Vec2(mLineX[1], 320));
	
	addChild(mGameMap);
	log("Map Detail:\n%s\n", mGameMap->infoHoriLines().c_str());
}

#pragma mark -
#pragma mark List of Sub Tests

void MoveLogicTest::defineTests()
{
	ADD_TEST(testWithVisual);
	ADD_TEST(testUpdateStateData);
	ADD_TEST(testMoveSlopeLine);
	ADD_TEST(testCalculateHorizontal);
	ADD_TEST(testCalculateVertical);
	ADD_TEST(testMoveVertical);
	ADD_TEST(testMoveOnSlope);
	ADD_TEST(testMoveHoriLine);
	ADD_TEST(step);
}

#pragma mark -
#pragma mark Sub Test Definition
void MoveLogicTest::testMoveVertical()
{
	log("this is a sample subTest");
	
	RouteMoveLogic *logic = RouteMoveLogic::create();

	logic->setSpeed(Vec2(100, 120));
	logic->setPosition(Vec2(25, 100));
	logic->setDir(DirUp);
	logic->setWalkingRoute(nullptr);
	
	Vec2 newPos = logic->getNewPosition(0.5);
	// Set the information
	
	//logic->setSpeed(Vec2(100))
	log("DEBUG: newPos=%s", POINT_TO_STR(newPos).c_str());
	
}


void MoveLogicTest::testMoveOnSlope()
{
	log("this is a sample subTest");
	
	MapLine *line = MapLine::create(MapLine::eTypeSlope, 25, 125, 100, 200);
	
	RouteMoveLogic *logic = RouteMoveLogic::create();
	
	logic->setSpeed(Vec2(100, 120));
	logic->setPosition(Vec2(25, 125));
	logic->setDir(DirRight);
	logic->setWalkingRoute(line);
	
	Vec2 newPos = logic->getNewPosition(0.5);
	// Set the information
	
	//logic->setSpeed(Vec2(100))
	log("DEBUG: newPos=%s", POINT_TO_STR(newPos).c_str());
	
}


void MoveLogicTest::testMoveHoriLine()
{
	log("this is a sample subTest");
	
	MapLine *line = MapLine::create(MapLine::eTypeSlope, 25, 125, 100, 125);
	
	RouteMoveLogic *logic = RouteMoveLogic::create();
	
	logic->setSpeed(Vec2(100, 120));
	logic->setPosition(Vec2(25, 125));
	logic->setDir(DirRight);
	logic->setWalkingRoute(line);
	
	for(float delta=0.0f; delta<=1.0f; delta += 0.1f) {
		
		Vec2 newPos = logic->getNewPosition(delta);
		log("DEBUG:delta=%f newPos=%s", delta, POINT_TO_STR(newPos).c_str());
	}
}

void MoveLogicTest::testMoveSlopeLine()
{
	log("this is a sample subTest");
	
	MapLine *line = MapLine::create(MapLine::eTypeSlope, 25, 150, 100, 125);
	
	RouteMoveLogic *logic = RouteMoveLogic::create();
	
	logic->setSpeed(Vec2(100, 120));
	logic->setPosition(Vec2(25, 150));
	logic->setDir(DirRight);
	logic->setWalkingRoute(line);
	
	for(float delta=0.0f; delta<=1.0f; delta += 0.1f) {
		
		//Vec2 newPos = logic->getNewPosition(delta);
		//log("DEBUG:delta=%f newPos=%s", delta, POINT_TO_STR(newPos).c_str());
		
		Vec2 oldPos = logic->getPosition();
		Vec2 newPos = logic->getNewPosition(0.2f);
		logic->setPosition(newPos);
		
		log("DEBUG:oldPos=%s newPos=%s", POINT_TO_STR(oldPos).c_str(), POINT_TO_STR(newPos).c_str());
	}
}


void MoveLogicTest::testCalculateVertical()
{
	// Setup the Movement Logic
	RouteMoveLogic *logic = RouteMoveLogic::create();
	logic->setWalkingRoute(nullptr);
	logic->setGameMap(mGameMap);
	
	// Define the inital point & other setting
	Vec2 startPos = Vec2(mLineX[0], 210);
	Dir startDir = DirUp;
	
	Vec2 speed = Vec2(100, 100);
	
	
	//
	logic->setSpeed(speed);
	
	// Running Variable
	Vec2 pos = startPos;
	Dir dir = startDir;
	
	
	float time = 0;
	float deltaStep = 0.1f;
	int nStep = 30;
	
	log("%3.3f: newPos=%s moveResult=%s", time,
						POINT_TO_STR(pos).c_str(),
						logic->infoResult().c_str());
	
	for(int i=0; i<nStep; i++) {
	
		logic->setPosition(pos);
		logic->setDir(dir);
	
		time += deltaStep;
		
		Vec2 newPos = logic->calculateNewPosition(deltaStep);
	
		log("%3.3f: oldPos=%s newPos=%s moveResult=%s",
						time,
						POINT_TO_STR(pos).c_str(),
						POINT_TO_STR(newPos).c_str(),
						logic->infoResult().c_str());
		
		// Setting the latest
		pos = newPos;
		if(logic->getNextDir() != DirNone) {
			dir = logic->getNextDir();
			logic->setWalkingRoute(logic->getCrossingRoute());
		}
		
		
		
		//
		
		
	}
}

void MoveLogicTest::testCalculateHorizontal()
{
	// Setup the Movement Logic
	RouteMoveLogic *logic = RouteMoveLogic::create();
	logic->setWalkingRoute(nullptr);
	logic->setGameMap(mGameMap);
	
	// Define the inital point & other setting
	Vec2 startPos = Vec2(mLineX[0], 240);
	Dir startDir = DirUp;
	
	Vec2 speed = Vec2(100, 100);
	
	
	//
	logic->setSpeed(speed);
	
	// Running Variable
	Vec2 pos = startPos;
	Dir dir = startDir;
	
	
	float time = 0;
	float deltaStep = 0.1f;
	int nStep = 5;
	
	log("%3.3f: newPos=%s moveResult=%s", time,
		POINT_TO_STR(pos).c_str(),
		logic->infoResult().c_str());
	
	for(int i=0; i<nStep; i++) {
		
		logic->setPosition(pos);
		logic->setDir(dir);
		
		time += deltaStep;
		
		Vec2 newPos = logic->calculateNewPosition(deltaStep);
		
		log("%3.3f: oldPos=%s newPos=%s moveResult=%s",
			time,
			POINT_TO_STR(pos).c_str(),
			POINT_TO_STR(newPos).c_str(),
			logic->infoResult().c_str());
		
		// Setting the latest
		pos = newPos;
		if(logic->getNextDir() != DirNone) {
			dir = logic->getNextDir();
			logic->setWalkingRoute(logic->getCrossingRoute());
		}
		
		
		
		//
		
		
	}
}




void MoveLogicTest::testUpdateStateData()
{
	// Setup the Movement Logic
	RouteMoveLogic *logic = RouteMoveLogic::create();
	logic->setWalkingRoute(nullptr);
	logic->setGameMap(mGameMap);
	
	// Define the inital point & other setting
	Vec2 startPos = Vec2(mLineX[0], 210);
	Dir startDir = DirUp;
	
	Vec2 speed = Vec2(100, 100);
	
	
	//
	logic->setSpeed(speed);
	
	// Running Variable
	Vec2 pos = startPos;
	Dir dir = startDir;

	logic->setPosition(pos);
	logic->setDir(dir);
	
	
	float time = 0;
	float deltaStep = 0.1f;
	int nStep = 30;
	
	log("%3.3f: newPos=%s moveResult=%s", time,
		POINT_TO_STR(pos).c_str(),
		logic->infoResult().c_str());
	
	for(int i=0; i<nStep; i++) {
		time += deltaStep;
		
		Vec2 newPos = logic->calculateNewPosition(deltaStep);
		
		log("%3.3f: oldPos=%s newPos=%s moveResult=%s",
			time,
			POINT_TO_STR(pos).c_str(),
			POINT_TO_STR(newPos).c_str(),
			logic->infoResult().c_str());
		
		// Setting the latest
		logic->updateStateData(newPos);
		pos = logic->getPosition();
//		pos = newPos;
//		if(logic->getNextDir() != DirNone) {
//			dir = logic->getNextDir();
//			logic->setWalkingRoute(logic->getCrossingRoute());
//		}
//		
//		
//		
//		//
//		
//		
	}
}

void MoveLogicTest::step()
{
	if(mLogic == nullptr) {
		return;
	}
	
	Vec2 newPos = mLogic->calculateNewPosition(0.1f);
	
	mTestSprite->setPosition(newPos);
	
	mLogic->updateStateData(newPos);
}



void MoveLogicTest::testWithVisual()
{
	// Define the inital point & other setting
	Vec2 startPos = Vec2(mLineX[0], 210);
	Dir startDir = DirUp;
	
	Vec2 speed = Vec2(100, 100);
	
	
	// Setup the sprite position
	mTestSprite->setPosition(startPos);
	
	
	// Setup the Movement Logic
	RouteMoveLogic *logic = RouteMoveLogic::create();
	setMoveLogic(logic);

	
	
	// initialize
	logic->setPosition(startPos);
	logic->setSpeed(speed);
	logic->setWalkingRoute(nullptr);
	logic->setGameMap(mGameMap);
	logic->setDefaultDir(startDir);
	logic->setDir(startDir);
	
}


#endif
