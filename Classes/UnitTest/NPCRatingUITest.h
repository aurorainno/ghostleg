#ifdef ENABLE_TDD
//
//  NPCRatingUITest.h
//
//
#ifndef __TDD_NPCRatingUITest__
#define __TDD_NPCRatingUITest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class NPCRatingUITest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testGalleryView();
};

#endif

#endif
