#ifdef ENABLE_TDD
//
//  GameContinueDialogTest.h
//
//
#ifndef __TDD_GameContinueDialogTest__
#define __TDD_GameContinueDialogTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class GameContinueDialogTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testIncCost();
	void testNoAd();
};

#endif

#endif
