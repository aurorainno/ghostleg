#ifdef ENABLE_TDD
//
//  ADResponseGetRankingTest.m
//	TDD Framework
//
//
#include "ADResponseGetRankingTest.h"
#include "TDDHelper.h"
#include "ADResponseGetRanking.h"
#include "ADResponseSubmitScore.h"
#include "ADResponseFBConnect.h"
#include "TimeHelper.h"
#include "AstrodogData.h"

void ADResponseGetRankingTest::setUp()
{
	log("TDD Setup is called");
}


void ADResponseGetRankingTest::tearDown()
{
	log("TDD tearDown is called");
}

void ADResponseGetRankingTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void ADResponseGetRankingTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void ADResponseGetRankingTest::defineTests()
{
	ADD_TEST(testParse);
	ADD_TEST(testParseSubmitScore);
	ADD_TEST(testParseFBConnect);
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void ADResponseGetRankingTest::testSample()
{
	log("this is a sample subTest");
	
	ADResponseGetRanking *response = ADResponseGetRanking::create();
	
	
	
	log("response=%s", response->toString().c_str());
}

void ADResponseGetRankingTest::testParse()
{
	log("testParse logic");
	
	ADResponseGetRanking *response = ADResponseGetRanking::create();
	
	std::string file = "json/response/getRanking.json";
	std::string content = FileUtils::getInstance()->getStringFromFile(file);
	
	response->parseResponse(content);
	
	log("response=%s", response->toString().c_str());
	
	long long endTime = response->getLeaderboard()->getEndTime();
	
	
	long long now = TimeHelper::getCurrentTimeStampInMillis();
	
	long long diff = endTime - now;
	
	std::map<std::string,int> result = TimeHelper::parseMillis(diff);
	log("result: %dday %dhours %dminutes", result["days"], result["hours"], result["minutes"]);
	log("now=%lld", now);
}

void ADResponseGetRankingTest::testParseSubmitScore()
{
	log("testParse logic");
	
	ADResponseSubmitScore *response = ADResponseSubmitScore::create();
	
	std::string file = "json/response/submitScore.json";
	std::string content = FileUtils::getInstance()->getStringFromFile(file);
	
	response->parseResponse(content);
	
	log("response=%s", response->toString().c_str());
}

void ADResponseGetRankingTest::testParseFBConnect()
{
	log("testFBConnect logic");
	
	ADResponseFBConnect *response = ADResponseFBConnect::create();
	
	std::string file = "json/response/connectFB.json";
	std::string content = FileUtils::getInstance()->getStringFromFile(file);
	
	response->parseResponse(content);
	
	log("response=%s", response->toString().c_str());

}



#endif
