#ifdef ENABLE_TDD
//
//  ItemTest.h
//
//
#ifndef __TDD_ItemTest__
#define __TDD_ItemTest__

// Include Header

#include "TDDBaseTest.h"

class Item;

// Class Declaration 
class ItemTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
	
private:
	Item *mItem;
	
private:
	void testCoin();
	void testUse();
	void testStar();
	void testMissile();
	void testPotion();
	void testCapsule();
	void testAllCapsule();
	void testCashout();
	void testMagnet();
	

private: // helper method
	void setItem(Item *item);
};

#endif

#endif
