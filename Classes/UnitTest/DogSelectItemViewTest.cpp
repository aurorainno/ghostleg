#ifdef ENABLE_TDD
//
//  DogSelectItemViewTest.m
//	TDD Framework
//
//
#include "DogSelectItemViewTest.h"
#include "TDDHelper.h"
#include "DogSelectItemView.h"
#include "CharacterItemView.h"
#include "VisibleRect.h"
#include "DogManager.h"
#include "PlayerCharProfile.h"
#include "PlayerManager.h"

void DogSelectItemViewTest::setUp()
{
	log("TDD Setup is called");
}


void DogSelectItemViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void DogSelectItemViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DogSelectItemViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DogSelectItemViewTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testDifferentState);
	ADD_TEST(testDogForSuggest);
	ADD_TEST(debugDog1);
}

#pragma mark -
#pragma mark Sub Test Definition
void DogSelectItemViewTest::debugDog1()
{
	log("this is a sample subTest");
	DogSelectItemView *itemView = DogSelectItemView::create();
	
	itemView->setPosition(Vec2(0, VisibleRect::center().y));
	
	itemView->setDog(1);
	
	addChild(itemView);
}

void DogSelectItemViewTest::testSample()
{
	log("this is a sample subTest");

    PlayerCharProfile *profile = PlayerCharProfile::create();
    profile->setup(2);
    profile->alterFragmentCount(2);
    profile->doUpgradeLogic();

	CharacterItemView *itemView = CharacterItemView::create();
	
	itemView->setPosition(Vec2(0, VisibleRect::center().y));
	
    itemView->setBtnCallback([=](int charID){
        log("char %d has been selected",charID);
    });
    
    itemView->setView(profile);

    log("view height: %.2f",itemView->getContentSize().height);
	
	addChild(itemView);
}

void DogSelectItemViewTest::testDogForSuggest()
{
	DogSelectItemView *itemView = DogSelectItemView::create();
	
	itemView->setPosition(Vec2(0, VisibleRect::center().y));
	
	itemView->setDogForSuggestion(4);
	
	addChild(itemView);
}


void DogSelectItemViewTest::testDifferentState()
{
	log("this is a sample subTest");
	
	bool testData[3][2] = {
		// isSelectd, isUnlocked
		{false, false},
		{false, true},
		{true, true},			// note: no isUnlocked=false but selected
	};
	
	Vec2 pos = VisibleRect::top() - Vec2(0, 100);
	pos.x = 0;
	
	for(int i=0; i<3; i++) {
		bool isSelected = testData[i][0];
		bool isUnlocked = testData[i][1];
		

		DogSelectItemView *itemView = DogSelectItemView::create();
		
		itemView->setPosition(pos);
		
		itemView->setDog(2);
		itemView->testUIState(isSelected, isUnlocked);
		addChild(itemView);

		
		pos.y -= (itemView->getContentSize().height + 10);
	}
	
}


#endif
