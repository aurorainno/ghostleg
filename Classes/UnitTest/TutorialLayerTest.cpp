#ifdef ENABLE_TDD
//
//  TutorialLayerTest.m
//	TDD Framework
//
//
#include "TutorialLayerTest.h"
#include "TDDHelper.h"
#include "TutorialLayer.h"
#include "TutorialManager.h"
#include "GameWorld.h"
#include "Player.h"
#include "TutorialData.h"
#include "VisibleRect.h"

void TutorialLayerTest::setUp()
{
	log("TDD Setup is called");
	
	TutorialManager::instance()->loadTutorialData();
	
	mTutorialLayer = nullptr;
	
	// start schedule
	scheduleUpdate();

}



void TutorialLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void TutorialLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void TutorialLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}

void TutorialLayerTest::update(float delta)
{
	if(mTutorialLayer) {
		GameWorld::instance()->getPlayer()->move(delta);
		GameWorld::instance()->moveCameraToPlayer();
		mTutorialLayer->update(delta);
	}
}


#pragma mark -
#pragma mark List of Sub Tests

void TutorialLayerTest::defineTests()
{
	ADD_TEST(testSimpleCase);
	ADD_TEST(testUnlockPlanet);
	ADD_TEST(testUnlockPlanet2);
}

#pragma mark -
#pragma mark Sub Test Definition
void TutorialLayerTest::testSimpleCase()
{
	clearNodes();
	
	TutorialLayer *tutorialLayer = TutorialLayer::create();
	tutorialLayer->reset();
	addChild(tutorialLayer);
	mTutorialLayer = tutorialLayer;

	mTutorialLayer->update(0);
	
}

void TutorialLayerTest::testUnlockPlanet()
{
	// Tutorial Data
	Vector<TutorialData *> tutorialList;
	
	TutorialData *tutorialData = TutorialData::create();
	tutorialData->setStep(0);
	tutorialData->setStartCondition(TutorialData::StartCondition::AutoStart);
	tutorialData->setStartConditionArg(0);
	tutorialData->setEndCondition(TutorialData::EndCondition::TapHitArea);
	//tutorialData->setDisplayType(TutorialData::DisplayType::DisplayTypeBanner);
	tutorialData->setDisplayType(TutorialData::DisplayType::DisplayTypeTip);
	tutorialData->setDisplayCsb("tutorial_unlock_planet.csb");
	tutorialData->setDisplayPosition(VisibleRect::center());
//	tutorialData->setHitAreaByRadius(VisibleRect::center(), 50);
	tutorialList.pushBack(tutorialData);

	
	
	
	//
	clearNodes();
	
	TutorialLayer *tutorialLayer = TutorialLayer::create();
	
	tutorialLayer->setColor(Color3B::YELLOW);
	
	tutorialLayer->setAutoCleanup(true);
	tutorialLayer->setIsInGameTutorial(false);
	tutorialLayer->setTutorialDataList(tutorialList);
	tutorialLayer->setCloseCallback([&](Ref *sender){
		mTutorialLayer = nullptr;
	});
	
	
	
	tutorialLayer->reset();
	addChild(tutorialLayer);
	mTutorialLayer = tutorialLayer;
	
	mTutorialLayer->update(0);
}

void TutorialLayerTest::testUnlockPlanet2()
{
	clearNodes();
	
	TutorialLayer *tutorialLayer = TutorialLayer::create();
	
	tutorialLayer->setCloseCallback([&](Ref *sender){
		mTutorialLayer = nullptr;
	});
	
//	tutorialLayer->setCoachmarkTutorial("testing", "tutorial_unlock_planet.csb",
//										Vec2(100, 100));
	
	
	//
	tutorialLayer->reset();
	addChild(tutorialLayer);
	mTutorialLayer = tutorialLayer;
	
	mTutorialLayer->update(0);
}

#endif
