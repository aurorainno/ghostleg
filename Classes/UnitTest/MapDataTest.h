#ifdef ENABLE_TDD
//
//  MapDataTest.h
//
//
#ifndef __TDD_MapDataTest__
#define __TDD_MapDataTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class MapDataTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testEnemyList();
	void testSample();
	void testRandomLine();
};

#endif

#endif
