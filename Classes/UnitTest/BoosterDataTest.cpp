#ifdef ENABLE_TDD
//
//  BoosterDataTest.m
//	TDD Framework
//
//
#include "BoosterDataTest.h"
#include "TDDHelper.h"
#include "BoosterData.h"

void BoosterDataTest::setUp()
{
	log("TDD Setup is called");
}


void BoosterDataTest::tearDown()
{
	log("TDD tearDown is called");
}

void BoosterDataTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void BoosterDataTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void BoosterDataTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void BoosterDataTest::testSample()
{
	BoosterData *data = BoosterData::create();
	
	data->setType(BoosterItemDoubleCoin);
	data->setPrice(Price(MoneyTypeDiamond, 3));
	data->setInfo("This is very powerful");
	data->setAttribute("duration", 3);
	data->setAttribute("speed", 3.4f);
	
	log("boosterData=%s", data->toString().c_str());
}


#endif
