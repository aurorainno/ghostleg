#ifdef ENABLE_TDD
//
//  UnlockDogLayerTest.h
//
//
#ifndef __TDD_UnlockDogLayerTest__
#define __TDD_UnlockDogLayerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class UnlockDogLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
    virtual void testFireWork();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testBuyUnlock();
};

#endif

#endif
