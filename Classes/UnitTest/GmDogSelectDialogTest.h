#ifdef ENABLE_TDD
//
//  GmDogSelectDialogTest.h
//
//
#ifndef __TDD_GmDogSelectDialogTest__
#define __TDD_GmDogSelectDialogTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class GmDogSelectDialogTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
};

#endif

#endif
