#ifdef ENABLE_TDD
//
//  GameModelTest.h
//
//
#ifndef __TDD_GameModelTest__
#define __TDD_GameModelTest__

// Include Header

#include "TDDBaseTest.h"

class GameModel;
class ItemEffectUILayer;

// Class Declaration 
class GameModelTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void addSampleModel();
	void testLoader();

	void testSpriteSheet();
	void changeAnime();
	void toggleFace();
	void testItemEffectUI();
	void testAddIndicator();
	void testRemoveIndicator();
	void testPlayCsbOnce();
	void testAddCsbAnime();
	void testBoundingBox();
	void testActionManager();
	void testUpdate();
	void testPauseResume();
	void testParticle();
	void testPlayAction();
	void testOpacity();
	void testPlayerVisualEffect();
	void testStatusIcon();
	void testAllEnemyStatus();
	void testUpdateIndicator();
	void testPerformance();
	
private:
	GameModel *mModel;
	ActionManager *mActionManager;
	ItemEffectUILayer *mIndicator;
}; 

#endif

#endif
