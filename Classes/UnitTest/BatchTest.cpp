#ifdef ENABLE_TDD
//
//  BatchTest.m
//	TDD Framework
//
//
#include "BatchTest.h"
#include "TDDHelper.h"
#include "VisibleRect.h"
#include "ui/CocosGUI.h"
#include "RouteLineNode.h"

void BatchTest::setUp()
{
	log("TDD Setup is called");
}


void BatchTest::tearDown()
{
	log("TDD tearDown is called");
}

void BatchTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void BatchTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


void BatchTest::addTestSprite(const Vec2 &pos)
{
	Sprite *sprite = Sprite::create("general/route_line.png");
	sprite->setPosition(pos);
	addChild(sprite);
}

#pragma mark -
#pragma mark List of Sub Tests

void BatchTest::defineTests()
{
	ADD_TEST(testSprite);
	ADD_TEST(testSprLayout);
	ADD_TEST(testRouteLine);
}

#pragma mark -
#pragma mark Sub Test Definition
void BatchTest::testSprite()
{
	float x = RandomHelper::random_int(10, (int)(VisibleRect::right().x -10));
	float y = RandomHelper::random_int(10, (int)(VisibleRect::top().y -10));
	addTestSprite(Vec2(x, y));
}

void BatchTest::testSprLayout()
{
	float x = RandomHelper::random_int(10, (int)(VisibleRect::right().x -10));
	float y = RandomHelper::random_int(10, (int)(VisibleRect::top().y -10));

	ui::Layout *layout = ui::Layout::create();
	layout->setPosition(Vec2(x, y));
	addChild(layout);
	
	Sprite *sprite = Sprite::create("general/route_line.png");
	sprite->setPosition(Vec2::ZERO);
	layout->addChild(sprite);
}


void BatchTest::testRouteLine()
{
	float x = RandomHelper::random_int(10, (int)(VisibleRect::right().x -10));
	float y = RandomHelper::random_int(10, (int)(VisibleRect::top().y -10));
	
	RouteLineNode *node = RouteLineNode::create();
	node->setPosition(Vec2::ZERO);
	addChild(node);
	
	node->setStart(VisibleRect::center());
	node->setEnd(Vec2(x, y));
	node->setUseDrawNode(false);
	node->updateLine();
}



#endif
