#ifdef ENABLE_TDD
//
//  NPCTest.h
//
//
#ifndef __TDD_NPCTest__
#define __TDD_NPCTest__

// Include Header

#include "TDDBaseTest.h"
#include "NPC.h"

// Class Declaration
class NPCTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testNPCList();
	void testDifferentFace();
	void testEmoji();
	void changeEmoji();
	
private:
	NPC *mNpc;
	NPC::Emoji mNpcEmoji;
};

#endif

#endif
