#ifdef ENABLE_TDD
//
//  DeviceHelperTest.m
//	TDD Framework 
//
//
#include "DeviceHelperTest.h"
#include "TDDHelper.h"
#include "DeviceHelper.h"

void DeviceHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void DeviceHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void DeviceHelperTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(DeviceHelperTest::testExternalPath);
	SUBTEST(DeviceHelperTest::testConnectInternet);
    SUBTEST(DeviceHelperTest::testGetBuildNumber);
    SUBTEST(DeviceHelperTest::testGetCountryCode);
}

#pragma mark -
#pragma mark Sub Test Definition
void DeviceHelperTest::testExternalPath(Ref *sender)
{
	//log("this is a sample subTest");
	std::string storePath = DeviceHelper::getStoragePath();
	
	log("storePath=%s", storePath.c_str());
}

void DeviceHelperTest::testConnectInternet(Ref *sender)
{
	bool isConnected = DeviceHelper::isConnectedToInternet();
	
	log("isConnected=%d", isConnected);
}

void DeviceHelperTest::testGetBuildNumber(cocos2d::Ref *sender)
{
    int buildNum = DeviceHelper::getBuildNumber();
    
    log("build number=%d", buildNum);
}

void DeviceHelperTest::testGetCountryCode(cocos2d::Ref *sender)
{
    std::string countryCode = DeviceHelper::getCountryCode();
    
    log("countryCode=%s",countryCode.data());
}
#endif
