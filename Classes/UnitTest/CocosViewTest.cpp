#ifdef ENABLE_TDD
//
//  CocosViewTest.m
//	TDD Framework 
//
//
#include "CocosViewTest.h"
#include "TDDHelper.h"
#include "VisibleRect.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

using namespace cocostudio::timeline;

void CocosViewTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void CocosViewTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void CocosViewTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testLayer);
	ADD_TEST(testCsbAnime);
}

#pragma mark -
#pragma mark Sub Test Definition
void CocosViewTest::testCsbAnime()
{
	Node *rootNode = CSLoader::createNode("UiTestScene.csb");
	addChild(rootNode);
	
	Node *fileNode = rootNode->getChildByName("titleFileNode");
	ActionTimeline *itemAction =
		(ActionTimeline *) fileNode->getActionByTag(fileNode->getTag());
	itemAction->play("anime1", true);
	log("itemAction is null? %d", (itemAction == nullptr));
}

void CocosViewTest::subTest()
{
	log("this is a sample subTest");
	
	ScrollView *scrollView = ScrollView::create(Size(200, 200));
	scrollView->setColor(Color3B::BLUE);
	scrollView->setAnchorPoint(Vec2(0.0, 0.0));
	scrollView->setPosition(Vec2(50, 50));
	
	
	addChild(scrollView);
	
	//Color3B
	
	Color4B color = Color4B::RED;
	
	Vec2 pos(10, 0);
	for(int i=0; i<10; i++) {
		LayerColor *layer = LayerColor::create(color, 180, 50);
		
		layer->setPosition(pos);
		
		scrollView->addChild(layer);
		
		pos.y += layer->getContentSize().height + 10;
		
		color.r -= 10;
	}
	
}

void CocosViewTest::testLayer()
{
	log("this is a sample subTest");

	LayerColor *colorLayer = LayerColor::create(Color4B::RED, 100, 100);
	colorLayer->setAnchorPoint(Vec2(0, 1));
	colorLayer->setPosition(VisibleRect::center());
	
	addChild(colorLayer);
}

#endif
