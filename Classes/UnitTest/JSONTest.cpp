#ifdef ENABLE_TDD
//
//  JSONTest.m
//	TDD Framework 
//
//
#include <map>

#include "JSONTest.h"
#include "TDDHelper.h"
#include "JSONHelper.h"
#include "JSONData.h"

// Using JSON
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include "cocostudio/DictionaryHelper.h"

#include "AstrodogData.h"

using namespace rapidjson;
using namespace cocostudio;


#pragma mark - Extends Class of JSONData
class MyAssetData : public JSONData, public Ref
{
public:
	CC_SYNTHESIZE(int, mMoney, Money);
	CC_SYNTHESIZE(int, mGem, Gem);
	
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
	{
		addInt(allocator, "money", mMoney, outValue);
		addInt(allocator, "gem", mGem, outValue);
	}
	
	virtual bool parseJSON(const rapidjson::Value &jsonValue)
	{
		mMoney = aurora::JSONHelper::getInt(jsonValue, "money", 0);
		mGem = aurora::JSONHelper::getInt(jsonValue, "gem", 0);
		
		return true;
	}

	
	std::string toString() {
		return StringUtils::format("money=%d gem=%d", mMoney, mGem);
	}
};

#pragma mark - Test Logic

void JSONTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void JSONTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void JSONTest::defineTests()
{
	ADD_TEST(subTest);
    ADD_TEST(subTest2);
	ADD_TEST(testJSONData);
	ADD_TEST(testGetJsonStrMap);
    ADD_TEST(masteryTest);
    ADD_TEST(testAstrodogUserData);
}

#pragma mark -
#pragma mark Sub Test Definition
// Reference:
//	http://rstechjournal.blogspot.hk/2014/08/using-rapidjson-in-cocos2d-x-creating.html
//	http://rapidjson.org/md_doc_tutorial.html
void JSONTest::subTest()
{
	log("this is a sample subTest");
	std::string content = FileUtils::getInstance()->getStringFromFile("json/testing.json");
	
	log("content:\n%s", content.c_str());
	rapidjson::Document doc;
	doc.Parse<0>(content.c_str());
	if (doc.HasParseError())
	{
		CCLOG("GetParseError %d\n", doc.GetParseError());
		return;
	}
	
	const char *value = DICTOOL->getStringValue_json(doc, "str");
	log("str=%s", value);
	
	float pi = DICTOOL->getFloatValue_json(doc, "pi");
	log("pi=%f", pi);
	
	int intValue = DICTOOL->getIntValue_json(doc, "i");
	log("i=%d", intValue);
}

void JSONTest::subTest2()
{
    log("this is a sample subTest");
    std::string content = FileUtils::getInstance()->getStringFromFile("json/testing.json");
    
    log("content:\n%s", content.c_str());
    rapidjson::Document doc;
    doc.Parse(content.c_str());
    if (doc.HasParseError())
    {
        CCLOG("GetParseError %d\n", doc.GetParseError());
        return;
    }
    
    const char *value = doc["str"].GetString();
    log("str=%s", value);
    
    rapidjson::Value& c = doc["pi"];
    log("pi=%.2f", c.GetDouble());
    
//    int intValue = DICTOOL->getIntValue_json(doc, "i");
    log("i=%d", doc["i"].GetInt());
    
    const rapidjson::Value& objectArray = doc["obj"];
    for(rapidjson::SizeType i=0;i<objectArray.Size();i++){
        const rapidjson::Value& currentObj = objectArray[i];
        
        log("obj[%i], id=%i, content=%s", i,currentObj["id"].GetInt(),currentObj["content"].GetString());
    }
}

void JSONTest::masteryTest()
{
    log("this is a sample subTest");
    std::string content = FileUtils::getInstance()->getStringFromFile("json/mastery.json");
    
    log("content:\n%s", content.c_str());
    rapidjson::Document masteryArray;
    masteryArray.Parse(content.c_str());
    if (masteryArray.HasParseError())
    {
        CCLOG("GetParseError %d\n", masteryArray.GetParseError());
        return;
    }
    
    for(rapidjson::SizeType i=0;i<masteryArray.Size();i++){
        const rapidjson::Value& mastery = masteryArray[i];
        printMastery(mastery);
    }
}

void JSONTest::printMastery(const rapidjson::Value& mastery){
    log("\nmastery[%i], id=%i, name=%s, info=%s,\nprice:", mastery["masteryID"].GetInt()-1,mastery["masteryID"].GetInt(),mastery["name"].GetString(),mastery["info"].GetString());
    for(int j=0;j<mastery["price"].Size();j++){
        printf("%i, ",mastery["price"][j].GetInt());
    }
    
    log("\nlevelValue:");
    for(int k=0;k<mastery["value"].Size();k++){
        printf("%.1f, ",mastery["value"][k].GetDouble());
    }
}

void JSONTest::testGetJsonStrMap()
{
	std::string content = FileUtils::getInstance()->getStringFromFile("json/testing.json");
	rapidjson::Document docRoot;
	docRoot.Parse(content.c_str());
	if (docRoot.HasParseError())
	{
		CCLOG("testGetJsonStrMap %d\n", docRoot.GetParseError());
		return;
	}
	
	std::map<std::string, std::string> outMap;
	aurora::JSONHelper::getJsonStrMap(docRoot, "prop", outMap);
	
	
}


void JSONTest::testJSONData()
{
	MyAssetData *data = new MyAssetData();
	data->setGem(1000);
	data->setMoney(10);
	
	std::string json = data->toJSONContent();
	
	log("json: %s", json.c_str());
	
	
	MyAssetData *parsedData = new MyAssetData();
	
	parsedData->parseJSONContent(json);
	
	log("parsedData: %s", parsedData->toString().c_str());
	
	delete data;
	delete parsedData;
}

void JSONTest::testAstrodogUserData()
{
    AstrodogUser *user = AstrodogUser::create();
    user->setUid(999);
    user->setFbID("10999");
    user->setName("Calvin");
    user->setCountry("HK");
    
//    user->addFriend(1111);
//    user->addFriend(2222);
//    user->addFriend(3333);
//    user->addFriend(4444);
//    user->addFriend(5555);
    
    std::string content = user->toJSONContent();
    
    AstrodogUser *userTwo = AstrodogUser::create();
    userTwo->parseJSONContent(content);
    
    log("%s", userTwo->toString().data());
}


#endif
