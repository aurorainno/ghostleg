#ifdef ENABLE_TDD
//
//  EmojiAnimeNodeTest.h
//
//
#ifndef __TDD_EmojiAnimeNodeTest__
#define __TDD_EmojiAnimeNodeTest__

// Include Header

#include "TDDBaseTest.h"

class EmojiAnimeNode;

// Class Declaration
class EmojiAnimeNodeTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testEmoji();
	void changeEmoji();
	void testCloseEmoji();

private:
	int mEmojiValue;
	EmojiAnimeNode *mEmojiNode;
};

#endif

#endif
