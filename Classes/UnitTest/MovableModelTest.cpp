#ifdef ENABLE_TDD
//
//  MovableModelTest.m
//	TDD Framework 
//
//
#include "MovableModelTest.h"
#include "TDDHelper.h"
#include "MovableModel.h"
#include "Enemy.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "EnemyFactory.h"


void MovableModelTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	
	mModel = MovableModel::create();
	mModel->setPosition(Vec2(50, 100));
	mModel->setDir(DirUp);
	mModel->setup("player1.csb");
	
	addChild(mModel);

//	// Add the world
//	GameWorld *world = GameWorld::instance();
//	addChild(world);

}


void MovableModelTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
}

#pragma mark -
#pragma mark List of Sub Tests

void MovableModelTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(MovableModelTest::subTest);
	SUBTEST(MovableModelTest::toggleRun);
	SUBTEST(MovableModelTest::testEnemyTexture);
	SUBTEST(MovableModelTest::testEnemyFactory);
}

#pragma mark -
#pragma mark Sub Test Definition
void MovableModelTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	
	mModel->move(0.03f);
}

void MovableModelTest::toggleRun(Ref *sender)
{
	if(mRunning) {
		GameWorld::instance()->stop();
	} else {
		GameWorld::instance()->start();
	}
}

void MovableModelTest::testEnemyFactory(Ref *sender)
{
	Enemy *normal = EnemyFactory::createByType(100);			// normal type
	Enemy *moveStraight = EnemyFactory::createByType(101);		// move straight at corner

	GameWorld::instance()->addEnemyAtLine(normal, 0, 500);
	GameWorld::instance()->addEnemyAtLine(moveStraight, 1, 500);
	
//	GameWorld::instance()->addE
}

void MovableModelTest::testEnemyTexture(Ref *sender)
{
	Vec2 pos = Vec2(100, 100);
	for(int i=1; i<=6; i++) {
		Enemy *enemy = Enemy::create();
		enemy->setPosition(pos);
		enemy->setResource(i);
		
		addChild(enemy);
		pos.y += 50;
	}
}


#endif
