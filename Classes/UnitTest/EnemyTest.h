#ifdef ENABLE_TDD
//
//  EnemyTest.h
//
//
#ifndef __TDD_EnemyTest__
#define __TDD_EnemyTest__

// Include Header

#include "TDDBaseTest.h"

class Enemy;

// Class Declaration 
class EnemyTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();

	void setEnemy(int enemyID);
	Enemy *createEnemy(int enemyID);
	
private:
	void testSetupScaleAndFlip();
	void testFlip();
	void testScale2();
	void testScale();
	void testDirection();
	void testGivenEnemy();
	void changeEnemy();
	void subTest();
	void step();
	void hitEnemy();
	void testOpacity();
	void wakeUpAlert();
	void testEnemySize1();
	
private:
	int mEnemyID;
	Enemy *mEnemy;
}; 

#endif

#endif
