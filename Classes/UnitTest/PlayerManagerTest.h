#ifdef ENABLE_TDD
//
//  PlayerManagerTest.h
//
//
#ifndef __TDD_PlayerManagerTest__
#define __TDD_PlayerManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class PlayerManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testInfo();
	void testAbilitySetting();
	void testPlayerCharData();
	void testPlayerCharProfile();
	void testUpgrade();
	void testLoadData();
	void testPlayerCharSetJSON();
	void testPlayerCharSetSaveLoad();
	void testSelect();
	void testCharacterList();
	void testAbilityBuff();
	void testAbilityByLevel();
    void testAlterFragment();
};

#endif

#endif
