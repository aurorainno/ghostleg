#ifdef ENABLE_TDD
//
//  RandomHelperTest.h
//
//
#ifndef __TDD_RandomHelperTest__
#define __TDD_RandomHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class RandomHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testRandomRange();
	void testShuffle();
	void testFindRandomValue();
	void testFindMultiRandomValue();
	void testFindMultiRandomValueRepeat();
	void testRandomizedList();
}; 

#endif

#endif
