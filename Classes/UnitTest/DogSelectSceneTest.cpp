#ifdef ENABLE_TDD
//
//  DogSelectSceneTest.m
//	TDD Framework
//
//
#include "DogSelectSceneTest.h"
#include "TDDHelper.h"
#include "DogSelectionScene.h"
#include "CharacterSelectionLayer.h"
#include "DogData.h"
#include "DogManager.h"
#include "PlayerManager.h"
#include "PlayerCharProfile.h"

void DogSelectSceneTest::setUp()
{
	log("TDD Setup is called");
}


void DogSelectSceneTest::tearDown()
{
	log("TDD tearDown is called");
}

void DogSelectSceneTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DogSelectSceneTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DogSelectSceneTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void DogSelectSceneTest::testSample()
{
	log("this is a sample subTest");
	
//	DogSelectionSceneLayer *layer = DogSelectionSceneLayer::create();
	
//	addChild(layer);
    
//    DogData* data = DogManager::instance()->getSelectedDogData();
    Vector<PlayerCharProfile*> lockedList = PlayerManager::instance()->getLockedCharacters();
    Vector<PlayerCharProfile*> ownedList = PlayerManager::instance()->getOwnedCharacters();
    
    CharacterSelectionLayer *layer = CharacterSelectionLayer::create();
    layer->setScrollView(lockedList, ownedList);
    
 //   layer->setupTopPanel(data->getDogID());
    addChild(layer);
}


#endif
