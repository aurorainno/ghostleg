#ifdef ENABLE_TDD
//
//  CommonDialogTest.m
//	TDD Framework
//
//
#include "CommonDialogTest.h"
#include "TDDHelper.h"
#include "CommonDialog.h"

void CommonDialogTest::setUp()
{
	log("TDD Setup is called");
}


void CommonDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void CommonDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void CommonDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void CommonDialogTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void CommonDialogTest::testSample()
{
	log("this is a sample subTest");
	CommonDialog *dialog = CommonDialog::create();
	addChild(dialog);
	
	
	dialog->setTitle("Hello World");
	dialog->setContent("This is a good day to code");
	dialog->setActionCallback([&](Ref *sender, CommonDialog::Action action){
		log("action=%d", action);
	});
	
}


#endif
