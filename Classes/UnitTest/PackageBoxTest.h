#ifdef ENABLE_TDD
//
//  PackageBoxTest.h
//
//
#ifndef __TDD_PackageBoxTest__
#define __TDD_PackageBoxTest__

// Include Header

#include "TDDBaseTest.h"

class PackageBox;

// Class Declaration
class PackageBoxTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testDropDown();
	
private:
	PackageBox *mPackageBox;
};

#endif

#endif
