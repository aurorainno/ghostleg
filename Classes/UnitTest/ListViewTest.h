#ifdef ENABLE_TDD
//
//  ListViewTest.h
//
//
#ifndef __TDD_ListViewTest__
#define __TDD_ListViewTest__

// Include Header

#include "TDDBaseTest.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;

// Class Declaration
class ListViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testTestList();
	void testDogListBug();
	void testDogList();
	
private:
	ListView *mListView;
};

#endif

#endif
