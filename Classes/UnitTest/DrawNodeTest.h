#ifdef ENABLE_TDD
//
//  DrawNodeTest.h
//
//
#ifndef __TDD_DrawNodeTest__
#define __TDD_DrawNodeTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class DrawNodeTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testLine(Ref *sender);
	void testRect(Ref *sender);
	
private:
	DrawNode *mDrawNode;
}; 

#endif

#endif
