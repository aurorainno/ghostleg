#ifdef ENABLE_TDD
//
//  ModelGalleryTest.m
//	TDD Framework 
//
//

#include "ModelGalleryTest.h"
#include "TDDHelper.h"
#include "GameModel.h"
#include "VisibleRect.h"
#include "StringHelper.h"

const int numAnimation = 18;
const char animationArray[numAnimation][50] =
{
	"item/item_puzzle.csb",
	"player/player_003.csb",
	"player/player_007.csb",
	"player/player_008.csb",
	"player/player_009.csb",
	"player/player_010.csb",
	"player/player_011.csb",
	"player/player_012.csb",
	"player/player_013.csb",
	"player/player_014.csb",
	"player/player_015.csb",
	"player/player_016.csb",
	"player/player_017.csb",
	"player/player_018.csb",
	"player/player_019.csb",
	"player/player_020.csb",
	"player/player_021.csb",
	
	
//	"enemy/monster_010.csb",
//	"enemy/monster_311.csb",
//	"enemy/monster_316.csb",
//	"enemy/monster_009.csb",
//	"enemy/monster_001.csb",
//	"enemy/monster_002.csb",
//	"enemy/monster_003.csb",
//	"enemy/monster_004.csb",
//	"enemy/monster_005.csb",
//	"enemy/monster_006.csb",
//	"enemy/monster_008.csb",
	
//	"monster09.csb", "monster10.csb", "monster11.csb",
//	"monster12.csb", "monster13.csb", "monster14.csb", "monster15.csb",
//	"monster16.csb", "monster17.csb", "monster18.csb", "monster19.csb",
//	"item01.csb", "item02.csb", "missile.csb"
	
};

const int numAction = 7;
const GameModel::Action actionArray[numAction] =
{
	GameModel::Action::Walk,
	GameModel::Action::Climb,
	GameModel::Action::Idle,
	GameModel::Action::Attack,
	GameModel::Action::Consume,
	GameModel::Action::Die,
	GameModel::Action::Invisible,
};

void ModelGalleryTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	// Load SpriteSheet
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("player/player_001.plist");
	
	// The Model
	mModel = GameModel::create();
	mModel->setScale(1.0f);
	
	addChild(mModel);
	mModel->setPosition(VisibleRect::center());

	Vec2 pos = VisibleRect::center();
	
	pos.y -= 30;
	
	// Label of the Animation
	TTFConfig ttfConfig("arial.ttf", 20);
	Label *label;
	
	label = Label::createWithTTF(ttfConfig, "Animation");
	label->setPosition(pos);
	addChild(label);
	mAnimeLabel = label;
	
	pos.y -= 30;
	label = Label::createWithTTF(ttfConfig, "Action");
	label->setPosition(pos);
	addChild(label);
	mActionLabel = label;
	
	
	pos.y -= 30;
	label = Label::createWithTTF(ttfConfig, "Dir");
	label->setPosition(pos);
	addChild(label);
	mDirLabel = label;
	
	setAnimation(animationArray[0]);
	setAction(actionArray[0]);
	//
	// mModel->setup(animationArray[0]);
}

void ModelGalleryTest::setAnimation(const char *name)
{
	mModel->setup(name, (GameModel::Action) mAction);
	mAnimeLabel->setString(name);
}

void ModelGalleryTest::setAction(int actionValue)
{
	GameModel::Action action = (GameModel::Action) actionValue;
	mAction = (int) action;
	
	// Name
	const char *actionName;
	switch(action) {
		case GameModel::Action::Walk:		actionName = "walk"; break;
		case GameModel::Action::Climb:		actionName = "climb"; break;
		case GameModel::Action::Idle:		actionName = "idle"; break;
		case GameModel::Action::Attack:		actionName = "attack"; break;
		case GameModel::Action::Consume:	actionName = "consume"; break;
		case GameModel::Action::Invisible:	actionName = "invisible"; break;
		default: actionName = "unknown"; break;
	}
	
	
	//
	mModel->setAction(action);
	mActionLabel->setString(actionName);
}


void ModelGalleryTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ModelGalleryTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(ModelGalleryTest::changeAnime);
	SUBTEST(ModelGalleryTest::changeAction);
	SUBTEST(ModelGalleryTest::changeDir);
	SUBTEST(ModelGalleryTest::addParticleEffect);
	SUBTEST(ModelGalleryTest::getModelInfo);
}

#pragma mark -
#pragma mark Sub Test Definition

void ModelGalleryTest::getModelInfo(Ref *sender)
{
	if(mModel) {
		Size contentSize = mModel->getContentSize();
		
		log("Size: %s", SIZE_TO_STR(contentSize).c_str());
	}
}

void ModelGalleryTest::changeAnime(Ref *sender)
{
	log("this is a sample subTest");
	static int idx = 1;
	int modIdx = idx % numAnimation;
	const char *newAnime = animationArray[modIdx];
	setAnimation(newAnime);
	
	idx++;
}

void ModelGalleryTest::changeAction(Ref *sender)
{
	log("this is a sample subTest");
	static int idx = 1;
	int modIdx = idx % numAction;

	setAction(actionArray[modIdx]);
	
	idx++;
}


void ModelGalleryTest::changeDir(Ref *sender)
{
	log("Calling changeDir");
	static bool toLeft = true;
	
	toLeft = ! toLeft;
	
	if(mModel) {
		mModel->setFace(toLeft);
	}
	
	if(toLeft) {
		mDirLabel->setString("left");
	} else {
		mDirLabel->setString("right");
	}
}

void ModelGalleryTest::addParticleEffect(Ref *sender)
{
	if(mModel) {
		mModel->addParticleEffect(GameRes::Particle::ShieldBreak);
	}
}

#endif
