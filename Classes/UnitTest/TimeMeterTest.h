#ifdef ENABLE_TDD
//
//  TimeMeterTest.h
//
//
#ifndef __TDD_TimeMeterTest__
#define __TDD_TimeMeterTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class TimeMeterTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testTime();
	void testHighTime();
	void testStartStop();
	void testSummary();
};

#endif

#endif
