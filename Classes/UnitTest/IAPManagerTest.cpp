#ifdef ENABLE_TDD
//
//  IAPManagerTest.m
//	TDD Framework 
//
//
#include "IAPManagerTest.h"
#include "TDDHelper.h"
#include "IAPManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "IAPStatusCode.h"
#include "VisibleRect.h"
#include "CoinShopItemView.h"
#include "CoinShopScene.h"
#include "AdManager.h"

namespace {
	void showProductList() {
		log("%s", GameManager::instance()->getIAPManager()->infoGameProductList().c_str());
	}
}

void IAPManagerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void IAPManagerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void IAPManagerTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testGetMoneyDetail);
	ADD_TEST(testAddCandySuccess);
	ADD_TEST(testAddStarSuccess);
	ADD_TEST(testResetConsumable);
	ADD_TEST(testUpdateProductInfo);
	ADD_TEST(testPurchaseStar);
	ADD_TEST(testPurchaseNoAd);
	ADD_TEST(testLoadProducts);
	ADD_TEST(testActionAddCandy);
	ADD_TEST(testActionAddStar);
    ADD_TEST(testActionAddDiamond);
	ADD_TEST(testActionRemoveAd);
    ADD_TEST(testProductItemView);
    ADD_TEST(testCoinShopScene);
    ADD_TEST(testShowErrMsg);
    ADD_TEST(testShowAlert);
}

#pragma mark -
#pragma mark Sub Test Definition
void IAPManagerTest::subTest()
{
	log("this is a sample subTest");

	GameManager::instance()->getIAPManager()->setup();
  
}

void IAPManagerTest::testGetMoneyDetail()
{
	std::vector<std::string> productList;
	productList.push_back("starpack1");
	productList.push_back("candypack1");
	
	for(std::string name : productList) {
		GameProduct *product = GameManager::instance()->getIAPManager()->getGameProductByName(name);
		
		int amount;
		MoneyType type;
		
		product->getMoneyDetail(type, amount);
		
		log("product=%s type=%d value=%d", name.c_str(), type, amount);
	}
}

void IAPManagerTest::testAddCandySuccess()
{
	// Add some star
	GameManager::instance()->getUserData()->addCandy(500);
	
	//
	std::string prodName = "candypack1";
	GameProduct *product = GameManager::instance()->getIAPManager()->getGameProductByName(prodName);
	mShopLayer->handleGameProductSuccess(product);
}

void IAPManagerTest::testAddStarSuccess()
{
	// Add some star
	GameManager::instance()->getUserData()->addCoin(1000);
	
	//
	std::string prodName = "starpack1";
	GameProduct *product = GameManager::instance()->getIAPManager()->getGameProductByName(prodName);
	mShopLayer->handleGameProductSuccess(product);
}


void IAPManagerTest::testCoinShopScene()
{
 //   GameManager::instance()->getIAPManager()->setup();
    CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
    layer->setPosition(VisibleRect::leftBottom());
    
    auto button = Button::create();
    button->setColor(Color3B::GREEN);
    button->setTitleText("Reset No ads");
    
    button->addTouchEventListener([&](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getIAPManager()->resetNonConsumable("noad");
                break;
            }
        }
    });
    
    button->setPosition(Vec2(30,80));
    
    addChild(layer);
    addChild(button);
	
	mShopLayer = layer;
}

void IAPManagerTest::testProductItemView(){
    GameManager::instance()->getIAPManager()->setup();
    Vec2 pos = Vec2(VisibleRect::left());
    pos.x += 10;
    pos.y += 40;
    Vector<GameProduct*> gameProductArray = GameManager::instance()->getIAPManager()->getProductArray();
    for(int i=0;i<gameProductArray.size();i++){
        CoinShopItemView* itemView = CoinShopItemView::create();
        gameProductArray.at(i)->setIAPPrice("$ 0.88");
        itemView->setProduct(gameProductArray.at(i));
        itemView->setPosition(pos);
        addChild(itemView);
        pos.y-=80;
    }
}

void IAPManagerTest::testPurchaseStar()
{
	GameManager::instance()->getIAPManager()->setPurchaseCallback(
	  [](GameProduct *product, IAPStatusCode status, std::string msg)
	  {
		  log("product=%s", product ? product->toString().c_str() : "");
		  log("status=%d", status);
		  log("msg=%s", msg.c_str());
			  
		  log("playerCoin=%d", GameManager::instance()->getUserData()->getTotalCoin());
	  }
	);
	
	log("playerCoin=%d", GameManager::instance()->getUserData()->getTotalCoin());
	GameManager::instance()->getIAPManager()->purchase("starpack1");		// callback
	
}

void IAPManagerTest::testPurchaseNoAd()
{
	
	GameManager::instance()->getIAPManager()->setPurchaseCallback(
		  [](GameProduct *product, IAPStatusCode status, std::string msg)
		  {
			  // possible status
			  //			IAP_SUCCESS				: Display popup / update right-top corner coin value
			  //										 update the Ad own status
			  //			IAP_PURCHASE_CANCEL		: Nothing to do
			  //			Else (Error)			: Display popup with the msg
			  //
			  //			All cases: remove progress layer
			  
			  
			  log("product=%s", product ? product->toString().c_str() : "");
			  log("status=%d", status);
			  log("msg=%s", msg.c_str());
			  
			  log("showAd=%d", GameManager::instance()->shouldShowAd());
		  }
	  );
	
	log("showAd=%d", GameManager::instance()->shouldShowAd());
	GameManager::instance()->getIAPManager()->purchase("noad");		// callback
}

void IAPManagerTest::testActionAddCandy()
{
	int originValue = GameManager::instance()->getUserData()->getTotalCandy();
	
	
	IAPStatusCode status = GameManager::instance()->getIAPManager()->executionAction("addCandy:1000");
	
	int valueAfter = GameManager::instance()->getUserData()->getTotalCandy();
	
	int expectValue = originValue + 1000;
	
	log("expect=%d result=%d status=%d", expectValue, valueAfter, status);
}

void IAPManagerTest::testActionAddDiamond()
{
    int originValue = GameManager::instance()->getUserData()->getTotalDiamond();
    
    IAPStatusCode status = GameManager::instance()->getIAPManager()->executionAction("addDiamond:800");
    
    int valueAfter = GameManager::instance()->getUserData()->getTotalDiamond();
    
    int expectValue = originValue + 800;
    
    log("expect=%d result=%d status=%d", expectValue, valueAfter, status);
}


void IAPManagerTest::testActionAddStar()
{
	
	int originCoin = GameManager::instance()->getUserData()->getTotalCoin();
	
	
	IAPStatusCode status = GameManager::instance()->getIAPManager()->executionAction("addStar:1000");
	
	int coinAfterAdd = GameManager::instance()->getUserData()->getTotalCoin();
	
	int expectCoin = originCoin + 1000;
	
	log("expect=%d result=%d status=%d", expectCoin, coinAfterAdd, status);
}


void IAPManagerTest::testActionRemoveAd()
{
	AdManager::instance()->setNoAd(false);
	
	IAPStatusCode status = GameManager::instance()->getIAPManager()->executionAction("removeAd");
	
	bool result = GameManager::instance()->getUserData()->isNoAd();
	bool expect = true;
	
	log("expect=%d result=%d status=%d", expect, result, status);
}


void IAPManagerTest::testShowErrMsg(){
    CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
	
	layer->setPosition(VisibleRect::leftBottom());
    addChild(layer);
	
	layer->hideProgress();
    layer->showErrorMessage("Testing");
}

void IAPManagerTest::testShowAlert(){
    GameManager::instance()->getIAPManager()->setup();
    CoinShopSceneLayer *layer = CoinShopSceneLayer::create();
    layer->setPosition(VisibleRect::leftBottom());
    addChild(layer);
    layer->showAlert("Purchase Success", "100 star is added");
}


void IAPManagerTest::testLoadProducts()
{
	GameManager::instance()->getIAPManager()->loadProductList();
	
	showProductList();
}

void IAPManagerTest::testUpdateProductInfo()
{
	// Load from JSON (
//	GameManager::instance()->getIAPManager()->loadProductList();
	
	
	GameManager::instance()->getIAPManager()->setUpdateProductCallback(
						[](IAPStatusCode status, std::string msg) {
							log("status=%d msg=%s", status, msg.c_str());
							
							// possible status = SUCCESS | status != SUCCESS
							showProductList();
					   }
					);
	
	// Start update
	GameManager::instance()->getIAPManager()->updateProductInfo();
	
	
}

void IAPManagerTest::testResetConsumable()
{
	GameManager::instance()->getIAPManager()->resetNonConsumable("noad");
}

#endif
