 #ifdef ENABLE_TDD
//
//  HttpTest.m
//	TDD Framework
// Sample Code:
//		cpp-test/Classes/ExtensionsTest/NetworkTest/HttpClientTest
//
//
#include "HttpTest.h"
#include "TDDHelper.h"

void HttpTest::setUp()
{
	log("TDD Setup is called");
}


void HttpTest::tearDown()
{
	log("TDD tearDown is called");
}

void HttpTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void HttpTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void HttpTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void HttpTest::testSample()
{
	log("this is a sample subTest");
	
	HttpRequest* request = new (std::nothrow) HttpRequest();
	
	// required fields
	request->setUrl("http://httpbin.org/ip");
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(CC_CALLBACK_2(HttpTest::onHttpRequestCompleted, this));
	request->setTag("GET immediate test2");
	HttpClient::getInstance()->sendImmediate(request);
	
	// don't forget to release it, pair to new
	request->release();
}

void HttpTest::onHttpRequestCompleted(HttpClient *sender, HttpResponse *response)
{
	if (!response)
	{
		return;
	}
	
	log("response returned status=%ld", response->getResponseCode());

	std::vector<char> *buffer = response->getResponseData();
	std::string result = "";
	for (unsigned int i = 0; i < buffer->size(); i++)
	{
		
		result += (*buffer)[i];
//		log("%c", (*buffer)[i]);
	}
	
	log("Result: %s\n", result.c_str());
}

#endif
