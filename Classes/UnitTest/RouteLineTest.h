#ifdef ENABLE_TDD
//
//  RouteLineTest.h
//
//
#ifndef __TDD_RouteLineTest__
#define __TDD_RouteLineTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class RouteLineTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testRotationOnLayout();
	void testSample();
	void testGraphicLine();
	void testUpdateLineBySprite();
	void testDifferentAngle();
	void testDifferentLength();
	void testRotation();
	void testAnimation();
};

#endif

#endif
