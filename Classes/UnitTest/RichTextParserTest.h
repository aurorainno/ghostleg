#ifdef ENABLE_TDD
//
//  RichTextParserTest.h
//
//
#ifndef __TDD_RichTextParserTest__
#define __TDD_RichTextParserTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class RichTextParserTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
};

#endif

#endif
