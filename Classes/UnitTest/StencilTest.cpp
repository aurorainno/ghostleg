#ifdef ENABLE_TDD
//
//  StencilTest.m
//	TDD Framework
//
//
#include "StencilTest.h"
#include "TDDHelper.h"
#include "VisibleRect.h"
void StencilTest::setUp()
{
	log("TDD Setup is called");
}


void StencilTest::tearDown()
{
	log("TDD tearDown is called");
}

void StencilTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void StencilTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


void StencilTest::drawPie(DrawNode *node, const Vec2 &origin, float radius, float angle)
{
	int num_of_points = 15;
	float borderWidth = 1;
	Color4F fillColor = Color4F::BLUE;
	Color4F borderColor = Color4F::BLUE;
	
	const cocos2d::Vec2 start = origin + Vec2(radius, 0);
	const auto angle_step = 2 * M_PI * angle / 360.f / num_of_points;
	
	std::vector<cocos2d::Point> circle;
	
	circle.emplace_back(origin);
	for (int i = 0; i <= num_of_points; i++)
	{
		auto rads = angle_step * i;
		auto x = origin.x + radius * cosf(rads);
		auto y = origin.y + radius * sinf(rads);
	
		circle.emplace_back(x, y);
	}
	
	node->drawPolygon(circle.data(), circle.size(), fillColor, borderWidth, borderColor);
}


//drawCircularSector(cocos2d::DrawNode* node, cocos2d::Vec2 origin, float radius, float angle_degree,
//				   cocos2d::Color4F fillColor, float borderWidth, cocos2d::Color4F bordercolor,int num_of_points)
//{
//	
//	if (!node){return;}
//	node->clear();
//	
//	
//	
//	const cocos2d::Vec2 start = origin + cocos2d::Vec2{radius, 0};
//	const auto angle_step = 2 * M_PI * angle_degree / 360.f / num_of_points;
//	std::vector<cocos2d::Point> circle;
//	
//	circle.emplace_back(origin);
//	for (int i = 0; i <= num_of_points; i++)
//	{
//		auto rads = angle_step * i;
//		auto x = origin.x + radius * cosf(rads);
//		auto y = origin.y + radius * sinf(rads);
//		circle.emplace_back(x, y);
//	}
//	
//	node->drawPolygon(circle.data(), circle.size(), fillColor, borderWidth, bordercolor);
//}

#pragma mark -
#pragma mark List of Sub Tests

void StencilTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testDrawPie);
}

#pragma mark -
#pragma mark Sub Test Definition
void StencilTest::testSample()
{
	log("this is a sample subTest");
	
	DrawNode *stencil = DrawNode::create();
	stencil->clear();
	drawPie(stencil, Vec2::ZERO, 50, 90);
	
	ClippingNode *clipNode = ClippingNode::create();
	clipNode->setPosition(VisibleRect::center());
	clipNode->setStencil(stencil);
	addChild(clipNode);
	
	Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");
	clipNode->addChild(sprite);
	
//	Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");
//	
//	sprite->setPosition(VisibleRect::center());
//	sprite->setStencil(stencil);
//	addChild(sprite);
}

void StencilTest::testDrawPie()
{
	DrawNode *node = DrawNode::create();
	
	node->setPosition(VisibleRect::center());
	
	drawPie(node, Vec2::ZERO, 50, 90);
	
	addChild(node);
	
}

#endif
