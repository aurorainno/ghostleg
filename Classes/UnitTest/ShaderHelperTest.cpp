#ifdef ENABLE_TDD
//
//  ShaderHelperTest.m
//	TDD Framework 
//
//
#include "ShaderHelperTest.h"
#include "TDDHelper.h"
#include "ShaderHelper.h"

namespace
{
	DrawNode *createDrawNode(const Vec2 &start, const Vec2 &end, float size, Color4F color)
	{
		
		DrawNode *drawNode = DrawNode::create();
		
		drawNode->drawSegment(start, end, size, color);
		
		return drawNode;
	}
}


void ShaderHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void ShaderHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ShaderHelperTest::defineTests()
{
	ADD_TEST(testNormalNode);
	ADD_TEST(testShaderNode);
}


#pragma mark -
#pragma mark Sub Test Definition
void ShaderHelperTest::testNormalNode()
{
	log("this is a sample subTest");
	
	DrawNode *drawNode = createDrawNode(Vec2(0, 0), Vec2(100, 100), 3, Color4F::RED);
	
	drawNode->setPosition(Vec2(100, 100));
	
	addChild(drawNode);
}

void ShaderHelperTest::testShaderNode()
{
	log("testShaderNode");
	
	DrawNode *drawNode;
 
	
	drawNode = createDrawNode(Vec2(0, 0), Vec2(0, 100), 3, Color4F::RED);
	drawNode->setPosition(Vec2(100, 100));
	addChild(drawNode);
	//aurora::ShaderHelper::setShaderToDrawNode(drawNode, "", "shader/dash_line.fsh");
	aurora::ShaderHelper::setShaderToDrawNode(drawNode, "shader/dash.vsh", "shader/dash.fsh");
	
	drawNode = createDrawNode(Vec2(0, 0), Vec2(0, 100), 3, Color4F::RED);
	drawNode->setPosition(Vec2(105, 90));
	addChild(drawNode);
	//aurora::ShaderHelper::setShaderToDrawNode(drawNode, "", "shader/dash_line.fsh");
	aurora::ShaderHelper::setShaderToDrawNode(drawNode, "shader/dash.vsh", "shader/dash.fsh");
	
	
	drawNode = createDrawNode(Vec2(0, 0), Vec2(0, 200), 3, Color4F::BLUE);
	drawNode->setPosition(Vec2(200, 70));
	addChild(drawNode);
	aurora::ShaderHelper::setShaderToDrawNode(drawNode, "", "shader/dash_line.fsh");

	
	// Horizontal line
	drawNode = createDrawNode(Vec2(0, 0), Vec2(200, 0), 3, Color4F::YELLOW);
	drawNode->setPosition(Vec2(50, 50));
	addChild(drawNode);
	aurora::ShaderHelper::setShaderToDrawNode(drawNode, "", "shader/dash_h_line.fsh");

	
	// Blink line
	drawNode = createDrawNode(Vec2(0, 0), Vec2(100, 100), 3, Color4F::GREEN);
	drawNode->setPosition(Vec2(50, 200));
	addChild(drawNode);
	aurora::ShaderHelper::setShaderToDrawNode(drawNode, "", "shader/blink_line.fsh");

}


#endif
