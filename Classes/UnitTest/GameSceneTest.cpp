#ifdef ENABLE_TDD
//
//  GameSceneTest.m
//	TDD Framework 
//
//
#include "GameSceneTest.h"
#include "TDDHelper.h"
#include "GameWorld.h"
#include "GameScene.h"
#include "ItemEffect.h"
#include "NPCOrder.h"
#include "NPCRating.h"
#include "StageManager.h"
#include "NpcDistanceLayer.h"
#include "VisibleRect.h"

void GameSceneTest::setUp()
{
	
	
	log("TDD Setup is called");
	log("Please write somethings");
	mGameLayer = nullptr;
	mIsPaused = false;
	
	testGameLayer();
}


void GameSceneTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	
}

#pragma mark -
#pragma mark List of Sub Tests

void GameSceneTest::defineTests()
{
	ADD_TEST(testAddCoinWithAnime);
	ADD_TEST(testAddScoreWithAnime);
	ADD_TEST(testCheckPointDialog);
	ADD_TEST(testShowPuzzleCount);
	ADD_TEST(testShowNpcIndicator);
	ADD_TEST(testShowTimeExtend);
	ADD_TEST(testStartCountDown);
	ADD_TEST(testPauseCountDown);
	ADD_TEST(testStopCountDown);
	ADD_TEST(testTopGUI);
	ADD_TEST(testGameLayer);
	ADD_TEST(testGameOver);
	ADD_TEST(testSetTutorialUI);
	ADD_TEST(testHideTapHint);
	ADD_TEST(testCapsuleButton);
	ADD_TEST(testEnergyParticle);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameSceneTest::testAddCoinWithAnime()
{
	GameWorld::instance()->addCoinWithAnimation(100, VisibleRect::center());
	
	
	std::vector<int> coinList;
	coinList.push_back(10);
	coinList.push_back(20);
	coinList.push_back(30);
	coinList.push_back(40);
	
	GameWorld::instance()->addMultipleCoinWithAnimation(coinList, Vec2(100, 100), 0.4f);
}

void GameSceneTest::testAddScoreWithAnime()
{
	GameWorld::instance()->addScoreWithAnimation(1234, ScoreTypeDelivery);
}

void GameSceneTest::testCheckPointDialog()
{
	if(mGameLayer == nullptr) {
		return;
	}
	
	NPCRating *rating = NPCRating::create();
	
	rating->setScore(1000);
	rating->setTimeUsed(12.33);
	rating->setRewardValue(100, 10);
	
	mGameLayer->showCheckPointDialog(rating);
	

}


void GameSceneTest::testShowPuzzleCount()
{
	if(mGameLayer == nullptr) {
		return;
	}
	static int counter = 1;
	
	mGameLayer->updatePuzzleCount(counter, true);
	
	
	counter++;
}


// void GameSceneTest::
void GameSceneTest::testShowNpcIndicator()
{
	if(mGameLayer == nullptr) {
		return;
	}
	
	NpcDistanceLayer *layer = mGameLayer->getNpcDistanceLayer();
	layer->setupDots(5);
	
	
	NPCOrder *npcOrder = StageManager::instance()->getNpcOrder(101);
	npcOrder->setOrderIndex(2);
	
	mGameLayer->showNpcDistance(npcOrder);
	
}

void GameSceneTest::testShowTimeExtend()
{
	if(mGameLayer == nullptr) {
		return;
	}
	mGameLayer->showTimeExtended(10);
}


void GameSceneTest::testStartCountDown()
{
	if(mGameLayer == nullptr) {
		return;
	}
	mGameLayer->startClockCountDownEffect();
}

void GameSceneTest::testStopCountDown()
{
	if(mGameLayer == nullptr) {
		return;
	}
	mGameLayer->stopClockCountDownEffect();
}


void GameSceneTest::testPauseCountDown()		// Also resume
{
	if(mGameLayer == nullptr) {
		return;
	}
	if(mIsPaused) {
		mGameLayer->pauseClockCountDownEffect();
		mIsPaused = true;
	} else {
		mGameLayer->resumeClockCountDownEffect();
		mIsPaused = false;
	}
}


void GameSceneTest::testGameOver()
{
	if(mGameLayer == nullptr) {
		return;
	}
	
	//GameWorld::instance()->updatePlayerRecord();

	mGameLayer->handleWorldEvent(nullptr, GameWorldEvent::EventGameOver, 0);
}


void GameSceneTest::testSetTutorialUI()
{
	GameSceneLayer *layer = GameSceneLayer::create();
	layer->setStartOnEnter(false);
	addChild(layer);
	mGameLayer = layer;

	
//	createTestButton("TurnOn", Vec2(50, 100), [&](Ref *) {
//		mGameLayer->setTutorialUI(true);
//	});
//	
//	createTestButton("TurnOff", Vec2(250, 100), [&](Ref *) {
//		mGameLayer->setTutorialUI(false);
//	});

	
	// playButton->addClickEventListener(callback);
}



void GameSceneTest::testEnergyParticle()
{
	GameSceneLayer *layer = GameSceneLayer::create();
	layer->setStartOnEnter(false);
	addChild(layer);
	mGameLayer = layer;
	
	ui::Widget::ccWidgetClickCallback callback = [&](Ref *) {
		mGameLayer->playEnergyParticle();
	};
//	
//	Button *playButton = createTestButton("play", Vec2(50, 50), callback);
//	playButton->addClickEventListener(callback);
}

void GameSceneTest::testCapsuleButton()
{
	GameSceneLayer *layer = GameSceneLayer::create();
	
	layer->setStartOnEnter(false);
	
	addChild(layer);
	
	mGameLayer = layer;
	
	mGameLayer->enableCapsuleButton(ItemEffect::Type::ItemEffectTimeStop);
}

void GameSceneTest::testGameLayer()
{
	if(mGameLayer != nullptr){
		mGameLayer->removeFromParent();
		mGameLayer = nullptr;
	}
	
	GameSceneLayer *layer = GameSceneLayer::create();
	
	layer->setStartOnEnter(false);
	
	addChild(layer);

	mGameLayer = layer;
	
	GameWorld::instance()->setGameUILayer(mGameLayer);
}

void GameSceneTest::testHideTapHint()
{
	if(mGameLayer) {
		mGameLayer->hideTapHint();
	}
}


void GameSceneTest::testTopGUI()
{
	if(mGameLayer) {
		NPCOrder *npcOrder = StageManager::instance()->getNpcOrder(101);
		mGameLayer->setNpcInfo(npcOrder);
		//mGameLayer->setRemainDistance(1000);
	//	mGameLayer->hideTapHint();
	}
}


#endif
