#ifdef ENABLE_TDD
//
//  UIBestDistanceTest.m
//	TDD Framework
//
//
#include "UIBestDistanceTest.h"
#include "TDDHelper.h"
#include "UIBestDistance.h"

void UIBestDistanceTest::setUp()
{
	log("TDD Setup is called");
}


void UIBestDistanceTest::tearDown()
{
	log("TDD tearDown is called");
}

void UIBestDistanceTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void UIBestDistanceTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void UIBestDistanceTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void UIBestDistanceTest::testSample()
{
	log("this is a sample subTest");
    UIBestDistance* ui = UIBestDistance::create();
    ui->setScale(0.6);
    ui->setPosition(Vec2(getContentSize().width/2,getContentSize().height/2));
    ui->runAnimation();
    addChild(ui);
}


#endif
