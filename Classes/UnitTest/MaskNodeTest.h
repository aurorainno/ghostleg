#ifdef ENABLE_TDD
//
//  MaskNodeTest.h
//
//
#ifndef __TDD_MaskNodeTest__
#define __TDD_MaskNodeTest__

// Include Header

#include "TDDBaseTest.h"

class CircularMaskNode;

// Class Declaration
class MaskNodeTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testAddAngle();

private:
	CircularMaskNode *mMaskNode;
};

#endif

#endif
