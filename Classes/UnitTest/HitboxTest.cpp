#ifdef ENABLE_TDD
//
//  HitboxTest.m
//	TDD Framework
//
//
#include "HitboxTest.h"
#include "TDDHelper.h"
#include "Enemy.h"
#include "EnemyFactory.h"
#include "VisibleRect.h"
#include "StringHelper.h"
#include "Player.h"
#include "GameModel.h"
#include "ui/CocosGUI.h"
#include "Star.h"


Enemy *HitboxTest::createEnemy(int enemyID)
{
	Enemy *enemy = EnemyFactory::instance()->createEnemy(enemyID);
	if(enemy) {
		enemy->setPosition(VisibleRect::center());
		enemy->update(0);
		enemy->changeToActive();
	}
	return enemy;
}

void HitboxTest::setUp()
{
	log("TDD Setup is called");
	mMainLayer = Layer::create();
	addChild(mMainLayer);
	
	mHitBoxLayer = Layer::create();
	addChild(mHitBoxLayer);
	
	setupModels();
	
	// Touch Handling
	setupTouchListener();
}

void HitboxTest::setupPlayer()
{
	// Player
	Player *player = Player::create();
	player->reset();
	player->setPosition(VisibleRect::center());
	mMainLayer->addChild(player);
	mPlayer = player;
	
}

void HitboxTest::setupItem()
{
	
	// Star
    mStar = Star::create(Star::SmallStar);
	mStar->setPosition(VisibleRect::center() + Vec2(50, 50));
	mMainLayer->addChild(mStar);
}

void HitboxTest::setupModels()
{
	mMainLayer->removeAllChildren();
	mEnemyList.clear();
	
	//int enemyID = 118;
	//int enemyID = 510;
	//int enemyID = 112;
	//int enemyID = 201;
	//int enemyID = 41;	// Laser Enemy
	//int enemyID = 515;	// Moving Saw
	//int enemyID = 51;
	int enemyID = 1;
	
	Enemy *enemy = createEnemy(enemyID);
	enemy->setPosition(Vec2(150, 200));
	mMainLayer->addChild(enemy);
	mEnemyList.push_back(enemy);

	//mEnemy->setupScaleAndFlip(1.0, 1.0, true, true);
	//mEnemy->setupScaleAndFlip(0.5, 0.5, false, false);
	//enemy->setupScaleAndFlip(0.5, 0.5, false, false);
	enemy->setupHitboxes();
	//mEnemy->setHitboxVisible(true);
	//mEnemy->
	enemy->setAction(GameModel::Action::Attack);
	//mEnemy->setAction(GameModel::Action::Attack);
	enemy->pause();
	mEnemy = enemy;
	
	setupPlayer();
	setupItem();
}

void HitboxTest::setupTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(HitboxTest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(HitboxTest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(HitboxTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(HitboxTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}

bool HitboxTest::onTouchBegan(Touch *touch, Event *event)
{
	mLastPos = touch->getLocation();
	
	return true;
}

void HitboxTest::onTouchEnded(Touch *touch, Event *event)
{
}

void HitboxTest::onTouchMoved(Touch *touch, Event *event)
{
	Vec2 newPos = touch->getLocation();
	
	
	
	Vec2 diff = newPos - mLastPos;
	mLastPos = newPos;
	
	mPlayer->setFace(diff.x < 0);
	
	// Move Player by diff
	Vec2 oldPos = mPlayer->getPosition();
	mPlayer->setPosition(oldPos + diff);
	
	// Hitbox test
	bool flag;

	for(Enemy *enemy : mEnemyList) {
		flag = mPlayer->isCollideModel(enemy);
		log("player hit enemy: %d", flag);
	}
	
	flag = mPlayer->isCollideModel(mStar);
	log("player hit star: %d", flag);
}

void HitboxTest::onTouchCancelled(Touch *touch, Event *event)
{
	
}



void HitboxTest::tearDown()
{
	log("TDD tearDown is called");
}

void HitboxTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void HitboxTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}

void HitboxTest::highlightHitBox(GameModel *model)
{
	mHitBoxLayer->removeAllChildren();
	
	Rect rect = mEnemy->getBoundingBox();
	LayerColor *colorBox = LayerColor::create(Color4B::RED,
											  rect.size.width, rect.size.height);
	colorBox->setPosition(rect.origin);
	mHitBoxLayer->addChild(colorBox);
}

void HitboxTest::highlightRect(Rect &rect)
{
	LayerColor *colorBox = LayerColor::create(Color4B::GREEN,
									rect.size.width, rect.size.height);
	colorBox->setPosition(rect.origin);
	mHitBoxLayer->addChild(colorBox);
}

void HitboxTest::highlightCircle(Circle &circle)
{
	// Using Square
//	LayerColor *colorBox = LayerColor::create(Color4B::YELLOW,
//											  circle.radius *2, circle.radius*2);
//	colorBox->setPosition(circle.origin);
//	mHitBoxLayer->addChild(colorBox);
	
	
	// Using Circle
	DrawNode *drawNode = DrawNode::create();
	
	
	drawNode->drawCircle(circle.origin, circle.radius, 0, 360, true, 1.0f, 1.0f, Color4F::YELLOW);
	mHitBoxLayer->addChild(drawNode);
}



#pragma mark -
#pragma mark List of Sub Tests

void HitboxTest::defineTests()
{
	ADD_TEST(testDifferentHitbox);
	ADD_TEST(moveLayer);
	ADD_TEST(moveEnemy);
	ADD_TEST(clearHitbox);
	ADD_TEST(testFourFlip);
	ADD_TEST(showHitbox);
	ADD_TEST(hideHitbox);
	ADD_TEST(stepEnemy);
	ADD_TEST(checkHitBox);
	ADD_TEST(testHitboxInfo);
}

#pragma mark -
#pragma mark Sub Test Definition
void HitboxTest::testHitboxInfo()
{
	Player *player = Player::create();
	player->reset();
	
	
	log("Collider Info:\n%s", player->infoColliders().c_str());

}

void HitboxTest::moveEnemy()
{
	if(mEnemy == nullptr) {
		return;
	}
	
	Vec2 pos = mEnemy->getPosition();
	pos.y += 30;
	
	mEnemy->setPosition(pos);
}

void HitboxTest::moveLayer()
{
	log("this is a sample subTest");
	
	Vec2 pos = mMainLayer->getPosition();
	pos.y += 50;
	
	mMainLayer->setPosition(pos);
	//mHitBoxLayer->setPosition(pos);
	
	//Enemy *HitboxTest::createEnemy(int enemyID)
}

void HitboxTest::checkHitBox()
{
	
	mHitBoxLayer->removeAllChildren();
	

	//Rect mainBox = mEnemy->getMainBoundingBox();
	//highlightRect(mainBox);
	
	std::vector<GameModel *> modelList;
	modelList.push_back(mStar);
	modelList.push_back(mPlayer);
	
	for(Enemy *enemy : mEnemyList) {
		modelList.push_back(enemy);
	}
	
	int index = 0;
	for(GameModel *model : modelList) {
		//log("DEBUG>>> %s" + model->toString().c_str());
		log("DEBUG>> model-%d (%s)", (index++), model->getName().c_str());
		// BOX Hitbox
		std::vector<Rect> hitboxList;
		model->getHitboxRect(hitboxList);
		for(Rect box : hitboxList) {
			log("RECT: %s", RECT_TO_STR(box).c_str());
			highlightRect(box);
		}

		// Circle Hitbox
		std::vector<Circle> circleList;
		model->getHitCircle(circleList);
		for(Circle circle : circleList) {
			log("CIRCLE: %s", circle.toString().c_str());
			highlightCircle(circle);
		}

	}

	
}

void HitboxTest::clearHitbox()
{
	mHitBoxLayer->removeAllChildren();
}

void HitboxTest::stepEnemy()
{
	for(Enemy *enemy : mEnemyList) {
		enemy->stepAction();
	}
	
}

void HitboxTest::showHitbox()
{
	GameModel::showHitbox(true);
	setupModels();
}

void HitboxTest::hideHitbox()
{
	GameModel::showHitbox(false);
	setupModels();
}

void HitboxTest::testFourFlip()
{
	mMainLayer->removeAllChildren();
	mEnemyList.clear();
	
	//int enemyID = 118;
	//int enemyID = 510;
	int enemyID = 115;
	//int enemyID = 201;
	//int enemyID = 112;
	
	bool flipx[4] = {false, false, true, true};
	bool flipy[4] = {false, true, false, true};
	
	//
	Vec2 pos = Vec2(150, 100);
	float scale = 0.4;
	
	for(int i=0; i<4; i++) {
		Enemy *enemy = createEnemy(enemyID);
		enemy->setPosition(pos);
		mMainLayer->addChild(enemy);
		mEnemyList.push_back(enemy);
		
		enemy->setupScaleAndFlip(scale, scale, flipx[i], flipy[i]);
		enemy->setupHitboxes();
		enemy->setAction(GameModel::Action::Walk);
		
		pos.y += 100;
	}
	
	

	setupPlayer();
	setupItem();
}


void HitboxTest::testDifferentHitbox()
{
	///
	std::vector<int> enemyList;
	enemyList.push_back(1);				// general alien
	enemyList.push_back(41);			// laser alien
	enemyList.push_back(200);
	
	bool flipX = true;
	bool flipY = true;
	float scaleX = 0.5;
	float scaleY = 0.2;
	
	for(int enemyID : enemyList) {
		Enemy *enemy = createEnemy(enemyID);
		if(enemyID > 100) {
			enemy->setupScaleAndFlip(scaleX, scaleY, flipX, flipY);
		}
		enemy->setupHitboxes();
		
		
		log("enemyID=%d hitbox=%s", enemyID, enemy->infoHitbox().c_str());
	}
	
}

#endif
