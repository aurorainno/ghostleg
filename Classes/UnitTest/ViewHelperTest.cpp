#ifdef ENABLE_TDD
//
//  ViewHelperTest.m
//	TDD Framework 
//
//
#include "ViewHelperTest.h"
#include "TDDHelper.h"
#include "ViewHelper.h"
#include "VisibleRect.h"
#include "ShaderHelper.h"

void ViewHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void ViewHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ViewHelperTest::defineTests()
{
	ADD_TEST(testAlert);
	ADD_TEST(testOutlineText);
	ADD_TEST(testDrawLine);
	ADD_TEST(testCreateAnime);
	ADD_TEST(testLoopAnime);
	ADD_TEST(testBearingCoordinate);
	ADD_TEST(testDrawPie);
	ADD_TEST(testMaskTexture);
}

#pragma mark -
#pragma mark Sub Test Definition
void ViewHelperTest::testAlert()
{
//	void ViewHelper::showFadeAlert(Node *parent, const std::string &msg, int y,
//								   int fontSize,
//								   const Color4B &color)
	ViewHelper::showFadeAlert(this, "testing", 450);
}

void ViewHelperTest::testOutlineText()
{
	Vec2 pos = VisibleRect::center();
	pos.y = 100;
	
	for(int fontSize=10; fontSize <= 30; fontSize +=5) {
		std::string str = StringUtils::format("Font %d", fontSize);
		ui::Text *text = ViewHelper::createOutlineText(str, fontSize,
								Color3B::WHITE, 2, Color3B::BLACK);
	
		text->setPosition(pos);
	
		addChild(text);
	
		pos.y += 2 * fontSize;
	}


}


void ViewHelperTest::testDrawLine()
{
	
	//	static void drawLine(DrawNode *drawNode, const Vec2 &start, const Vec2 &end,
	//						 float thickness, const Color4F &color);
	

	DrawNode *node = DrawNode::create();
	//node->setPosition(Vec2::ZERO);
	node->setPosition(Vec2::ZERO);
	addChild(node);
	
	Vec2 start = Vec2::ZERO;
	Vec2 end = VisibleRect::rightTop();
	Color4F color = Color4F::BLUE;
	float thickness = 0.1;
	
	ViewHelper::drawLine(node, start, end, thickness, color);
	
	
	
	DrawNode *dotLine = DrawNode::create();
	dotLine->setPosition(Vec2::ZERO);
	addChild(dotLine);
	
	start = VisibleRect::leftTop();
	end = VisibleRect::rightBottom();
	color = Color4F::RED;
	thickness = 0.5;
	
	aurora::ShaderHelper::setShaderToDrawNode(dotLine, "shader/dash.vsh", "shader/dash.fsh");
	ViewHelper::drawLine(dotLine, start, end, thickness, color);
}


void ViewHelperTest::testDrawPie()
{
//	void ViewHelper::drawPie(DrawNode *node, float radius,
//							 float startDegree, float endDegree,
//							 const Color4F &color,
//							 const Vec2 &offset)
	DrawNode *node = DrawNode::create();
	node->setPosition(VisibleRect::center());
	
	//ViewHelper::drawPie(node, 50, 0, 45, Color4F::RED);
	//ViewHelper::drawPie(node, 50, 40, 90, Color4F::RED);
	ViewHelper::drawPie(node, 50, 0, 3.6, Color4F::RED);
	
	addChild(node);
	
	
	

}


void ViewHelperTest::testBearingCoordinate()
{
	float testAngle[8] = {
			0, 45, 135, 225, 315, 360,
			60, 150,
	};
	
	for(float degree : testAngle)
	{
		Vec2 result = ViewHelper::convertBearingCoordinate(1.0f, degree);
		
		log("degree=%f x=%f y=%f", degree, result.x, result.y);
	}
}

void ViewHelperTest::testCreateAnime()
{
	log("this is a sample subTest");
	
	
	Node *node = ViewHelper::createAnimationNode("tutorial_dialog2.csb", "active", false, true);
	//Node *node = ViewHelper::createAnimationNode("tutorial_dialog5.csb", "active", false, true);
	
	node->setScale(0.5f);
	node->setPosition(Vec2(0, 0));
	//node->setPosition(VisibleRect::center());

	addChild(node);
}


void ViewHelperTest::testLoopAnime()
{
	log("this is a sample subTest");
	
	
	Node *node = ViewHelper::createAnimationNode("tutorial_dialog2.csb", "active", true, false);
	
	node->setPosition(VisibleRect::center());
	
	addChild(node);
}

void ViewHelperTest::testMaskTexture()
{
	Sprite *sprite = Sprite::create("guiImage/ability_bg_06.png");
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);
	
	Vec2 maskPos = VisibleRect::center() - Vec2(50, 100);
	
	//
	Texture2D *maskTexture = ViewHelper::getMaskTexture(
									VisibleRect::getVisibleRect().size,
										0.3f, 40, maskPos);
	Sprite *maskSprite = Sprite::createWithTexture(maskTexture);
	maskSprite->setAnchorPoint(Vec2::ZERO);
	maskSprite->setFlippedY(true);
	maskSprite->setBlendFunc(BlendFunc{GL_ONE_MINUS_SRC_ALPHA, GL_SRC_COLOR});

	addChild(maskSprite);
	
}

#endif
