#ifdef ENABLE_TDD
//
//  OptimizeTest.h
//
//
#ifndef __TDD_OptimizeTest__
#define __TDD_OptimizeTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class OptimizeTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
};

#endif

#endif
