#ifdef ENABLE_TDD
//
//  DebugMapTest.m
//	TDD Framework 
//
//
#include "DebugMapTest.h"
#include "TDDHelper.h"
#include "LevelData.h"
#include "GameManager.h"

void DebugMapTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	hideMenu();
}


void DebugMapTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

void DebugMapTest::onEnter()
{
	TDDTest::onEnter();
	
	hideMenu();
}

#pragma mark -
#pragma mark List of Sub Tests

void DebugMapTest::setSubTest(Vector<MenuItem *> &menuArray)
{
//	SUBTEST(DebugMapTest::subTest);

	ccMenuCallback callback = CC_CALLBACK_1(DebugMapTest::changeDebugMap, this);

	std::vector<int> maps = MapLevelDataCache::instance()->getAvailableMaps();
	std::sort(maps.begin(), maps.end());   // Sort in
	
	Vec2 zero = Vec2(0, 0);
	Vec2 top = Director::getInstance()->convertToGL(zero);
	top.y -= 50;
	
	const int numCol = 5;
	float column[numCol] = {0, 64, 128, 192, 256};
	
	
	float spacing = 20;
	
//	auto menuItem = createMenuItemWithFont(name, TDD_FONT_NAME, Color3B::WHITE, callback);
//	Menu *menu = Menu::createWithArray(<#const Vector<cocos2d::MenuItem *> &arrayOfItems#>)
//	menu->setPosition(top);
	
	Vector<MenuItem *> menuItems;

	ccMenuCallback resetCallback = CC_CALLBACK_1(DebugMapTest::resetDebugMap, this);
	MenuItem *menuItem = TDDHelper::createMenuItem("Reset DebugMap", resetCallback);
	menuItem->setPosition(Vec2(160, 0));
	menuItems.pushBack(menuItem);
	
	float y = -spacing;

	
	for(int i=0; i<maps.size(); i++) {
		int mapID = maps[i];
		log("mapID=%d", mapID);
		
		char tempstr[50];
		sprintf(tempstr, "%05d", mapID);
		
		MenuItem *menuItem = TDDHelper::createMenuItem(tempstr, callback);
		
		float x = column[i % numCol] + 32;
		
		menuItem->setPosition(Vec2(x, y));
		menuItem->setEnabled(true);
		
		if(i % numCol == numCol-1) {
			y -= spacing;
		}
		
		menuItem->setTag(mapID);
		
		menuItems.pushBack(menuItem);
	}
	
	Menu *menu = Menu::createWithArray(menuItems);
	
	menu->setPosition(top);
	addChild(menu);
	
	
	
	hideMenu();
}

#pragma mark -
#pragma mark Sub Test Definition
void DebugMapTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
}

void DebugMapTest::changeDebugMap(Ref *sender)
{
	MenuItem *menuItem = (MenuItem *)sender;
	int selectedMap = menuItem->getTag();
	
	log("selected map: %d", selectedMap);
	GameManager::instance()->setDebugMap(selectedMap);
}


void DebugMapTest::resetDebugMap(Ref *sender)
{
	GameManager::instance()->resetDebugMap();
}


#endif
