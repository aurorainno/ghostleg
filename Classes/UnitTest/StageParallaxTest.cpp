#ifdef ENABLE_TDD
//
//  StageParallaxTest.m
//	TDD Framework
//
//
#include "StageParallaxTest.h"
#include "TDDHelper.h"
#include "StageParallaxLayer.h"

void StageParallaxTest::setUp()
{
	log("TDD Setup is called");
}


void StageParallaxTest::tearDown()
{
	log("TDD tearDown is called");
}

void StageParallaxTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void StageParallaxTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void StageParallaxTest::defineTests()
{
	ADD_TEST(testShake);
	ADD_TEST(testSample);
	ADD_TEST(testHideRoute);
	
}

#pragma mark -
#pragma mark Sub Test Definition
void StageParallaxTest::testSample()
{
	log("this is a sample subTest");
	StageParallaxLayer *layer = StageParallaxLayer::create();
	
	layer->setStage(5);
	
	addChild(layer);
	
	mParallax = layer;

}

//void StageParallaxTest::showWindEffect()
//{
//	if(mParallax == nullptr) {
//		return;
//	}
//	
//	mParallax->showWindEffect();
//}
//
//
//void StageParallaxTest::hideWindEffect()
//{
//	if(mParallax == nullptr) {
//		return;
//	}
//	
//	mParallax->hideWindEffect();
//}


void StageParallaxTest::testShake()
{
	if(mParallax == nullptr) {
		return;
	}
	
	mParallax->shake(0.3f);
}

void StageParallaxTest::testHideRoute()
{
	log("this is a sample subTest");
	StageParallaxLayer *layer = StageParallaxLayer::create();
	layer->setShowRoute(false);
	layer->setStage(5);
	
	addChild(layer);
	
	mParallax = layer;
	
}


#endif
