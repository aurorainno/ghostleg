#ifdef ENABLE_TDD
//
//  TutorialTest.m
//	TDD Framework 
//
//
#include "TutorialTest.h"
#include "TDDHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "TutorialLayer.h"
#include "TutorialManager.h"
#include "VisibleRect.h"
#include "StringHelper.h"
#include "GameScene.h"

void TutorialTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	TutorialManager::instance()->loadTutorialData();
//	
//	//
//	GameWorld *world = GameWorld::instance();
//	
//	world->setShowTutorial(true);
//	
//	world->resetGame();
//	world->getModelLayer()->update(0);
//	addChild(world);
//	world->setUserTouchEnable(false);
//	
	TutorialLayer *tutorialLayer = TutorialLayer::create();
	addChild(tutorialLayer);
	mTutorialLayer = tutorialLayer;
}


void TutorialTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void TutorialTest::defineTests()
{
	ADD_TEST(tutorialScene);
	ADD_TEST(testCanSlopeLineEnd);
	ADD_TEST(testCanSlopeLineStart);
	ADD_TEST(testShowCsb);
	ADD_TEST(testShowDialog);
	ADD_TEST(testCloseDialog);
	ADD_TEST(testDialog);
	ADD_TEST(testTutorial);
	ADD_TEST(testShowTutorial);
	ADD_TEST(testAnimation);
	ADD_TEST(testShowEndBanner);
	ADD_TEST(testFinalDisplayPos);
	
}

#pragma mark -
#pragma mark Sub Test Definition
void TutorialTest::tutorialScene()
{
	GameWorld::instance()->removeFromParent();
	
	auto scene = GameSceneLayer::createTutorialScene();
	
	Director::getInstance()->pushScene(scene);
}


void TutorialTest::testFinalDisplayPos()
{
	TutorialData *data = TutorialManager::instance()->getTutorialData(2);	// last one
	TutorialStep *step = TutorialStep::create();
//	
//	step->setUILayer(mTutorialLayer);
	step->setData(data);
//	
//	step->showTutorial();
	log("TutorialData:%s", data->toString().c_str());

	log("finalPos=%s", POINT_TO_STR(step->getFinalDisplayPosition()).c_str());

}


void TutorialTest::testShowEndBanner()
{
	log("this is a sample subTest");
	
	TutorialData *data = TutorialManager::instance()->getTutorialData(-1);	// last one
	TutorialStep *step = TutorialStep::create();
	
	step->setUILayer(mTutorialLayer);
	step->setData(data);
	
	step->showTutorial();
}


void TutorialTest::testShowTutorial()
{
	log("this is a sample subTest");
	
	int stepID = 2;
	TutorialData *data = TutorialManager::instance()->getTutorialData(stepID);
	TutorialStep *step = TutorialStep::create();
	
	log("DEBUG: Tutorial Data: %s", data->toString().c_str());
	//log("DEBUG: Tutorial Step: %s", step->toString().c_str());
	
	step->setUILayer(mTutorialLayer);
	step->setData(data);
	
	step->showTutorial();
}

void TutorialTest::testShowDialog()
{
	mTutorialLayer->showDialog("TestingTesting");
}

void TutorialTest::testCloseDialog()
{
	mTutorialLayer->closeDialog();
}

void TutorialTest::testDialog()
{
	mTutorialLayer->reset();
	
	
	TutorialData *data = TutorialData::create();
	data->setShowNpcDialog(true);
	data->setDialogMessage(("Testing Testing"));
	
	
	TutorialStep *step = TutorialStep::create();
	
	step->setUILayer(mTutorialLayer);
	step->setData(data);
	step->showTutorial();
}

void TutorialTest::testTutorial()
{
	mTutorialLayer->reset();
	std::function<void(float delta)> logic = [&](float delta) {
		
		mTutorialLayer->update(delta);
		if(mTutorialLayer->isWaitForPlayer() == false) {
			GameWorld::instance()->getModelLayer()->update(delta);
			GameWorld::instance()->moveCameraToPlayer();
		}
		//
	};
	
	schedule(logic, 0, "tutorial");
}

void TutorialTest::testAnimation()
{
    Node* node;
	node = mTutorialLayer->addLoopAnimation("tutorial_dialog1.csb", Vec2(205, 360),0.5);
  
    node = mTutorialLayer->addLoopAnimation("tutorial_dialog2.csb", Vec2(68, 240),0.55);

    node = mTutorialLayer->addLoopAnimation("tutorial_dialog3.csb", Vec2(115, 455),0.5);

    node = mTutorialLayer->addLoopAnimation("tutorial_dialog4.csb", Vec2(115, 575),0.5);

	mTutorialLayer->addOneTimeAnimation("tutorial_dialog5.csb", VisibleRect::center());
}

void TutorialTest::testShowCsb()
{
	mTutorialLayer->reset();
	
	
	// Display Position Type
//	std::string posType = aurora::JSONHelper::getString(data, "positionType", "");
//	tutorialData->setDisplayPosType(TutorialData::getPositionTypeByName(posType));
//	
//	// Display Pos
//	intArray.clear();
//	aurora::JSONHelper::getJsonIntArray(data, "displayPos", intArray);
//	tutorialData->setDisplayPosition(getPositionFromArray(intArray));
//	
//	// Display Csb
//	std::string displayCsb = aurora::JSONHelper::getString(data, "displayCsb", "");
//	tutorialData->setDisplayCsb(displayCsb);

	
	TutorialData *data = TutorialData::create();
	data->setShowNpcDialog(true);
	data->setDialogMessage(("Testing Testing"));
	data->setDisplayPosType(TutorialData::PositionType::MapPos);
	data->setDisplayPosition(Vec2(200, 230));
	data->setDisplayCsb("tutorial/tutorial_step_dropline1.csb");
	
	
	TutorialStep *step = TutorialStep::create();
	
	step->setUILayer(mTutorialLayer);
	step->setData(data);
	step->showTutorial();
}

void TutorialTest::testCanSlopeLineStart()
{
	TutorialData *data = TutorialData::create();
//	"startTouchArea" : [200, 100, 80, 80],
//	"endTouchArea" : [120, 180, 80, 80]
//	CC_SYNTHESIZE(Rect, mHitArea, HitArea);
//	CC_SYNTHESIZE(Rect, mSlopeEndHitArea, SlopeEndHitArea);

	data->setHitArea(Rect(200, 100, 80, 80));
	data->setSlopeEndHitArea(Rect(120, 180, 80, 80));
	
	
	TutorialStep *step = TutorialStep::create();
	step->setData(data);
	
	std::vector<Vec2> testData;
	testData.push_back(Vec2(200, 100));	// true
	testData.push_back(Vec2(280, 100));	// true
	testData.push_back(Vec2(160, 180));	// true
	testData.push_back(Vec2(110, 180));	// false
	
	for(Vec2 pos : testData)
	{
		bool result = step->canSlopeLineStart(pos);
		log("pos=%s result=%d", POINT_TO_STR(pos).c_str(), result);
	}
	
}

void TutorialTest::testCanSlopeLineEnd()
{
	TutorialData *data = TutorialData::create();
	//	"startTouchArea" : [200, 100, 80, 80],
	//	"endTouchArea" : [120, 180, 80, 80]
	//	CC_SYNTHESIZE(Rect, mHitArea, HitArea);
	//	CC_SYNTHESIZE(Rect, mSlopeEndHitArea, SlopeEndHitArea);
	
	data->setHitArea(Rect(200, 100, 80, 80));
	data->setSlopeEndHitArea(Rect(120, 180, 80, 80));
	
	
	TutorialStep *step = TutorialStep::create();
	step->setData(data);
	
	std::vector<Vec2> testData;
	testData.push_back(Vec2(120, 180));	// true
	testData.push_back(Vec2(80, 180));	// true
	testData.push_back(Vec2(160, 180));	// true
	testData.push_back(Vec2(200, 180));	// false
	
	for(Vec2 pos : testData)
	{
		bool result = step->canSlopeLineEnd(pos);
		log("pos=%s result=%d", POINT_TO_STR(pos).c_str(), result);
	}
	
}

#endif
