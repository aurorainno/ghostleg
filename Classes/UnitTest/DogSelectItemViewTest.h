#ifdef ENABLE_TDD
//
//  DogSelectItemViewTest.h
//
//
#ifndef __TDD_DogSelectItemViewTest__
#define __TDD_DogSelectItemViewTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class DogSelectItemViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testDifferentState();
	void testDogForSuggest();
	void debugDog1();
};

#endif

#endif
