#ifdef ENABLE_TDD
//
//  FBProfilePicTest.h
//
//
#ifndef __TDD_FBProfilePicTest__
#define __TDD_FBProfilePicTest__

// Include Header

#include "TDDBaseTest.h"

class FBProfilePic;

// Class Declaration
class FBProfilePicTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testDifferentSize();
	void setProfilePic();
	void clearProfilePic();
	void clearCache();
	
private:
	FBProfilePic *mProfilePic;
};

#endif

#endif
