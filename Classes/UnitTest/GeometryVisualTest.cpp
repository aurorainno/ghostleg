#ifdef ENABLE_TDD
//
//  GeometryVisualTest.m
//	TDD Framework 
//
//
#include "GeometryVisualTest.h"
#include "TDDHelper.h"
#include "StringHelper.h"
#include "GeometryHelper.h"
#include "VisibleRect.h"

void GeometryVisualTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");

	mUpdateLogic = nullptr;
	mDrawNode = DrawNode::create();
	addChild(mDrawNode);

	scheduleUpdate();
}


void GeometryVisualTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	unscheduleUpdate();
}

void GeometryVisualTest::update(float delta)
{
	// log("update is called");
	if(mUpdateLogic) {
		mUpdateLogic(delta);
	}
}

#pragma mark -
#pragma mark List of Sub Tests

void GeometryVisualTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(GeometryVisualTest::subTest);
	SUBTEST(GeometryVisualTest::testUpdateLogic);
	SUBTEST(GeometryVisualTest::testHomingLogic);
}

void GeometryVisualTest::redraw()
{
	mDrawNode->clear();
	
	std::vector<Rect>::iterator itRect = mRectArray.begin();
	for(;itRect != mRectArray.end(); itRect++) {
		Rect rect = *itRect;
		
		Vec2 origin = rect.origin;
		Vec2 dest = origin + rect.size;
		
		mDrawNode->drawRect(origin, dest, Color4F::BLUE);
	}
	
	//
	std::vector<Rect *>::iterator itRectRef = mRectRefArray.begin();
	for(;itRectRef != mRectRefArray.end(); itRectRef++) {
		Rect *rectPtr = *itRectRef;
		Rect rect = *rectPtr;
		
		Vec2 origin = rect.origin;
		Vec2 dest = origin + rect.size;
		
		mDrawNode->drawRect(origin, dest, Color4F::RED);
	}
}

#pragma mark -
#pragma mark Sub Test Definition
void GeometryVisualTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	Rect rect = Rect(50, 50, 100, 100);
	mRectArray.push_back(rect);
	
	redraw();
}


void GeometryVisualTest::testUpdateLogic(Ref *sender)
{
	mRectArray.clear();
	
	mRect1 = Rect(50, 50, 10, 10);
	Vec2 speed = Vec2(100, 50);
	
	
	mRectRefArray.push_back(&mRect1);
	
	mUpdateLogic = [&](float delta) {
		
		//log("updateLogic is called. delta=%f", delta);
		Vec2 newPos = mRect1.origin + Vec2(100 * delta, 50 * delta) ;
		mRect1.origin = newPos;
		
		//log("updateLogic is called. mRect1=%s newPos=%f,%f delta=%f",
		//		RECT_TO_STR(mRect1).c_str(), newPos.x, newPos.y, delta);
		
		redraw();
	};
	
}


void GeometryVisualTest::testHomingLogic(Ref *sender)
{
	mRectArray.clear();
	
	Vec2 center = VisibleRect::center();
	
	Vec2 topLeft = center + Vec2(-100, 100);
	Vec2 topRight = center + Vec2(100, 100);
	Vec2 botLeft = center + Vec2(-100, -100);
	Vec2 botRight = center + Vec2(100, -100);
	
	
	mRect1 = Rect(VisibleRect::center(), Size(10, 10));
	mRect2 = Rect(botRight, Size(10, 10));
	mRectMoving = mRect1;
	
	Vec2 speed = Vec2(100, 50);
	
	
	mRectArray.push_back(mRect1);
	
	mRectRefArray.push_back(&mRectMoving);
	mRectRefArray.push_back(&mRect2);
	
	mUpdateLogic = [&](float delta) {
		
		//log("updateLogic is called. delta=%f", delta);
		Vec2 newPos;
		

		// Detail coding
//		float angle = aurora::GeometryHelper::findAngleRadian(mRectMoving.origin, mRect2.origin);
//		Vec2 diff = aurora::GeometryHelper::resolveVec2(delta * 70, angle);
//		//mRectMoving.origin + Vec2(70 * delta, 50 * delta) ;
//		mRectMoving.origin = mRectMoving.origin + diff;

		mRectMoving.origin = aurora::GeometryHelper::calculateNewTracePosition(
						mRectMoving.origin, mRect2.origin, 70, delta);
		
		
		newPos = mRect2.origin + Vec2(0 * delta, 20 * delta) ;
		mRect2.origin = newPos;

		//log("updateLogic is called. mRect1=%s newPos=%f,%f delta=%f",
		//		RECT_TO_STR(mRect1).c_str(), newPos.x, newPos.y, delta);
		
		redraw();
	};
	
}

#endif
