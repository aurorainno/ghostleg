#ifdef ENABLE_TDD
//
//  SpeedEffectTest.m
//	TDD Framework
//
//
#include "SpeedEffectTest.h"
#include "TDDHelper.h"
#include "Player.h"
#include "VisibleRect.h"
#include "SpeedUpEffect.h"
#include "GameWorld.h"
#include "StageParallaxLayer.h"
#include "ModelLayer.h"
#include "GameRes.h"

void SpeedEffectTest::setUp()
{
	log("TDD Setup is called");
	
	
	// Setup the space background
	mParallaxLayer = StageParallaxLayer::create();
	mParallaxLayer->setMainNode(GameWorld::instance());
	addChild(mParallaxLayer);
	
	
	GameWorld::instance()->setParallaxLayer(mParallaxLayer);
	
	
	Player *player = GameWorld::instance()->getPlayer();
	
	std::string csbName = GameRes::getCharacterCsbName(3);
	player->setup(csbName);
	//player->setup("character")
	player->setPosition(VisibleRect::center());
	mPlayer = player;

	GameWorld::instance()->setUserTouchEnable(false);
	GameWorld::instance()->moveCameraToPlayer();
	
	GameWorld::instance()->getModelLayer()->setupMotionStreak();
	GameWorld::instance()->getModelLayer()->setMotionEnable(true);
	
	setupTouchListener();
	
	scheduleUpdate();
	
	mMotionStyle = 0;
	mSpeed = 50;
}

void SpeedEffectTest::update(float delta)
{
	Vec2 pos = mPlayer->getPosition();
	pos.y += mSpeed * delta;
	
	mPlayer->setPosition(pos);
	GameWorld::instance()->getModelLayer()->updateMotion(delta);
	
	GameWorld::instance()->moveCameraToPlayer();
}

void SpeedEffectTest::tearDown()
{
	log("TDD tearDown is called");
}

void SpeedEffectTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void SpeedEffectTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests





#pragma mark - Touch Handling
void SpeedEffectTest::setupTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(SpeedEffectTest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(SpeedEffectTest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(SpeedEffectTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(SpeedEffectTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}

bool SpeedEffectTest::onTouchBegan(Touch *touch, Event *event)
{
	mLastPos = touch->getLocation();
	
	return true;
}

void SpeedEffectTest::onTouchEnded(Touch *touch, Event *event)
{
}

void SpeedEffectTest::onTouchMoved(Touch *touch, Event *event)
{
	Vec2 newPos = touch->getLocation();
	
	
	Vec2 diff = newPos - mLastPos;
	
	// Move Player by diff
	Vec2 oldPos = mPlayer->getPosition();
	float newX = oldPos.x + diff.x;
	if(newX > 320){
		newX = 320;
	}else if(newX < 0) {
		newX = 0;
	}
	
	mPlayer->setPosition(Vec2(newX, oldPos.y));
	
	mLastPos = newPos;
}

void SpeedEffectTest::onTouchCancelled(Touch *touch, Event *event)
{
	
}


#pragma mark -
#pragma mark Sub Test Definition

void SpeedEffectTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testChangeStyle);
	ADD_TEST(testIncreaseSpeed);
}


void SpeedEffectTest::testSample()
{
	log("this is a sample subTest");
}


void SpeedEffectTest::testIncreaseSpeed()
{
	mSpeed += 50;
	
}


void SpeedEffectTest::testChangeStyle()
{
	ModelLayer::MotionStyle newStyle = (ModelLayer::MotionStyle) mMotionStyle;
	//::StyleMaxSpeed;
	GameWorld::instance()->getModelLayer()->changeMotionStyle(newStyle);
	
	mMotionStyle++;
	if(mMotionStyle > ModelLayer::MotionStyle::StyleSpeedUp) {
		mMotionStyle = 0;
	}
}

#endif
