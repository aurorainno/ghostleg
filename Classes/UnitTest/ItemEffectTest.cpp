#ifdef ENABLE_TDD
//
//  ItemEffectTest.m
//	TDD Framework 
//
//
#include "ItemEffectTest.h"
#include "TDDHelper.h"
#include "Player.h"
#include "InvulnerableEffect.h"
#include "ShieldEffect.h"
#include "TimeStopEffect.h"
#include "VisibleRect.h"
#include "GameWorld.h"
#include "EnemyFactory.h"
#include "MissileEffect.h"
#include "GrabStarEffect.h"
#include "StarMagnetEffect.h"
#include "ModelLayer.h"
#include "ItemEffectUILayer.h"
#include "Star.h"

void ItemEffectTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	addChild(GameWorld::instance());
	
	Layer *uiLayer = Layer::create();
	addChild(uiLayer);
	
	GameWorld::instance()->setEffectUILayer(uiLayer);
	
	
	// Player
	Player *player = GameWorld::instance()->getPlayer();
	player->reset();
	player->setPosition(VisibleRect::center());
	setPlayer(player);
	
	
	// Enemy
	Enemy *enemy = EnemyFactory::instance()->createEnemy(1);
	enemy->setState(Enemy::State::Active);
	enemy->setPosition(VisibleRect::top());
	//addChild(enemy);
	GameWorld::instance()->getModelLayer()->addEnemy(enemy);
	
	
	setEnemy(enemy);
	
	// Some Star
	for(int py=300; py<=800; py+=40) {
        Star *star = Star::create(Star::SmallStar);
		int px = RandomHelper::random_int(10, 300);
		
		star->setPosition(Vec2(px, py));
		
		GameWorld::instance()->getModelLayer()->addItem(star);
		
	}
	
}


void ItemEffectTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	GameWorld::instance()->setEffectUILayer(GameWorld::instance());
}

#pragma mark -
#pragma mark List of Sub Tests

void ItemEffectTest::defineTests()
{
	ADD_TEST(activateBooster);
	ADD_TEST(testMagnetPowerUp);
	ADD_TEST(resetPlayer);
	ADD_TEST(activateCapsule);
	ADD_TEST(step);
	ADD_TEST(hitPlayer);
	ADD_TEST(testInvulnerable);
	ADD_TEST(testStarMagnet);
	ADD_TEST(testGrabStar);
	ADD_TEST(testMissile);
	ADD_TEST(testShield);
	ADD_TEST(testTimeStop);
	ADD_TEST(setWorldStatus);
    ADD_TEST(testItemEffectUI);
}

#pragma mark -
#pragma mark Sub Test Definition
void ItemEffectTest::activateBooster()
{
	getPlayer()->activateBooster(BoosterItemMissile);
}

void ItemEffectTest::testMagnetPowerUp()
{
	getPlayer()->activatePowerUpByType(ItemEffect::Type::ItemEffectStarMagnet);
}

void ItemEffectTest::resetPlayer()
{
	log("this is a sample subTest");
	getPlayer()->reset();
	GameWorld::instance()->moveCameraToPlayer();
}

void ItemEffectTest::step()
{
	float delta = 0.4f;

	GameWorld::instance()->getModelLayer()->update(delta);
//	getPlayer()->update(delta);
//	getEnemy()->update(delta);
	
	GameWorld::instance()->moveCameraToPlayer();
	
	log("Player Detail:\n%s\n", getPlayer()->infoAttribute().c_str());
}


void ItemEffectTest::testStarMagnet()
{
	StarMagnetEffect *effect = StarMagnetEffect::create();
	getPlayer()->attachItemEffect(effect);
	
	setItemEffect(effect);
}


void ItemEffectTest::activateCapsule()
{
	getPlayer()->activateCapsule();
}

void ItemEffectTest::hitPlayer()
{
	getPlayer()->getItemEffect()->onEvent(ItemEffect::Event::PlayerIsHit);
	if(mItemEffect) {
		mItemEffect->update(1);
	}
}

void ItemEffectTest::testInvulnerable()
{
	InvulnerableEffect *effect = InvulnerableEffect::create();
	getPlayer()->attachItemEffect(effect);
	
	setItemEffect(effect);
}

void ItemEffectTest::testShield()
{
	ShieldEffect *effect = ShieldEffect::create();
	effect->setMaxCount(2);
	
	
	getPlayer()->attachItemEffect(effect);
	
	setItemEffect(effect);
}

void ItemEffectTest::testTimeStop()
{
	TimeStopEffect *effect = TimeStopEffect::create();
	effect->setMaxDuration(2);
	
	
	getPlayer()->attachItemEffect(effect);
	
	setItemEffect(effect);
}

void ItemEffectTest::testMissile()
{
	MissileEffect *effect = MissileEffect::create();
	effect->setMaxCount(5);
	
	getPlayer()->attachItemEffect(effect);
	
	setItemEffect(effect);
}

void ItemEffectTest::testGrabStar()
{
	GrabStarEffect *effect = GrabStarEffect::create();
	effect->setDuration(0.7);
	
	getPlayer()->attachItemEffect(effect);
	
	setItemEffect(effect);
}

void ItemEffectTest::setWorldStatus()
{
	GameWorld::instance()->setStatus(GameWorld::Status::Slow);
}


void ItemEffectTest::testItemEffectUI(){
    ItemEffectUILayer *layer = ItemEffectUILayer::create();
    layer->setPosition(VisibleRect::center());
    addChild(layer);
    layer->configAsTimer("icon_timestop.png", "## sec", 10);
}







#endif
