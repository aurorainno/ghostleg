#ifdef ENABLE_TDD
//
//  GameContinueDialogTest.m
//	TDD Framework
//
//
#include "GameContinueDialogTest.h"
#include "TDDHelper.h"
#include "GameContinueDialog.h"
#include "GameManager.h"

void GameContinueDialogTest::setUp()
{
	log("TDD Setup is called");
}


void GameContinueDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void GameContinueDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void GameContinueDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void GameContinueDialogTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testNoAd);
	ADD_TEST(testIncCost);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameContinueDialogTest::testSample()
{
	log("this is a sample subTest");
	GameContinueDialog *dialog = GameContinueDialog::create();
	addChild(dialog);
	
	dialog->setCallback([&](Ref *ref, GameContinueDialog::GameContinueOption opt) {
		log("debug: %d", opt);
	});

}

void GameContinueDialogTest::testNoAd()
{
	log("this is a sample subTest");
	GameContinueDialog *dialog = GameContinueDialog::create();
	dialog->hideWatchAd();
	addChild(dialog);
	
	dialog->setCallback([&](Ref *ref, GameContinueDialog::GameContinueOption opt) {
		log("debug: %d", opt);
	});
	
}


void GameContinueDialogTest::testIncCost()
{
	GameManager::instance()->increaseReviveCost();
}


#endif
