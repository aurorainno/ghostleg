#ifdef ENABLE_TDD
//
//  GameCenterTestTest.h
//
//
#ifndef __TDD_GameCenterTestTest__
#define __TDD_GameCenterTestTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class GameCenterTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
    void testGameCenterLayer(Ref* sender);
	void testSignin(Ref *sender);
	void testBestDistance(Ref *sender);
	void testSubmitDistance(Ref *sender);
}; 

#endif

#endif
