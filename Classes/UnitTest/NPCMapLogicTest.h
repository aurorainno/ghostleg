#ifdef ENABLE_TDD
//
//  NPCMapLogicTest.h
//
//
#ifndef __TDD_NPCMapLogicTest__
#define __TDD_NPCMapLogicTest__

// Include Header
class NPCMapLogic;
class StageParallaxLayer;

#include "TDDBaseTest.h"

// Class Declaration
class NPCMapLogicTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	void setupParallax();
	
	void movePlayer(int offset);
private:
	void testSample();
	void addNpcMap();
	void setNpcMap1();
	void testAddPackageBox();
	void moveUp();
	void moveDown();


private:
	NPCMapLogic *mMapLogic;
	StageParallaxLayer *mParallaxLayer;
};

#endif

#endif
