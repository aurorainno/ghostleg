#ifdef ENABLE_TDD
//
//  GameResTest.m
//	TDD Framework
//
//
#include "GameResTest.h"
#include "TDDHelper.h"
#include "GameRes.h"

void GameResTest::setUp()
{
	log("TDD Setup is called");
}


void GameResTest::tearDown()
{
	log("TDD tearDown is called");
}

void GameResTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void GameResTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void GameResTest::defineTests()
{
	ADD_TEST(testCountryFlag);
	ADD_TEST(testMoneyIcon);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameResTest::testCountryFlag()
{
	std::vector<std::string> countryList;
	
	countryList.push_back("UG");
	countryList.push_back("HK");
	countryList.push_back("US");
	countryList.push_back("XX");
	
	
	Vec2 startPos = Vec2(10, 10);
	
	
	Vec2 pos = startPos;
	for(std::string code : countryList) {
		std::string resName = GameRes::getCountryFlag(code);
		
		Sprite *sprite = Sprite::create(resName);
		sprite->setPosition(pos);
		sprite->setAnchorPoint(Vec2(0, 0));
		
		addChild(sprite);
		
		if((pos.x + sprite->getContentSize().width) > 320) {		// next row
			pos.x = startPos.x;
			pos.y += sprite->getContentSize().height + 10;
		} else {
			pos.x += sprite->getContentSize().width + 10;
		}
	}
	
}


void GameResTest::testMoneyIcon()
{
	std::vector<MoneyType> moneyTypeList;
	moneyTypeList.push_back(MoneyTypeStar);
	moneyTypeList.push_back(MoneyTypeCandy);
	
	Vec2 pos = Vec2(100, 100);
	for(MoneyType type : moneyTypeList) {
		std::string resName = GameRes::getMoneyIcon(type);
		
		Sprite *sprite = Sprite::create(resName);
		sprite->setPosition(pos);
		
		addChild(sprite);
		
		pos.y += 60;
	}
	
}


#endif
