#ifdef ENABLE_TDD
//
//  RandomHelperTest.m
//	TDD Framework 
//
//
#include "RandomHelperTest.h"
#include "TDDHelper.h"
#include "RandomHelper.h"
#include "StringHelper.h"
#include <map>

void RandomHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void RandomHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void RandomHelperTest::defineTests()
{
	ADD_TEST(testRandomizedList);
	ADD_TEST(testFindMultiRandomValueRepeat);
	ADD_TEST(testFindMultiRandomValue);
	ADD_TEST(testFindRandomValue);
	ADD_TEST(testRandomRange);
	ADD_TEST(testShuffle);
}

#pragma mark -
#pragma mark Sub Test Definition
void RandomHelperTest::testRandomRange()
{
	const int numTest = 2;
	int testRange[numTest][2] = {
		{10, 30},
		{11, 12},
	};
	
	
	for(int i=0; i<numTest; i++) {
		int start = testRange[i][0];
		int end = testRange[i][1];

		for(int k=0; k<10; k++) {
			int result = aurora::RandomHelper::randomBetween(start, end);
			log("trial-%d: start=%d end=%d result=%d", k, start, end, result);
		}
		
		//int random = RandomHelper::randomBetween(start, end);
	}
}

void RandomHelperTest::testShuffle()
{
	static const int arr[] = {1, 2, 3, 4, 5, 6};
	std::vector<int> vec (arr, arr + sizeof(arr)/sizeof(arr[0]));
	
	aurora::RandomHelper::shuffleVector(vec);
	
	for(int i=0; i<vec.size(); i++){
		log("%d", vec[i]);
	}
}

void RandomHelperTest::testFindRandomValue()
{
	std::map<int, int> chanceTable;
	chanceTable[1] = 20;		// 20%
	chanceTable[2] = 50;		// 50%
	chanceTable[3] = 20;		// 20%
	chanceTable[4] = 8;			// 8%
	chanceTable[5] = 0;			// 0%
	chanceTable[6] = 2;			// 2%
	
	int nTrial = 50;
	
	std::map<int, int> resultStat;
	for(int i=0; i<nTrial; i++) {
		int result = aurora::RandomHelper::findRandomValue(chanceTable);
		
		log("trial %d: %d", i, result);
		resultStat[result]++;
	}
	
	log("Result stat:");
	for (std::map<int,int>::iterator it=resultStat.begin(); it!=resultStat.end(); ++it) {
		int value = it->first;
		int count = it->second;
		float percent = (float) count * 100 / nTrial;
		
		log("%d: count=%d percent=%f (%d)", value, count, percent, chanceTable[value]);
	}
	
}


void RandomHelperTest::testFindMultiRandomValueRepeat()
{
	std::map<int, int> chanceTable;
	chanceTable[1] = 50;		// 20%
	chanceTable[2] = 50;		// 50%
//	chanceTable[3] = 20;		// 20%
//	chanceTable[4] = 8;			// 8%
//	chanceTable[5] = 0;			// 0%
//	chanceTable[6] = 2;			// 2%
	
	int nTrial = 50;
	int nCollect = 5;
	
	std::map<int, int> resultStat;
	for(int i=0; i<nTrial; i++) {
		std::map<int, int> myChanceTable = chanceTable;
		std::vector<int> result = aurora::RandomHelper::findMultiRandomValue(myChanceTable, nCollect, true);
		
		log("trial %d: %s", i, VECTOR_TO_STR(result).c_str());
		
		for(int value : result) {
			resultStat[value]++;
		}
	}
	
	log("Result stat:");
	for (std::map<int,int>::iterator it=resultStat.begin(); it!=resultStat.end(); ++it) {
		int value = it->first;
		int count = it->second;
		float percent = (float) count * 100 / (nTrial * 2);
		
		log("%d: count=%d percent=%f (%d)", value, count, percent, chanceTable[value]);
	}
}

void RandomHelperTest::testFindMultiRandomValue()
{
	std::map<int, int> chanceTable;
	chanceTable[1] = 20;		// 20%
	chanceTable[2] = 50;		// 50%
	chanceTable[3] = 20;		// 20%
	chanceTable[4] = 8;			// 8%
	chanceTable[5] = 0;			// 0%
	chanceTable[6] = 2;			// 2%
	
	int nTrial = 50;
	
	std::map<int, int> resultStat;
	for(int i=0; i<nTrial; i++) {
		std::map<int, int> myChanceTable = chanceTable;
		std::vector<int> result = aurora::RandomHelper::findMultiRandomValue(myChanceTable, 2, false);
		
		log("trial %d: %s", i, VECTOR_TO_STR(result).c_str());
		
		for(int value : result) {
			resultStat[value]++;
		}
	}
	
	log("Result stat:");
	for (std::map<int,int>::iterator it=resultStat.begin(); it!=resultStat.end(); ++it) {
		int value = it->first;
		int count = it->second;
		float percent = (float) count * 100 / (nTrial * 2);
		
		log("%d: count=%d percent=%f (%d)", value, count, percent, chanceTable[value]);
	}
	
}


void RandomHelperTest::testRandomizedList()
{
	// getRandomizeList(std::vector<int> &vec, int numReturn, int numShuffle)
	std::vector<int> originList;
	for(int i=0; i<50; i++) { originList.push_back(i+1); }
	
	int nTrial = 20;
	
	for(int i=0; i<nTrial; i++) {
		std::vector<int> result = aurora::RandomHelper::getRandomizeList(originList, 20, 30);
	
		log("%d: %s   size=%ld", i, VECTOR_TO_STR(result).c_str(), result.size());
	}
}

//void RandomHelperTest::findRandomValue(

#endif
