#ifdef ENABLE_TDD
//
//  MapDataGeneratorTest.h
//
//
#ifndef __TDD_MapDataGeneratorTest__
#define __TDD_MapDataGeneratorTest__

// Include Header

#include "TDDBaseTest.h"

class MapDataGenerator;

// Class Declaration 
class MapDataGeneratorTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
	void updateMap(MapDataGenerator *generator);
	
private:
	void moveUp();
	void moveDown();
	void testNewMapGenerate();
	void testTutorialMap();
	void testGenNpcMap();
	void testGenByMapID();
	void debugEnemyMove();		// Fix enemy not walk on line
	void testMapGenerate();
	void testGenerateCapsule();
	void testGenerateManyCapsule();
	void testGenerateMapLines();
	void testAddEnemy();
	void testNewMap();
	void testAddLine();
	void testGenerate();
	void testRandomLine();
	void testGenerateByLevel();
}; 

#endif

#endif
