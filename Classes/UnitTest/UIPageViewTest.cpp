#ifdef ENABLE_TDD
//
//  UIPageViewTest.m
//	TDD Framework
//
//
#include "UIPageViewTest.h"
#include "TDDHelper.h"
#include <time.h>
#include "VisibleRect.h"

#include "GameContinueDialog.h"
#include "cocostudio/CocoStudio.h"

#include "ViewHelper.h"
#include "CommonMacro.h"
#include "AILPageView.h"

using namespace cocos2d::ui;
using namespace cocostudio::timeline;


void UIPageViewTest::setUp()
{
	log("TDD Setup is called");
}


void UIPageViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void UIPageViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void UIPageViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void UIPageViewTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testSizedPageSize);
	ADD_TEST(testWithPlanet);
	ADD_TEST(testAILPageView);
	ADD_TEST(testPlanet);
}

#pragma mark -
#pragma mark Sub Test Definition
void UIPageViewTest::testSample()
{
	log("this is a sample subTest");

	Node *rootNode = CSLoader::createNode("TestPageViewLayer.csb");
	addChild(rootNode);
	
	FIX_UI_LAYOUT(rootNode);
	
	PageView *pageView = (PageView *) rootNode->getChildByName("mainPageView");
	TDD_ASSERT_TRUE(pageView != nullptr, "pageView is null");
	
	pageView->setDirection(PageView::Direction::HORIZONTAL);
	
	Text *text;

	for(int i=0; i<10; i++) {
		std::string msg = StringUtils::format("Page %d", i);
		text = Text::create();
		text->setTextAreaSize(VisibleRect::getVisibleRect().size);
		//text->getCustomSize();
		text->setString(msg);
		pageView->addPage(text);
	}
	
	// pageView
	
	// pageView->add
}

Layout *UIPageViewTest::createPlanetGUI(int planetID)
{
	std::string name = StringUtils::format("res/planetGUI/planet_gui_%03d.csb", planetID);
	Node *guiNode = CSLoader::createNode(name);
	
	guiNode->setContentSize(VisibleRect::getVisibleRect().size);
	cocos2d::ui::Helper::doLayout(guiNode);
	
	Layout *panel = Layout::create();
	panel->setContentSize(VisibleRect::getVisibleRect().size);
	panel->addChild(guiNode);
	panel->setTouchEnabled(false);
	
	return panel;
	
}

void UIPageViewTest::testWithPlanet()
{
	log("this is a sample subTest");
	
//	Layout *planet = createPlanetGUI(1);
//	addChild(planet);
	
	Node *rootNode = CSLoader::createNode("TestPageViewLayer.csb");
	addChild(rootNode);
	
	FIX_UI_LAYOUT(rootNode);
	
	PageView *pageView = (PageView *) rootNode->getChildByName("mainPageView");
	TDD_ASSERT_TRUE(pageView != nullptr, "pageView is null");
	
	pageView->setDirection(PageView::Direction::HORIZONTAL);
	
	for(int i=1; i<=3; i++) {
		Layout *planet = createPlanetGUI(i);
		pageView->addPage(planet);
	}
	
	
	pageView->addEventListener([&](Ref *ref, PageView::EventType event) {
		log("Page Event");

		switch(event) {
			case PageView::EventType::TURNING: {
				log("Page Turned");
				PageView *view = (PageView *) ref;
				log("Current Page=%d", (int) view->getCurrentPageIndex());
			}
		}
	});
	
//	ui::ScrollView::ccScrollViewCallback scrollViewCallback = [&](Ref* ref, ui::ScrollView::EventType type) -> void{
//		if (type == ui::ScrollView::EventType::AUTOSCROLL_ENDED
//			&& _previousPageIndex != _currentPageIndex) {
//			pageTurningEvent();
//		}
//	};
}

void UIPageViewTest::modifyPageOpacity(ui::PageView *pageView, int page, int opacity)
{
	Widget *widget = pageView->getItem(page);
	if(widget) {
		log("Setting widget to opacity=%d", opacity);
		Vector<Node *> children = widget->getChildren();
		for(auto node : children) {
			node->setOpacity(opacity);
		}

	} else {
		log("widget is null. page=%d", page);
	}
}

void UIPageViewTest::testAILPageView()
{
	AILPageView *pageView = AILPageView::create();
	pageView->setContentSize(VisibleRect::getVisibleRect().size);

	addChild(pageView);
	
	pageView->setDirection(PageView::Direction::HORIZONTAL);
	
	for(int i=1; i<=3; i++) {
		Layout *planet = createPlanetGUI(i);
		pageView->addPage(planet);
	}

	pageView->setCurrentPageIndex(1);
    
//    pageView->addClickEventListener([&,pageView](Ref*){
//        log("Page Clicked");
//        pageView->scrollToPage(pageView->getCurrentPageIndex()+1);
//    });
    
	pageView->addEventListener([&](Ref *ref, PageView::EventType event) {
		log("Page Event");
		
		switch(event) {
			case PageView::EventType::TURNING: {
				log("Page Turned");
				PageView *view = (PageView *)ref;
				log("Current Page=%d", view->getCurrentPageIndex());
				
				modifyPageOpacity(view, view->getCurrentPageIndex(), 255);
			}
		}
	});
	
	pageView->setScrollCallback([&](Ref *ref, Vec2 offset) {
        float modScroll = -offset.x > 320 ? fmod(-offset.x, 320.0f) : fmod(320.f, -offset.x);
		log("Scroll: %f, %f; %f", offset.x, offset.y, modScroll);
		
		PageView *view = (PageView *)ref;
		modifyPageOpacity(view, view->getCurrentPageIndex(), 150);
	});

    Button* btn = Button::create();
    btn->setTitleText("next");
    btn->setPosition(Vec2(160,480));
    addChild(btn);
    btn->addClickEventListener([&,pageView](Ref*){
        log("Page Clicked");
        pageView->scrollToPage(pageView->getCurrentPageIndex()+1);
    });
    
	//	ui::ScrollView::ccScrollViewCallback scrollViewCallback = [&](Ref* ref, ui::ScrollView::EventType type) -> void{
	//		if (type == ui::ScrollView::EventType::AUTOSCROLL_ENDED
	//			&& _previousPageIndex != _currentPageIndex) {
	//			pageTurningEvent();
	//		}
	//	};
	
	
	
}

void UIPageViewTest::testPlanet()
{
	Layout *planet = createPlanetGUI(1);
	addChild(planet);
	Vector<Node *> children = planet->getChildren();
	for(auto node : children) {
		node->setOpacity(50);
	}
	
}


void UIPageViewTest::testSizedPageSize()
{
	
	
	AILPageView *pageView = AILPageView::create();
	pageView->setContentSize(Size(320, 200));
	pageView->setPosition(Vec2(0, 200));
	addChild(pageView);
	
	pageView->setDirection(PageView::Direction::HORIZONTAL);
	
	
	std::vector<Color4B> colorList;
	colorList.push_back(Color4B::RED);
	colorList.push_back(Color4B::BLUE);
	colorList.push_back(Color4B::GREEN);
	colorList.push_back(Color4B::ORANGE);
	colorList.push_back(Color4B::YELLOW);
	
	Size pageSize = Size(150, 160);
	
	// Setting the page
	for(Color4B color : colorList) {
		//LayerColor *layer = LayerColor::create(color, pageSize.width, pageSize.height);
		Layout *layer = Layout::create();
		layer->setContentSize(pageSize);
		layer->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
		layer->setBackGroundColor(Color3B(color));
		pageView->addPage(layer);
	}
	
	pageView->setCurrentPageIndex(1);
	
//	//    pageView->addClickEventListener([&,pageView](Ref*){
//	//        log("Page Clicked");
//	//        pageView->scrollToPage(pageView->getCurrentPageIndex()+1);
//	//    });
//	
//	pageView->addEventListener([&](Ref *ref, PageView::EventType event) {
//		log("Page Event");
//		
//		switch(event) {
//			case PageView::EventType::TURNING: {
//				log("Page Turned");
//				PageView *view = (PageView *)ref;
//				log("Current Page=%d", view->getCurrentPageIndex());
//				
//				modifyPageOpacity(view, view->getCurrentPageIndex(), 255);
//			}
//		}
//	});
//	
//	pageView->setScrollCallback([&](Ref *ref, Vec2 offset) {
//		float modScroll = -offset.x > 320 ? fmod(-offset.x, 320.0f) : fmod(320.f, -offset.x);
//		log("Scroll: %f, %f; %f", offset.x, offset.y, modScroll);
//		
//		PageView *view = (PageView *)ref;
//		modifyPageOpacity(view, view->getCurrentPageIndex(), 150);
//	});
//
	
}

#endif
