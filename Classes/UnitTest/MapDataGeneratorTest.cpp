#ifdef ENABLE_TDD
//
//  MapDataGeneratorTest.m
//	TDD Framework 
//
//
#include "MapDataGeneratorTest.h"
#include "TDDHelper.h"
#include "MapDataGenerator.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "ModelLayer.h"
#include "Player.h"
#include "Item.h"
#include "LevelData.h"
#include "DebugInfo.h"
#include "ItemEffect.h"
#include "EnemyFactory.h"
#include "Enemy.h"
#include "ViewHelper.h"

namespace {
	Enemy *getEnemy(const Vector<Enemy *> &enemyList, int idx)
	{
		if(idx >= enemyList.size()) {
			return nullptr;
		}
		
		int finalIdx = (idx >= enemyList.size()) ? enemyList.size() - 1 : idx;
		
		return enemyList.at(finalIdx);
	}
	
	
}

void MapDataGeneratorTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	GameWorld::instance();	// Create the GameWorld
	
	GameWorld::instance()->getMap()->setVertLineVisible(false);
	
	addChild(GameWorld::instance());
	GameWorld::instance()->start();
}


void MapDataGeneratorTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}



#pragma mark -
#pragma mark List of Sub Tests

void MapDataGeneratorTest::defineTests()
{
	ADD_TEST(moveUp);
	ADD_TEST(moveDown);
	ADD_TEST(testNewMapGenerate);
	ADD_TEST(testNewMap);
	ADD_TEST(testGenNpcMap);
	ADD_TEST(testGenByMapID);
	ADD_TEST(testRandomLine);
	ADD_TEST(testTutorialMap);
	ADD_TEST(testGenerateMapLines);
	ADD_TEST(debugEnemyMove);
	ADD_TEST(testMapGenerate);
	ADD_TEST(testGenerateManyCapsule);
	ADD_TEST(testGenerateCapsule);
	ADD_TEST(testAddEnemy);
	ADD_TEST(testAddLine);
	ADD_TEST(testGenerate);
	ADD_TEST(testGenerateByLevel);
}


void MapDataGeneratorTest::testNewMapGenerate()
{
	
	GameWorld::instance()->getMap()->clearData();
	GameWorld::instance()->getModelLayer()->clearData();

	int mapID = 2000;
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	generator->init(map, player, 100);
	generator->generateGameMapAsync(mapID, false);		// Define the elements to be generated in queue
	
	log("QueueInfo:\n%s\n", generator->infoMapElementQueue().c_str());
	
	
	for(int i=0; i<8; i++) {
		generator->generateMapElements(GameWorld::instance());

		log("After %d generation", i);
		log("QueueInfo:\n%s\n", generator->infoMapElementQueue().c_str());
		log("ModelInfo:\n%s\n", GameWorld::instance()->getModelLayer()->info().c_str());
	}
	
	//
//	generator->addModelToWorld(GameWorld::instance());
//	
//	
//	GameWorld::instance()->getModelLayer()->update(0);
}

void MapDataGeneratorTest::updateMap(MapDataGenerator *generator)
{
	
	GameWorld::instance()->getMap()->clearData();
	GameWorld::instance()->getModelLayer()->clearData();
	
	generator->addModelToWorld(GameWorld::instance());
	
	
	GameWorld::instance()->getModelLayer()->update(0);
}

#pragma mark -
#pragma mark Sub Test Definition
void MapDataGeneratorTest::moveUp()
{
	int currentPos = GameWorld::instance()->getCameraY();
	
	GameWorld::instance()->setCameraY(currentPos + 100);
}

void MapDataGeneratorTest::moveDown()
{
	int currentPos = GameWorld::instance()->getCameraY();
	GameWorld::instance()->setCameraY(currentPos - 100);
}

void MapDataGeneratorTest::testNewMap()
{
	MapLevelData *mapData = new MapLevelData();
	
	mapData->load("level/mapData/sample.dat");
	
	log("mapData:\n%s\n", mapData->toString().c_str());
	
	// Setup Logic
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 0);
	
	
	// Core logic
	generator->generateByMapData(0, mapData);

	log("Generator:\n%s", generator->info().c_str());
	
	updateMap(generator);
}

void MapDataGeneratorTest::testAddEnemy()
{
	log("this is a sample subTest");
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	generator->init(map, player, 100);
	
	
//	generator->
	generator->testAddEnemyForPosition();
	
	
	log("Generator:\n%s", generator->info().c_str());
	
	
	delete generator;	// Don't need, go for destructor
}



void MapDataGeneratorTest::testAddLine()
{
	log("this is a sample subTest");
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	generator->init(map, player, 100);
	
	
	//	generator->
	generator->testAddLineForPosition();
	
	
	log("Generator:\n%s", generator->info().c_str());
	
	
	delete generator;	// Don't need, go for destructor
}

void MapDataGeneratorTest::testGenerate()
{
	log("this is a sample subTest");
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	generator->init(map, player, 100);
	
	generator->setLastCapsulePos(0);
	generator->setCapsuleType(ItemEffect::Type::ItemEffectMissile, 2, 3);
	

	
	//	generator->
	generator->generate();

	
	// Show the data in console
	log("Generator:\n%s", generator->info().c_str());

	
	// Show the data visually
	updateMap(generator);
	

	
	//
	
	// Clean up
	delete generator;
}


void MapDataGeneratorTest::testGenNpcMap()
{
	EnemyFactory::instance()->loadData();
	log("EnemyData:\n%s\n", EnemyFactory::instance()->infoEnemyData().c_str());
	
	//int mapID = 100;
	int mapID = 102;
	
	// Setup Logic
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 0);
	
	
	// Core logic
	generator->generateByNpcMapID(mapID);
	
	log("Generator:\n%s", generator->info().c_str());
	
	updateMap(generator);
	
}

void MapDataGeneratorTest::testGenByMapID()
{
	EnemyFactory::instance()->loadData();
	log("EnemyData:\n%s\n", EnemyFactory::instance()->infoEnemyData().c_str());
	
	//int mapID = 20000;
	//int mapID = 10000;
	//int mapID = 41;
	// int mapID = 1;
	//int mapID = 10004;
	//int mapID = 11005;
	//int mapID = 10004;
	int mapID = 10005;
	
	// Setup Logic
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 0);
	
	
	// Core logic
	generator->generateByMapID(mapID);
	
	log("Generator:\n%s", generator->info().c_str());
	
	updateMap(generator);

}

void MapDataGeneratorTest::testMapGenerate()
{
	EnemyFactory::instance()->loadData();
	log("EnemyData:\n%s\n", EnemyFactory::instance()->infoEnemyData().c_str());
	
	//int mapID = 20000;
	int mapID = 10001;
	//int mapID = 41;
	
	// Setup Logic
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 0);
	
	
	// Core logic
	generator->generateByMapID(mapID);
	
	log("Generator:\n%s", generator->info().c_str());
	
	updateMap(generator);
	
	std::vector<Vec2> spotList;
	spotList.push_back(Vec2(115, 175));
	spotList.push_back(Vec2(205, 350));
	
	spotList.push_back(Vec2( 61, 275));
	
	spotList.push_back(Vec2(115, 400));		// Eat Potion 
	spotList.push_back(Vec2(115, 475));
	
	spotList.push_back(Vec2(115, 525));
	spotList.push_back(Vec2(151, 575));
	
	for(int i=0; i<spotList.size(); i++) {
		ViewHelper::createRedSpot(GameWorld::instance()->getModelLayer(), spotList[i]);
	}
}

void MapDataGeneratorTest::debugEnemyMove()		// Fix enemy not walk on line
{
	int mapID = 20000;
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 0);
	generator->generateByMapID(mapID);
	
	log("Generator:\n%s", generator->info().c_str());
	
	
	Enemy *enemy = getEnemy(generator->getEnemy(), 0);
	log("found enemy=%s", enemy->toString().c_str());
	enemy->update(0.5f);
	log("0.5: %s", enemy->toString().c_str());
	enemy->update(0.5f);
	log("0.5: %s", enemy->toString().c_str());
}


void MapDataGeneratorTest::testGenerateMapLines()
{
	//generateMapLinesFromLevel
	
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	generator->init(map, player, 0);
	
	MapLevelData *levelData = MapLevelDataCache::instance()->getMap(20002);
	
	//	generator->
	generator->generateMapLinesFromLevel(levelData);
	
	// Show the data in console
	log("Generator:\n%s", generator->infoLine().c_str());
	
	
//	// Show the data visually
//	updateMap(generator);
//	
//	log("debugInfo: %s", DebugInfo::instance()->infoAtDistance(400).c_str());
}


void MapDataGeneratorTest::testGenerateByLevel()
{
	//generateMapLinesFromLevel
	
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	generator->init(map, player, 0);
	
	MapLevelData *levelData = MapLevelDataCache::instance()->getMap(1);
	
	//	generator->
	generator->generateMapLinesFromLevel(levelData);
	generator->generateItemsFromLevel(levelData);
	generator->generateEnemiesFromLevel(levelData);
	generator->generateItemsFromLevel(levelData);
	
	// Show the data in console
	log("Generator:\n%s", generator->info().c_str());
	
	
	// Show the data visually
	updateMap(generator);

	log("debugInfo: %s", DebugInfo::instance()->infoAtDistance(400).c_str());
}



void MapDataGeneratorTest::testGenerateCapsule()
{
	log("this is a sample subTest");
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	// Setting the testing Map
	//
	//int testMap = 0;
	int testMap = 1;
	
	//
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 100);
	//generator->setCapsuleType(ItemEffect::Type::Missile, 3);
	
	
	//	generator->
	generator->generateByMapID(testMap);
	
	
	log("Generator:\n%s", generator->info().c_str());
	
	
	delete generator;	// Don't need, go for destructor
}



void MapDataGeneratorTest::testGenerateManyCapsule()
{
	log("this is a sample subTest");
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	// Setting the testing Map
	//
	int testMap = 0;
	
	int mapStart = 100;
	int mLastCapPos = -1;
	for(int i=0; i<4; i++) {
		//
		MapDataGenerator *generator = new MapDataGenerator();
		generator->init(map, player, mapStart);
		generator->setCapsuleType(ItemEffect::Type::ItemEffectMissile, 3, 3);
		generator->setLastCapsulePos(mLastCapPos);
		
		
		generator->generateByMapID(testMap);
		
		log("Map %d:\n%s", mapStart, generator->infoItem().c_str());
		
		// update the distance data
		mapStart = generator->getRangeEnd();
		mLastCapPos = generator->getLastCapsulePos();
		
		
		delete generator;	// Don't need, go for destructor
	}
	
	
}


void MapDataGeneratorTest::testTutorialMap()
{
	int mapID = 10001;
	//int mapID = 41;
	
	GameMap *map = GameWorld::instance()->getMap();
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator *generator = new MapDataGenerator();
	generator->init(map, player, 0);
	generator->generateByMapID(mapID);
	
	log("Generator:\n%s", generator->info().c_str());
	
	updateMap(generator);
}

void MapDataGeneratorTest::testRandomLine()
{
	MapLevelData *levelData = MapLevelDataCache::instance()->getMap(20002);

//	void getRandomLineData(Vector<LevelLineData *> &outLineArray);
	
	Vector<LevelLineData *> result;
	
	levelData->getRandomLineData(result);

	for(LevelLineData *line : result) {
		log("debug: %s", line->toString().c_str());
	}
	
}

#endif
