#ifdef ENABLE_TDD
//
//  HorizontalListViewTest.m
//	TDD Framework
//
//
#include "HorizontalListViewTest.h"
#include "TDDHelper.h"
#include "HorizontalListView.h"
#include "StringHelper.h"
#include "ViewHelper.h"

void HorizontalListViewTest::setUp()
{
	log("TDD Setup is called");
}


void HorizontalListViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void HorizontalListViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void HorizontalListViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void HorizontalListViewTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testItemList2);
	ADD_TEST(testCallback);
}

#pragma mark -
#pragma mark Sub Test Definition
void HorizontalListViewTest::testSample()
{
	log("this is a sample subTest");
	
	HorizontalListView *listView = HorizontalListView::create();
	listView->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);
	listView->setBackGroundColor(Color3B::YELLOW);
	listView->setContentSize(Size(320, 200));
	listView->setPosition(Vec2(0, 200));
	
	
	addChild(listView);
	
	
	
	// Adding Item View
	std::vector<Color4B> colorList;
	colorList.push_back(Color4B::RED);
	colorList.push_back(Color4B::BLUE);
	colorList.push_back(Color4B::GREEN);
	colorList.push_back(Color4B::ORANGE);
	colorList.push_back(Color4B::MAGENTA);
	
	Size pageSize = Size(150, 160);
	
	// Setting the page
	for(Color4B color : colorList) {
		//LayerColor *layer = LayerColor::create(color, pageSize.width, pageSize.height);
		Layout *layer = Layout::create();
		layer->setContentSize(pageSize);
		layer->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
		layer->setBackGroundColor(Color3B(color));
		listView->addItem(layer);
	}
	
	//
	listView->scrollToItem(0);

	//
	mListView = listView;
}


void HorizontalListViewTest::testItemList2()
{
	log("this is a sample subTest");
	
	HorizontalListView *listView = HorizontalListView::create();
	listView->setBackGroundColorType(ui::Layout::BackGroundColorType::SOLID);
	listView->setBackGroundColor(Color3B::YELLOW);
	listView->setContentSize(Size(320, 200));
	listView->setPosition(Vec2(0, 200));
	
	
	addChild(listView);
	
	
	
	// Adding Item View
	std::vector<Color4B> colorList;
	colorList.push_back(Color4B::RED);
	colorList.push_back(Color4B::BLUE);
	colorList.push_back(Color4B::GREEN);
	colorList.push_back(Color4B::ORANGE);
	colorList.push_back(Color4B::MAGENTA);
	
	Size pageSize = Size(150, 160);
	
	// Setting the page
	for(Color4B color : colorList) {
		//LayerColor *layer = LayerColor::create(color, pageSize.width, pageSize.height);
		Layout *layer = Layout::create();
		layer->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
		layer->setContentSize(pageSize);
		ViewHelper::setSolidBackground(layer, color);
		listView->addItem(layer);
		
	}
	
	//
	listView->scrollToItem(0);
	
	//
	mListView = listView;
}




void HorizontalListViewTest::testCallback()
{
	testSample();
	
	mListView->addEventListener([&](Ref *ref, HorizontalListView::EventType event) {
		log("Item Event");
		
		switch(event) {
			case HorizontalListView::EventType::TURNING: {
				log("Page Turned");
				HorizontalListView *view = (HorizontalListView *) ref;
				log("Selected: %d", (int) view->getSelectedIndex());
			}
		}
	});
	
	mListView->setScrollCallback([&](Ref *ref,  Vec2 scrollOffset){
		log("scroll: %s", POINT_TO_STR(scrollOffset).c_str());
	});
}


#endif
