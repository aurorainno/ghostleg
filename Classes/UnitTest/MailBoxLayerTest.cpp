#ifdef ENABLE_TDD
//
//  MailBoxLayerTest.m
//	TDD Framework
//
//
#include "MailBoxLayerTest.h"
#include "MailboxLayer.h"
#include "MailItemView.h"
#include "TDDHelper.h"

void MailBoxLayerTest::setUp()
{
	log("TDD Setup is called");
}


void MailBoxLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void MailBoxLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void MailBoxLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void MailBoxLayerTest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(testItemView);
}

#pragma mark -
#pragma mark Sub Test Definition
void MailBoxLayerTest::testSample()
{
	log("this is a sample subTest");
    MailboxLayer* layer = MailboxLayer::create();
    
    Vector<AstrodogMail*> mailList{};
    AstrodogAttachment attachment;
    attachment.type = AstrodogAttachment::TypeDiamond;
    attachment.amount = 5;
    
    AstrodogAttachment attachment2;
    attachment2.type = AstrodogAttachment::TypeStar;
    attachment2.amount = 200;
    
    AstrodogAttachment attachment3;
    attachment3.type = AstrodogAttachment::TypePuzzle;
    attachment3.amount = 1;
    
    // Mail
    AstrodogMail *mail = new AstrodogMail();
    
    mail->setMailID(2);
    mail->setTitle("Hello");
    mail->setMessage("diamond");
    mail->setAttachment(attachment);
    mail->setCreateTime(12312312l);

    mailList.pushBack(mail);
    
    AstrodogMail *mail2 = new AstrodogMail();
    
    mail2->setMailID(3);
    mail2->setTitle("Hello");
    mail2->setMessage("coins");
    mail2->setAttachment(attachment2);
    mail2->setCreateTime(12312312l);
    
    mailList.pushBack(mail2);
    
    AstrodogMail *mail3 = new AstrodogMail();
    
    mail3->setMailID(4);
    mail3->setTitle("Hello");
    mail3->setMessage("fragment");
    mail3->setAttachment(attachment3);
    mail3->setCreateTime(12312312l);
    
    mailList.pushBack(mail3);
    
    
    layer->loadMailListView(mailList);
    addChild(layer);
}

void MailBoxLayerTest::testItemView()
{
    AstrodogAttachment attachment;
    attachment.type = AstrodogAttachment::TypeDiamond;
    attachment.amount = 5;
    
    // Mail
    AstrodogMail *mail = new AstrodogMail();
    
    mail->setMailID(1234);
    mail->setTitle("Hello");
    mail->setMessage("Fun fun fun fun!");
    mail->setAttachment(attachment);
    mail->setCreateTime(12312312l);

    
    MailItemView *itemView = MailItemView::create();
    itemView->setView(mail);
    itemView->setPosition(Vec2(0,568/2));
    addChild(itemView);
}


#endif
