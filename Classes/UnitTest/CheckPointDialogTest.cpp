#ifdef ENABLE_TDD
//
//  CheckPointDialogTest.m
//	TDD Framework
//
//
#include "CheckPointDialogTest.h"
#include "TDDHelper.h"
#include "CheckPointDialog.h"

void CheckPointDialogTest::setUp()
{
	log("TDD Setup is called");
	
	mDialog = nullptr;
}


void CheckPointDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void CheckPointDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void CheckPointDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void CheckPointDialogTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void CheckPointDialogTest::testSample()
{
	log("this is a sample subTest");
	if(mDialog == nullptr) {
		CheckPointDialog *dialog = CheckPointDialog::create();
		addChild(dialog);
		mDialog = dialog;
	}
	
	mDialog->setScore(2345);
	mDialog->setTime(123.23f);
	mDialog->setMoney(999);
	
	mDialog->showUp();
}


#endif
