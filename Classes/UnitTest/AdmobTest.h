#ifdef ENABLE_TDD
//
//  AdmobTest.h
//
//
#ifndef __TDD_AdmobTest__
#define __TDD_AdmobTest__

// Include Header

#include "TDDBaseTest.h"

class AdmobHandler;

// Class Declaration
class AdmobTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testBanner();
	void testInter();
	void testVideo();
	void hideBanner();
	void testVideo1();
	void testVideo2();
	
private:
	AdmobHandler *mHandler;
};

#endif

#endif
