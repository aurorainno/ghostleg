#ifdef ENABLE_TDD
//
//  ModelGalleryTest.h
//
//
#ifndef __TDD_ModelGalleryTest__
#define __TDD_ModelGalleryTest__

// Include Header

#include "TDDTest.h"

class GameModel;

// Class Declaration 
class ModelGalleryTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
	void setAnimation(const char *name);
	void setAction(int action);
	
private:
	void changeAnime(Ref *sender);
	void changeAction(Ref *sender);
	void changeDir(Ref *sender);
	void getModelInfo(Ref *sender);
	void addParticleEffect(Ref *sender);
	
private:
	int mAction;
	GameModel *mModel;
	Label *mAnimeLabel;
	Label *mActionLabel;
	Label *mDirLabel;
};

#endif

#endif
