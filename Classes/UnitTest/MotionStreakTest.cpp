#ifdef ENABLE_TDD
//
//  MotionStreakTest.m
//	TDD Framework
//
//
#include "MotionStreakTest.h"
#include "TDDHelper.h"
#include "ViewHelper.h"
#include "VisibleRect.h"
#include "StageParallaxLayer.h"

void MotionStreakTest::setUp()
{
	log("TDD Setup is called");
	
	// Setup the space background
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();

	
	// Main Node
	ui::Layout *box = ui::Layout::create();
	box->setBackGroundColor(Color3B::RED);
	box->setContentSize(Size(20, 20));
	box->setAnchorPoint(Vec2(0.5f, 0.5f));
	mMainNode = box;
	
	//mMainNode = LayerColor::create(Color4B::RED, 20, 20);
	mMainNode->setPosition(VisibleRect::center());
	addChild(mMainNode);
	
	
	// motion streak
	std::string textureName = StringUtils::format("motion/tail_008.png");
	Texture2D *image = Director::getInstance()->getTextureCache()->addImage(textureName);
	
	// float fade, float minSeg, float stroke,
	MotionStreak *motion = MotionStreak::create(1.5f, 10, 25, Color3B::WHITE, image);
	motion->setFastMode(true);
	addChild(motion);
	mMotion = motion;
	mMotion->setPosition(mMainNode->getPosition());
	
	
	// 
	setupTouchListener();
	
	scheduleUpdate();
}

void MotionStreakTest::update(float delta)
{
	mMotion->setPosition(mMainNode->getPosition());
}


void MotionStreakTest::tearDown()
{
	log("TDD tearDown is called");
}

void MotionStreakTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void MotionStreakTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


void MotionStreakTest::moveObject(const Vec2 &diff)
{
	if(mMainNode == nullptr) {
		return;
	}
	Vec2 position = mMainNode->getPosition();
	position += diff;
	
	mMainNode->setPosition(position);
}

void MotionStreakTest::setupTouchListener()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	
	touchListener->onTouchBegan =
 CC_CALLBACK_2(MotionStreakTest::onTouchBegan, this);
	touchListener->onTouchEnded =
 CC_CALLBACK_2(MotionStreakTest::onTouchEnded, this);
	touchListener->onTouchMoved =
	CC_CALLBACK_2(MotionStreakTest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(MotionStreakTest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
}

bool MotionStreakTest::onTouchBegan(Touch *touch, Event *event)
{
	mLastPos = touch->getLocation();
	
	return true;
}

void MotionStreakTest::onTouchEnded(Touch *touch, Event *event)
{
}

void MotionStreakTest::onTouchMoved(Touch *touch, Event *event)
{
	Vec2 newPos = touch->getLocation();
	Vec2 diff = newPos - mLastPos;
	mLastPos = newPos;
	
	//log("MovementDiff: %s", POINT_TO_STR(diff).c_str());
	moveObject(diff);
}

void MotionStreakTest::onTouchCancelled(Touch *touch, Event *event)
{
	
}

#pragma mark -
#pragma mark List of Sub Tests

void MotionStreakTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void MotionStreakTest::testSample()
{
	log("this is a sample subTest");
}


#endif
