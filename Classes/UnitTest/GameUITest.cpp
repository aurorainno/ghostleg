#ifdef ENABLE_TDD
//
//  GameUITest.m
//	TDD Framework 
//
//
#include "GameUITest.h"
#include "TDDHelper.h"
#include "GameUI.h"

#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include <time.h>
#include "VisibleRect.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;
using namespace cocostudio::timeline;

#define kTagParticle		12345
#define kTagParticleFire	12346

void GameUITest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void GameUITest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void GameUITest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(GameUITest::subTest);
	SUBTEST(GameUITest::testMask);
	SUBTEST(GameUITest::testMask2);
	SUBTEST(GameUITest::testParticle);
	SUBTEST(GameUITest::testTestUI);
	SUBTEST(GameUITest::testParticleAutoRemove);
	SUBTEST(GameUITest::testCheckRemove);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameUITest::testCheckRemove(Ref *sender)
{
	const int nCheck = 2;
	int checkingTags[nCheck] = {kTagParticle, kTagParticleFire};
	
	for(int i=0; i<nCheck; i++) {
		int tag = checkingTags[i];
		
		Node *node = getChildByTag(tag);
		log("tag=%d node=%s", tag, (node != nullptr ? "true" : "false"));
	}
	
}

void GameUITest::testParticleAutoRemove(Ref *sender)
{
	ParticleSystem *emitter = ParticleSystemQuad::create("particle_missileexplode.plist");
	emitter->setAutoRemoveOnFinish(true);
	emitter->setPosition(VisibleRect::center());
	emitter->setTag(kTagParticle);
	this->addChild(emitter);
}


void GameUITest::testParticle(Ref *sender)
{
	ParticleSystem *emitter = ParticleSystemQuad::create("particle_fire.plist");
	
	emitter->setPosition(VisibleRect::center());
	emitter->setTag(kTagParticleFire);
	this->addChild(emitter);
}


void GameUITest::subTest(Ref *sender)
{
	log("this is a sample subTest");
	
	ui::Button *button = ui::Button::create("popup_btn_03.png");
	button->setScale(0.5f);
	button->setZoomScale(1.5);
	button->setTitleText("test");
	button->setContentSize(Size(100, 40));
	button->setPosition(Vec2(150, 50));
	
	addChild(button);
}


void GameUITest::testTestUI(Ref *sender)
{
	
	Node *rootNode = CSLoader::createNode("UiTestScene.csb");
	addChild(rootNode);
	
	Button *button = (Button *) rootNode->getChildByName("testButton");
	// button->setZoomScale(1.5);
	
	button->setEnabled(true);
}

// http://www.onemoresoftwareblog.com/2013/12/cocos2d-x-ccclippingnode-triple-c.html
void GameUITest::testMask(Ref *sender)
{
	LayerColor *bgColor = LayerColor::create(Color4B::BLACK);
	addChild(bgColor);
	

	// Create stencil
	Node *stencil = Node::create();
	Sprite *mask = Sprite::create("circle_mask.png");
	mask->setPosition(Vec2(200, 200));
	stencil->addChild(mask);

	
	// The mask (clipping node)
	ClippingNode *maskNode = ClippingNode::create();
	maskNode->setColor(Color3B::BLACK);
	maskNode->setInverted(false);
	maskNode->setStencil(stencil);

	addChild(maskNode);
	
	
	Node *rootNode = CSLoader::createNode("UiTestScene.csb");
	maskNode->addChild(rootNode);
	
	
//	Mask *mask = Mask::create
//	
//	Node *rootNode = CSLoader::createNode("UiTestScene.csb");
//	addChild(rootNode);
//	
//	Button *button = (Button *) rootNode->getChildByName("testButton");
//	// button->setZoomScale(1.5);
//	
//	button->setEnabled(true);
}

// http://discuss.cocos2d-x.org/t/masking-moving-sprite-with-static-sprite-mask-or-using-a-sprite-mask-to-stencil-a-layer/12572/3
void GameUITest::testMask2(Ref *sender)
{
	
	Node *rootNode = CSLoader::createNode("UiTestScene.csb");
	addChild(rootNode);

	
	// Mask Layer
	LayerColor *maskLayer = LayerColor::create(Color4B(0,0,0,200));
	//addChild(maskLayer);
	
	
	// Mask Sprite
	Sprite *mask = Sprite::create("circle_mask.png");
	mask->setPosition(Vec2(0, 0));
	
	// Create stencil
	Node *stencil = Node::create();
	stencil->addChild(mask);
	
	
	// The mask (clipping node)
	ClippingNode *maskNode = ClippingNode::create();
	maskNode->setInverted(true);
	maskNode->setAlphaThreshold(0);
	maskNode->setStencil(stencil);
	maskNode->addChild(maskLayer);
	
	addChild(maskNode);
	
	
	std::function<void(float)> myUpdate = [mask](float delta) {
		// log("delta=%f", delta);
		Vec2 currentPos = mask->getPosition();
		currentPos += Vec2(10, 20) * delta;
		mask->setPosition(currentPos);
	};
	
	schedule(myUpdate, "moveMask");
}

#endif
