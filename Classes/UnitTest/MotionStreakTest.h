#ifdef ENABLE_TDD
//
//  MotionStreakTest.h
//
//
#ifndef __TDD_MotionStreakTest__
#define __TDD_MotionStreakTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class MotionStreakTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	
	
	
#pragma mark - Touch Handling
	void moveObject(const Vec2 &diff);
	void setupTouchListener();
	bool onTouchBegan(Touch *touch, Event *event);
	void onTouchEnded(Touch *touch, Event *event);
	void onTouchMoved(Touch *touch, Event *event);
	void onTouchCancelled(Touch *touch, Event *event);

private:
	void update(float delta);
	
private:
	void testSample();
	
private:
	Node *mMainNode;
	MotionStreak *mMotion;
	Vec2 mLastPos;
};

#endif

#endif
