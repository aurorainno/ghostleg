#ifdef ENABLE_TDD
//
//  DailyRewardDialogTest.m
//	TDD Framework
//
//
#include "DailyRewardDialogTest.h"
#include "TDDHelper.h"
#include "DailyRewardDialog.h"
#include "DailyReward.h"

void DailyRewardDialogTest::setUp()
{
	log("TDD Setup is called");
}


void DailyRewardDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void DailyRewardDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DailyRewardDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DailyRewardDialogTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testRewardAnimation);
	ADD_TEST(testCallback);
	ADD_TEST(testCollectStatus);
	ADD_TEST(testSetRewardData);
}

#pragma mark -
#pragma mark Sub Test Definition
void DailyRewardDialogTest::testSample()
{
	
	DailyRewardDialog *dialog = DailyRewardDialog::create();
	
	addChild(dialog);
}


void DailyRewardDialogTest::testCallback()
{
	
	DailyRewardDialog *dialog = DailyRewardDialog::create();
	
	addChild(dialog);
	
	dialog->setCallback([&, dialog](){
		dialog->closeDialog();
	});
}

void DailyRewardDialogTest::testCollectStatus()
{
	
	DailyRewardDialog *dialog = DailyRewardDialog::create();
	
	addChild(dialog);
	
	
	for(int day=1; day<=5; day++) {
		bool hasCollect = day <= 3;
		
		dialog->setCollectStatus(day, hasCollect);
	}
	
	
	
	
}


void DailyRewardDialogTest::testSetRewardData()
{
	// Prepare the reward
	Vector<RewardData *> rewardList;
	
	for(int i=0; i<4; i++) {	// Day 1 ~ Day 4
		RewardData *data = RewardData::create(RewardData::RewardTypeStar, 0, 100 * (i+1));
		
		rewardList.pushBack(data);
	}
	

	
	
	
	// Create the dialog
	DailyRewardDialog *dialog = DailyRewardDialog::create();
	
	addChild(dialog);
	
	// Setting the reward
	int day = 1;
	for(RewardData *reward : rewardList) {
		dialog->setRewardAtDay(day, reward);
 
        bool collected = day <= 3;
		dialog->setCollectStatus(day, collected);
		day++;
	}
	

}



void DailyRewardDialogTest::testRewardAnimation()
{
	
	DailyRewardDialog *dialog = DailyRewardDialog::create();
	
	addChild(dialog);

	// For quick checking
	for(int day=1; day<=5; day++) {
		dialog->setCollectStatus(day, false);
//		dialog->showRewardCollectAnimation(day);	// for quick test
	}
	
	
	dialog->showRewardCollectAnimation(1); // input: day
	
	
}

#endif
