#ifdef ENABLE_TDD
//
//  NPCChanceTest.m
//	TDD Framework
//
//
#include "NPCChanceTest.h"
#include "TDDHelper.h"
#include "NPCChanceTable.h"
#include "StringHelper.h"

void NPCChanceTest::setUp()
{
	log("TDD Setup is called");
}


void NPCChanceTest::tearDown()
{
	log("TDD tearDown is called");
}

void NPCChanceTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NPCChanceTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NPCChanceTest::defineTests()
{
	ADD_TEST(testRollNpc);
	ADD_TEST(testLoad);
}

#pragma mark -
#pragma mark Sub Test Definition
void NPCChanceTest::testLoad()
{
	log("this is a sample subTest");
	
	NPCChanceTable chanceTable;
	
	std::string jsonContent =
	  "["
		"  {\"101\":10, \"102\":10, \"103\":30, \"104\":30, \"105\":10 }, "
		"  {\"103\":20, \"104\":30, \"105\":20, \"109\":30 } "
	 "]";
	
	
	
	chanceTable.parseJSONContent(jsonContent);
	
	log("Result:\n%s\n", chanceTable.toString().c_str());
}

void NPCChanceTest::testRollNpc()
{
	log("this is a sample subTest");
	
	NPCChanceTable chanceTable;
	chanceTable.setupMockData();

	std::vector<int> result = chanceTable.rollNpc(0);
	
	log("Result: %s", VECTOR_TO_STR(result).c_str());
}


#endif
