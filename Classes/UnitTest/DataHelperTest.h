#ifdef ENABLE_TDD
//
//  DataHelperTest.h
//
//
#ifndef __TDD_DataHelperTest__
#define __TDD_DataHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class DataHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testDequeue();
	void testDequeuePointer();
};

#endif

#endif
