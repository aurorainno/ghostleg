#ifdef ENABLE_TDD
//
//  PlayerAbilityTest.h
//
//
#ifndef __TDD_PlayerAbilityTest__
#define __TDD_PlayerAbilityTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class PlayerAbilityTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testMainAttribute();
	void testAddingSetting();
	void testGlobalGameplaySetting();
	void testMockAbility();
	void testGameplaySetting();
	void testBonusReward();
	void testAttributeBuff();
	void testAbilityDesc();
	void testBonusAttribute();
	
	
#pragma mark - Booster & PowerUp
	void testSlowerMissile();
	void testPowerUp();
	void testLongerBooster();
};

#endif

#endif
