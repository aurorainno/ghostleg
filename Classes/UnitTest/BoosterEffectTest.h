#ifdef ENABLE_TDD
//
//  BoosterEffectTest.h
//
//
#ifndef __TDD_BoosterEffectTest__
#define __TDD_BoosterEffectTest__

// Include Header

#include "TDDBaseTest.h"

class Player;
class ItemEffect;

// Class Declaration
class BoosterEffectTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	
private:
	void testSpeedUp();
	void testDifferentAngle();
	
	
private:
	Player *mPlayer;
	ItemEffect *mEffect;
	float mRotation = 0;
};

#endif

#endif
