#ifdef ENABLE_TDD
//
//  MasterySceneTest.h
//
//
#ifndef __TDD_MasterySceneTest__
#define __TDD_MasterySceneTest__

// Include Header

#include "TDDTest.h"
#include "MasteryScene.h"
// Class Declaration 
class MasterySceneTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void simpleTest(Ref *sender);
	void testDiffLevel(Ref *sender);
	void testMasteryScene(Ref *sender);
	void testCallback(Ref *sender);
    void testNotEnoughDialog(Ref *sender);
    
    MasterySceneLayer *mLayer;
}; 

#endif

#endif
