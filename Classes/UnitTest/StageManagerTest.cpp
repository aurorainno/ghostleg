#ifdef ENABLE_TDD
//
//  StageManagerTest.m
//	TDD Framework
//
//
#include "StageManagerTest.h"
#include "TDDHelper.h"
#include "StageManager.h"
#include "StageData.h"
#include "StringHelper.h"
#include "LevelData.h"
#include "NPCOrderData.h"
#include "GameplaySetting.h"
#include "GameWorld.h"

void StageManagerTest::setUp()
{
	log("TDD Setup is called");
	
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	StageManager::instance()->setGameplaySetting(setting);
	log("GameSetting:%s\n", setting->toString().c_str());

}


void StageManagerTest::tearDown()
{
	log("TDD tearDown is called");
}

void StageManagerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void StageManagerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void StageManagerTest::defineTests()
{
	ADD_TEST(loadStage0);
	ADD_TEST(testSample);
	ADD_TEST(testRollPuzzle);
	ADD_TEST(testPuzzleChanceTable);
	ADD_TEST(testNextNPCOrder);
	ADD_TEST(testRollOneNpc);
	ADD_TEST(testNpcOrderGetRandomMaps);
	ADD_TEST(testGetRandomMaps);
	ADD_TEST(testGetRequireMaps);
	ADD_TEST(testRollNpc);
	ADD_TEST(testGetNextNPCOrderList);
	ADD_TEST(testParsingData);
	ADD_TEST(testStageData);
	ADD_TEST(testGetAllGameMap);
	ADD_TEST(testPlayerNpcOrder);
}

#pragma mark -
#pragma mark Sub Test Definition

void StageManagerTest::loadStage0()
{
	StageManager::instance()->loadStageData(0);
	StageData *stageData = StageManager::instance()->getStageData(0);
	
	log("stageData: %s", stageData->toString().c_str());
}

void StageManagerTest::testRollPuzzle()
{
	StageManager::instance()->setCurrentStage(1);
	StageManager::instance()->reset();
	
	int numCollect = 3;
	std::vector<int> result = StageManager::instance()->rollRewardPuzzle(numCollect);
	
	log("result: %s", VECTOR_TO_STR(result).c_str());
}



void StageManagerTest::testPuzzleChanceTable()
{
	StageManager::instance()->setCurrentStage(1);
	StageManager::instance()->reset();
	
	
	log("%s", StageManager::instance()->infoPuzzleChanceTable().c_str());
}


void StageManagerTest::testNpcOrderGetRandomMaps()
{
	int npcID = 101;
	NPCOrderData *data = StageManager::instance()->getNpcOrderData(npcID);
	GameplaySetting *setting = GameplaySetting::create();
	
	NPCOrder *order = NPCOrder::create();
	order->setup(data, setting);
	
	for(int returnSize=0; returnSize<6; returnSize++) {
		std::vector<int> maps = order->getRandomMaps(returnSize);
		
		log("return=%d maps=%s", returnSize, VECTOR_TO_STR(maps).c_str());
	}
}

void StageManagerTest::testGetRandomMaps()
{
	std::vector<int> testMaps;
	int repeat = 5;
	
	testMaps.push_back(101);
	testMaps.push_back(102);
	
	for(int map : testMaps) {
		NPCOrderData *data = StageManager::instance()->getNpcOrderData(map);
		
		for(int i=0; i<repeat; i++) {
			std::vector<int> maps = data->getRandomMaps();
			
			log("MAPID: %d loop-%d: %s", map, i, VECTOR_TO_STR(maps).c_str());
		}
	}
}


void StageManagerTest::testGetRequireMaps()
{
	//StageManager::ins
	NPCOrderData *data = StageManager::instance()->getNpcOrderData(101);
	
	log("DEBUG: data=%s", data->toString().c_str());
	log("DEBUG: map=%s", data->infoMap().c_str());
	
	std::vector<int> maps = data->getRequireMapList();
	log("DEBUG: requiredMap=%s", VECTOR_TO_STR(maps).c_str());
	
	// Using Old Data Format
	
	data = StageManager::instance()->getNpcOrderData(102);
	
	log("DEBUG: data=%s", data->toString().c_str());
	log("DEBUG: map=%s", data->infoMap().c_str());
	
	maps = data->getRequireMapList();
	log("DEBUG: requiredMap=%s", VECTOR_TO_STR(maps).c_str());
}

void StageManagerTest::testGetAllGameMap()
{
	std::vector<int> maps = StageManager::instance()->getAllGameMap();
	
	log("maps=%ld", maps.size());
	log("%s", VECTOR_TO_STR(maps).c_str());
	
	std::vector<int> available = MapLevelDataCache::instance()->getAvailableMaps();
	log("Available Maps:\n%s", VECTOR_TO_STR(available).c_str());
}

void StageManagerTest::testSample()
{
	log("this is a sample subTest");
	StageManager::instance()->setupMockData();
	
	log("%s", StageManager::instance()->infoStageData().c_str());
}

void StageManagerTest::testStageData()
{
	log("%s", StageManager::instance()->infoStageData().c_str());
}

void StageManagerTest::testParsingData()
{
	//std::string content = FileUtils::getInstance()->getStringFromFile("level/stage/stage_001.json");
	std::string content = FileUtils::getInstance()->getStringFromFile("level/stage/stage_002.json");
	
	log("CONTENT\n%s\n", content.c_str());
	
	StageData *stageData = StageData::create();
	stageData->parseJSONContent(content);
	
	log("INFO:\n%s\n", stageData->toString().c_str());
}

void StageManagerTest::testGetNextNPCOrderList()
{
	
	Vector<NPCOrder *> orderList = StageManager::instance()->getNextNPCOrderList();
	
	for(int i=0; i<orderList.size(); i++) {
		NPCOrder *order = orderList.at(i);
		
		log("%s", order->toString().c_str());
		
		if(i == 0) {
			StageManager::instance()->setNpcOrderDone(order->getOrderID());
		}
	}
	
}


void StageManagerTest::testPlayerNpcOrder()
{
	NPCOrderData *orderData = StageManager::instance()->getNpcOrderData(101);
	GameplaySetting *setting = GameplaySetting::create();
	
	setting->setAttributeValue(GameplaySetting::BonusTipPercent, 0.01);
	
	NPCOrder *order = NPCOrder::create();
	order->setup(orderData, setting);
	
	
	log("NPCOrder: %s", order->toString().c_str());
	
}

void StageManagerTest::testNextNPCOrder()
{
	StageManager::instance()->setCurrentStage(1);
	StageManager::instance()->reset();
	int nTrial = 20;
	
	for(int i=0; i<nTrial; i++) {
		NPCOrder *order = StageManager::instance()->getNextNPCOrder();
		
		log("%s", order->toString().c_str());
		
		if(order->getIsLastOrder()){
			break;
		}
		
		StageManager::instance()->setNpcOrderDone(order->getOrderID());
	}

}

void StageManagerTest::testRollOneNpc()
{
	StageData *stageData = StageManager::instance()->getStageData(1);
	
	int nTrial = 20;
	
	int orderServed = 0;
	
	for(int i=0; i<nTrial; i++) {
		int result = stageData->rollOneNpc(orderServed);
		bool isLastOrder = stageData->isLastOrder(orderServed);
		
		
		log("orderServed: %d  result=%d isLast=%d", orderServed, result, isLastOrder);
		
		orderServed++;
		if(isLastOrder) {
			break;
		}
	}

}

void StageManagerTest::testRollNpc()
{
	StageData *stageData = StageManager::instance()->getStageData(1);
	
	int nTrial = 20;

	int orderServed = 0;
	
	for(int i=0; i<nTrial; i++) {
		std::vector<int> result = stageData->rollNpc(orderServed);
		
		
		log("orderServed: %d  result=%s", orderServed, VECTOR_TO_STR(result).c_str());
		
		orderServed++;
	}
	
	
}

#endif
