#ifdef ENABLE_TDD
//
//  TutorialNpcDialogTest.h
//
//
#ifndef __TDD_TutorialNpcDialogTest__
#define __TDD_TutorialNpcDialogTest__

// Include Header

class TutorialNpcDialog;

#include "TDDBaseTest.h"

// Class Declaration
class TutorialNpcDialogTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testShow();
	void testClose();
	
private:
	TutorialNpcDialog *mDialog;
};

#endif

#endif
