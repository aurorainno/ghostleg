#ifdef ENABLE_TDD
//
//  PositionRecorderTest.m
//	TDD Framework
//
//
#include "PositionRecorderTest.h"
#include "TDDHelper.h"
#include "PositionRecorder.h"
#include "StringHelper.h"

void PositionRecorderTest::setUp()
{
	log("TDD Setup is called");
}


void PositionRecorderTest::tearDown()
{
	log("TDD tearDown is called");
}

void PositionRecorderTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PositionRecorderTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PositionRecorderTest::defineTests()
{
	ADD_TEST(testRecordPosition);
	ADD_TEST(testNearestSpawnPosition);
}

#pragma mark -
#pragma mark Sub Test Definition
void PositionRecorderTest::testRecordPosition()
{
	PositionRecorder *recorder = PositionRecorder::create();
	recorder->setSampleSize(10);
	
	int speed = 300;
	float delta = 0.1f;
	
	int loop = 20;
	Vec2 pos = Vec2(100, 200);
	
	for(int i=0; i<loop; i++) {
		recorder->recordPosition(pos, delta);
		
		pos.y += speed * delta;
	}
	
	log("%s", recorder->toString().c_str());
}

void PositionRecorderTest::testNearestSpawnPosition()
{
	std::vector<Vec2> positionList;
	positionList.push_back(Vec2(120.00,206.36));
	positionList.push_back(Vec2(120.00,261.44));
	positionList.push_back(Vec2(120.00,314.55));
	positionList.push_back(Vec2(120.00,380.40));
	positionList.push_back(Vec2(120.00,446.10));
	positionList.push_back(Vec2(120.00,485.38));
	positionList.push_back(Vec2(146.83,586.53));
	
	
	PositionRecorder *recorder = PositionRecorder::create();
	for(Vec2 pos : positionList) {
		recorder->recordPosition(pos, 1);
	}
	
	log("%s", recorder->toString().c_str());
	
	Vec2 playerPos = Vec2(120, 485);
	
	Vec2 resultPos;
	bool isOkay = recorder->findNearestSpawnPosition(playerPos, resultPos);
	
	log("isOkay=%d result=%s", isOkay, POINT_TO_STR(resultPos).c_str());
}


#endif
