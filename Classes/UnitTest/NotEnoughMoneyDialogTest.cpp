#ifdef ENABLE_TDD
//
//  NotEnoughMoneyDialogTest.m
//	TDD Framework
//
//
#include "NotEnoughMoneyDialogTest.h"
#include "TDDHelper.h"
#include "NotEnoughMoneyDialog.h"

void NotEnoughMoneyDialogTest::setUp()
{
	log("TDD Setup is called");
}


void NotEnoughMoneyDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void NotEnoughMoneyDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NotEnoughMoneyDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NotEnoughMoneyDialogTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testDialogMode);
}

#pragma mark -
#pragma mark Sub Test Definition
void NotEnoughMoneyDialogTest::testSample()
{
	NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
	dialog->setMode(NotEnoughMoneyDialog::DiamondMode);
	addChild(dialog);
}

void NotEnoughMoneyDialogTest::testDialogMode()
{
	NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
	dialog->setMode(NotEnoughMoneyDialog::DiamondMode);
	dialog->setRedirectByScene(false);
	addChild(dialog);
}


#endif
