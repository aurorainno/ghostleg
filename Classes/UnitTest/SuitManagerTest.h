#ifdef ENABLE_TDD
//
//  SuitManagerTest.h
//
//
#ifndef __TDD_SuitManagerTest__
#define __TDD_SuitManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class SuitManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void loadData();
	void testInfo();
	void getCapsuleItemEffectAndValue();
	void testDescription();
}; 

#endif

#endif
