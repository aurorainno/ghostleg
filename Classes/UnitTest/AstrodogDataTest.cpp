#ifdef ENABLE_TDD
//
//  AstrodogDataTest.m
//	TDD Framework
//
//
#include "AstrodogDataTest.h"
#include "TDDHelper.h"
#include "AstrodogData.h"
//#include "ADResp

void AstrodogDataTest::setUp()
{
	log("TDD Setup is called");
}


void AstrodogDataTest::tearDown()
{
	log("TDD tearDown is called");
}

void AstrodogDataTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AstrodogDataTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AstrodogDataTest::defineTests()
{
	ADD_TEST(testUser);
	ADD_TEST(testParseResponse);
}

#pragma mark -
#pragma mark Sub Test Definition
void AstrodogDataTest::testUser()
{
	AstrodogUser *user = AstrodogUser::create();
	user->setUid(3309);
	user->setFbID("13579");
	user->setName("Testing");
	user->setCountry("CN");
	
	std::string json = user->toJSONContent();
	
	log("JSON:\n%s\n", json.c_str());

	AstrodogUser *parsed = AstrodogUser::create();
	parsed->parseJSONContent(json);
	
	log("parsedUser:[%s]", parsed->toString().c_str());
}


void AstrodogDataTest::testParseResponse()
{
	std::string json = FileUtils::getInstance()->getStringFromFile("test/getRanking.json");
	log("JSON:\n%s\n", json.c_str());
	
	
}


#endif
