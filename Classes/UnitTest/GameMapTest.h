#ifdef ENABLE_TDD
//
//  GameMapTest.h
//
//
#ifndef __TDD_GameMapTest__
#define __TDD_GameMapTest__

// Include Header

#include "TDDBaseTest.h"

class GameMap;
class MapLine;

// Class Declaration 
class GameMapTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void scrollUp();
	void scrollDown();
	void clearMap();
	void showTestMap();
	void addMapLine();
	void testGetApproachingLineX();
	void removeRouteLine();
	void addTestLines();
	void testRemoveOldLines();
	void testAddSlopeLine();
	void testAdjustPositionBeforeLastDropLine();
	void testEffectRotation();
	void testIsOnVertLine();
	
private:
	GameMap *mMap;
	int mScroll;
	MapLine *mLastLine;
	
}; 

#endif

#endif
