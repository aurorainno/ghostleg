#ifdef ENABLE_TDD
//
//  StageParallaxTest.h
//
//
#ifndef __TDD_StageParallaxTest__
#define __TDD_StageParallaxTest__

class StageParallaxLayer;

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class StageParallaxTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testHideRoute();
	void testShake();

private:
	StageParallaxLayer *mParallax;
};

#endif

#endif
