#ifdef ENABLE_TDD
//
//  GameTouchLayerTest.h
//
//
#ifndef __TDD_GameTouchLayerTest__
#define __TDD_GameTouchLayerTest__

// Include Header

#include "TDDBaseTest.h"

class GameMap;
class GameTouchLayer;

// Class Declaration 
class GameTouchLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
	void handleDrawLine(const Vec2 &start, const Vec2 &end);
	
	void update(float delta);
	void updateMap();
	
private:
	void subTest();
	void play();
	void pause();
	void moveUp();
	void moveDown();
	
private:
	GameMap *mMap;
	GameTouchLayer *mTouchLayer;
	float mScrollY;
}; 

#endif

#endif
