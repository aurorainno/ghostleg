#ifdef ENABLE_TDD
//
//  EndGameTest.m
//	TDD Framework
//
//
#include "EndGameTest.h"
#include "TDDHelper.h"
#include "EndGameScene.h"
#include "GameOverDialog.h"
#include "GameWorld.h"
#include "PlayerGameResult.h"

void EndGameTest::setUp()
{
	log("TDD Setup is called");
	mDialog = nullptr;
	mLastScoreType = -1;
	
	PlayerGameResult *result = GameWorld::instance()->getGameResult();
	result->setKillScore(123);
	result->setDeliveryScore(1123);
	result->setDistanceScore(123);
	result->setClearStageScore(4123);
	result->setCollectScore(323);
	result->setBonusScore(2323);
	result->setTotalScore(2323);
	

}


void EndGameTest::tearDown()
{
	log("TDD tearDown is called");
}

void EndGameTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void EndGameTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void EndGameTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testDisplayGameScores);
	ADD_TEST(testPopText);
	ADD_TEST(testGameOverDialog);
	//ADD_TEST(
}

#pragma mark -
#pragma mark Sub Test Definition
void EndGameTest::testSample()
{
	//log("this is a sample subTest");
	EndGameSceneLayer *layer = EndGameSceneLayer::create();
	
	
	addChild(layer);
	layer->showGameOverDialog();
}

void EndGameTest::testGameOverDialog()
{
	mDialog = GameOverDialog::create();
	addChild(mDialog);
}

void EndGameTest::testDisplayGameScores()
{
	if(mDialog == nullptr) {
		return;
	}
	
	

	mDialog->hideAllPopText();
	mDialog->displayGameScores();
	
}

void EndGameTest::testPopText()
{
	if(mDialog == nullptr) {
		return;
	}
	
//	ScoreTypeKill,
//	ScoreTypeDistance,
//	ScoreTypeCollect,
//	ScoreTypeAllClear,
//	ScoreTypeBonus,
//	
//	ScoreTypeTotal,
	
	
	ScoreType scoreType;
	// Define the score type
	if(-1 == mLastScoreType || ScoreTypeTotal == mLastScoreType) {
		scoreType = ScoreTypeDelivery;
		mDialog->hideAllPopText();
	} else {
		scoreType = (ScoreType) (mLastScoreType+1);
	}

	log("Showing popScoreText for scoreType=%d", scoreType);
	//
	
	mDialog->popScoreText(scoreType, [&](){
		log("When Pop score done");
	});
	
	
	//
	mLastScoreType = scoreType;
}


#endif
