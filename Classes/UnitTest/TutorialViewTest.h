#ifdef ENABLE_TDD
//
//  TutorialViewTest.h
//
//
#ifndef __TDD_TutorialViewTest__
#define __TDD_TutorialViewTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class TutorialViewTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
	void testView(Ref *sender);
	void resetTutorial(Ref *sender);
}; 

#endif

#endif
