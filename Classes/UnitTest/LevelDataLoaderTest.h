#ifdef ENABLE_TDD
//
//  LevelDataLoaderTest.h
//
//
#ifndef __TDD_LevelDataLoaderTest__
#define __TDD_LevelDataLoaderTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class LevelDataLoaderTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testNpcMap();
	void testData();
	void testLoad();
	void testGetMap();
	void testChangePlanet();
	void testMapCache();
	void testNeedNewMap();
	void testSpeedUp();
	void testLoadAllMap();
	void testRandomLine();
	void testLoadConfig();
	void testLoadLevel();
	void testGetSection();
	void testNextMapCycle();
	void testMapGeneration();
	void testCycleInfo();
	void testGetMapList();
	void testSpeedUp2();
}; 

#endif

#endif
