#ifdef ENABLE_TDD
//
//  EndGameTest.h
//
//
#ifndef __TDD_EndGameTest__
#define __TDD_EndGameTest__

// Include Header

#include "TDDBaseTest.h"

class GameOverDialog;

// Class Declaration
class EndGameTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testGameOverDialog();
	void testPopText();
	void testDisplayGameScores();
	
private:
	GameOverDialog *mDialog;
	int mLastScoreType;
};

#endif

#endif
