#ifdef ENABLE_TDD
//
//  Vec2Test.h
//
//
#ifndef __TDD_Vec2Test__
#define __TDD_Vec2Test__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class Vec2Test : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
	void testIntersect(Ref *sender);
}; 

#endif

#endif
