#ifdef ENABLE_TDD
//
//  RateAppTest.m
//	TDD Framework 
//
//
#include "RateAppTest.h"
#include "GameManager.h"
#include "TDDHelper.h"



void RateAppTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void RateAppTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void RateAppTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(RateAppTest::subTest);
    SUBTEST(RateAppTest::resetRateNoticeFlag);
}

#pragma mark -
#pragma mark Sub Test Definition
void RateAppTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
    RateAppHelper::getInstance()->showRatePrompt(true);
}

void RateAppTest::resetRateNoticeFlag(Ref* sender)
{
    GameManager::instance()->resetRateNoticeFlag();
}


#endif
