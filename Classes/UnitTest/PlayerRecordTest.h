#ifdef ENABLE_TDD
//
//  PlayerRecordTest.h
//
//
#ifndef __TDD_PlayerRecordTest__
#define __TDD_PlayerRecordTest__

// Include Header

#include "TDDBaseTest.h"

class PlayerGameResult;

// Class Declaration 
class PlayerRecordTest : public TDDBaseTest
{
protected:
	void setupGameResult(PlayerGameResult &result, int distance,
						 int star, int dog, int planet, int kill);
	
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void subTest();
	void testIsStageClear();
	void testParsePlayerCharData();
	void testPlayerGameData();
	void testSetDogRecord();
	void testUpdateWithResult();
	void testClearRecord();
	void testSetGlobalRecord();
}; 

#endif

#endif
