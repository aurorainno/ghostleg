#ifdef ENABLE_TDD
//
//  MasteryTest.m
//
//	Start Game
//		-> resetPlayer
//			-> updateMasteryAttribute
//	In Game
//		Regenerate Energy
//		Consume Energy for Line
//		Add Potion
//
//
//	TDD Framework 
//
//
#include "MasteryTest.h"
#include "TDDHelper.h"
#include "Mock.h"
#include "Mastery.h"
#include "MasteryManager.h"
#include "GameManager.h"
#include "Player.h"

void MasteryTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	mManager = new MasteryManager();
	mManager->loadMasteryData();
}


void MasteryTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
	
	delete mManager;
}

#pragma mark -
#pragma mark List of Sub Tests

void MasteryTest::defineTests()
{
	ADD_TEST(testOneData);
	ADD_TEST(testDescription);
	ADD_TEST(testSetPlayerAttribute);
	ADD_TEST(testDataArray);
	ADD_TEST(testListMastery);
	ADD_TEST(parsePlayerMastery);
	ADD_TEST(testGetSetPlayerMastery);
	ADD_TEST(testSaveLoadMastery);
}

#pragma mark -
#pragma mark Sub Test Definition
void MasteryTest::testOneData()
{
	//log("this is a sample subTest");
	
	MasteryData *masteryData = Mock::createMasteryData(1, "Enery", "Regenerate the energy faster");
	
	
	log("toString=%s", masteryData->toString().c_str());
}


void MasteryTest::testDataArray()
{
	Vector<MasteryData *> dataArray = mManager->getMasteryArray();
	
	for(int i=0; i<dataArray.size(); i++) {
		MasteryData *mastery = dataArray.at(i);
		
		log("mastery: %s", mastery->toString().c_str());
	}
}

void MasteryTest::parsePlayerMastery()
{
	std::string info = "mastery=3 level=4";
	
	PlayerMastery playerMastery;
	
	playerMastery.parse(info);
	
	
	log("playerMastery: %s", playerMastery.toString().c_str());
}

void MasteryTest::testGetSetPlayerMastery()
{
	mManager->setPlayerMasteryLevel(1, 3);
	mManager->setPlayerMasteryLevel(2, 2);
	
	for(int i=1; i<=4; i++) {
		int level = mManager->getPlayerMasteryLevel(i);
		
		log("mastery=%d level=%d", i, level);
	}
}


void MasteryTest::testSaveLoadMastery()
{
	mManager->loadPlayerMastery();
	
	log("Just After load player mastery:\n%s\n", mManager->infoPlayerMasteryArray().c_str());
	
	int level;
	
	
	level = mManager->getPlayerMasteryLevel(1);
	mManager->setPlayerMasteryLevel(1, level+1);
	mManager->setPlayerMasteryLevel(2, 2);

	mManager->savePlayerMastery();
	
	log("Just After Save player mastery:\n%s\n", mManager->infoPlayerMasteryArray().c_str());
}

void MasteryTest::testListMastery()
{
	// Get the list of mastery data
	Vector<MasteryData *> masteryList = GameManager::instance()->getMasteryManager()->getMasteryArray();
	
	// Loop and use the list
	for(int i=0; i<masteryList.size(); i++) {
		MasteryData *masteryData = masteryList.at(i);
		
		int masteryID = masteryData->getMasteryID();
		
		int currentLevel = GameManager::instance()->getMasteryManager()->getPlayerMasteryLevel(masteryID);
		bool isMaxLevel = masteryData->getMaxLevel() == currentLevel;
		int nextLevel = isMaxLevel ? currentLevel : currentLevel + 1;
		
		int upgradeCost = masteryData->getUpgradePrice(nextLevel);
		
		int currentValue = masteryData->getLevelValue(currentLevel);
		int nextValue = masteryData->getLevelValue(nextLevel);
		
		log("name:%s", masteryData->getName().c_str());
		log("info:%s", masteryData->getInfo().c_str());
		log("change: %d -> %d %s", currentValue, nextValue,
							(masteryData->getPercentFlag() ? "%" : "") );
		log("upgrade: %d", upgradeCost);
	}
}

void MasteryTest::testSetPlayerAttribute()
{
	log("Mastery Data:\n%s\n", GameManager::instance()->getMasteryManager()->infoMasteryArray().c_str());
	log("Player Mastery:\n%s\n", GameManager::instance()->getMasteryManager()->infoPlayerMasteryArray().c_str());
	
	Player *player = new Player();
	
	// Setting the data
	
	GameManager::instance()->getMasteryManager()->setPlayerMasteryAttribute(player);
	
	log("Player Attribute:\n%s\n", player->infoAttribute().c_str());
	
	
	// clean up
	player->release();
}

void MasteryTest::testDescription()
{

	for(int mastery=1; mastery<=4; mastery++) {
		//MasteryData *getMasteryData(int masteryID);
		MasteryData *masteryData = GameManager::instance()->getMasteryManager()->getMasteryData(mastery);
		
		int max = masteryData->getMaxLevel();
		for(int level=1; level<=max; level++) {
			log("id=%d name=%s level=%d: %s", mastery,
				masteryData->getName().c_str(), level,
				masteryData->getDisplayInfo(level).c_str());
		}
	}
}


#endif
