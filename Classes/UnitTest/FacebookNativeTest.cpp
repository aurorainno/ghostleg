#ifdef ENABLE_TDD
//
//  FacebookNativeTest.m
//	TDD Framework
//
//
#include "FacebookNativeTest.h"
#include "TDDHelper.h"

#include "FacebookTestHelper.h"

void FacebookNativeTest::setUp()
{
	log("TDD Setup is called");
}


void FacebookNativeTest::tearDown()
{
	log("TDD tearDown is called");
}

void FacebookNativeTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void FacebookNativeTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void FacebookNativeTest::defineTests()
{
	ADD_TEST(testAccessToken);
	ADD_TEST(testInvite);
}

#pragma mark -
#pragma mark Sub Test Definition
void FacebookNativeTest::testInvite()
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	FacebookTestHelper::testFacebookInvite();
#endif
}

void FacebookNativeTest::testAccessToken()
{
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	FacebookTestHelper::testAccessToken();
#endif
}

#endif
