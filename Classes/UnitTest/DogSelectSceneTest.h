#ifdef ENABLE_TDD
//
//  DogSelectSceneTest.h
//
//
#ifndef __TDD_DogSelectSceneTest__
#define __TDD_DogSelectSceneTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class DogSelectSceneTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
};

#endif

#endif
