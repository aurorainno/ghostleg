#ifdef ENABLE_TDD
//
//  EmojiAnimeNodeTest.m
//	TDD Framework
//
//
#include "EmojiAnimeNodeTest.h"
#include "TDDHelper.h"
#include "EmojiAnimeNode.h"
#include "VisibleRect.h"

void EmojiAnimeNodeTest::setUp()
{
	log("TDD Setup is called");


	EmojiAnimeNode *node = EmojiAnimeNode::create();
	
	
	node->setPosition(VisibleRect::center());
	
	node->setEmojiEndCallback([&](){
		log("Emoji End Callback");
	});
	
	
	addChild(node);

	mEmojiNode = node;
	
	mEmojiValue = 1;
}


void EmojiAnimeNodeTest::tearDown()
{
	log("TDD tearDown is called");
}

void EmojiAnimeNodeTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void EmojiAnimeNodeTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void EmojiAnimeNodeTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testEmoji);
	ADD_TEST(testCloseEmoji);
	ADD_TEST(changeEmoji);
	
	
}

#pragma mark -
#pragma mark Sub Test Definition
void EmojiAnimeNodeTest::testSample()
{
	log("this is a sample subTest");
	mEmojiNode->showWaiting();
}

void EmojiAnimeNodeTest::testEmoji()
{
	log("Showing Emoji: %d", mEmojiValue);
	mEmojiNode->showEmoji(mEmojiValue);
}

void EmojiAnimeNodeTest::testCloseEmoji()
{
	mEmojiNode->closeEmoji();
}

void EmojiAnimeNodeTest::changeEmoji()
{
	mEmojiValue++;
	if(mEmojiValue > 5) {
		mEmojiValue = 1;
	}
	
	testEmoji();
}

#endif
