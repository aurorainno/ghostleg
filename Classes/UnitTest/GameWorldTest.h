#ifdef ENABLE_TDD
//
//  GameWorldTest.h
//
//
#ifndef __TDD_GameWorldTest__
#define __TDD_GameWorldTest__

// Include Header

#include "TDDBaseTest.h"
#include "CommonType.h"


class StageParallaxLayer;

// Class Declaration 
class GameWorldTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();

	void handleWorldEvent(Ref *sender, GameWorldEvent event, int arg);
	
	void update(float delta);
	
	float getTravelledDistance();
	
private:
	void testUpdate();
	void testStart();
	void testStop();
	void toggleUpdate();
	void movePlayer();
	void cameraUp();
	void cameraDown();
	void generateData();
	void resetGame();
	
private:
	bool mEnableUpdate;
	float mInitialPlayerPos;
	StageParallaxLayer *mParallaxLayer;
}; 

#endif

#endif
