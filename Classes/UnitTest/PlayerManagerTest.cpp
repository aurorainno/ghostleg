#ifdef ENABLE_TDD
//
//  PlayerManagerTest.m
//	TDD Framework
//
//
#include "PlayerManagerTest.h"
#include "TDDHelper.h"
#include "PlayerManager.h"
#include "PlayerCharData.h"
#include "PlayerCharProfile.h"
#include "PlayerCharDataSet.h"
#include "CharGameData.h"
#include "PlayerAbility.h"
#include "PlayerAbilityFactory.h"


void PlayerManagerTest::setUp()
{
	log("TDD Setup is called");
	
	//PlayerManager::instance()->setupMockData();
	PlayerManager::instance()->setup();
}


void PlayerManagerTest::tearDown()
{
	log("TDD tearDown is called");
}

void PlayerManagerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PlayerManagerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PlayerManagerTest::defineTests()
{
	ADD_TEST(testInfo);
	ADD_TEST(testAbilitySetting);
	ADD_TEST(testAbilityByLevel);
	ADD_TEST(testAbilityBuff);
	ADD_TEST(testCharacterList);
	ADD_TEST(testSelect);
	ADD_TEST(testPlayerCharSetSaveLoad);
	ADD_TEST(testPlayerCharSetJSON);
	ADD_TEST(testUpgrade);
	ADD_TEST(testLoadData);
	ADD_TEST(testPlayerCharProfile);
	ADD_TEST(testPlayerCharData);
    ADD_TEST(testAlterFragment);
}

#pragma mark -
#pragma mark Sub Test Definition
void PlayerManagerTest::testInfo()
{
	log("Character Info: %s", PlayerManager::instance()->infoAllCharData().c_str());
	
	log("---------------");
	log("Internal PlayerData\n%s\n", PlayerManager::instance()->infoPlayerCharData().c_str());
	
	log("---------------");
	log("PlayerProfiles\n%s\n", PlayerManager::instance()->infoPlayerCharProfile().c_str());
	
	log("---------------");
	log("PlaySetting\n%s\n", PlayerManager::instance()->infoGlobalPlaySetting().c_str());
}

void PlayerManagerTest::testAbilitySetting()
{
	int testChar = 19;
	std::vector<int> charList;
//	if(testChar > 0) {
//		charList.push_back(testChar);
//	}
//	charList.push_back(20);
//	charList.push_back(21);
	
	std::string info = PlayerManager::instance()->infoCharAbility(charList);
	
	log("Ability Setting\n%s\n", info.c_str());
	
}

void PlayerManagerTest::testLoadData()
{
	// PlayerManager::instance()->loadData();
	
	log("AllCharData\n%s\n", PlayerManager::instance()->infoAllCharData().c_str());
	
	log("AddPlayerData\n%s\n", PlayerManager::instance()->infoPlayerCharData().c_str());
}

void PlayerManagerTest::testPlayerCharData()
{
	for(int charID=1; charID <= 2; charID++ ){
		PlayerCharData *charData = PlayerManager::instance()->getPlayerCharData(charID);
		
		log("charID=%d data=%s\n", charID, charData->toString().c_str());
		
		
	}
	
}

void PlayerManagerTest::testPlayerCharProfile()
{
	for(int charID=1; charID <= 2; charID++ ){
		PlayerCharProfile *profile = PlayerCharProfile::create();
		profile->setup(charID);
		
		log("profile=%s", profile->toString().c_str());
		//profile-
		
//		PlayerCharData *charData = PlayerManager::instance()->getPlayerCharData(charID);
//		
//		log("charID=%d data=%s\n", charID, charData->toString().c_str());
//		
		
	}
	
}



void PlayerManagerTest::testUpgrade()
{
	int charID = 1;
	
	PlayerCharProfile *profile = PlayerManager::instance()->getPlayerCharProfile(charID);
	
	profile->alterFragmentCount(20);
	
	log("Before Upgrade: %s\n", profile->toString().c_str());
	
	PlayerCharProfile::UpgradeStatus status = PlayerManager::instance()->upgradeChar(charID);
	log("Upgrade: %d", status);
	
	log("After Upgrade: %s\n", profile->toString().c_str());
	
	
	log("Whole Profile: %s\n", PlayerManager::instance()->infoPlayerCharData().c_str());
}


void PlayerManagerTest::testPlayerCharSetJSON()
{
	PlayerCharDataSet *dataSet = PlayerCharDataSet::create();

	dataSet->setupMockData();
	
	std::string json = dataSet->toJSONContent();
	
	
	log("JSON:\n%s\n", json.c_str());
	
	PlayerCharDataSet *newSet = PlayerCharDataSet::create();
	newSet->parseJSONContent(json);
	
	log("Object Parsed:\n%s\n", newSet->toString().c_str());
}


void PlayerManagerTest::testPlayerCharSetSaveLoad()
{
	PlayerCharDataSet *dataSet = PlayerCharDataSet::create();
	
	dataSet->load();
	
	log("After Load:\n%s\n", dataSet->toString().c_str());

	dataSet->alterFragmentCount(1, 5);
//	PlayerCharData *charData = dataSet->getPlayerCharData(1);
//	charData->alterFragmentCount(5);
	
	log("After add Fragment:\n%s\n", dataSet->toString().c_str());
	
	dataSet->save();
}


void PlayerManagerTest::testSelect()
{
	static int selectChar = 1;
	
	// Setting
	PlayerManager::instance()->selectChar(selectChar);
	
	
	log("Internal PlayerData\n%s\n", PlayerManager::instance()->infoPlayerCharData().c_str());
	
	log("Selected: %d", PlayerManager::instance()->getSelectedChar());
	
	// Setting next;
	selectChar++;
	if(selectChar >= 3) {
		selectChar = 1;
	}
}

void PlayerManagerTest::testAlterFragment()
{
    Vector<PlayerCharProfile *> list = PlayerManager::instance()->getOwnedCharacters();
    for(PlayerCharProfile* profile : list){
        profile->alterFragmentCount(100);
    }
    Vector<PlayerCharProfile *> listTwo = PlayerManager::instance()->getLockedCharacters();
    for(PlayerCharProfile* profile : listTwo){
        profile->alterFragmentCount(20);
    }

}

void PlayerManagerTest::testCharacterList()
{
	// Setting up
	PlayerManager::instance()->resetPlayerCharData();
//	PlayerManager::instance()->forceUnlock(4);
//   PlayerManager::instance()->forceUnlock(5);
//    PlayerManager::instance()->forceUnlock(6);
	PlayerManager::instance()->selectChar(1);
	
	//
	Vector<PlayerCharProfile *> ownedList = PlayerManager::instance()->getOwnedCharacters();
	
	log("Owned List");
	for(PlayerCharProfile *profile : ownedList) {
		log("%s", profile->toString().c_str());
	}
	
	
	Vector<PlayerCharProfile *> lockedList = PlayerManager::instance()->getLockedCharacters();
	log("Locked List");
	for(PlayerCharProfile *profile : lockedList) {
		log("%s", profile->toString().c_str());
	}
}

void PlayerManagerTest::testAbilityBuff()
{
	CharGameData *gameData = PlayerManager::instance()->getCharGameData(1);
	
	
	// Setup Ability
	gameData->clearAbilityList();
	PlayerAbility *ability;
	
	// maxSpeed percentage
	for(int i=0; i<3; i++){
		ability = PlayerAbilityFactory::instance()->create("maxSpeed");
		ability->setParam1(0.1 * (i+1));
		gameData->addAbility(ability);
	}
	for(int i=0; i<2; i++){
		ability = PlayerAbilityFactory::instance()->create("stunReduction");
		ability->setParam1(0.1 * (i+1));
		gameData->addAbility(ability);
	}
	
	PlayerCharData *playerData = PlayerCharData::create();
	playerData->setCharID(1);
	
	playerData->setCurrentLevel(0);
	
	PlayerCharProfile *profile = PlayerCharProfile::create();
	profile->setCharGameData(gameData);
	profile->setPlayerCharData(playerData);
	profile->update();
	
	log("original profile: %s", profile->toString().c_str());
	
	for(int level=1; level<=5; level++) {
		playerData->setCurrentLevel(level);
		profile->update();
		
		log("profile-lv%d: %s", level, profile->toString().c_str());
	}

	
//	gameData->addAbility(<#PlayerAbility *ability#>);
}

void PlayerManagerTest::testAbilityByLevel()
{
	PlayerCharProfile *profile = PlayerManager::instance()->getPlayerCharProfile(3);
	
	for(int level=1; level<=5; level++ ){
		PlayerAbility *ability = profile->getAbilityByLevel(level);
		
		std::string desc = ability->getDesc();
		
		log("LV %d:\n %s\n %s", level, ability->toString().c_str(), desc.c_str());
	}
	
}

#endif
