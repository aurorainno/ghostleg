#ifdef ENABLE_TDD
//
//  ShareHelperTest.m
//	TDD Framework 
//
//
#include "ShareHelperTest.h"
#include "TDDHelper.h"
#include "ShareHelper.h"
#include "GameManager.h"

void ShareHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void ShareHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ShareHelperTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(ShareHelperTest::testSimpleShare);
	SUBTEST(ShareHelperTest::testShareScore);
}

#pragma mark -
#pragma mark Sub Test Definition
void ShareHelperTest::testSimpleShare(Ref *sender)
{
	//log("this is a sample subTest");
	ShareHelper::share("Testing Sharing!!!");
}

void ShareHelperTest::testShareScore(Ref *sender)
{
	//log("this is a sample subTest");
	GameManager::instance()->share(1000,2);
}


#endif
