#ifdef ENABLE_TDD
//
//  StageManagerTest.h
//
//
#ifndef __TDD_StageManagerTest__
#define __TDD_StageManagerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class StageManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void loadStage0();
	void testSample();
	void testRollPuzzle();
	void testPuzzleChanceTable();
	void testNextNPCOrder();
	void testNpcOrderGetRandomMaps();
	void testParsingData();
	void testStageData();
	void testGetAllGameMap();
	void testGetNextNPCOrderList();
	void testPlayerNpcOrder();
	void testRollNpc();
	void testRollOneNpc();
	void testGetRequireMaps();
	void testGetRandomMaps();
};

#endif

#endif
