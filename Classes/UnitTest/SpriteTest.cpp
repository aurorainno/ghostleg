#ifdef ENABLE_TDD
//
//  SpriteTest.m
//	TDD Framework 
//
//
#include <string>

#include "SpriteTest.h"
#include "TDDHelper.h"
#include "VisibleRect.h"

void SpriteTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void SpriteTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void SpriteTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(SpriteTest::testSample);
	SUBTEST(SpriteTest::testSetTexture);
	SUBTEST(SpriteTest::testSpriteWithSize);
	SUBTEST(SpriteTest::testUsingSpriteSheet);
}

#pragma mark -
#pragma mark Sub Test Definition
void SpriteTest::testSample(Ref *sender)
{
	log("this is a sample subTest");
	
	const int numTest = 3;
	std::string testName[numTest] = {
			"smile.png",
			"tiger.png",
			"ghost.png",
	};
	
	Vec2 pos = Vec2(50, 30);		// bottom to top
	
	for(int i=0; i<numTest; i++){
		std::string name = testName[i];
		
		log("spriteName=%s", name.c_str());
		
		Sprite *sprite = Sprite::create(name);
		sprite->setPosition(pos);
		
		addChild(sprite);
		
		pos.y += sprite->getContentSize().height + 20;
	}
}


void SpriteTest::testSetTexture(Ref *sender)
{
	log("this is a sample subTest");
	
	const int numTest = 3;
	std::string testName[numTest] = {
		"smile.png",
		"tiger.png",
		"ghost.png",
	};
	
	Vec2 pos = Vec2(50, 30);		// bottom to top
	
	for(int i=0; i<numTest; i++){
		std::string name = testName[i];
		
		log("spriteName=%s", name.c_str());
		
		Sprite *sprite = Sprite::create(name);
		sprite->setTexture(name);
		sprite->setPosition(pos);
		
		addChild(sprite);
		
		pos.y += sprite->getContentSize().height + 20;
	}
}

void SpriteTest::testUsingSpriteSheet(Ref *sender)
{
	Sprite *sprite = Sprite::createWithSpriteFrameName("star.png");
	sprite->setPosition(VisibleRect::center());
	
	addChild(sprite);

	
	// Change the sprite
	sprite->setSpriteFrame("icon_timestop.png");
}

void SpriteTest::testSpriteWithSize(Ref *sender)
{
	
	float x = RandomHelper::random_int(10, (int)(VisibleRect::right().x -10));
	float y = RandomHelper::random_int(10, (int)(VisibleRect::top().y -10));

	Sprite *sprite = Sprite::create("general/route_line.png");
	sprite->setPosition(Vec2(x, y));
	sprite->setTextureRect(Rect(0, 0, 30, 30));
	addChild(sprite);
	//sprite->set

}

#endif
