#ifdef ENABLE_TDD
//
//  NpcDistanceTest.h
//
//
#ifndef __TDD_NpcDistanceTest__
#define __TDD_NpcDistanceTest__

// Include Header

#include "TDDBaseTest.h"

class NpcDistanceLayer;

// Class Declaration
class NpcDistanceTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	void setupDistanceLayer();
	
private:
	void testSample();
	void testShowUp();
	void testNextNpc();
	
private:
	NpcDistanceLayer *mDistanceLayer;
	int mOrderIndex;
};

#endif

#endif
