#ifdef ENABLE_TDD
//
//  GmSceneTest.m
//	TDD Framework 
//
//
#include "GmSceneTest.h"
#include "TDDHelper.h"
#include "GmScene.h"
#include "DebugMapView.h"

void GmSceneTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void GmSceneTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void GmSceneTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(GmSceneTest::testScene);
	SUBTEST(GmSceneTest::testDebugMap);
}

#pragma mark -
#pragma mark Sub Test Definition
void GmSceneTest::testScene(Ref *sender)
{
	log("this is a sample subTest");
	
	auto scene = GmSceneLayer::createScene();
	
	Director::getInstance()->pushScene(scene);
}

void GmSceneTest::testDebugMap(Ref *sender)
{
	DebugMapView *debugMapView = DebugMapView::create();
	
	addChild(debugMapView);
}

#endif
