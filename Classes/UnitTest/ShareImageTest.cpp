 #ifdef ENABLE_TDD
//
//  ShareImageTest.m
//	TDD Framework 
//
//
#include "ShareImageTest.h"
#include "TDDHelper.h"
#include "ShareImage.h"
#include "DeviceHelper.h"
#include "VisibleRect.h"

void ShareImageTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void ShareImageTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ShareImageTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(ShareImageTest::testImage);
	SUBTEST(ShareImageTest::testSaveImage);
	SUBTEST(ShareImageTest::testWriteToFile);
}

#pragma mark -
#pragma mark Sub Test Definition
void ShareImageTest::testImage(Ref *sender)
{
	log("this is a sample subTest");
	ShareImage *shareImage = ShareImage::create();
	
	shareImage->setScore(10000);
    shareImage->setStage(3);
	
	Sprite *sprite = shareImage->getSprite();
	sprite->setPosition(VisibleRect::center());
	//sprite->setScale(0.4f, -0.4f);
	
	addChild(sprite);
}

void ShareImageTest::testSaveImage(Ref *sender)
{
	log("this is a sample subTest");
	std::string fullpath = FileUtils::getInstance()->getWritablePath();
	log("debug: fileUtil.writablePath=%s", fullpath.c_str());
	
	
	ShareImage *shareImage = ShareImage::create();
	
	shareImage->setCallback([](RenderTexture*, const std::string &saveFile){
		log("file saved [%s]", saveFile.c_str());
	});
	
	shareImage->saveToLocal();
	
	
}


void ShareImageTest::testWriteToFile(Ref *sender)
{
	// using writable path
	std::string fullpath = FileUtils::getInstance()->getWritablePath();
	std::string file = fullpath + "test.txt";
	
	log("debug: file=%s", file.c_str());
	
	
	FileUtils::getInstance()->writeStringToFile("testing content", file);

	// using external
	fullpath = DeviceHelper::getStoragePath();
	file = fullpath + "test_external.txt";
	
	log("debug: external. file=%s", file.c_str());
	
	FileUtils::getInstance()->writeStringToFile("testing content", file);
	// std::string fullFileUtils::getWritablePath();
}



#endif
