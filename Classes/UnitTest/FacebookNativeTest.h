#ifdef ENABLE_TDD
//
//  FacebookNativeTest.h
//
//
#ifndef __TDD_FacebookNativeTest__
#define __TDD_FacebookNativeTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class FacebookNativeTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testInvite();
	void testAccessToken();
};

#endif

#endif
