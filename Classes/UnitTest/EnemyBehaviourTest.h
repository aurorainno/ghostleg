#ifdef ENABLE_TDD
//
//  EnemyBehaviourTest.h
//
//
#ifndef __TDD_EnemyBehaviourTest__
#define __TDD_EnemyBehaviourTest__

// Include Header

#include "TDDTest.h"
#include "Enemy.h"

// Class Declaration 
class EnemyBehaviourTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testLoadJSON(Ref *sender);
	void testEnemyData(Ref *sender);
	void testLoadData(Ref *sender);
	void testSetEnemy(Ref *sender);
	void testUpdate(Ref *sender);
	void testGetSpeed(Ref *sender);
	void debugAutoBehaviour(Ref *sender);
	void testDropBehaviour(Ref *sender);
	
private:
	CC_SYNTHESIZE_RETAIN(Enemy *, mEnemy, Enemy);
}; 

#endif

#endif
