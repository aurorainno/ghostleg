#ifdef ENABLE_TDD
//
//  LuckyDrawManagerTest.m
//	TDD Framework
//
//
#include "LuckyDrawManagerTest.h"
#include "LuckyDrawReward.h"
#include "TDDHelper.h"
#include "LuckyDrawManager.h"

void LuckyDrawManagerTest::setUp()
{
	log("TDD Setup is called");
}


void LuckyDrawManagerTest::tearDown()
{
	log("TDD tearDown is called");
}

void LuckyDrawManagerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void LuckyDrawManagerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void LuckyDrawManagerTest::defineTests()
{
	ADD_TEST(testRoll);
	ADD_TEST(testParseJSON);
	ADD_TEST(testInfoRewardList);
}

#pragma mark -
#pragma mark Sub Test Definition
void LuckyDrawManagerTest::testParseJSON()
{
	LuckyDrawReward *reward = LuckyDrawReward::create();
	
	reward->setItemName("Testing");
	reward->setItemID(1001);
	reward->setProbability(1000);
	reward->setQuantity(5);
	reward->setRarityType(LuckyDrawReward::RarityRare);
	reward->setRewardType(LuckyDrawReward::RewardBooster);
	
	std::string json = reward->toJSONContent();
	
	LuckyDrawReward *parsed = LuckyDrawReward::create();
	parsed->parseJSONContent(json);
	
	log("Origin: %s", reward->toString().c_str());
	log("Parsed: %s", parsed->toString().c_str());
}

void LuckyDrawManagerTest::testInfoRewardList()
{
	LuckyDrawManager::instance()->setup();
	std::string info = LuckyDrawManager::instance()->infoRewardList();
	log("INFO:\n%s\n", info.c_str());
}

void LuckyDrawManagerTest::testRoll()
{
	LuckyDrawManager::instance()->setup();
	
	int numRoll = 20;
	
	for(int i=0; i<numRoll; i++) {
		LuckyDrawReward *reward = LuckyDrawManager::instance()->roll();
		
		log("%d: %s", i, reward->toString().c_str());
	}
}
#endif
