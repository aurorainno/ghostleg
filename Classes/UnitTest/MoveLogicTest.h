#ifdef ENABLE_TDD
//
//  MoveLogicTest.h
//
//
#ifndef __TDD_MoveLogicTest__
#define __TDD_MoveLogicTest__

// Include Header

#include "TDDBaseTest.h"

class GameMap;
class RouteMoveLogic;

// Class Declaration
class MoveLogicTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	void setupGameMap();
	void setMoveLogic(RouteMoveLogic *logic);
	
private:
	void testMoveVertical();
	void testMoveOnSlope();
	void testMoveHoriLine();
	void testMoveSlopeLine();
	void testCalculateVertical();
	void testCalculateHorizontal();
	void testUpdateStateData();
	void testWithVisual();
	void step();
	
private:
	GameMap *mGameMap;
	float mLineX[4];
	float mTimeElapse;
	
	RouteMoveLogic *mLogic;
	Sprite *mTestSprite;
};

#endif

#endif
