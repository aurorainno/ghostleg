#ifdef ENABLE_TDD
//
//  SpeedEffectTest.h
//
//
#ifndef __TDD_SpeedEffectTest__
#define __TDD_SpeedEffectTest__

// Include Header

#include "TDDBaseTest.h"

class StageParallaxLayer;
class Player;

// Class Declaration
class SpeedEffectTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testChangeStyle();
	void testIncreaseSpeed();
	
	void update(float delta);

#pragma mark - Touch Handling
	void setupTouchListener();
	bool onTouchBegan(Touch *touch, Event *event);
	void onTouchEnded(Touch *touch, Event *event);
	void onTouchMoved(Touch *touch, Event *event);
	void onTouchCancelled(Touch *touch, Event *event);

	
#pragma mark - Internal Data
private:
	StageParallaxLayer *mParallaxLayer;
	Player *mPlayer;
	Vec2 mLastPos;
	int mMotionStyle;
	float mSpeed;
};

#endif

#endif
