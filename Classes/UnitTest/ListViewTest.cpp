#ifdef ENABLE_TDD
//
//  ListViewTest.m
//	TDD Framework
//
//
#include "ListViewTest.h"
#include "TDDHelper.h"
#include "cocostudio/CocoStudio.h"

#include "DogSelectItemView.h"


void ListViewTest::setUp()
{
	log("TDD Setup is called");
	Node *node = CSLoader::createNode("UiTestScene.csb");
	addChild(node);

	mListView = (ListView *) node->getChildByName("testListView");
}


void ListViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void ListViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void ListViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void ListViewTest::defineTests()
{
	ADD_TEST(testTestList);
	ADD_TEST(testDogList);
	ADD_TEST(testDogListBug);
}

#pragma mark -
#pragma mark Sub Test Definition
void ListViewTest::testTestList()
{
	log("this is a sample subTest");

	Text *text = Text::create();
	
	// Node *listView =
	mListView->setItemModel(text);
	mListView->removeAllItems();
	for(int i=0; i<5; i++) {
		mListView->pushBackDefaultItem();
	}
	
	Vector<Widget*>& widgetList = mListView->getItems();
	
	int idx = 0;
	for(Widget *widget : widgetList) {
		Text *text = (Text *) widget;
		
		text->setString(StringUtils::format("Testing %d", idx));
		
		idx++;
	}
}

void ListViewTest::testDogListBug()
{
	// Node *listView =
	DogSelectItemView *itemView = DogSelectItemView::create();
	mListView->setItemModel(itemView);
	mListView->removeAllItems();
	for(int i=0; i<5; i++) {
		mListView->pushBackDefaultItem();
	}
	
	Vector<Widget*>& widgetList = mListView->getItems();
	
	int dogID = 1;
	for(Widget *widget : widgetList) {
		
		DogSelectItemView *dogView = (DogSelectItemView *) widget;
		
		dogView->setDog(dogID);
		dogView->testUIState(false, false);
		
		dogID++;
	}
}

void ListViewTest::testDogList()
{
	// Node *listView =
	mListView->removeAllItems();
	mListView->setItemsMargin(5);//spacing between item
	
	int dogID = 1;
    
	for(int i=0; i<5; i++) {
		// mListView->pushBackDefaultItem();

		DogSelectItemView *dogView = DogSelectItemView::create();
		mListView->pushBackCustomItem(dogView);
		dogView->setDog(dogID);
		dogView->testUIState(false, false);
		
		dogID++;
	}
    

    mListView->jumpToItem(4, Vec2(1,1), Vec2::ZERO);
    
    
	
}

#endif
