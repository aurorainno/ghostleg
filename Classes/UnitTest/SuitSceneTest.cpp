#ifdef ENABLE_TDD
//
//  SuitSceneTest.m
//	TDD Framework 
//
//
#include "SuitSceneTest.h"
#include "TDDHelper.h"
#include "SuitScene.h"
#include "SuitManager.h"
#include "GameManager.h"

void SuitSceneTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void SuitSceneTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void SuitSceneTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(SuitSceneTest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void SuitSceneTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
    SuitSceneLayer *layer = SuitSceneLayer::create();
    
    auto button = Button::create();
    button->setColor(Color3B::GREEN);
    button->setTitleText("Reset");
    
    button->addTouchEventListener([&,layer](Ref *sender, Widget::TouchEventType type) {
        switch(type) {
            case ui::Widget::TouchEventType::ENDED: {
                GameManager::instance()->getSuitManager()->resetAllSuit();
                layer->refreshPage();
                break;
            }
        }
    });
    
    button->setPosition(Vec2(30,80));
    
    addChild(layer);
    addChild(button);
}


#endif
