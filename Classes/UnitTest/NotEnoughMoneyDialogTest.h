#ifdef ENABLE_TDD
//
//  NotEnoughMoneyDialogTest.h
//
//
#ifndef __TDD_NotEnoughMoneyDialogTest__
#define __TDD_NotEnoughMoneyDialogTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class NotEnoughMoneyDialogTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testDialogMode();
};

#endif

#endif
