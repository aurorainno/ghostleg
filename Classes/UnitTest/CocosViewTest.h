#ifdef ENABLE_TDD
//
//  CocosViewTest.h
//
//
#ifndef __TDD_CocosViewTest__
#define __TDD_CocosViewTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class CocosViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void subTest();
	void testLayer();
	void testCsbAnime();
}; 

#endif

#endif
