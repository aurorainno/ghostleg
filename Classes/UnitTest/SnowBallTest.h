#ifdef ENABLE_TDD
//
//  SnowBallTest.h
//
//
#ifndef __TDD_SnowBallTest__
#define __TDD_SnowBallTest__

// Include Header

#include "TDDBaseTest.h"

class GameMap;
#include "SnowBall.h"

// Class Declaration
class SnowBallTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
	void setupGameMap();
	void setupSnowBall();
	void addSnowBall(SnowBall *ball);
	
private:
	void testSample();
	void step();
	void fire();
	void testTwoScale();
	
private:
	GameMap *mGameMap;
	float mLineX[4];
	Vector<SnowBall *> mBallList;
};

#endif

#endif
