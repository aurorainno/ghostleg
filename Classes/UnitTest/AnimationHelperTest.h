#ifdef ENABLE_TDD
//
//  AnimationHelperTest.h
//
//
#ifndef __TDD_AnimationHelperTest__
#define __TDD_AnimationHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class AnimationHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
	Node *createBox(const Color4B &color);
private:
	void subTest();
	void testBezier2();
	void testShowAndHide();
	void testMoveToAction();
	void testAddRotatingAura();
	void testAnimeCsb();
	void testClosing();
	void testRotateForver();
	void testPopup();
	void testBlink();
	void testAlphaBlink();
	void testBezier();
	void testBlinkForever();
	void testScaleUpAndFade();
	void testJellyEffect();
    void testMoveByEffect();
	void testSetToastEffect();
	void testShake();
}; 

#endif

#endif
