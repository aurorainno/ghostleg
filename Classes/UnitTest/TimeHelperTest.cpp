#ifdef ENABLE_TDD
//
//  TimeHelperTest.m
//	TDD Framework
//
//
#include "TimeHelperTest.h"
#include "TDDHelper.h"
#include "TimeHelper.h"

void TimeHelperTest::setUp()
{
	log("TDD Setup is called");
}


void TimeHelperTest::tearDown()
{
	log("TDD tearDown is called");
}

void TimeHelperTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void TimeHelperTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void TimeHelperTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testTimeDiff);
	ADD_TEST(testRemainTimeString);
}

#pragma mark -
#pragma mark Sub Test Definition
void TimeHelperTest::testRemainTimeString()
{
	int secPerHour = 3600;
	int secPerMinute = 60;
	int secPerDay = 24 * secPerHour;

	std::vector<long long> testList;
	testList.push_back(1000 * (1 * secPerDay + 11 * secPerHour + 30 * secPerMinute));
	testList.push_back(1000 * (3 * secPerDay + 11 * secPerHour + 30 * secPerMinute));
	testList.push_back(1000 * (0 * secPerDay + 11 * secPerHour + 30 * secPerMinute));
	testList.push_back(1000 * (0 * secPerDay + 1 * secPerHour + 30 * secPerMinute));
	testList.push_back(1000 * (0 * secPerDay + 0 * secPerHour + 22 * secPerMinute));
	testList.push_back(1000 * (0 * secPerDay + 0 * secPerHour + 1 * secPerMinute));
	
	for(long long remain : testList) {
		std::string str = TimeHelper::getRemainTimeString(remain);
		
		log("[%s] remain=[%lld]", str.c_str(), remain);
	}
}


void TimeHelperTest::testSample()
{
	log("this is a sample subTest");
    long long ms = TimeHelper::getCurrentTimeStampInMillis();
    log("millis:%lld",ms);
    //3d 17h 48m
    std::map<std::string,int> result = TimeHelper::parseMillis(323287326);
    log("result: %dday %dhours %dminutes",result["days"],result["hours"],result["minutes"]);
    
}

void TimeHelperTest::testTimeDiff()
{
	long long endTime = 1497110400429;
	long long now = TimeHelper::getCurrentTimeStampInMillis();
	
	long long diff = endTime - now;
	
	std::map<std::string,int> result = TimeHelper::parseMillis(diff);
	log("result: %dday %dhours %dminutes", result["days"], result["hours"], result["minutes"]);
	log("now=%lld", now);
	
	
}


#endif
