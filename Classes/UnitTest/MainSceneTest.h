#ifdef ENABLE_TDD
//
//  MainSceneTest.h
//
//
#ifndef __TDD_MainSceneTest__
#define __TDD_MainSceneTest__

// Include Header

#include "TDDBaseTest.h"

class GameSceneLayer;
class MainSceneLayer;

// Class Declaration 
class MainSceneTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testSetLeaderboardTime();
	void testMainScene();
	void testGameScene();
	void testShowUnlockPlanet();
	void testShowUnlockDog();
	void testComingSoon();
	void showMainUI();
	void hideMainUI();
	
private:
	GameSceneLayer *mGameLayer;
	MainSceneLayer *mMainLayer;
}; 

#endif

#endif
