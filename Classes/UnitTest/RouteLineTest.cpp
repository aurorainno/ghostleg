#ifdef ENABLE_TDD
//
//  RouteLineTest.m
//	TDD Framework
//
//
#include "RouteLineTest.h"
#include "TDDHelper.h"
#include "RouteLineNode.h"
#include "VisibleRect.h"
#include "AnimationHelper.h"

void RouteLineTest::setUp()
{
	log("TDD Setup is called");
	//setBackgroundColor(Color3B(0, 0, 50));
	Sprite *bg = Sprite::create("hires/BGMain.png");
	bg->setAnchorPoint(Vec2::ZERO);
	addChild(bg);
}


void RouteLineTest::tearDown()
{
	log("TDD tearDown is called");
}

void RouteLineTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void RouteLineTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void RouteLineTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testAnimation);
	ADD_TEST(testRotation);
	ADD_TEST(testRotationOnLayout);
	ADD_TEST(testDifferentAngle);
	ADD_TEST(testUpdateLineBySprite);
	ADD_TEST(testGraphicLine);
	ADD_TEST(testDifferentLength);
}

#pragma mark -
#pragma mark Sub Test Definition
void RouteLineTest::testSample()
{
	log("this is a sample subTest");
	
	RouteLineNode *node = RouteLineNode::create();
	node->setPosition(Vec2::ZERO);
	addChild(node);
	
	node->setShader("dash");
	node->setStart(VisibleRect::rightTop());
	node->setEnd(VisibleRect::leftBottom());
	
	node->updateLine();
}


void RouteLineTest::testGraphicLine()
{
	RouteLineNode *node = RouteLineNode::create();
	node->setPosition(Vec2::ZERO);
	addChild(node);
	
	node->testLineSegment();
	log("Line info: %s", node->info().c_str());
}

//updateLineBySprite
void RouteLineTest::testUpdateLineBySprite()
{
	RouteLineNode *node = RouteLineNode::create();
	node->setPosition(Vec2::ZERO);
	addChild(node);

	node->setStart(VisibleRect::center());
	//node->setEnd(VisibleRect::rightTop());
	node->setEnd(VisibleRect::rightBottom());
	node->setUseDrawNode(false);
	node->updateLine();
}


void RouteLineTest::testDifferentAngle()
{
	std::vector<Vec2> endPoints;
	
	endPoints.push_back(VisibleRect::leftBottom());
	endPoints.push_back(VisibleRect::left());
	endPoints.push_back(VisibleRect::leftTop());
	endPoints.push_back(VisibleRect::top());
	endPoints.push_back(VisibleRect::bottom());
	endPoints.push_back(VisibleRect::rightTop());
	endPoints.push_back(VisibleRect::right());
	endPoints.push_back(VisibleRect::rightBottom());
	
	for(Vec2 end : endPoints) {
		RouteLineNode *node = RouteLineNode::create();
		node->setPosition(Vec2::ZERO);
		addChild(node);
		
		node->setStart(VisibleRect::center());
		node->setEnd(end);
		node->setUseDrawNode(false);
		node->updateLine();
	}
}


void RouteLineTest::testDifferentLength()
{
	std::vector<Vec2> startPoints;
	std::vector<Vec2> endPoints;
	
	Vec2 start = Vec2(100, 100);
	Vec2 end = Vec2(100, 300);
	
	int num = 10;
	
	for(int i=0; i<num; i++) {
		startPoints.push_back(start);
		endPoints.push_back(end);
		
		start.x += 10;
		end.y -= 10;
		end.x = start.x;
	}
	
	
	
	
	for(int i=0; i<num; i++) {
		RouteLineNode *node = RouteLineNode::create();
		node->setPosition(Vec2::ZERO);
		addChild(node);
		
		start = startPoints[i];
		end = endPoints[i];
		
		node->setStart(start);
		node->setEnd(end);
		node->setUseDrawNode(false);
		node->updateLine();
	}
}

void RouteLineTest::testRotation()
{
	int num = 3;
	
	std::vector<Layer *> layerList;
	
	for(int i=0; i<num; i++) {
		Layer *layer = Layer::create();
		layer->setPosition(Vec2(100, 100));
		//layer->setPosition(Vec2(0, 0));
		
		Sprite *sprite = Sprite::create("general/route_line.png");		// origin size: 26 x 188
		sprite->setScale(0.4f);
		sprite->setAnchorPoint(Vec2(0.5f, 0));
		sprite->setPosition(Vec2::ZERO);

		layer->addChild(sprite);
		
		addChild(layer);
		
		layerList.push_back(layer);
	}
	
	float angle = 0;
	for(Layer *layer : layerList) {
		layer->setRotation(-angle);
		
		
		angle += 5;
	}

}

void RouteLineTest::testRotationOnLayout()
{
	int num = 20;
	
	std::vector<ui::Layout *> layerList;
	
	for(int i=0; i<num; i++) {
		ui::Layout *layer = ui::Layout::create();
		layer->setPosition(Vec2(100, 100));
		//layer->setPosition(Vec2(0, 0));
		
		Sprite *sprite = Sprite::create("general/route_line.png");		// origin size: 26 x 188
		sprite->setScale(0.4f);
		sprite->setAnchorPoint(Vec2(0.5f, 0));
		sprite->setPosition(Vec2::ZERO);
		
		layer->addChild(sprite);
		
		addChild(layer);
		
		layerList.push_back(layer);
	}
	
	float angle = 0;
	for(ui::Layout *layer : layerList) {
		layer->setRotation(angle);
		
		
		angle += 10;
	}
	
}

void RouteLineTest::testAnimation()
{
	RouteLineNode *node = RouteLineNode::create();
	node->setPosition(Vec2::ZERO);
	addChild(node);
	
	node->setStart(VisibleRect::center());
	node->setEnd(VisibleRect::rightBottom());
	node->setUseDrawNode(false);
	node->updateLine();

	//AnimationHelper::setAlphaBlink(node, 3, 0, 125, 255);
	AnimationHelper::setForeverBlink(node, 3, 125);
}

#endif
