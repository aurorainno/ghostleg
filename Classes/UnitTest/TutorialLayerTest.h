#ifdef ENABLE_TDD
//
//  TutorialLayerTest.h
//
//
#ifndef __TDD_TutorialLayerTest__
#define __TDD_TutorialLayerTest__

// Include Header

#include "TDDBaseTest.h"

class TutorialLayer;

// Class Declaration
class TutorialLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

	
private:
	void update(float delta);
	
private:
	void testSimpleCase();
	void testUnlockPlanet();
	void testUnlockPlanet2();

private:
	TutorialLayer *mTutorialLayer;
};

#endif

#endif
