#ifdef ENABLE_TDD
//
//  LeaderboardLayerTest.m
//	TDD Framework
//
//
#include "LeaderboardLayerTest.h"
#include "TDDHelper.h"
#include "LeaderboardScene.h"

void LeaderboardLayerTest::setUp()
{
	log("TDD Setup is called");
}


void LeaderboardLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void LeaderboardLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void LeaderboardLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void LeaderboardLayerTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void LeaderboardLayerTest::testSample()
{
	log("this is a sample subTest");
	
	LeaderboardLayer *layer = LeaderboardLayer::create();
	layer->setDialogMode(LeaderboardLayer::SceneMode);
	
	// add layer as a child to scene
	addChild(layer);
}


#endif
