#ifdef ENABLE_TDD
//
//  EnemyBehaviourTest.m
//	TDD Framework 
//
//
#include "EnemyBehaviourTest.h"
#include "TDDHelper.h"
#include "EnemyData.h"
#include "EnemyBehaviour.h"
#include "EnemyFactory.h"
#include "Enemy.h"
#include "StringHelper.h"
#include "DropStuffBehaviour.h"

void EnemyBehaviourTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void EnemyBehaviourTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void EnemyBehaviourTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(EnemyBehaviourTest::testLoadJSON);

	SUBTEST(EnemyBehaviourTest::testGetSpeed);
	SUBTEST(EnemyBehaviourTest::testDropBehaviour);
	SUBTEST(EnemyBehaviourTest::debugAutoBehaviour);
	SUBTEST(EnemyBehaviourTest::testEnemyData);
	SUBTEST(EnemyBehaviourTest::testUpdate);
	SUBTEST(EnemyBehaviourTest::testLoadData);
	SUBTEST(EnemyBehaviourTest::testSetEnemy);
}

#pragma mark -
#pragma mark Sub Test Definition
void EnemyBehaviourTest::testGetSpeed(Ref *sender)
{
	Vector<EnemyData *> dataList = EnemyFactory::instance()->loadEnemyDataListFromJson("json/enemy.json");
	
	for(int i=0; i<dataList.size(); i++) {
		EnemyData *data = dataList.at(i);
		
		log("%d: %s", data->getType(), POINT_TO_STR(data->getSpeedVec()).c_str());
	}
}

void EnemyBehaviourTest::testLoadJSON(Ref *sender)
{
	Vector<EnemyData *> dataList = EnemyFactory::instance()->loadEnemyDataListFromJson("json/enemy.json");
	
	for(int i=0; i<dataList.size(); i++) {
		EnemyData *data = dataList.at(i);
		
		log("%s", data->toString().c_str());
	}
}


void EnemyBehaviourTest::testSetEnemy(Ref *sender)
{
	int monsterID = 3;	// freemove
	//int monsterID = 2;	// normal
	Enemy *enemy = EnemyFactory::instance()->createEnemy(monsterID);
	
	setEnemy(enemy);
	
	log("Setup: enemy=%s\n", enemy->toString().c_str());
}

void EnemyBehaviourTest::testUpdate(Ref *sender)
{
	if(mEnemy == nullptr) {
		return;
	}
	
	mEnemy->update(0.3f);
	
	log("Update: enemy=%s", mEnemy->toString().c_str());
}

void EnemyBehaviourTest::testEnemyData(Ref *sender)
{
	log("this is a sample subTest");
	
	EnemyData *enemyData = EnemyData::create();
	
	//enemyData->parse("type=4 behaviour=normal speed=0 active=onStart");
	enemyData->parse("type=1 behaviour=fallDown speedX=100 speedY=50 active=topY:100");
	
	log("data=%s", enemyData->toString().c_str());
}

void EnemyBehaviourTest::testLoadData(Ref *sender)
{
	EnemyFactory::instance()->loadData();
	
	log("Result:\n%s\n", EnemyFactory::instance()->infoEnemyData().c_str());
}



void EnemyBehaviourTest::testDropBehaviour(Ref *sender)
{
	EnemyFactory::instance()->loadData();
	
	
	EnemyData *data = EnemyFactory::instance()->getEnemyDataByType(11);
	DropStuffBehaviour *behaviour = DropStuffBehaviour::create();
	behaviour->setData(data);
	behaviour->onCreate();	// will call updateProperty
	
	log("infoAttribute=%s", behaviour->infoAttribute().c_str());
	log("info=%s", behaviour->toString().c_str());
	
}


void EnemyBehaviourTest::debugAutoBehaviour(Ref *sender)
{
	int monsterID = 12;	// autohide
	EnemyFactory::instance()->loadData();
	
	EnemyBehaviour *behaviour = EnemyFactory::instance()->createBehaviourByMonsterID(monsterID);
	
	log("behaviour=%s", behaviour->infoAttribute().c_str());
	
	Enemy *enemy = EnemyFactory::instance()->createEnemy(monsterID);
	log("enemy=%s", enemy->toString().c_str());
}

#endif
