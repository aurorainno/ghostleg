#ifdef ENABLE_TDD
//
//  EnemyFactoryTest.h
//
//
#ifndef __TDD_EnemyFactoryTest__
#define __TDD_EnemyFactoryTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class EnemyFactoryTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testEnemyData();
};

#endif

#endif
