#ifdef ENABLE_TDD
//
//  UpdateCheckerTest.m
//	TDD Framework
//
//
#include "UpdateCheckerTest.h"
#include "TDDHelper.h"
#include "UpdateChecker.h"

void UpdateCheckerTest::setUp()
{
	log("TDD Setup is called");
}


void UpdateCheckerTest::tearDown()
{
	log("TDD tearDown is called");
}

void UpdateCheckerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void UpdateCheckerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void UpdateCheckerTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testChecking);
	ADD_TEST(testParseResponse);
	ADD_TEST(testPopup);
}

#pragma mark -
#pragma mark Sub Test Definition
void UpdateCheckerTest::testSample()
{
	log("this is a sample subTest");
	
	log("%s", UpdateChecker::instance()->info().c_str());
}

void UpdateCheckerTest::testChecking()
{
	log("this is a sample subTest");
	
	UpdateChecker::instance()->setGUIFile("UpdateNoticeLayer.csb");
	UpdateChecker::instance()->startChecking();
}

void UpdateCheckerTest::testParseResponse()
{
	UpdateChecker::instance()->testParseResponse();
}

void UpdateCheckerTest::testPopup()
{
	UpdateChecker::instance()->setGUIFile("UpdateNoticeLayer.csb");
	UpdateChecker::instance()->showPopup();
}


#endif
