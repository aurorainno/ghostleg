#ifdef ENABLE_TDD
//
//  NPCOrderUITest.h
//
//
#ifndef __TDD_NPCOrderUITest__
#define __TDD_NPCOrderUITest__

// Include Header

#include "TDDBaseTest.h"
#include "NPCOrderSelectDialog.h"

class NPCOrder;

// Class Declaration
class NPCOrderUITest : public TDDBaseTest , public NPCOrderSelectDialogListener
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testNPCOrderItemView();
    void testNPCOrderSelectDialog();
    
#pragma ---listener callback---
private:
    void onNPCOrderSelected(NPCOrder* order)override;
    void onRefreshNPC()override;
    void onDialogClosed()override;

private:
	NPCOrderSelectDialog *mDialog;
};

#endif

#endif
