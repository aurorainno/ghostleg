#ifdef ENABLE_TDD
//
//  TutorialTest.h
//
//
#ifndef __TDD_TutorialTest__
#define __TDD_TutorialTest__

// Include Header

#include "TDDBaseTest.h"

class TutorialLayer;

// Class Declaration 
class TutorialTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void tutorialScene();
	void testShowTutorial();
	void testDialog();
	void testShowDialog();
	void testCloseDialog();
	void testShowCsb();
	void testTutorial();
	void testAnimation();
	void testShowEndBanner();
	void testFinalDisplayPos();
	void testCanSlopeLineStart();
	void testCanSlopeLineEnd();
private:
	TutorialLayer *mTutorialLayer;
}; 

#endif

#endif
