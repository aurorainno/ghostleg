#ifdef ENABLE_TDD
//
//  AstrodogDataTest.h
//
//
#ifndef __TDD_AstrodogDataTest__
#define __TDD_AstrodogDataTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class AstrodogDataTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testUser();
	void testParseResponse();
};

#endif

#endif
