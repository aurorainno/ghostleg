#ifdef ENABLE_TDD
//
//  GameResTest.h
//
//
#ifndef __TDD_GameResTest__
#define __TDD_GameResTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class GameResTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testMoneyIcon();
	void testCountryFlag();
};

#endif

#endif
