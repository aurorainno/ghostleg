#ifdef ENABLE_TDD
//
//  DialogTest.h
//
//
#ifndef __TDD_DialogTest__
#define __TDD_DialogTest__

// Include Header

#include "TDDTest.h"

class GameOverDialog;

// Class Declaration 
class DialogTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
	
	// Helper
	GameOverDialog *createGameOverDialog();
private:
	void testGameOver(Ref *sender);
	void testHideRevive(Ref *sender);
	void testUnlockMsg(Ref *sender);
	void testGameOverNewScore(Ref *sender);
}; 

#endif

#endif
