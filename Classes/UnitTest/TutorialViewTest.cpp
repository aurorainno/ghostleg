#ifdef ENABLE_TDD
//
//  TutorialViewTest.m
//	TDD Framework 
//
//
#include "TutorialViewTest.h"
#include "TDDHelper.h"
#include "TutorialView.h"
#include "GameManager.h"
#include "UserGameData.h"

void TutorialViewTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void TutorialViewTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void TutorialViewTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(TutorialViewTest::subTest);
	SUBTEST(TutorialViewTest::resetTutorial);
	SUBTEST(TutorialViewTest::testView);
}

#pragma mark -
#pragma mark Sub Test Definition
void TutorialViewTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
}

void TutorialViewTest::testView(Ref *sender)
{
	TutorialView *view = TutorialView::create();
	
	view->setCallback([] (Ref *) {
		log("Tutorial is ended");
	});
	
	addChild(view);
}

void TutorialViewTest::resetTutorial(Ref *sender)
{
	GameManager::instance()->getUserData()->markTutorialPlayed(false);
}
#endif
