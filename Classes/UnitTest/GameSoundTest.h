#ifdef ENABLE_TDD
//
//  GameSoundTest.h
//
//
#ifndef __TDD_GameSoundTest__
#define __TDD_GameSoundTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class GameSoundTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testStageMusic();
	void testAudioEngine();
	void testCoinSoundSpeed();
	void testEffect();
	void testMusic();
	void testInGameBgm();
	void testStopMusic();
	void testPause();
	void testResume();
	void testSound();
	void testCoinSFX();
}; 

#endif

#endif
