#ifdef ENABLE_TDD
//
//  AdManagerTest.h
//
//
#ifndef __TDD_AdManagerTest__
#define __TDD_AdManagerTest__

// Include Header

#include "TDDBaseTest.h"
#include <vector>
#include <string>

// Class Declaration 
class AdManagerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void subTest();
	void changeVideo();
	void testPlayAd();
	
    void bannerAdTest();
    void videoAdTest();
    void hideAdTest();
	void showTestBanner();
	void toggleNoAd();
	
	void testfindSelectedProviderAndAd();
	void testFindDefaultVideoProvider();
	void testInterAd();
	void testAdReadiness();
	
private:
	std::string mVideoName;
	
	int mVideoToPlay;
	std::vector<std::string> mVideoList;
	std::vector<int> mVideoKeyList;
};

#endif

#endif
