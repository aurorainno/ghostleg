#ifdef ENABLE_TDD
//
//  GmDogSelectDialogTest.m
//	TDD Framework
//
//
#include "GmDogSelectDialogTest.h"
#include "TDDHelper.h"
#include "GmDogSelectionDialog.h"

void GmDogSelectDialogTest::setUp()
{
	log("TDD Setup is called");
}


void GmDogSelectDialogTest::tearDown()
{
	log("TDD tearDown is called");
}

void GmDogSelectDialogTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void GmDogSelectDialogTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void GmDogSelectDialogTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void GmDogSelectDialogTest::testSample()
{
	log("this is a sample subTest");
	
	GmDogSelectionDialog *dialog = GmDogSelectionDialog::create();
	addChild(dialog);
}


#endif
