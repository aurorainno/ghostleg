#ifdef ENABLE_TDD
//
//  ParallaxTest.m
//	TDD Framework 
//
//
#include "ParallaxTest.h"
#include "TDDHelper.h"
#include "ParallaxLayer.h"
#include "SpriteParallaxSubLayer.h"
#include "CsbParallaxSubLayer.h"
#include "StageParallaxLayer.h"
#include "VisibleRect.h"

const float kSpeed = 100.0f;

namespace {
	SpriteParallaxSubLayer *createSpriteLayer(const std::string &spriteName,
				const Size &parentSize, const Vec2 &speed,
				float opacity, float scale, float spacing)
	{
		SpriteParallaxSubLayer *layer = new SpriteParallaxSubLayer();
		layer->init(parentSize, speed);
		layer->setOpacity(opacity);
		
		layer->setSpriteByName(spriteName, scale, spacing);

		
		layer->autorelease();
		
		return layer;
	}
}

void ParallaxTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	mScrollY = 0;
	mScrollX = 0;
	mIsPlaying = false;
	mTestSubLayer = nullptr;
	mParallaxLayer = nullptr;
	mCsbSubLayer = nullptr;
}


void ParallaxTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ParallaxTest::defineTests()
{
	ADD_TEST(moveUp);
	ADD_TEST(moveDown);
	ADD_TEST(togglePlay);
	ADD_TEST(testForest);
	ADD_TEST(testCsbParallax);
	ADD_TEST(testHeaderParallax);
	ADD_TEST(moveLeft);
	ADD_TEST(moveRight);
	ADD_TEST(testTintParallax);
	ADD_TEST(testBackgroundTint);
	ADD_TEST(testSpaceLayer);
	ADD_TEST(subTest);
	ADD_TEST(testSpaceScene);
	ADD_TEST(addParallaxLayer);
	ADD_TEST(testScrollingSpace);
}

#pragma mark -
#pragma mark Sub Test Definition
void ParallaxTest::togglePlay()
{
	if(mIsPlaying == false) {
		mIsPlaying = true;
		scheduleUpdate();
	} else {
		mIsPlaying = false;
		unscheduleUpdate();
	}
}

void ParallaxTest::moveUp()
{
	mScrollY += 10;
	updateScroll();
}

void ParallaxTest::moveDown()
{
	mScrollY -= 10;
	updateScroll();
}

void ParallaxTest::moveLeft()
{
	mScrollX -= 10;
	updateScroll();
}

void ParallaxTest::moveRight()
{
	mScrollX += 10;
	updateScroll();
}

void ParallaxTest::update(float delta)
{
	mScrollY += kSpeed * delta;
	updateScroll();
}

void ParallaxTest::testCsbParallax()
{
	CsbParallaxSubLayer *layer = new CsbParallaxSubLayer();
	layer->init(Size(320, 400), Vec2(0, 3));
	addChild(layer);
	
	layer->setupCsbByName("Space/SpaceObjectsLayer.csb");
	
	mCsbSubLayer = layer;
}

void ParallaxTest::updateScroll()
{
	if(mTestSubLayer) {
		mTestSubLayer->setScrollY(mScrollY);
		mTestSubLayer->setScrollX(mScrollX);
	}
	if(mParallaxLayer) {
		mParallaxLayer->setScrollY(mScrollY);
		mParallaxLayer->setScrollX(mScrollX);
	}
	if(mCsbSubLayer) {
		mCsbSubLayer->setScrollY(mScrollY);
		mCsbSubLayer->setScrollX(mScrollX);
	}
}

void ParallaxTest::subTest()
{
	log("this is a sample subTest");

	SpriteParallaxSubLayer *layer = new SpriteParallaxSubLayer();
	layer->init(Size(320, 400), Vec2(0, 3));
	layer->setColor(Color3B::BLACK);
	layer->setOpacity(255);
	
	layer->setSpriteByName("smoke_layer.png");

	addChild(layer);
	
	mTestSubLayer = layer;
}


void ParallaxTest::addParallaxLayer()
{
	log("this is a sample subTest");
	
	ParallaxLayer *layer = ParallaxLayer::create();
	layer->setColor(Color3B::BLACK);
	addChild(layer);
	mParallaxLayer = layer;

	
	Size parentSize = layer->getContentSize();
	std::string spriteName = "smoke_layer.png";
	
	log("DEBUG: parentSize: w=%f h=%f", parentSize.width, parentSize.height);
	
	
	SpriteParallaxSubLayer *subLayer;
	subLayer = createSpriteLayer(spriteName, parentSize, Vec2(0.5f, 0.5f), 100, 0.5f, 50);
	layer->addSubLayer(subLayer);

	subLayer = createSpriteLayer(spriteName, parentSize, Vec2(1.0f, 1.0f), 50, 1.0f, 30);
	layer->addSubLayer(subLayer);

	subLayer = createSpriteLayer(spriteName, parentSize, Vec2(2.0f, 2.0f), 50, 1.5f, 10);
	layer->addSubLayer(subLayer);

	
	
}


void ParallaxTest::testSpaceScene()
{
	log("this is a sample subTest");
	
	ParallaxLayer *layer = ParallaxLayer::create();
	layer->setColor(Color3B::BLACK);
	addChild(layer);
	mParallaxLayer = layer;
	
	
	Size parentSize = layer->getContentSize();
	
	
	SpriteParallaxSubLayer *subLayer;
	
	// Gradient
	subLayer = createSpriteLayer("bg_gradient.png", parentSize, Vec2(0, 0), 1, 0.5f, 0);
	layer->addSubLayer(subLayer);
	
	// Star 1
	subLayer = createSpriteLayer("space_parax_01.png", parentSize, Vec2(0.1f, 0.1f), 0.8f, 0.5, 80);
	layer->addSubLayer(subLayer);

	// Star 2
	subLayer = createSpriteLayer("space_parax_02.png", parentSize, Vec2(0.5f, 0.3f), 0.3f, 0.8, 200);
	layer->addSubLayer(subLayer);

	
	// Star 2
	
//	subLayer = createSpriteLayer(spriteName, parentSize, Vec2(0.5f, 100, 0.5f, 50);
//	layer->addSubLayer(subLayer);
//	
//	subLayer = createSpriteLayer(spriteName, parentSize, 1.0f, 50, 1.0f, 30);
//	layer->addSubLayer(subLayer);
//	
//	subLayer = createSpriteLayer(spriteName, parentSize, 2.0f, 50, 1.5f, 10);
//	layer->addSubLayer(subLayer);
	
	
	
}


void ParallaxTest::testForest()
{
	StageParallaxLayer *layer = StageParallaxLayer::create();
	
	addChild(layer);
	
	mParallaxLayer = layer;
	
}


void ParallaxTest::testSpaceLayer()
{
	StageParallaxLayer *layer = StageParallaxLayer::create();
	
	addChild(layer);
	
	mParallaxLayer = layer;
	
}


void ParallaxTest::testScrollingSpace()
{
	StageParallaxLayer *layer = StageParallaxLayer::create();
	
	addChild(layer);
	
	layer->scheduleUpdate();
	
	mParallaxLayer = layer;
	
}


void ParallaxTest::testBackgroundTint()
{
	
	const Color3B colors[4] = {
		Color3B::RED,
		Color3B::BLUE,
		Color3B::MAGENTA,
		Color3B::ORANGE,
	};
	
	setBackgroundColor(Color3B::BLACK);
	
	Vec2 pos = VisibleRect::center();
	pos.x = 0;
	
	for(int i=0; i<4; i++) {
		Sprite *sprite = Sprite::create("bg_gradient.png");
		sprite->setPosition(pos);
		addChild(sprite);
		
		sprite->setScale(0.2f);
		sprite->setContentSize(Size(40, 300));
		
		sprite->setColor(colors[i]);
		sprite->setAnchorPoint(Vec2(0, 0.5f));
		pos.x += 50;
	}
//
//	addChild(layer);
//	
//	layer->setBackgroundTint(Color3B::RED);
//	
//	layer->scheduleUpdate();
//	
//	mParallaxLayer = layer;
	
}


void ParallaxTest::testTintParallax()
{
	
	const Color3B colors[4] = {
		Color3B::RED,
		Color3B::BLUE,
		Color3B::MAGENTA,
		Color3B::ORANGE,
	};
	
	setBackgroundColor(Color3B::BLACK);
	StageParallaxLayer *layer = StageParallaxLayer::create();
	layer->setBackgroundTint(colors[2]);
	addChild(layer);
	
	layer->scheduleUpdate();
	
	mParallaxLayer = layer;
	
}


void ParallaxTest::testHeaderParallax()
{
	log("this is a sample subTest");
	
	ParallaxLayer *layer = ParallaxLayer::create();
	layer->setColor(Color3B::BLACK);
	layer->setAutoScroll(100);
	addChild(layer);
	mParallaxLayer = layer;
	
	
	Size parentSize = layer->getContentSize();
	
	
	// Gradient
	Vec2 speed = Vec2(0, 1.0f);
	SpriteParallaxSubLayer *subLayer = new SpriteParallaxSubLayer();
	subLayer->init(parentSize, speed);
	subLayer->setupParallax("background0.png", "background1.png", 0.2f);
	layer->addSubLayer(subLayer);
	subLayer->release();

}
#endif
