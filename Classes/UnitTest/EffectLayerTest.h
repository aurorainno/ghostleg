#ifdef ENABLE_TDD
//
//  EffectLayerTest.h
//
//
#ifndef __TDD_EffectLayerTest__
#define __TDD_EffectLayerTest__

// Include Header

#include "TDDBaseTest.h"
#include "Player.h"

class EffectLayer;

// Class Declaration 
class EffectLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
	void update(float dt);
	
private:
	void testExplode();
	void testShieldBreak();
	void testDoubleStar();
    
private:
	
	void showWindEffect();
	void hideWindEffect();
	
	void testCoinAnimation();
	void testRunAddScoreAnimation();
	
	void testAddScreenLayer();
	void testAddScrollLayer();
	void testParticle();
	void testCollectCapsule();
	void testCollectCapsule2();
	void moveUp();
	void moveDown();
	void autoUp();
    void testDeduceEnergy();
	void testShowCollectItemEffect();
	void testCandyEffect();
	void testStarEffect();
	void testCaution();
	void testShowCoinEffect();
	
	void setCamera(float y);
	
private:
	float mOffset;
	int mSpeedY;
	Player* mPlayer;
	EffectLayer *mEffectLayer;
}; 

#endif

#endif
