#ifdef ENABLE_TDD
//
//  DogInfoLayerTest.m
//	TDD Framework
//
//
#include "DogInfoLayerTest.h"
#include "TDDHelper.h"
#include "DogInfoLayer.h"
#include "CharacterInfoLayer.h"
#include "PlayerManager.h"
#include "PlayerCharData.h"
#include "AbilityInfoLayer.h"

void DogInfoLayerTest::setUp()
{
	log("TDD Setup is called");
}


void DogInfoLayerTest::tearDown()
{
	log("TDD tearDown is called");
}

void DogInfoLayerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DogInfoLayerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DogInfoLayerTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testXmasDog);
	ADD_TEST(testSetMoneyType);
    ADD_TEST(testAbilityInfoPage);
}

#pragma mark -
#pragma mark Sub Test Definition
void DogInfoLayerTest::testSample()
{
	log("this is a sample subTest");
	
//	DogInfoLayer *layer = DogInfoLayer::create();
//	layer->setDog(3);
//	addChild(layer);
    CharacterInfoLayer* layer = CharacterInfoLayer::create();
    PlayerCharProfile* profile = PlayerManager::instance()->getPlayerCharProfile(2);
    PlayerCharData* data = PlayerCharData::create();
    data->setCharID(2);
    data->setCurrentLevel(2);
    data->setCollectedFragment(8);
    profile->setPlayerCharData(data);
    
    layer->setView(profile);
    addChild(layer);
}

void DogInfoLayerTest::testAbilityInfoPage()
{
    AbilityInfoLayer *layer = AbilityInfoLayer::create();
    PlayerCharProfile* profile = PlayerManager::instance()->getPlayerCharProfile(2);
    PlayerCharData* data = PlayerCharData::create();
    data->setCharID(2);
    data->setCurrentLevel(3);
    data->setCollectedFragment(20);
    profile->setPlayerCharData(data);
    layer->setView(profile);
    addChild(layer);
}

void DogInfoLayerTest::testXmasDog()
{
	DogInfoLayer *layer = DogInfoLayer::create();
	layer->setDog(25);
	addChild(layer);
}

void DogInfoLayerTest::testSetMoneyType()
{
	DogInfoLayer *layer = DogInfoLayer::create();
	layer->setDog(2);
	addChild(layer);
	
	layer->setMoneyType(MoneyTypeCandy);
}



#endif
