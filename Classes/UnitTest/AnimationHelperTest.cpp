#ifdef ENABLE_TDD
//
//  AnimationHelperTest.m
//	TDD Framework 
//
//
#include "AnimationHelperTest.h"
#include "TDDHelper.h"
#include "AnimationHelper.h"
#include "Player.h"
#include "VisibleRect.h"
#include "cocostudio/CocoStudio.h"
#include "ActionShake.h"


void AnimationHelperTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void AnimationHelperTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void AnimationHelperTest::defineTests()
{
	ADD_TEST(subTest);
	ADD_TEST(testBezier2);
	ADD_TEST(testShowAndHide);
	ADD_TEST(testMoveToAction);
	ADD_TEST(testShake);
	ADD_TEST(testSetToastEffect);
	ADD_TEST(testAddRotatingAura);
	ADD_TEST(testClosing);
	ADD_TEST(testPopup);
	ADD_TEST(testJellyEffect);
	ADD_TEST(testAnimeCsb);
	ADD_TEST(testRotateForver);
	ADD_TEST(testBlinkForever);
	ADD_TEST(testScaleUpAndFade);
	ADD_TEST(testBezier);
	
	ADD_TEST(testAlphaBlink);
	ADD_TEST(testBlink);
    ADD_TEST(testMoveByEffect);
}

Node *AnimationHelperTest::createBox(const Color4B &color)
{
	LayerColor *box = LayerColor::create(color, 50, 50);
	box->setPosition(VisibleRect::center());
	return box;
}

#pragma mark -
#pragma mark Sub Test Definition
void AnimationHelperTest::subTest()
{
	log("this is a sample subTest");
}


void AnimationHelperTest::testShowAndHide()
{
	Vec2 hidePos = Vec2(100, 300);
	Vec2 showPos = Vec2(100, 200);
	
	float duration = 0.3f;
	float showDuration = 1.0f;
	
	// Show Dialog
	Node *node = createBox(Color4B::RED);
	node->setPosition(hidePos);
	addChild(node);
	
	ActionInterval *moveIn = EaseBackOut::create(MoveTo::create(duration, showPos));
	DelayTime *delay = DelayTime::create(showDuration);
	ActionInterval *moveOut = EaseBackIn::create(MoveTo::create(duration, hidePos));
	
	Sequence *sequence = Sequence::create(moveIn, delay, moveOut, nullptr);
	node->runAction(sequence);
	
	
//	Action *action = EaseBackOut::create(AnimationHelper::createMoveToAction(duration, showPos1));
//	node->runAction(action);
//	
//	// Hide Dialog
//	Node *blueBox = createBox(Color4B::BLUE);
//	blueBox->setPosition(showPos2);
//	addChild(blueBox);
//	
//	action = EaseBackIn::create(AnimationHelper::createMoveToAction(duration, hidePos2));
//	blueBox->runAction(action);
	//
}


void AnimationHelperTest::testMoveToAction()
{
	Vec2 hidePos1 = Vec2(100, 300);
	Vec2 showPos1 = Vec2(100, 200);
	
	Vec2 hidePos2 = Vec2(200, 300);
	Vec2 showPos2 = Vec2(200, 200);
	
	float duration = 0.5f;
	
	// Show Dialog
	Node *node = createBox(Color4B::RED);
	node->setPosition(hidePos1);
	addChild(node);
	
	Action *action = EaseBackOut::create(AnimationHelper::createMoveToAction(duration, showPos1));
	node->runAction(action);
	
	// Hide Dialog
	Node *blueBox = createBox(Color4B::BLUE);
	blueBox->setPosition(showPos2);
	addChild(blueBox);
	
	action = EaseBackIn::create(AnimationHelper::createMoveToAction(duration, hidePos2));
	blueBox->runAction(action);
//
}


void AnimationHelperTest::testAddRotatingAura()
{
//	static void addRotatingAura(Node *parentNode,
//								const float &duration,
//								int numAura,
//								const std::string &auraImage);
	Vec2 pos = VisibleRect::center();
	AnimationHelper::addRotatingAura(this, 5, 3, "unlock_aura.png", pos);
	
	pos.y += 100;
	AnimationHelper::addRotatingAura(this, 4, 1, "item_aura.png", pos);
}

void AnimationHelperTest::testClosing()
{
	//clear
	clearNodes();
	
	Node *redBox = createBox(Color4B::RED);
	addChild(redBox);
	
	float duration = 0.5f;
	std::function<void()> callback = [&]{
		log("closing is done!!");
	};
	
	
	// Approach 1
	//Action *action = AnimationHelper::getClosingAction(duration, true, callback);
	//redBox->runAction(action);
	
	// Approach 2
	AnimationHelper::runClosingAction(redBox, duration, true, callback);
	//AnimationHelper::runPopupAction(redBox, 0.1f, duration, callback);
}

void AnimationHelperTest::testRotateForver()
{
	RepeatForever *action = AnimationHelper::getRotateForever(1.0f);
	
	Node *redBox = createBox(Color4B::RED);
	addChild(redBox);
	
	redBox->runAction(action);
	
}


void AnimationHelperTest::testPopup()
{
	//clear
	clearNodes();
	
	Node *redBox = createBox(Color4B::RED);
	addChild(redBox);
	
	float duration = 0.5f;
	std::function<void()> callback = [&]{
		log("popup is done!!");
	};
	
	
	// Approach 1
	// Action *action = AnimationHelper::getPopupAction(0.01f,
	//										redBox->getScaleX(), duration,
	//										callback);
	// 	redBox->runAction(action);
	
	// Approach 2
	AnimationHelper::runPopupAction(redBox, 0.1f, -1,duration, callback);
}

void AnimationHelperTest::testAnimeCsb()
{
	log("this is a sample subTest");
	Node *node = CSLoader::createNode("missile.csb");
	node->setPosition(VisibleRect::center());
	addChild(node);
}



void AnimationHelperTest::testBezier2()
{
	Node *redBox = createBox(Color4B::RED);
	redBox->setOpacity(150);
	Node *blueBox = createBox(Color4B::BLUE);
	blueBox->setOpacity(150);
	
	Vec2 fromPos = Vec2(100, 400);
	Vec2 toPos   = Vec2(100, 100);
	float duration = 3;
	
	redBox->setPosition(fromPos);
	addChild(redBox);
	
	blueBox->setPosition(fromPos);
	addChild(blueBox);
	
	
	Action *actionRed;
	Action *actionBlue;
	
	actionRed = MoveTo::create(duration, toPos);
	
	
	ccBezierConfig c;
	c.endPosition = toPos;
	c.controlPoint_1 = Vec2(fromPos.x-100, fromPos.y);		// Vec2(fromPos.x, toPos.y);
	c.controlPoint_2 = Vec2(toPos.x-100, toPos.y);
	actionBlue = BezierTo::create(duration, c);
	
	
	// run the action
	redBox->runAction(actionRed);
	blueBox->runAction(actionBlue);
	
	//
	//
	//
	//	BezierTo *b =
}


void AnimationHelperTest::testBezier()
{
	Node *redBox = createBox(Color4B::RED);
	redBox->setOpacity(150);
	Node *blueBox = createBox(Color4B::BLUE);
	blueBox->setOpacity(150);
	
	Vec2 fromPos = Vec2(100, 400);
	Vec2 toPos = Vec2(320-50, 0);
	float duration = 3;
	
	redBox->setPosition(fromPos);
	addChild(redBox);
	
	blueBox->setPosition(fromPos);
	addChild(blueBox);
	
	
	Action *actionRed;
	Action *actionBlue;
	
	actionRed = MoveTo::create(duration, toPos);
	
	
	ccBezierConfig c;
	c.endPosition = toPos;
	c.controlPoint_1 = Vec2(toPos.x, fromPos.y);		// Vec2(fromPos.x, toPos.y);
	c.controlPoint_2 = toPos;
	actionBlue = BezierTo::create(duration, c);
	
	
	// run the action
	redBox->runAction(actionRed);
	blueBox->runAction(actionBlue);
	
//
//
//	
//	BezierTo *b =
}

void AnimationHelperTest::testBlink()
{
	log("Blinking");
	
	Player *player = Player::create();
	player->setPosition(VisibleRect::center());
	addChild(player);
	
	AnimationHelper::setBlinkEffect(player, 4, 8);
}


void AnimationHelperTest::testBlinkForever()
{
	log("Blinking");
	
	Sprite *sprite = Sprite::create("general/route_line.png");		// origin size: 26 x 188
	sprite->setScale(0.4f);
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);
	
	
	std::function<void ()> func = []() {
		log("animation done!");
	};
	
	//AnimationHelper::setAlphaBlink(sprite, 3, 0, 125, 255, func);
	
	//setForeverBlink(Node *node, float duration, int opacity)
	AnimationHelper::setForeverBlink(sprite, 3, 125);
}


void AnimationHelperTest::testAlphaBlink()
{
	log("Blinking");
	
	Player *player = Player::create();
	player->setPosition(VisibleRect::center());
	addChild(player);
	
	
	std::function<void ()> func = []() {
		log("animation done!");
	};
	
	AnimationHelper::setAlphaBlink(player, 4, 8, 50, 150, func);
}


void AnimationHelperTest::testScaleUpAndFade()
{
	
	Sprite *sprite = Sprite::create("item_suit1.png");
	sprite->setScale(0.4f);
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);
	//sprite->setUserData
	
	
	Sequence *seq = Sequence::create(
						EaseOut::create(ScaleBy::create(0.3, 1.5), 5.0f),
						EaseIn::create(FadeOut::create(0.5), 2.0f),
						nullptr);
	
	sprite->runAction(seq);
}

void AnimationHelperTest::testJellyEffect()
{
//	Sprite *sprite = Sprite::create("guiImage/btn_start.png");
    Sprite *sprite = Sprite::create("guiImage/btn_ui_new.png");
	sprite->setScale(0.5f);
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);
	
	//setJellyEffect(Node *node, float cycleDuration, float scale, int repeatCount);
	AnimationHelper::setJellyEffect(sprite, 0.4,1.3, 0);
}

void AnimationHelperTest::testMoveByEffect()
{
    Sprite *sprite = Sprite::create("guiImage/btn_ui_new.png");
    sprite->setScale(0.5f);
    sprite->setPosition(VisibleRect::center());
    addChild(sprite);
    
    AnimationHelper::setMoveUpEffect(sprite, 0.3, 5, true, 0);
}

void AnimationHelperTest::testSetToastEffect()
{
	Sprite *sprite = Sprite::create("res/dialogue/ingame_dialog_02.png");
	sprite->setScale(0.5f);
	sprite->setAnchorPoint(Vec2(0.5, 0));
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);
	
//	const float &finalScale,
//	float delay,
//	float showDuration,
//	float popDuration,
//	const std::function<void ()> &callback)
	AnimationHelper::setToastEffect(sprite, 0.5f, 1, 1, 0.4, [=]{
		sprite->removeFromParent();
	});
}


void AnimationHelperTest::testShake()
{
	
	Sprite *sprite = Sprite::create("item_suit1.png");
	sprite->setScale(0.4f);
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);
	
	Shake *shake = Shake::create(1.0f, 10);
	sprite->runAction(shake);
	
//	//sprite->setUserData
//	
//	
//	Sequence *seq = Sequence::create(
//									 EaseOut::create(ScaleBy::create(0.3, 1.5), 5.0f),
//									 EaseIn::create(FadeOut::create(0.5), 2.0f),
//									 nullptr);
//	
//	sprite->runAction(seq);
}



#endif
