#ifdef ENABLE_TDD
//
//  ParticleTest.h
//
//
#ifndef __TDD_ParticleTest__
#define __TDD_ParticleTest__

// Include Header

#include "TDDBaseTest.h"


#include "extensions/cocos-ext.h"	
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
using namespace cocostudio::timeline;

// Class Declaration 
class ParticleTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	CC_SYNTHESIZE_RETAIN(ActionTimeline *, mTimeline, Timeline);
	
private:
	void subTest();
	void playParticle();
	void stopParticle();
	void playAnimation();
	void setOneTimeParticle();
	void testStatus();
	void testFireParticle();
	void testSpeedyParticle();
	
	
	void onFrameEvent(Frame *frame);

	void moveParticle(const Vec2 &diff);
	
#pragma mark - Touch Handling
	void setupTouchListener();
	bool onTouchBegan(Touch *touch, Event *event);
	void onTouchEnded(Touch *touch, Event *event);
	void onTouchMoved(Touch *touch, Event *event);
	void onTouchCancelled(Touch *touch, Event *event);

	
private:
	ParticleSystemQuad *mParticle;
	Vec2 mLastPos;
};




#endif

#endif
