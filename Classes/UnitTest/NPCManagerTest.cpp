#ifdef ENABLE_TDD
//
//  NPCManagerTest.m
//	TDD Framework
//
//
#include "NPCManagerTest.h"
#include "TDDHelper.h"
#include "NPCManager.h"
#include "StageManager.h"
#include "NPCRating.h"
#include "GameWorld.h"
#include "GameplaySetting.h"

void NPCManagerTest::setUp()
{
	log("TDD Setup is called");
	
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	
	StageManager::instance()->setGameplaySetting(setting);
	//StageManager::instance()->updateGameplaySetting();		// Setting are changed

	log("GameSetting:%s\n", setting->toString().c_str());
}


void NPCManagerTest::tearDown()
{
	log("TDD tearDown is called");
}

void NPCManagerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void NPCManagerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void NPCManagerTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testNpcRatingWithSetting);
	ADD_TEST(testGetRatingList);
	ADD_TEST(testAddNpcRating);
}

#pragma mark -
#pragma mark Sub Test Definition
void NPCManagerTest::testSample()
{
	log("this is a sample subTest");
}

void NPCManagerTest::testNpcRatingWithSetting()
{
	
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	StageManager::instance()->setGameplaySetting(setting);
	
	
	
	
	
	//
	NPCManager::instance()->resetNpcRating();
	
	NPCOrder *npcOrder;
	NPCRating *rating;
	
	// Without any Buff
	int remainTime = 20;
	setting->reset();
	npcOrder = StageManager::instance()->getNpcOrder(101);
	rating = NPCManager::instance()->addNpcRating(npcOrder, remainTime);
	log("WithoutBuff: %s", rating->toString().c_str());
	
	// With Buff
	setting->reset();
	setting->setExtraReward(5, 300);
	setting->setExtraReward(4, 200);
	setting->setExtraReward(3, 100);
	setting->setExtraReward(2, 50);
	setting->setExtraReward(1, 10);
	setting->setAttributeValue(GameplaySetting::BonusTipPercent, 0.05);

	npcOrder = StageManager::instance()->getNpcOrder(101);
	rating = NPCManager::instance()->addNpcRating(npcOrder, remainTime);
	log("WithBuff: %s", rating->toString().c_str());
}

void NPCManagerTest::testAddNpcRating()
{
	NPCManager::instance()->resetNpcRating();
	
	NPCOrder *npcOrder;
	
	npcOrder = StageManager::instance()->getNpcOrder(101);
	NPCManager::instance()->addNpcRating(npcOrder, 10);
	
	npcOrder = StageManager::instance()->getNpcOrder(101);
	NPCManager::instance()->addNpcRating(npcOrder, 5);
	
	npcOrder = StageManager::instance()->getNpcOrder(101);
	NPCManager::instance()->addNpcRating(npcOrder, 1);
	
	log("%s", NPCManager::instance()->infoNpcRating().c_str());
}

void NPCManagerTest::testGetRatingList()
{
	Vector<NPCRating *> ratingList = NPCManager::instance()->getRatingList();
	
	for(NPCRating *rating : ratingList)
	{
		log("%s", rating->toString().c_str());
	}
	
	log("Other Data");
	log("MoneyEarn     : %d", NPCManager::instance()->getTotalCollected());
	log("Tip Earn      : %d", NPCManager::instance()->getTotalTip());
	log("Client Count  : %d", NPCManager::instance()->getClientCount());
	log("Client Score  : %d", NPCManager::instance()->getClientScore());
	log("NoCrash Score : %d", NPCManager::instance()->getNoCrashScore());
	log("Total Score   : %d", NPCManager::instance()->getTotalScore());
}

#endif
