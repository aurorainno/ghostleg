#ifdef ENABLE_TDD
//
//  ResponsiveUITest.h
//
//
#ifndef __TDD_ResponsiveUITest__
#define __TDD_ResponsiveUITest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class ResponsiveUITest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
