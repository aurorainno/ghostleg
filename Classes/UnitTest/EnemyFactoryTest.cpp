#ifdef ENABLE_TDD
//
//  EnemyFactoryTest.m
//	TDD Framework
//
//
#include "EnemyFactoryTest.h"
#include "TDDHelper.h"
#include "EnemyFactory.h"

void EnemyFactoryTest::setUp()
{
	log("TDD Setup is called");
}


void EnemyFactoryTest::tearDown()
{
	log("TDD tearDown is called");
}

void EnemyFactoryTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void EnemyFactoryTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void EnemyFactoryTest::defineTests()
{
	ADD_TEST(testEnemyData);
}

#pragma mark -
#pragma mark Sub Test Definition
void EnemyFactoryTest::testEnemyData()
{
//	log("this is a sample subTest");
	std::string info = EnemyFactory::instance()->infoEnemyData();
	
	log("info: %s", info.c_str());
}


#endif
