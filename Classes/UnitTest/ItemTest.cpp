#ifdef ENABLE_TDD
//
//  ItemTest.m
//	TDD Framework 
//
//
#include "ItemTest.h"
#include "TDDHelper.h"
#include "Coin.h"
#include "Star.h"
#include "EnergyPotion.h"
#include "Capsule.h"
#include "ItemEffect.h"
#include "GameWorld.h"
#include "Missile.h"
#include "Player.h"
#include "Cashout.h"
#include "VisibleRect.h"
#include "MagnetCapsule.h"

void ItemTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");

	mItem = nullptr;
}


void ItemTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void ItemTest::defineTests()
{
	ADD_TEST(testStar);
	ADD_TEST(testCashout);
	ADD_TEST(testMagnet);
	ADD_TEST(testCoin);
	ADD_TEST(testCapsule);
	ADD_TEST(testMissile);
	ADD_TEST(testPotion);
	ADD_TEST(testUse);
	ADD_TEST(testAllCapsule);
}

void ItemTest::setItem(Item *item)
{
	Item *oldRef = mItem;
	CC_SAFE_RETAIN(item);
	mItem = item;

	
	if(oldRef != nullptr) {
		removeChild(oldRef);
	}
	
	CC_SAFE_DELETE(oldRef);
}


#pragma mark -
#pragma mark Sub Test Definition
void ItemTest::testCashout()
{
	Cashout *cashout = Cashout::create();
	
	cashout->setPosition(VisibleRect::center());
	
	addChild(cashout);
	
	setItem(cashout);
}

void ItemTest::testMagnet()
{
	MagnetCapsule *magnet = MagnetCapsule::create();
	
	magnet->setPosition(VisibleRect::center());
	
	addChild(magnet);
	
	setItem(magnet);
}


void ItemTest::testMissile()
{
	log("testing for star");
	
	Missile *missile = Missile::create();
	
	missile->setPosition(Vec2(50, 100));
	
	missile->setRotation(-50);
	
	addChild(missile);
	
	
	setItem(missile);
	
	missile->runAction(MoveTo::create(2, Vec2(200, 500)));
}

void ItemTest::testStar()
{
	log("testing for star");
	
    Star *star = Star::create(Star::SmallStar);
	
	star->setPosition(Vec2(50, 100));
	
	addChild(star);
	
	setItem(star);
}



void ItemTest::testPotion()
{
	log("this is a sample subTest");
	
	EnergyPotion *item = EnergyPotion::create();
	
	item->setPosition(Vec2(50, 100));
	
	addChild(item);
	
	setItem(item);
}


void ItemTest::testCapsule()
{
	
	Capsule *item = Capsule::create();
	
	item->setItemEffectType(ItemEffect::Type::ItemEffectMissile);
	
	item->setPosition(Vec2(50, 100));
	
	addChild(item);
	
	setItem(item);
}

void ItemTest::testAllCapsule()
{
	const int nCapsule = 4;
	ItemEffect::Type capsuleType[nCapsule] = {
		ItemEffect::Type::ItemEffectMissile,
		ItemEffect::Type::ItemEffectDoubleCoins,
		ItemEffect::Type::ItemEffectInvulnerable,
		ItemEffect::Type::ItemEffectTimeStop,
	};
	
	Vec2 pos = Vec2(50, 100);
	
	for(int i=0; i<nCapsule; i++) {
		Capsule *item = Capsule::create();
		
		item->setItemEffectType(capsuleType[i]);
		
		item->setPosition(pos);
		
		addChild(item);
		
		pos.y += 80;
	}
}



void ItemTest::testCoin()
{
	log("this is a sample subTest");

    Coin *coin = Coin::create(Coin::SmallCoin);
	
	coin->setPosition(Vec2(50, 100));
	
	addChild(coin);
	
	setItem(coin);
}


void ItemTest::testUse()
{
	if(mItem == nullptr) {
		log("No item is defined");
		return;
	}
	
	Player *player = GameWorld::instance()->getPlayer();
	
	log("before using item: player: %s\n", player->infoAttribute().c_str());
	
	mItem->use();
	
	log("After using item: player: %s\n", player->infoAttribute().c_str());
}

#endif
