#ifdef ENABLE_TDD
//
//  SettingSceneTest.h
//
//
#ifndef __TDD_SettingSceneTest__
#define __TDD_SettingSceneTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class SettingSceneTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
