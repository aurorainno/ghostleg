#ifdef ENABLE_TDD
//
//  AnalyticsTest.m
//	TDD Framework 
//
//
#include "AnalyticsTest.h"
#include "TDDHelper.h"
#include "Analytics.h"

void AnalyticsTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void AnalyticsTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void AnalyticsTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(AnalyticsTest::testLogScreen);
	SUBTEST(AnalyticsTest::testLogEvent);
}

#pragma mark -
#pragma mark Sub Test Definition
void AnalyticsTest::testLogScreen(Ref *sender)
{
	//Analytics::instance()->logScreen(Analytics::Screen::main);
	LOG_SCREEN(Analytics::Screen::main_screen);
}

void AnalyticsTest::testLogEvent(Ref *sender)
{
	//Analytics::instance()->logScreen(Analytics::Screen::main);
	LOG_EVENT(Analytics::Category::testing, Analytics::Action::on_enter, "", 0);
}

#endif
