#ifdef ENABLE_TDD
//
//  ADResponseGetRankingTest.h
//
//
#ifndef __TDD_ADResponseGetRankingTest__
#define __TDD_ADResponseGetRankingTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class ADResponseGetRankingTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testParse();
	void testParseSubmitScore();
	void testParseFBConnect();
};

#endif

#endif
