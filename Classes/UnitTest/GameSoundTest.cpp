#ifdef ENABLE_TDD
//
//  GameSoundTest.m
//	TDD Framework 
//
//
#include "GameSoundTest.h"
#include "TDDHelper.h"
#include "GameSound.h"
#include "TimeMeter.h"
#include <AudioEngine.h>

void GameSoundTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	GameSound::preloadGameSound();
	
}


void GameSoundTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

void GameSoundTest::defineTests()
{
	ADD_TEST(testAudioEngine);
	ADD_TEST(testStageMusic);
	ADD_TEST(testCoinSoundSpeed);
	ADD_TEST(testCoinSFX);
	ADD_TEST(testEffect);
	ADD_TEST(testMusic);
	ADD_TEST(testInGameBgm);
	ADD_TEST(testStopMusic);
	ADD_TEST(testPause);
	ADD_TEST(testResume);
	ADD_TEST(testSound);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameSoundTest::testAudioEngine()
{
	cocos2d::experimental::AudioEngine::play2d("coin_3.mp3");
}

void GameSoundTest::testCoinSoundSpeed()
{

	int loop = 100;
	for(int i=0; i<loop; i++) {
		TimeMeter::instance()->start("Coin.playSound");
		//GameSound::playStarSound();
		GameSound::playCoinSound();
		TimeMeter::instance()->stop("Coin.playSound");
	}

	TimeMeter::instance()->showSummary();
}

void GameSoundTest::testStageMusic()
{
	static int stage = 1;
	
	GameSound::playStageMusic(stage);
	
	
	stage++;
	if(stage > 5) {
		stage = 1;
	}
}


void GameSoundTest::testEffect()
{
	log("testEffect");
	GameSound::playSound(GameSound::Potion);
}

void GameSoundTest::testMusic()
{
	GameSound::playMusic(GameSound::InGame);
}

void GameSoundTest::testInGameBgm()
{
	static int music = GameSound::Bgm01;
	
	GameSound::playMusic((GameSound::Music) music);
	
	
	// next music
	if(music == GameSound::Bgm03) {
		music = GameSound::Bgm01;
	} else {
		music++;
	}
	
	
}

void GameSoundTest::testStopMusic()
{
	GameSound::stop();
}


void GameSoundTest::testPause()
{
	GameSound::pause();
}


void GameSoundTest::testResume()
{
	GameSound::resume();
}

void GameSoundTest::testSound()
{
	//GameSound::playSound(GameSound::VoiceReady);
	GameSound::playSound(GameSound::CountDown);
}

void GameSoundTest::testCoinSFX()
{
	
	GameSound::playStarSound();
	
	//GameSound::playSound(GameSound::VoiceReady);
	// GameSound::playSound(GameSound::CountDown);
}


#endif
