#ifdef ENABLE_TDD
//
//  DogSuggestionLayerTest.h
//
//
#ifndef __TDD_DogSuggestionLayerTest__
#define __TDD_DogSuggestionLayerTest__

// Include Header

#include "TDDBaseTest.h"

class DogSuggestionLayer;

// Class Declaration
class DogSuggestionLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testChangeDog();
	
	DogSuggestionLayer *mLayer;
};

#endif

#endif
