#ifdef ENABLE_TDD
//
//  MapDataTest.m
//	TDD Framework
//
//
#include "MapDataTest.h"
#include "TDDHelper.h"
#include "LevelData.h"
#include "StringHelper.h"

void MapDataTest::setUp()
{
	log("TDD Setup is called");
}


void MapDataTest::tearDown()
{
	log("TDD tearDown is called");
}

void MapDataTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void MapDataTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void MapDataTest::defineTests()
{
	ADD_TEST(testEnemyList);
	ADD_TEST(testSample);
	ADD_TEST(testRandomLine);
}

#pragma mark -
#pragma mark Sub Test Definition
void MapDataTest::testEnemyList()
{
	log("this is a sample subTest");
	
	std::string mapFile = StringUtils::format("mapData/map%d.dat", 801);
	// Load the Map
	MapLevelData *mapData = new MapLevelData();
	bool isLoaded = mapData->load(mapFile.c_str());
	
	log("mapData: %s", mapData->toString().c_str());
	
	std::vector<int> enemyList = mapData->getEnemyIDList();
	std::vector<int> npcList = mapData->getNpcIDList();
	
	log("EnemyList: %s", VECTOR_TO_STR(enemyList).c_str());
	log("npcList: %s", VECTOR_TO_STR(npcList).c_str());
	// mapData->preloadCsb();
}


void MapDataTest::testSample()
{
	log("this is a sample subTest");
	
	std::string mapFile = StringUtils::format("mapData/map%d.dat", 1);
	
	
	if(FileUtils::getInstance()->isFileExist(mapFile) == false) {
		log("file not found: %s", mapFile.c_str());
		return;
	}
	
	// Load the Map
	MapLevelData *mapData = new MapLevelData();
	bool isLoaded = mapData->load(mapFile.c_str());
	
	log("mapData: %s", mapData->toString().c_str());
}


void MapDataTest::testRandomLine()
{
	log("this is a sample subTest");
	
	std::string mapFile = StringUtils::format("mapData/map%d.dat", 1);
	
	if(FileUtils::getInstance()->isFileExist(mapFile) == false) {
		log("file not found: %s", mapFile.c_str());
		return;
	}
	
	// Load the Map
	MapLevelData *mapData = new MapLevelData();
	bool isLoaded = mapData->load(mapFile.c_str());
	
	log("mapData: %s", mapData->toString().c_str());
	
	
	Vector<LevelLineData *> randomLine;
	
	mapData->getRandomLineData(randomLine);
	
	
	for(LevelLineData *line : randomLine) {
		log("line=%s", line->toString().c_str());
	}
	
	
}

#endif
