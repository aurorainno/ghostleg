#ifdef ENABLE_TDD
//
//  GameMapTest.m
//	TDD Framework 
//
//
#include "GameMapTest.h"
#include "TDDHelper.h"
#include "GameMap.h"
#include "MapLine.h"
#include "MapDataGenerator.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "Player.h"
#include "StringHelper.h"
#include "GeometryHelper.h"

float kScroll = 111;

void GameMapTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");

	setBackgroundColor(Color3B(0, 0, 50));
	
	GameMap *map = GameMap::create();
	
	
	//map->addSlopeLine(Vec2(50, 100), Vec2(100, 200));
	//MapLine *line = MapLine::create(MapLine::eTypeSlope, 0, 0, 200, 200);
	MapLine *line = MapLine::create(MapLine::eTypeSlope, 0, 100, 200, 100);
	map->addSlopeLine(line);
	
	addChild(map);

	mMap = map;
	mScroll = 0;
	
	mLastLine = nullptr;
}


void GameMapTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void GameMapTest::defineTests()
{
	ADD_TEST(scrollUp);
	ADD_TEST(scrollDown);
	ADD_TEST(testIsOnVertLine);
	ADD_TEST(clearMap);
	ADD_TEST(testEffectRotation);
	ADD_TEST(testAdjustPositionBeforeLastDropLine);
	ADD_TEST(testAddSlopeLine);
	ADD_TEST(testRemoveOldLines);
	ADD_TEST(showTestMap);
	ADD_TEST(addMapLine);
	ADD_TEST(removeRouteLine);
	ADD_TEST(addTestLines);
	ADD_TEST(testGetApproachingLineX);
}

#pragma mark -
#pragma mark Sub Test Definition


void GameMapTest::testGetApproachingLineX()
{
	std::vector<float> approachRightList;
	
	approachRightList.push_back(10);
	approachRightList.push_back(25);	// boundry case
	approachRightList.push_back(35);
	approachRightList.push_back(80);
	approachRightList.push_back(204);
	approachRightList.push_back(205);	// boundry case
	approachRightList.push_back(206);	// out bound
	
	for(float x : approachRightList) {
		int line = mMap->getApproachingLineIndex(x, DirRight);
		
		log("myX=%f line=%d lineX=%d" , x, line, mMap->getVertLineX(line));
	}
	
}


void GameMapTest::scrollUp()
{
	log("this is a sample subTest");
	
	mScroll += kScroll;
	
	mMap->setOffsetY(mScroll);
	//mMap->setCa
}

void GameMapTest::scrollDown()
{
	log("this is a sample subTest");
	
	mScroll -= kScroll;
	
	mMap->setOffsetY(mScroll);
}

void GameMapTest::clearMap()
{
	log("this is a sample subTest");
	mMap->clearData();
}

void GameMapTest::showTestMap()
{
	//int mapID = 20000;
	int mapID = 10001;
	//int mapID = 41;
	
	Player *player = (Player *) GameWorld::instance()->getPlayer();
	
	MapDataGenerator generator;
	generator.init(mMap, player, 0);
	generator.generateByMapID(mapID);
	
	
	mMap->addSlopeLines(generator.getHoriLine());
	//log("Generator:\n%s", generator->info().c_str());
	
}

void GameMapTest::addMapLine()
{
	float startX = (float) RandomHelper::random_int(10, 100);
	float startY = (float) RandomHelper::random_int(100, 200);
	float endX = startX + RandomHelper::random_int(0, 100);
	float endY = startY + RandomHelper::random_int(-100, 100);
	
	mMap->setStage(3);
	
	MapLine *line = MapLine::create(MapLine::eTypeSlope, startX, startY, endX, endY);
	mMap->addSlopeLine(line);
	mLastLine = line;
}

void GameMapTest::removeRouteLine()
{
	if(mLastLine == nullptr) {
		return;
	}
	
	mMap->removeRouteLine(mLastLine);
}

void GameMapTest::addTestLines()
{
	if(mMap == nullptr) {
		return;
	}
	float posY = mMap->getOffsetY();
	float posX = 10;
	
	int numLines = 30;
	
	for(int i=0; i<numLines; i++) {
		float startX = posX;
		float startY = posY;
		float endX = startX + 100;
		float endY = posY;
	
		MapLine *line = MapLine::create(MapLine::eTypeSlope, startX, startY, endX, endY);
		mMap->addSlopeLine(line);
		mLastLine = line;
		
		posY += 100;
		posX += 5;
	}
}

void GameMapTest::testRemoveOldLines()
{
	if(mMap == nullptr) {
		return;
	}
	
	log("Lines before Cleanup: %s", mMap->infoHoriLines().c_str());
	
	mMap->cleanupOldRouteLine();
	
	log("Lines after Cleanup: %s", mMap->infoHoriLines().c_str());
}

void GameMapTest::testAddSlopeLine()
{
	Vec2 playerPos = Vec2(40, 110);
	GameWorld::instance()->getPlayer()->setPosition(playerPos);
	
	mMap->clearData();
	
	log("Lines after Cleanup:\n%s", mMap->infoHoriLines().c_str());
	
	Vec2 startPos = Vec2(40, 100);
	Vec2 endPos = Vec2(120, 150);
	
	mMap->addSlopeLine(startPos, endPos);
	
	
	log("Lines after add 1st line:\n%s", mMap->infoHoriLines().c_str());
}

void GameMapTest::testAdjustPositionBeforeLastDropLine()
{
	mMap->clearData();
	
	Vec2 startPos = Vec2(40, 100);
	Vec2 endPos = Vec2(120, 150);
	mMap->addSlopeLine(startPos, endPos);
	log("Lines after add 1st line:\n%s", mMap->infoHoriLines().c_str());

	Vec2 checkPos = Vec2(120, 100);
	
	Vec2 result = mMap->adjustPositionBeforeLastDropLine(checkPos);
	
	log("resultPos=%s", POINT_TO_STR(result).c_str());
	
}



//void GameModel::setCsbAnimeRotation(float degree)
//{
//	std::vector<std::string> mapKeyVec = mCsbNodeMap.keys();
//	
//	for(std::string key : mapKeyVec)
//	{
//		Node *csbNode = mCsbNodeMap.at(key);
//		csbNode->setRotation(degree);
//	}
//}
void GameMapTest::testEffectRotation()
{
	float startX = 160; float startY = 200;
	
	Vec2 startPoint = Vec2(startX, startY);
	std::vector<Vec2> endPointList;
	//endPointList.push_back(Vec2(210, 280));	// quarter1
	//endPointList.push_back(Vec2(110, 280));		// quarter4
	endPointList.push_back(Vec2(210, 120));			// quarter3
	//endPointList.push_back(Vec2(110, 120));
	
	
	mMap->setStage(3);
	
	for(Vec2 endPoint : endPointList) {
		MapLine *line;
		float startX = startPoint.x;
		float startY = startPoint.y;
		float endX = endPoint.x;
		float endY = endPoint.y;
		
		
		line = MapLine::create(MapLine::eTypeSlope, startX, startY, endX, endY);
		mMap->addSlopeLine(line);
		
		float rotation = aurora::GeometryHelper::findCocosDegree(startPoint, endPoint);
//
//		
		Node *csbNode = CSLoader::createNode("SpeedUp.csb");
		csbNode->setRotation(90 - rotation);
		csbNode->setPosition(startPoint);
		csbNode->setScale(0.4f);
		csbNode->setAnchorPoint(Vec2(0.5, 0));
		addChild(csbNode);
		log("ROTATION: start=%s end=%s rotation=%f", POINT_TO_STR(startPoint).c_str(),
			POINT_TO_STR(endPoint).c_str(),
			rotation);
		
	}
	
	
	
	
	//mLastLine = line;
}

void GameMapTest::testIsOnVertLine()
{
	for(float x=0; x<=320; x+=10) {
		bool result = GameMap::isOnVertLine(x);
		
		log("x=%f result=%d", x, result);
	}
	
	
}

#endif
