#ifdef ENABLE_TDD
//
//  GameUITest.h
//
//
#ifndef __TDD_GameUITest__
#define __TDD_GameUITest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class GameUITest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
	void testTestUI(Ref *sender);
	void testParticle(Ref *sender);
	void testParticleAutoRemove(Ref *sender);
	void testCheckRemove(Ref *sender);
	void testMask(Ref *sender);
	void testMask2(Ref *sender);
}; 

#endif

#endif
