#ifdef ENABLE_TDD
//
//  PackageBoxTest.m
//	TDD Framework
//
//
#include "PackageBoxTest.h"
#include "TDDHelper.h"
#include "PackageBox.h"
#include "VisibleRect.h"

void PackageBoxTest::setUp()
{
	log("TDD Setup is called");

	//
	PackageBox *packageBox = PackageBox::create();
	packageBox->setPosition(VisibleRect::center());
	
	addChild(packageBox);
	
	mPackageBox = packageBox;

}


void PackageBoxTest::tearDown()
{
	log("TDD tearDown is called");
}

void PackageBoxTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PackageBoxTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PackageBoxTest::defineTests()
{
	ADD_TEST(testDropDown);
}

#pragma mark -
#pragma mark Sub Test Definition
void PackageBoxTest::testDropDown()
{
	log("this is a sample subTest");
	mPackageBox->showAnimation(PackageBox::Animation::DropDown,
							   [&](void) {
								   log("Animation Done");
							   });
}


#endif
