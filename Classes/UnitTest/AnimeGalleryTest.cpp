#ifdef ENABLE_TDD
//
//  AnimeGalleryTest.m
//	TDD Framework
//
//
#include "AnimeGalleryTest.h"
#include "TDDHelper.h"
#include "StageParallaxLayer.h"
#include "GameModel.h"
#include "VisibleRect.h"

const int numAnimation = 6;
const char animationArray[numAnimation][50] =
{
	"player/player_001.csb",
	"player/player_002.csb",
	"player/player_003.csb",
	"player/player_004.csb",
	"player/player_005.csb",
	"player/player_006.csb",
};

const int numAction = 3;
const GameModel::Action actionArray[numAction] =
{
	GameModel::Action::Climb,
	GameModel::Action::Walk,
	GameModel::Action::Die,
};


void AnimeGalleryTest::setupBackground()
{
	bool useParallax = false;
	
	// Make the background fill with parallax
	//
	if(useParallax) {
		StageParallaxLayer *parallax = StageParallaxLayer::create();
		addChild(parallax);
		parallax->scheduleUpdate();
	} else {
		// Fill with color
		setBackgroundColor(Color3B(0, 255, 0));
	}
}

void AnimeGalleryTest::update(float delta)
{
//	float deltaY = delta * -20;
//	Vec2 newPos = mModel->getPosition() + Vec2(0, deltaY);
//	mModel->setPosition(newPos);
	
	Vec2 modelPos = mModel->getPosition();
//	Size screenSize = VisibleRect::getVisible
//	float offset = modelPos->getPosition().y - screenSize.height / 2;
//	
//	
}

void AnimeGalleryTest::setupModel()
{
	// The Model
	mModel = GameModel::create();
	mModel->setScale(1.0f);
	
	addChild(mModel);
	mModel->setPosition(VisibleRect::center());
	
	Vec2 pos = VisibleRect::center();
	
}

void AnimeGalleryTest::setupTouchControl()
{
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = [&](Touch *touch, Event *event) {
		log("Touched event received!");
		Vec2 pos = touch->getLocation();
		
		if(pos.y >= VisibleRect::center().y) {
			setNextAnimation();
		} else {
			setNextAction();
		}
		
		return true;
	};
 
//	typedef std::function<bool(Touch*, Event*)> ccTouchBeganCallback;
//	CC_CALLBACK_2(TouchTest::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	//controlLayer->s
}

void AnimeGalleryTest::setAnimationByIndex(int index)
{
	mSelectedAnimation = index;

	setAnimation(animationArray[mSelectedAnimation]);

}

void AnimeGalleryTest::setActionByIndex(int index)
{
	mSelectedAction = index;
	
	setAction(actionArray[mSelectedAction]);
}

void AnimeGalleryTest::setAnimation(const char *name)
{
	mModel->setup(name, (GameModel::Action) mAction);
	mModel->setVisible(true);
}

void AnimeGalleryTest::setAction(int actionValue)
{
	GameModel::Action action = (GameModel::Action) actionValue;
	mAction = (int) action;
	
	//
	mModel->setAction(action);
}

void AnimeGalleryTest::setNextAnimation()
{
	int newIndex = mSelectedAnimation + 1;
	if(newIndex >= numAnimation) {
		newIndex = 0;
	}
	
	setAnimationByIndex(newIndex);
}

void AnimeGalleryTest::setNextAction()
{
	int newIndex = mSelectedAction + 1;

	if(newIndex >= numAction) {
		newIndex = 0;
	}
	
	setActionByIndex(newIndex);

}

void AnimeGalleryTest::setUp()
{
	log("TDD Setup is called");
	
	
	setupBackground();
	
	setupModel();
	
	setupTouchControl();
	
	setActionByIndex(0);
	setAnimationByIndex(0);
	
	setMenuVisible(false);
	
	Director::getInstance()->setDisplayStats(false);
	
	scheduleUpdate();
}



void AnimeGalleryTest::tearDown()
{
	log("TDD tearDown is called");
}

void AnimeGalleryTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AnimeGalleryTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AnimeGalleryTest::defineTests()
{
	ADD_TEST(setNextAction);
	ADD_TEST(setNextAnimation);
}

#pragma mark -
#pragma mark Sub Test Definition
void AnimeGalleryTest::testSample()
{
	log("this is a sample subTest");
}


#endif
