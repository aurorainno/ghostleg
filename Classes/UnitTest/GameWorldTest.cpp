#ifdef ENABLE_TDD
//
//  GameWorldTest.m
//	TDD Framework 
//
//
#include "GameWorldTest.h"
#include "TDDHelper.h"
#include "GameWorld.h"
#include "MovableModel.h"
#include "GameMap.h"
#include "Player.h"
#include "ModelLayer.h"
#include "StageParallaxLayer.h"

void GameWorldTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	
	// Setup background
	mParallaxLayer = StageParallaxLayer::create();
	mParallaxLayer->setShowRoute(true);
	mParallaxLayer->setShowBackground(false);
	mParallaxLayer->setStage(1);
	addChild(mParallaxLayer);
	
	// mParallaxLayer->setMainNode(GameWorld::instance());
	
	GameWorld::instance()->setParallaxLayer(mParallaxLayer);
	

	
	GameWorld *world = GameWorld::instance();
	world->setGameUILayer(nullptr);
	world->resetGame();
	world->setEventCallback(CC_CALLBACK_3(GameWorldTest::handleWorldEvent, this));
	
	GameWorld::instance()->moveCameraToPlayer();
	addChild(world);
	
	mEnableUpdate = false;
	
	log("Map Detail:%s\n", world->getMap()->infoHoriLines().c_str());
	
	scheduleUpdate();
	
	mInitialPlayerPos = GameWorld::instance()->getPlayer()->getPositionY();
	
	GameWorld::instance()->activateChaser();
}

float GameWorldTest::getTravelledDistance()
{
	float playerY = GameWorld::instance()->getPlayer()->getPositionY();
	
	
	return playerY - mInitialPlayerPos;
}

void GameWorldTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

void GameWorldTest::update(float delta)
{
	if(mEnableUpdate == false) {
		return;
	}
	
	if(getTravelledDistance() > 3000) {
		GameWorld::instance()->deActivateChaser();
	}
	
	GameWorld::instance()->getModelLayer()->update(delta);
	GameWorld::instance()->moveCameraToPlayer();
}


#pragma mark -
#pragma mark Call Back
void GameWorldTest::handleWorldEvent(Ref *sender, GameWorldEvent event, int arg)
{
	log("receiveEvent: event=%d arg=%d", event, arg);
}


#pragma mark -
#pragma mark List of Sub Tests

void GameWorldTest::defineTests()
{
	ADD_TEST(testUpdate);
	ADD_TEST(toggleUpdate);
	ADD_TEST(resetGame);
	ADD_TEST(testStart);
	ADD_TEST(testStop);
	ADD_TEST(movePlayer);
	ADD_TEST(cameraUp);
	ADD_TEST(cameraDown);
	ADD_TEST(generateData);
}

#pragma mark -
#pragma mark Sub Test Definition
void GameWorldTest::toggleUpdate()
{
	mEnableUpdate = !mEnableUpdate;
}

void GameWorldTest::testUpdate()
{
	
	GameWorld::instance()->update(0.05f);
}

void GameWorldTest::testStart()
{
	GameWorld::instance()->start();
}

void GameWorldTest::testStop()
{
	GameWorld::instance()->stop();
}

void GameWorldTest::movePlayer()
{
	log("this is a sample subTest");

	GameWorld::instance()->getPlayer()->move(0.5f);
	GameWorld::instance()->moveCameraToPlayer();
}

void GameWorldTest::cameraUp()
{
	log("this is a sample subTest");

	float currentY = GameWorld::instance()->getCameraY();
	GameWorld::instance()->setCameraY(currentY + 30);
	
}

void GameWorldTest::cameraDown()
{
	log("this is a sample subTest");
	
	float currentY = GameWorld::instance()->getCameraY();
	float newY = currentY - 30;
	if(newY < 0) {
		newY = 0;
	}
	GameWorld::instance()->setCameraY(newY);
}

void GameWorldTest::generateData()
{
	GameWorld::instance()->generateNewMapData();
}

void GameWorldTest::resetGame()
{
	GameWorld::instance()->resetGame();
}


#endif
