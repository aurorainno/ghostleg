#ifdef ENABLE_TDD
//
//  NodeSpaceTest.m
//	TDD Framework 
//
//
#include "NodeSpaceTest.h"
#include "TDDHelper.h"

void NodeSpaceTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void NodeSpaceTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void NodeSpaceTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(NodeSpaceTest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void NodeSpaceTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
}


#endif
