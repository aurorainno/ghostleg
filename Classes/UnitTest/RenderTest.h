#ifdef ENABLE_TDD
//
//  RenderTest.h
//
//
#ifndef __TDD_RenderTest__
#define __TDD_RenderTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class RenderTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

private:
	Vec2 getRandomPosition();
	void addEnemy(int enemyID, const Vec2 &pos);
private:
	void testAddStar();
	void testAddEnemy();
	void testRandomEnemy();
	void testConsumeStar();
	void testAddPlayer();
};

#endif

#endif
