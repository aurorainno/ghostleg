#ifdef ENABLE_TDD
//
//  KeyTest.m
//	TDD Framework 
//
//
#include "KeyTest.h"
#include "TDDHelper.h"

void KeyTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
	
	auto keyboardListener = EventListenerKeyboard::create();
	keyboardListener->onKeyPressed = CC_CALLBACK_2(KeyTest::keyPressed, this);
	keyboardListener->onKeyReleased = CC_CALLBACK_2(KeyTest::keyReleased, this);
	
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(keyboardListener, this);
}


void KeyTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

void KeyTest::keyPressed(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	log("KeyPressed: keyCode=%d", keyCode);
}

void KeyTest::keyReleased(cocos2d::EventKeyboard::KeyCode keyCode, cocos2d::Event *event)
{
	log("keyReleased: keyCode=%d", keyCode);
}

#pragma mark -
#pragma mark List of Sub Tests

void KeyTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(KeyTest::subTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void KeyTest::subTest(Ref *sender)
{
	log("this is a sample subTest");
}


#endif
