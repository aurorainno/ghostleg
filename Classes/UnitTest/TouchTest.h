#ifdef ENABLE_TDD
//
//  TouchTest.h
//
//
#ifndef __TDD_TouchTest__
#define __TDD_TouchTest__

// Include Header

#include "TDDTest.h"


// Class Declaration 
class TouchTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
	void addOneByOneListener();
	
	bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
	void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
	void onTouchMoved(cocos2d::Touch*, cocos2d::Event*);
	void onTouchCancelled(cocos2d::Touch*, cocos2d::Event*);
	
private:
	void testOneByOne(Ref *sender);
	void testDoubleTap(Ref *sender);
}; 

#endif

#endif
