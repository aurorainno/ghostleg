#ifdef ENABLE_TDD
//
//  RenderItemTest.m
//	TDD Framework
//
//
#include "RenderItemTest.h"
#include "TDDHelper.h"

#include "GameWorld.h"
#include "MovableModel.h"
#include "GameMap.h"
#include "Player.h"
#include "ModelLayer.h"
#include "StageParallaxLayer.h"
#include "Star.h"
#include "MagnetCapsule.h"
#include "Cashout.h"
#include "ItemPuzzle.h"
#include "SpeedupCapsule.h"

void RenderItemTest::setUp()
{
	log("TDD Setup is called");
	
	
	mParallaxLayer = StageParallaxLayer::create();
	mParallaxLayer->setShowRoute(true);
	mParallaxLayer->setShowBackground(false);
	mParallaxLayer->setStage(1);
	addChild(mParallaxLayer);
	
	// mParallaxLayer->setMainNode(GameWorld::instance());
	
	GameWorld::instance()->setParallaxLayer(mParallaxLayer);
	
	
	
	GameWorld *world = GameWorld::instance();
	world->setGameUILayer(nullptr);
	world->resetGame();
	//world->setEventCallback(CC_CALLBACK_3(GameWorldTest::handleWorldEvent, this));
	
	GameWorld::instance()->moveCameraToPlayer();
	addChild(world);
}


void RenderItemTest::addMagnet(const Vec2 &pos)
{
	MagnetCapsule *item = MagnetCapsule::create();
	item->setPosition(pos);
	GameWorld::instance()->getModelLayer()->addItem(item);
	mMagnetList.pushBack(item);
}

void RenderItemTest::consumeAllMagnet()
{
	for(Node *node : mMagnetList) {
		MagnetCapsule *item = dynamic_cast<MagnetCapsule *>(node);
		if(item == nullptr) {
			continue;
		}
		
		item->use();
	}
	
	mMagnetList.clear();
}


void RenderItemTest::addCashout(const Vec2 &pos)
{
	Cashout *item = Cashout::create();
	item->setPosition(pos);
	GameWorld::instance()->getModelLayer()->addItem(item);
	mCashoutList.pushBack(item);
}

void RenderItemTest::consumeAllItem(Vector<Node *> &itemList)
{
	for(Node *node : itemList) {
		Item *item = dynamic_cast<Item *>(node);
		if(item == nullptr) {
			continue;
		}
		
		item->use();
	}
	
	itemList.clear();
}

void RenderItemTest::consumeAllCashout()
{
	for(Node *node : mCashoutList) {
		Cashout *item = dynamic_cast<Cashout *>(node);
		if(item == nullptr) {
			continue;
		}
		
		item->use();
	}
	
	mCashoutList.clear();
}


void RenderItemTest::addCoin(const Vec2 &pos)
{
	Star *star = Star::create(Star::SmallStar);
	star->setPosition(pos);
	GameWorld::instance()->getModelLayer()->addItem(star);
	mCoinList.pushBack(star);
}

void RenderItemTest::consumeAllCoin()
{
	
	for(Node *node : mCoinList) {
		Star *star = dynamic_cast<Star *>(node);
		if(star == nullptr) {
			continue;
		}
		
		star->use();
	}
	
	mCoinList.clear();
	
}

void RenderItemTest::tearDown()
{
	log("TDD tearDown is called");
}

void RenderItemTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void RenderItemTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void RenderItemTest::defineTests()
{
	ADD_TEST(clearAll);
	ADD_TEST(addSpeedUp);
	ADD_TEST(consumeAllSpeedUp);
	ADD_TEST(testAddCoin);
	ADD_TEST(testConsumeAllCoin);
	ADD_TEST(testAddMagnet);
	ADD_TEST(testConsumeAllMagnet);
	ADD_TEST(testAddCashout);
	ADD_TEST(testConsumeAllCashout);
	ADD_TEST(addPuzzle);
	ADD_TEST(consumeAllPuzzle);
}

#pragma mark -
#pragma mark Sub Test Definition
void RenderItemTest::testAddCoin()
{
	float x = RandomHelper::random_real(50.0, 300.0);
	float y = RandomHelper::random_real(200.0, 400.0);
	
	addCoin(Vec2(x, y));
}

void RenderItemTest::testConsumeAllCoin()
{
	consumeAllCoin();
}

void RenderItemTest::testAddMagnet()
{
	float x = RandomHelper::random_real(50.0, 300.0);
	float y = RandomHelper::random_real(200.0, 400.0);
	
	addMagnet(Vec2(x, y));
}

void RenderItemTest::testConsumeAllMagnet()
{
	consumeAllMagnet();
}

void RenderItemTest::testAddCashout()
{
	float x = RandomHelper::random_real(50.0, 300.0);
	float y = RandomHelper::random_real(200.0, 400.0);
	
	addCashout(Vec2(x, y));
}

void RenderItemTest::testConsumeAllCashout()
{
	consumeAllCashout();
}

void RenderItemTest::addPuzzle()
{
	float x = RandomHelper::random_real(50.0, 300.0);
	float y = RandomHelper::random_real(200.0, 400.0);
	Vec2 pos = Vec2(x, y);
	ItemPuzzle *item = ItemPuzzle::create();
	
	item->setPosition(pos);
	GameWorld::instance()->getModelLayer()->addItem(item);
	mPuzzleList.pushBack(item);
}

void RenderItemTest::consumeAllPuzzle()
{
	consumeAllItem(mPuzzleList);
}

void RenderItemTest::addSpeedUp()
{
	float x = RandomHelper::random_real(50.0, 300.0);
	float y = RandomHelper::random_real(200.0, 400.0);
	Vec2 pos = Vec2(x, y);
	SpeedUpCapsule *item = SpeedUpCapsule::create();
	
	item->setPosition(pos);
	GameWorld::instance()->getModelLayer()->addItem(item);
	mSpeedupList.pushBack(item);
}

void RenderItemTest::consumeAllSpeedUp()
{
	consumeAllItem(mSpeedupList);
}

void RenderItemTest::clearAll()
{
	GameWorld::instance()->getModelLayer()->removeAllItems();
	mCoinList.clear();
	mMagnetList.clear();
	mCashoutList.clear();
	mPuzzleList.clear();
	mSpeedupList.clear();
}


#endif
