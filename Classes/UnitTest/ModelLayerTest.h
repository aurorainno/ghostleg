#ifdef ENABLE_TDD
//
//  ModelLayerTest.h
//
//
#ifndef __TDD_ModelLayerTest__
#define __TDD_ModelLayerTest__

// Include Header

#include "TDDTest.h"

class ModelLayer;

// Class Declaration 
class ModelLayerTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void testAddItem(Ref *sender);
	void testMoveUp(Ref *sender);
	void testMoveDown(Ref *sender);
	void step(Ref *sender);
	void testShowAlertNearPlayer(Ref *sender);
	void testGetNearestEnemy(Ref *sender);
	void testMissile(Ref *sender);
	void testMovingStar(Ref *sender);
	void testVisibleStar(Ref *sender);
	
private:
	ModelLayer *mModelLayer;
	float mCameraY;
}; 

#endif

#endif
