#ifdef ENABLE_TDD
//
//  DeliveryCompleteNoticeViewTest.m
//	TDD Framework
//
//
#include "DeliveryCompleteNoticeViewTest.h"
#include "NPCOrder.h"
#include "TDDHelper.h"

void DeliveryCompleteNoticeViewTest::setUp()
{
	log("TDD Setup is called");
}


void DeliveryCompleteNoticeViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void DeliveryCompleteNoticeViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void DeliveryCompleteNoticeViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void DeliveryCompleteNoticeViewTest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(testSetView);
    ADD_TEST(testShowView);
    ADD_TEST(testHideView);
    ADD_TEST(testShowReward);
}

#pragma mark -
#pragma mark Sub Test Definition
void DeliveryCompleteNoticeViewTest::testSample()
{
	log("this is a sample subTest");
    DeliveryCompleteNoticeView* view = DeliveryCompleteNoticeView::create();
    float posY = Director::getInstance()->getOpenGLView()->getVisibleSize().height;
    view->setPosition(Vec2(0,posY));
    addChild(view);
    mView = view;
}

void DeliveryCompleteNoticeViewTest::testSetView()
{
    if(mView){
        NPCOrder* order = NPCOrder::create();
        order->setNpcID(2);
        order->setReward(200);
        mView->setReward(order);
        mView->setBonusTip(20);
        mView->setOnShowRewardCallback([=](){
            this->testHideView();
        });
    }
}

void DeliveryCompleteNoticeViewTest::testShowView()
{
    if(mView){
        mView->showView();
    }
}

void DeliveryCompleteNoticeViewTest::testHideView()
{
    if(mView){
        mView->closeView();
    }
}

void DeliveryCompleteNoticeViewTest::testShowReward()
{
    if(mView){
        mView->showReward();
    }
}


#endif
