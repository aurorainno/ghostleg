#ifdef ENABLE_TDD
//
//  TimeHelperTest.h
//
//
#ifndef __TDD_TimeHelperTest__
#define __TDD_TimeHelperTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class TimeHelperTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
	void testRemainTimeString();
	void testTimeDiff();
};

#endif

#endif
