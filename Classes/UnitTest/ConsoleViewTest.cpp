#ifdef ENABLE_TDD
//
//  ConsoleViewTest.m
//	TDD Framework
//
//
#include "ConsoleViewTest.h"
#include "TDDHelper.h"
#include "ConsoleView.h"

void ConsoleViewTest::setUp()
{
	log("TDD Setup is called");
}


void ConsoleViewTest::tearDown()
{
	log("TDD tearDown is called");
}

void ConsoleViewTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void ConsoleViewTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void ConsoleViewTest::defineTests()
{
	ADD_TEST(testSample);
	ADD_TEST(testAppend);
}

#pragma mark -
#pragma mark Sub Test Definition
void ConsoleViewTest::testSample()
{
	log("this is a sample subTest");
	
	ConsoleView *consoleView = ConsoleView::create();
	

	addChild(consoleView);
	
	consoleView->append("Testing!!!");
	
	mConsoleView = consoleView;
}

void ConsoleViewTest::testAppend()
{
	std::string content = "";
	
	for(int i=0; i<100; i++) {
		content += StringUtils::format("Line %d\n", i);
	}
	
	mConsoleView->append(content);
}

#endif
