#ifdef ENABLE_TDD
//
//  DeliveryCompleteNoticeViewTest.h
//
//
#ifndef __TDD_DeliveryCompleteNoticeViewTest__
#define __TDD_DeliveryCompleteNoticeViewTest__

// Include Header

#include "TDDBaseTest.h"
#include "DeliveryCompleteNoticeView.h"

// Class Declaration
class DeliveryCompleteNoticeViewTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testSetView();
    void testShowView();
    void testHideView();
    void testShowReward();
    
private:
    DeliveryCompleteNoticeView* mView;
    
};

#endif

#endif
