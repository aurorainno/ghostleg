#ifdef ENABLE_TDD
//
//  AwardStarLayerTest.h
//
//
#ifndef __TDD_AwardStarLayerTest__
#define __TDD_AwardStarLayerTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class AwardStarLayerTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testSample();
    void testAd();
};

#endif

#endif
