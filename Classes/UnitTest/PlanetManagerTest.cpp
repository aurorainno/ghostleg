#ifdef ENABLE_TDD
//
//  PlanetManagerTest.m
//	TDD Framework
//
//
#include "PlanetManagerTest.h"
#include "TDDHelper.h"
#include "PlanetManager.h"
#include "StringHelper.h"
#include "UnlockPlanetLayer.h"
#include "VisibleRect.h"

void PlanetManagerTest::setUp()
{
	log("TDD Setup is called");
	
	// Adding a background
	Sprite *sprite = Sprite::create("background/lowres/BGMain.png");
	sprite->setPosition(VisibleRect::center());
	addChild(sprite);


	//
	std::string msg = StringUtils::format("Selected Planet: %d", PlanetManager::instance()->getSelectedPlanet());
	
	ui::Text *text = ui::Text::create();
	text->setString(msg);
	
	text->setPosition(Vec2(50, 300));
	
	addChild(text);
	mPlanetText = text;
	
	
	
	log("GameData:\n%s", PlanetManager::instance()->infoGameData().c_str());
}


void PlanetManagerTest::tearDown()
{
	log("TDD tearDown is called");
}

void PlanetManagerTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PlanetManagerTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PlanetManagerTest::defineTests()
{
	ADD_TEST(testGameInfo);
	ADD_TEST(testIsTypeMatched);
	ADD_TEST(testDogTeamList);
	ADD_TEST(testUnlockPlanetLayer);
	ADD_TEST(testParseTypeStr);
	ADD_TEST(testPlanetData);
	ADD_TEST(testChangePlanet);
	ADD_TEST(testLock);
	ADD_TEST(testUnLock);
	ADD_TEST(testUnLockAll);
}

#pragma mark -
#pragma mark Sub Test Definition
void PlanetManagerTest::testGameInfo()
{
	log("GameData:\n%s\n", PlanetManager::instance()->infoGameData().c_str());
	
}

void PlanetManagerTest::testPlanetData()
{
	log("this is a sample subTest");
	
	log("Planets:\n%s\n", PlanetManager::instance()->infoPlanetData().c_str());
	
	
	std::vector<int> planetArray = PlanetManager::instance()->getPlanetList();
	
	log("planetArray=%s", VECTOR_TO_STR(planetArray).c_str());
}


void PlanetManagerTest::testGameData()
{

}


void PlanetManagerTest::testChangePlanet()
{
	int currentPlanet = PlanetManager::instance()->getSelectedPlanet();
	
	int newValue = currentPlanet + 1;
	if(newValue > 3) {
		newValue = 1;
	}
	
	PlanetManager::instance()->selectPlanet(newValue);
	
	std::string msg = StringUtils::format("Planet: %d", PlanetManager::instance()->getSelectedPlanet());
	mPlanetText->setString(msg);
}

void PlanetManagerTest::testLock()
{
	int selected = PlanetManager::instance()->getSelectedPlanet();
	PlanetManager::instance()->setPlanetLockStatus(selected, false);
	
	log("%s", PlanetManager::instance()->infoGameData().c_str());
}

void PlanetManagerTest::testUnLock()
{
	int selected = PlanetManager::instance()->getSelectedPlanet();
	PlanetManager::instance()->unlockPlanet(selected);
	
	log("%s", PlanetManager::instance()->infoGameData().c_str());
}

void PlanetManagerTest::testUnLockAll()
{
	std::vector<int> planetList = PlanetManager::instance()->getPlanetList();
	for(int planetID : planetList) {
		PlanetManager::instance()->unlockPlanet(planetID);
	}
}

void PlanetManagerTest::testParseTypeStr()
{
	std::vector<std::string> testList;
	testList.push_back("normal");
	testList.push_back("event");
	testList.push_back("dailyReward");
	
	for(std::string typeStr : testList) {
		PlanetData::PlanetType type = PlanetData::parseType(typeStr);
		
		log("typeStr=[%s] typeValue=%d", typeStr.c_str(), type);
	}
}

void PlanetManagerTest::testIsTypeMatched()
{
	const int nData = 4 * 2;
	
	int testData[nData] = {
		// Type to check, Set of type
		PlanetData::Normal		, (PlanetData::Normal | PlanetData::Event),
		PlanetData::DailyReward	, (PlanetData::Normal | PlanetData::DailyReward),
		PlanetData::Default		, (PlanetData::Normal | PlanetData::Event | PlanetData::DailyReward),
		PlanetData::Normal		, (PlanetData::Normal | PlanetData::Event | PlanetData::DailyReward),
	};
	
	for(int i=0; i<nData; i+=2) {
		PlanetData::PlanetType typeToCheck = (PlanetData::PlanetType) testData[i];
		PlanetData::PlanetType typeSet = (PlanetData::PlanetType) testData[i+1];
		
		
		bool result = PlanetData::isTypeMatched(typeToCheck, typeSet);
		
		log("result=%d typeToCheck=%d typeSet=%d", result, typeToCheck, typeSet);
	}
}

void PlanetManagerTest::testDogTeamList()
{
	//getDogTeamList
	std::vector<int> dogList = PlanetManager::instance()->getDogTeamList();
	
	for(int team : dogList) {
		log("%d", team);
	}
}

void PlanetManagerTest::testUnlockPlanetLayer()
{
	clearNodes();
	
	
	
	std::vector<int> planetList;
	planetList.push_back(2);
//	planetList.push_back(3);
	
	UnlockPlanetLayer *layer = UnlockPlanetLayer::create();
	layer->setPlanetList(planetList);
	//layer->setParentLayer(this);
	addChild(layer);
}

#endif
