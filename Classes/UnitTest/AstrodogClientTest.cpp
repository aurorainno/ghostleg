#ifdef ENABLE_TDD
//
//  AstrodogClientTest.m
//	TDD Framework
//
//
#include "AstrodogClientTest.h"
#include "URLHelper.h"
#include "TDDHelper.h"
#include "AstrodogClient.h"

void AstrodogClientTest::setUp()
{
	log("TDD Setup is called");
	
	AstrodogClient::instance()->setListener(this);
	
	std::vector<int> planets;
	planets.push_back(1);
	planets.push_back(2);
	
	AstrodogClient::instance()->setupPlanetList(planets);
}


void AstrodogClientTest::tearDown()
{
	log("TDD tearDown is called");
}

void AstrodogClientTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void AstrodogClientTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void AstrodogClientTest::defineTests()
{
	ADD_TEST(testUrlEncode);
	ADD_TEST(testUserInfo);
	ADD_TEST(testRemainTime);
	ADD_TEST(testGetRankPosition);
	ADD_TEST(testInfoLeaderboard);
	ADD_TEST(testLoadAllLeaderboard);
	ADD_TEST(testLBSaveLoad);
	ADD_TEST(testLeaderboard);
	ADD_TEST(testAstrodogRecord);
	ADD_TEST(testGetRanking);
	ADD_TEST(testResetUser);
    ADD_TEST(testSubmitScore);
	ADD_TEST(testSubmitScoreNewUser);
	ADD_TEST(testSubmitFBOldUser);
	ADD_TEST(testSubmitFBNewUser);
    ADD_TEST(testConnectFB);
    ADD_TEST(testURLHelper);
}



#pragma mark -
#pragma mark Sub Test Definition

// Testing of GetRanking

void AstrodogClientTest::testGetRankPosition()
{
	int planet = 1;
	AstrodogLeaderboardType type = AstrodogLeaderboardCountry;
	
	int position = AstrodogClient::instance()->getRankPosition(type, planet);
	
	log("Rank at %d planet=%d position=%d", type, planet, position);
}

void AstrodogClientTest::testGetRanking()
{
	int planet = 1;
	AstrodogClient::instance()->getRanking(AstrodogLeaderboardGlobal, planet);
}


void AstrodogClientTest::onGetRanking(int status, AstrodogLeaderboardType type, int planet,
									  const std::map<std::string, int> &metaData)
{
	log("onGetRanking: status=%d type=%d planet=%d", status, type, planet);
	
	log("%s", AstrodogClient::instance()->toString().c_str());
	
	AstrodogLeaderboard *leaderboard = AstrodogClient::instance()->getLeaderboard(type, planet);
	
	log("Leaderboard Updated:\n%s\n", (leaderboard == nullptr ? "null" : leaderboard->toString().c_str()));

	// Meta data:
	std::map<std::string, int> meta = metaData;
	for (std::map<std::string, int>::iterator it=meta.begin(); it!=meta.end(); ++it) {
		std::string key = it->first;
		int value = it->second;
		
		log("Meta: %s=%d", key.c_str(), value);
	}

}

#pragma mark - Testing Leaderboard Record
void AstrodogClientTest::testInfoLeaderboard()
{
	AstrodogClient::instance()->loadAllLeaderboard();
	
	log("%s", AstrodogClient::instance()->infoLeaderboard().c_str());
}


void AstrodogClientTest::testLoadAllLeaderboard()
{
	AstrodogClient::instance()->loadAllLeaderboard();

	log("%s", AstrodogClient::instance()->infoLeaderboard().c_str());
}

void AstrodogClientTest::testLBSaveLoad()
{
	AstrodogLeaderboardType boardType = AstrodogLeaderboardGlobal;
	int planetID = 1;
	
	AstrodogRecord *record = AstrodogRecord::create();
	record->setScore(100);
	record->setRank(1);
	record->setUid(123456);
	record->setCountry("HK");
	record->setFbID("xxxyyyzzz");
	record->setName("I am Good");

	AstrodogLeaderboard *leaderboard = AstrodogLeaderboard::create();
	leaderboard->addRecord(record);
	leaderboard->setPlanet(planetID);
	leaderboard->setType(AstrodogLeaderboardGlobal);
	leaderboard->setWeek("201705");

	
	AstrodogClient::instance()->setLeaderboard(leaderboard);
	AstrodogClient::instance()->saveLeaderboard(leaderboard);
	
	AstrodogClient::instance()->loadLeaderboard(boardType, planetID);
	
	log("%s", AstrodogClient::instance()->infoLeaderboard().c_str());
	
}

void AstrodogClientTest::testLeaderboard()
{
	AstrodogRecord *record = AstrodogRecord::create();
	record->setScore(100);
	record->setRank(1);
	record->setUid(123456);
	record->setCountry("HK");
	record->setFbID("xxxyyyzzz");
	record->setName("I am Good");

	AstrodogLeaderboard *leaderboard = AstrodogLeaderboard::create();
	leaderboard->addRecord(record);
	leaderboard->setType(AstrodogLeaderboardGlobal);
	leaderboard->setWeek("201705");
	
	std::string json = leaderboard->toJSONContent();
	log("json\n%s\n", json.c_str());
	
	
	AstrodogLeaderboard *newLB = AstrodogLeaderboard::create();
	newLB->parseJSONContent(json);
	
	log("Leaderboard:\n%s\n", leaderboard->toString().c_str());

}

void AstrodogClientTest::testAstrodogRecord()
{
	AstrodogRecord *record = AstrodogRecord::create();
	record->setScore(100);
	record->setRank(1);
	record->setUid(123456);
	record->setCountry("HK");
	record->setFbID("xxxyyyzzz");
	record->setName("I am Good");
	
	std::string json = record->toJSONContent();
	log("json\n%s\n", json.c_str());
	
	AstrodogRecord *newRecord = AstrodogRecord::create();
	newRecord->parseJSONContent(json);
	
	log("json:%s", newRecord->toString().c_str());
}

#pragma mark - Testing FB

void AstrodogClientTest::testConnectFB()
{
	AstrodogClient::instance()->connectFB();
	//
}

void AstrodogClientTest::testSubmitFBOldUser()
{
	AstrodogUser *user = AstrodogUser::create();
	user->setUid(1234);
	user->setPlayerID("test_bobby");
	user->setName("Ken");
	user->setCountry("HK");
	AstrodogClient::instance()->setUser(user);
	
	AstrodogClient::instance()->submitFBInfo("kkk1", "fb_111222");
}

void AstrodogClientTest::testSubmitFBNewUser()
{
	AstrodogClient::instance()->setUser(nullptr);

	AstrodogClient::instance()->submitFBInfo("kkk1", "11223344");
}

void AstrodogClientTest::onFBConnect(int status)
{
	log("onFBConnect: status=%d", status);
}

#pragma mark - Submit Score

void AstrodogClientTest::onSubmitScore(int status)
{
	log("onSubmitScore: status=%d", status);
	log("Astrodog Status:\n%s", AstrodogClient::instance()->toString().c_str());
}


void AstrodogClientTest::testSubmitScore()
{
	AstrodogUser *user = AstrodogUser::create();
	user->setUid(1234);
	user->setPlayerID("fb_10001");
	user->setName("Ken");
	user->setCountry("HK");
	AstrodogClient::instance()->setUser(user);
	
	int planet = 1;
	AstrodogClient::instance()->submitScore(planet, 1603);
}

void AstrodogClientTest::testSubmitScoreNewUser()
{
	int planet = 1;
	
	AstrodogClient::instance()->setUser(nullptr);
	AstrodogClient::instance()->submitScore(planet, 7749);
}

#pragma mark - Other

void AstrodogClientTest::testUrlEncode()
{
	std::string name = "陳一心";
	char *encodeName = URLHelper::url_encode(name.c_str());
	log("name=[%s] encode=%s", name.c_str(), encodeName);
}

void AstrodogClientTest::testURLHelper()
{
    std::string url = "http://127.0.0.1:3000/connectFB?uid=27&fbid=testing&name=calvin K&country=HK";
    char* urlCharPtr = (char*) url.c_str();
    char* encodedURL = URLHelper::url_encode(urlCharPtr);
    log("encodedeURL:%s",encodedURL);
    char* decodedURL = URLHelper::url_decode(encodedURL);
    log("decodedURL:%s",decodedURL);
}

void AstrodogClientTest::testResetUser()
{
	AstrodogClient::instance()->resetUser();
}

void AstrodogClientTest::testUserInfo()
{
	AstrodogUser *user = AstrodogClient::instance()->getUser();
	log("userInfo=%s", user == nullptr ? "None" : user->toString().c_str());
}

void AstrodogClientTest::testRemainTime()
{
	long long endTime = 1497110400000;
	
	
}

#endif
