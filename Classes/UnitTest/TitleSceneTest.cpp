#ifdef ENABLE_TDD
//
//  TitleSceneTest.m
//	TDD Framework
//
//
#include "TitleSceneTest.h"
#include "TDDHelper.h"
#include "TitleScene.h"

void TitleSceneTest::setUp()
{
	log("TDD Setup is called");
}


void TitleSceneTest::tearDown()
{
	log("TDD tearDown is called");
}

void TitleSceneTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void TitleSceneTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void TitleSceneTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void TitleSceneTest::testSample()
{
	log("this is a sample subTest");
	TitleSceneLayer *layer = TitleSceneLayer::create();
	
	addChild(layer);
}


#endif
