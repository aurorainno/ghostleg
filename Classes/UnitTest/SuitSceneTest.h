#ifdef ENABLE_TDD
//
//  SuitSceneTest.h
//
//
#ifndef __TDD_SuitSceneTest__
#define __TDD_SuitSceneTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class SuitSceneTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	void subTest(Ref *sender);
}; 

#endif

#endif
