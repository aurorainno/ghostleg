#ifdef ENABLE_TDD
//
//  DebugMapTest.h
//
//
#ifndef __TDD_DebugMapTest__
#define __TDD_DebugMapTest__

// Include Header

#include "TDDTest.h"

// Class Declaration 
class DebugMapTest : public TDDTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void setSubTest(Vector<MenuItem *> &menuArray);
	
private:
	virtual void onEnter();
	void changeDebugMap(Ref *sender);
	void resetDebugMap(Ref *sender);
	void subTest(Ref *sender);
}; 

#endif

#endif
