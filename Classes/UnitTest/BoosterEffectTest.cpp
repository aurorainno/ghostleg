#ifdef ENABLE_TDD
//
//  BoosterEffectTest.m
//	TDD Framework
//
//
#include "BoosterEffectTest.h"
#include "TDDHelper.h"
#include "Player.h"
#include "VisibleRect.h"
#include "SpeedUpEffect.h"
#include "GameWorld.h"

void BoosterEffectTest::setUp()
{
	log("TDD Setup is called");
	
	addChild(GameWorld::instance());
	
	
	Player *player = GameWorld::instance()->getPlayer();
	player->setPosition(VisibleRect::center());
	
	mPlayer = player;
	mEffect = nullptr;
}


void BoosterEffectTest::tearDown()
{
	log("TDD tearDown is called");
	CC_SAFE_FREE(mEffect);
}

void BoosterEffectTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void BoosterEffectTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}



#pragma mark -
#pragma mark List of Sub Tests

void BoosterEffectTest::defineTests()
{
	ADD_TEST(testSpeedUp);
	ADD_TEST(testDifferentAngle);
}

#pragma mark -
#pragma mark Sub Test Definition
void BoosterEffectTest::testSpeedUp()
{
	log("this is a sample subTest");
	SpeedUpEffect *effect = SpeedUpEffect::create();
	effect->setIsPowerUp(true);
	//effect->activate();
	
	mPlayer->setupItemEffect(effect);
	
	mEffect = effect;
	
	CC_SAFE_RETAIN(mEffect);
}

void BoosterEffectTest::testDifferentAngle()
{
	Vec2 pos = Vec2(100, 50);
	
	for(float rotation=0; rotation <= 360; rotation += 45) {
		Node *csbNode = CSLoader::createNode("SpeedUp.csb");
		csbNode->setRotation(rotation);
		csbNode->setPosition(pos);
		csbNode->setScale(0.3f);
		
		addChild(csbNode);
		
		pos.y += 50;
		
		
	}
	

	
}

#endif
