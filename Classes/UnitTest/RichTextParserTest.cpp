#ifdef ENABLE_TDD
//
//  RichTextParserTest.m
//	TDD Framework
//
//
#include "RichTextParserTest.h"
#include "TDDHelper.h"
#include "RichTextParser.h"


void RichTextParserTest::setUp()
{
	log("TDD Setup is called");
}


void RichTextParserTest::tearDown()
{
	log("TDD tearDown is called");
}

void RichTextParserTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void RichTextParserTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void RichTextParserTest::defineTests()
{
	ADD_TEST(testSample);
}

#pragma mark -
#pragma mark Sub Test Definition
void RichTextParserTest::testSample()
{
	log("this is a sample subTest");
	
	std::string str = "Hello this is <c1>color</c1> <c2>highlight</c2> demo.\n"
						"This is the <c2>2nd</c2> line.";
	
	RichTextParser parser;
	
	parser.parse(str);
	
	std::vector<RichTextToken> tokenList = parser.getTokenList();
	
	for(RichTextToken token : tokenList) {
		log("%s", RichTextParser::getTokenString(token).c_str());
	}
}


#endif
