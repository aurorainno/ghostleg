#ifdef ENABLE_TDD
//
//  ItemEffectTest.h
//
//
#ifndef __TDD_ItemEffectTest__
#define __TDD_ItemEffectTest__

// Include Header

#include "TDDBaseTest.h"
#include "ItemEffect.h"
#include "Player.h"
#include "Enemy.h"

// Class Declaration 
class ItemEffectTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
	
	CC_SYNTHESIZE_RETAIN(Player *, mPlayer, Player);
	
	CC_SYNTHESIZE(ItemEffect *, mItemEffect, ItemEffect);
	CC_SYNTHESIZE(Enemy *, mEnemy, Enemy);
	
public:
	void resetPlayer();
	void step();
	void activateBooster();
	void activateCapsule();
	void hitPlayer();
	void testMagnetPowerUp();
	void testInvulnerable();
	void testShield();
	void testGrabStar();
	void testTimeStop();
	void testMissile();
	void testStarMagnet();
	void setWorldStatus();
    void testItemEffectUI();
}; 

#endif

#endif
