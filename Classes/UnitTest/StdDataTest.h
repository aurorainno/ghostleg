#ifdef ENABLE_TDD
//
//  StdDataTest.h
//
//
#ifndef __TDD_StdDataTest__
#define __TDD_StdDataTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration 
class StdDataTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testSwap();
	void testStdMap();
	void testVectorCopy();
	void testEraseVector();
	void testEraseCocosVector();
	void testEraseVectorBug();
}; 

#endif

#endif
