#ifdef ENABLE_TDD
//
//  SnowBallTest.m
//	TDD Framework
//
//
#include "SnowBallTest.h"
#include "TDDHelper.h"
#include "GameMap.h"
#include "SnowBall.h"
#include "GameWorld.h"
#include "Player.h"

void SnowBallTest::setUp()
{
	log("TDD Setup is called");
	
	mBallList.clear();
	
	// Testing Map
	GameWorld *world = GameWorld::instance();
	addChild(world);
	
	
	mGameMap = world->getMap();
	setupGameMap();
	
	setupSnowBall();
}

void SnowBallTest::addSnowBall(SnowBall *ball)
{
	mBallList.pushBack(ball);
}

void SnowBallTest::setupSnowBall()
{
	Vec2 initPos = Vec2(mLineX[2], 100);
	SnowBall *ball = SnowBall::create();
	ball->setPosition(initPos);
	addChild(ball);
	
	ball->setupMoveLogic(DirUp, nullptr);
	
	addSnowBall(ball);
}

void SnowBallTest::fire()
{
	SnowBall *ball = SnowBall::create();
	addChild(ball);
	
	ball->setupMoveLogic();
	
	addSnowBall(ball);
}

void SnowBallTest::setupGameMap()
{
	//mGameMap->s
	mLineX[0]= mGameMap->getVertLineX(0);
	mLineX[1] = mGameMap->getVertLineX(1);
	mLineX[2] = mGameMap->getVertLineX(2);
	
	
	mGameMap->addSlopeLine(Vec2(mLineX[0], 250), Vec2(mLineX[1], 200));
	mGameMap->addSlopeLine(Vec2(mLineX[0], 320), Vec2(mLineX[1], 320));
	mGameMap->addSlopeLine(Vec2(mLineX[0], 330), Vec2(mLineX[1], 350));
	mGameMap->addSlopeLine(Vec2(mLineX[2], 200), Vec2(mLineX[1], 100));	// case the point1 > point2
	
	log("Map Detail:\n%s\n", mGameMap->infoHoriLines().c_str());
}



void SnowBallTest::tearDown()
{
	log("TDD tearDown is called");
}

void SnowBallTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void SnowBallTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void SnowBallTest::defineTests()
{
	ADD_TEST(testTwoScale);
	ADD_TEST(step);
	ADD_TEST(fire);
}

#pragma mark -
#pragma mark Sub Test Definition
void SnowBallTest::testSample()
{
	log("this is a sample subTest");
}

void SnowBallTest::testTwoScale()
{
	SnowBall *ball;
	
	ball = SnowBall::create();
	ball->setScale(0.5f);
	ball->setPosition(Vec2(mLineX[2], 100));
	addChild(ball);
	ball->setupMoveLogic(DirUp, nullptr);
	
	addSnowBall(ball);
	
	
	ball = SnowBall::create();
	ball->setScale(0.9f);
	ball->setPosition(Vec2(mLineX[1], 100));
	addChild(ball);
	ball->setupMoveLogic(DirUp, nullptr);
	
	addSnowBall(ball);
	
	
}


void SnowBallTest::step()
{
	float delta = 0.1f;
	for(SnowBall *ball : mBallList) {
		ball->update(delta);
	}
	
	GameWorld::instance()->getPlayer()->update(delta);
}

#endif
