#ifdef ENABLE_TDD
//
//  MasteryTest.h
//
//
#ifndef __TDD_MasteryTest__
#define __TDD_MasteryTest__

// Include Header

#include "TDDBaseTest.h"

class MasteryManager;

// Class Declaration 
class MasteryTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	
private:
	void testOneData();
	void testDataArray();
	void parsePlayerMastery();
	void testGetSetPlayerMastery();
	void testSaveLoadMastery();
	void testListMastery();
	void testSetPlayerAttribute();
	void testDescription();
private:
	MasteryManager *mManager;
}; 

#endif

#endif
