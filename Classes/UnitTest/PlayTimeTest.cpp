#ifdef ENABLE_TDD
//
//  PlayTimeTest.m
//	TDD Framework
//
//
#include "PlayTimeTest.h"
#include "TDDHelper.h"
#include "StringHelper.h"
#include "GameManager.h"
#include "PlayerRecord.h"
#include <ctime>

void PlayTimeTest::setUp()
{
	log("TDD Setup is called");
}


void PlayTimeTest::tearDown()
{
	log("TDD tearDown is called");
}

void PlayTimeTest::willRunTest(const std::string &name)
{
	log("Before run %s", name.c_str());
}

void PlayTimeTest::didRunTest(const std::string &name)
{
	log("After run %s", name.c_str());
}


#pragma mark -
#pragma mark List of Sub Tests

void PlayTimeTest::defineTests()
{
	ADD_TEST(testSample);
    ADD_TEST(updatePlayTimeRecord);
}

#pragma mark -
#pragma mark Sub Test Definition
void PlayTimeTest::testSample()
{
	log("this is a sample subTest");
    time_t timer;
    struct tm y2k = {0};
    struct tm virtualTime = {0};
    double seconds;
    int days;
    
    y2k.tm_hour = 0;   y2k.tm_min = 0; y2k.tm_sec = 0;
    y2k.tm_year = 116; y2k.tm_mon = 9; y2k.tm_mday = 5;
    
    virtualTime.tm_hour = 23;   virtualTime.tm_min = 59; virtualTime.tm_sec = 59;
    virtualTime.tm_year = 116; virtualTime.tm_mon = 9; virtualTime.tm_mday = 12;
    
    time(&timer);  /* get current time; same as: timer = time(NULL)  */
    
    seconds = difftime(timer,mktime(&y2k));
   // seconds = difftime(mktime(&virtualTime),mktime(&y2k));
    days = std::floor(seconds/86400);

    printf ("%d days since Oct 5, 2016 in the current timezone\n", days);
}

void PlayTimeTest::updatePlayTimeRecord()
{
    PlayerRecord* record = GameManager::instance()->getPlayerRecord();
    record->updatePlayTimeRecord();
    
}

#endif
