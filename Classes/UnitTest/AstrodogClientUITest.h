#ifdef ENABLE_TDD
//
//  AstrodogClientUITest.h
//
//
#ifndef __TDD_AstrodogClientUITest__
#define __TDD_AstrodogClientUITest__

// Include Header
#include <vector>

#include "TDDBaseTest.h"
#include "AstrodogData.h"
#include "AstrodogClient.h"

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class ConsoleView;

// Class Declaration
class AstrodogClientUITest : public TDDBaseTest, AstrodogClientListener
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test

	
#pragma mark - AstrodogClientListener
	virtual void onGetRanking(int status, AstrodogLeaderboardType type, int planet,
							  const std::map<std::string, int> &metaData);
	virtual void onSubmitScore(int status);
	virtual void onFBConnect(int status);
	
#pragma mark - ConsoleView
// ConsoleView
private:		// method
	void setContent(const std::string &content);
	void addContent(const std::string &content);
	
private:		// data
	ConsoleView *mConsoleView;

	
#pragma mark - Information Text
	// ConsoleView
private:		// method
	void setInfo(const std::string &content);
	
private:		// data
	ui::Text *mInfoText;
	
	
#pragma mark - Core Functions
	void showLeaderboard();
	void updateInfo();
	void changeScore(int change);
	
#pragma mark - Internal Data
// Internal Data
private:		// data
	AstrodogLeaderboardType mBoardType;
	int mPlanet;
	int mScore;
	
	std::vector<int> mPlanetList;
	std::vector<AstrodogLeaderboardType> mBoardTypeList;
	
	
	
#pragma mark - User Feature
private:
	void loadLeaderboard();
	void changeBoard();
	void changePlanet();
	void scoreAdd20();
	void scoreMinus20();
	void submitScore();
	void testFacebook();
	void logoutFB();
};

#endif

#endif
