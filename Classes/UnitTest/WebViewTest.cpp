#ifdef ENABLE_TDD
//
//  WebViewTest.m
//	TDD Framework 
//
//
#include "WebViewTest.h"
#include "TDDHelper.h"
#include "cocos2d.h"
#include "ui/UIWebView.h"
#include "WebViewLayer.h"

using namespace cocos2d::ui;

void WebViewTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void WebViewTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void WebViewTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(WebViewTest::subTest);
    SUBTEST(WebViewTest::WebViewLayerTest);
}

#pragma mark -
#pragma mark Sub Test Definition
void WebViewTest::subTest(Ref *sender)
{
    log("this is a sample subTest");
    Size  winSize = Director::getInstance()->getVisibleSize();
    
  
    
    auto webView = cocos2d::experimental::ui::WebView::create();
    webView->setPosition(winSize/2);
    webView->setContentSize(winSize * 0.7);
    webView->loadURL("http://test.aurorainno.com:12046/game/like.html");
    webView->setScalesPageToFit(true);
    webView->setOnJSCallback([](cocos2d::experimental::ui::WebView *sender, const std::string &url){
        printf("JSCallback is triggered.\n");
    });
    
    Button* backBtn = Button::create();
    backBtn->setTitleText("back");
    backBtn->setTitleColor(Color3B(0, 255, 0));
    backBtn->setPosition(Vec2(webView->getPosition().x-webView->getContentSize().width/2+backBtn->getContentSize().width/2,
                              webView->getPosition().y+webView->getContentSize().height/2+
                              backBtn->getContentSize().height));
    backBtn->addClickEventListener([&,webView](Ref* ref){
        if(webView->canGoBack())
            webView->goBack();
    });
    
    Button* closeBtn = Button::create();
    closeBtn->setTitleText("close");
    closeBtn->setTitleColor(Color3B(255, 0, 0));
    closeBtn->setPosition(Vec2(webView->getPosition().x+webView->getContentSize().width/2-closeBtn->getContentSize().width/2,
                               webView->getPosition().y+webView->getContentSize().height/2+
                               closeBtn->getContentSize().height));
    closeBtn->addClickEventListener([&,closeBtn,webView,backBtn](Ref* ref){
            closeBtn->removeFromParent();
            webView->removeFromParent();
            backBtn->removeFromParent();
    });
    

    this->addChild(webView);
    this->addChild(closeBtn);
    this->addChild(backBtn);
}

void WebViewTest::WebViewLayerTest(Ref *sender){
    auto webViewLayer = WebViewLayer::create();
    addChild(webViewLayer);
}

#endif
