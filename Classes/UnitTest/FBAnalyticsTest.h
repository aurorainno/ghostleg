#ifdef ENABLE_TDD
//
//  FBAnalyticsTest.h
//
//
#ifndef __TDD_FBAnalyticsTest__
#define __TDD_FBAnalyticsTest__

// Include Header

#include "TDDBaseTest.h"

// Class Declaration
class FBAnalyticsTest : public TDDBaseTest
{
protected:
	virtual void setUp();
	virtual void tearDown();
	virtual void defineTests();
	virtual void willRunTest(const std::string &name);	// before run a test
	virtual void didRunTest(const std::string &name);	// after run a test
	
private:
	void testLogEvent();
};

#endif

#endif
