#ifdef ENABLE_TDD
//
//  SuitManagerTest.m
//	TDD Framework 
//
//
#include "SuitManagerTest.h"
#include "TDDHelper.h"
#include "SuitManager.h"
#include "GameManager.h"

void SuitManagerTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void SuitManagerTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void SuitManagerTest::defineTests()
{
	ADD_TEST(loadData);
	ADD_TEST(testDescription);
	ADD_TEST(testInfo);
	ADD_TEST(getCapsuleItemEffectAndValue);
}

#pragma mark -
#pragma mark Sub Test Definition
void SuitManagerTest::getCapsuleItemEffectAndValue()
{
	ItemEffect::Type effectType;
	float effectValue;
	
	
	GameManager::instance()->getSuitManager()->setDogSuitAndLevel(10, 2);
	GameManager::instance()->getSuitManager()
				->getCapsuleItemEffectAndValue(effectType, effectValue);
	
	log("type=%d value=%f", effectType, effectValue);
}


void SuitManagerTest::loadData()
{
	log("this is a sample subTest");
	
	SuitManager *manager = new SuitManager();
	
	manager->loadSuitData();
	manager->loadPlayerSuit();
	
	
	log("SuitData:\n%s\n", manager->infoSuitArray().c_str());
	log("PlayerSuit:\n%s\n", manager->infoPlayerSuitArray().c_str());
//
	delete manager;
}


void SuitManagerTest::testInfo()
{
	log("this is a sample subTest");
	
	SuitManager *manager = GameManager::instance()->getSuitManager();
	
	
	log("SuitData:\n%s\n", manager->infoSuitArray().c_str());
	log("PlayerSuit:\n%s\n", manager->infoPlayerSuitArray().c_str());
	//
	delete manager;
}

void SuitManagerTest::testDescription()
{
	log("this is a sample subTest");

	for(int suit=2; suit <= 7; suit++) {
		SuitData *suitData = GameManager::instance()->getSuitManager()->getSuitData(suit);
		
		int max = suitData->getMaxLevel();
		for(int level=1; level<=max; level++) {
			log("id=%d name=%s level=%d: %s", suit,
					suitData->getName().c_str(), level,
					suitData->getDisplayInfo(level).c_str());
		}
	}
}
//void SuitManagerTest::testAllDescription(
#endif
