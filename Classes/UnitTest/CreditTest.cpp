#ifdef ENABLE_TDD
//
//  CreditTest.m
//	TDD Framework 
//
//
#include "CreditTest.h"
#include "TDDHelper.h"
#include "CreditScene.h"

void CreditTest::setUp()
{
	log("TDD Setup is called");
	log("Please write somethings");
}


void CreditTest::tearDown()
{
	log("TDD tearDown is called");
	log("Please do clean up here");
}

#pragma mark -
#pragma mark List of Sub Tests

void CreditTest::setSubTest(Vector<MenuItem *> &menuArray)
{
	SUBTEST(CreditTest::testScene);
	SUBTEST(CreditTest::testOpenClose);
	SUBTEST(CreditTest::testInputSecret);
}

#pragma mark -
#pragma mark Sub Test Definition
void CreditTest::testScene(Ref *sender)
{
	auto scene = CreditSceneLayer::createScene();
	Director::getInstance()->pushScene(scene);
}

void CreditTest::testOpenClose(Ref *sender)
{
	CreditSceneLayer *layer = CreditSceneLayer::create();
	addChild(layer);
	mLayer = layer;
	
	// Open
	createTestButton("Show", Vec2(50, 100), [&](Ref *){
		mLayer->showSecretPanel();
	});
	
	// Close
	createTestButton("Hide", Vec2(250, 100), [&](Ref *){
		mLayer->hideSecretPanel();
	});


	// Director::getInstance()->pushScene(scene);
}


void CreditTest::testInputSecret(Ref *sender)
{
	CreditSceneLayer *layer = CreditSceneLayer::create();
	addChild(layer);
	mLayer = layer;
	
	layer->showSecretPanel();

	// Open
	createTestButton("getSecret", Vec2(50, 100), [&](Ref *){
		log("inputSecret=%s", mLayer->getInputSecret().c_str());
	});
	
	
	
	// Director::getInstance()->pushScene(scene);
}


#endif
