//
//  CashOutEffect.hpp
//  GhostLeg
//
//  Created by Calvin on 24/3/2017.
//
//

#ifndef CashOutEffect_hpp
#define CashOutEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class Player;

class CashOutEffect : public ItemEffect
{
    
public:
    
#pragma mark - Public static Method
    CREATE_FUNC(CashOutEffect);
    
    CashOutEffect();
    
    virtual Node *onCreateUI();
    virtual void onAttach();
    virtual void onDetach();
    virtual void onUpdate(float delta);
    virtual bool shouldDetach();
    virtual void setMainProperty(float value);
    virtual void activate();
	virtual std::string toString();
	virtual float getRemainTime();
    
    CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
    
private:
    void updateUI();
    
private:
    // Internal Data
    bool mStart;
    float mRemain;
    float mMaxDuration;
    Player* mPlayer;
};

#endif /* CashOutEffect_hpp */
