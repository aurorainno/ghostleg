//
//  DeliveryCompleteNoticeView.cpp
//  GhostLeg
//
//  Created by Calvin on 20/3/2017.
//
//

#include "DeliveryCompleteNoticeView.h"
#include "NPCOrder.h"
#include "AnimationHelper.h"
#include "RichTextLabel.h"

const Color4B kColorYellow = Color4B(0xff, 0xd8, 0x00, 255);

bool DeliveryCompleteNoticeView::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("DeliveryCompleteNoticeView.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    mReward = 0;
    mTips = 0;
    
    return true;
}


void DeliveryCompleteNoticeView::setupUI(cocos2d::Node *rootNode)
{
    setContentSize(rootNode->getContentSize());
    mRootNode = rootNode;
    
    mRewardPanel = rootNode->getChildByName("rewardPanel");
    mTipsPanel = rootNode->getChildByName("tipsPanel");
    
    mNpcSprite = rootNode->getChildByName<Sprite*>("npcSprite");
    
    Text* rewardText = mRewardPanel->getChildByName<Text*>("mainRewardText");
    RichTextLabel* InfoRichTextLabel = RichTextLabel::createFromText(rewardText);
    
    InfoRichTextLabel->setTextColor(1, kColorYellow);
    
    mMainRewardText = InfoRichTextLabel;
    
    mRewardPanel->addChild(mMainRewardText);
    rewardText->setVisible(false);
    
    Text* bonusRewardText = mTipsPanel->getChildByName<Text*>("bonusRewardText");
    InfoRichTextLabel = RichTextLabel::createFromText(bonusRewardText);
    
    InfoRichTextLabel->setTextColor(1, kColorYellow);
    
    mBonusRewardText = InfoRichTextLabel;
    
    mTipsPanel->addChild(mBonusRewardText);
    bonusRewardText->setVisible(false);

    mMainRewardPopText = mRewardPanel->getChildByName<Text*>("popText");
    mBonusRewardPopText = mTipsPanel->getChildByName<Text*>("popText");
}

void DeliveryCompleteNoticeView::setReward(NPCOrder *npcOrder)
{
    int npcId = npcOrder->getNpcID();
    int reward = npcOrder->getReward();
    
    std::string iconResName = StringUtils::format("npc/npc_icon_%03d.png",npcId);
    mNpcSprite->setTexture(iconResName);
    
//    mMainRewardText->setString(StringUtils::format("%d",reward));
    mReward = reward;
}

void DeliveryCompleteNoticeView::setBonusTip(int value)
{
//    mBonusRewardText->setString(StringUtils::format("%d",value));
    mTips = value;
}

 
void DeliveryCompleteNoticeView::showReward()
{
	mRewardPanel->setVisible(true);
	if(mReward <= 0) {
		mReward = 0;
	}
    mMainRewardText->setVisible(false);
    mMainRewardPopText->setVisible(true);
	
	// log("**** DEBUG: showReward: starting: reward=%d tip=%d", mReward, mTips);
	std::string rewardText = StringUtils::format("<c1>%d</c1> received", mReward);
	std::string bonusText = StringUtils::format("<c1>%d</c1> bonus", mTips);
	
    AnimationHelper::popTextEffect(this, mMainRewardPopText, 0, mReward, [=](){
        mMainRewardText->setVisible(true);
        mMainRewardPopText->setVisible(false);
        mMainRewardText->setString(rewardText);
		
		if(mTips <= 0) {
			// log("**** DEBUG: showReward: reward callback. No Tip Effect");
			if(mOnShowRewardCallback){
				mOnShowRewardCallback();
			}
			return;
		}
		
		mTipsPanel->setVisible(true);
        mBonusRewardText->setVisible(false);
        mBonusRewardPopText->setVisible(true);
        
		AnimationHelper::popTextEffect(this, mBonusRewardPopText, 0, mTips, [=](){
            mBonusRewardText->setVisible(true);
            mBonusRewardPopText->setVisible(false);
			mBonusRewardText->setString(bonusText);
			// log("**** DEBUG: showReward: reward callback");
            if(mOnShowRewardCallback){
				mOnShowRewardCallback();
			}
		});
	});
	
//    if(mTips){
//        mTipsPanel->setVisible(true);
//    }
}

void DeliveryCompleteNoticeView::showView()
{
    mRewardPanel->setVisible(false);
    mTipsPanel->setVisible(false);
	
    float displacementY = this->getContentSize().height;
    MoveBy* moveBy = MoveBy::create(0.5, Vec2(0,-displacementY));
    CallFunc* callFunc = CallFunc::create([=](){
        //showReward();
    });
	
	
    this->runAction(Sequence::create(moveBy,callFunc,NULL));
}


void DeliveryCompleteNoticeView::closeView()
{
    float displacementY = this->getContentSize().height;
    MoveBy* moveBy = MoveBy::create(0.5, Vec2(0,displacementY));
    
    CallFunc* callFunc = CallFunc::create([=](){
        if(mOnCloseCallback){
            mOnCloseCallback();
        }
    });

    this->runAction(Sequence::create(moveBy,callFunc,NULL));
    
    mReward = 0;
    mTips = 0;
}

void DeliveryCompleteNoticeView::setOnShowRewardCallback(std::function<void ()> callback)
{
    mOnShowRewardCallback = callback;
}

void DeliveryCompleteNoticeView::setOnCloseCallback(std::function<void ()> callback)
{
    mOnCloseCallback = callback;
}




