//
//  CashOutEffect.cpp
//  GhostLeg
//
//  Created by Calvin on 24/3/2017.
//
//

#include "CashOutEffect.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "Player.h"
#include "Enemy.h"
#include "Star.h"
#include "EffectLayer.h"

namespace {
	
	bool isEnemyAvailable(Enemy *enemy)
	{
		if(enemy->isObstacle()) {
			return false;
		}
		
		return (enemy->getState() == Enemy::Idle || enemy->getState() == Enemy::Active);
	}
}

CashOutEffect::CashOutEffect()
: ItemEffect(Type::ItemEffectCashOut)
, mLayer(nullptr)
, mStart(false)
{
    // TODO
}


Node *CashOutEffect::onCreateUI()
{
    return nullptr;
}

void CashOutEffect::onAttach()
{
    mRemain = mMaxDuration;
    ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
    mPlayer = modelLayer->getPlayer();
}

void CashOutEffect::onDetach()
{
    // GameWorld::instance()->getModelLayer()->removeEffectStatusFromAllEnemy();
    // EnemyFactory::instance()->resetDefaultEffectStatus();
    if(mPlayer) {
        mPlayer->removeCustomIndicator();
//        mPlayer->removeEffectStatus();
//       mPlayer->removeVisualEffect();
//        mPlayer->removeCsbAnimeByName(kSpeedUpCsbname);
    }
    setLayer(nullptr);
}


void CashOutEffect::onUpdate(float delta)
{
    if(mStart == false){
		return;
	}
	
	mRemain -= delta;
	
	updateUI();
        
	ModelLayer * modelLayer = GameWorld::instance()->getModelLayer();
	Vector<Enemy *> enemyList = modelLayer->getVisualEnemyList(-50,50);
		
		
	for(Enemy* enemy : enemyList){
		if(isEnemyAvailable(enemy)) {
			//log("Enemy is convert to money: %d", enemy->getObjectID());
			enemy->changeToChangeToMoney();
		}
	}
}

void CashOutEffect::setMainProperty(float value)
{
    mMaxDuration = value;
}

bool CashOutEffect::shouldDetach()
{
    return mRemain <= 0;
}


void CashOutEffect::updateUI()
{
    if(mLayer) {
        //        mLayer->setVisible(true);
        mLayer->setValue(mRemain);
    }
}

void CashOutEffect::activate()
{
    mStart = true;
    
    GameSound::playSound(GameSound::GrabStar);
    
    
    
    //	GameWorld::instance()->getModelLayer()->applyEffectStatusToAllEnemy(EffectStatusTimestop);
    //	EnemyFactory::instance()->setDefaultEffectStatus(EffectStatusTimestop);
//    ItemEffectUILayer *layer = ItemEffectUILayer::create();
 //   layer->setUseSpriteFrame(false);
 //   layer->configAsTimer("icon_speedup.png", "## sec", mMaxDuration);
    
 //   setLayer(layer);
 //   mLayer->setVisible(true);
    GameWorld::instance()->getEffectLayer()->addPowerUpIndicator(EffectLayer::CashOut, mMaxDuration);
    if(mPlayer) {
//        mPlayer->removeEffectStatus();
//        mPlayer->setEffectStatus(EffectStatusSpeedUp);
//        mPlayer->activateSpeedUp(mVelocity);
//        mPlayer->playCsbAnime(kSpeedUpCsbname, kSpeedUpAnimeName, false);
 //       mPlayer->addCustomIndicator(mLayer, Vec2(-30, 30), 1.0,false);
    }
    
    
    
    //
    //    SnowBall *snowBall = SnowBall::create();
    //    snowBall->setPosition(player->getPositionX(),player->getPositionY());
    //    snowBall->setAcceler(mVelocity);
    //
    //    modelLayer->addItem(snowBall);
    //
    //    log("SnowBallEffect is activated");
    //
//    GameWorld::instance()->disableCapsuleButton();
    
    updateUI();
}

float CashOutEffect::getRemainTime()
{
	return mRemain;
}

std::string CashOutEffect::toString()
{
	return StringUtils::format("CashOut: duration=%f", mMaxDuration);
}
