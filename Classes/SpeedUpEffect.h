//
//  SpeedUpEffect.hpp
//  GhostLeg
//
//  Created by Calvin on 8/12/2016.
//
//

#ifndef SpeedUpEffect_hpp
#define SpeedUpEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class Player;

class SpeedUpEffect : public ItemEffect
{
    
public:
    
#pragma mark - Public static Method
    CREATE_FUNC(SpeedUpEffect);
    
    SpeedUpEffect();
    
    virtual Node *onCreateUI();
    virtual void onAttach();
    virtual void onDetach();
    virtual void onUpdate(float delta);
    virtual bool shouldDetach();
    virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
    virtual void activate();
	virtual float getRemainTime();
	
	virtual std::string toString();
	
    CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
    
private:
    void updateUI();
    
private:
    // Internal Data
    bool mStart;
    float mRemain;
    float mMaxDuration;
    float mVelocity;
	float mBonusVelocity;
    Player* mPlayer;
};

#endif /* SpeedUpEffect_hpp */
