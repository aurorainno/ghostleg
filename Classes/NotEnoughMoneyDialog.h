//
//  NotEnoughMoneyDialog.hpp
//  GhostLeg
//
//  Created by Calvin on 1/2/2017.
//
//

#ifndef NotEnoughMoneyDialog_hpp
#define NotEnoughMoneyDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "CommonMacro.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NotEnoughMoneyDialog : public Layer
{
public:
    enum DialogMode{
        StarMode,
        DiamondMode,
        PuzzleMode
    };
    
    CREATE_FUNC(NotEnoughMoneyDialog);
	
	SYNTHESIZE_BOOL(mRedirectByScene, RedirectByScene);
	SYNTHESIZE_BOOL(mAlreadyInShop, AlreadyInShop);
    SYNTHESIZE_BOOL(mAutoremove, Autoremove);
    
    NotEnoughMoneyDialog();
    virtual bool init() override;
    
    void setupUI(Node* node);

    void setMode(DialogMode mode);
    
    void hideShopButton();
    
    void setCallbackAfterRedirect(std::function<void()> callback);
	
private:
	void redirectToShop();
    void redirectToLuckDraw();
	
private:
    Sprite *mDiamondSprite, *mStarSprite, *mPuzzleSprite;
    Button *mShopBtn, *mDrawBtn;
    Button* mCancelBtn;
    std::function<void()> mCallback;

	Vec2 mSingleButtonPos;
};

#endif /* NotEnoughMoneyDialog_hpp */
