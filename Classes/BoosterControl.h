//
//  BoosterControl.hpp
//  GhostLeg
//
//  Created by Ken Lee on 24/1/2017.
//
//

#ifndef BoosterControl_hpp
#define BoosterControl_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "CommonType.h"

USING_NS_CC;
using namespace cocos2d::ui;

class BoosterControl;
class CircularMaskNode;

class BoosterControl : public Layer
{
public:
    enum State{
        Active,
        Using,
        Cooldown,
    };
    
public:
	typedef std::function<bool (BoosterControl *)> BoosterControllCallback;
	// typedef std::function<void()>
public:
	
	CREATE_FUNC(BoosterControl);
	
	BoosterControl();
	virtual bool init() override;
	
	void setupUI();
	
	void setBoosterType(BoosterItemType type);
	BoosterItemType getBoosterType();
	
	
	void setCallback(const BoosterControllCallback &callback);

	void update(float delta);
    
    void updateCounterUI();
    void updateButtonState();

private:
	void onBoosterClicked();
	bool doCallback();

#pragma mark - Active & Cooldown state
public:
	void setActiveState();
	void setCooldownState();
    void setUsingState();

private:
	void changeOpacity(int value);
	void handleCooldown(float delta);
    void handleUsing();
	
private:
	Node *mRootNode;
    Node* mCountPanel;
	Button *mBoosterButton;
	Sprite *mCooldownSprite;
	Text *mCountText;
	BoosterItemType mType;
	BoosterControllCallback mCallback;
    State mState;
	
	// For cooldown
	bool mIsCooldown;			// is it under cooldown state
	float mCooldownRemain;		//
    float mCooldownTime;
    CircularMaskNode* mCooldownMask;
};



#endif /* BoosterControl_hpp */
