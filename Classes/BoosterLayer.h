//
//  BoosterLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 23/1/2017.
//
//

#ifndef BoosterLayer_hpp
#define BoosterLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class RichTextLabel;

class BoosterLayer : public Layer
{
public:
    CREATE_FUNC(BoosterLayer);
    
    BoosterLayer();
    
    virtual bool init() override;
    void setupUI(Node* rootNode);
    void onEnter() override;
    
    void refreshAllItemView();
    void setItemView(int tag, int count, bool isSelected = false);
    void setItemCallback(int tag);

    void setupDistrictUI(int planetID);
    
    void setStartCallback(std::function<void()> callback);
    void setOnEnterCallback(std::function<void()> callback);
private:
    Node* mMainPanel;
    
    Sprite* mPlanetSprite;
    Text* mDistrictLabel;
    Text* mPlanetNameLabel;
    
    Button* mStartBtn;
    Button* mCancelBtn;
    RichTextLabel* mInfoLabel;
    std::function<void()> mStartCallback;
    std::function<void()> mOnEnterCallback;
};



#endif /* BoosterLayer_hpp */
