//
//  CharacterInfoLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 7/4/2017.
//
//

#include "CharacterInfoLayer.h"
#include "RichTextLabel.h"
#include "PlayerManager.h"
#include "AbilityInfoLayer.h"
#include "InfoDesLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "AnimeNode.h"
#include "TutorialLayer.h"
#include "GameWorld.h"
#include "RichTextLabel.h"
#include "GameSound.h"
#include "Analytics.h"
#include "NotEnoughMoneyDialog.h"

#define kMaxMaxSpeed 400
#define kMaxAcceleration 400
#define kMaxTurnSpeed 300
#define kMaxStunReduction 1.5

const Color4B kColorYellow = Color4B(0xff, 0xde, 0x01, 255);

bool CharacterInfoLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    mTutorialLayer = nullptr;
    Node* rootNode = CSLoader::createNode("CharacterInfoLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    scheduleUpdate();
    
    return true;
}

void CharacterInfoLayer::setupUI(cocos2d::Node *rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    mRootNode = rootNode->getChildByName("mainPanel");
    
    mStarText = (Text*) rootNode->getChildByName("totalCoinText");
    mDiamondText = rootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mBlockPanel = rootNode->getChildByName("blockPanel");
    
    mFbConnectBtn = (Button*) rootNode->getChildByName("fbBtn");
    mFbName = (Text *) rootNode->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) rootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
    mFbConnectBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });

    NotEnoughMoneyDialog *dialog = NotEnoughMoneyDialog::create();
    dialog->setVisible(false);
    dialog->setRedirectByScene(false);
    dialog->setAutoremove(false);
    mNotEnoughResourcesDialog = dialog;
    rootNode->addChild(mNotEnoughResourcesDialog);
    
    mInfoBtn = mRootNode->getChildByName<Layout*>("infoBtn");
    mCharNameText = mRootNode->getChildByName<Text*>("nameText");
    mCharSprite = mRootNode->getChildByName<Sprite*>("charSprite");
    mStatPanel = mRootNode->getChildByName("statPanel");
    mLevelPanel = mRootNode->getChildByName<Layout*>("levelPanel");
    mUpgradePanel = mRootNode->getChildByName("upgradePanel");
    
    mBackBtn = mRootNode->getChildByName<Button*>("backBtn");
    mBackBtn->addClickEventListener([=](Ref*){
        if(mExitCallback){
            mExitCallback(mPlayerProfile->getCharID());
        }
        this->removeFromParent();
    });
    
    mInfoBtn->addClickEventListener([=](Ref*){
        InfoDesLayer *layer = InfoDesLayer::create();
        addChild(layer);
    });
    
    mLevelPanel->addClickEventListener([=](Ref*){
        AbilityInfoLayer* layer = AbilityInfoLayer::create();
        layer->setView(mPlayerProfile);
        addChild(layer);
    });
    
    Text* infoText = mUpgradePanel->getChildByName<Text*>("infoText");
    RichTextLabel* InfoRichTextLabel = RichTextLabel::createFromText(infoText);
    
    InfoRichTextLabel->setTextColor(1, kColorYellow);
    
    mInfoText = InfoRichTextLabel;
    
    mUpgradePanel->addChild(InfoRichTextLabel);
    infoText->setVisible(false);
}

void CharacterInfoLayer::onEnter()
{
    Layer::onEnter();
	
    updateStars();
    updateDiamond();
    setupFbProfile();
    
    setUpgradeTutorialStepOne();
	
	LOG_SCREEN(Analytics::scn_character_detail);
}

void CharacterInfoLayer::update(float delta)
{
    if(mTutorialLayer){
        mTutorialLayer->update(delta);
    }
}

void CharacterInfoLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void CharacterInfoLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void CharacterInfoLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        mFbName->setString(user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
 //       mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void CharacterInfoLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}

void CharacterInfoLayer::setView(PlayerCharProfile* charProfile)
{
    setPlayerProfile(charProfile);
	
	charProfile->update();		// update the latest main attribute
	
    mCharNameText->setString(charProfile->getName());
    mCharSprite->setTexture(StringUtils::format("playChar/player_icon_%03d.png",mPlayerProfile->getCharID()));

    setStatView();
    setLevelView();
    setUpgradeView();
}



void CharacterInfoLayer::setLevelView()
{
    Text* levelText = mLevelPanel->getChildByName<Text*>("levelText");

    
    if(!mPlayerProfile->isLocked()){
        levelText->setString(StringUtils::format("level %d",mPlayerProfile->getCurrentLevel()));
        Node* mainNode = mLevelPanel->getChildByName("unlockedPanel");
        mainNode->setVisible(true);
        int level = 0;
        Vector<Node*> list = mainNode->getChildren();
        
        for(Node* levelSprite : list){
            if(level >= mPlayerProfile->getCurrentLevel()){
                break;
            }
        
            if(levelSprite->getName().find("levelSprite")!=std::string::npos){
                levelSprite->setVisible(true);
                level++;
            }
        }
        
        if(mPlayerProfile->isMaxLevel()){
            Node* maxLabel = mainNode->getChildByName("maxLabel");
            maxLabel->setVisible(true);
        }
    }else{
        levelText->setString("Locked");
        Node* mainNode = mLevelPanel->getChildByName("lockedPanel");
        mainNode->setVisible(true);
        
        Sprite* charSmallIcon = mainNode->getChildByName<Sprite*>("charSmallIcon");
        charSmallIcon->setTexture(StringUtils::format("fragment/ui_fragment_%02d.png",mPlayerProfile->getCharID()));
        
        Text* levelValueText = mainNode->getChildByName<Text*>("levelText");
        levelValueText->setString(StringUtils::format("%d/%d",mPlayerProfile->getCollectedFragCount(),mPlayerProfile->getUpgradeFragCount()));
        
        Node* progressBar = mainNode->getChildByName("clippedNode")->getChildByName("levelSprite");
        float movePortion =  1 - ((float)mPlayerProfile->getCollectedFragCount() / (float)mPlayerProfile->getUpgradeFragCount());
        float barWidth = progressBar->getContentSize().width * progressBar->getScaleX();
        
        progressBar->setPositionX(-30 - barWidth*movePortion);
    }
}

void CharacterInfoLayer::setUpgradeView()
{
    Text* titleText = mUpgradePanel->getChildByName<Text*>("titleText");
    if(mPlayerProfile->isLocked()){
        titleText->setString("information");
    }else if(mPlayerProfile->isMaxLevel()){
        titleText->setString("max level");
    }else{
        titleText->setString("next level");
    }

    PlayerAbility* ability = mPlayerProfile->getAbilityByLevel(mPlayerProfile->getCurrentLevel() + 1);
    std::string upgradeDes;
    if(mPlayerProfile->isLocked()){
        upgradeDes = mPlayerProfile->getUnlockDescription();
    }else{
        upgradeDes = ability == nullptr ? "" : ability->getDesc();
    }
    
    mInfoText->setString(upgradeDes);
    
    Node* upgradeInfoPanel = mUpgradePanel->getChildByName("upgradeInfoPanel");
    if(mPlayerProfile->isMaxLevel() || mPlayerProfile->isLocked()){
        upgradeInfoPanel->setVisible(false);
    }
    
    Sprite* charSmallIcon01 = upgradeInfoPanel->getChildByName<Sprite*>("charSmallIcon01");
    Sprite* charSmallIcon02 = upgradeInfoPanel->getChildByName<Sprite*>("charSmallIcon02");
    charSmallIcon01->setTexture(StringUtils::format("fragment/ui_fragment_%02d.png",mPlayerProfile->getCharID()));
    charSmallIcon02->setTexture(StringUtils::format("fragment/ui_fragment_%02d.png",mPlayerProfile->getCharID()));
    
    
    Text* collectedFragText = upgradeInfoPanel->getChildByName<Text*>("collectedFragText");
    Text* upgradeCoinText = upgradeInfoPanel->getChildByName<Text*>("upgradeCoinText");
    Text* upgradeFragText = upgradeInfoPanel->getChildByName<Text*>("upgradeFragText");
    
    collectedFragText->setString(StringUtils::format("%d",mPlayerProfile->getCollectedFragCount()));
    upgradeCoinText->setString(StringUtils::format("%d",mPlayerProfile->getUpgradeMoney()));
    upgradeFragText->setString(StringUtils::format("%d",mPlayerProfile->getUpgradeFragCount()));
    
    Button* upgradeBtn = upgradeInfoPanel->getChildByName<Button*>("upgradeBtn");
    upgradeBtn->addClickEventListener([=](Ref*){
        this->handleUpgrade();
    });
}

void CharacterInfoLayer::handleUpgrade(bool isTutorial)
{
    if(isTutorial){
        int requireFragment = mPlayerProfile->getUpgradeFragCount();
        int collectedFrag = mPlayerProfile->getCollectedFragCount();
        
        int requireMoney = mPlayerProfile->getUpgradeMoney();
        int myMoney = GameManager::instance()->getMoneyValue(MoneyType::MoneyTypeStar);
        
        if(collectedFrag < requireFragment){
            PlayerManager::instance()->addPlayerFragment(mPlayerProfile->getCharID(), requireFragment - collectedFrag);
        }
        
        if(myMoney < requireMoney){
            GameWorld::instance()->addCoin(requireMoney - myMoney);
        }
    }
    
    PlayerCharProfile::UpgradeStatus status = PlayerManager::instance()->upgradeChar(mPlayerProfile->getCharID());
    
    if(status!=PlayerCharProfile::UpgradeOk){
        if(isTutorial){
           
            
        }else{
            if(status==PlayerCharProfile::UpgradeNotEnoughFragment){
                mNotEnoughResourcesDialog->setMode(NotEnoughMoneyDialog::PuzzleMode);
                mNotEnoughResourcesDialog->setCallbackAfterRedirect([=]{
                    this->updateStars();
                    this->setView(mPlayerProfile);
                    mNotEnoughResourcesDialog->setVisible(false);
                });
                mNotEnoughResourcesDialog->setVisible(true);
            }else if(status==PlayerCharProfile::UpgradeNotEnoughMoney){
                mNotEnoughResourcesDialog->setMode(NotEnoughMoneyDialog::StarMode);
                mNotEnoughResourcesDialog->setCallbackAfterRedirect([=]{
                    this->updateStars();
                    this->updateDiamond();
                    mNotEnoughResourcesDialog->setVisible(false);
                });
                mNotEnoughResourcesDialog->setVisible(true);
            }
            return;
        }
    }
    
    GameSound::playSound(GameSound::UpgradeChar);
    mBlockPanel->setVisible(true);
    setPlayerProfile(PlayerManager::instance()->getPlayerCharProfile(mPlayerProfile->getCharID()));
    Node* mainNode = mLevelPanel->getChildByName("unlockedPanel");
    Sprite* levelSprite = (Sprite*) mainNode->getChildByName(StringUtils::format("levelSprite%02d",mPlayerProfile->getCurrentLevel()));
    AnimeNode* animeNode = AnimeNode::create();
    animeNode->setup(StringUtils::format("gui_character_levelbar_lvl%d.csb",mPlayerProfile->getCurrentLevel()));
    animeNode->setStartAnime("active",false,true);
    animeNode->setPosition(levelSprite->getPosition());
    animeNode->setPlayEndCallback([=](Ref*, std::string animeName){
        if(animeName == "active"){
            setView(mPlayerProfile);
            mBlockPanel->setVisible(false);
            this->updateStars();
            if(mUpgradeBtnCallback){
                mUpgradeBtnCallback(mPlayerProfile->getCharID());
            }
            if(isTutorial){
                setUpgradeTutorialStepTwo();
            }
        }
    });
    mainNode->addChild(animeNode);
}

void CharacterInfoLayer::setUpgradeTutorialStepOne()
{
    bool isPlayed = GameManager::instance()->getUserData()->isCoachmarkPlayed("selectChar");
    if(isPlayed){
        return;
    }
    
    TutorialLayer *tutorialLayer = TutorialLayer::create();
    //	tutorialLayer->setColor(Color3B::BLACK);
    //	tutorialLayer->setOpacity(200);
    
    tutorialLayer->setContentSize(getContentSize());
    
    tutorialLayer->setLocalZOrder(10000);	// Very high
    tutorialLayer->setCloseCallback([&](Ref *sender){
        handleUpgrade(true);
        mTutorialLayer = nullptr;
    });
    
    
    Button* upgradeBtn = mUpgradePanel->getChildByName("upgradeInfoPanel")->getChildByName<Button*>("upgradeBtn");
    Vec2 upgradeBtnPos = upgradeBtn->convertToWorldSpaceAR(Vec2::ZERO);
    Vec2 btnSize = upgradeBtn->getContentSize() * upgradeBtn->getScale();
    Vec2 csbPos = Vec2(upgradeBtnPos.x, upgradeBtnPos.y + 20);
    
    tutorialLayer->setCoachmarkTutorial("selectChar",
                                        "tutorial/tutorial_point.csb",
                                        csbPos,upgradeBtnPos,btnSize,-120);
    
    
    //
    tutorialLayer->reset();
    addChild(tutorialLayer);
    mTutorialLayer = tutorialLayer;
    
    mTutorialLayer->update(0);

}

void CharacterInfoLayer::setUpgradeTutorialStepTwo()
{
    GameManager::instance()->getUserData()->setCoachmarkPlayed("selectChar", true);

    TutorialLayer *tutorialLayer = TutorialLayer::create();
    
    //	tutorialLayer->setColor(Color3B::BLACK);
    //	tutorialLayer->setOpacity(200);
    
    tutorialLayer->setContentSize(getContentSize());
    
    tutorialLayer->setLocalZOrder(10000);	// Very high
    tutorialLayer->setCloseCallback([=](Ref *sender){
        mTutorialLayer = nullptr;
        if(mExitCallback){
            mExitCallback(mPlayerProfile->getCharID());
        }
        this->removeFromParent();
    });
    
    Vec2 backBtnPos = mBackBtn->convertToWorldSpaceAR(Vec2::ZERO);
    Vec2 btnSize = mBackBtn->getContentSize() * mBackBtn->getScale();
    Vec2 csbPos = Vec2(backBtnPos.x + 20, backBtnPos.y);
    
    tutorialLayer->setCoachmarkTutorial("selectChar",
                                        "tutorial/tutorial_point.csb",
                                        csbPos,backBtnPos,btnSize,-20, true, "now you are \nmuch stronger", false);
    
    
    //
    tutorialLayer->reset();
    addChild(tutorialLayer);
    mTutorialLayer = tutorialLayer;
    
    mTutorialLayer->update(0);

}

void CharacterInfoLayer::setStatView()
{
	GameplaySetting *setting = PlayerManager::instance()->getCharacterGameplaySetting(
												mPlayerProfile->getCharID());
	
	
	
	
	
	///
    Node* maxSpeedPanel = mStatPanel->getChildByName("maxSpeedPanel");
    int maxSpeed = mPlayerProfile->getMaxSpeed();
    setupStatBar(maxSpeedPanel,maxSpeed,kMaxMaxSpeed);
    
    Node* accelerationPanel = mStatPanel->getChildByName("accelerationPanel");
    float acceleration = mPlayerProfile->getAcceleration();
    setupStatBar(accelerationPanel,acceleration,kMaxAcceleration);

    Node* turnSpeedPanel = mStatPanel->getChildByName("turnSpeedPanel");
    int turnSpeed = mPlayerProfile->getTurnSpeed();
    setupStatBar(turnSpeedPanel,turnSpeed,kMaxTurnSpeed);

    Node* recoveryPanel = mStatPanel->getChildByName("recoveryPanel");
    float recovery = mPlayerProfile->getStunReduction();
    setupStatBar(recoveryPanel,recovery,kMaxStunReduction);
}

void CharacterInfoLayer::setupStatBar(cocos2d::Node *panel, float value, float maxValue)
{
    Node* progessBar = panel->getChildByName("progressBar");
    Text* valueText = progessBar->getChildByName<Text*>("valueText");
    if(panel->getName().find("recovery")!=std::string::npos){
        valueText->setString(StringUtils::format("%.1f",value));
    }else{
        valueText->setString(StringUtils::format("%d",(int) value));
    }
    
    float movePortion =  1 - (value / maxValue);
    float barWidth = progessBar->getContentSize().width * progessBar->getScaleX();
    barWidth -= barWidth * 0.16;
    progessBar->setPositionX(- barWidth * movePortion);
}

void CharacterInfoLayer::setUpgradeBtnCallback(std::function<void (int)> callback)
{
    mUpgradeBtnCallback = callback;
}

void CharacterInfoLayer::setOnExitCallback(std::function<void (int)> callback)
{
    mExitCallback = callback;
}
