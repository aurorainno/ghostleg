//
//  ChaserBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 2/5/2017.
//
//

#include "ChaserBehaviour.h"
#include "Enemy.h"
#include "StringHelper.h"
#include "EnemyFactory.h"
#include "BulletBehaviour.h"
#include "Player.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "GameSound.h"
#include "Constant.h"

#define kBulletEnemyID 81

const float kSpeedThresholdMin = 80;			// The speed when player speed is too low
const float kSpeedThresholdMax = 400;
const float kSpeedOffset = 20;				// the diff between player and chaser speed
//const float kSpeedOffsetRatio = 0.9;
const float kSpeedOffsetRatio = 0.7f;

//CC_SYNTHESIZE(int, mInitialSpeed, InitialSpeed);
//CC_SYNTHESIZE(int, mMaxSpeed, MaxSpeed);
//CC_SYNTHESIZE(int, mAcceleration, Acceleration);
// mPlayerSpeedAtStart
const int kUpperInactiveOffset = 300;

ChaserBehaviour::ChaserBehaviour()
: mStartChasing(false)
, mSpeedY(0)
, mInitWithPlayerSpeed(true)
, mAcceleration(10)
, mMaxSpeed(250)
, mSpeedRatio(kSpeedOffsetRatio)
, mBrakeSpeedRatio(0.2f)
, mIncStunSpeed(150)
{}


void ChaserBehaviour::onCreate()
{

    updateProperty();
    //    getEnemy()->setAnimationEndCallFunc(GameModel::Attack, CC_CALLBACK_0(FireBulletBehaviour::onAttack, this));
    //    getEnemy()->setOnFrameEventCallback([=](const std::string &eventName, EventFrame* event){
    //        if(eventName == "EmitBullet"){
    //            fireBullet();
    //        }
    //    });
    //
    getEnemy()->changeToIdle();
}

void ChaserBehaviour::onVisible()
{
    
}


void ChaserBehaviour::onInActive()
{
	mStartChasing = false;
	getEnemy()->setVisible(false);
}

void ChaserBehaviour::onActive()
{
	mStartChasing = true;
	getEnemy()->setVisible(true);
	
	// Reset Speed
	// mSpeedY = mInitWithPlayerSpeed ? GameWorld::instance()->getPlayer()->getSpeedY()
	//									: mInitialSpeed;
	
	setupSpeed();
	//log("ChaserMaxSpeed: %d", mMaxSpeed);
}

void ChaserBehaviour::onUpdate(float delta)
{
    if(mStartChasing == false){
		return;
	}
	
    if(getEnemy()->getEffectStatus() == EffectStatusTimestop){
		//log("ChaserBehaviour: enemy timeStopped");
        return;
    }
	
	float playerX = GameWorld::instance()->getPlayer()->getPositionX();
	float playerY = GameWorld::instance()->getPlayer()->getPositionY();

	updateSpeedWithPlayer(delta);
	//log("Chaser: speed=%f", mSpeedY);
	
	//
	float finalSpeed;
//	if(mUsingStunSpeed == false && ) {
//	  // make brake less dangerous
//		finalSpeed = mSpeedY * mBrakeSpeedRatio;
//	} else {
		finalSpeed = mSpeedY;
//	}
	
	float posX = playerX;
	float posY = getEnemy()->getPositionY() + finalSpeed * delta;

//	float minEnemyY = playerY - 300;
//	float maxEnemyY = playerY + 10;
//	if(posY < minEnemyY) {
//		posY = minEnemyY;
//	} else if(posY > maxEnemyY) {
//		posY = maxEnemyY;
//	}

	posY = adjustYPosition(posY, playerY);
	
	getEnemy()->setPosition(posX, posY);
	
	
	// updateSpeed(delta);
	
}


void ChaserBehaviour::setupSpeed()
{
	Player *player = GameWorld::instance()->getPlayer();
	float playerSpeed = player == nullptr ? mInitialSpeed : player->getSpeedVector().y;

	if(mInitWithPlayerSpeed) {
		//
		//	mSpeedY = mInitWithPlayerSpeed ? GameWorld::instance()->getPlayer()->getSpeedY()
		//										: mInitialSpeed;
		//
		mSpeedY = playerSpeed;
	} else {
		mSpeedY = mInitialSpeed;
	}
	
	mInitialMaxSpeed = MIN(playerSpeed * mSpeedRatio, mMaxSpeed);
	mSpeedThreshold = playerSpeed * 0.5f;		//MAX(kSpeedThresholdMin, mMaxSpeed -);
	mReachMax = false;		// Make Chaser using its own acceleration before at Max Speed
	
	mSpeedWhenStun = mMinStunSpeed;
}

void ChaserBehaviour::updateSpeedWithPlayer(float delta)
{
	Player *player = GameWorld::instance()->getPlayer();
	
	
//	if(mReachMax == false) {				// ken: more graceful to the beginning momenet
//		mSpeedY += mAcceleration * delta;
//		if(mSpeedY > mMaxSpeed) {
//			mSpeedY = mMaxSpeed;
//		}
//		mReachMax = mSpeedY >= mInitialMaxSpeed;		//
//		return;
//	}
	
	//
	
	Vec2 playerSpeed = player->getSpeedVector();
	
	if(player->isStunning()){
		mUsingStunSpeed = true;
		mSpeedY = mSpeedWhenStun;
		return;
	}
	
	mUsingStunSpeed = false;
	
	if(GameWorld::instance()->getIsBraking()){
		mSpeedY = playerSpeed.y * mBrakeSpeedRatio;
	} else {
		// TODO: Tuning!
		//mSpeedY = speedY - kSpeedOffset;			// Chaser speed: mMaxSpeed ~ (playerSpeed - 20)
		mSpeedY = playerSpeed.y * mSpeedRatio;
	}
	
	
//	
//	if(mSpeedY >= mSpeedThreshold) {
//		mSpeedY = mSpeedThreshold;
//	}
}

void ChaserBehaviour::updateSpeed(float delta)
{
	if(mSpeedY >= mMaxSpeed) {
		return;
	}
	
	// increase the speed
	mSpeedY += mAcceleration * delta;
	if(mSpeedY > mMaxSpeed) {
		mSpeedY = mMaxSpeed;
	}
}


bool ChaserBehaviour::shouldInActive()
{
	return false;
}

bool ChaserBehaviour::onCollidePlayer(Player *player)
{
    getEnemy()->setAction(GameModel::Action::Attack);
    GameSound::playSoundFile("enemySFX/monster_striking.mp3");
    GameSound::playSound(GameSound::VoiceDeath);
    
    return false;
}


void ChaserBehaviour::updateProperty()
{
//    if(mData == nullptr) {
//        log("BomberBehaviour::updateProperty :data is null");
//        return;
//    }
//    
//    std::map<std::string, std::string> property = mData->getMapProperty("bomberSetting");
//    
//    if(property.find("cooldown") == property.end()) {	// not found
//        mCooldown = 2.0f;
//    } else {
//        mCooldown = aurora::StringHelper::parseFloat(property["cooldown"]);
//    }
//    
//    mIsLeft = getEnemy()->isFaceLeft();
}

std::string ChaserBehaviour::toString()
{
    return "ChaserBehaviour";
}


float ChaserBehaviour::adjustYPosition(float y, float playerY)
{
	float maxEnemyY = playerY + 10;
	if(y > maxEnemyY) {
		return maxEnemyY;			// check the upper bound
	}
	
	// Check lower bound
	float screenBottomY = GameWorld::instance()->getBottomMapY();
	
	if(y > screenBottomY){
		return y;
	}
	
	
	return screenBottomY;
}

void ChaserBehaviour::increaseSpeedThreshold()
{
	float newTheshold = mSpeedThreshold + 50;
	if(newTheshold > kSpeedThresholdMax) {
		newTheshold = kSpeedThresholdMax;
	}
	
	mSpeedThreshold = newTheshold;
	
	
	//
	if(mSpeedWhenStun < mMaxStunSpeed) {
		mSpeedWhenStun *= (mIncStunSpeed / 100.0f);
		if(mSpeedWhenStun > mMaxStunSpeed) {
			mSpeedWhenStun = mMaxStunSpeed;
		}
	}
	
}
