//
//  Coin.cpp
//  GhostLeg
//
//  Created by Ken Lee on 21/3/2016.
//
//

#include "Coin.h"
#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"
#include "RandomHelper.h"
#include "ViewHelper.h"
#include "ModelLayer.h"
#include "GeometryHelper.h"
#include "EffectLayer.h"
#include "GameplaySetting.h"
#include "TimeMeter.h"


const std::string Coin::kSmallCoinCsbFile = "item/item_coin.csb";
const std::string Coin::kBigCoinCsbFile = "item/item_big_coin.csb";

Coin::Coin(Coin::CoinType type)
: Collectable(parseCoinType(type))
, mCoinType(type)
{
	
}

Item::Type Coin::parseCoinType(Coin::CoinType type)
{
    switch (type) {
        case SmallCoin:
        return Item::ItemSmallCoin;
        
        case LargeCoin:
        return Item::ItemBigCoin;
        
        default:
        return Item::ItemSmallCoin;
    }
}


bool Coin::init()
{
    if(Collectable::init() == false) {
        log("Coin: init failed");
        return false;
    }
    
	setScale(1.0);
	
    std::string csbFile = mCoinType == SmallCoin ? kSmallCoinCsbFile : kBigCoinCsbFile;
    
#if IS_ANDROID
    mCsbFile = csbFile;		// this help less lagging, lazy loading
#else
    setup(csbFile);	// note: default Action is idle
#endif
    return true;
	
}

bool Coin::use()
{
    bool isUsed = Collectable::use();
    
    if(isUsed == false) {
        return false;
    }
    
    // Make sound effect
    TSTART("Coin.playSound");
    GameSound::playStarSound();
    TSTOP("Coin.playSound");
    // Sound sound = (Sound)(GameSound::Star1 + idx);	// if idx=2, Sound = Star3 (1+2)
    
    //GameSound::playSound(GameSound::Star4);
    // GameSound::playCoinSound();
    //TimeMeter::instance()->stop("Coin.playSound");
    //GameSound::playSound(GameSound::Coin);
    
    
    // Need a double star logic
    bool isDoubleStar = canTriggerDoubleCoin();
    int starValue = 1;
    
    if(isDoubleStar){
        starValue = 2;
        showDoubleCoinEffect();
    }
    
    //XXX
    TSTART("Coin.collectCoin");
    GameWorld::instance()->collectCoin(starValue);		// note: collect coin already gain score
    TSTOP("Coin.collectCoin");
    
    return true;

}



bool Coin::canTriggerDoubleCoin(){
    int start = 1, end = 100;
    int randValue = RandomHelper::random_int(start, end);		// aurora::RandomHelper::randomBetween(start, end);
    
    //GameplaySetting
    GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
    int threshold = setting->getAttributeValue(GameplaySetting::DoubleStarPercent);
    
    //log("*** DEBUG: randValue=%d threshold=%d", randValue, threshold);
    
    return randValue <= threshold;
}

void Coin::showDoubleCoinEffect(){
    //    ModelLayer* modelLayer = GameWorld::instance()->getModelLayer();
    //    Vec2 pos = modelLayer->getPlayer()->getPosition();
    GameWorld::instance()->getEffectLayer()->showDoubleStarEffect();
}
