//
//	The order of the NPC
//
//  NPCOrder.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/3/2017.
//
//

#ifndef NPCOrder_hpp
#define NPCOrder_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "JSONData.h"
#include "CommonMacro.h"

class NPCOrderData;
class GameplaySetting;

USING_NS_CC;

class NPCOrder : public JSONData, public Ref
{
public:
	CREATE_FUNC(NPCOrder);
	NPCOrder();
	virtual bool init() { return true; }

public:
	void setup(NPCOrderData *npcData, GameplaySetting *setting);		// setting affect the bonus value
	
public:
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(int, mOrderID, OrderID);
	CC_SYNTHESIZE(int, mNpcID, NpcID);
	CC_SYNTHESIZE(int, mNpcMap, NpcMap);
	CC_SYNTHESIZE(int, mDifficulty, Difficulty);	// 1 - Easy  3 - Hard
	CC_SYNTHESIZE(int, mOrderIndex, OrderIndex);	// position of the order, 0 = first order
	CC_SYNTHESIZE(bool, mIsLastOrder, IsLastOrder);
	
	CC_SYNTHESIZE(int, mTimeLimit, TimeLimit);	// time limit in second
	CC_SYNTHESIZE(int, mReward, Reward);		// reward money for this order
	
	CC_SYNTHESIZE_READONLY(int, mBaseTimeLimit, BaseTimeLimit);	// time limit in second
	CC_SYNTHESIZE_READONLY(int, mBaseReward, BaseReward);		// reward money for this order
	
	CC_SYNTHESIZE_READONLY(int, mBonusTimeLimit, BonusTimeLimit);
	CC_SYNTHESIZE_READONLY(int, mBonusReward, BonusReward);
	CC_SYNTHESIZE_READONLY(float, mBonusTipRatio, BonusTipRatio);
	
	CC_SYNTHESIZE(int, mFragChar, FragChar);	// Fragment Character
	CC_SYNTHESIZE(int, mFragReward, FragReward);	// Fragment reward count
	
	
	
public:
	int getDistance();
	int getPixelDistance();
	float getFinalBonusTipRatio();
	std::vector<int> getRandomMaps(int numReturn = 0);	// 0 mean all maps
	int calculateTimeLimit();
	bool isTutorialNpc();
	
public:
	std::string getRewardText();
	std::string getTimeLimitText();
	std::string toString();
	//std::string
	
	
	
	

#pragma mark - Map related stuff
public:
	std::vector<int> &getMapList();
	std::string infoMap();
	
	
private:
	NPCOrderData *mNpcOrderData;
	std::vector<int> mMapList;		// the final maplist

//#pragma mark - JSON
//public: // JSON Data
//	void defineJSON(rapidjson::Document::AllocatorType &allocator,
//					rapidjson::Value &outValue);
//	bool parseJSON(const rapidjson::Value &jsonValue);
	

};

#endif /* NPCOrder_hpp */
