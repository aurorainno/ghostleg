//
//  NPC.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#ifndef NPC_hpp
#define NPC_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "CommonType.h"
#include "GameModel.h"
#include "NPCOrder.h"

USING_NS_CC;

class EmojiAnimeNode;

class NPC : public GameModel
{
public:
	enum Emoji {
		EmojiWaiting = 0,
		Emoji1 = 1,				// Normal
		Emoji2 = 2,
		Emoji3 = 3,
		Emoji4 = 4,
		Emoji5 = 5,				// Very Happy
	};
	
public:
#pragma mark - Public static Method
	CREATE_FUNC(NPC);
	
	static std::string getCsbFilename(int npcID);
	
#pragma mark - Public Method and Properties
	NPC();
	
	virtual bool init();
	virtual void setNpc(int npcID);
	
	
	CC_SYNTHESIZE_RETAIN(NPCOrder *, mOrder, Order);
	
	virtual void update(float delta);
	
	virtual std::string toString();
	
	bool shouldBeCleanUp();
	virtual std::string getName();
	
	int getNpcID();
	
	void setEmojiVisible(bool flag);
	void closeEmoji();
	void setEmoji(Emoji emoji, const std::function<void()> &callback=nullptr);
	
private:
	void setupEmoji();
	
#pragma mark - Internal Data
protected:
	int mNpcID;
	EmojiAnimeNode *mEmojiNode;
};



#endif /* NPC_hpp */
