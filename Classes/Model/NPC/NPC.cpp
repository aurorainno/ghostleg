//
//  NPC.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#include "NPC.h"


//
//  NPC.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2016.
//
//

#include "NPC.h"
#include "StringHelper.h"
#include "RandomHelper.h"
#include "CommonType.h"
#include "VisibleRect.h"
#include "EmojiAnimeNode.h"
#include <map>


std::string NPC::getCsbFilename(int npcID)
{
	return StringUtils::format("res/npc/npc_%03d.csb", npcID);
}

NPC::NPC()
: mOrder(nullptr)
, mEmojiNode(nullptr)
{
	
}

bool NPC::init()
{
	bool isOk = GameModel::init();	// calling the init of parent class
	if (isOk == false)
	{
		return false;
	}
	
	setVisible(false);
	setupEmoji();
	
	return true;
}


void NPC::update(float delta)
{
	GameModel::update(delta);		// animation update
}

int NPC::getNpcID()
{
	return mNpcID;
}

void NPC::setNpc(int npcID)
{
	mNpcID = npcID;
	
	// Set up the appearance
	// temp use
	std::string filename = getCsbFilename(mNpcID);
	if(FileUtils::getInstance()->isFileExist(filename) == false) {
		filename = "npc/npc_001.csb";
	}
	
	setup(filename);
	setAction(GameModel::Action::Idle);		// Idle animation
	setVisible(true);
}


bool NPC::shouldBeCleanUp()
{
	return false;
}


std::string NPC::toString()
{
	std::string result = "";
	
	result += " npcID=" + INT_TO_STR(getNpcID());
	
	return result;
}

std::string NPC::getName()
{
	return StringUtils::format("npc_%d", mNpcID);
}


void NPC::setEmojiVisible(bool flag)
{
	if(mEmojiNode) {
		mEmojiNode->setVisible(flag);
	}
	
}

void NPC::closeEmoji()
{
	if(mEmojiNode) {
		mEmojiNode->closeEmoji();
	}
}

void NPC::setEmoji(NPC::Emoji emoji, const std::function<void()> &callback)
{
	if(mEmojiNode == nullptr) {
		return;
	}
	
	
	setEmojiVisible(true);
	
	if(emoji == EmojiWaiting) {
		mEmojiNode->showWaiting();
	} else {
		mEmojiNode->setEmojiEndCallback(callback);
		mEmojiNode->showEmoji((int) emoji);
	}
}

void NPC::setupEmoji()
{
	EmojiAnimeNode *node = EmojiAnimeNode::create();
	node->setPosition(Vec2(0, 50));		// TODO
	node->setScale(0.7f);
	addChild(node);
	
	mEmojiNode = node;

}
