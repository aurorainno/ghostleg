//
//  NPCOrder.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/3/2017.
//
//

#include "NPCOrder.h"
#include "NPCOrderData.h"
#include "GameplaySetting.h"
#include "StringHelper.h"
#include "Constant.h"
#include "JSONHelper.h"
#include "PlayerManager.h"
#include "NPCManager.h"

//const float kTimeReductionRate = 0.5f;
const float kTimeReductionRate = 2.0f;

const float kDefaultTipRatio = 0.1;

namespace {
	int calcFragmentCount(int min, int max) {
		if(min == max) {
			return min;
		}
		
		return RandomHelper::random_int(min, max);
	}
}

NPCOrder::NPCOrder()
: mNpcID(0)
, mTimeLimit(0)
, mReward(0)
, mDifficulty(1)
, mOrderIndex(0)
, mNpcOrderData(nullptr)
, mIsLastOrder(false)
{
	
}


void NPCOrder::setup(NPCOrderData *npcData, GameplaySetting *setting)
{
	CC_ASSERT(npcData != nullptr);
	CC_ASSERT(setting != nullptr);
	
	//
	mNpcOrderData = npcData;
	
	// Copy data from npcOrderData
	mOrderID = npcData->getOrderID();
	mNpcMap = npcData->getNpcMap();
	mNpcID = npcData->getNpcID();
	mName = npcData->getName();
	mDifficulty = npcData->getDifficulty();
	
	
	int rewardChar = npcData->getFragChar();
	if(PlayerManager::instance()->isCharacterNeedFragment(rewardChar)) {
		mFragChar = rewardChar;
		mFragReward = calcFragmentCount(npcData->getFragMin(), npcData->getFragMax());
	} else {
		mFragChar = 0;
		mFragReward = 0;
	}
	
	// Base Data
	mBaseReward = npcData->getReward();
	mBaseTimeLimit = calculateTimeLimit();
	
	// Bonus Data
	mBonusReward = setting->getAttributeValue(GameplaySetting::BonusReward);
	mBonusTipRatio = setting->getAttributeValue(GameplaySetting::BonusTipPercent);	// +percent for the reward
	mBonusTimeLimit = setting->getAttributeValue(GameplaySetting::ExtraTimeLimit);
	
	//
	mReward = mBaseReward + mBonusReward;
	mTimeLimit = mBaseTimeLimit + mBonusTimeLimit;
	
	//mBonusReward
}

int NPCOrder::calculateTimeLimit()
{
	int originTime = mNpcOrderData->calcTimeLimit();
	
	int reduceTime = (int)(kTimeReductionRate * NPCManager::instance()->getClientCount());
	
	int finalTime = originTime - reduceTime;
	if(finalTime < 20) {
		finalTime = 20;
	}
	
	return finalTime;
}

std::vector<int> NPCOrder::getRandomMaps(int numReturn)
{
	std::vector<int> maps = mNpcOrderData->getRandomMaps();
	if(numReturn == 0 || maps.size() <= numReturn) {
		return maps;
	}
	
	// Trim the last
	
	
	std::vector<int>::const_iterator first = maps.begin();
	std::vector<int>::const_iterator last = maps.begin() + numReturn;
	return std::vector<int>(first, last);
}

float NPCOrder::getFinalBonusTipRatio()
{
	return kDefaultTipRatio;
}

std::string NPCOrder::toString()
{
	return StringUtils::format("order=%d map=%d npc=%d reward=%d timeLimit=%d "
							   "difficulty=%d bonusTipRatio=%f fragment=%d/%d "
							   "isLastOrder=%d",
							   getOrderID(), getNpcMap(), getNpcID(),
							   getReward(), getTimeLimit(), getDifficulty(),
							   getBonusTipRatio(), getFragChar(), getFragReward(),
							   getIsLastOrder());
}

int NPCOrder::getDistance()
{
	return getPixelDistance() / 100;
}
							   

int NPCOrder::getPixelDistance()
{
	return mNpcOrderData->getPixelDistance();
}



#pragma mark - Map related stuff

std::string NPCOrder::infoMap()
{
	return mNpcOrderData->infoMap();
}

std::vector<int> &NPCOrder::getMapList()
{
	return mNpcOrderData->getMapList();
}

//public:
//CC_SYNTHESIZE(int, mGameSpeed, GameSpeed);
//void setMapList(const std::vector<int> mapList);
//std::vector<int> &getMapList;
//std::string infoMap();
//

std::string NPCOrder::getRewardText()
{
	if(mBonusReward == 0){
		return StringUtils::format("%d", mBaseReward);
	}
	
	return StringUtils::format("%d+%d", mBaseReward, mBonusReward);
}

std::string NPCOrder::getTimeLimitText()
{
	if(mBonusTimeLimit == 0){
		return StringUtils::format("%d", mBaseTimeLimit);
	}

	return StringUtils::format("%d+%d", mBaseTimeLimit, mBonusTimeLimit);
}

bool NPCOrder::isTutorialNpc()
{
	if(mNpcOrderData == nullptr) {
		return false;
	}
	return mNpcOrderData->isTutorialNpc();
}
