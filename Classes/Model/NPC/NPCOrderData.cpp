//
//  NPCOrderData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 28/3/2017.
//
//

#include "NPCOrderData.h"
#include "StringHelper.h"
#include "Constant.h"
#include "JSONHelper.h"
#include "RandomHelper.h"

const int kNumReturnMap = 6;

#pragma mark - GameMapRange
GameMapRange::GameMapRange(int _start, int _end)
{
	start = _start;
	end = _end;
}

GameMapRange::GameMapRange(const std::string &str)
{
	int _start, _end;
	if(str.find('-') == std::string::npos) { // cannot find separator
		int value = aurora::StringHelper::parseInt(str);
		start = value;
		end = value;
		return;
	}
		
	
	sscanf(str.c_str(), "%d-%d", &_start, &_end);
	
	start = _start;
	end = _end;
}

std::string GameMapRange::toString()
{
	return StringUtils::format("[%d-%d]", start, end);
}


#pragma mark - NPCOrderData

NPCOrderData::NPCOrderData()
: mNpcID(0)
, mTimeLimit(0)
, mReward(0)
, mDifficulty(1)
, mGameSpeed(200)
, mMapList()
, mFragMin(0)
, mFragMax(0)
, mFragChar(0)
, mMapRangeList()
, mSecPerMap(10)
, mTutorialNpc(false)
{
	
}

std::string NPCOrderData::toString()
{
	return StringUtils::format("order=%d map=%d npc=%d reward=%d timeLimit=%d difficulty=%d",
							   getOrderID(), getNpcMap(), getNpcID(),
							   getReward(), getTimeLimit(), getDifficulty());
}

int NPCOrderData::getDistance()
{
	return getPixelDistance() / 1000;
}


int NPCOrderData::getPixelDistance()
{
	int numMap;
	
	if(mMapRangeList.size() > 0) {
		numMap = (int) mMapRangeList.size();
	} else {
		numMap = (int) MIN(mMapList.size(), kNumReturnMap);
	}
	
	
	return (int)(numMap * kMapHeight);
}

int NPCOrderData::calcTimeLimit()
{
	if(getTimeLimit() > 0) {
		return getTimeLimit();
	}
	
	if(mMapRangeList.size() == 0) {		// OLD Style
		return 30;
	}
	
	return (int)(mSecPerMap * mMapRangeList.size());
}

#pragma mark - Map related stuff
void NPCOrderData::setMapList(const std::vector<int> mapList)
{
	mMapList.clear();
	for(int mapID : mapList) {
		mMapList.push_back(mapID);
	}
}

std::string NPCOrderData::infoMap()
{
	std::string infoMap = StringUtils::format("mapList: mapCount=%ld", mMapList.size());
	
	infoMap += " map=" + aurora::StringHelper::vectorToString(mMapList);
	infoMap += "\n";
	
	infoMap += StringUtils::format("  mapRangeList: count=%ld\n", mMapRangeList.size());
	for(GameMapRange range : mMapRangeList) {
		infoMap += range.toString() + " ";
	}
	infoMap += "\n";
	
	
	return infoMap;
}

std::vector<int> &NPCOrderData::getMapList()
{
	return mMapList;
}

//public:
//CC_SYNTHESIZE(int, mGameSpeed, GameSpeed);
//void setMapList(const std::vector<int> mapList);
//std::vector<int> &getMapList;
//std::string infoMap();
//



#pragma mark - JSON
void NPCOrderData::defineJSON(rapidjson::Document::AllocatorType &allocator,
						  rapidjson::Value &outValue)
{
	// No need to save
}

bool NPCOrderData::parseJSON(const rapidjson::Value &jsonValue)
{
	//	"npcName"		: "Annie",
	//	"npcAnime"		: 1,
	//	"reward"		: 1000,
	//	"timeLimit"		: 10,
	//	"difficulty"	: 1,
	//	"gameMap"		: [1, 2, 2, 3, 3, 4, 5, 5]
	std::string name = aurora::JSONHelper::getString(jsonValue, "npcName");
	int npcAnime = aurora::JSONHelper::getInt(jsonValue, "npcAnime", 1);
	int reward = aurora::JSONHelper::getInt(jsonValue, "reward", 100);
	int timeLimit = aurora::JSONHelper::getInt(jsonValue, "timeLimit", 10);
	int difficulty = aurora::JSONHelper::getInt(jsonValue, "difficulty", 1);
	int npcMap = aurora::JSONHelper::getInt(jsonValue, "npcMap", 100);
	int fragChar = aurora::JSONHelper::getInt(jsonValue, "fragChar", 0);
	int fragMin = aurora::JSONHelper::getInt(jsonValue, "fragMin", 0);
	int fragMax = aurora::JSONHelper::getInt(jsonValue, "fragMax", 0);
	float secPerMap = aurora::JSONHelper::getFloat(jsonValue, "secPerMap", 10.0f);
	bool isTutorial = aurora::JSONHelper::getBool(jsonValue, "tutorial", false);
	
	
	setmSecPerMap(secPerMap);
	setName(name);
	setNpcID(npcAnime);
	setReward(reward);
	setTimeLimit(timeLimit);
	setDifficulty(difficulty);
	setNpcMap(npcMap);
	setFragMin(fragMin);
	setFragMax(fragMax);
	setFragChar(fragChar);
	setTutorialNpc(isTutorial);
	
	mMapList.clear();
	aurora::JSONHelper::getJsonIntArray(jsonValue, "gameMap", mMapList);
	
	parseMapRangeData(jsonValue, "mapList");
	
	
	return true;
}


void NPCOrderData::parseMapRangeData(const rapidjson::Value &jsonValue, const std::string &jsonName)
{
	
	std::vector<std::string> mapRangeStrList;
	aurora::JSONHelper::getJsonStrArray(jsonValue, jsonName.c_str(), mapRangeStrList, true);
	
	
	mMapRangeList.clear();
	
	for(std::string rangeStr : mapRangeStrList) {
		GameMapRange range = GameMapRange(rangeStr);
		
		mMapRangeList.push_back(range);
	}
	
//	
//	const rapidjson::Value &arrayValue = jsonValue[name];
//	if(arrayValue.IsArray() == false) {
//		return;
//	}
//	
//	mMapRangeList.clear();
//	
//	for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
//	{
//		outArray.push_back(arrayValue[i].GetInt());
//	}
	
//	//const rapidjson::Value &jsonValue
//	aurora::JSONHelper::get(jsonValue, "gameMap", mMapList);
}


std::vector<int> NPCOrderData::getRequireMapList()
{
	if(mMapRangeList.size() == 0) {		// OLD Format
		return mMapList;
	}
	
	
	// New Map List
	std::vector<int> result;
	
	for(GameMapRange range : mMapRangeList) {
		for(int mapID = range.start; mapID <= range.end; mapID++) {
			result.push_back(mapID);
		}
	}

	return result;
}

std::vector<int> NPCOrderData::getRandomMapFromMapList()
{
	std::vector<int> sourceMap = getMapList();
	
	int totalMapSize = (int) sourceMap.size();
	int numShuffle = totalMapSize * 1.5;
	
	std::vector<int> randomMapList = aurora::RandomHelper::getRandomizeList(
								sourceMap, kNumReturnMap, numShuffle);
	std::vector<int> result;
	for(int mapID : randomMapList) {
		result.push_back(mapID);
	}

	return result;
}

std::vector<int> NPCOrderData::getRandomMapFromMapRangeList()
{
	std::vector<int> result;
	
	for(GameMapRange range : mMapRangeList) {
		
		int mapID = range.start == range.end ? range.start
						: RandomHelper::random_int(range.start, range.end);
		
		result.push_back(mapID);
	}
	
	return result;
}


std::vector<int> NPCOrderData::getRandomMaps()
{
	if(mMapRangeList.size() == 0) {		// OLD Style
		return getRandomMapFromMapList();
	} else {							// New Style
		return getRandomMapFromMapRangeList();
	}
}
