//
//  NPCOrderData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 28/3/2017.
//
//

#ifndef NPCOrderData_hpp
#define NPCOrderData_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "JSONData.h"
#include "CommonMacro.h"

USING_NS_CC;

class GameMapRange
{
public:

public:	// Construct
	GameMapRange(int _start, int _end);
	GameMapRange(const std::string &str);

public:
	int start;
	int end;
	
	std::string toString();
};


class NPCOrderData : public JSONData, public Ref
{
public:
	CREATE_FUNC(NPCOrderData);
	NPCOrderData();
	virtual bool init() { return true; }
	
public:
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(int, mOrderID, OrderID);
	CC_SYNTHESIZE(int, mNpcID, NpcID);
	CC_SYNTHESIZE(int, mNpcMap, NpcMap);
	CC_SYNTHESIZE(int, mTimeLimit, TimeLimit);	// time limit in second
	CC_SYNTHESIZE(int, mReward, Reward);		// reward money for this order
	CC_SYNTHESIZE(int, mDifficulty, Difficulty);	// 1 - Easy  3 - Hard
	CC_SYNTHESIZE(int, mFragChar, FragChar);		// The character of the fragment
	CC_SYNTHESIZE(int, mFragMin, FragMin);			// Minimum Fragment Given
	CC_SYNTHESIZE(int, mFragMax, FragMax);			// Minimum Fragment
	CC_SYNTHESIZE(float, mSecPerMap, mSecPerMap);
	SYNTHESIZE_BOOL(mTutorialNpc, TutorialNpc);
	
public:
	int getDistance();
	int getPixelDistance();
	int calcTimeLimit();
	
public:
	std::string toString();
	
#pragma mark - Map related stuff
public:
	CC_SYNTHESIZE(int, mGameSpeed, GameSpeed);
	void setMapList(const std::vector<int> mapList);
	std::vector<int> &getMapList();
	std::string infoMap();
	
	
	std::vector<int> getRequireMapList();		// The Map need to load
	
	std::vector<int> getRandomMaps();
	
	std::vector<GameMapRange> &getMapRangeList();
	
private:
	std::vector<int> getRandomMapFromMapList();
	std::vector<int> getRandomMapFromMapRangeList();
	
private:
	std::vector<int> mMapList;
	std::vector<GameMapRange> mMapRangeList;
	
#pragma mark - JSON
public: // JSON Data
	void defineJSON(rapidjson::Document::AllocatorType &allocator,
					rapidjson::Value &outValue);
	bool parseJSON(const rapidjson::Value &jsonValue);
	
	void parseMapRangeData(const rapidjson::Value &jsonValue, const std::string &jsonName);
};


#endif /* NPCOrderData_hpp */
