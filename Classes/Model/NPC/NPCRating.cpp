//
//  NPCRating.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/3/2017.
//
//

#include "NPCRating.h"

NPCRating::NPCRating()
: mOrderID(0)
, mNpcID(0)
, mRating(0)
, mReward(0)
, mScore(0)
, mTip(0)
, mRemainTime(0)
, mTimeUsed(0)
, mFragChar(0)
, mFragCount(0)
{
	
}

std::string NPCRating::toString()
{
	std::string info = StringUtils::format("orderID=%d", getOrderID());
	
	info += StringUtils::format(" npcID=%d", getNpcID());
	info += StringUtils::format(" score=%d rating=%d", getScore(), getRating());
	info += StringUtils::format(" reward=%d/%d+%d", getReward(), getBaseReward(), getBonusReward());
	//info += StringUtils::format(" tip=%d/%d+%d", getTip(), getBaseTip(), getBonusTip());
	info += StringUtils::format(" remainTime=%d", getRemainTime());
	info += StringUtils::format(" timeUsed=%.2f", getTimeUsed());
	info += StringUtils::format(" frag=%d/%d", mFragChar, mFragCount);
	
	return info;
}


int NPCRating::getTotalReward()
{
	return getReward();		//+ getTip();
}


void NPCRating::setRewardValue(int baseReward, int bonusReward)
{
	mBaseReward = baseReward;
	mBonusReward = bonusReward;
	
	mReward = mBaseReward + mBonusReward;
}

void NPCRating::setTipValue(int baseTip, int bonusTip)
{
	mBaseTip = baseTip;
	mBonusTip = bonusTip;
	
	mTip = mBaseTip + mBonusTip;
}
