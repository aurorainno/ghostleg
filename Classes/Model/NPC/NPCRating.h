//
//	The Rating of the NPC
//
//
//  NPCRating.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/3/2017.
//
//

#ifndef NPCRating_hpp
#define NPCRating_hpp

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class NPCRating : public Ref
{
public:
	CREATE_FUNC(NPCRating);
	
public:
	NPCRating();
	virtual bool init() { return true; }
	
public:
	CC_SYNTHESIZE(int, mOrderID, OrderID);
	CC_SYNTHESIZE(int, mNpcID, NpcID);
	
	CC_SYNTHESIZE(int, mRating, Rating);
	CC_SYNTHESIZE(int, mReward, Reward);	// money reward
	CC_SYNTHESIZE(int, mTip, Tip);			// tip based on the rating
	CC_SYNTHESIZE(int, mScore, Score);
	
	CC_SYNTHESIZE(int, mBaseReward, BaseReward);	// money reward
	CC_SYNTHESIZE(int, mBonusReward, BonusReward);	// money reward
	CC_SYNTHESIZE(int, mBaseTip, BaseTip);			// tip based on the rating
	CC_SYNTHESIZE(int, mBonusTip, BonusTip);
	
	CC_SYNTHESIZE(float, mTimeUsed, TimeUsed);		// seconds used in this round
	CC_SYNTHESIZE(int, mRemainTime, RemainTime);	// time remain when reach the NPC
	
	CC_SYNTHESIZE(int, mFragChar, FragChar);
	CC_SYNTHESIZE(int, mFragCount, FragCount);
	
	int getTotalReward();
	
	void setRewardValue(int baseReward, int bonusReward);
	void setTipValue(int baseTip, int bonusTip);
	
public:
	std::string toString();
};

#endif /* NPCRating_hpp */
