//
//  FireWork.cpp
//  GhostLeg
//
//  Created by Calvin on 27/10/2016.
//
//

#include "FireWork.h"

FireWork::FireWork():
mTimeLine(nullptr),
mRootNode(nullptr)
{
}

FireWork::~FireWork()
{
    setTimeLine(nullptr);       // release take place in the setter
    
    mRootNode = nullptr;		// it is retain and release by Node->add/removeChild
}


bool FireWork::init()
{
    return true;
}

void FireWork::setup(const std::string &csbName,std::string animationName){
    // Setting the Root Node
    Node *rootNode = CSLoader::createNode(csbName);
    if(rootNode == nullptr) {
        log("FireWork. cannot create the node: %s", csbName.c_str());
        return;
    }
    rootNode->setPosition(Vec2(-320, 0));
    rootNode->setScale(0.7);
    
    addChild(rootNode);
    mRootNode = rootNode;
    
    mAnimationName = animationName;
    // This is for debug
    // ViewHelper::createRedSpot(this, Vec2(0, 0));
    
    
    // Setting the animation
    cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
    if(timeline == nullptr) {
        log("FireWork. cannot create the timeLine: %s", csbName.c_str());
        return;
    }
    
    setTimeLine(timeline);		// retain take place in the setter
    mRootNode->runAction(mTimeLine);
    if(mTimeLine) {
        mOriginTimeSpeed = mTimeLine->getTimeSpeed();
        mTimeLine->setFrameEventCallFunc(CC_CALLBACK_1(FireWork::onFrameEvent, this));
    }
    
    playAnimation();
}

void FireWork::onFrameEvent(Frame *frame)
{
    log("FireWork::onFrameEvent: called");
    if(frame == nullptr) {
        log("FireWork::onFrameEvent: fame is null");
        return;
    }
    
    EventFrame* event = dynamic_cast<EventFrame*>(frame);
    if(!event) {
        log("FireWork::onFrameEvent: event is null");
        return;
    }
    
    
    std::string str = event->getEvent();
    log("FireWork::onFrameEvent: event %s found.", str.c_str());
    if (str == "playParticle")
    {
        handlePlayParticleEvent(event);
    }
    
}

void FireWork::handlePlayParticleEvent(EventFrame *event)
{
    // log("playParticle event is trigger");
    Node *node = event->getNode();
    if(node == nullptr) {
        return;
    }
    
    ParticleSystemQuad *particle = dynamic_cast<ParticleSystemQuad*>(node);
    if(particle == nullptr) {
        return;
    }
    
    particle->resetSystem();
}


void FireWork::playAnimation()
{
    mTimeLine->play(mAnimationName, false);
}



