//
//  PlayerSuit.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/6/2016.
//
//

#include "Suit.h"

#include "StringHelper.h"

SuitData *SuitData::create()
{
	SuitData *data = new SuitData();
	
	data->autorelease();
	
	return data;
}

std::string SuitData::toString()
{
	std::string result = "";
	
	result += "name=" + getName();
	result += " desc=" + getInfo();
	result += " passive=" + INT_TO_STR(getPassiveSkill());
	result += " item=" + INT_TO_STR(getItemEffect());
	result += " maxLv=" + INT_TO_STR(getMaxLevel());
	
	result += "\n";
	
	for(int i=0; i<getMaxLevel(); i++) {
		int level = i;
		float value = getLevelValue(level);
		int price = getUpgradePrice(level);
		
		char tempstr[50];
		sprintf(tempstr, "level=%3d value=%5.2f price=%5d\n", level, value, price);
		result += tempstr;
	}
	
	return result;
}

std::string SuitData::getDisplayInfo(int level)
{
//    std::vector<std::string> tokens = aurora::StringHelper::split(mInfo, '#');
//    if(tokens.size()<=1){
//        return mInfo;
//    }else{
//        std::string result = tokens[0] + INT_TO_STR(level) + tokens[1];
//        return result;
//    }
	float firstValue = getLevelValue(1);
    float value = getLevelValue(level);
    float valueDiff = std::abs(value - firstValue);
	float diffPercent = (firstValue == 0) ? 0 : roundf(valueDiff * 1000 / firstValue) / 10 ;
    std::string snowBallSizeStr = getSnowBallLevelValue(level);
	
	//log("id=%d level=%d value=%f diff=%f", mSuitID, level, value, valueDiff);
	
	std::string valueString = aurora::StringHelper::formatDecimal(value);
	std::string valueDiffString = aurora::StringHelper::formatDecimal(valueDiff);
	std::string valueDiffPercentStr = aurora::StringHelper::formatDecimal(diffPercent);
	std::string suffixS = (value > 1) ? "s" : "";
	
	std::string resultInfo = mInfo;
	
	aurora::StringHelper::replaceString(resultInfo, "#diffp", valueDiffPercentStr);
    aurora::StringHelper::replaceString(resultInfo, "#diff", valueDiffString);
    aurora::StringHelper::replaceString(resultInfo, "#size", snowBallSizeStr);
	aurora::StringHelper::replaceString(resultInfo, "#s", suffixS);
    aurora::StringHelper::replaceString(resultInfo, "#", valueString);
    
    return resultInfo;
}

std::string SuitData::getSnowBallLevelValue(int level)
{
    if(level <= 0) {
        return "";
    }else if(level == 1){
        return "Big";
    }else if(level == 2){
        return "Giant";
    }else if(level >= 3){
        return "Massive";
    }
    return "";
}

float SuitData::getLevelValue(int level)
{
	if(level < 0) {
		return 0;
	}else if(level >= mLevelValue.size()) {	 // prevent out of bound
		level = (int)(mLevelValue.size() - 1);
	}
	
	return mLevelValue[level];
}


void SuitData::setLevelValue(int level, float value)
{
	if(level < 0 || level >= mLevelValue.size()) {
		return;
	}
	mLevelValue[level] = value;
}

int SuitData::getUpgradePrice(int level)
{
	if(level <= 0) {
		return 0;
	}else if(level >= mLevelPrice.size()) {
		level = (int)(mLevelPrice.size() - 1);
	}
	
	return mLevelPrice[level];
}

void SuitData::reset()
{
	mLevelPrice.clear();
	mLevelValue.clear();
}

void SuitData::addLevelValue(float value)
{
	mLevelValue.push_back(value);
}

void SuitData::addLevelPrice(int price)
{
	mLevelPrice.push_back(price);
}


std::string PlayerSuit::toString()
{
	std::string result = "";
	
	result += "suit=" + INT_TO_STR(getSuitID());
	result += " level=" + INT_TO_STR(getLevel());
	
	return result;
}

void PlayerSuit::parse(const std::string &string)
{
	int suitID;
	int level;
	
	sscanf(string.c_str(), "suit=%d level=%d", &suitID, &level);
	
	
	setSuitID(suitID);
	setLevel(level);
}
