//
//  EnemyBehaviour.hpp
//  GhostLeg
//
//	Abstract Oject of the EnemyBehaviour
//
//
//  Created by Ken Lee on 31/5/2016.
//
//

#ifndef EnemyBehaviour_hpp
#define EnemyBehaviour_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class Enemy;
class EnemyData;
class Player;
class Item;

class EnemyBehaviour : public Ref
{
public: // enum
	enum InfoType {
		InfoTypeMapTopY,
		InfoTypeMapBottomY,
		InfoTypeDistanceToPlayer,
		InfoTypeVertialDistToPlayer,
		InfoTypeHorizontalDistToPlayer,
	};
	
	enum MoveType
	{
		Free,
		Route,
	};
	
	enum BehaviourType
	{
		Normal,
		FreeMove,
		Obstacle,
		FallDown,
		DropStuff,
		AutoHide,
		SlowPlayer,
        ReduceEnergy,
		Thief,		// Stoling item or star
        Patrol,
        Attack,
		SuddenMove,
        Jump,
        FireBullet,
        Bullet,
        Bomber,
        Chaser
	};
	
	enum ActiveCondition
	{
		TopY,
		BottomY,
		PlayerDistance,		// player distance less than arg
		PlayerYDistance,	// player distance less than arg
		PlayerXDistance,	// player distance less than arg
		OnStart, 
	};
public: // static helper
	static BehaviourType getBehaviourType(const std::string typeStr);
	static void parseActiveCondition(EnemyData *enemyData, const std::string typeStr);
	
public:
	bool init() { return true; }	// For Create
	
	EnemyBehaviour();
	
	CC_SYNTHESIZE(Enemy *, mEnemy, Enemy);
	
	CC_SYNTHESIZE(Vec2, mSpeed, Speed);
    CC_SYNTHESIZE(Vec2, mAcceler, Acceler);
	
	void setData(EnemyData *data);
	const EnemyData *getData() const;

public:	// key interface
	virtual bool canCollideItem() { return false; }		// default no collision
	virtual void onCreate() = 0;
	virtual void onVisible() = 0;
	virtual void onActive() = 0;
	virtual void onUpdate(float delta) = 0;
	virtual void onInActive();
	virtual void onAddToWorld() {} // default nothing
	
	// When the enemy hit/killed by player or missile, ...
	virtual void onHit(const Vec2 &hitPos) {};		// normal case nothing
	
	// When enemy collide with player
	virtual bool onCollidePlayer(Player *player);
	
	// When enemy collide with item
	virtual bool onCollideItem(Item *item);		// item being consumed
	
	virtual bool shouldActive();
	virtual bool shouldInActive();
	
	virtual float getSpeedX();
	virtual float getSpeedY();
    
    virtual void onApplyEffectStatus(EffectStatus status);
    virtual void onRemoveEffectStatus(EffectStatus status);

	virtual bool isStatic();		// For static enemy, no state and no update
	
public: // Helper Method
	Vec2 getPosition();
	float getInfo(InfoType type);
	virtual std::string toString();
	int getRes();
    AlertType getAlertType();
	
	std::string infoAttribute();
	
protected:
	bool isActiveConditionMatch();
	void handleMove(float delta);
	
protected:		//
	EnemyData *mData;
	
	
};

#endif /* EnemyBehaviour_hpp */
