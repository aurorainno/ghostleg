//
//  SlowPlayerBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef SlowPlayerBehaviour_hpp
#define SlowPlayerBehaviour_hpp

#include <stdio.h>
#include "NormalBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class Player;

class SlowPlayerBehaviour : public NormalBehaviour
{
public:
	CREATE_FUNC(SlowPlayerBehaviour);

	SlowPlayerBehaviour();

	bool init() { return true; }

	std::string toString();

public:
	virtual bool onCollidePlayer(Player *player);
};



#endif /* NormalBehaviour_hpp */
