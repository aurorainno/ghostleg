//
//  ThiefBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef ThiefBehaviour_hpp
#define ThiefBehaviour_hpp

#include <stdio.h>
#include "NormalBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class Player;
class ItemEffectUILayer;

class ThiefBehaviour : public NormalBehaviour
{
public:
	CREATE_FUNC(ThiefBehaviour);

	ThiefBehaviour();

	bool init();

	std::string toString();

public:
	virtual void onCreate();
	virtual bool canCollideItem();
	virtual bool onCollidePlayer(Player *player);
	virtual bool onCollideItem(Item *item);
	virtual void onHit(const Vec2 &hitPos);

private:
	void giveStarReward();
	void giveCandyReward();
	
private:
	int mStarCollected;
	int mCandyCollected;
	ItemEffectUILayer *mIndicator;
};



#endif /* NormalBehaviour_hpp */
