//
//  SuddenMoveBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef SuddenMoveBehaviour_hpp
#define SuddenMoveBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class SuddenMoveBehaviour : public EnemyBehaviour
{
public:
	CREATE_FUNC(SuddenMoveBehaviour);

	SuddenMoveBehaviour();

	bool init() { return true; }

	std::string toString();

	
public:
	virtual void onCreate();
	virtual void onVisible();
	virtual void onActive();
	virtual void onUpdate(float delta);
	virtual void onAddToWorld();
private:
	void updateProperty();
	void fixPosition(Vec2 &newPos);
	Vec2 getStartPosition();
	
private:
	int mOffsetX = 0;
	int mOffsetY = 0;
	float mMoveDuration = 0;
	Vec2 mFinalPosition;
	Vec2 mMoveSpeed;
	bool mIsStopped;
};



#endif /* NormalBehaviour_hpp */
