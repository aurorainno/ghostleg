//
//  AttackBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 23/2/2017.
//
//

#include "AttackBehaviour.h"
#include "StringHelper.h"
#include "Enemy.h"
#include "GameSound.h"
#include "GameWorld.h"

const int kCrabID[] = {4,7};
const int kLaserID = 41;


AttackBehaviour::AttackBehaviour()
: EnemyBehaviour()
, mAccumTime(0)
, mCoolDownTime(1)
, mStartCountDown(false)
, mShouldFlipX(false)
, mCurrentFace(false)
{}


void AttackBehaviour::onCreate()
{
    updateProperty();
    getEnemy()->setAnimationEndCallFunc(GameModel::Attack, CC_CALLBACK_0(AttackBehaviour::onAttack, this));
    getEnemy()->changeToIdle();
}

void AttackBehaviour::onVisible()
{
    
}

void AttackBehaviour::onActive()
{
    startAttack();
}

void AttackBehaviour::onUpdate(float delta)
{
    if(getEnemy()->getEffectStatus() == EffectStatusTimestop||
       getEnemy()->getState() !=  Enemy::State::Active){
        return;
    }
    
    if(mStartCountDown){
        mAccumTime += delta;
        if(mAccumTime > mCoolDownTime){
            mAccumTime = 0;
            startAttack();
        }
    }
}

bool AttackBehaviour::isColliable()
{
    if(getEnemy()->getEnemyData()->getType() == kLaserID){
        return mStartCountDown == true;
    }else{
        return true;
    }
}

void AttackBehaviour::startAttack()
{
    mStartCountDown = false;
    if(mShowAlert){
        getEnemy()->showWakeupAlert([=](){
            if(mShouldFlipX){
                getEnemy()->setFace(mCurrentFace);
                mCurrentFace = !mCurrentFace;
            }
            getEnemy()->setAction(GameModel::Attack);
        });
    }else{
        if(mShouldFlipX){
            getEnemy()->setFace(mCurrentFace);
            mCurrentFace = !mCurrentFace;
        }
        getEnemy()->setAction(GameModel::Attack);
    }
}

void AttackBehaviour::onAttack()
{
    getEnemy()->setAction(GameModel::Idle);

    mStartCountDown = true;
}

void AttackBehaviour::updateProperty()
{
    if(mData == nullptr) {
        log("AttackBehaviour::updateProperty :data is null");
        return;
    }
    
    std::map<std::string, std::string> property = mData->getMapProperty("attackSetting");
	
	if(property.find("cooldown") == property.end()) {	// not found
		mCoolDownTime = 3.0f;
	} else {
		mCoolDownTime = aurora::StringHelper::parseFloat(property["cooldown"]);
	}
	
    mShouldFlipX = property["flipX"] == "true" ? true : false;
    mCurrentFace = getEnemy()->isFaceLeft();
    mShowAlert = property["alert"] == "true" ? true : false;
    
}

std::string AttackBehaviour::toString()
{
    return "AttackBehaviour";
}

void AttackBehaviour::onApplyEffectStatus(EffectStatus status)
{
    if(status == EffectStatusTimestop){
        if(mStartCountDown){
            return;
        }
        getEnemy()->setAction(GameModel::Idle);
    }
}

void AttackBehaviour::onRemoveEffectStatus(EffectStatus status)
{
    if(status == EffectStatusTimestop){
        if(mStartCountDown){
            return;
        }
        startAttack();
    }
}

