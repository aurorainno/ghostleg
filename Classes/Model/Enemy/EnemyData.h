//
//  EnemyData.hpp
//  GhostLeg
//
//  The Data Structure defining the enemy behaviour setting
//
//  Created by Ken Lee on 31/5/2016.
//
//

#ifndef EnemyData_hpp
#define EnemyData_hpp

#include <stdio.h>
#include <string>
#include <map>
#include "cocos2d.h"

#include "EnemyBehaviour.h"
#include "CommonMacro.h"
#include "CommonType.h"

USING_NS_CC;

class EnemyData : public Ref
{
public:
	static std::string tagToString(const ObjectTag &tag);
	static ObjectTag parseTagStr(const std::string &tag);
	
public:
	CREATE_FUNC(EnemyData);
	
	EnemyData();
	
    CC_SYNTHESIZE(bool, mDestroyable, Destroyable);
	CC_SYNTHESIZE(int, mType, Type);
	CC_SYNTHESIZE(int, mRes, Res);	
	CC_SYNTHESIZE(AlertType, mAlertType, AlertType);
	CC_SYNTHESIZE(EnemyBehaviour::BehaviourType, mBehaviour, Behaviour);
	CC_SYNTHESIZE(EnemyBehaviour::ActiveCondition, mActiveCond, ActiveCond);
	CC_SYNTHESIZE(float, mActiveCondArg1, ActiveCondArg1);
    CC_SYNTHESIZE(float, mAccelerX, AccelerX);
    CC_SYNTHESIZE(float, mAccelerY, AccelerY);
	CC_SYNTHESIZE(ObjectTag, mTag, Tag);
	CC_SYNTHESIZE(HitboxType, mHitboxType, HitboxType);
	
	
	SYNTHESIZE_BOOL(mIsTrap, Trap);
	SYNTHESIZE_BOOL(mIsFreeMove, FreeMove);				// -> isFreeMove, setFreeMove
	SYNTHESIZE_BOOL(mHideAtStart, HideAtStart);
	
	const std::vector<float> &getSpeedXArray() const;
	const std::vector<float> &getSpeedYArray() const;
	
	
	std::string toString();
	
	// Extra Property Getter / Setter
	std::string getProperty(const std::string &key);
    std::vector<std::string> getSfx(const std::string &key) const;
	const std::map<std::string, std::string> &getMapProperty(const std::string &key) ;
	void setProperty(const std::string &key, const std::string &value);
	void setMapProperty(const std::string &key, const std::map<std::string, std::string> &mapValue);
    void setSfxMap(const std::map<std::string, std::vector<std::string>> &sfxMap);
	

	
	void setSpeedXArray(const std::vector<float> &speedArray);
	void setSpeedYArray(const std::vector<float> &speedArray);
	
	bool init() { return true; } ;		// Make CREATE macro workable

	void parse(const char *dataLine);
	
	Vec2 getSpeedVec();
	float getSpeedX();
	float getSpeedY();
	
	
	
	
private:
	std::vector<float> mSpeedXArray;
	std::vector<float> mSpeedYArray;
	
	std::map<std::string, std::string> mStrProperty;
	std::map<std::string, std::map<std::string, std::string>> mMapProperty;
    std::map<std::string, std::vector<std::string>> mSfxMap;
};


#endif /* EnemyData_hpp */
