//
//  NormalBehaviour.hpp
//  GhostLeg
//
//  Created by Ken Lee on 31/5/2016.
//
//

#ifndef NormalBehaviour_hpp
#define NormalBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class NormalBehaviour : public EnemyBehaviour
{
public:
	CREATE_FUNC(NormalBehaviour);
	
	NormalBehaviour();
	
	bool init() { return true; }

	std::string toString();
	
public:
	virtual void onCreate();
	virtual void onVisible();
	virtual void onActive();
	virtual void onUpdate(float delta);

	virtual bool onCollidePlayer(Player *player);
};



#endif /* NormalBehaviour_hpp */
