//
//  NormalBehaviour.cpp
//  GhostLeg
//
//  Created by Ken Lee on 31/5/2016.
//
//

#include "NormalBehaviour.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "GameWorld.h"
#include "GameSound.h"


NormalBehaviour::NormalBehaviour()
: EnemyBehaviour()
{
	
}

void NormalBehaviour::onCreate()
{
	if(mData->isHideAtStart() == false) {
		getEnemy()->changeToIdle();
	}
}

void NormalBehaviour::onVisible()
{
	if(!getEnemy()->getSoundPlayed()) {
//		GameSound::playEnemyAppearSound(getEnemy()->getResID());
	}
}


void NormalBehaviour::onActive()
{
	//
	// log("onActive!");
}


void NormalBehaviour::onUpdate(float delta)
{
	// log("onUpdate!");
	
	Enemy *enemy = getEnemy();
	if(enemy == nullptr) {
		return;
	}
	
	if(enemy->getState() == Enemy::State::Active)
	{
		handleMove(delta);
	}
}

bool NormalBehaviour::onCollidePlayer(Player *player)
{
	Enemy *enemy = getEnemy();	// Error handling
	if(enemy == nullptr) {
		return false;
	}
	
	return EnemyBehaviour::onCollidePlayer(player);
}


std::string NormalBehaviour::toString()
{
	return "NormalEnemyBehaviour";
}
