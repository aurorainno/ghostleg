//
//  MoveUpBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef DropStuffBehaviour_hpp
#define DropStuffBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class DropStuffBehaviour : public EnemyBehaviour
{
public:
	CREATE_FUNC(DropStuffBehaviour);

	DropStuffBehaviour();

	bool init() { return true; }
	

	std::string toString();

public:
	virtual void onCreate();
	virtual void onVisible();
	virtual void onActive();
	virtual void onUpdate(float delta);

	
private:
	void dropStarLogic(float delta);
	void updateProperty();
	
private:
	// count=2,interval=1,item=star,start=topY:-100
	int mStartDropPosY;		// the condition start droping (e.g myPosY > mapBottom + startDropY
	int mDropCount;			// Number of things will dropped
	float mDropInterval;	// Time Elapse between two drop
    float mCounter;
	std::string mDropItem;	//
	
	
	bool mHasDrop;
};



#endif /* NormalBehaviour_hpp */
