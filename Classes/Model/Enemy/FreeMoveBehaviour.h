//
//  FreeMoveBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef FreeMoveBehaviour_hpp
#define FreeMoveBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class FreeMoveBehaviour : public EnemyBehaviour
{
public:
	CREATE_FUNC(FreeMoveBehaviour);

	FreeMoveBehaviour();

	bool init() { return true; }

	std::string toString();

public:
	virtual void onCreate();
	virtual void onVisible();
	virtual void onActive();
	virtual void onUpdate(float delta);
};



#endif /* NormalBehaviour_hpp */
