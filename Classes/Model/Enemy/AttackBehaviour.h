//
//  AttackBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 23/2/2017.
//
//

#ifndef AttackBehaviour_hpp
#define AttackBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class AttackBehaviour : public EnemyBehaviour
{
public:
    CREATE_FUNC(AttackBehaviour);
    
    AttackBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onCreate();
    virtual void onVisible();
    virtual void onActive();
    virtual void onUpdate(float delta);
    void onApplyEffectStatus(EffectStatus status) override;
    void onRemoveEffectStatus(EffectStatus status) override;
    
    void startAttack();
    void onAttack();
    void updateProperty();
    
    bool isColliable();
    
private:
    float mAccumTime;
    float mCoolDownTime;
    bool mStartCountDown;
    bool mShouldFlipX;
    bool mCurrentFace;
    bool mShowAlert;
    
    
};



#endif /* AttackBehaviour_hpp */
