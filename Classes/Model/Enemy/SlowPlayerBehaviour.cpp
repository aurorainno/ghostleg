//
//  SlowPlayerBehaviour.cpp
//  GhostLeg
//
//  The Code is generated by createBehaviour.sh
//
//

#include "SlowPlayerBehaviour.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "GameWorld.h"
#include "GameSound.h"
#include "Player.h"

SlowPlayerBehaviour::SlowPlayerBehaviour()
: NormalBehaviour()
{

}

std::string SlowPlayerBehaviour::toString()
{
	return "SlowPlayerBehaviour";
}

bool SlowPlayerBehaviour::onCollidePlayer(Player *player)
{
	Enemy *enemy = getEnemy();	// Error handling
	if(enemy == nullptr) {
		return false;
	}
	
	// core logic
	bool isInvulnerable = player->isInvulnerable();
	bool hasShield = player->hasShield();
	if(isInvulnerable || hasShield) {	// nothing happened
		return false;
	}
	
	// handle the enemy
	enemy->beHit();	// Vec2(0, 0));	// define some effect
	enemy->setVisible(false);
	
	// handle the player
	player->setEffectStatus(EffectStatus::EffectStatusSlow, 3);	// 3 second slow
	
	
	return false;	// player not die
}
