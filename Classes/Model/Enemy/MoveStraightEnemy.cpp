//
//  MoveStraightEnemy.cpp
//  GhostLeg
//
//  Created by Ken Lee on 24/3/2016.
//
//

#include "MoveStraightEnemy.h"


MoveStraightEnemy::MoveStraightEnemy()
{
	
}

bool MoveStraightEnemy::init()
{
	bool isOkay = Enemy::init();
	if(isOkay == false) {
		return false;
	}
	
	
	return true;
}


void MoveStraightEnemy::move(float delta)
{
	Vec2 newPos = getNewPosition(delta);
	
	setPosition(newPos);
}