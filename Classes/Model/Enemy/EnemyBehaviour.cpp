//
//  EnemyBehaviour.cpp
//  GhostLeg
//
//  Created by Ken Lee on 31/5/2016.
//
//

#include "EnemyBehaviour.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "GameWorld.h"
#include "Player.h"
#include "Item.h"
#include "StringHelper.h"
#include "EffectLayer.h"

const float kUpperInactiveOffset = 3000;		//	should be more than map height!!
const float kLowerInactiveOffset = 500;

#pragma mark - Helper Method
// Helper method
EnemyBehaviour::BehaviourType EnemyBehaviour::getBehaviourType(const std::string typeStr)
{
	if("normal" == typeStr) {
		return EnemyBehaviour::BehaviourType::Normal;
	} else if("obstacle" == typeStr) {
		return EnemyBehaviour::BehaviourType::Obstacle;
	} else if("freeMove" == typeStr) {
		return EnemyBehaviour::BehaviourType::FreeMove;
	} else if("dropStuff" == typeStr) {
		return EnemyBehaviour::BehaviourType::DropStuff;
	} else if("autoHide" == typeStr) {
		return EnemyBehaviour::BehaviourType::AutoHide;
    } else if("reduceEnergy" == typeStr){
        return EnemyBehaviour::BehaviourType::ReduceEnergy;
	} else if("slowPlayer" == typeStr){
		return EnemyBehaviour::BehaviourType::SlowPlayer;
	} else if("thief" == typeStr){
		return EnemyBehaviour::BehaviourType::Thief;
    } else if("patrol" == typeStr){
        return EnemyBehaviour::BehaviourType::Patrol;
    } else if("attack" == typeStr){
        return EnemyBehaviour::BehaviourType::Attack;
	} else if("suddenMove" == typeStr){
		return EnemyBehaviour::BehaviourType::SuddenMove;
    } else if("jump" == typeStr){
        return EnemyBehaviour::BehaviourType::Jump;
    } else if("fireBullet" == typeStr){
        return EnemyBehaviour::BehaviourType::FireBullet;
    }  else if("bullet" == typeStr){
        return EnemyBehaviour::BehaviourType::Bullet;
    }  else if("bomber" == typeStr){
        return EnemyBehaviour::BehaviourType::Bomber;
    }  else if("chaser" == typeStr){
        return EnemyBehaviour::BehaviourType::Chaser;
    }  else{
		return EnemyBehaviour::BehaviourType::Normal;
	}
}



void EnemyBehaviour::parseActiveCondition(EnemyData *enemyData, const std::string typeStr)
{
	if(typeStr == "") {
		enemyData->setActiveCond(EnemyBehaviour::ActiveCondition::OnStart);
		enemyData->setActiveCondArg1(0);
		return;
	}
	
	std::vector<std::string> token = aurora::StringHelper::split(typeStr, ':');
	
	std::string cond = token[0];
	std::string argStr = token.size() <= 1 ? "0" : token[1];
	
	// log("debug: cond=%s arg=%s", cond.c_str(), argStr.c_str());
	
	// Condition Type
	EnemyBehaviour::ActiveCondition condType;
	if("topY" == cond) {
		condType = EnemyBehaviour::ActiveCondition::TopY;
	} else if("bottomY" == cond) {
		condType = EnemyBehaviour::ActiveCondition::BottomY;
	} else if("player" == cond) {
		condType = EnemyBehaviour::ActiveCondition::PlayerDistance;
	} else if("playerX" == cond) {
		condType = EnemyBehaviour::ActiveCondition::PlayerXDistance;
	} else if("playerY" == cond) {
		condType = EnemyBehaviour::ActiveCondition::PlayerYDistance;
	} else {
		condType = EnemyBehaviour::ActiveCondition::OnStart;
	}
	
	float arg1 = aurora::StringHelper::parseFloat(argStr);
	
	
	// Set to Enemy Data
	enemyData->setActiveCond(condType);
	enemyData->setActiveCondArg1(arg1);
}

#pragma mark - Object Definition

EnemyBehaviour::EnemyBehaviour()
: mEnemy(nullptr)
, mSpeed(Vec2(0, 0))
{
	
}

Vec2 EnemyBehaviour::getPosition()
{
	if(mEnemy == nullptr) {
		return Vec2(0, 0);
	}
	
	return mEnemy->getPosition();
}

float EnemyBehaviour::getInfo(EnemyBehaviour::InfoType type)
{
	Vec2 myPos = getPosition();
	
	switch(type) {
		case InfoType::InfoTypeDistanceToPlayer:
			return GameWorld::instance()->getDistanceToPlayer(myPos);
		
		case InfoType::InfoTypeVertialDistToPlayer:
			return GameWorld::instance()->getYDistanceToPlayer(myPos);
			
		case InfoType::InfoTypeHorizontalDistToPlayer:
			return GameWorld::instance()->getXDistanceToPlayer(myPos);
		
		case InfoType::InfoTypeMapTopY:
			return GameWorld::instance()->getTopMapY();
			
		case InfoType::InfoTypeMapBottomY:
			return GameWorld::instance()->getBottomMapY();
			
		default:
			return 0;
	}
	
	
	return 0;
}

bool EnemyBehaviour::isActiveConditionMatch()
{
	if(mData == nullptr) {
		return true;
	}
	
	Vec2 pos = getPosition();
	
	switch(mData->getActiveCond()) {
		case TopY: {
			float offset = getInfo(InfoType::InfoTypeMapTopY) + mData->getActiveCondArg1();
			return pos.y < offset;
		}
			
		case BottomY: {
			float offset = getInfo(InfoType::InfoTypeMapBottomY) + mData->getActiveCondArg1();
			return pos.y < offset;
		}
			
		case PlayerDistance: {
			float dist = getInfo(InfoType::InfoTypeDistanceToPlayer);
			
			return dist < mData->getActiveCondArg1();
		}

		case PlayerXDistance: {
			float dist = getInfo(InfoType::InfoTypeHorizontalDistToPlayer);
			
			return dist < mData->getActiveCondArg1();
		}

		case PlayerYDistance: {
			float dist = getInfo(InfoType::InfoTypeVertialDistToPlayer);
			
			return dist < mData->getActiveCondArg1();
		}

		case OnStart: {
			return true;
		}			
	}
	
	return false;
}

const EnemyData *EnemyBehaviour::getData() const
{
	return mData;
}

void EnemyBehaviour::setData(EnemyData *data)	// note: this is a weak reference to EnemyData
												// but it is owned by EnemyFactory Singleton
{
	mData = data;
	if(mData) {
        setAcceler(Vec2(mData->getAccelerX(),mData->getAccelerY()));
		Vec2 speedVec = mData->getSpeedVec();		
        setSpeed(speedVec);
	}
}


bool EnemyBehaviour::shouldActive()
{
	return isActiveConditionMatch();		// default implemenation
}

bool EnemyBehaviour::shouldInActive()
{
	Vec2 currentPos = getPosition();
	
	// getCameraY return the y of map bottom position;
	float bottomMargin = GameWorld::instance()->getCameraY() - kLowerInactiveOffset;		// For move down monster
	float topMargin = GameWorld::instance()->getTopMapY() + kUpperInactiveOffset;		// For move up monster
	
	return currentPos.y < bottomMargin || currentPos.y > topMargin;
}

void EnemyBehaviour::onInActive()
{
	//log("onInActive");
}

std::string EnemyBehaviour::toString()
{
	return "BaseEnemyBehaviour";
}

//float &getInfo(InfoType type) const;


float EnemyBehaviour::getSpeedX()
{
	return mSpeed.x;
}

float EnemyBehaviour::getSpeedY()
{
	return mSpeed.y;
}

int EnemyBehaviour::getRes()
{
	return mData ? mData->getRes() : 1;
}

AlertType EnemyBehaviour::getAlertType(){
    return mData->getAlertType();
}


std::string EnemyBehaviour::infoAttribute()
{
	std::string result = "";
	
	result = "speed=" + POINT_TO_STR(getSpeed());
	result += " alert=" + INT_TO_STR(getAlertType());
	result += " res=" + INT_TO_STR(getRes());
	result += " data[=" + mData->toString() + "]";
	
	
	return result;
}



void EnemyBehaviour::handleMove(float delta)
{
	Enemy *enemy = getEnemy();
	if(enemy == nullptr) {
		return;
	}
	
	
	if(getData()->isFreeMove()) {
		enemy->moveFree(delta);
	} else {
		enemy->move(delta);
	}
}


// General Logic
bool EnemyBehaviour::onCollidePlayer(Player *player)
{
	Enemy *enemy = getEnemy();	// Error handling
	if(enemy == nullptr) {
		return false;
	}
	
	// core logic
	bool isInvulnerable = player->isInvulnerable();
	//bool hasShield = player->hasShield();
//    if(isInvulnerable){
//        return false;
//    }
	
	// the enemy is a trap that can't be destroy by player
	if(isInvulnerable && getData()->getDestroyable() && !getEnemy()->isObstacle()) {
		// Enemy being hit
		enemy->beHit();
        
        if(player->getEffectStatus() == EffectStatusInvulnerable){
            GameWorld::instance()->getEffectLayer()->playCsbAnime("particle_invshield_hit_2.csb", "active", false, true, 1.0f, enemy->getPosition());
        }
		
		Vec2 force = Vec2(0, player->getSpeedY());
		enemy->showKnockoutEffect(force);
        
		
		return false;
	}
	
	return player->handleBeingHitted();
}

void EnemyBehaviour::onApplyEffectStatus(EffectStatus status)
{
    //log("onApplyEffectStatus: %d",(int)status);
}

void EnemyBehaviour::onRemoveEffectStatus(EffectStatus status)
{
   //  log("onRemoveEffectStatus: %d",(int)status);
}

bool EnemyBehaviour::onCollideItem(Item *item)
{
	log("EnemyBehaviour::onCollideItem: item collided=%s", item->toString().c_str());
	return false;
}

bool EnemyBehaviour::isStatic()
{
	return false;
}
