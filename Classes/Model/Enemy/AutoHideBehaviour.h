//
//  AutoHideBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef AutoHideBehaviour_hpp
#define AutoHideBehaviour_hpp

#include <stdio.h>
#include "NormalBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class AutoHideBehaviour : public NormalBehaviour
{
public:
	CREATE_FUNC(AutoHideBehaviour);

	AutoHideBehaviour();

	bool init() { return true; }

	std::string toString();

public:
	virtual void onCreate();
	virtual void onVisible();
	virtual void onActive();
};



#endif /* NormalBehaviour_hpp */
