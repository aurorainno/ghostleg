//
//  MoveStraightEnemy.hpp
//  GhostLeg
//
//  Created by Ken Lee on 24/3/2016.
//
//

#ifndef MoveStraightEnemy_hpp
#define MoveStraightEnemy_hpp

#include <stdio.h>

//
//  Enemy.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2016.
//

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "Enemy.h"

USING_NS_CC;

class MoveStraightEnemy : public Enemy
{
	
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(MoveStraightEnemy);
	
#pragma mark - Public Method and Properties
	MoveStraightEnemy();
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual void move(float delta);
};




#endif /* MoveStraightEnemy_hpp */
