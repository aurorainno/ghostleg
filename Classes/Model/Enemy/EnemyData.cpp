//
//  EnemyData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 31/5/2016.
//
//

#include "EnemyData.h"
#include "StringHelper.h"
#include "RandomHelper.h"

namespace  {
		
}



std::string EnemyData::tagToString(const ObjectTag &tag)
{
	switch(tag) {
		case ObjectObstacle		: return "obstacle";
		case ObjectEnemy		: return "enemy";
		case ObjectLaser		: return "laser";
		case ObjectBullet		: return "bullet";
		case ObjectBomb			: return "bomb";
		default					: return "unknown";
	}
}

ObjectTag EnemyData::parseTagStr(const std::string &tag)
{
	if("obstacle" == tag) {
		return ObjectObstacle;
	}
	
	if("enemy" == tag) {
		return ObjectEnemy;
	}
	
	if("laser" == tag) {
		return ObjectLaser;
	}
	
	if("bullet" == tag) {
		return ObjectBullet;
	}
	
	if("bomb" == tag) {
		return ObjectBomb;
	}
	
	return ObjectUnknown;
}


//CC_SYNTHESIZE(int, mType, Type);
//CC_SYNTHESIZE(int, mRes, Res);
//CC_SYNTHESIZE(EnemyBehaviour::BehaviourType, mBehaviour, Behaviour);
//CC_SYNTHESIZE(float, mSpeedX, SpeedX);
//CC_SYNTHESIZE(float, mSpeedY, SpeedY);
//CC_SYNTHESIZE(EnemyBehaviour::ActiveCondition, mActiveCond, ActiveCond);
//CC_SYNTHESIZE(float, mActiveCondArg1, ActiveCondArg1);
//SYNTHESIZE_BOOL(mIsFreeMove, FreeMove);	// -> isFreeMove, setFreeMove

EnemyData::EnemyData()
: mType(0)
, mRes(0)
, mBehaviour(EnemyBehaviour::BehaviourType::Normal)
, mActiveCond(EnemyBehaviour::ActiveCondition::OnStart)
, mActiveCondArg1(0)
, mIsFreeMove(false)
, mIsTrap(false)
, mDestroyable(true)
, mSpeedXArray()
, mSpeedYArray()
, mHideAtStart(false)
, mStrProperty()
, mMapProperty()
, mHitboxType(HitboxDynamic)
{
	
}

const std::vector<float> &EnemyData::getSpeedXArray() const
{
	return mSpeedXArray;
}

const std::vector<float> &EnemyData::getSpeedYArray() const 
{
	return mSpeedYArray;
}

void EnemyData::setSpeedXArray(const std::vector<float> &speedArray)
{
	mSpeedXArray = speedArray;
}

void EnemyData::setSpeedYArray(const std::vector<float> &speedArray)
{
	mSpeedYArray = speedArray;
}



void EnemyData::parse(const char *dataLine)
{
	int type;
	int resID;
	char behaviourStr[256];
	char activeCondStr[256];
	float speedX, speedY;
	
	sscanf(dataLine, "type=%d res=%d behaviour=%s speedX=%f speedY=%f active=%s",
		                  &type, &resID, behaviourStr,
			              &speedX, &speedY, activeCondStr);
	
	//
	setBehaviour(EnemyBehaviour::getBehaviourType(behaviourStr));
	
	// Active Condition
	EnemyBehaviour::parseActiveCondition(this, activeCondStr);

	mSpeedXArray.push_back(speedX);
	mSpeedYArray.push_back(speedY);
	setType(type);
	setRes(resID);
}

float EnemyData::getSpeedX()
{
	int size = (int) mSpeedXArray.size();
	
	if(size == 0) {
		return 0;
	} else if(size == 1) {
		return mSpeedXArray[0];
	} else {
		// Random value
		int idx = aurora::RandomHelper::randomBetween(0, size-1);
		return mSpeedXArray[idx];
	}
}

float EnemyData::getSpeedY()
{
	int size = (int) mSpeedYArray.size();
	
	if(size == 0) {
		return 0;
	} else if(size == 1) {
		return mSpeedYArray[0];
	} else {
		// Random value
		int idx = aurora::RandomHelper::randomBetween(0, size-1);
		return mSpeedYArray[idx];
	}
}

Vec2 EnemyData::getSpeedVec()
{
	int size = (int) mSpeedXArray.size();
	if(size == 0) {
		return Vec2(0, getSpeedY());
	}
	
	int idx = aurora::RandomHelper::randomBetween(0, size-1);
	
	float speedX = mSpeedXArray.size() <= idx ? 0 : mSpeedXArray[idx];
	float speedY = mSpeedYArray.size() <= idx ? 0 : mSpeedYArray[idx];
    
	
	return Vec2(speedX, speedY);
}

void EnemyData::setSfxMap(const std::map<std::string, std::vector<std::string> > &sfxMap)
{
    mSfxMap = sfxMap;
}

void EnemyData::setProperty(const std::string &key, const std::string &value)
{
	mStrProperty[key] = value;
}

void EnemyData::setMapProperty(const std::string &key, const std::map<std::string, std::string> &mapValue)
{
	log("debug: key=%s map=%s type=%d", key.c_str(), aurora::StringHelper::mapToString(mapValue).c_str(), mType);
	mMapProperty[key] = mapValue;
}

std::string EnemyData::getProperty(const std::string &key)
{
	std::string result = mStrProperty[key];
	
	return result;
}

std::vector<std::string> EnemyData::getSfx(const std::string &key) const
{
    std::vector<std::string> list{};
    if(mSfxMap.find(key) == mSfxMap.end()){
        return list;
    }else{
        list = mSfxMap.at(key);
        return list;
    }
}



const std::map<std::string, std::string> &EnemyData::getMapProperty(const std::string &key)
{
	return mMapProperty[key];
}



std::string EnemyData::toString()
{
	std::string result = "";
	
	result += "type=" + INT_TO_STR(getType());
	result += " res=" + INT_TO_STR(getRes());
	result += " behaviour=" + INT_TO_STR(getBehaviour());
	result += " speedX=" + FLOAT_TO_STR(getSpeedX());
	result += " speedY=" + FLOAT_TO_STR(getSpeedY());
	result += " activeCond=" + INT_TO_STR(getActiveCond());
	result += " activeCondArg1=" + FLOAT_TO_STR(getActiveCondArg1());
	result += " speedX=[" + VECTOR_TO_STR(getSpeedXArray()) + "]";
	result += " speedY=[" + VECTOR_TO_STR(getSpeedYArray()) + "]";
	result += " tag=" + EnemyData::tagToString(getTag());
	result += " destroyable=" + BOOL_TO_STR(getDestroyable());
	result += " isFreeMove=" + BOOL_TO_STR(isFreeMove());
	result += " isTrap=" + BOOL_TO_STR(isTrap());
    
    std::string alertTypeString[] = {"NoAlert","ScreenAlert","ModelAlert"};
    result += " alertType=[" + alertTypeString[getAlertType()] + "]";
	
	// Special property
	std::map<std::string, std::string> property;
	
	property = getMapProperty("dropSetting");
	result += " dropSetting=[" + aurora::StringHelper::mapToString(property) + "]";
	
	return result;
}


