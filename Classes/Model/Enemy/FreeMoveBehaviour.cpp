//
//  FreeMoveBehaviour.cpp
//  GhostLeg
//
//  The Code is generated by createBehaviour.sh
//
//

#include "FreeMoveBehaviour.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "GameWorld.h"
#include "GameSound.h"
#include "GameScene.h"

namespace {
	Vec2 calcNewPosition(const Vec2& oldPos, const float speedX, const float speedY, float delta) {
		Vec2 pos = oldPos;
		
		pos.x += delta * speedX;
		pos.y += delta * speedY;
		
		return pos;
	}
}

FreeMoveBehaviour::FreeMoveBehaviour()
: EnemyBehaviour()
{

}

void FreeMoveBehaviour::onCreate()
{
	getEnemy()->changeToIdle();
}

void FreeMoveBehaviour::onVisible()
{
	if(!getEnemy()->getSoundPlayed()) {
//		GameSound::playEnemyAppearSound(getEnemy()->getResID());
	}
}


void FreeMoveBehaviour::onActive()
{
//    float posX = getEnemy()->getPositionX();
//
//    log("onActive! posX: %.0f",posX);
//    GameWorld::instance()->getGameUILayer()->showCaution(posX);
}


void FreeMoveBehaviour::onUpdate(float delta)
{
	// log("onUpdate!");

	Enemy *enemy = getEnemy();
	if(enemy == nullptr) {
		return;
	}

	if(enemy->getState() == Enemy::State::Active) {
		enemy->moveFree(delta);

		// enemy->move(delta);
//		Vec2 speed = enemy->getFinalSpeed();
//		Vec2 newPos = calcNewPosition(getPosition(), speed.x, speed.y, delta);
//		
//		enemy->setPosition(newPos);
		
		
	}
}

std::string FreeMoveBehaviour::toString()
{
	return "FreeMoveBehaviour";
}
