//
//  ObstacleBehaviour.hpp
//  GhostLeg
//
//  Created by createBehaviour.sh
//
//

#ifndef ObstacleBehaviour_hpp
#define ObstacleBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class ObstacleBehaviour : public EnemyBehaviour
{
public:
	CREATE_FUNC(ObstacleBehaviour);

	ObstacleBehaviour();

	bool init() { return true; }

	std::string toString();
	virtual bool isStatic();

public:
	virtual void onCreate();
	virtual void onVisible();
	virtual void onActive();
	virtual void onUpdate(float delta);
};



#endif /* NormalBehaviour_hpp */
