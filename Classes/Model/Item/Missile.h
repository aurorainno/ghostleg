//
//  Missile.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/6/2016.
//
//

#ifndef Missile_hpp
#define Missile_hpp

#include <stdio.h>
#include "Item.h"
#include "Enemy.h"

class Missile : public Item
{
public:
    enum MissileType{
        Homing,
        RandomEmit
    };
    
#pragma mark - Public static Method
	CREATE_FUNC(Missile);
	
#pragma mark - Public Method and Properties
	Missile();
	~Missile();

	CC_SYNTHESIZE_RETAIN(Enemy *, mTargetEnemy, TargetEnemy);
	CC_SYNTHESIZE(float, mVelocity, Velocity);
    CC_SYNTHESIZE(float, mAcceler, Acceler);
	CC_SYNTHESIZE(float, mMaxVelocity, MaxVelocity);
    CC_SYNTHESIZE(int, mID, ID);
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();
	virtual void update(float delta);
    virtual void onEnter();
	virtual bool isWeapon();
	
    void setMissileType(MissileType type);
	
private:
	bool shouldRemove();
	void handleCollision(Enemy* enemy = nullptr);
	void updateVelocity(float delta);
	
private:
	bool mHasHit;
    float mPreviousAngle;
    MissileType mType;
};
#endif /* Missile_hpp */
