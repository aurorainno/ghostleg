//
//  MagnetCapsule.hpp
//  GhostLeg
//
//  Created by Ken Lee on 17/1/2017.
//
//

#ifndef MagnetCapsule_hpp
#define MagnetCapsule_hpp

#include <stdio.h>

#include "Item.h"


class MagnetCapsule : public Item
{
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(MagnetCapsule);
	
	static const std::string kCsbFile;
	
#pragma mark - Public Method and Properties
	MagnetCapsule();
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();		// use when the Astrodog hit this GameMap Elements
    virtual bool shouldBeCleanUp();
};


#endif /* MagnetCapsule_hpp */
