//
//  SpeedUpCapsule.hpp
//  GhostLeg
//
//  Created by Ken Lee on 17/1/2017.
//
//

#ifndef SpeedUpCapsule_hpp
#define SpeedUpCapsule_hpp

#include <stdio.h>
#include "Item.h"


class SpeedUpCapsule : public Item
{
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(SpeedUpCapsule);
	static const std::string kCsbFile;
	
#pragma mark - Public Method and Properties
	SpeedUpCapsule();
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();		// use when the Astrodog hit this GameMap Elements
};


#endif /* SpeedUpCapsule_hpp */
