//
//  ItemPuzzle.cpp
//  GhostLeg
//
//  Created by Ken Lee on 8/5/2017.
//
//

#include "ItemPuzzle.h"
#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"


ItemPuzzle::ItemPuzzle()
: Item(Item::Type::ItemPuzzle)
{
	
}


bool ItemPuzzle::init()
{
	if(Item::init() == false) {
		log("ItemPuzzle: init failed");
		return false;
	}
	setScale(0.8);
	setup("item/item_puzzle.csb");	// note: default Action is idle
	return true;
}

bool ItemPuzzle::use()
{
	if(Item::use() == false) {
		return false;
	}
	
	// Make sound effect
//	GameSound::playSound(GameSound::Unlock);
    GameSound::playSound(GameSound::CollectPuzzle);
	GameSound::playSound(GameSound::VoiceLaugh);
	

	log("Puzzle is collected"); 
	// Core Logic
	GameWorld::instance()->collectPuzzle();
	
	
	return true;
}

void ItemPuzzle::update(float delta)
{
}
