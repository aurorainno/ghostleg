//
//  ShieldCapsule.hpp
//  GhostLeg
//
//  Created by Ken Lee on 17/1/2017.
//
//

#ifndef ShieldCapsule_hpp
#define ShieldCapsule_hpp

#include <stdio.h>
#include "Item.h"


class ShieldCapsule : public Item
{
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(ShieldCapsule);
	
#pragma mark - Public Method and Properties
	ShieldCapsule();
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();		// use when the Astrodog hit this GameMap Elements
};


#endif /* ShieldCapsule_hpp */
