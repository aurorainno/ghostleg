//
//  PackageBox.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#include "PackageBox.h"
PackageBox::PackageBox()
: Item(Item::Type::ItemPackageBox)
{
	
}

bool PackageBox::init()
{
	if(Item::init() == false) {
		log("Capsule: init failed");
		return false;
	}
	
	setPackage(1);
	
	return true;
}

void PackageBox::setPackage(int packageType)
{
	std::string csbName = StringUtils::format("package/package_%03d.csb", packageType);
	setup(csbName);
	
	//setAction(<#GameModel::Action action#>);
}

void PackageBox::showAnimation(Animation animation, std::function<void()> endCallback)
{
	std::string animeName = getAnimationName(animation);
	bool isLooping = isAnimationLooping(animation);
	
	if(isLooping == false && endCallback != nullptr) {
		setAnimationEndCallFunc(animeName, endCallback);
	}
	getTimeLine()->setTimeSpeed(1.5);		// value higher -> play faster
	runAnimation(animeName, isLooping);
}

std::string PackageBox::getAnimationName(Animation animation)
{
	switch (animation) {
		case Animation::Disappear	: return "disappear";
		case Animation::DropDown	: return "dropdown";
		case Animation::MovingUp	: return "moving";
		case Animation::MovingSide	: return "movingside";
        case Animation::MonsterChase: return "MonsterChase";
		default						: return "";
	}
}

bool PackageBox::isAnimationLooping(Animation animation)
{
    if(Animation::Disappear == animation || Animation::DropDown == animation || Animation::MonsterChase == animation) {
		return false;
	} else {
		return true;
	}
}
