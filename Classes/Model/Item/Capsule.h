//
//  Capsule.hpp
//  GhostLeg
//
//  Created by Ken Lee on 18/6/2016.
//
//

#ifndef Capsule_hpp
#define Capsule_hpp

#include <stdio.h>
#include "Item.h"
#include "ItemEffect.h"

class Capsule : public Item
{
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(Capsule);
	
#pragma mark - Public Method and Properties
	Capsule();
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();
	
	CC_SYNTHESIZE(float, mItemEffectValue, ItemEffectValue);
	CC_SYNTHESIZE(int, mLevel, Level);
	
	CC_SYNTHESIZE_READONLY(ItemEffect::Type, mItemEffectType, ItemEffectType);
public:
	void setItemEffectType(ItemEffect::Type effectType);
};
#endif /* Capsule_hpp */
