//
//  Cashout.hpp
//  GhostLeg
//
//  Created by Calvin on 6/12/2016.
//
//

#ifndef Cashout_hpp
#define Cashout_hpp

#include <stdio.h>
#include "Item.h"


class Cashout : public Item
{
public:
    
#pragma mark - Public static Method
    CREATE_FUNC(Cashout);
	static const std::string kCsbFile;
    
#pragma mark - Public Method and Properties
    Cashout();
    
    virtual bool init();	// virtual because different derived class have different init logic
    virtual bool use();		// use when the Astrodog hit this GameMap Elements
};

#endif /* Cashout_hpp */
