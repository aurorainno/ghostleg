//
//  Missile.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/6/2016.
//
//

#include "Missile.h"

#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"
#include "RandomHelper.h"
#include "ViewHelper.h"
#include "ModelLayer.h"
#include "GeometryHelper.h"
#include "StringHelper.h"
#include "EffectLayer.h"
#include "GameSound.h"
#include "ItemManager.h"
#include "AttackBehaviour.h"

const int kBulletID = 80;

Missile::Missile()
: Item(Item::Type::ItemMissile)
, mVelocity(200)
, mAcceler(250)
, mHasHit(false)
, mTargetEnemy(nullptr)
{
	
}

Missile::~Missile()
{
	log("Destruction of Missile");
	setTargetEnemy(nullptr);
}


bool Missile::init()
{
	if(Item::init() == false) {
		log("Missile: init failed");
		return false;
	}
	
	SpeedData speedData = ItemManager::instance()->getMissileSpeedData();
	
	mVelocity = speedData.initialSpeed;
	mAcceler = speedData.acceleration;
	mMaxVelocity = speedData.maxSpeed;
	
	setup("missile.csb");	// note: default Action is idel

    setScale(0.6);
	return true;
}

bool Missile::use()
{
	return false;
}

void Missile::onEnter()
{
    Node::onEnter();
    GameSound::playSound(GameSound::MissileLoop,true,StringUtils::format("missile%02d",mID));
}


void Missile::handleCollision(Enemy* enemy)
{
    Enemy* targetEnemy = mTargetEnemy;
    
	if(enemy == nullptr && mTargetEnemy == nullptr) {
		return;
    }else if(enemy){
        targetEnemy = enemy;
    }
    
    
	mHasHit = true;
	
	
	
//  Explode Effect
	
//	GameWorld::instance()->getEffectLayer()->showEffect(GameRes::Particle::MissileExplode,
//														getPosition());
	
//  GameWorld::instance()->getEffectLayer()->playCsbAnime("missile_hit.csb", "active", false, true);
    GameWorld::instance()->getEffectLayer()->playCsbAnime("missile_hit.csb", "active", false, true, 0.5,  targetEnemy->getPosition());
	
	// Make the Enemy Move
	float angle = aurora::GeometryHelper::findAngleRadian(getPosition(), targetEnemy->getPosition());
	Vec2 force = aurora::GeometryHelper::resolveVec2(mVelocity * 1.5, angle);
	force.y = fabsf(force.y);		// Move Up
	
	targetEnemy->beHit();
	targetEnemy->showKnockoutEffect(force);
    
    GameSound::playSound(GameSound::MissileHit);

}



void Missile::update(float delta)
{
	updateVelocity(delta);
	
	
	//
	if(mTargetEnemy != nullptr && mType == Homing) {
		//
       
		Vec2 newPos = aurora::GeometryHelper::calculateNewTracePosition(getPosition(),
											mTargetEnemy->getCenterPosition(),
											mVelocity, delta);
		setPosition(newPos);
		
        Vec2 p2 = mTargetEnemy->getCenterPosition();
        Vec2 p1 = getPosition();
        
        float dy = p2.y - p1.y;
        float dx = p2.x - p1.x;
        float angle = atan2f(dy, dx);
        
        angle = CC_RADIANS_TO_DEGREES(angle);
        
        setRotation(-angle);
        log("angle: %.2f",angle);
        
		
		// Collision check
		Rect myBound = getBoundingBox();
		Rect enemyBound = mTargetEnemy->getBoundingBox();
		
		if(myBound.intersectsRect(enemyBound)) {
			handleCollision();
		}
    }else if(mType == RandomEmit){
        float angle = -CC_DEGREES_TO_RADIANS(getRotation());
        Vec2 diff = aurora::GeometryHelper::resolveVec2(delta * mVelocity, angle);
        setPosition(getPosition() + diff);
		
        
        Vector<Enemy*> enemyArray = GameWorld::instance()->getModelLayer()->getEnemyList();
        Rect myBound = getBoundingBox();

        for(Enemy* enemy : enemyArray){
            if(enemy){
                if(enemy->isObstacle()){
                    continue;
                }
                
                if(!enemy->getEnemyData()->getDestroyable()){
                    continue;
                }
                
                if(enemy->getEnemyData()->getType() == kBulletID){
                    continue;
                }
                
                AttackBehaviour* attackBehaviour = dynamic_cast<AttackBehaviour*>(enemy->getBehaviour());
                if(attackBehaviour){
                    if(!attackBehaviour->isColliable()){
                        continue;
                    }
                }
            
                Rect enemyBound = enemy->getBoundingBox();
                if(myBound.intersectsRect(enemyBound)) {
                    handleCollision(enemy);
                }
            }
        }
        
    }
	
	
	
	// Removal Handling
	if(shouldRemove()) {
        GameSound::stopSound(StringUtils::format("missile%02d",mID));
		GameWorld::instance()->getModelLayer()->removeItem(this);
	}
}

// Two scenario:
//		1. already hit the enemy
//		2. Out of the screen
bool Missile::shouldRemove()
{
	if(mHasHit) {
		return true;
	}
	
	Rect myRect = getBoundingBox();
	myRect.size = Size(10, 10);
	
	return GameWorld::instance()->isRectVisible(myRect) == false;
}

void Missile::setMissileType(Missile::MissileType type)
{
    mType = type;
}


void Missile::updateVelocity(float delta)
{
	if(mVelocity >= mMaxVelocity) {
		return;
	}
	
	float newValue = mVelocity + delta * mAcceler;
	if(newValue > mMaxVelocity) {
		newValue = mMaxVelocity;
	}
	
	mVelocity = newValue;
}


bool Missile::isWeapon()
{
	return true;
}
