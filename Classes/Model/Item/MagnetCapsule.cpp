//
//  MagnetCapsule.cpp
//  GhostLeg
//
//  Created by Ken Lee on 17/1/2017.
//
//

#include "MagnetCapsule.h"

#include "GameSound.h"
#include "GameWorld.h"
#include "Player.h"
#include "ItemEffect.h"
#include "AnimationHelper.h"
#include "EffectLayer.h"

const std::string MagnetCapsule::kCsbFile = "item/item_box_magnet.csb";

MagnetCapsule::MagnetCapsule(): Item(Item::Type::ItemMagnet)
{
	
}


bool MagnetCapsule::init()
{
	if(Item::init() == false) {
		log("MagnetCapsule: init failed");
		return false;
	}
	
	// AnimationHelper::addRotatingAura(this, 4, 1, "item_aura.png");
	
	setScale(0.4f);
	//setup("item_box_03.csb");
	mCsbFile = "item/item_box_magnet.csb";

	
	return true;
}

bool MagnetCapsule::shouldBeCleanUp(){
    if(mUsed){
        return true;
    }
    
    float bottomMargin = GameWorld::instance()->getCameraY() - 200;
    if(getPosition().y < bottomMargin){
        return true;
    }
    
    return false;
}

bool MagnetCapsule::use()
{
	if(Item::use() == false) {
		return false;
	}
	
    
	GameSound::playSound(GameSound::VoiceCool);
	GameSound::playSound(GameSound::CollectCapsule);
    GameWorld::instance()->getEffectLayer()->playCsbAnime("particle_consume_general.csb", "active", false, true,1.0, getPosition());
	
	GameWorld::instance()->getPlayer()->activatePowerUpByType(ItemEffect::ItemEffectStarMagnet);
	
	return true;
}
