//
//  SpeedUpCapsule.cpp
//  GhostLeg
//
//  Created by Ken Lee on 17/1/2017.
//
//

#include "SpeedUpCapsule.h"

#include "GameSound.h"
#include "GameWorld.h"
#include "Player.h"
#include "ItemEffect.h"
#include "AnimationHelper.h"

const std::string SpeedUpCapsule::kCsbFile = "item/item_speedup.csb";


SpeedUpCapsule::SpeedUpCapsule(): Item(Item::Type::ItemSpeedUp)
{
	
}


bool SpeedUpCapsule::init()
{
	if(Item::init() == false) {
		log("SpeedUpCapsule: init failed");
		return false;
	}
	
	// AnimationHelper::addRotatingAura(this, 4, 1, "item_aura.png");
	
	setScale(0.5f);
	
	mCsbFile = kCsbFile;
	
	
	
	return true;
}

bool SpeedUpCapsule::use()
{
	if(Item::use() == false) {
		return false;
	}
	
	GameSound::playSound(GameSound::VoiceGo);
	GameSound::playSound(GameSound::CollectCapsule);
	
	GameWorld::instance()->getPlayer()->activatePowerUpByType(
						ItemEffect::ItemEffectSpeedUp);
	
	return true;
}
