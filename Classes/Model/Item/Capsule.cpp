//
//  Capsule.cpp
//  GhostLeg
//
//  Created by Ken Lee on 18/6/2016.
//
//

#include "Capsule.h"

#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"
#include "InvulnerableEffect.h"
#include "ShieldEffect.h"
#include "TimeStopEffect.h"
#include "DoubleCoinsEffect.h"
#include "MissileEffect.h"
#include "EffectLayer.h"
#include "ItemEffectFactory.h"
#include "Analytics.h"

namespace {
	std::string getCapsuleCsb(ItemEffect::Type type) {
		switch(type) {
			case ItemEffect::Type::ItemEffectMissile:
				return "item_missile1.csb";
				
			case ItemEffect::Type::ItemEffectInvulnerable:
				return "item_invur.csb";
				
			case ItemEffect::Type::ItemEffectDoubleCoins:
				return "item_slow.csb";
				
			case ItemEffect::Type::ItemEffectTimeStop:
				return "item_timestop.csb";
				
			case ItemEffect::Type::ItemEffectStarMagnet:
				return "item_magnet.csb";
				
			case ItemEffect::Type::ItemEffectSnowball:
				return "item_snowball.csb";
                
            case ItemEffect::Type::ItemEffectSpeedUp:
                return "item_speedup.csb";
				
			default:
				return "";
		}
	}
	
	void applyItemEffect(ItemEffect::Type itemEffectType, float itemValue, int level) {
		Player *player = GameWorld::instance()->getPlayer();
		
		if(player == nullptr) {
			log("Capsule:applyItemEffect:error: player is null");
			return;
		}
		
		ItemEffect *effect = ItemEffectFactory::createEffectByType(itemEffectType);
		if(effect == nullptr) {
			log("Capsule:applyItemEffect:error: effect is null");
			return;
		}
		
		effect->setLevel(level);
		effect->setMainProperty(itemValue);
		player->attachItemEffect(effect);
	}
}

Capsule::Capsule()
: Item(Item::Type::ItemCapsule)
, mItemEffectType(ItemEffect::Type::ItemEffectNone)
, mItemEffectValue(0)
{
	
}


bool Capsule::init()
{
	if(Item::init() == false) {
		log("Capsule: init failed");
		return false;
	}
	
	return true;
}

void Capsule::setItemEffectType(ItemEffect::Type effectType)
{
	mItemEffectType = effectType;
	
	setScale(0.9f);
	
	std::string csbName = getCapsuleCsb(mItemEffectType);
	if(csbName != "") {
		setup(csbName);
	}
}




bool Capsule::use()
{
	if(Item::use() == false) {
		return false;
	}
	
	GameSound::playSound(GameSound::CollectCapsule);
	GameSound::playSound(GameSound::VoicePowerUp);
	
	ItemEffect::Type itemType = mItemEffectType;
	float itemValue = mItemEffectValue;

	// ken: no more activate skill by capsule, use booster now
	
//	std::function<void()> callback = [&, itemType, itemValue]() {
//		applyItemEffect(itemType, itemValue, mLevel);
//	};
//
//    
//	GameWorld::instance()->getEffectLayer()->showCollectCapsuleEffect((int) mItemEffectType,
//																	  getPosition(), callback);

	return true;
}
