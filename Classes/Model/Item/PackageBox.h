//
//  PackageBox.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#ifndef PackageBox_hpp
#define PackageBox_hpp

#include <stdio.h>
#include "Item.h"

class PackageBox : public Item
{
public:
	enum Animation {
		DropDown,
		Disappear,
		MovingUp,		// Moving Up
		MovingSide,		// Moving Left/Right
        MonsterChase,
	};
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(PackageBox);
	
#pragma mark - Public Method and Properties
	PackageBox();
	
	virtual bool init();	// virtual because different derived class have different init logic

	void setPackage(int packageType);
	
	void showAnimation(Animation animation, std::function<void()> endCallback = nullptr);
	
private:
	std::string getAnimationName(Animation animation);
	bool isAnimationLooping(Animation animation);
	
};
#endif /* PackageBox_hpp */
