//
//  Cashout.cpp
//  GhostLeg
//
//  Created by Calvin on 6/12/2016.
//
//

#include "Cashout.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "Enemy.h"
#include "Star.h"
#include "AnimationHelper.h"

const std::string Cashout::kCsbFile = "item/item_box_cashout.csb";

Cashout::Cashout(): Item(Item::Type::ItemCashout)
{
    
}


bool Cashout::init()
{
    if(Item::init() == false) {
        log("Cashout: init failed");
        return false;
    }
    
//    setScale(0.9f);
//    setup("item_candystick.csb");
	// AnimationHelper::addRotatingAura(this, 4, 1, "item_aura.png");
	
	setScale(0.4f);
	//setup("item_box_01.csb");
	mCsbFile = kCsbFile;

    
    return true;
}

bool Cashout::use()
{
    if(Item::use() == false) {
        return false;
    }
	
	GameSound::playSound(GameSound::VoiceCool);
    GameWorld::instance()->getPlayer()->activatePowerUpByType(ItemEffect::ItemEffectCashOut);
    
    return true;
}
