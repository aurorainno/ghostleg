//
//  ItemPuzzle.hpp
//  GhostLeg
//
//  Created by Ken Lee on 8/5/2017.
//
//

#ifndef ItemPuzzle_hpp
#define ItemPuzzle_hpp

#include <stdio.h>

#include "Item.h"


class ItemPuzzle : public Item
{
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(ItemPuzzle);
	
#pragma mark - Public Method and Properties
	ItemPuzzle();
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();
	virtual void update(float delta);
 
};



#endif /* ItemPuzzle_hpp */
