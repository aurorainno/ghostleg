//
//  ShieldCapsule.cpp
//  GhostLeg
//
//  Created by Ken Lee on 17/1/2017.
//
//

#include "ShieldCapsule.h"

#include "GameSound.h"
#include "GameWorld.h"
#include "Player.h"
#include "ItemEffect.h"
#include "AnimationHelper.h"

ShieldCapsule::ShieldCapsule(): Item(Item::Type::ItemShield)
{
	
}


bool ShieldCapsule::init()
{
	if(Item::init() == false) {
		log("ShieldCapsule: init failed");
		return false;
	}
	
	AnimationHelper::addRotatingAura(this, 4, 1, "item_aura.png");
	setScale(0.9f);
	setup("item_invur.csb");
	
	
	
	return true;
}

bool ShieldCapsule::use()
{
	if(Item::use() == false) {
		return false;
	}
	
	GameSound::playSound(GameSound::CollectCapsule);
	
	GameWorld::instance()->getPlayer()->activatePowerUpByType(ItemEffect::ItemEffectShield);
	
	return true;
}
