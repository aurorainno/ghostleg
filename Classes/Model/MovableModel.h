//
//  MovableModel.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#ifndef MovableModel_hpp
#define MovableModel_hpp

#include <stdio.h>
#include "cocos2d.h"

#include "CommonType.h"
#include "MapLine.h"
#include "GameRes.h"

#include "GameModel.h"

USING_NS_CC;

// Note: MovableModel is not a game name, better name should be "ActorModel"
class MovableModel : public GameModel
{	
public:
	
	
#pragma mark - Public static Method
	CREATE_FUNC(MovableModel);

#pragma mark - Public Method and Properties
	MovableModel();
	
	virtual bool init();	// virtual because different derived class have different init logic
	
	virtual void move(float delta);	// delta is the time delta value
	virtual void update(float delta);
	virtual void reset();
    void setSpeed(float speed);
	
	virtual void moveFree(float delta);		// Move freely without the route constraint
	virtual void onEnterSlopeLine() {}
	virtual void onExitSlopeLine() {}
	
	CC_SYNTHESIZE(Dir, mDir, Dir);				// The direction of model just use
	CC_SYNTHESIZE(Dir, mNextDir, NextDir);		// The direction of model will use in next update/move
	
	// The line walking on
	CC_SYNTHESIZE(int, mVertLine, VertLine);				//	note: vertical line just need X pos, seem no use!!!
	CC_SYNTHESIZE_RETAIN(MapLine *, mHoriLine, HoriLine);	//		  horizontal line need the line segment

	CC_SYNTHESIZE(float, mSpeedY, SpeedY);			// pixel per second
	CC_SYNTHESIZE(float, mSpeedX, SpeedX);			// pixel per second
	CC_SYNTHESIZE(float, mSlowFactor, SlowFactor);	//

	// note:
	void initDir(Dir dir);
	
	virtual void resetToUpwardDir();
	
	virtual std::string toString();
	
	bool isCollide(Node *otherObject);

	virtual Vec2 getFinalSpeed() const;
	virtual	Vec2 getCenterPosition() const;

	// Effect Status
	EffectStatus getEffectStatus() const;
	virtual void setEffectStatus(EffectStatus status, float duration = -1);
	virtual void removeEffectStatus();
    
    virtual void onApplyEffectStatus(EffectStatus status){};
    virtual void onRemoveEffectStatus(EffectStatus status){};
    
	void handleEffectStatus(float delta);
    
    void setStatusAnimationByEffect(EffectStatus effect,float scale, bool isFront);
    void removeStatusAnimation(EffectStatus effect);
    
    std::string getAnimeCsbNameByEffect(EffectStatus effect);
	
#pragma mark - Internal Data
protected:
//	Dir mNextDir;
//	MapLine *mNextLine;
	Dir mDefaultDir;
	EffectStatus mEffectStatus;
	bool mHasEffectStatusDuration;
	float mEffectStatusRemainTime;		//

#pragma mark - Internal Method
protected:
	Vec2 getNewPosition(float delta) const;
	Vec2 calculateMovement(float delta) ;
	void updateActionByDir();
	Vec2 getMidPoint();
	
	//Vec2 calcNewPosition(float de)
};


#endif /* MovableModel_hpp */
