//
//  MapLine.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2016.
//
//

#ifndef MapLine_hpp
#define MapLine_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class MapLine : public Ref
{
public: // constant
	enum Type {
		eTypeFix,				// Fix , generate originally
		eTypeCreate,			// Created by player
        eTypeSlope,			// Created by player and it is slope type
	};

public:
	static MapLine *create(MapLine::Type type, float x1, float y1, float x2, float y2);
	static MapLine *create(const MapLine *mapLine);
    static MapLine *cloneMapLine(const MapLine *mapLine);
	
	MapLine();
	std::string toString();
	Vec2 getStartPoint() const;
	Vec2 getEndPoint() const;
	MapLine *clone();
	
	float getMaxY();
    
    bool isLineCreatedByUser();
    bool isSlopeLine() const;
    bool isSlopeUpward() const;
    bool isMovingUpwardAlongSlopeLine(Dir movingDirection) const;
    Vec2 getSlopeDestinationPoint(Dir movingDirection) const;
    float getLineSlope(Dir movingDirection);
    Color4F getLineColor();
    
    CC_SYNTHESIZE(int, mLineType, LineType);		// the type defined in Map Editor
	CC_SYNTHESIZE(Type, mType, Type);
	CC_SYNTHESIZE(LineSegment, mLine, Line);
    
    
    CC_SYNTHESIZE(int, mLineObjectID, LineObjectID);

	
};

#endif /* MapLine_hpp */
