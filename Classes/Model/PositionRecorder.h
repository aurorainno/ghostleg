//
//  PositionRecorder.hpp
//  GhostLeg
//
//  Created by Ken Lee on 7/4/2017.
//
//

#ifndef PositionRecorder_hpp
#define PositionRecorder_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <queue>

USING_NS_CC;

class PositionRecorder : public Ref
{
public:
	CREATE_FUNC(PositionRecorder);
	
	PositionRecorder();
	bool init() { return true; }
	
public:
	CC_SYNTHESIZE(float, mInterval, Interval);
	CC_SYNTHESIZE(int, mSampleSize, SampleSize);		// how many to be recorded

	void reset();
	void recordPositionNow(const Vec2 &pos);
	void recordPosition(const Vec2 &pos, float timeDelta);
	
	bool findNearestSpawnPosition(const Vec2 &playerPos, Vec2 &outPos);
	
	std::string toString();

private:
	void dequeueFront();

private:
	std::vector<Vec2> mQueueVector;
	float mCooldown;
};

#endif /* PositionRecorder_hpp */
