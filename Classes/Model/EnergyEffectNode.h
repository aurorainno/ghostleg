//
//  EnergyEffectNode.hpp
//  GhostLeg
//
//  Created by Calvin on 19/10/2016.
//
//

#ifndef EnergyEffectNode_hpp
#define EnergyEffectNode_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;

class EnergyEffectNode : public Node
{
public:
    CREATE_FUNC(EnergyEffectNode);
    virtual bool init();
    
    void setEnergyText(int energy);
    virtual void setOpacity(GLubyte opacity);
    
private:
    Text* mEnergyText;
    Node* mRootNode;
};


#endif /* EnergyEffectNode_hpp */
