//
//  Item.hpp
//
//	Item is the static model in the Game; E.g Gold, Potion
//
//  GhostLeg
//
//  Created by Ken Lee on 21/3/2016.
//
//

#ifndef Item_hpp
#define Item_hpp

#include <stdio.h>
#include "cocos2d.h"

#include "CommonType.h"

USING_NS_CC;

class MovableModel;
#include "GameModel.h"



class Item : public GameModel
{
public:
	enum Type
	{
		// Note: the number need to match with our Game Map Editor!!!
		ItemGold		= 0,			// Note: no more Gold item now.
		
		// non usable 
		ItemPackageBox  = 100,
		
		//
		ItemSmallCoin	= 201,		// --> Coin
		ItemPuzzle		= 202,		// Was Potion
		ItemCapsule		= 203,
		ItemCashout		= 211,		// Was CandyStick
		ItemSpeedUp		= 221,
		ItemShield		= 222,
		ItemMagnet		= 223,
		
		ItemPotion		= 230,		// Was Potion
		
		ItemBigCoin		= 231,
		ItemSmallStar	= 232,
		ItemMediumStar	= 233,
		ItemBigStar		= 234,
		
		// Non map editor settable item 
		ItemMissile		= 1001,
        ItemSnowBall    = 1002
	};
	
public:
	
#pragma mark - Public Method and Properties
	Item(Type type);
	
	CC_SYNTHESIZE(Vec2, mSpeed, Speed);
    CC_SYNTHESIZE(float, mAccel, Accel)
	
	virtual bool init();
	virtual void setup(const std::string &csbName);
	
	virtual bool use();			// using the item by the player
	virtual void useStart();	// End of use item animation
	virtual void useEnd();		// End of use item animation
	virtual void update(float delta);
	virtual bool isWeapon() { return false; }
	
	virtual std::string toString();
	
	bool isCollidePlayer(const Rect &playerRect);
	
	bool isCollide(MovableModel *otherModel);		// no use
	
	void markUsed(bool flag, bool autoRemove=true);
	bool isUsed();			//
	
    virtual bool shouldBeCleanUp();
	virtual std::string getName();
	
	Type getType();
    
#pragma mark - Internal Data
protected:
	//	Dir mNextDir;
	//	MapLine *mNextLine;
	Type mType;
	bool mUsed;
	bool mConsumed;

#pragma mark - Internal Method
protected:
	void fadeOut();
	CallFunc *endUseAction();
	
	
#pragma mark - Lazy loading for the Csb
protected:
	void setupCsb();

protected:
	std::string mCsbFile;
	bool mHasSetup;		// false when animation(csb) not setup
};


#endif /* Item_hpp */
