//
//  PlayerLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#ifndef PlayerLayer_hpp
#define PlayerLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "MovableModel.h"
#include "Player.h"
#include "Item.h"
#include "NPC.h"
#include "Enemy.h"
#include "GameplaySetting.h"
#include <set>

class ChaserEnemy;
class Enemy;
class Star;
class Collectable;

USING_NS_CC;

class ModelLayer : public cocos2d::LayerColor
{
public:
	CREATE_FUNC(ModelLayer);
	
	virtual bool init();
	
	ModelLayer();

	CC_SYNTHESIZE_RETAIN(Player *, mPlayer, Player);
	

	void update(float delta);
	void setOffsetY(float y);		// maybe: rename as setCameraY
	void clearData();
	void clearEnemy();		// clear enemy
    void removeEnemy(Enemy* enemy);
	
	void deactiveAllEnemy();
	
	void addPlayer(Player *player);
	void addEnemy(Enemy *enemy);
	void addEnemyList(const Vector<Enemy *> &enemyList);
	
	void addNpcList(const Vector<NPC *> &npcList);
	void addNpc(NPC *npc);
	NPC *getLastNpc();
	void clearNPC();
	void removePlayer();
	
	void addItem(Item *item);
	void removeItem(Item *item);
	void addItemList(const Vector<Item *> &itemList);
	void removeAllItems();
	void removeUsedItem();
    void cleanupInactiveEnemyAndItem();
	void reset();

	void pauseAllModel();
	void resumeAllModel();
	
	void handleItemCollision();
	
	
	int moveAllStarsToPlayer();		// return number of star collecting

	
	
	void applyEffectStatusToAllEnemy(EffectStatus status);


	void removeEffectStatusFromAllEnemy();
	
	void resetPlayer();
	MovableModel *getPlayerCollision();
	CollisionType handlePlayerCollision();
	
	Vector<Enemy *> getNearestEnemy(int distance, int maxCount, std::set<int> excludeSet = std::set<int>());
    Vector<Enemy *> getEnemyList();
	Vector<Enemy *> getItemCollidableEnemyList();
	
	Vector<Collectable *> getVisibleCollectableList();
    Vector<Star *> getStarList();
    Vector<Enemy *> getVisualEnemyList(float upperOffset = 0, float lowerOffset = 0);
	
	std::string info();
	std::string statInfo();
	
private:
	void setupInitialPlayerPos();
	void setEnemyPosition(Enemy *enemy, int vertLineIdx);
	void addModel(Enemy *model);
    void addModel(Enemy *model,int zOrder);
	void removeModel(Enemy *model);
	
	
	
private:
	Vector<Enemy *> mModelList;		// TODO: change to EnemyList 
	Vector<NPC *> mNpcList;
	Vector<Item *> mItemList;
	


#pragma mark - Package handling
public:
	void addPackageBox(const Vec2 &pos, std::function<void()> func);

#pragma mark - Motion
public:
	enum MotionStyle {
		StyleNone,
		StyleNormal,
		StyleMaxSpeed,
		StyleSpeedUp,		
	};
public:
	void setupMotionStreak();
	void changeMotionStyle(MotionStyle style);
	void setMotionEnable(bool flag);
	
	void updateMotion(float delta=0);
private:
	MotionStreak *mMotion;


#pragma mark - Chaser Enemy
public:
	void resetChaserEnemy();
	void resetChaserPosition();
	void setChaserEnemyActive();
	void setChaserEnemyInActive();
	bool handleChaserCollision();
	void adjustChaserPosition();
    void adjustChaserAppearOnBottom();
	void increaseChaserSpeed();
	void setChaserAttribute(const std::map<std::string, int> &attribute);
    void applyEffectStatusToChaser(EffectStatus status);
    void removeEffectStatusFromChaser();
    
    
private:
	void setupChaserEnemy();
	
private:
	ChaserEnemy *mChaserEnemy;


	
#pragma mark - Setup ModelLayer with GameplaySetting
public:
	void setupWithGameplaySetting(GameplaySetting *playSetting);
	
#pragma mark - Defend Mechanic
public:
	
	void setupDefendAttribute(GameplaySetting *playSetting);
	void testDefendAttribute();
	
private:
	BlockType isEnemyBlocked(Enemy *enemy);
	void handleEnemyBlocking(BlockType blockType, Player *player, Enemy *enemy);
	int getBlockerCount(BlockType blockType);
	void reduceBlockerCount(BlockType blockType);
	float getBlockAttribute(const std::map<GameplaySetting::Attribute, float> &attributeMap,
							GameplaySetting::Attribute attribute);
	void showBlockAnimation();

	void setupBlockUI(BlockType blockType);
	void updateBlockUI(BlockType blockType, int currentCount);
	
private:
	bool mHasDefend;			// true if there are any blocking
	std::map<BlockType, int> mEnemyBlockerMap;			// number of blocker count
	int mEnemyBlockingChance;	// Preventing block by general enemy
	
	
};


#endif /* PlayerLayer_hpp */
