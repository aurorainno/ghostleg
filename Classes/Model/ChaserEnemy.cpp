//
//  ChaserEnemy.cpp
//  GhostLeg
//
//  Created by Ken Lee on 9/5/2017.
//
//

#include "ChaserEnemy.h"
#include "EnemyFactory.h"
#include "EnemyBehaviour.h"
#include "ChaserBehaviour.h"
#include "DataHelper.h"
#include "GameWorld.h"
#include "GameplaySetting.h"
#include "Constant.h"

ChaserEnemy::ChaserEnemy()
: Enemy()
, mBehaviour(nullptr)
{
	
}

ChaserEnemy::~ChaserEnemy()
{
	
}

bool ChaserEnemy::init()
{
	if(Enemy::init() == false){
		return false;
	}
	
	
	setupEnemy();
	
	return true;
}


void ChaserEnemy::setupEnemy()
{
	// Attach a behaviour to the enemy
	EnemyBehaviour *behaviour = EnemyFactory::instance()->createBehaviourByMonsterID(kChaserEnemyID);
	if(behaviour == nullptr) {
		log("ChaserEnemy: behaviour is nullptr");
		return;
	}
	
	ChaserBehaviour *chaserBehaviour = dynamic_cast<ChaserBehaviour *>(behaviour);
	if(chaserBehaviour == nullptr) {
		log("ChaserEnemy: chaserBehaviour is nullptr");
		return;
	}
	mBehaviour = chaserBehaviour;
	
	// Setting the behaviour
	chaserBehaviour->setEnemy(this);
	setBehaviour(chaserBehaviour);
	
	// Setting the resource
	int resID = behaviour->getRes();
	setResource(resID);
	
    setupAttribute();
	

	
	// For testing
	// setupMockAttribute();
	
//	if(mEffectStatus != EffectStatusNone) {
//		enemy->setEffectStatus(mEffectStatus);
//	}
	
}

//void ChaserEnemy::setEffectStatus(EffectStatus status, float duration)
//{
//    mEffectStatus = status;
//    
//    std::string iconName = "";
//    
//    float timeSpeed = 1;
//    
//    bool anyVisualEffect = true;
//    float scale;
//    bool isFront;
//    
//    switch (status) {
//        case EffectStatusTimestop :
//        {
//            //			name = "status_timestop.png";
//            timeSpeed = 0;
//            scale = 0.7;		// Animation ???
//            isFront = false;
//            break;
//        }
//		default : {
//            anyVisualEffect = false;
//            break;
//        }
//    }
//	
//	setTimeSpeedByPercent(timeSpeed);
//	
//    if(anyVisualEffect == false) {
//        return;
//    }
//    
//    // Handle Effect Duration
//    if(duration < 0) {
//        mHasEffectStatusDuration = false;
//    } else {
//        mHasEffectStatusDuration = true;
//        mEffectStatusRemainTime = duration;
//    }
//    
//    setTimeSpeedByPercent(timeSpeed);
//    
//    if(iconName.length() > 0) {
//        setStatusIconByName(iconName);
//    }
//    
//    setStatusAnimationByEffect(status, scale, isFront);
//    onApplyEffectStatus(status);
//}

void ChaserEnemy::setupMockAttribute()
{
	std::map<std::string, int> speedMap;
	
	speedMap["initialSpeed"] = 0;
	speedMap["initWithPlayerSpeed"] = 0;
	speedMap["acceleration"] = 200;
	speedMap["maxSpeed"] = 100;

//	speedMap["initialSpeed"] = 0;
//	speedMap["initWithPlayerSpeed"] = 0;
//	speedMap["acceleration"] = 300;
//	speedMap["maxSpeed"] = 500;

	
	setSpeedAttribute(speedMap);
}

float ChaserEnemy::adjustSpeed(float originSpeed)
{
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	float slowRatio = setting == nullptr ? 0
			: setting->getAttributeValue(GameplaySetting::Attribute::ChaserSlowRatio);
	slowRatio = slowRatio / 100;		// percent to decimal
	if(slowRatio == 0) {
		return originSpeed;
	}
	
	return originSpeed * (1 - slowRatio);
}

void ChaserEnemy::setSpeedAttribute(const std::map<std::string, int> &attribute)
{
	
	if(mBehaviour == nullptr) {
		log("ChaserEnemy:setSpeedAttribute: behaviour is null");
		return;
	}
	
	int value;
	
	value = aurora::DataHelper::getMapValue(attribute, "initialSpeed", 0);
	mBehaviour->setInitialSpeed(value);

	value = aurora::DataHelper::getMapValue(attribute, "initWithPlayerSpeed", 0);
	mBehaviour->setInitWithPlayerSpeed(value == 1);

	value = aurora::DataHelper::getMapValue(attribute, "acceleration", 10);
	mBehaviour->setAcceleration(value);

	// Stun Setting
	value = aurora::DataHelper::getMapValue(attribute, "minStunSpeed", 100);
	value = adjustSpeed(value);
	mBehaviour->setMinStunSpeed(value);
	
	value = aurora::DataHelper::getMapValue(attribute, "maxStunSpeed", 300);
	value = adjustSpeed(value);
	mBehaviour->setMaxStunSpeed(value);
	
	value = aurora::DataHelper::getMapValue(attribute, "incStunSpeed", 50);
	mBehaviour->setIncStunSpeed(value);
	
	value = aurora::DataHelper::getMapValue(attribute, "speedRatio", 70);
	mBehaviour->setSpeedRatio(((float) value) / 100);
	//
	
	value = aurora::DataHelper::getMapValue(attribute, "brakeSpeedRatio", 10);
	mBehaviour->setBrakeSpeedRatio(((float) value) / 100);
	
	// Adjust the max speed
	value = aurora::DataHelper::getMapValue(attribute, "maxSpeed", 200);
	value = adjustSpeed(value);
	
	
	
	
	mBehaviour->setMaxSpeed(value);
}


void ChaserEnemy::increaseSpeed()
{
	if(mBehaviour == nullptr) {
		log("ChaserEnemy:increaseSpeed");
		return;
	}

	mBehaviour->increaseSpeedThreshold();
}

