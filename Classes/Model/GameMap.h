//
//  GameMap.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2016.
//
//

#ifndef GameMap_hpp
#define GameMap_hpp

#include <stdio.h>
#include <vector>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "MapLine.h"
#include "RouteLineNode.h"


USING_NS_CC;

class RouteLineNode;

class GameMap : public LayerColor
{
public:
	static float getFirstLineX();
	static float getLastLineX();
	static float getXAtLine(int lineIndex);
	
public:
	GameMap();

	CREATE_FUNC(GameMap);
	
	
	CC_SYNTHESIZE(float, mLineScale, LineScale);
	
	virtual bool init();
	
	void setStage(int stage);
	
	void setOffsetY(float y);
	float getOffsetY();
	
	const std::vector<int> &getVertLines() const;
	const MapLine *willCrossAnyHoriLine(const Vec2 &pos1, const Vec2 &pos2,
										bool &matchStartPoint);
	
	
	int getVertLineCount();
	int getVertLineX(int lineIndex);
	
	static bool isOnVertLine(float x);
	
	std::string infoHoriLines();
    void addHoriLines(Vec2 touchPoint);
    void addSlopeLine (Vec2 startPoint, Vec2 endPoint);
    void removeLineFromMap(int lineObjectID);

	
	void addHoriLines(const Vector<MapLine *> &newLines);
    float getClostestLineX(float touchPointX);
	
	int getApproachingLineIndex(float currentX, Dir dir);	// dir should be dirLeft or dirRight
	
    Vec2 getIntersectPoint(Vec2 slopeLineStartPoint ,Vec2 slopeLineEndPoint);
	
	void clearData();		// Clean all the horiLine data
	
	void cleanupOldRouteLine();
	void cleanupPlayerRouteLine();
	
	void setupVertLineLayer();
	
	void setVertLineVisible(bool flag);
	
	void addSlopeLine(MapLine *mapLine);
	void addSlopeLines(const Vector<MapLine *> &newLines);
	//void updateSlopeLines();
	void removeRouteLine(MapLine *mapLine);
	
	// Adjust the Position so that it is before last drop line
	Vec2 adjustPositionBeforeLastDropLine(const Vec2 &checkPos);
	
protected:
	RouteLineNode *getRouteLineNode(MapLine *line);
	RouteLineNode *addRouteLine(MapLine *line);
	RouteLineNode *createRouteLine(Node *parent, const Vec2 &start, const Vec2 &end);
	
	
	void setupBlinkEffect();
	
	bool shouldMapLineCleanup(MapLine *line);
	
private:
	Layer *mVertLineLayer;
	ui::Layout *mSlopeLineLayer;
	DrawNode *mLineLayer;				// the layer drawing the line
	DrawNode *mHLineLayer;
	std::vector<int> mVertLines;		// the x position of the line index
//	std::vector<MapLine *> mHoriLines;
//    std::vector<MapLine *> mSlopeLines;
	Vector<MapLine *> mHoriLines;		// it doesn't have with forward declaration
	float mOffsetY;
	
//	Vector<MapLine *> mSlopeLines;
	Map<MapLine *, RouteLineNode *> mLineRouteMap;	//
	
	
protected:
	void setupVertLines();
	void redrawLines();
	void setupHoriLines();
	MapLine *generateMapLine(const MapLine *lastLine);
	MapLine *generateFirstLine();
	void generateHoriLines(bool isBegin);
	bool needGenerateNewLines();
	float mVertLineSectionH;
	int mStage;
	std::string mRouteImage;
};

#endif /* GameMap_hpp */
