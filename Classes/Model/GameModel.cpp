//
//  GameModel.cpp
//  GhostLeg
//
//  Created by Ken Lee on 6/5/2016.
//
//

#include "GameModel.h"
#include "StringHelper.h"
#include "GeometryHelper.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "GameSound.h"
#include "TimeMeter.h"

#include <string>


//const float kDefaultScale = 0.52f;
const float kDefaultScale = 1.0f;

const Rect kDefaultRect(-40, 0, 80, 80);

static bool sShowHitbox = false;

const int kZOrderHitbox = 1000;

namespace {
	

	std::string getActionAnimeName(GameModel::Action action) {
		switch (action) {
			case GameModel::Action::Walk		: return "walk";
			case GameModel::Action::Climb		: return "climb";
			case GameModel::Action::Idle		: return "idle";
			case GameModel::Action::Consume		: return "consume";
			case GameModel::Action::Die			: return "die";
			case GameModel::Action::Attack		: return "attack";
            case GameModel::Action::Destroy		: return "destroy";
            case GameModel::Action::Jump        : return "attack";
			case GameModel::Action::Invisible	: return "invisible";
    		default								: return "unknown";
    	}
	}

	bool isLoopAnimation(GameModel::Action action) {
		switch (action) {
			case GameModel::Action::Consume:
			case GameModel::Action::Invisible:
            case GameModel::Action::Attack: return false;
            case GameModel::Action::Jump: return false;
            case GameModel::Action::Destroy: return false;
			default:
				return true;
		}
	}
}

void GameModel::showHitbox(bool flag)
{
	sShowHitbox = flag;
	
}

bool GameModel::isShowingHitbox()
{
	return sShowHitbox;
}

GameModel::GameModel()
: mTimeLine(nullptr)
, mRootNode(nullptr)
, mActionManager(nullptr)
, mBoundingImage(nullptr)
, mMainSprite(nullptr)
, mVisualEffectNode(nullptr)
, mStatusIconNode(nullptr)
, mStatusPos(0, 0)
, mUseQuickHitboxCheck(false)
, mTimelineMap()
, mCsbNodeMap()
, mIndicatorNode(nullptr)
, mHitboxNodeList()
, mHitboxSizeList()
, mHitCircleNodeList()
, mRootScale(kDefaultScale)
{
}

bool GameModel::init()
{
//	setActionManager(nullptr);
	return true;
}

GameModel::~GameModel()
{
	//log("GameModel: cleanup");
	stopAllActions();
	
	setTimeLine(nullptr);       // release take place in the setter

	//
	mRootNode = nullptr;		// it is retain and release by Node->add/removeChild

	mBoundingImage = nullptr;
}

void GameModel::cleanup()
{
	if(mRootNode) {
		removeChild(mRootNode);
		mRootNode = nullptr;
	}

	if(mTimeLine) {
		setTimeLine(nullptr);
	}
	mBoundingImage = nullptr;

}

void GameModel::setup(const std::string &csbName, Action defaultAction)
{
	// clear up old animation
	cleanup();


	// Setting the Root Node
	TSTART("model.createNode");
	Node *rootNode = CSLoader::createNode(csbName);
	TSTOP("model.createNode");
	if(rootNode == nullptr) {
		log("GameModel. cannot create the node: %s", csbName.c_str());
		return;
	}
	rootNode->setLocalZOrder(10);
	rootNode->setPosition(Vec2(0, 0));
	rootNode->setScale(getRootScale());
	
	mMainSprite = (Sprite *) rootNode->getChildByName("charSprite");
    mUpperEffectNode = rootNode->getChildByName("upperEffectNode");
	mLowerEffectNode = rootNode->getChildByName("lowerEffectNode");
	
	// setupSingleHitBox(rootNode);
	
	//Node *hitBox = (Node *) rootNode->getChildByName("hitbox");
	//log("GameModel: %s: mainSprite=%d hitbox=%d",
	//				csbName.c_str(), (mMainSprite != nullptr), (hitBox != nullptr));
	// setupBoundingBox(rootNode);
	setupContentSize(rootNode);  //
	setupStatusPos(rootNode);

	// Bind to the Game Model object
	addChild(rootNode);
	mRootNode = rootNode;

	setupHitboxes();
	
	
	// This is for debug
	//ViewHelper::createRedSpot(this, Vec2(0, 0));


	// Setting the animation
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
	
	if(timeline == nullptr) {
		log("GameModel. cannot create the timeLine: %s", csbName.c_str());
		return;
	}

	setTimeLine(timeline);		// retain take place in the setter
	mRootNode->runAction(mTimeLine);
	if(mTimeLine) {
		mOriginTimeSpeed = mTimeLine->getTimeSpeed();
		mTimeLine->setFrameEventCallFunc(CC_CALLBACK_1(GameModel::onFrameEvent, this));
	}

	setAction(defaultAction);
}

void GameModel::setupContentSize(Node *rootNode)
{

	Sprite *checkSprite = nullptr;

	Sprite *mainSprite = (Sprite *) rootNode->getChildByName("charSprite");
	if(mainSprite != nullptr) {
		//log("debug: using charSprite");
		checkSprite = mainSprite;
	} else {
		Sprite *hitbox = (Sprite *) rootNode->getChildByName("hitbox");
		//log("debug: using hitbox");
		checkSprite = hitbox;
	}

	if(checkSprite == nullptr) {
		//log("debug: using default");
		setContentSize(kDefaultRect.size);
	} else {
		setContentSize(checkSprite->getContentSize() * getScaleX());
	}

}

void GameModel::setupStatusPos(Node *rootNode)
{
	Node *node = rootNode->getChildByName("statusNode");
	if(node != nullptr) {		// Good Case
		mStatusPos = node->getPosition();
		return;
	}

	// Determine from mainSprite
	if(mMainSprite) {
		Size size = mMainSprite->getContentSize();
		float offsetY = mMainSprite->getPositionY() - size.height/2;
		mStatusPos = Vec2(-size.width/4, size.height + offsetY);
		return;
	}

	// using the default. Vec2::ZERO
}

void GameModel::setupBoundingBox(Node *rootNode)
{
	Rect boundRect;
	if(rootNode == nullptr) {
		return;
	}

	Sprite *image = (Sprite *) rootNode->getChildByName("hitbox");
	mBoundingImage = image;
	if(image == nullptr) {	// try charSprite
		Node *mainSprite = (Node *) rootNode->getChildByName("charSprite");
		if(mainSprite == nullptr) {
			return;
		}
		image = (Sprite *) mainSprite->getChildByName("hitbox");
		if(image == nullptr) {
			return;
		}
		mBoundingImage = image;
	}

	if(mBoundingImage) {
		mBoundingImage->setVisible(false);
	}

//	mRelativeBoundRect = boundRect;

//	mBoundingImage = image;
//	boundRect = mBoundingImage == nullptr ? kDefaultRect
//										: mBoundingImage->getBoundingBox();
//
//
//	if(getScaleY() == 1) {
//		mRelativeBoundRect = boundRect;
//		return;
//	}
//
//	float scale = fabs(getScaleY());
//
//	Vec2 origin = boundRect.origin * scale;
//	Size size = boundRect.size * scale;
//	log("scale=%f origin=%s size=%s", scale,
//				POINT_TO_STR(origin).c_str(), SIZE_TO_STR(size).c_str());
//
//	mRelativeBoundRect = Rect(origin, size);
}

float GameModel::getAnimationDuration()
{
    return mTimeLine->getDuration();
}

float GameModel::getStartFrame()
{
    return mTimeLine->getStartFrame();
}

float GameModel::getEndFrame()
{
    return mTimeLine->getEndFrame();
}

void GameModel::runAnimation(const std::string &animeName, bool isLoop)
{
	if(mTimeLine == nullptr) {
		log("Debug: runAnimation: mTimeLine is null");
		return;	// nothing to do
	}

	mTimeLine->play(animeName, isLoop);
}

void GameModel::setAction(Action action)
{
	if(mRootNode == nullptr) {
		log("Debug: setAction: mRootNode is null");
		return;	// nothing to do
	}
	
	std::string animeName = getActionAnimeName(action);
	if(animeName.length() == 0){
		log("Debug: setAction: animeName is null");
		return;		// something wrong
	}
	runAnimation(animeName, isLoopAnimation(action));
    
    playSoundWithAction(action);
}



float GameModel::getDefaultScale()
{
	return kDefaultScale;
}

bool GameModel::isFaceLeft()
{
	float newScaleX = mRootNode->getScaleX();
	
	return newScaleX > 0;
}

void GameModel::setFace(bool toLeft)		// the character is facing left by default
{
	float currentScale = mRootNode->getScaleY();	// assume ScaleY = ScaleX

	float newScaleX = toLeft ? currentScale : -currentScale;

	mRootNode->setScale(newScaleX, currentScale);
}

void GameModel::testHitBox()
{
//	Vec2 newPos = mBoundingImage->convertToWorldSpace(Vec2::ZERO);
//	log("newPos: %s", POINT_TO_STR(newPos).c_str());
//
	log("infoHitbox: %s", infoColliders().c_str());
	
	for(int i=0; i<10; i++) {
		Node *subHitbox = mMainSprite->getChildByName(StringUtils::format("hitbox_%d", i));
		if(subHitbox == nullptr) {
			continue;
		}
		//subHitbox->getUser
		Vec2 hitboxPos = subHitbox->convertToWorldSpace(Vec2::ZERO);
		log("hitbox-%d pos: %s", i, POINT_TO_STR(hitboxPos).c_str());
	}
}


Rect GameModel::getBoundingBox() const
{
	Rect rect = mBoundingImage == nullptr ? kDefaultRect
					: mBoundingImage->getBoundingBox();

	//log("getBounding (Origin): %s", RECT_TO_STR(rect).c_str());
	
	rect = aurora::GeometryHelper::scaleRect(rect, getScaleY());
	
	//log("getBounding (After Scale): %s", RECT_TO_STR(rect).c_str());

	rect.origin.x += getPositionX();
	rect.origin.y += getPositionY();

	return rect;
}

void GameModel::setAnimationEndCallFunc(Action action, std::function<void()> func)
{
	std::string animeName = getActionAnimeName(action);

	if(mTimeLine) {
		mTimeLine->setAnimationEndCallFunc(animeName, func);
	}
}


void GameModel::setAnimationEndCallFunc(const std::string &animeName,
												std::function<void()> func)
{
	if(mTimeLine && func) {
		mTimeLine->setAnimationEndCallFunc(animeName, func);
	}
}

void GameModel::setCsbAnimeFrameEventCallback(std::string csbName, std::string animation, EventFrameCallback callback)
{
    cocostudio::timeline::ActionTimeline *timeline = mTimelineMap.at(csbName);
    timeline->setFrameEventCallFunc(CC_CALLBACK_1(GameModel::onFrameEvent, this));
    setOnFrameEventCallback(callback);
}


std::string GameModel::infoComponentPos()
{
	std::string result = "";

	std::string myPos = POINT_TO_STR(getPosition());





	return result;
}

std::string GameModel::info()
{
	std::string result = "";

	result += "Position=(";
	result += POINT_TO_STR(getPosition());
	result += ")";


	return result;
}

void GameModel::setActionManager(ActionManager *manager)
{
//	mActionManager = manager;
//
//	if(mActionManager == nullptr) {
//		mActionManager = Director::getInstance()->getActionManager();
//	}
//
//	Node::setActionManager(mActionManager);
//
//	if(mRootNode) {
//		mRootNode->setActionManager(mActionManager);
//	}
}

void GameModel::setTimeSpeedByPercent(float percent)
{
	if(mTimeLine) {
		mTimeLine->setTimeSpeed(percent * mOriginTimeSpeed);
	}
}

void GameModel::stepAction()
{
	if(mTimeLine == nullptr) {
		return;
	}
	
	int currentFrame = mTimeLine->getCurrentFrame();
	int endFrame = mTimeLine->getEndFrame();
	int nextFrame = (currentFrame == endFrame) ?
						mTimeLine->getStartFrame() : currentFrame + 1;
		
	mTimeLine->gotoFrameAndPause(nextFrame);
}

void GameModel::pause()
{
	if(mTimeLine) {
		mTimeLine->pause();
	}
}

void GameModel::resume()
{
	if(mTimeLine) {
		mTimeLine->resume();
	}
}

void GameModel::setOnFrameEventCallback(EventFrameCallback callback)
{
    mEventFrameCallback = callback;
}

void GameModel::onFrameEvent(Frame *frame)
{
	// log("GameModel::onFrameEvent: called");
	if(frame == nullptr) {
		log("GameModel::onFrameEvent: fame is null");
		return;
	}

	EventFrame* event = dynamic_cast<EventFrame*>(frame);
	if(!event) {
		log("GameModel::onFrameEvent: event is null");
		return;
	}


	std::string str = event->getEvent();
	// log("GameModel::onFrameEvent: event %s found.", str.c_str());
	if (str == "playParticle")
	{
		handlePlayParticleEvent(event);
    }else if(mEventFrameCallback){
        mEventFrameCallback(str,event);
    }

}

void GameModel::handlePlayParticleEvent(EventFrame *event)
{
	// log("playParticle event is trigger");
	Node *node = event->getNode();
	if(node == nullptr) {
		return;
	}
	ParticleSystemQuad *particle = dynamic_cast<ParticleSystemQuad*>(node);
	if(particle == nullptr) {
		return;
	}

	particle->resetSystem();
}

void GameModel::setOpacity(GLubyte opacity)
{
	Node::setOpacity(opacity);

	if(mRootNode) {
		mRootNode->setOpacity(opacity);
	}
}

Vec2 GameModel::getCenterPoint()
{
	Size size = getContentSize();
	return Vec2(0, size.height/2);
}


void GameModel::addParticleEffect(GameRes::Particle particle, const Vec2 &offset)
{


	std::string name = GameRes::getParticleName(particle);
	bool autoRemove = GameRes::isLoopParticle(particle) == false;
	float scale = GameRes::getParticleScale(particle);

	if(name.length() == 0) {                                                     
		return;	// nothing to do
	}


	ParticleSystem *emitter = ParticleSystemQuad::create(name);
	if(emitter == nullptr) {
		log("showEffect: fail to load particle. name=%s", name.c_str());
		return;
	}
	emitter->setBlendFunc(BlendFunc{GL_ONE, GL_ONE});
	emitter->setAutoRemoveOnFinish(autoRemove);
	emitter->setScale(scale);

	Vec2 pos;
	Node *parent = getParentNodeForEffect(false, pos);

	pos += offset;
	
	emitter->setPosition(pos);
	parent->addChild(emitter);

	if(! autoRemove) {
		changeVisualEffect(emitter);
	}
}


void GameModel::changeVisualEffect(Node *newNode)
{
	removeVisualEffect();
	setVisualEffectNode(newNode);
}

void GameModel::removeVisualEffect()
{
	if(getVisualEffectNode()) {
		getVisualEffectNode()->removeFromParent();
		setVisualEffectNode(nullptr);
	}
}


Node *GameModel::getParentNodeForEffect(bool onMainSprite, Vec2 &outPos)
{
	if(onMainSprite == false) {
		outPos = Vec2::ZERO;
		return this;
	}
	
	Node *parentNode = this;
	
	Vec2 pos = getCenterPoint();
	if(onMainSprite && mMainSprite != nullptr) {
		Size size = mMainSprite->getContentSize();
		pos = Vec2(size.width/2, size.height/2);
		//pos = Vec2::ZERO;
		parentNode = mMainSprite;
	}

	// Set the sprite position and return the parent node
	outPos = pos;

	return parentNode;

}

void GameModel::addSpriteEffectByName(const std::string &spriteFrameName, bool onMainSprite, float scale)
{
	Sprite *sprite = Sprite::createWithSpriteFrameName(spriteFrameName);
	if(sprite == nullptr) {
		log("addSpriteEffectByName. sprite is null. name=%s", spriteFrameName.c_str());
		return;
	}

	sprite->setAnchorPoint(Vec2(0.5, 0.5));
	sprite->setScale(scale);


	Vec2 pos;
	Node *parentNode = getParentNodeForEffect(onMainSprite, pos);
	sprite->setPosition(pos);
	parentNode->addChild(sprite);

	changeVisualEffect(sprite);
}

void GameModel::removeCsbAnimeByName(const std::string &csbName)
{
	Node *node = mCsbNodeMap.at(csbName);
	if(node != nullptr) {
		node->removeFromParent();
	}

	// Erase from the map
	mTimelineMap.erase(csbName);
	mCsbNodeMap.erase(csbName);
	
	
}

void GameModel::playCsbAnime(const std::string &csbName, const std::string &animeName,
				bool isLoop, bool removeSelf,
				const std::function<void ()> &endAnimeCallback)
{


	cocostudio::timeline::ActionTimeline *timeline = mTimelineMap.at(csbName);
	if(timeline == nullptr) {
		log("playCsbAnime: timeline is nullptr");
		return;
	}

	setCsbAnimeVisible(csbName, true);		// Turn on visibility before play
	timeline->play(animeName, isLoop);

	bool hasEndCallFunc = removeSelf || (endAnimeCallback != nullptr);

	if(isLoop == false && hasEndCallFunc) {
		timeline->setAnimationEndCallFunc(animeName,
			[=]() {

				if(endAnimeCallback) {
					endAnimeCallback();
				}

				if(removeSelf) {
					removeCsbAnimeByName(csbName);
				}
			});
	}
}

void GameModel::setCsbAnimeVisible(const std::string &csbName,bool flag)
{
	Node *node = mCsbNodeMap.at(csbName);
	if(node != nullptr) {
		node->setVisible(flag);
	}

}


void GameModel::removeCustomIndicator()
{
	if(mIndicatorNode != nullptr) {
		mIndicatorNode->removeFromParent();
		mIndicatorNode = nullptr;
	}
}

void GameModel::addCustomIndicator(Node *node, const Vec2 &offset, float scale, bool onMainSprite)
{
	removeCustomIndicator();	// clean up first
	
	//node = LayerColor::create(Color4B::RED, 500, 500);
	
	node->setAnchorPoint(Vec2(0.5, 0));
	
	// Add the new indicator
	Vec2 pos;
	//Node *parentNode = getParentNodeForEffect(onMainSprite, pos);
	//pos += offset;
	node->setScale(scale);
	node->setPosition(offset);

	addChild(node);

	mIndicatorNode = node;
}


void GameModel::setCsbAnimeRotation(float degree)
{
	std::vector<std::string> mapKeyVec = mCsbNodeMap.keys();
	
	for(std::string key : mapKeyVec)
	{
		Node *csbNode = mCsbNodeMap.at(key);
        if(csbNode->getParent() != mUpperEffectNode &&
           csbNode->getParent() != mLowerEffectNode){
            csbNode->setRotation(degree);
        }
	}
}

void GameModel::addCsbAnimeByName(const std::string &csbName, bool isFront,
								  float scale, const Vec2 &offset)
{
	NodeType type = isFront ? UpperEffectNode : LowerEffectNode;
	
	addCsbAnimeByName(csbName, type, scale, offset);
}



void GameModel::addCsbAnimeByName(const std::string &csbName, NodeType parentNodeType,
								float scale, const Vec2 &offset)
{
	Node *csbNode = CSLoader::createNode(csbName);
	if(csbNode == nullptr) {
		log("addCsbEffectByName. cannot create the node: %s", csbName.c_str());
		return;
	}

	//csbNode->setAnchorPoint(Vec2(0.5, 0.5));
	csbNode->setScale(scale);

	Vec2 pos;
//	Node *parentNode = getParentNodeForEffect(onMainSprite, pos);
	pos += offset;

	csbNode->setPosition(pos);
    
	Node* parentNode = getNodeByNodeType(parentNodeType);
    if(parentNode){
        parentNode->addChild(csbNode);
    }

	if(parentNodeType != ObjectNode) {
		mCsbNodeMap.insert(csbName, csbNode);		// insert for making rotation
	}

	// Setting the animation
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
	if(timeline == nullptr) {
		log("addCsbAnimeByName:. cannot create the timeLine: %s", csbName.c_str());
		return;
	}

	csbNode->runAction(timeline);
	
	// Save the timeline
	mTimelineMap.insert(csbName, timeline);
}


void GameModel::adjustAnimeNode(const std::string &csbName, float angle, Vec2 offset)
{
    if(mCsbNodeMap.find(csbName)==mCsbNodeMap.end()){
        log("GameMode::rotateAnimeNode: cannot find anime node!");
        return;
    }
    Node* csbNode = mCsbNodeMap.at(csbName);
    Vec2 pos = getCenterPoint();;
    pos += offset;
//    if(angle < 0){
//        csbNode->setScaleX(-1);
//    }else{
//        csbNode->setScaleX(1);
//    }
    csbNode->setRotation(angle);
    csbNode->setPosition(pos);
}


void GameModel::setStatusIconByName(const std::string &spriteFrameName)
{
	Sprite *sprite = Sprite::createWithSpriteFrameName(spriteFrameName);
	if(sprite == nullptr) {
		log("addStatusIconByName. sprite is null. frameName=%s", spriteFrameName.c_str());
		return;
	}

	if(mMainSprite == nullptr) {
		log("addStatusIconByName. mainSprite is null. frameName=%s model=%s",
			spriteFrameName.c_str(), getName().c_str());
		return;
	}

	removeStatusIcon();


	// Define the AnchorPoint
	sprite->setScale(0.4f);
	sprite->setAnchorPoint(Vec2(0.5f, 0.5f));

	// Add the status icon
//	Size size = mMainSprite->getContentSize();
//	float offsetY = mMainSprite->getPositionY() - size.height/2;
//	Vec2 pos = Vec2(-size.width/4, size.height + offsetY);
	sprite->setPosition(mStatusPos);

	// Attach to Node
	addChild(sprite);

	mStatusIconNode = sprite;
}

void GameModel::removeStatusIcon()
{
	if(mStatusIconNode) {
		mStatusIconNode->removeFromParent();
		mStatusIconNode = nullptr;
	}
}



void GameModel::setupScaleAndFlip(float scaleX, float scaleY, bool flipX, bool flipY)
{
	if(mRootNode == nullptr) {
		return;
	}
	float originScaleX = mRootNode->getScaleX();
	float originScaleY = mRootNode->getScaleY();
	
	float finalScaleX = originScaleX * scaleX * (flipX ? -1 : 1);
	float finalScaleY = originScaleY * scaleY * (flipY ? -1 : 1);
	
	mRootNode->setScaleX(finalScaleX);
	mRootNode->setScaleY(finalScaleY);
}

void GameModel::setFlipX(bool flip)
{
	if(mMainSprite) {
		mMainSprite->setFlippedX(flip);
	}
}

void GameModel::setFlipY(bool flip)
{
	if(mMainSprite) {
		mMainSprite->setFlippedY(flip);
	}
}

//void GameModel::setStatusIcon(
void GameModel::setHitboxVisible(bool flag)
{
	if(mBoundingImage) {
		mBoundingImage->setVisible(flag);
	}
}


#pragma mark - Hitbox handling
void GameModel::setupSingleHitBox(Node *rootNode)
{
	if(rootNode == nullptr) {
		return;
	}
	
	// clean up
	mHitboxNodeList.clear();
	
	Node *hitboxSprite = (Node *) rootNode->getChildByName("hitbox");		// hit box at root node
	if(hitboxSprite == nullptr) {		// Default
		return;
	}

	addHitbox(hitboxSprite, false);
	mMainBoundingBox = hitboxSprite->getBoundingBox();
}



void GameModel::setupHitboxesForNode(Node *node)
{
	if(node == nullptr) {
		return;
	}
	
	if(getUseQuickHitboxCheck()){
		setupSingleHitBox(node);
		return;
	}
	
	//
	Vector<Node *> childList = node->getChildren();
	for(Node *subNode : childList) {
		std::string name = subNode->getName();
		// log("DEBUG >>>(%s) : subNode=%s ", getName().c_str(), name.c_str());
		if(aurora::StringHelper::startsWith(name, "hitbox") == false) {
			continue;	// non-hitbox
		}
		
		
		
		bool isCircle = aurora::StringHelper::contains(name, "circle");
		//log("DEBUG >>> : name=%s ", name.c_str());
		
		addHitbox(subNode, isCircle);
	}
}

Node* GameModel::getNodeWithName(std::string name)
{
    if(mRootNode == nullptr) {
        return nullptr;
    }
    
    Vector<Node *> childList = mRootNode->getChildren();
    for(Node *child : childList) {
        std::string nodeName = child->getName();
        if(aurora::StringHelper::startsWith(nodeName, name)){
            return child;
        }
    }
    
    return nullptr;
}


void GameModel::setupHitboxes()		// note: call after the setupScaleAndFlip
{
	if(mRootNode == nullptr) {
		return;
	}
	
	std::vector<Node *> nodeList;
	nodeList.push_back(mRootNode);
	
	Vector<Node *> childList = mRootNode->getChildren();
	for(Node *child : childList) {
		std::string name = child->getName();
		if(aurora::StringHelper::startsWith(name, "charSprite")){
			nodeList.push_back(child);
		}
	}
	
	// clean up
	mHitboxNodeList.clear();
	mHitCircleNodeList.clear();

	for(Node *node : nodeList) {
		setupHitboxesForNode(node);
	}
	
}



Size GameModel::getHitboxSize(Node *hitbox, bool doAbs)
{
	Size size = hitbox->getContentSize();	// non scaled size
	Vec2 scale = aurora::GeometryHelper::getNodeFinalScale(hitbox, mRootNode);
	Size scaledSize;
	scaledSize.width = getScaleX() * scale.x * size.width;
	scaledSize.height = getScaleY() * scale.y * size.height;
	
	if(doAbs) {
		scaledSize.width = fabs(scaledSize.width);
		scaledSize.height = fabs(scaledSize.height);
	}
	
	
	return scaledSize;
}

void GameModel::addHitbox(Node *subHitbox, bool isCircle)
{
	if(subHitbox == nullptr) {
		return;
	}
	
	subHitbox->setGlobalZOrder(kZOrderHitbox);
	subHitbox->setVisible(sShowHitbox);
	
	if(isCircle) {
		mHitCircleNodeList.pushBack(subHitbox);
	} else {
		mHitboxNodeList.pushBack(subHitbox);
	}
	
//	Size size = subHitbox->getContentSize();	// non scaled size
//	Vec2 scale = aurora::GeometryHelper::getNodeFinalScale(subHitbox, this);
//	Size scaledSize;
//	scaledSize.width = fabs(getScaleX() * scale.x) * size.width;
//	scaledSize.height = fabs(getScaleY() * scale.y) * size.height;
//	
//	mHitboxSizeList.push_back(scaledSize);
}


void GameModel::getHitCircle(std::vector<Circle> &circleList)
{
	circleList.clear();
	
	Vec2 nodePos = getPosition();
	
	for(int i=0; i<mHitCircleNodeList.size(); i++) {
		Node *node = mHitCircleNodeList.at(i);
		if(node->getOpacity() == 0) {
			continue;
		}
		Size size = getHitboxSize(node, true);		// mHitboxSizeList.at(i);
		
		float radius = MIN(size.width, size.height) / 2;	// pick the minium
		
		Vec2 worldPos =	node->convertToWorldSpaceAR(Vec2::ZERO);		// node->convertToWorldSpaceAR(Vec2::ZERO);
		Vec2 hitboxPos = worldPos;	//- nodePos;
		Vec2 anchor = node->getAnchorPoint();
		Vec2 offset = Vec2(radius * (anchor.x - 0.5f), radius * (anchor.y - 0.5f));
		Vec2 centerPos = worldPos - offset;
		
		Circle circle(centerPos, radius);
		circleList.push_back(circle);
	}

}

void GameModel::getHitboxRect(std::vector<Rect> &hitboxList)
{
	hitboxList.clear();
	if(getUseQuickHitboxCheck()) {
		hitboxList.push_back(getFinalMainBoundingBox(true));
		return;
	}
	
	Vec2 nodePos = getPosition();
	
	for(int i=0; i<mHitboxNodeList.size(); i++) {
		Node *node = mHitboxNodeList.at(i);
		
		Size size = getHitboxSize(node, true);		// mHitboxSizeList.at(i);
		
//		log("Debug: hitboxRect: %s: size=%s opacity=%d", getName().c_str(),
//			SIZE_TO_STR(size).c_str(), node->getOpacity());
		
		if(node->getOpacity() == 0) {
			continue;
		}
		
		
		
		Size signedSize = getHitboxSize(node, false);
		
		Vec2 worldPos =	node->convertToWorldSpaceAR(Vec2::ZERO);		// node->convertToWorldSpaceAR(Vec2::ZERO);
		Vec2 hitboxPos = worldPos;	//- nodePos;
		Vec2 anchor = node->getAnchorPoint();
		if(signedSize.width < 0) { anchor.x = 1- anchor.x; }
		if(signedSize.height < 0) { anchor.y = 1- anchor.y; }
		
		Vec2 offset = Vec2(size.width * anchor.x,
						  size.height * anchor.y);
		//Vec2 offset =
		
//		log("DEBUG: hitbox: size=%s absSize=%s offset=%s anchor=%s worldPos=%s",
//					SIZE_TO_STR(signedSize).c_str(),
//					SIZE_TO_STR(size).c_str(),
//					POINT_TO_STR(offset).c_str(),
//					POINT_TO_STR(anchor).c_str(),
//					POINT_TO_STR(worldPos).c_str()
//				);
		
		Rect rect(hitboxPos - offset, size);
		
		hitboxList.push_back(rect);
	}
}

Rect GameModel::getDefaultHitbox()
{
	std::vector<Rect> hitboxList;
	getHitboxRect(hitboxList);
	
	if(hitboxList.size() == 0) {
		return Rect::ZERO;
	}
	
	return hitboxList.at(0);
}

Rect GameModel::getMainHitboxRect() const	// bounding box of the main sprite
{
	Rect rect;
	if(mMainSprite == nullptr) {
		rect = Node::getBoundingBox();
		
		rect.size.width = 1;
		rect.size.height = 1;
	} else {
		rect = mMainSprite->getBoundingBox();
	}
	
	Vec2 worldPos = convertToWorldSpaceAR(Vec2::ZERO);
	rect.origin = worldPos - Vec2(rect.size.width/2, rect.size.height/2);
	
	return rect;
}


std::string GameModel::infoHitbox()
{
	if(getUseQuickHitboxCheck()) {
		Rect myRect = getFinalMainBoundingBox();
		
		return StringUtils::format("quickCheck: [%s]" , RECT_TO_STR(myRect).c_str());
	}
	
	// My Hitbox
	std::vector<Rect> myHitboxes;
	std::vector<Circle> myHitCircles;
	
	getHitboxRect(myHitboxes);
	getHitCircle(myHitCircles);

	std::string info = "multiple:";
	
	info += " rect:";
	for(Rect rect : myHitboxes) {
		info += "[";
		info += RECT_TO_STR(rect);
		info += "]";
	}
	
	info += " circle:";
	for(Circle circle : myHitCircles) {
		info += "[";
		info += CIRCLE_TO_STR(circle);
		info += "]";
	}
	
	
	return info;
}

std::string GameModel::infoColliders()
{
	std::string info = "";
	
	info += infoColliderList("Box Collider", mHitboxNodeList);
	info += "\n";
	info += infoColliderList("Circle Collider", mHitCircleNodeList);
	
	return info;
}

std::string GameModel::infoColliderList(const std::string &title, const Vector<Node *> &colliderList)
{
	std::string info = "";
	
	info = StringUtils::format("%s count: %d\n", title.c_str(), (int) colliderList.size());
	for(int i=0; i<colliderList.size(); i++) {
		Node *node = colliderList.at(i);
		Vec2 worldPos = node->convertToWorldSpaceAR(Vec2::ZERO);
		info += " nodePos: " + POINT_TO_STR(node->getPosition());
		info += " worldPos: " + POINT_TO_STR(worldPos);
		info += "\n";
	}
	
	return info;
}

#pragma mark - Collision Check

bool GameModel::isCollideModel(GameModel *model)
{
	if(model == nullptr) {
		return false;
	}
	
	if(getUseQuickHitboxCheck() && model->getUseQuickHitboxCheck()) {
		TSTART("CalculateSimplebox");
		Rect myRect = getFinalMainBoundingBox();
		Rect otherRect = model->getFinalMainBoundingBox();
		bool result = myRect.intersectsRect(otherRect);
		TSTOP("CalculateSimplebox");
		
		return result;
	}
	
	
	// My Hitbox
	TSTART("CalculateHitbox");
	std::vector<Rect> myHitboxes;
	std::vector<Circle> myHitCircles;
	// Other hitbox
	std::vector<Rect> otherHitboxes;
	std::vector<Circle> otherHitCircles;
	
	
	getHitboxRect(myHitboxes);
	getHitCircle(myHitCircles);
	
	model->getHitboxRect(otherHitboxes);
	model->getHitCircle(otherHitCircles);
	TSTOP("CalculateHitbox");
	
	TSTART("checkCollision");
	if(aurora::GeometryHelper::anyCollision(myHitboxes, otherHitboxes)) {
		//log("collision:Rect2Rect:me=%s other=%s", getName().c_str(), model->getName().c_str());
		TSTOP("checkCollision");
		return true;
	}
	
	if(aurora::GeometryHelper::anyCollision(myHitboxes, otherHitCircles)) {
		//log("collision:Rect2Circle:me=%s other=%s", getName().c_str(), model->getName().c_str());
		TSTOP("checkCollision");
		return true;
	}
	
	if(aurora::GeometryHelper::anyCollision(otherHitboxes, myHitCircles)) {
		//log("collision:Circle2Rect:me=%s other=%s", getName().c_str(), model->getName().c_str());
		TSTOP("checkCollision");
		return true;
	}
	
	if(aurora::GeometryHelper::anyCollision(myHitCircles, otherHitCircles)) {
		//log("collision:Circle2Circle:me=%s other=%s", getName().c_str(), model->getName().c_str());
		TSTOP("checkCollision");
		return true;
	}
	
	TSTOP("checkCollision");
	return false;
}

bool GameModel::isCollideWithCircle(const Circle &circle)
{
	
	return false;
}


bool GameModel::isCollideWithRect(const Rect &rectWorldCoord)
{
	std::vector<Rect> myHitboxes;
	
	getHitboxRect(myHitboxes);
	
	for(Rect hitbox : myHitboxes) {
		
		if(hitbox.intersectsRect(rectWorldCoord)) {
			log("DEBUG: collison Found. myHitbox=%s checkHitbox=%s",
				RECT_TO_STR(hitbox).c_str(), RECT_TO_STR(rectWorldCoord).c_str());
			
			return true;
		}
	}
	
	return false;
}

Dir GameModel::getHorizontalDir()
{
	float scale = mRootNode->getScaleX();
	// scale > 0: origin Dir = DirLeft / scale < 0 : DirRight
	
	return scale >= 0 ? DirLeft : DirRight;
	
}

Dir GameModel::getVerticalDir()
{
	float scale = mRootNode->getScaleY();
	
	return scale >= 0 ? DirDown : DirUp;
}

std::string GameModel::getName()
{
	return "GameModel";
}


Node *GameModel::getNodeByNodeType(const NodeType &type)
{
//	ModelNode,		// the GameModel node
//	UpperEffectNode,
//	LowerEffectNode,
//	RootNode
	switch(type) {
		case ObjectNode			:	{  return this;	}
		case UpperEffectNode	:	{  return mUpperEffectNode;	}
		case LowerEffectNode	:	{  return mLowerEffectNode;	}
		default					:	{  return mRootNode;	}
	}
	
}


std::string GameModel::infoMainBoundingBox()
{
	Rect finalBound = getFinalMainBoundingBox();
	
	return StringUtils::format("boundingBox: final=%s origin=%s",
							   RECT_TO_STR(finalBound).c_str(),
							   RECT_TO_STR(mMainBoundingBox).c_str());
}

Rect GameModel::getFinalMainBoundingBox(bool isWorldCoord)
{
	Rect rect = mMainBoundingBox;
	
	rect.origin.x += getPositionX();
	rect.origin.y += getPositionY();
	
	if(isWorldCoord == false) {
		return rect;
	}
	
	Node *parentLayer = getParent();
	float layerOffset = parentLayer == nullptr ? 0 : parentLayer->getPositionY();
	
//	log("getFinalMainBoundingBox: BEFORE OFFSET: %s", infoMainBoundingBox().c_str());
	
	rect.origin.y += layerOffset;
//	log("getFinalMainBoundingBox: AFTER OFFSET: offset=%f bound=%s (%s)",
//						layerOffset, RECT_TO_STR(rect).c_str(),
//						RECT_TO_STR(mMainBoundingBox).c_str());
	
	return rect;
}


bool GameModel::willBecomeVisible() {
	float topBound = GameWorld::instance()->getTopMapY();
	float threshold = topBound + 300;		// ken: TODO: move to Constant
	
	return getPositionY() < threshold;
}
