//
//  GameMap.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2016.
//
//

#include <stdio.h>
#include <math.h>
#include "CommonType.h"
#include "GameMap.h"
#include "MapLine.h"
#include "GameWorld.h"
#include "Player.h"
#include "RandomHelper.h"
#include "GeometryHelper.h"
#include "GameSound.h"
#include "ShaderHelper.h"
#include "AnimationHelper.h"
#include "StringHelper.h"


const char *kLinePng = "general/route_line.png";


// The 4 line at (x=40, 120, 200, 280)
const int kMarginX = 10;			//
const int kGridBetweenLine = 3;		// was 4

const int kGridSize = 20;
const int kFirstX = kMarginX + kGridSize;		// 1 grid at margin (30)
const int kSpacing = kGridSize * (kGridBetweenLine + 1);	// also the horizontal line width
															// 20 * 4 = 80 ?
const int kTotalGrid = 16;		// 4 + 4 * 3 = 16 (horizontally)

const int kVertLineCount = 4;
const float kLineThickness = 0.5f;




//const Color4F kMainColor(0.0000, 0.4216, 0.7000, 1.0);
const Color4F kMainColor(0.4275,1.0000,0.9882, 1.0);

const float kLineSegmentLength = 1122;		// check this with the image file

const float kCleanupOffset = 300;			// the offset in Y axis for the clean up action
											// 300px is around half of the screen

#pragma mark - Local Private Method 

namespace  {
	void drawVerticalLine(DrawNode *drawNode, int x, int offsetY) {
		Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

		Vec2 start = Vec2(x, 0);
		Vec2 end = Vec2(x, size.height * 2);
		
		
		drawNode->drawSegment(start, end, kLineThickness, kMainColor);
		//drawNode->drawSegment(start, end, 0.5f, Color4F(0.0000, 0.7216, 1.0000, 1.0));
		//drawNode->drawLine(start, end, Color4F::RED);
	}
    

	//Can use for Slope Line as well
	void drawHorizontalLine(DrawNode *drawNode, MapLine *mapLine, int offsetY)
    {
        
        Vec2 start = mapLine->getStartPoint();
        Vec2 end = mapLine->getEndPoint();
        start.y -= offsetY;
        end.y -= offsetY;
		drawNode->drawSegment(start, end, 0.5f, Color4F::RED);	// mapLine->getLineColor());

		
	}
    
	
	void adjustDropPosition(Vec2 &pos1, Vec2 &pos2) {
		Vec2 playerPos = GameWorld::instance()->getPlayer()->getPosition();
		
		if(pos1.x == playerPos.x) {
			if(pos1.y < playerPos.y + 5) {
				pos1.y = playerPos.y + 5;
			}
		}
		
		if(pos2.x == playerPos.x) {
			if(pos2.y < playerPos.y + 5) {
				pos2.y = playerPos.y + 5;
			}
		}
	}
	
	float getModValue(float value, float modValue)
	{
		float quotient = floorf(value / modValue);
		
		return quotient * modValue - value;
	}

}


#pragma mark - Class Logic
GameMap::GameMap()
: mLineRouteMap()
, mLineScale(0.5)
, mStage(1)
, mRouteImage(kLinePng)
{
	mOffsetY = 0;
}

bool GameMap::init()
{
	Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	// log("World size=%s", StringHelper::strSize(size).c_str());
	
	bool isOk = LayerColor::initWithColor(Color4B(0, 0, 0, 0), size.width, size.height);
	if (isOk == false)
	{
		return false;
	}
	
	
	mSlopeLineLayer = ui::Layout::create();
	mSlopeLineLayer->setAnchorPoint(Vec2::ZERO);
	mSlopeLineLayer->setPosition(Vec2::ZERO);
	AnimationHelper::setAlphaBlink(mSlopeLineLayer, 3, 0, 125, 255, nullptr);
	addChild(mSlopeLineLayer);
	
	// Line Layer
	mLineLayer = DrawNode::create();
	mLineLayer->setPosition(Vec2(0, 0));
	addChild(mLineLayer);
	mLineLayer->setVisible(false);
	
	// Set the Shader
	aurora::ShaderHelper::setShaderToDrawNode(mLineLayer, "shader/dash.vsh", "shader/dash.fsh");
	
	mHLineLayer = DrawNode::create();
	mHLineLayer->setPosition(Vec2(0, 0));
	addChild(mHLineLayer);
	aurora::ShaderHelper::setShaderToDrawNode(mHLineLayer, "", "shader/dash_h_line.fsh");
	mHLineLayer->setVisible(false);
	
	// Setup Data
	setupVertLines();	// define the array of vertLines

	setupVertLineLayer();		// the vertical line graphics
	
	//redrawLines();
	
	
	return true;
}

void GameMap::setupVertLines()
{
	mVertLines.clear();
	
	// line: 40, 120, 200, 280
	
	int numLine = kVertLineCount;
	int x = kFirstX + kGridSize/2;
	
	for(int i=0; i<numLine; i++) {
		mVertLines.push_back(x);
		x += kGridSize * (kGridBetweenLine + 1);
	}
}

void GameMap::setupHoriLines()
{
	mHoriLines.clear();
	
	
}


void GameMap::setVertLineVisible(bool flag)
{
	if(mVertLineLayer) {
		mVertLineLayer->setVisible(flag);
	}
}

void GameMap::setupVertLineLayer()
{
	mVertLineLayer = Layer::create();
	addChild(mVertLineLayer);
	mVertLineLayer->setVisible(false);
	
	// find the total leng of the vertical line
	float scale = mLineScale;
	float segmentLen = kLineSegmentLength * scale;
	int numSegment = (int) ceilf(VisibleRect::getVisibleRect().size.height / segmentLen);
	float totalLen = segmentLen * numSegment * 2;
	
	mVertLineSectionH = segmentLen * numSegment;
	
	log("DEBUG: setupVertLine: segmentLen=%f numSeg=%d sectionH=%f totalLen=%f",
					segmentLen, numSegment, mVertLineSectionH, totalLen);
	
	for(int x : mVertLines) {
		Vec2 start = Vec2(x, 0);
		Vec2 end = Vec2(x, totalLen);
		
		
		
		// RouteLineNode *route =
		//createRouteLine(mVertLineLayer, start, end);
		
		// AnimationHelper::setAlphaBlink(route, 3, 0, 125, 255, nullptr);
		
	}
}

void GameMap::redrawLines()
{
	mLineLayer->clear();
	mHLineLayer->clear();
	
//	// Drawing the Vertical lines
//	for(int i=0; i<mVertLines.size(); i++) {
//		float x = mVertLines[i];
//		drawVerticalLine(mLineLayer, x, mOffsetY);
//	}
	
	bool showLine = false;
	
	if(showLine == false) {
		return;
	}
	
	// Drawing the horizontal lines
	for(int i=0; i<mHoriLines.size(); i++) {
		MapLine *line = mHoriLines.at(i);
        drawHorizontalLine(mHLineLayer, line, mOffsetY);//log("DEBUG: line=%s", line->toString().c_str());
	}
    
    
    
	mHLineLayer->visit();
	mLineLayer->visit();
	
}


// note:
//		startPoint and endPoint are screen position
//
void GameMap::addSlopeLine (Vec2 startPoint, Vec2 endPoint)
{
	// Re-organise the point
	bool needSwap = startPoint.x > endPoint.x;
	
	Vec2 leftPoint = needSwap ? endPoint : startPoint;
	Vec2 rightPoint = needSwap ? startPoint : endPoint;
	
	// Add the map offset
	leftPoint.y += mOffsetY;
	rightPoint.y += mOffsetY;
	
	// Adjust for player position
	adjustDropPosition(leftPoint, rightPoint);
	
	
	// Adjust for line
	Vec2 result;
	
	// Adjust to based on the current line
	result = adjustPositionBeforeLastDropLine(leftPoint);
	if(result != Vec2::ZERO) {
		leftPoint = result;
	}
	result = adjustPositionBeforeLastDropLine(rightPoint);
	if(result != Vec2::ZERO) {
		rightPoint = result;
	}
	
	
	//
    MapLine *mapLine = MapLine::create(MapLine::eTypeSlope,
				leftPoint.x, leftPoint.y,
				rightPoint.x, rightPoint.y);
	
	addSlopeLine(mapLine);
}


void GameMap::addHoriLines(Vec2 touchPoint)
{
    float horiLineStartX =0;
    float horiLineEndX =0;
    float touchPointX = touchPoint.x;
    float horiLineY = mOffsetY + touchPoint.y;
    if(touchPointX < kFirstX){return;}
    if(touchPointX >= mVertLines[mVertLines.size()-1]){return;}



    for(int i=1; i<kVertLineCount;i++ )
    {
        float firstLineX = mVertLines[i-1];
        float secondLineX = mVertLines[i];

        if(touchPointX>=firstLineX && touchPointX<= secondLineX)
        {
                horiLineStartX = firstLineX;
                horiLineEndX = secondLineX;
                break;
        }
        
    }
    
    MapLine *mapLine = MapLine::create(MapLine::eTypeCreate, horiLineStartX, horiLineY, horiLineEndX, horiLineY);
    mHoriLines.pushBack(mapLine);
    //redrawLines();
    
}


void GameMap::removeLineFromMap(int lineObjectID)
{
	
    int removingIndex = -1;
	
	MapLine *foundLine = nullptr;
	
    for(int i=0; i<mHoriLines.size(); i++)
    {
        MapLine *line = mHoriLines.at(i);
        if(line->getLineObjectID() == lineObjectID)
        {
            removingIndex =  i;
			foundLine = line;
            break;
        }
    }
    
    
    if(removingIndex >= 0)
    {
        mHoriLines.erase(mHoriLines.begin() + removingIndex);
        //redrawLines();
		
		if(foundLine != nullptr) {
			removeRouteLine(foundLine);
		}
    }
    
    
} 


void GameMap::generateHoriLines(bool isBegin)
{
	MapLine *line;
	if(isBegin) {
		mHoriLines.clear();
	}
	
	
	size_t numLine = mHoriLines.size();
	if(numLine == 0) {
		line = generateFirstLine();
		mHoriLines.pushBack(line);
	} else {
		line = mHoriLines.at(numLine - 1);
	}
	
	int numGenerate = 10;
    
	
	for(int i=0; i<numGenerate; i++) {
		line = generateMapLine(line);
	
		mHoriLines.pushBack(line);
	}
}

MapLine *GameMap::generateFirstLine()
{
	float y = 20;
	float x = kFirstX;
	MapLine *mapLine = MapLine::create(MapLine::eTypeFix, x, y, x+kSpacing, y);
	
	return mapLine;
}

MapLine *GameMap::generateMapLine(const MapLine *lastLine)
{
	
	static int col = 0;
	int variableSpacing = aurora::RandomHelper::randomBetween(5, 50) * 3;
	
	float y = lastLine->getLine().y1 + variableSpacing;
	float startX = kFirstX + kSpacing * col;
	float endX = startX + kSpacing;

	// change the col
	col = (col + 1) % (kVertLineCount - 1);		//
	
	MapLine *mapLine = MapLine::create(MapLine::eTypeFix, startX, y, endX, y);
	
	return mapLine;
}

int GameMap::getVertLineX(int lineIndex)
{
	if(lineIndex >= mVertLines.size()) {
		lineIndex = mVertLines.size() - 1;
	}
	
	if(lineIndex < 0) {
		lineIndex = 0;
	}
	
	return mVertLines[lineIndex];
}

void GameMap::setOffsetY(float y)
{
	mOffsetY = y;
	
	
	Size size = Director::getInstance()->getOpenGLView()->getVisibleSize();
	float modOffset = fmod(mOffsetY, mVertLineSectionH);
	
	//
	//redrawLines();
	//mLineLayer->setPositionY(-modOffset);
	
	// New Logic
	mSlopeLineLayer->setPositionY(-mOffsetY);
	
	//mVertLineLayer->setPositionY(-modOffset);			// ken: change to background parallax
	
	// Cleanup
	cleanupOldRouteLine();
}


bool GameMap::needGenerateNewLines()
{
	//
	Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	float bottomLine = mOffsetY;
	float topLine = bottomLine + size.height;
	
	size_t numLine = mHoriLines.size();
	if(numLine == 0) {
		return true;
	}
	
	// Last line
	MapLine *lastLine = mHoriLines.at(numLine - 1);
	float maxY = lastLine->getMaxY();
	//log("check: maxY=%f topLine=%f", maxY, topLine);
	
	if(maxY >= topLine) {
		return false;
	} else {
		return true;
	}
}

const std::vector<int> &GameMap::getVertLines() const
{
	return mVertLines;
}


const MapLine *GameMap::willCrossAnyHoriLine(const Vec2 &pos1, const Vec2 &pos2,
											 bool &matchStartPoint)
{
	
	for(MapLine *line : mHoriLines) {
		
		Vec2 startPoint = line->getStartPoint();
		Vec2 endPoint = line->getEndPoint();
		// if(line->getLine().x1 )
		
		bool anyCross;
		
		// Check the start point
		anyCross = aurora::GeometryHelper::isLineIntersect(pos1, pos2, startPoint, true, false);
		if(anyCross) {
			matchStartPoint = true;
			return line;
		}
		
		// Check the end point
		anyCross = aurora::GeometryHelper::isLineIntersect(pos1, pos2, endPoint, true, false);
		if(anyCross) {
			matchStartPoint = false;
			return line;
		}
		
	}
	
	return nullptr;
}


std::string GameMap::infoHoriLines()
{
	std::string result;
	

	char temp[100];
	

	for(MapLine *line : mHoriLines) {
		
		Vec2 start = line->getStartPoint();
		Vec2 end = line->getEndPoint();

		sprintf(temp, "start=%f,%f end=%f,%f\n", start.x, start.y, end.x, end.y);
		
		result += temp;
	}
	
	
	
	return result;
}

void GameMap::addHoriLines(const Vector<MapLine *> &newLines)
{
	
    
	for(int i=0; i<newLines.size(); i++) {
		MapLine *line = newLines.at(i);

		mHoriLines.pushBack(line);
	}
	
	//redrawLines();
}

void GameMap::clearData()
{
	mHoriLines.clear();
	
	mSlopeLineLayer->removeAllChildren();
	mLineRouteMap.clear();
	
	//redrawLines();
}


int GameMap::getVertLineCount()
{
	return mVertLines.size();
}

float GameMap::getFirstLineX()
{
	return (float)(kFirstX + kGridSize/2);
}

float GameMap::getLastLineX()
{
	return (float)(kFirstX + kGridSize * (kTotalGrid - 1) + kGridSize/2);
}

float GameMap::getXAtLine(int lineIndex)	// lineIndex=0, 1, 2, 3
{
	return (float)(kFirstX + kSpacing * lineIndex + kGridSize/2);
}


#pragma mark - Helper Functions
float GameMap::getClostestLineX(float touchPointX)
{
    float resultX = kFirstX + kGridSize/2;
    
    if(touchPointX < kFirstX){
		return resultX;
	}
	
    if(touchPointX >= mVertLines[mVertLines.size()-1]){return mVertLines[mVertLines.size()-1];}
    
    
    for(int i=1; i<kVertLineCount;i++ )
    {
        float firstLineX = mVertLines[i-1];
        float secondLineX = mVertLines[i];
        
        if(touchPointX>=firstLineX && touchPointX<= secondLineX)
        {
            
            if (fabs((touchPointX - firstLineX)) <= fabs(touchPointX -secondLineX))
            {
                resultX = firstLineX;
            }
            else { resultX = secondLineX;}
            break;
        }
        
    }
    
    return resultX;

}



Vec2 GameMap::getIntersectPoint(Vec2 slopeLineStartPoint ,Vec2 slopeLineEndPoint)
{
    std::vector<Vec2> intersectPointsArray = std::vector<Vec2>();
    
    intersectPointsArray.clear();
    
    Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();

    
    for(int i=0; i<kVertLineCount;i++ )
    {
        Vec2 verticalLineStart = Vec2(getVertLineX(i), 0);
        Vec2 verticalLineEnd = Vec2(getVertLineX(i), size.height);

        if( Vec2::isSegmentIntersect(verticalLineStart, verticalLineEnd, slopeLineStartPoint, slopeLineEndPoint))
        {
            Vec2 intersectPoint = Vec2::getIntersectPoint(verticalLineStart, verticalLineEnd, slopeLineStartPoint, slopeLineEndPoint);
            

            bool isWithinStartPoint = (
                                        (fabs(intersectPoint.y - slopeLineStartPoint.y)<1)
                                                &&
                                        (fabs(intersectPoint.x - slopeLineStartPoint.x) <=5)
            
                                        );
           
            if(!intersectPoint.isZero() && !intersectPoint.equals(slopeLineStartPoint) && !isWithinStartPoint)
            {
                intersectPointsArray.push_back(intersectPoint);
            }
        }        
        
    }
    
    
    if(intersectPointsArray.size()<=0){return Vec2::ZERO;}
    
    
    int closetestPointIndex = 0;
    float closetestDistance  = size.height;

    for(int k=0; k< intersectPointsArray.size() ;k++)
    {
        Vec2 tempPoint = intersectPointsArray[k];
        float distance = fabs(slopeLineStartPoint.x - tempPoint.x);
        
        if(distance < closetestDistance)
        {
            closetestDistance = distance;
            closetestPointIndex = k;
        }
        
        
    }
    
    return intersectPointsArray[closetestPointIndex];
    

}

#pragma mark - New Logic
RouteLineNode *GameMap::createRouteLine(Node *parent, const Vec2 &start, const Vec2 &end)
{
	
	Vec2 relativeEnd = end - start;
	
	RouteLineNode *node = RouteLineNode::create();
	node->setRouteLineImage(mRouteImage);
	parent->addChild(node);
	
	node->setSegmentScale(mLineScale);
	
	node->setPosition(start);
	node->setStart(Vec2::ZERO);
	node->setEnd(relativeEnd);
//	
//	node->setPosition(Vec2::ZERO);
//	node->setStart(start);
//	node->setEnd(end);
	
	node->setUseDrawNode(false);
	node->updateLine();

	return node;
}


void GameMap::setStage(int stage)
{
	mStage = stage;
	mRouteImage = StringUtils::format("scene/stage%02d_create_line.png", mStage);
}

void GameMap::addSlopeLines(const Vector<MapLine *> &newLines)
{
	for(MapLine *line : newLines) {
		// log("debug: %s", line->toString().c_str());

		addSlopeLine(line);
	}
	
}

bool GameMap::isOnVertLine(float x)
{
	
//	int numLine = kVertLineCount;
//	int x = kFirstX + kGridSize/2;
//	
//	for(int i=0; i<numLine; i++) {
//		mVertLines.push_back(x);
//		x += kGridSize * (kGridBetweenLine + 1);
//	}
	
	float offsetX = x - (kFirstX + kGridSize/2);
	float modValue = kGridSize * (kGridBetweenLine + 1);
	float modResult = fmod(offsetX, modValue);
	
//	for(int vertX : mVertLines) {
//		if(vertX == x) {
//			return true;
//		}
//	}
	
	return modResult == 0.0f;
}


void GameMap::addSlopeLine(MapLine *mapLine)
{
	mHoriLines.pushBack(mapLine);
	
	RouteLineNode *routeNode = addRouteLine(mapLine);
	
	mLineRouteMap.insert(mapLine, routeNode);
	
//	redrawLines();
}

RouteLineNode *GameMap::getRouteLineNode(MapLine *line)
{
	return mLineRouteMap.at(line);
}

RouteLineNode * GameMap::addRouteLine(MapLine *line)
{
	return createRouteLine(mSlopeLineLayer, line->getStartPoint(), line->getEndPoint());
}


void GameMap::removeRouteLine(MapLine *mapLine)
{
	RouteLineNode *node = getRouteLineNode(mapLine);
	if(node == nullptr) {
		log("error: no routeNode for [%s]", mapLine->toString().c_str());
		return;	// not exist
	}
	
	node->removeFromParent();			// remove from the screen
	mLineRouteMap.erase(mapLine);		// remove from the mapping
	
}

float GameMap::getOffsetY()
{
	return mOffsetY;
}

bool GameMap::shouldMapLineCleanup(MapLine *line)
{
	if(line == nullptr) {	// protection
		return false;
	}
	
	// define the threshold for checking which line can be cleaned up
	float thresholdY = mOffsetY - kCleanupOffset;
	float checkY = MAX(line->getStartPoint().y, line->getEndPoint().y);
	
	return checkY < thresholdY;
}

// Note:
//		the data structure holding the mapLine
//		- MapLayer
//		- mLineRouteMap
//		- mHoriLines

void GameMap::cleanupOldRouteLine()
{
	int idx = 0;
	std::vector<MapLine *> deleteList;	// No need to use cocos::Vector, that save retain/release call
	
	
	
	Vector<MapLine *>::iterator it = mHoriLines.begin();
	for(; it != mHoriLines.end(); it++) {
		MapLine *mapLine = *it;
		if(mapLine == nullptr) {
			continue;
		}

		bool shouldRemove = shouldMapLineCleanup(mapLine);
		//log("debug: %s", mapLine->toString().c_str());
		//log("debug:   shouldCleanup=%d", shouldRemove);

		if(shouldRemove == false) {
			continue;
		}
		
		
		// Start Erase
		removeRouteLine(mapLine);
		
		// mHoriLines.erase(it);
		
		deleteList.push_back(mapLine);
		
		idx++;
	}
	
	
	// Delete the erased objects
	for(MapLine *line : deleteList) {
		mHoriLines.eraseObject(line);
	}
	
	
	// log("Deleted Lines: %s", VECTOR_TO_STR(deleteList).c_str());
}

void GameMap::cleanupPlayerRouteLine()
{
	int idx = 0;
	std::vector<MapLine *> deleteList;	// No need to use cocos::Vector, that save retain/release call
	
	
	
	Vector<MapLine *>::iterator it = mHoriLines.begin();
	for(; it != mHoriLines.end(); it++) {
		MapLine *mapLine = *it;
		if(mapLine == nullptr) {
			continue;
		}
		if(mapLine->getType() == MapLine::eTypeFix) {
			continue;
		}
		
		// Start Erase
		removeRouteLine(mapLine);
		
		// mHoriLines.erase(it);
		
		deleteList.push_back(mapLine);
	}
	
	// Delete the erased objects
	for(MapLine *line : deleteList) {
		mHoriLines.eraseObject(line);
	}
	
}

int GameMap::getApproachingLineIndex(float currentX, Dir dir)	// dir should be dirLeft or dirRight
{
	if(dir == DirRight) {
		for(int i=0; i<kVertLineCount; i++) {
			float lineX = getVertLineX(i);
			
			if(currentX >= lineX) {
				continue;
			}
			
			return i;
		}
		
		return -1;		// error value
	} else if(dir == DirLeft) {
		for(int i=kVertLineCount-1; i<kVertLineCount; i--) {
			float lineX = getVertLineX(i);
			
			if(currentX <= lineX) {
				continue;
			}
			
			return i;
		}
		return -1;		// error value
	} else {
		return -1;
	}
}


Vec2 GameMap::adjustPositionBeforeLastDropLine(const Vec2 &checkPos)
{
	
	float maxY = -1;
	
	for(MapLine *mapLine : mHoriLines) {
		if(mapLine == nullptr) {
			continue;
		}
		
		if(mapLine->getType() == MapLine::eTypeFix) {
			continue;
		}
		
		Vec2 startPos = mapLine->getStartPoint();
		Vec2 endPos = mapLine->getEndPoint();
		Vec2 posList[2] = {startPos, endPos};
		
		for(int i=0; i<2; i++) {
			Vec2 linePos = posList[i];
			
			
			if(linePos.x == checkPos.x) { // Able to check
				if(linePos.y > maxY) {
					maxY = linePos.y;
				}
			}
		}
	}
	
	if(maxY <= 0) {
		return Vec2::ZERO;
	}
	
	if(checkPos.y >= maxY) {
		return Vec2::ZERO;
	}
	
	Vec2 resultPos = checkPos;
	
	resultPos.y = maxY + 1;
	
	return resultPos;
}

