//
//  EffectLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/6/2016.
//
//

#ifndef EffectLayer_hpp
#define EffectLayer_hpp

#include <stdio.h>


#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "MovableModel.h"
#include "Player.h"
#include "Item.h"
#include <set>
#include <map>
#include "GameRes.h"
#include "ItemEffectIndicator.h"		// because using in Vector


class Enemy;
class Star;

USING_NS_CC;

class EffectLayer : public cocos2d::LayerColor
{
public:
	enum Effect {
		MissileExplode	= 1,
		ShieldBreak		= 2,
	};
    
    enum PowerUpType {
        SpeedUp			= 3,
        Magnet			= 4,
        CashOut			= 5,
    };
    
    enum BoosterType {
        Shield			= 6,
        TimeStop		= 7,
    };
	
public:
	CREATE_FUNC(EffectLayer);
	
	CC_SYNTHESIZE(Vec2, mScorePosition, ScorePosition);
	CC_SYNTHESIZE(Vec2, mCoinPosition, CoinPosition);
	
	EffectLayer();
	
	virtual bool init();
    void update(float delta);
	
	void setOffsetY(float y);
	void showEffect(GameRes::Particle particle, const Vec2 &pos);

	void reset();
	
	
	
	void addNodeToScrollLayer(Node *node);
	void addNodeToScreenLayer(Node *node);
	
	void showEnemyAlert(float x, bool isTop, int ID,const std::function<void()> &callback);	// isTop
    void showNotEnoughEnergyAlert();
    void showDoubleStarEffect();
    void showDeduceEnergyEffect(int energy);
	
    void eraseCautionById(int ID);
    void eraseAllCaution();
    void pauseAllCaution();
    void resumeAllCaution();
    
	Vec2 toScreenPosition(const Vec2 &mapPosition);
	
	void showCollectCapsuleEffect(int itemEffectType, const Vec2 &callerPos,
								  const std::function<void()> &callback=nullptr);
	
	void showConsumeCoinEffect(const Vec2 &pos);
	void showSlowEffect();

    
	void showScreenParticle(const std::string &particle, const Vec2 &position);

	void showPowerUpEffect(const Vec2 &position);

	
	void showCollectItemEffect(const std::string &imageFile,
							   float scale,
							   const Vec2 &startPos,
							   const Vec2 &toPos,
							   const std::function<void()> &callback=nullptr);
	
	void showCollectManyCandyEffect(int totalAmount, const Vec2 &startPos);
	void showCollectCandyEffect(int amount, const Vec2 &startPos);
	
	void showCollectManyStarEffect(const std::vector<int> &amount, const Vec2 &startPos);
	
	void showCollectManyStarEffect(int totalAmount, const Vec2 &startPos);
	void showCollectStarEffect(int amount, const Vec2 &startPos);
	
	void cleanup();
	
    void playCsbAnime(const std::string &csbName, const std::string &animeName, bool isLoop, bool removeSelf,
                      float scale, const Vec2 &pos, const std::function<void ()> endAnimeCallback = nullptr);
    
    void removeCsbAnimeByID(int animeID);
    
    void resetIndicator();
    void addPowerUpIndicator(PowerUpType type, float time);
    void addBoosterIndicator(BoosterType type, float time);
	

	
	void runAddScoreAnimation(const int &score, const Vec2 &startPos,
							  const std::function<void ()> &endAnimeCallback = nullptr,
							  const float &duration = 0.5f);
	void runAddCoinAnimation(const Vec2 &startPos,
							 const std::function<void ()> &callback,
							 const float &duration);
    
    void reorganizeIndicatorPos(ItemEffectIndicator* finishedIndicator);
	
	
	std::string statInfo();
	
#pragma mark - SpeedUp
public:
	void showWindEffect();
	void hideWindEffect();
	
private:
	void setupWindEffect();
	
private:
	Sprite *mWindEffect;
	
	
#pragma mark - Other
private:
	void addStarToPlayer(int amount);
	void addCandyToPlayer(int amount);
	
private:
	Layer *mScrollLayer;		// the layer scroll with the map
	Layer *mScreenLayer;		// the layer fix with screen
	float mOffsetY;				//
    std::map<int, Sprite*> mCautionMap;
	int mStarCollected;
	int mCandyCollected;
    int mAnimeID;
    Map<int, cocostudio::timeline::ActionTimeline *> mTimelineMap;
    Map<int, Node *> mCsbNodeMap;
    std::map<int, std::function<void()>> mCallbackMap;
    Vector<ItemEffectIndicator *> mItemEffectList;
    
};


#endif /* EffectLayer_hpp */
