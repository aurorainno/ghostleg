//
//  PositionRecorder.cpp
//  GhostLeg
//
//  Created by Ken Lee on 7/4/2017.
//
//

#include "PositionRecorder.h"
#include "StringHelper.h"
#include "GameMap.h"

PositionRecorder::PositionRecorder()
: Ref()
, mInterval(0.1)
, mCooldown(0)
, mSampleSize(100)
, mQueueVector()
{
	
}

void PositionRecorder::reset()
{
	mCooldown = 0;
	
	// clear the queue
	mQueueVector.clear();
}

void PositionRecorder::recordPositionNow(const Vec2 &pos)
{
	if(mQueueVector.size() >= mSampleSize) {
		dequeueFront();
	}
	
	mQueueVector.push_back(pos);
	mCooldown = mInterval;
}

void PositionRecorder::recordPosition(const Vec2 &pos, float timeDelta)
{
	mCooldown -= timeDelta;
	if(mCooldown > 0) {
		return;	// wait later
	}
	
	recordPositionNow(pos);
}

void PositionRecorder::dequeueFront()
{
	mQueueVector.erase(mQueueVector.begin());
}

std::string PositionRecorder::toString()
{
	std::string result = StringUtils::format("QueueSize: %ld\n", mQueueVector.size());
	
	for(Vec2 pos : mQueueVector) {
		result += POINT_TO_STR(pos);
		result += " ";
	}

	
	return result;
}

bool PositionRecorder::findNearestSpawnPosition(const Vec2 &playerPos, Vec2 &outPos)
{
	// float timeBound = 0.4;		// the position at 1 second before
	
	float distanceY;
	//float distanceThreshold = 130;
	float distanceThreshold = 50;
	
	Vec2 position = Vec2::ZERO;
	// float timeElapse = 0.0;
	
	int lastIdx = (int) (mQueueVector.size()-1);
	
	for(int i=lastIdx; i>=0; i--) {
		Vec2 myPos = mQueueVector[i];
		
		
		// timeElapse += mInterval;
		
		if(GameMap::isOnVertLine(myPos.x) == false) {
			continue;
		}
		
		if(playerPos.y < myPos.y) {	// don't count the position above the player
			continue;
		}
		
		position = myPos;
		
	
		
		distanceY = fabs(playerPos.y - position.y);
		if(distanceY >= distanceThreshold) {
			break;
		}
		
//		// Ending condition
//		if(timeElapse >= timeBound ) {
//			break;
//		}
	}
	
	outPos = position;

	return position != Vec2::ZERO;
}

//bool findNearestSpawnPosition(Vec2 &outPos)
//{
//	std::vector<Vec2> top5Positions;
//	float firstTimeElapse;
//	
//	
//}
