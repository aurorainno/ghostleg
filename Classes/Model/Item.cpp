//
//  Item.cpp
//  GhostLeg
//
//  Created by Ken Lee on 21/3/2016.
//
//

#include "Item.h"
#include "MovableModel.h"
#include "GameWorld.h"
#include "TimeMeter.h"

Item::Item(Type type)
: mType(type)
, mUsed(false)
, mConsumed(false)
, mAccel(0)
, mCsbFile("")
, mHasSetup(false)
{
	
};


bool Item::init()
{
	bool isOk = GameModel::init();
	if (isOk == false)
	{
		log("Item: fail to create");
		return false;
	}
	
	setUseQuickHitboxCheck(true);
	
	return true;
}

void Item::update(float delta)
{
	if(mHasSetup == false) {
		if(willBecomeVisible()) {
			TSTART("item.setupCsb");
			setupCsb();		//
			TSTOP("item.setupCsb");
			//log("DEBUG: setupCsb: %s", mCsbFile.c_str());
		} else {
			return;		// no need to continue to upcoming codes
		}
	}
	
	
	
	// Update the movement
	if(mSpeed.x == 0 && mSpeed.y == 0) {
//        log("---------\nstar%s speed is zero\n---------",getName().c_str());
		return;
	}
    
    mSpeed += Vec2(mAccel,mAccel) * delta;
	
	// Move to a new position
	Vec2 newPos = getPosition() + delta * mSpeed;
	setPosition(newPos);
	
}


bool Item::shouldBeCleanUp(){
    float bottomMargin = GameWorld::instance()->getCameraY() - 200;
    return getPosition().y < bottomMargin;
}

bool Item::use()
{
	//log("Please implement the use method for type=%d", mType);
	if(mUsed) {
		//log("item already used");
		return false;
	}

	// Fade out the and disappear
	useStart();
	
	setAction(Action::Consume);
	
	// fadeOut();	// will call useEnd when finish the animation
	
	
	return true;
}

void Item::useStart()	// End of use item animation
{
	mUsed = true;
}

void Item::useEnd()		// End of use item animation
{
	// log("Item:useEnd: is called");
	//removeFromParent();
}

bool Item::isUsed()
{
	return mUsed;
}


std::string Item::toString()
{
	char temp[200];
	
	std::string result = "";
	
	Vec2 pos = getPosition();
	sprintf(temp, "pos:(%.2f, %.2f)", pos.x, pos.y);
	result += temp;
	
	sprintf(temp, " type=%d", mType);
	result += temp;
	
	return result;
}

bool Item::isCollide(MovableModel *otherModel)
{
	Rect myRect = this->getBoundingBox();
	
	Rect otherRect = otherModel->getBoundingBox();
		
	return myRect.intersectsRect(otherRect);
}


CallFunc *Item::endUseAction()
{
	
	CallFunc *endCall = CallFunc::create(
										 // lambda
										 [&](){
											 Item::useEnd();	// calling the outside
										 });

	return endCall;
}

void Item::fadeOut()
{
	MoveBy *moveBy = MoveBy::create(0.5f, Vec2(0, 50));
	
	FadeOut *fadeOut = FadeOut::create(0.5f);
	
	Spawn *parellelAction = Spawn::create(fadeOut, moveBy, nullptr);
	
	CallFunc *endCall = endUseAction();
	
	Sequence *seq = Sequence::create(parellelAction, endCall, nullptr);
	
	this->runAction(seq);
}

void Item::setup(const std::string &csbName)
{
	GameModel::setup(csbName);
	
	
	std::function<void ()> callback = [&](){
		Item::useEnd();	// calling the outside
	};
	
	setAnimationEndCallFunc(Action::Consume, callback);
	
	mHasSetup = true;
}


Item::Type Item::getType()
{
	return mType;
}

void Item::markUsed(bool flag, bool autoRemove)
{
	mUsed = flag;
	
	if(mUsed && autoRemove) {
		setVisible(false);
		removeFromParent();
	}
}

std::string Item::getName()
{
	return StringUtils::format("item-%d", mType);
}


bool Item::isCollidePlayer(const Rect &playerRect)
{
	Rect myRect = getFinalMainBoundingBox(false);
	return myRect.intersectsRect(playerRect);
}


#pragma mark - Lazy loading for the Csb
//protected:
void Item::setupCsb()
{
	if(mCsbFile == "") {
		log("%s::setupCsb: csb undefined.", getName().c_str());
		return;
	}
	
	setup(mCsbFile);	// will turn 'mHasSetup' to true when success
}
//
//protected:
//std::string mCsbFile;
//bool mHasSetup;		// false when animation(csb) not setup
