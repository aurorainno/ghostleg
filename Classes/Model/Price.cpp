//
//  Price.cpp
//  GhostLeg
//
//  Created by Ken Lee on 20/1/2017.
//
//

#include "Price.h"


Price::Price()
: type(MoneyTypeStar)
, amount(0)
{
	
}

Price::Price(MoneyType _type, int _amount)
: type(_type)
, amount(_amount)
{
	
}

std::string Price::typeToString(MoneyType type)
{
	switch (type) {
		case MoneyTypeStar			: return "star";
		case MoneyTypeDiamond		: return "diamond";
		case MoneyTypeCandy			: return "candy";
		default						: return "unknown";
	}
}

std::string Price::toString()
{
	return StringUtils::format("%d %s", amount, typeToString(type).c_str());
}

