//
//  FireWork.hpp
//  GhostLeg
//
//  Created by Calvin on 27/10/2016.
//
//

#ifndef FireWork_hpp
#define FireWork_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "GameRes.h"

// Cocos Studio
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
using namespace cocostudio::timeline;


USING_NS_CC;

class FireWork : public Node
{

public:
    CREATE_FUNC(FireWork);
    
    FireWork();
    virtual ~FireWork();
    
    virtual bool init();
    
    CC_SYNTHESIZE_RETAIN(cocostudio::timeline::ActionTimeline *, mTimeLine, TimeLine);
    
    virtual void setup(const std::string &csbName,std::string animationName);
    
//    virtual void setAnimationEndCallFunc(Action action, std::function<void()> func);

    void playAnimation();
    
private:
    std::string mAnimationName;
    Node* mRootNode;
    float mOriginTimeSpeed;
    
#pragma mark - FrameEvent Handling
    virtual void onFrameEvent(Frame *frame);	// note: override method should use it too
    void handlePlayParticleEvent(EventFrame *event);
};



#endif /* FireWork_hpp */
