//
//  DailyReward.cpp
//  GhostLeg
//
//  Created by Ken Lee on 21/11/2016.
//
//

#include "DailyReward.h"
#include "RewardManager.h"
#include "TimeHelper.h"
#include "StringHelper.h"

#define kKeyRewardProfileMap "ghostleg.rewardProfileMap"
#define kKeyRewardProfileCurrentCycle "ghostleg.rewardProfileCurrentCycle"
#define kKeyRewardProfileLastCollectedDay "ghostleg.rewardProfileCollectedDay"

RewardData::RewardData()
: Ref()
, mType(RewardType::RewardTypeStar)
, mObjectID(0)
, mCount(1)
{
	
}

RewardData *RewardData::create(RewardType type, int objectID, int count)
{
//	__TYPE__ *pRet = new(std::nothrow) __TYPE__(); \
//	if (pRet && pRet->init()) \
//	{ \
//		pRet->autorelease(); \
//		return pRet; \
//	} \
//	else \
//	{ \
//		delete pRet; \
//		pRet = nullptr; \
//		return nullptr; \
//	} \

	RewardData *data = new RewardData();
	data->setType(type);
	data->setObjectID(objectID);
	data->setCount(count);
	
	data->autorelease();
	
	return data;
}

RewardData::RewardType RewardData::getRewardTypeByName(std::string typeStr)
{
    if(typeStr == "star"){
        return RewardData::RewardTypeStar;
    }else if(typeStr == "diamond"){
        return RewardData::RewardTypeDiamond;
    }else if(typeStr == "fragment"){
        return RewardData::RewardTypeFragment;
    }
    
    log("cannot get correct reward type!!");
    return RewardData::RewardTypeStar;
}

std::string RewardData::toString()
{
	std::string result = "";
	
	result += StringUtils::format("Type=%d", mType);
	result += StringUtils::format(" objectID=%d", mObjectID);
	result += StringUtils::format(" count=%d", mCount);
	
	return result;
}

#pragma mark - DailyReward

DailyReward::DailyReward()
: Ref()
, mRewardMap()
, mDefaultRewardMap()
{
	
	// Define the Default Reward
	mDefaultReward.setType(RewardData::RewardTypeStar);
	mDefaultReward.setObjectID(0);
	mDefaultReward.setCount(100);
 //   mCycleRange = RewardManager::instance()->getCycleRange();
    
}


void DailyReward::addReward(int day, RewardData *data)
{
	mRewardMap.insert(day, data);
}

RewardData *DailyReward::getRewardDataAtDay(int day)
{
	RewardData *result = mRewardMap.at(day);
	
	if(result == nullptr) {
		return &mDefaultReward;
	}
	
	return result;
}

bool DailyReward::haveRewardDataAtDay(int day)
{
	return mRewardMap.find(day) != mRewardMap.end();
}

RewardData *DailyReward::getRewardAtDay(int cycle, int day)
{
	// log("Getting Reward for cycle=%d day=%d", cycle, day);
	// Correct
	if(day <= 0) {
		day = 1;
	} else if(day > mCycleRange) {
		day = mCycleRange;
	}
	
	
	
	int actualDay = cycle * mCycleRange + day;
	if(haveRewardDataAtDay(actualDay)) {
		return getRewardDataAtDay(actualDay);
	} else {
		return getDefaultRewardAtDay(day);		// using the default reward
	}
	
//	// if no reward found, use the second cycle (cycle1) reward first, then the first cycle (cycle0)
//	// use the
//	int dayCycle1 = mCycleRange + day;
//	if(haveRewardDataAtDay(dayCycle1)) {
//		return getRewardDataAtDay(dayCycle1);
//	}
//	
//	return getRewardDataAtDay(day);
}


RewardData *DailyReward::getRewardAtDay(int day)
{
	int cycle = (day - 1) / mCycleRange;
	int cycleDay = (day - 1) % mCycleRange + 1;
	
	//log("Getting Reward for day=%d cycle=%d cycleDay=%d", day, cycle, cycleDay);
	
	return getRewardAtDay(cycle, cycleDay);
}

Vector<RewardData *> DailyReward::getRewardList(int cycle)
{
	Vector<RewardData *> result;
	for(int day=1; day<=mCycleRange; day++) {
		result.pushBack(getRewardAtDay(cycle, day));
	}
	
	return result;
}

std::string DailyReward::toString()
{
	std::string result = "";

	
	result += "Default Reward\n";
	result += toStringOfDefaultReward();
	result += "\n";
	
	result += "Daily Reward\n";
	result += toStringOfDailyReward();
	result += "\n";
	
	
	return result;
}

std::string DailyReward::toStringOfDailyReward()
{
	return rewardMapToString(mRewardMap);
}

void DailyReward::cleanup()
{
	mRewardMap.clear();
}


std::string DailyReward::rewardMapToString(const Map<int, RewardData *> &rewardMapRef)
{
	Map<int, RewardData *> rewardMap = rewardMapRef;
	
	
	std::string result = "";
	Map<int, RewardData *>::iterator it = rewardMap.begin();
	for(; it != rewardMap.end(); it++) {
		int day = it->first;
		RewardData *reward = it->second;
		
		// log("day %d: %s", day, reward->toString().c_str());
		result += StringUtils::format("Day %d:", day);
		result += reward->toString() + "\n";
	}
	
	return result;

}


#pragma mark - Default Reward
void DailyReward::setDefaultReward(int day, RewardData *data)
{
	mDefaultRewardMap.insert(day, data);
}

RewardData *DailyReward::getDefaultRewardAtDay(int day)
{
	RewardData *result = mDefaultRewardMap.at(day);
	
	if(result == nullptr) {
		return &mDefaultReward;
	}
	
	return result;
}

std::string DailyReward::toStringOfDefaultReward()
{
	return rewardMapToString(mDefaultRewardMap);
}


#pragma mark - Daily Reward Profile

DailyRewardProfile::DailyRewardProfile()
: Ref()
, mCurrentCycle(0)
, mCollectedFlagMap()
, mLastCollectedDate(-1)
{
	
}


void DailyRewardProfile::reset()
{
    mCollectedFlagMap.clear();
    mCurrentCycle = 0;
    mLastCollectedDate = -1;
}

void DailyRewardProfile::resetCollectedFlag()
{
    mCollectedFlagMap.clear();
}

void DailyRewardProfile::markDayCollected(int day)
{
    mLastCollectedDate = day;
    for(int i=0;i<mCycleRange;i++){
        bool result = mCollectedFlagMap[i];
        if(result == false){
            mCollectedFlagMap[i] = true;
            break;
        }
    }
}

void DailyRewardProfile::increaseCycle()
{
    mCurrentCycle++;
}

int DailyRewardProfile::getCurrentDayWithinACycle()
{
    for(int day=0;day<mCycleRange;day++)
    {
        if(!isDayCollected(day)){
            return day + 1;
        }
    }
//    log("All five days are collected when try to get first uncollected day, maybe need to reset first?");
    return mCycleRange+1;
}

bool DailyRewardProfile::isDayCollected(int day)
{
    return mCollectedFlagMap[day] == true;
}

bool DailyRewardProfile::isCollectedToday(int givenDay)
{
    if(givenDay == -1){
        givenDay = TimeHelper::getCurrentTimeStampInDays();
    }
    
    return mLastCollectedDate == givenDay;
}

bool DailyRewardProfile::isCollectedYesterday(int givenDay)
{
    if(givenDay == -1){
        givenDay = TimeHelper::getCurrentTimeStampInDays();
    }
    
    return givenDay - mLastCollectedDate <= 1;
}

bool DailyRewardProfile::isAllDayCollected()
{
    for(int i=0;i<mCycleRange;i++){
        bool result = mCollectedFlagMap[i];
        if(result == false){
            return false;
        }
    }
    return true;
}

std::string DailyRewardProfile::toString()
{
    std::string result = "";
    std::map<int, bool>::iterator it = mCollectedFlagMap.begin();
    for(; it != mCollectedFlagMap.end(); it++) {
        int day = it->first;
        bool collected = it->second;
        
        // log("day %d: %s", day, reward->toString().c_str());
        result += StringUtils::format("Day %d ", day);
        result += StringUtils::format("collected: %d ", collected);
    }
    
    result += StringUtils::format("cycle: %d", mCurrentCycle);
    result += StringUtils::format("lastDay: %d", mLastCollectedDate);
    
    return result;
}

void DailyRewardProfile::save()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("DailyRewardProfile::save: userDefault not available");
        return;
    }
    
    std::string rewardProfileCurrentCycle = "";
    std::string rewardProfileLastCollectedDay = "";
    std::string rewardProfileMap = "";
    
    rewardProfileCurrentCycle += "currentCycle=" + INT_TO_STR(mCurrentCycle);
    
    rewardProfileLastCollectedDay += "lastCollectedDay=" + INT_TO_STR(mLastCollectedDate);
    
    for(std::map<int,bool>::iterator it = mCollectedFlagMap.begin();it!=mCollectedFlagMap.end();it++)
    {
        int day = it->first;
        int collected = (int)it->second;
        rewardProfileMap += "day=" + INT_TO_STR(day);
        rewardProfileMap += " collected=" + INT_TO_STR(collected);
        rewardProfileMap += "\n";
    }
//    for (int dogID : mNewDogList) {
//        int id = dogID;
//        newDogList += "dogID=" + INT_TO_STR(id);
//        newDogList += "\n";
//    }
    
    userData->setStringForKey(kKeyRewardProfileCurrentCycle, rewardProfileCurrentCycle);
    userData->setStringForKey(kKeyRewardProfileLastCollectedDay, rewardProfileLastCollectedDay);
    userData->setStringForKey(kKeyRewardProfileMap, rewardProfileMap);
}

void DailyRewardProfile::load()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("DailyRewardProfile::load: userDefault not available");
        return;
    }
    
    mCollectedFlagMap.clear();
    mLastCollectedDate = -1;
    mCurrentCycle = 0;
    
    std::string content = userData->getStringForKey(kKeyRewardProfileMap, "");
    
    if(content != "") {
        std::stringstream ss(content);
        std::string to;
        
        while(std::getline(ss, to, '\n')){
            int day;
            int collected;
            
            sscanf(to.c_str(), "day=%d collected=%d", &day, &collected);
            
            mCollectedFlagMap[day] = (bool)collected;
        }
    }
    
    content = userData->getStringForKey(kKeyRewardProfileLastCollectedDay, "");
    
    if(content!=""){
        sscanf(content.c_str(), "lastCollectedDay=%d", &mLastCollectedDate);
    }
    
    content = userData->getStringForKey(kKeyRewardProfileCurrentCycle, "");
    
    if(content!=""){
        sscanf(content.c_str(), "currentCycle=%d", &mCurrentCycle);
    }

    
}


