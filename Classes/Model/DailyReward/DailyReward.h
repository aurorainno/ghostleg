//
//  DailyReward.hpp
//  GhostLeg
//
//  Created by Ken Lee on 21/11/2016.
//
//

#ifndef DailyReward_hpp
#define DailyReward_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "CommonMacro.h"

USING_NS_CC;

class RewardData : public Ref
{
public:
	enum RewardType {
		RewardTypeStar,
		RewardTypeDiamond,
        RewardTypeFragment,
	};

public:
	RewardData();
    
    static RewardData::RewardType getRewardTypeByName(std::string typeStr);
	
	static RewardData *create(RewardType type, int objectID, int count);
	
	CC_SYNTHESIZE(RewardType, mType, Type);
	CC_SYNTHESIZE(int, mObjectID, ObjectID);		// For Dog
	CC_SYNTHESIZE(int, mCount, Count);				// number of star, for dog: 1 pcs

	std::string toString();
};


class DailyReward : public Ref
{
public:
    DailyReward();
	
    CC_SYNTHESIZE(int, mCycleRange, CycleRange);	// Number of day per cycle
    
	void addReward(int day, RewardData *data);
	RewardData *getRewardAtDay(int cycle, int day);
	RewardData *getRewardAtDay(int day);
	
	
	//
	Vector<RewardData *> getRewardList(int cycle);	// cycle started from 0
 
	void cleanup();
	
	
	std::string toString();

	std::string toStringOfDailyReward();
	
#pragma mark - String
	std::string rewardMapToString(const Map<int, RewardData *> &rewardMap);
	
#pragma mark - Default Reward
	void setDefaultReward(int day, RewardData *data);
	RewardData *getDefaultRewardAtDay(int day);
	std::string toStringOfDefaultReward();

private:
	RewardData *getRewardDataAtDay(int day);	// mainly get the data, internal use
	bool haveRewardDataAtDay(int day);
	
private:
	Map<int, RewardData *> mRewardMap;		// key: Day (1 ~ N) Value: Reward
	
	Map<int, RewardData *> mDefaultRewardMap;		// key: Day (1 ~ N) Value: Reward
	
	RewardData mDefaultReward;

};



class DailyRewardProfile : public Ref
{
public:
	DailyRewardProfile();
	
    void save();
    void load();
    
	void reset();					// reset everything
	void resetCollectedFlag();		// reset all flag
	void increaseCycle();
	void markDayCollected(int day);
	bool isDayCollected(int day);
	
	bool isCollectedToday(int givenDate = -1);		// givenDate = Number of day after 2016/1/1
												// result: No Reward Popup
	
	bool isCollectedYesterday(int givenDate = -1);	// givenDate = Number of day after 2016/1/1
												// result: reset if false
	
	bool isAllDayCollected();					// true: increase reward cycle
    
    int getCurrentDayWithinACycle();
    
	std::string toString();
	
public: // data
    CC_SYNTHESIZE(int, mCycleRange, CycleRange);	// Number of day per cycle
	CC_SYNTHESIZE(int, mCurrentCycle, CurrentCycle);	// Number of day per cycle

	CC_SYNTHESIZE(int, mLastCollectedDate, LastCollectedDate); // the (Day After 2016) when at last reward collection
	
	std::map<int, bool> mCollectedFlagMap;		// key: day in the cycle (Day 1, Day 2, Day 3, ...)
};



#endif /* DailyReward_hpp */
