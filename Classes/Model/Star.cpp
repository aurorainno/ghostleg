//
//  Star.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/5/2016.
//
//

#include "Star.h"
#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"
#include "RandomHelper.h"
#include "ViewHelper.h"
#include "ModelLayer.h"
#include "GeometryHelper.h"
#include "EffectLayer.h"
#include "GameplaySetting.h"
#include "TimeMeter.h"

const int kAcceleration = 10.0;

const std::string Star::kSmallStarCsbFile  = "item/item_star1.csb";
const std::string Star::kMediumStarCsbFile = "item/item_star2.csb";
const std::string Star::kLargeStarCsbFile  = "item/item_star3.csb";

Star::Star(Star::StarType type)
: Collectable(parseStarType(type))
, mStarType(type)
{
	
}

Item::Type Star::parseStarType(Star::StarType type)
{
    switch (type) {
        case SmallStar:			return Item::ItemSmallStar;
        case MediumStar:		return Item::ItemMediumStar;
        case LargeStar:			return Item::ItemBigStar;
        default:				return Item::ItemSmallStar;
    }
}

bool Star::init()
{
	if(Collectable::init() == false) {
		log("Star: init failed");
		return false;
	}
    
    std::string csbFile;
    
    if(mStarType == SmallStar) {
        csbFile = kSmallStarCsbFile;
    } else if(mStarType == MediumStar){
        csbFile = kMediumStarCsbFile;
    } else{
        csbFile = kLargeStarCsbFile;
    }
    setScale(1.0);
    
    
#if IS_ANDROID
    mCsbFile = csbFile;		// this help less lagging, lazy loading
#else
    setup(csbFile);	// note: default Action is idle
#endif
    return true;

}

bool Star::use()
{
	TSTART("Star.use");
	bool isUsed = Item::use();
	
	if(isUsed == false) {
		return false;
	}
	
	// Make sound effect
	TSTART("Coin.playSound");
    GameSound::playStarSound();
	TSTOP("Coin.playSound");
	// Sound sound = (Sound)(GameSound::Star1 + idx);	// if idx=2, Sound = Star3 (1+2)
	
	//GameSound::playSound(GameSound::Star4);
	// GameSound::playCoinSound();
	//TimeMeter::instance()->stop("Coin.playSound");
	//GameSound::playSound(GameSound::Coin);
	
	
	// Need a double star logic
	bool isDoubleStar = canTriggerDoubleStar();
    int starValue = 1;
    
    if(isDoubleStar){
        starValue = 2;
        showDoubleStarEffect();
    }
	
	//XXX
	TSTART("Coin.collectCoin");
	GameWorld::instance()->collectCoin(starValue);		// note: collect coin already gain score
	TSTOP("Coin.collectCoin");
	
	TSTOP("Star.use");
	
	return true;
}

#pragma mark - Double Star

bool Star::canTriggerDoubleStar(){
    int start = 1, end = 100;
	int randValue = RandomHelper::random_int(start, end);		// aurora::RandomHelper::randomBetween(start, end);
	
	//GameplaySetting
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	int threshold = setting->getAttributeValue(GameplaySetting::DoubleStarPercent);

	//log("*** DEBUG: randValue=%d threshold=%d", randValue, threshold);
	
	return randValue <= threshold;
}

void Star::showDoubleStarEffect(){
//    ModelLayer* modelLayer = GameWorld::instance()->getModelLayer();
//    Vec2 pos = modelLayer->getPlayer()->getPosition();
    GameWorld::instance()->getEffectLayer()->showDoubleStarEffect();
}


