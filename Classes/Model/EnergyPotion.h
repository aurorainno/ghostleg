//
//  EnergyPotion.h
//  GhostLeg
//
//  Created by Gavin Chiu on 22/3/16.
//
//

#ifndef EnergyPotion_h
#define EnergyPotion_h



#include <stdio.h>
#include "Item.h"


class EnergyPotion : public Item
{
public:
    
#pragma mark - Public static Method
    CREATE_FUNC(EnergyPotion);
    
#pragma mark - Public Method and Properties
    EnergyPotion();
    
    virtual bool init();	// virtual because different derived class have different init logic
    virtual bool use();		// use when the Astrodog hit this GameMap Elements
};



#endif /* EnergyPotion_h */
