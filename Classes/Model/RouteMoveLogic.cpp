//
//  RouteMoveLogic.cpp
//  GhostLeg
//
//  Created by Ken Lee on 8/12/2016.
//
//

#include "RouteMoveLogic.h"
#include "MapLine.h"
#include "GeometryHelper.h"
#include "StringHelper.h"
#include "GameMap.h"

RouteMoveLogic::RouteMoveLogic()
: mWalkingRoute(nullptr)
, mGameMap(nullptr)
, mCrossingRoute(nullptr)
, mDefaultDir(DirUp)
{
	
}

bool RouteMoveLogic::init()
{
	
	return true;
}

void RouteMoveLogic::setGameMap(GameMap *map)
{
	mGameMap = map;
}

Vec2 RouteMoveLogic::handleMoveVertical(const Vec2 &newPos)
{
	if(mGameMap == nullptr) {
		return newPos;		// Weird case!!!
	}

	// Try to find the junction !!
	
	MapLine *mapLine;
	
	Vec2 startPos = mPosition;
	bool outMatchAtStart;		// if there is crossing, it is match the line start point
	
	mapLine = (MapLine *) mGameMap->willCrossAnyHoriLine(startPos, newPos, outMatchAtStart);
	
	if(mapLine == nullptr) {	// no crossing
		mNextDir = mDir;	// No change in the dir
		return newPos;
	}

	// Find the Next Dir
	bool isStartAtRight = mapLine->getStartPoint().x > mapLine->getEndPoint().x;
	
//	Vec2 leftPoint = needSwap ? mapLine->getEndPoint() : mapLine->getStartPoint();
//	Vec2 rightPoint = needSwap ? mapLine->getStartPoint() : mapLine->getEndPoint();
	
	
	
	log("Change Found: mapLine=%s", mapLine->toString().c_str());
	mNextDir = outMatchAtStart ? DirRight : DirLeft;
	Vec2 crossPos = outMatchAtStart ? mapLine->getStartPoint() : mapLine->getEndPoint();
	setCrossingRoute(MapLine::cloneMapLine(mapLine));
	
	return crossPos;
}

Vec2 RouteMoveLogic::handleMoveOnSlope(const Vec2 &newPos)
{
	if(mWalkingRoute == nullptr) {
		log("handleMoveOnSlope: mWalkingRoute is null");
		return mPosition;
	}
	
	Vec2 endPoint = mDir == DirRight ? mWalkingRoute->getEndPoint()
										: mWalkingRoute->getStartPoint();
	
	if(newPos == endPoint) {	// reach the end point,
		mNextDir = mDefaultDir;
		
		setCrossingRoute(nullptr);

		return newPos;
	}
	
	// The newPos is beyond or before the next Axis Line
		
	Vec2 startPos = mPosition; // + (Vec2(0.000001f, 0) * (mDir == DirRight ? 1 : -1)) ;
	
	int lineIndex = mGameMap->getApproachingLineIndex(mPosition.x, mDir);	// + 0.001 prevent getting the current vertical line
	if(lineIndex < 0) {	// Weird cases
		log("handleMoveOnSlope: invalid index: x=%f mDir=%d", mPosition.x, mDir);
		return mPosition;
	}
	
	float nextAxisX = mGameMap->getVertLineX(lineIndex);
	
	bool hasCross = aurora::GeometryHelper::checkIfCrossXAxis(startPos, newPos, nextAxisX);
	
	if(hasCross == false) {
		return newPos;
	}
	
	// Reach the end of the route line
	mNextDir = mDefaultDir;
	
	setCrossingRoute(nullptr);
	
	return endPoint;
}

Dir RouteMoveLogic::getNextDir()
{
	return mNextDir;
}

Vec2 RouteMoveLogic::calculateNewPosition(float delta)
{
	// Reset the output first
	setCrossingRoute(nullptr);
	mNextDir = DirNone;
	
	//
	Vec2 newPos = getNewPosition(delta);
	
	Vec2 resultPos;
	
	// Current state: Up & Down
	if(mDir == DirLeft || mDir == DirRight) {
		resultPos = handleMoveOnSlope(newPos);
	} else {
		resultPos = handleMoveVertical(newPos);
	}
	
	log("** DEBUG: newPos=%s mDir=%d oldPos=%s resultPos=%s",
		POINT_TO_STR(newPos).c_str(),
		mDir, POINT_TO_STR(mPosition).c_str(),
		POINT_TO_STR(resultPos).c_str());
	
	return resultPos;
}

Vec2 RouteMoveLogic::getNewPosition(float delta)
{
	// Vertical Line Case
	if(mWalkingRoute == nullptr) {
		// On vertical line
		float speedValue = fabsf(mSpeed.y);
		
		float dirValue = mDir == DirUp ? 1 : -1;
		
		Vec2 newPos = mPosition;
		newPos.y += delta * speedValue * dirValue;
		
		return newPos;
	}

	// Slope and Horizontal Line Case
	float speedValue = fabsf(mSpeed.x);
	
	Vec2 targetPos = mWalkingRoute->getSlopeDestinationPoint(mDir);
	
	//log("DEBUG: start=[%s] end=[%s]", POINT_TO_STR(mPosition).c_str(), POINT_TO_STR(targetPos).c_str());
	
	Vec2 newPos = aurora::GeometryHelper::calculateNewTracePosition(mPosition,
										targetPos,
										speedValue,
										delta);
	return newPos;
}


void RouteMoveLogic::updateStateData(const Vec2 &pos)
{
	setPosition(pos);
	
	if(getNextDir() != DirNone) {	// Route is changed
		setDir(mNextDir);
		setWalkingRoute(getCrossingRoute());
	}
}

std::string RouteMoveLogic::infoResult()
{
	std::string result = "";
	
	result += StringUtils::format("NextDir=%d", getNextDir());
	
	result += " crossingRoute=";
	if(getCrossingRoute() == nullptr) {
		result += "none";
	} else {
		result += "[" + getCrossingRoute()->toString() + "]";
	}
	
	return result;
}


//
//Vec2 RouteMoveLogic::getNewPosition(const Vec2 oldPos, float delta)	// no consideration about the route
//{
//	
//}
//
//Vec2 RouteMoveLogic::findNextPosition(const Vec2 &pos, float delta)
//{
//	
//	Vec2 curPos = mPosition;
//	
//	
//	Vec2 newPos = getNewPosition(delta);
//	
//	
//	// Current state: Up & Down
//	if(mDir == DirUp || mDir == DirDown)
//	{
//		bool matchStart;
//		const MapLine *mapLine;
//		
//		Vec2 startPos = curPos;		//+ (Vec2(0, 0.0001f) * (mDir == DirUp ? 1 : -1)) ;
//		mapLine = GameWorld::instance()->getMap()->willCrossAnyHoriLine(startPos, newPos, matchStart);
//		
//		// log("DEBUG: UP/DOWN: start=%f,%f new=%f,%f cross=%d", startPos.x, startPos.y, newPos.x, newPos.y, (mapLine != nullptr));
//		
//		// Case 'No reach any cross line'
//		if(mapLine == nullptr) {		// not crossing the horizontal line
//			return newPos;
//		} else {
//			// Case 'Reach a cross line'
//			//			setHoriLine(MapLine::create(mapLine));
//			setHoriLine(MapLine::cloneMapLine(mapLine));
//			Vec2 startPoint = mapLine->getStartPoint();
//			Vec2 endPoint = mapLine->getEndPoint();
//			
//			newPos = matchStart ? startPoint : endPoint;
//			
//			mNextDir = matchStart ? DirRight : DirLeft;
//			
//			//Gavin - Modify to support Slope Line Movement
//			if(mapLine->isSlopeLine())
//			{
//				Vec2 destinationPoint = (matchStart)? endPoint:startPoint;
//				mNextDir = (destinationPoint.x <= curPos.x)? DirLeft : DirRight;
//			}
//			
//		}
//	}
//	else if(mDir == DirLeft || mDir == DirRight)
//	{
//		MapLine *horiLine = getHoriLine();
//		if(horiLine != nullptr) {
//			
//			Vec2 endPoint = mDir == DirRight ?
//			horiLine->getEndPoint()
//			: horiLine->getStartPoint();
//			
//			
//			Vec2 startPos = curPos; // + (Vec2(0.000001f, 0) * (mDir == DirRight ? 1 : -1)) ;
//			bool reachEnd = aurora::GeometryHelper::isLineIntersect(startPos, newPos, endPoint, true, false);
//			
//			
//			
//			
//			//Gavin - Modify to support Slope Line Movement
//			if(horiLine->isSlopeLine())
//			{
//				bool movingUp = horiLine->isMovingUpwardAlongSlopeLine(mDir);
//				endPoint =  horiLine->getSlopeDestinationPoint(mDir);
//				if(!movingUp && (mDir == DirRight) &&  curPos.y <= endPoint.y){reachEnd = true;}
//				else if( movingUp && (mDir == DirRight) && curPos.y >= endPoint.y){reachEnd = true;}
//				else if( movingUp  && (mDir == DirLeft) && curPos.y >= endPoint.y){reachEnd = true;}
//				else if( !movingUp  && (mDir == DirLeft)  && curPos.y <= endPoint.y){reachEnd = true;}
//				if(reachEnd == true)
//				{
//					GameWorld::instance()->getMap()->removeLineFromMap(horiLine->getLineObjectID());
//					setHoriLine(nullptr);
//				}
//				
//			}
//			
//			
//			
//			//log("DEBUG: LEFT/RIGHT: start=%f,%f new=%f,%f cross=%d", startPos.x, startPos.y, newPos.x, newPos.y, (mapLine != nullptr));
//			// Case 'Pass directly'
//			if(reachEnd == false) {
//				return newPos;
//			}
//			
//			
//			
//			// Refine the newPos
//			newPos = endPoint;
//			mNextDir = mDefaultDir;
//			
//			return newPos;
//		} else {
//			// note: something wrong here!!!
//			log("No horiLine found!!");
//		}
//	}
//	
//	return newPos;
//}
