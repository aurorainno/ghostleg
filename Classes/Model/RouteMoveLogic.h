
//
//  RouteMoveLogic.hpp
//  GhostLeg
//
//  Created by Ken Lee on 8/12/2016.
//
//

#ifndef RouteMoveLogic_hpp
#define RouteMoveLogic_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "MapLine.h"

USING_NS_CC;

class GameMap;

class RouteMoveLogic : public Ref
{
public:
	CREATE_FUNC(RouteMoveLogic);

	CC_SYNTHESIZE(Vec2, mSpeed, Speed);		// the speed of the object, we use the absolute value
											// mSpeed.x is the speed when walking the slope line
											// mSpeed.y is the speed walking on the vertical line

	CC_SYNTHESIZE(Dir, mDir, Dir);			// Current Dir
	CC_SYNTHESIZE(Dir, mDefaultDir, DefaultDir);		// default vertical direction
	CC_SYNTHESIZE_RETAIN(MapLine *, mWalkingRoute, WalkingRoute);	// the route walking on
	
	CC_SYNTHESIZE(Vec2, mPosition, Position);	// current position

	
	// Output
	CC_SYNTHESIZE_RETAIN(MapLine *, mCrossingRoute, CrossingRoute);	// the route is stepping at
	

	
	RouteMoveLogic();
	virtual bool init();
	
	void setGameMap(GameMap *map);

	Vec2 calculateNewPosition(float delta);	// Conside the junction point in Map
	Vec2 getNewPosition(float delta);	// without check junction point
	
	Dir getNextDir();
	
	void updateStateData(const Vec2 &pos);
	
	std::string infoResult();
	
private:
	Vec2 handleMoveVertical(const Vec2 &newPos);
	Vec2 handleMoveOnSlope(const Vec2 &newPos);

private:
	GameMap *mGameMap;
	Dir mNextDir;
};

#endif /* RouteMoveLogic_hpp */
