//
//  StateLogicStateLogicFinishGame.h
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#ifndef StateLogicFinishGame_hpp
#define StateLogicFinishGame_hpp

#include <stdio.h>

#include "StateLogic.h"


class StateLogicFinishGame : public StateLogic
{
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();


private:
	float mCooldown;
	bool mIsGameOverDone;
};

#endif /* StateLogicOrderSelect_hpp */
