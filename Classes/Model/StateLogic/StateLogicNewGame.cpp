//
//  StateLogicNewGame.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "StateLogicNewGame.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameScene.h"
#include "StageManager.h"
#include "GameplaySetting.h"
#include "PlanetManager.h"
#include "NPCManager.h"
#include "NpcDistanceLayer.h"
#include "PlayerManager.h"
#include "PlayerCharProfile.h"
#include "TimeMeter.h"

void StateLogicNewGame::enterState()
{
	int selectedPlanet = PlanetManager::instance()->getSelectedPlanet();
	
	
	
	StageManager::instance()->setCurrentStage(selectedPlanet);
	StageManager::instance()->reset();

	// need to call before game reset
	setupGameplaySetting();
	
	// log("DEBUG: EnterState: NewGame");
	mWorld->sendEvent(GameWorldEvent::EventTapHintVisibility, 0);
	mWorld->resetGame();
	mWorld->getGameUILayer()->reset();
    //mWorld->getGameUILayer()->setTimeTextVisibility(true);
	
	float timeLimit = 60;
	mWorld->pauseGameClock();
	mWorld->setTotalTime(timeLimit);
	mWorld->setRemainTime(timeLimit);
	mWorld->setCountDownStarted(false);
	
	// Reset Chaser
	
	// Stage Handling
	
	
	
	// Update the NpcDistance Indicator
	int numOrder = StageManager::instance()->getTotalOrderCount();
	mWorld->getGameUILayer()->getNpcDistanceLayer()->setupDots(numOrder);
	
	//
	// Preload the sprite sheet to increase in game peformance
	std::string spriteName = StringUtils::format("enemy/stage%02d", selectedPlanet);
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile(spriteName + ".plist", spriteName + ".png");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("enemy/enemy.plist", "enemy/enemy.png");
	SpriteFrameCache::getInstance()->addSpriteFramesWithFile("item/item.plist", "item/item.png");
	
	
	
	
	NPCManager::instance()->resetNpcRating();
	
	TimeMeter::instance()->reset();
}

void StateLogicNewGame::setupGameplaySetting()
{
//	GameplaySetting *setting = mWorld->getGameplaySetting();
//	setting->reset();
//	
//	setting->setExtraReward(5, 300);
//	setting->setExtraReward(4, 200);
//	setting->setExtraReward(3, 100);
//	setting->setExtraReward(2, 50);
//	setting->setExtraReward(1, 10);
	
	//
	mWorld->updateGameplaySetting();
	
	// Setting the player
//	Player *player = mWorld->getPlayer();
//	log("MainAttribute: %s", player->infoMainAttribute().c_str());
	
}

void StateLogicNewGame::update(float delta)
{
	// Switch to Running when first line is placed.
	//if(mWorld->isFirstLineDropped()) {
	mWorld->changeState(GameWorld::State::OrderSelect);
}

bool StateLogicNewGame::exitState()
{
	log("DEBUG: exitState: NewGame");
	return true;
}
