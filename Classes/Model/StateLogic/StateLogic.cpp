//
//  StateLogic.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "StateLogic.h"
#include "GameWorld.h"


StateLogic::StateLogic()
: mWorld(nullptr)
, mEnd(true)
{
	
}
void StateLogic::setGameWorld(GameWorld *world)
{
	mWorld = world;
}

bool StateLogic::isEnd()
{
	return mEnd;
}

void StateLogic::sendEvent(GameWorldEvent worldEvent, int arg)
{
	if(mWorld != nullptr) {
		mWorld->sendEvent(worldEvent, arg);
	}	
}

GameSceneLayer *StateLogic::getUILayer()
{
	if(mWorld == nullptr) {
		return nullptr;
	}
	
	return mWorld->getGameUILayer();
}