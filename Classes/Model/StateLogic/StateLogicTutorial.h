//
//  StateRunningLogic.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#ifndef StateTutorialLogic_hpp
#define StateTutorialLogic_hpp

#include <stdio.h>
#include "StateLogic.h"

class TutorialLayer;

class StateLogicTutorial : public StateLogic
{
public:
	
	
	
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();
	
	void reset();
	
private:
	void setupTutorial();
	void setupGameWorld();
	void setupNextNPC();

private:
	TutorialLayer *mTutorialLayer;
	bool mHasInit;
	int mClientIndex;
	bool mTutorialEnd;
};

#endif /* StateRunningLogic_hpp */
