//
//  StateLogicGameOver.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "StateLogicGameOver.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameTouchLayer.h"
#include "GameScene.h"
#include "GameContinueDialog.h"
#include "Constant.h"
#include "GameSound.h"
#include "Player.h"

void StateLogicGameOver::enterState()
{
	
	// Commit money earn
	GameManager::instance()->getUserData()->saveMoney();
	
	// Save the score
	int newDistance = mWorld->getPlayerDistance();
    
    mGameOverAnimationTime = 0;
    mGameOverAnimationFinishFlag = false;
    mAnimationDone = false;
    
	GameManager::instance()->getUserData()->setDistance(newDistance);
	mWorld->setUserTouchEnable(false);
	
    mWorld->resetInitialMapOffset();	// need to reset because maybe modified when played the tutorial in this game
    
//    mWorld->getGameUILayer()->setTimeUpTextVisibility(true);
    // mWorld->getGameUILayer()->setTimeTextVisibility(false);
    
	
    //GameWorld::instance()->getGameTouchLayer()->hideTouchIndicator();
    GameWorld::instance()->getGameTouchLayer()->clearTouchIndicator();
	GameWorld::instance()->getPlayer()->setIsDying(true);
	GameWorld::instance()->getPlayer()->showDyingAnimation();
    GameWorld::instance()->shakeBackground(0.5);
	
	
	GameWorld::instance()->stopClockCountDown();
    
    
	// TODO:
	showEndGameAnimation();
	
	// sendEvent(EventGameOver, 0);	// popup the GameOverDialog dialog
}

// TODO:
void StateLogicGameOver::showEndGameAnimation()
{
	/// LOGIC
	//		0. Chaser monstor show attack animation
	//		1. Hide the player's package
	//		2. Show package animation (drop out)
	//		3. Player show "sweating" animation
	//		4. Package out of screen OR animation time over 5 seconds
	//			-> EndAnimation and call 'onGameOverAnimationEnd'
	
    GameWorld::instance()->getPlayer()->showBreakingPackage([=]{
        mGameOverAnimationFinishFlag = true;
    });
	
}


bool StateLogicGameOver::isAnimationPlayed()
{
	return mGameOverAnimationTime >= 1.5f && mGameOverAnimationFinishFlag;
}

void StateLogicGameOver::update(float delta)
{
	if(mAnimationDone) {
		return;
	}

	// Animation Not DONE
	mGameOverAnimationTime += delta;
	if(isAnimationPlayed() == false) {
		return;
	}

	// ANimation just played
	mAnimationDone = true;
	onGameOverAnimationEnd();
//	
//	
//    if(mGameOverAnimationTime >= 1.5f && mAnimationDone == false)
//    {
//        mGameOverAnimationTime = 0;
//        mAnimationDone = true;
//        // GameWorld::instance()->getPlayer()->stopDyingAnimation();
//		
//		
//		
//		
//		
////        sendEvent(EventGameOver, 0);	// popup the GameOverDialog dialog
//    }
//    
}


void StateLogicGameOver::onGameOverAnimationEnd()
{
	GameWorld::instance()->deActivateChaser();

	
   //  GameSound::clearSound();
    mWorld->getGameUILayer()->setTimeUpTextVisibility(false);
    if(canContinue()) {
		showGameContinue();
	} else {
		handleNotContinue();
	}
	
}

bool StateLogicGameOver::exitState()	// When user press "Button" in GameOverDialog
{
    GameWorld::instance()->getPlayer()->setPackageVisible(true);
	return true;
}


void StateLogicGameOver::showGameContinue()
{
	GameSceneLayer *parent = mWorld->getGameUILayer();
	
	
	GameContinueDialog *dialog = GameContinueDialog::create();
	
	bool hideAd = GameManager::instance()->getReviveCount() >= 5;	//
	if(hideAd) {
		dialog->hideWatchAd();
	}
	
	
	parent->addChild(dialog);
	
	dialog->setCallback([&](Ref *ref, GameContinueDialog::GameContinueOption opt) {
		log("debug: %d", opt);
		
		if(opt == GameContinueDialog::GameContinueOptionExit) {
			handleNotContinue();
		} else if(opt == GameContinueDialog::GameContinueOptionUseStar) {
			handleUseStar();
		} else if(opt == GameContinueDialog::GameContinueOptionWatchAd) {
			handleWatchAd();
		}
		
		Node *dialog = (Node *) ref;
		dialog->removeFromParent();
	});
}


void StateLogicGameOver::handleWatchAd()
{
	
	startRevive();
	
}


void StateLogicGameOver::handleUseStar()
{
	mWorld->updateCoins(false);
	
	startRevive();
}

void StateLogicGameOver::startRevive()
{
	// Increase the revive cost
	GameManager::instance()->increaseRevive();
	
	
	//
	GameWorld::instance()->getPlayer()->stopDyingAnimation();
	GameWorld::instance()->revivePlayer();
    
    //add remain time by 10
    GameWorld::instance()->setRemainTime(12);
	GameWorld::instance()->setCountDownStarted(false);
	
	
	// mReviveCount++;
}

void StateLogicGameOver::handleNotContinue()
{
	mWorld->sendEvent(EventGameOver, 0);
}

bool StateLogicGameOver::canContinue()
{
	return mWorld->getReviveCount() < kMaxReviveCount;
}
