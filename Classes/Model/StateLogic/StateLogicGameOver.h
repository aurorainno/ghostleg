//
//  StateLogicGameOver.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#ifndef StateLogicGameOver_hpp
#define StateLogicGameOver_hpp

#include <stdio.h>

#include "StateLogic.h"

class StateLogicGameOver : public StateLogic
{
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();
    
    CC_SYNTHESIZE(float, mGameOverAnimationTime, GameOverAnimationTime);
    CC_SYNTHESIZE(bool, mGameOverAnimationFinishFlag, GameOverAnimationFinishFlag);
    CC_SYNTHESIZE(bool, mAnimationDone, AnimationDone);

private:
	void showGameContinue();
	void handleWatchAd();
	void handleUseStar();
	void handleNotContinue();
	bool canContinue();
	void onGameOverAnimationEnd();
	
	void showEndGameAnimation();
	
	bool isAnimationPlayed();
	
	void startRevive();
};

#endif /* StateLogicGameOver_hpp */
