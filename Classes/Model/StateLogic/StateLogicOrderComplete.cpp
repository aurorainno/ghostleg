//
//  StateLogicOrderSelect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#include "StateLogicOrderComplete.h"
//
//  StateLogicOrderComplete.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "GameWorld.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "ModelLayer.h"
#include "EffectLayer.h"
#include "Player.h"
#include "NPCMapLogic.h"
#include "GameScene.h"
#include "CommonDialog.h"
#include "GameTouchLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameMap.h"
#include "NPC.h"
#include "NPCRating.h"
#include "NPCManager.h"
#include "StageManager.h"
#include "GameSound.h"

namespace {
	void breakdownReward(int totalReward, int portion, std::vector<int> &rewardList)
	{
		int count = totalReward / portion;
		
		int remain = totalReward;
		
		rewardList.clear();
		
		for(int i=0; i<count; i++){
			rewardList.push_back(portion);
			
			remain -= portion;
		}
		
		if(remain > 0) {
			rewardList.push_back(remain);
		}
	}
}

void StateLogicOrderComplete::enterState()
{
	//
//	mWorld->setUserTouchEnable(false);
//	mWorld->getGameTouchLayer()->resetTouchInfo();		// some bugfix when reach the end!!
// mWorld->getPlayer()->setCsbAnimeRotation(0);

	// Deactivate the Chaser
	mWorld->deActivateChaser();

	
	GameWorld::instance()->stopClockCountDown();
	//
	
	mWorld->getGameUILayer()->setTopPanelVisible(true);
	
	
	// Prepare the Game Data
	mCurrentOrder = mWorld->getMapLogic()->getCurrentNPCOrder();
	mCurrentRating = NPCManager::instance()->addNpcRating(mCurrentOrder, mWorld->getTimeElapse());

	mExtendTime = (int) (0.8 * mCurrentOrder->calculateTimeLimit());
	
	// Modify the NPC emoji
	NPC *npc = mWorld->getModelLayer()->getLastNpc();
	if(npc != nullptr) {
		npc->setEmoji(NPC::EmojiWaiting);
		mNpcPosY = npc->getPositionY();
	} else {
		mNpcPosY = mWorld->getPlayer()->getPositionY() + 10;
	}

	showNpcEmoji();		// need to call after mCurrentRating had set
	
	// Reward the player
	// NPCManager::instance()->rewardByNPCRating(mCurrentRating);

	// Clear the chasing enemy
	mWorld->getModelLayer()->deactiveAllEnemy();
    GameSound::stopAllEnemySound();
	
	
	// Mark the Npc Order Done
	StageManager::instance()->setNpcOrderDone(mCurrentOrder->getOrderID());

	// Start a substate
	startSubStateApproachNpc();
}

void StateLogicOrderComplete::update(float delta)
{
	switch(mSubState) {
		case SubStatePlayerApproachNpc: {
			handleSubStateApproachNpc(delta);
			break;
		}
			
		case SubStatePlayerLeavingNpc: {
			handleSubStateLeavingNpc(delta);
			break;
		}
			
//		case SubStateMovingToNPC: {
//			handleSubStateMovingToNPC(delta);
//			break;
//		}
//			
		case SubStateReceivePackage: {
			handleSubStateReceivePackage(delta);
			break;
		}
			
		case SubStateRewardPlayer: {
			handleSubStateRewardPlayer(delta);
			break;
		}
			
		
		default: {
			break;
		}
	}
}

bool StateLogicOrderComplete::exitState()	// true if the State is transit (without update)
{
	mWorld->setUserTouchEnable(true);
	
	
	Vec2 playerPos = GameWorld::instance()->getPlayer()->getPosition();
	float startX = GameWorld::instance()->getMap()->getClostestLineX(playerPos.x);
	playerPos.x = startX;		// Adjustment
	GameWorld::instance()->getPlayer()->setPosition(playerPos);
	
	return true;
}


void StateLogicOrderComplete::startSubStateMovingToNPC()
{
	mSubState = SubStateMovingToNPC;
	
	// Simple way to move player
	MoveTo *moveTo = MoveTo::create(1.5, mPlayerStopPos);
	mWorld->getPlayer()->runAction(moveTo);
	
	mWorld->getPlayer()->resetToUpwardDir();
	
	// TODO: Change Face !
    bool isFaceLeft = mPlayerStopPos.x - mWorld->getPlayer()->getPositionX() < 0 ? true : false;
    mWorld->getPlayer()->setFace(isFaceLeft);
	
	// Show Top Notice
	mWorld->getGameUILayer()->setDeliveryTips(mCurrentRating->getTip());
	mWorld->getGameUILayer()->setDeliveryReward(mCurrentOrder);
	mWorld->getGameUILayer()->showDeliveryNotice();
	
	// mWorld->getGameUILayer()->hideNpcAndTimeUI();
	
}

void StateLogicOrderComplete::handleSubStateMovingToNPC(float delta)
{
	if(mWorld->getPlayer()->getPositionY() < mPlayerStopPos.y) {	// when player is still moving to NPC
		mWorld->moveCameraToPlayer();
		return;
	}

	
	// When player reach the NPC Position

    
	// Need Substate Change
	startSubStateGivingPackage();
	
}

void StateLogicOrderComplete::startSubStateGivingPackage()
{
	mSubState = SubStateGivingPackage;
	
	

	
	// Anime the npc
	NPC *npc = mWorld->getModelLayer()->getLastNpc();
	
	if(npc != nullptr) {
		npc->closeEmoji();	// Change from "..."  to nothing
	}
	

	// Animate the player		// Make the package disappear
	Player *player = mWorld->getPlayer();
	//
	player->setPackageVisible(true);
	player->showGivingPackage([&](){
		startSubStateReceivePackage();		//
	});
	
//    
//    bool isFaceLeft = npc->getPositionX() - player->getPositionX() < 0 ? true : false;
//    mWorld->getPlayer()->setFace(isFaceLeft);
//	

}

void StateLogicOrderComplete::showRewardMoney()
{
	int money = mCurrentRating->getTotalReward();
	
	NPC *npc = mWorld->getModelLayer()->getLastNpc();
	Vec2 startPos = (npc == nullptr) ? Vec2::ZERO : npc->getPosition();
	startPos += Vec2(0, 40);
	
	std::vector<int> rewardList;
	breakdownReward(money, 50, rewardList);		// 100 : 1 coinIcon present 100 coin
	
	
	mWorld->addMultipleCoinWithAnimation(rewardList, VisibleRect::center(), 0.5f);
	
	
	
	
	//mWorld->getEffectLayer()->showCollectManyStarEffect(rewardList, startPos);
}

void StateLogicOrderComplete::startSubStateReceivePackage()
{
	mSubState = SubStateReceivePackage;

	mIsRewardShown = false;
	
    GameSound::playSound(GameSound::PassCheckPoint);
	
	// Score the Reward Score and
	// TODO:
	mWorld->getGameUILayer()->showCheckPointDialog(mCurrentRating, [&](void){
		//handleReward();
	});
	
	// NPC Animation
	mPlayerSpeed = 0;
	
	// NPC show package drop to the ground & npc showing the emoji
	Vec2 pos = mWorld->getMapLogic()->getDropPosition();
	mWorld->getModelLayer()->addPackageBox(pos, [&](void) {
	});
	
	// Handle the Top Notice
//	mWorld->getGameUILayer()->setDeliveryOnShowRewardCallback([&](){
//		mIsRewardShown = true;
//		mCloseNoticeCooldown = 1.5;
//	});
	
//	mWorld->getGameUILayer()->setDeliveryOnCloseCallback([=](){
//		log("***** DEBUG: start reward player");
//		startSubStateRewardPlayer();
//	});
	
	// mWorld->getGameUILayer()->showDeliveryReward();
	
	
	startSubStateRewardPlayer();
}

void StateLogicOrderComplete::handleSubStateReceivePackage(float delta)
{
//	if(mIsRewardShown == false) {
//		return;
//	}
//	
//	mCloseNoticeCooldown -= delta;
//	if(mCloseNoticeCooldown > 0) {
//		return;
//	}
//	
//	// Start closing and move to next substate
//	mWorld->getGameUILayer()->closeDeliveryNotice();
//	startSubStateRewardPlayer();
}


void StateLogicOrderComplete::showNpcEmoji()
{
	NPC *npc = mWorld->getModelLayer()->getLastNpc();
	
	if(npc == nullptr) {
		// BUG
		mDeliverActionDone = true;	// quick exit
		return;
	}
	
	NPC::Emoji emoji = (NPC::Emoji) mCurrentRating->getRating();
	npc->setEmoji(emoji, [&]() {
		//startS
//		startSubStateRewardPlayer();
	});
	

	
}

void StateLogicOrderComplete::movePlayer(float delta)
{
	if(mPlayerSpeed == 0) {
		return;
	}
	
	
	Player *player = mWorld->getPlayer();
	Vec2 pos = player->getPosition();
	
	pos += Vec2(0, mPlayerSpeed * delta);
	
	player->setPosition(pos);
	
	mWorld->moveCameraToPlayer();
}

void StateLogicOrderComplete::showThankYouDialog()
{
	Player *player = mWorld->getPlayer();
	
	player->showDialog(Player::DialogThankYou, 0, 1);
	
	mIsThankyouShown = true;
}


void StateLogicOrderComplete::showEnjoyDialog()
{
	Player *player = mWorld->getPlayer();
	
	player->showDialog(Player::DialogEnjoy, 0, 1);
	
	mIsEnjoyShown = true;
}





void StateLogicOrderComplete::startSubStateRewardPlayer()
{
	mSubState = SubStateRewardPlayer;
	
	
	mPlayerSpeed = 0;
	mTimePassed = 0;
	mIsThankyouShown = false;
	mIsEnjoyShown = false;
	
	
	// Extend Time handling
	if(! mCurrentOrder->getIsLastOrder()) {
		//mWorld->getGameUILayer()->showTimeExtended(mExtendTime);
		
		
		GameSound::playSound(GameSound::VoiceLaugh);
	}

	handleReward();

	
	
	//showRewardMoney();
	//rewardScore();
}

void StateLogicOrderComplete::handleSubStateRewardPlayer(float delta)
{
	// Player Movement
	mWorld->mModelLayer->update(delta);			// update the player
	mWorld->getGameUILayer()->update(delta);
	
	// Game Over
//	if(mWorld->handleCollisionLogic() == HitByEnemy) {
//		//mWorld->handleGameOver();
//		mWorld->getPlayer()->changePlayerState(Player::StateStun);
//	}
	
	// Handle Camera
	mWorld->moveCameraToPlayer();
	
	
	// Others
	//mWorld->
	
	// Time data
	mTimePassed += delta;
	
	//
	if(mTimePassed > 0.5 && mIsThankyouShown == false) {
		showThankYouDialog();
		mPlayerSpeed = 160;
	}
	
	if(mTimePassed > 1.0 && mIsEnjoyShown == false) {
		showEnjoyDialog();
	}
	
	if(mTimePassed >= 2.0) {
		handleEnd();
	}

}

void StateLogicOrderComplete::handleEnd()
{
	if(mCurrentOrder->getIsLastOrder()) {
		// End Game
		mWorld->changeState(GameWorld::State::FinishGame);
	} else {
		GameWorld::State nextState;
		
		nextState = GameWorld::instance()->isShowTutorial() ?
				GameWorld::State::Tutorial : GameWorld::State::OrderSelect;
		
		mWorld->changeState(nextState);
	}
}

// Approach NPC
void StateLogicOrderComplete::startSubStateApproachNpc()
{
	mSubState = SubStatePlayerApproachNpc;
	
	
}

void StateLogicOrderComplete::handleSubStateApproachNpc(float delta)
{
	// Update model
	mWorld->mModelLayer->update(delta);			// update the player
	mWorld->getGameUILayer()->update(delta);
	
	// Game Over
//	if(mWorld->handleCollisionLogic()) {
//		mWorld->getPlayer()->changePlayerState(Player::StateStun);
//	}
	
	// Handle Camera
	mWorld->moveCameraToPlayer();
	
	if(mWorld->getPlayer()->getPositionY() >= mNpcPosY) {
		startSubStateReceivePackage();
	}
}

// Leaving NPC
void StateLogicOrderComplete::startSubStateLeavinghNpc()
{
	mSubState = SubStatePlayerLeavingNpc;
}

void StateLogicOrderComplete::handleSubStateLeavingNpc(float delta)
{
	
}

void StateLogicOrderComplete::rewardScore()
{
	
	GameWorld::instance()->addScoreWithAnimation(
				mCurrentRating->getScore(), ScoreTypeDelivery);
}


void StateLogicOrderComplete::handleReward()
{
	rewardScore();
	showRewardMoney();
	handleClearStageScore();
	
	// Commit money earn
	GameManager::instance()->getUserData()->saveMoney();
}

void StateLogicOrderComplete::handleClearStageScore()
{
	if(mCurrentOrder->getIsLastOrder() == false) {
		return;			// No need
	}
	
	StageData *data = StageManager::instance()->getCurrentStageData();
	if(data == nullptr) {
		return;
	}
	
	int rewardScore = data->getClearStageScore();
	
	GameWorld::instance()->addScoreWithAnimation(rewardScore, ScoreTypeAllClear, 1.0f);
}



