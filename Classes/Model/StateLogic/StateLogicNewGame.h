//
//  StateLogicNewGame.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#ifndef StateLogicNewGame_hpp
#define StateLogicNewGame_hpp

#include <stdio.h>

#include "StateLogic.h"

class StateLogicNewGame : public StateLogic
{
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();

private:
	void setupGameplaySetting();
};

#endif /* StateLogicNewGame_hpp */
