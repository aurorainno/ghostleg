//
//  StateRunningLogic.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "StateLogicRunning.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "LevelData.h"
#include "Player.h"
#include "GameSound.h"
#include "GameScene.h"
#include "NPCMapLogic.h"
#include "StageManager.h"
#include "GameplaySetting.h"
#include "GameTouchLayer.h"
#include "TimeMeter.h"



const int kEnergyToDrawALine = 15;
const float kEnergyRegenerationTime = 8.0f;	// 8 second to generate


void StateLogicRunning::enterState()
{
	mWorld->setUserTouchEnable(true);
	
	if(StageManager::instance()->getOrderDoneCount() == 0)	{	// voice only in the first npc
		GameSound::playSound(GameSound::VoiceReady);
	}
	
	
	mWorld->getGameTouchLayer()->reset();
	mWorld->setIsBraking(false);
    mWorld->getGameUILayer()->showNpcAndTimeUI();
        
    mWorld->getGameUILayer()->showDialogue(GameSceneLayer::DialogueType::Waiting, 1, 1);
	mWorld->getPlayer()->showDialog(Player::DialogOnMyWay, 1.5, 1);
	
	mCountDown = 0.0f;
	mCanStart = true;
	
	mWorld->resumeGameClock();
	
	
	// Apply Starting Effect
	
}


void StateLogicRunning::update(float delta)
{
	
//	if(mWorld->needNewMapData()) {
//		mWorld->generateNewMapData();
//	}
//	
	if(mCanStart == false) {
		mCountDown -= delta;
		if(mCountDown > 0) {
			return;
		}
		
		//GameSound::playSound(GameSound::VoiceGo);
		
		mCanStart = true;
	}
	
	
	TSTART("stateRunning.total");
	
	
	// Update with Delta Time
	TSTART("modelUpdate");
	mWorld->mModelLayer->update(delta);
	TSTOP("modelUpdate");
	
	TSTART("uiUpdate");
	mWorld->getGameUILayer()->update(delta);
	TSTOP("uiUpdate");
	
	TSTART("ClocknDistance");
	mWorld->updateGameClock(delta);
	mWorld->getPlayer()->updateTravelDistance();
	TSTOP("ClocknDistance");
	
	
	TSTART("ApplyEffect");
	if(mWorld->getPlayer()->getTravelDistance() > 100) {
		mWorld->applyStartingItemEffect();
	}
	TSTOP("ApplyEffect");
	
    //remain distance
	TSTART("UpdateDistance");
	int remainDistance = mWorld->getMapLogic()->getRemainDistance();
	mWorld->getGameUILayer()->setRemainDistance(remainDistance / 100);
	TSTOP("UpdateDistance");

	// ken: no more energy stuff
	// Handle Energy
//	bool anyUpdate = mWorld->getPlayer()->regenerateEnergy(delta);
//	
//	if(anyUpdate) {
//		int energy = (int) floorf(mWorld->getPlayer()->getPlayerEnergy());
//		mWorld->sendEvent(EventUpdateBarEnergy, energy);
//	}
	
	// Handle Camera
	mWorld->moveCameraToPlayer();
	
	// Game Over
	TSTART("Collision");
	CollisionType colType = mWorld->handleCollisionLogic();
	if(HitByEnemy == colType) {
		mWorld->getPlayer()->changePlayerState(Player::StateStun);
	}else if(HitByChaser == colType) {
		mWorld->handleGameOver();
	}
	TSTOP("Collision");
    
//    if(mWorld->checkTimeout()) {
//        mWorld->handleGameOver();
//    }
//	

	
	// Update the Game Map
	TSTART("updateWorldData");
	mWorld->getMapLogic()->updateWorldData(delta);
	TSTOP("updateWorldData");
	
	TSTOP("stateRunning.total");
	
	if(mWorld->getMapLogic()->isAtNpcMap()) {
		mWorld->changeState(GameWorld::State::OrderComplete);
	}

}

bool StateLogicRunning::exitState()
{
	mWorld->pauseGameClock();
	return true;
}


