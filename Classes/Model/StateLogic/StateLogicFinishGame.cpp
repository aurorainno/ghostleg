
#include "StateLogicFinishGame.h"
//
//  StateLogicFinishGame.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "GameWorld.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "ModelLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameTouchLayer.h"
#include "GameScene.h"
#include "Constant.h"
#include "GameSound.h"


void StateLogicFinishGame::enterState()
{
	// Save the score
	int newDistance = mWorld->getPlayerDistance();
	
	GameManager::instance()->getUserData()->setDistance(newDistance);
	mWorld->setUserTouchEnable(false);
	
	mWorld->resetInitialMapOffset();	// need to reset because maybe modified when played the tutorial in this game
	
	//setFinishGameTitleVisible
	mWorld->getGameUILayer()->setFinishGameTitleVisible(true);
	//mWorld->getGameUILayer()->setTimeTextVisibility(false);
	
	GameSound::playSound(GameSound::VoiceSeeYa);
	
	
	GameWorld::instance()->getGameTouchLayer()->clearTouchIndicator();
	GameWorld::instance()->stopClockCountDown();
	
	mCooldown = 1.0f;
	mIsGameOverDone = false;
}

void StateLogicFinishGame::update(float delta)
{

	mCooldown -= delta;
	
	// Update model
	mWorld->mModelLayer->update(delta);			// update the player

	
	if(mCooldown < 0 && mIsGameOverDone == false) {
		mWorld->sendEvent(EventGameOver, 1);
		mIsGameOverDone = true;
	}
}

bool StateLogicFinishGame::exitState()	// true if the State is transit (without update)
{
	return false;
}
