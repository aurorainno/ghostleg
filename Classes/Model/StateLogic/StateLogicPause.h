//
//  StateLogicPause.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#ifndef StateLogicPause_hpp
#define StateLogicPause_hpp

#include <stdio.h>


#include "StateLogic.h"

class PauseDialog;

class StateLogicPause : public StateLogic
{
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();
	void showCountDown();
	
private:
	void showPauseDialog();
	void onCountDownEnd();
	
private:
    std::vector<Sprite*> mCountdownSpriteList;
	PauseDialog *mPauseDialog;
	int mCountDown;
};

#endif /* StateLogicPause_hpp */
