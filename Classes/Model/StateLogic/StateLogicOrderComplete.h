//
//  StateLogicStateLogicOrderComplete.h
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#ifndef StateLogicOrderComplete_hpp
#define StateLogicOrderComplete_hpp

#include <stdio.h>

#include "StateLogic.h"

class NPCRating;
class NPCOrder;

class StateLogicOrderComplete : public StateLogic
{
private:
	enum SubState {
		SubStateMovingToNPC,
		SubStateGivingPackage,
		SubStateReceivePackage,
		SubStateRewardPlayer,
		
		SubStatePlayerApproachNpc,
		SubStatePlayerLeavingNpc,
	};
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();

private:
	// SubStateMovingToNPC
	void startSubStateMovingToNPC();
	void handleSubStateMovingToNPC(float delta);
	void showNpcEmoji();
	
	// SubStateGivingPackage
	void startSubStateGivingPackage();
	
	// SubStateReceivePackage
	void startSubStateReceivePackage();
	void handleSubStateReceivePackage(float delta);
	
	// SubStateRewardPlayer
	void startSubStateRewardPlayer();
	void handleSubStateRewardPlayer(float delta);
	
	
	// Approach NPC
	void startSubStateApproachNpc();
	void handleSubStateApproachNpc(float delta);
	
	// Leaving NPC
	void startSubStateLeavinghNpc();
	void handleSubStateLeavingNpc(float delta);
	
	
	// Supporting methods
	void showRewardMoney();
	void handleReward();
	void handleClearStageScore();
	void rewardScore();
	void movePlayer(float delta);
	void showThankYouDialog();
	void showEnjoyDialog();
	
	void handleEnd();
	
	void backToMainSceneFromTutorial();
	
private:
	SubState mSubState;
	Vec2 mPlayerStopPos;
	float mNpcPosY;
	bool mDeliverActionDone;
	float mCooldown;
	float mTimePassed;
	bool mIsDialogShown;
	bool mIsThankyouShown;
	bool mIsEnjoyShown;
	float mPlayerSpeed;
	int mExtendTime;
	
	bool mIsRewardShown;
	float mCloseNoticeCooldown;
	
	NPCOrder *mCurrentOrder;
	NPCRating *mCurrentRating;
};

#endif /* StateLogicOrderSelect_hpp */
