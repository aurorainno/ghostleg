//
//  StateLogicPause.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "StateLogicPause.h"
#include "GameWorld.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "PauseDialog.h"
#include "ModelLayer.h"
#include "EffectLayer.h"
#include "TimeMeter.h"

namespace  {
	std::string getCountDownText(int value)
	{
		if(value <= 0) {
			return "Go";
		}
		
		char temp[30];
		sprintf(temp, "%d", value);
		
		return temp;
	}
}


void StateLogicPause::enterState()
{
	GameSceneLayer *uiLayer = getUILayer();
	if(uiLayer == nullptr) {
		// TODO report error;
		return;
	}
	
	uiLayer->pauseClockCountDownEffect();
	
	mWorld->setUserTouchEnable(false);
	uiLayer->showPauseDialog();
	uiLayer->hideGameUI();
	
	mWorld->getModelLayer()->pauseAllModel();
	
	// Show the TimeMeter Summary
	TimeMeter::instance()->showSummary();
}

void StateLogicPause::update(float delta)
{

}

bool StateLogicPause::exitState()
{
	GameSceneLayer *uiLayer = getUILayer();
	if(uiLayer != nullptr) {
		uiLayer->closePauseDialog();
	}
	
	//31d8d4
	
	// initialize count down
    for(int i=1;i<=4;i++){
        int index = i<4 ? 4-i : i;
        Sprite* countDownSprite = Sprite::create(StringUtils::format("guiImage/ingame_pause%d.png",index));
        countDownSprite->setScale(0.5);
        countDownSprite->setPosition(VisibleRect::center());
        countDownSprite->setVisible(false);
        mWorld->addChild(countDownSprite);
        mCountdownSpriteList.push_back(countDownSprite);
    }
    
	mCountDown = 3;
	
	showCountDown();
	
	mEnd = false;
	
	return false;
}




void StateLogicPause::showCountDown()
{
    Sprite* countDownSprite = mCountdownSpriteList.at(0);
	countDownSprite->setVisible(true);
	
	// fade in
	auto fadeIn = FadeIn::create(0.1f);
	auto delay = DelayTime::create(0.5f);
	auto fadeOut = FadeOut::create(0.3f);
	
	auto endOfRound = CallFunc::create([=](){
		mCountDown--;
        countDownSprite->setVisible(false);
        mCountdownSpriteList.erase(mCountdownSpriteList.begin());
		if(mCountDown >= 0) {
			showCountDown();
		} else {
			onCountDownEnd();
		}
	});
	
	//
    RemoveSelf* removeSelf = RemoveSelf::create();
	auto seq = Sequence::create(fadeIn, delay, fadeOut, endOfRound, removeSelf, nullptr);
    countDownSprite->runAction(seq);
}


void StateLogicPause::onCountDownEnd()
{
	
	GameSceneLayer *uiLayer = getUILayer();
	if(uiLayer != nullptr) {
		uiLayer->showGameUI();
	}
	
	mWorld->getModelLayer()->resumeAllModel();
    mWorld->getEffectLayer()->resumeAllCaution();
	
	mEnd = true;
	
	mWorld->setUserTouchEnable(true);
}
