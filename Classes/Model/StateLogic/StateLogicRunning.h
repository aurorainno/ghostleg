//
//  StateRunningLogic.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#ifndef StateRunningLogic_hpp
#define StateRunningLogic_hpp

#include <stdio.h>
#include "StateLogic.h"

class StateLogicRunning : public StateLogic
{
public:
	enum GameMode {
		ModeDieWhenHit,
		ModeStunWhenHit,
	};
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();

	void onEnterCountDownTime();
	
	
private:
	bool mCanStart;
	float mCountDown;
	bool mStartCountDown;
	
};

#endif /* StateRunningLogic_hpp */
