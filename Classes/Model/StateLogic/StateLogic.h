//
//  StateLogic.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#ifndef StateLogic_hpp
#define StateLogic_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"


USING_NS_CC;

class GameWorld;
class GameSceneLayer;

class StateLogic : public Ref
{
public:
	StateLogic();
	void setGameWorld(GameWorld *world);
	virtual void enterState() = 0;
	virtual void update(float delta) = 0;
	virtual bool exitState() = 0;		// return true if exit immediately
	bool isEnd();				// is the state ended;

	
protected:
	void sendEvent(GameWorldEvent worldEvent, int arg);
	GameSceneLayer *getUILayer();
	
protected:
	GameWorld *mWorld;
	bool mEnd;
};

#endif /* StateLogic_hpp */
