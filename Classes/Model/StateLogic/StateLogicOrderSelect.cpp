//
//  StateLogicOrderSelect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#include "StateLogicOrderSelect.h"
//
//  StateLogicOrderSelect
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "GameWorld.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "ModelLayer.h"
#include "CommonDialog.h"
#include "NPCMapLogic.h"
#include "NPCOrder.h"
#include "StageManager.h"
#include "NpcDistanceLayer.h"

void StateLogicOrderSelect::enterState()
{
	// Show up an dialog
	//showOrderSelectDialog();
    // mWorld->getGameUILayer()->hideNpcAndTimeUI();
	
	
	selectNextNpc();
	
//	CommonDialog *dialog = CommonDialog::create();
//	
//	mWorld->getGameUILayer()->addChild(dialog);
//	
//	dialog->setTitle("NPC Selection");
//	dialog->setContent("Selecting NPC 1");
//	dialog->setActionCallback([&](Ref *sender, CommonDialog::Action action){
//		onNpcSelected();
//	});
//	
	
}

void StateLogicOrderSelect::selectNextNpc()
{
	// Vector<NPCOrder *> orderList = StageManager::instance()->getNextNPCOrderList();
	NPCOrder *order = StageManager::instance()->getNextNPCOrder();
	
	if(order == nullptr) {
		return;
	}

	
	mWorld->getMapLogic()->setupWithNPCOrder(order);
	mWorld->getModelLayer()->update(0);
	
	mWorld->getPlayer()->setPackageVisible(true);
	
	// Update the GUI
	mWorld->getGameUILayer()->setNpcInfo(order);
	mWorld->getGameUILayer()->setTopPanelVisible(true);
	
	// Show the Distance
	mWorld->getGameUILayer()->showNpcDistance(order);

	// Reset the Clock
	mWorld->resetTimeElapse();
}

void StateLogicOrderSelect::update(float delta)
{
	mWorld->changeState(GameWorld::State::Running);
}

bool StateLogicOrderSelect::exitState()
{
	// Activate the Chaser before Exit
	mWorld->activateChaser();
	
	//
	return true;
}


void StateLogicOrderSelect::onNpcSelected()
{
	mWorld->getMapLogic()->setupWithNPC(1);
	mWorld->getModelLayer()->update(0);
	
	mWorld->getPlayer()->setPackageVisible(true);
	mWorld->changeState(GameWorld::State::Running);
}

void StateLogicOrderSelect::showOrderSelectDialog()
{
	// Prepare the OrderList
	Vector<NPCOrder *> orderList = StageManager::instance()->getNextNPCOrderList();
	
	// DEBUG:
	for(NPCOrder *order : orderList) {
		log("### DEBUG: %s", order->toString().c_str());
	}
	
	//
	NPCOrderSelectDialog *dialog = NPCOrderSelectDialog::create();
	dialog->setItemView(orderList);
	
	dialog->setListener(this);
	mWorld->getGameUILayer()->addChild(dialog);

	mSelectDialog = dialog;
}

void StateLogicOrderSelect::onNPCOrderSelected(NPCOrder* order)
{
	CC_SAFE_RETAIN(order);
	
	log("DEBUG: selectedOrder=%s", order->toString().c_str());
	
	// int selectedNPCID = order->getOrderID();
	
	closeOrderSelectDialog();
	
	
	
	
	//
	float timeLimit = order->getTimeLimit();
    mWorld->setTotalTime(timeLimit);
	mWorld->setRemainTime(timeLimit);
	mWorld->setCountDownStarted(false);
	
	mWorld->getMapLogic()->setupWithNPCOrder(order);
	mWorld->getModelLayer()->update(0);
	
	mWorld->getPlayer()->setPackageVisible(true);
	
	// Update the GUI
	mWorld->getGameUILayer()->setNpcInfo(order);
	mWorld->getGameUILayer()->setTopPanelVisible(true);
	
	
	// Show dialog
	
	
	//
	
	mWorld->changeState(GameWorld::State::Running);
	
	
	CC_SAFE_RELEASE_NULL(order);
}

void StateLogicOrderSelect::closeOrderSelectDialog()
{
	mSelectDialog->setListener(nullptr);
	mSelectDialog->removeFromParent();
	mSelectDialog = nullptr;
}

void StateLogicOrderSelect::onRefreshNPC()
{
	// TODO
}

void StateLogicOrderSelect::onDialogClosed()
{
	// TODO:
	
	

}
