//
//  StateRunningLogic.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/5/2016.
//
//

#include "StateLogicTutorial.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "TutorialLayer.h"
#include "TutorialData.h"
#include "TutorialManager.h"
#include "GameScene.h"
#include "MainScene.h"
#include "GameManager.h"
#include "GameTouchLayer.h"
#include "StageManager.h"
#include "NPCMapLogic.h"
#include "NPCOrder.h"
#include "NPCManager.h"
#include "GameplaySetting.h"
#include "Player.h"
#include "GameMap.h"
#include "PlayerManager.h"

void StateLogicTutorial::enterState()
{
	if(mHasInit == false) {
		setupTutorial();
		setupGameWorld();
		mHasInit = true;
	}
	
	// Add the Tutorial Layer
	if(mTutorialLayer == nullptr) {
		TutorialLayer *tutorialLayer = TutorialLayer::create();
		mWorld->addChild(tutorialLayer);
		mTutorialLayer = tutorialLayer;
		mTutorialLayer->reset();
	}

	
	setupNextNPC();
	
	log("StateLogicTutorla: client=%d", mClientIndex);
	
	mTutorialEnd = false;
	mTutorialLayer->setVisible(true);
	
	if(mClientIndex >= 1) {
		mWorld->activateChaser();
	} else {
		mWorld->deActivateChaser();
	}
}

void StateLogicTutorial::reset()
{
	mHasInit = false;
	mClientIndex = 0;
}

void StateLogicTutorial::setupTutorial()
{

	
	TutorialManager::instance()->loadTutorialData();
	
	mWorld->setShowTutorial(true);		// mark this is the tutorial mode
	mWorld->setUserTouchEnable(false);
	mWorld->getGameUILayer()->reset();
	
	
	mWorld->getGameUILayer()->setTutorialUI(true);
	
}

void StateLogicTutorial::setupGameWorld()
{
	StageManager::instance()->reset();
	
	mWorld->updateGameplaySetting(true);
	mWorld->setShowTutorial(true);		// mark this is the tutorial mode
	mWorld->resetGame();
	mWorld->setUserTouchEnable(false);
	mWorld->getGameUILayer()->reset();
	
	
	
	NPCManager::instance()->resetNpcRating();

	// Modify the Player Position
	float startX = GameMap::getXAtLine(2);
	float startY = 300;
	Vec2 initialPos = Vec2(startX, startY);
	
	Player *player = mWorld->getPlayer();
	player->setInitialPosition(initialPos);
	
}


void StateLogicTutorial::setupNextNPC()
{
	// Vector<NPCOrder *> orderList = StageManager::instance()->getNextNPCOrderList();
	NPCOrder *order = StageManager::instance()->getNextNPCOrder();
	
	if(order == nullptr) {
		log("StateLogicTutorial: order is nullptr");
		return;
	}

	mWorld->getMapLogic()->setupWithNPCOrder(order);		// Generating the map
	mWorld->getModelLayer()->update(0);
	mWorld->getPlayer()->setPackageVisible(true);
	
	// Update the GUI
	mWorld->getGameUILayer()->setNpcInfo(order);
	mWorld->getGameUILayer()->setTopPanelVisible(true);
	
	// Reset the Clock
	mWorld->resetTimeElapse();
}


void StateLogicTutorial::update(float delta)
{
	if(mTutorialEnd) {
		mWorld->mModelLayer->update(delta);
		mWorld->moveCameraToPlayer();
		mWorld->handleCollisionLogic();
		
		// Update the Game Map
		mWorld->getMapLogic()->updateWorldData(delta);
		if(mWorld->getMapLogic()->isAtNpcMap()) {
			mClientIndex++;
			mWorld->changeState(GameWorld::State::OrderComplete);
		}

		
		return;
	}
	

	if(mTutorialLayer == nullptr) {
		log("StaticLogicTutorial: tutorial Layer undefine!!!");
		return;
	}
	
	mTutorialLayer->update(delta);
	//log("DEBUG: playerSpeed=%f ", Game)
	if(mTutorialLayer->isWaitForPlayer() == false) {
		mWorld->mModelLayer->update(delta);
		mWorld->moveCameraToPlayer();
		mWorld->handleCollisionLogic();
	}

	// Update the Game Map
	mWorld->getMapLogic()->updateWorldData(delta);
	if(mWorld->getMapLogic()->isAtNpcMap()) {
		mClientIndex++;
		mWorld->changeState(GameWorld::State::OrderComplete);
		return;
	}
	
	
	
	if(mTutorialEnd == false && mTutorialLayer->isDone()) {
        mWorld->getGameUILayer()->setTutorialUI(false);
		mWorld->setUserTouchEnable(true);
		mWorld->getGameTouchLayer()->reset();
		if(mTutorialLayer) {
			mTutorialLayer->removeFromParent();
			mTutorialLayer = nullptr;
		}
		mTutorialEnd = true;
//		mWorld->changeState(GameWorld::State::Running);
	}
	
}

bool StateLogicTutorial::exitState()
{
	// Remove the Tutorial Layer
//	if(mTutorialLayer) {
//		mTutorialLayer->removeFromParent();
//	}
	
	
	NPCOrder *order = mWorld->getMapLogic()->getCurrentNPCOrder();
	
	if(order->getIsLastOrder()) {
		// Do tutorial Clean up
		if(mTutorialLayer) {
			mTutorialLayer->removeFromParent();
			mTutorialLayer = nullptr;
		}
		
		
		// Puzzle Reward (ken: because Tutorial wont go to EndGameScene)
		std::vector<int> puzzles = StageManager::instance()->rollRewardPuzzle(1);
		PlayerManager::instance()->addPlayerFragments(puzzles);


		// Reset the tutorial flag in GameWorld
		mWorld->setShowTutorial(false);
		GameManager::instance()->turnOffTutorial();		// Mark the tutorial has read
	}
	
	
	return true;
}

