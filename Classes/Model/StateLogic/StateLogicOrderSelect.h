//
//  StateLogicOrderSelect.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#ifndef StateLogicOrderSelect_hpp
#define StateLogicOrderSelect_hpp

#include <stdio.h>

#include "StateLogic.h"
#include "NPCOrderSelectDialog.h"

class NPCOrder;

class StateLogicOrderSelect : public StateLogic
							, public NPCOrderSelectDialogListener
{
public:
	virtual void enterState();
	virtual void update(float delta);
	virtual bool exitState();
	
	
private:
	void onNpcSelected();
	void showOrderSelectDialog();
	void closeOrderSelectDialog();
	void selectNextNpc();
	
#pragma mark - NPCOrderSelectDialogListener
private:
	void onNPCOrderSelected(NPCOrder* order) override;
	void onRefreshNPC() override;
	void onDialogClosed() override;
	
private:
	NPCOrderSelectDialog *mSelectDialog;
};

#endif /* StateLogicOrderSelect_hpp */
