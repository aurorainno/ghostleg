//
//  Player.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2016.
//
//

#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "MovableModel.h"
#include "ItemEffect.h"

USING_NS_CC;

class PackageBox;
class PositionRecorder;
class AnimeNode;

class Player : public MovableModel
{
public:
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(Player);
    

	
#pragma mark - Public Method and Properties
	Player();
	~Player();
	
	virtual bool init() override;	// virtual because different derived class have different init logic
	virtual void update(float delta) override;
	
	void reset() override;
	
	CC_SYNTHESIZE(int, mCollectCoin, CollectCoin);		// total collected coin
	CC_SYNTHESIZE(bool, mIsDying, IsDying);
	CC_SYNTHESIZE(bool, mIsDashing, IsDashing);
	
	
	bool isInvulnerable();
	bool hasShield();
	void handleShieldHitted();
	bool handleBeingHitted();
	
	void addCoin(int newCoin);
    
    void resetPlayerEnergy();
    void deductPlayerEnergy(float amount);
    void addPlayerEnergy(float amount);
	bool regenerateEnergy(float delta);
	void consumeEnergyByLine();

    virtual void setEffectStatus(EffectStatus status, float duration = -1) override;
    virtual void removeEffectStatus() override;
	virtual void setup(const std::string &csbName, Action defaultAction=Idle);
	
	bool hitBy(MovableModel *model);		// Return true if player is die after hit
    
    void addStunAnimation();
    void removeStunAnimation();
	
	
	virtual std::string getName();
	
    CC_SYNTHESIZE(float, mPlayerEnergy, PlayerEnergy);
	
	
	// Player Attribute
	CC_SYNTHESIZE(float, mTotalEnergy, TotalEnergy);			// energy max
	CC_SYNTHESIZE(float, mConsumeRate, ConsumeRate);			// energy per line
	CC_SYNTHESIZE(float, mRegenerateRate, RegenerateRate);	// energy per sec
	CC_SYNTHESIZE(float, mDoubleStarRate, DoubleStarRate);	// Rate to call double star
	
    
    CC_SYNTHESIZE(int, mNumberOfCapsuleUsed, NumberOfCapsuleUsed);

    CC_SYNTHESIZE(Vec2, mDeathPosition, DeathPosition);

    
	// CC_SYNTHESIZE(EffectStatus, mStatus, Status);
	
	std::string infoAttribute();
	std::string infoMainAttribute();
	
	ItemEffect *getItemEffect() const;
	void attachItemEffect(ItemEffect *effect);
	void detachItemEffect(ItemEffect *effect);
	void resetItemEffect();
	void setupDefaultItemEffect();
	
	void increaseSpeed(int incValue, int maxValue=0);
	int getSpeed();
	void setSpeed(int value);
	

	bool isReviving();
	void setReviving(float reviveDuration=-1, bool reActivateChaser=false);
	
    void showDyingAnimation();
    void stopDyingAnimation();
    
    void activateSpeedUp(int speed);
	void cancelSpeedUp();
    
    void autoSpeedUpByTime(float delta);
	
#pragma mark - Booster Item
public:
	void activateCapsule();
	void activateBooster(BoosterItemType type);
	void setupItemEffect(ItemEffect *effect);
	
	
public:
	float getItemEffectRemainTime(int type);
	float getPowerUpEffectRemainTime(int type);
	float getBoosterEffectRemainTime(int type);
	
#pragma mark - Power UP ITEM (in game collectible item)
public:
	ItemEffect *createPowerUpItem(ItemEffect::Type type);
	
	void detachPowerUpEffect(ItemEffect *currentEffect);
	void activatePowerUpByType(ItemEffect::Type effect);
	
	
	
	void attachPowerUp(ItemEffect *effect);
	void detachPowerUp(ItemEffect::Type type);
	
	
private:
	void clearAllPowerUpEffect();
	void sendEventToPowerUpEffect(ItemEffect::Type effect, ItemEffect::Event event);
	void removeExistingPowerUp(ItemEffect::Type effect);
	void addPowerUpItemEffectToPlayer(ItemEffect *effect);
	void updatePowerUpEffects(float delta);
	
//private:
//    CC_SYNTHESIZE(Animation *, mClimpUpAnimate, ClimpUpAnimate);
//    CC_SYNTHESIZE(Animation *, mSideWalkAnimate, SideWalkAnimate);
private:
	bool mIsReviving;
	ItemEffect *mItemEffect;
    int mSpeedUpOffset;
	
	Map<int, ItemEffect *> mPowerUpEffectMap;
	

#pragma mark - PackageBox
public:
	void setPackageVisible(bool flag);
	void updatePackageBoxAnimation();
	void showGivingPackage(const std::function<void()> &callback);
    void showBreakingPackage(const std::function<void()> &callback);
	
private:
	void setupPackageBox();
	
private:
	PackageBox *mPackageBox;
	std::function<void()> mGivingCallback;

#pragma mark - Dialogue
public:
	enum DialogType {
		DialogNone,				// Don't show any dialog and hidden
		DialogOnMyWay,
		DialogComming,
		DialogThankYou,
		DialogEnjoy,
	};
public:
	void closeDialog();
	void showDialog(DialogType dialog, float delay, float duration);

private:
	void setupDialog();
	int getDialogID(const DialogType &dialogType);

private:
	Sprite *mDialogSprite;

public:
	void updateMainAttribute();
	
	
#pragma mark - Player Status
public:
	bool isStunning();
	
#pragma mark - Player State
public:
	enum PlayerState {
		StateNone = 0,		// Default one
		StateNormal = 1,
		StateStun = 2,
	};

public:
	void changePlayerState(const PlayerState &state);
	
	
private:
	void enterPlayerStateNormal();
	void updatePlayerStateNormal(float delta);
	
	void enterPlayerStateStun();
	void updatePlayerStateStun(float delta);
	
	void onReviveDone();
	
	void setRespawnPosition();		// the position after stun
	
	void setStunInvulnerable(float duration);
	
private:
	PlayerState mPlayerState;

	// For Stun and Respawn handling
	float mRespawnCooldown;			// Turn to
	float mStunCooldown;
	bool mIsRespawning;				//

	
#pragma mark - Distance Reward
public:
	void resetDistanceReward();
	void enableDistanceReward(int money, int distanceInterval);
	void disableDistanceReward();
	
private:
	void checkDistanceReward();
	void giveDistanceReward();
	
private:
	bool mEnableDistanceReward;
	int mNextRewardDistance;			// Run
	int mDistanceRewardMoney;
	int mDistanceRewardInterval;

	
#pragma mark - Update Distance
public:
	CC_SYNTHESIZE(float, mTravelDistance, TravelDistance);		// pixel base
	
	float getTravelDistanceInMetre();
	void setInitialPosition(const Vec2 &pos);
	void updateTravelDistance();

private:
	void updateDistanceScore();
private:
	int mLastScoreDistanceMetre;
	
private:
	void markInitialPosition();
	
private:
	float mStartPositionY;		// starting positionY
	float mLastPositionY;	//
	
#pragma mark - Core Attribute
public:
	CC_SYNTHESIZE(int, mMaxSpeed, MaxSpeed);
	CC_SYNTHESIZE(int, mAcceleration, Acceleration);
	CC_SYNTHESIZE(float, mTurnSpeed, TurnSpeed);		// Extra speed when walk on the horizontal line
	CC_SYNTHESIZE(float, mStunReduction, StunReduction);

	float getStunDuration();
public:
	virtual Vec2 getFinalSpeed() const override;
	
	void resetSpeed();		// turn current speed to zero
	void updateSpeed(float delta);
	Vec2 getSpeedVector() const;
	
	bool isSpeedingUp();
private:
	float mCurrentSpeed;
	

	
#pragma mark - Particle & Effect
public:
	void addTrailParticle();
	void removeTrailParticle();
	
protected:
	void updateMotionStyle();
	
private:
	ParticleSystemQuad *mTrailParticle;
	int mMotionStyle;
	
#pragma mark - Enter/ExitSlopeLine
public:
	virtual void onEnterSlopeLine();
	virtual void onExitSlopeLine();

	
#pragma mark - Position Recorder
private:
	PositionRecorder *mPositionRecorder;

private:
    AnimeNode *mMaxSpeedVfx;
	
	
	
};



#endif /* Player_hpp */
