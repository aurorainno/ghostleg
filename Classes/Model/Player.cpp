
//
//  Player.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2016.
//
//

#include "Player.h"
#include "GameWorld.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "GameTouchLayer.h"
#include "StringHelper.h"
#include "MasteryManager.h"
#include "ItemEffect.h"
#include "Enemy.h"
#include "DebugInfo.h"
#include "GameSound.h"
#include "GameMap.h"
#include "ItemEffectFactory.h"
#include "SuitManager.h"
#include "AnimationHelper.h"
#include "Analytics.h"
#include "VisibleRect.h"
#include "DogManager.h"
#include "ModelLayer.h"
#include "EffectLayer.h"
#include "ReduceEnergyBehaviour.h"
#include "EnemyData.h"
#include "BoosterData.h"
#include "ItemManager.h"
#include "PackageBox.h"
#include "ActionShake.h"
#include "PositionRecorder.h"
#include "GameplaySetting.h"
#include "AnimeNode.h"

#pragma mark - Class Implementation

const int kDistanceScore = 1;

const int kZOrderItemEffect = 1000;	// Highest
const int kZOrderDialog = 2000;

const int kTagDieAction1 = 777;
const int kTagDieAction2 = 778;

//const int kStunDuration = 1;		// 2 seconds
const int kStunDuration = 1.5;		// 2 seconds

const int kStunDurationMin = 0.2;

const int kMaxEnergy = 100;
const int kSpeedY = 115;
const int kSpeedX = 115;
const float kAcceleration = 0.5;
const int kMaxSpeed = 400;
const int kMaxSpeedX = 600;
const int kRevivingDuration = 2;		// 3 seconds
const int kRevivingBlinks = 8;			// Number of blinks appear

const float kDialogScale = 0.5f;

const float kInitialSpeed = 80;

const float kTravelRewardInterval = 5000;		// 5000px = 50m

Player::Player()
: MovableModel()
, mItemEffect(nullptr)
, mIsReviving(false)
, mIsDying(false)
, mIsDashing(false)
, mPowerUpEffectMap()
, mPackageBox(nullptr)
, mPlayerState(StateNone)
, mCurrentSpeed(0)
, mMaxSpeed(250)
, mTurnSpeed(100)
, mAcceleration(100)
, mStunReduction(0)
, mTrailParticle(nullptr)
, mPositionRecorder(nullptr)
, mTravelDistance(0)
, mMaxSpeedVfx(nullptr)
{
	
}
Player::~Player()
{
	removeVisualEffect();
	CC_SAFE_RELEASE(mPositionRecorder);
	// resetItemEffect();	// Remove the itemEffect as well as the related link
}

bool Player::init()
{
	bool isOk = MovableModel::init();	// calling the init of parent class
	if (isOk == false)
	{
		return false;
	}
	
	setUseQuickHitboxCheck(true);
	
	//setRootScale(1.0f);
	//this->setScale(getDefaultScale());
    
    //setScale(1.0);
	setScale(0.9f);

	mSpeedX = kSpeedX;
	mSpeedY = kSpeedY;
	
//    AnimeNode *animeNode = AnimeNode::create();
//    animeNode->setup("particle_maxspeed_1.csb");
//    mLowerEffectNode->addChild(animeNode);
//    mMaxSpeedVfx = animeNode;
//    mMaxSpeedVfx->setVisible(false);
    
	// Initial setup
	mDefaultDir = DirUp;
	mDir = mDefaultDir;
	mNextDir = DirNone;
	setHoriLine(nullptr);

	mPositionRecorder = PositionRecorder::create();
	CC_SAFE_RETAIN(mPositionRecorder);

	setupDialog();
	
	reset();
    resetPlayerEnergy();
    resetSpeed();
	resetDistanceReward();
    
    
	
	log("DEBUG: initial dir=%d", mDir);
	
	return true;
}


void Player::autoSpeedUpByTime(float delta)
{
    mSpeedX += kAcceleration * delta;
    mSpeedY += kAcceleration * delta;
    
    if(mSpeedX > kMaxSpeedX){
        mSpeedX = kMaxSpeedX;
    }
    if(mSpeedY > kMaxSpeed){
        mSpeedY = kMaxSpeed;
    }
}

void Player::update(float delta)
{
	if(StateNormal == mPlayerState) {
		updatePlayerStateNormal(delta);
	} else if(StateStun == mPlayerState) {
		updatePlayerStateStun(delta);
	}
	
  	//log("DEBUG: delta=%f y=%f dir=%d", delta, getPositionY(), mDir);
}


void Player::resetPlayerEnergy()
{
    mPlayerEnergy = getTotalEnergy();
}


bool Player::regenerateEnergy(float delta)
{
	float max = getTotalEnergy();
	if(mPlayerEnergy >= max) {
		return false;
	}
	
	float amount = delta * getRegenerateRate();
	addPlayerEnergy(amount);
	//log("playerEnergy=%f amount=%f rate=%f", getPlayerEnergy(), amount, getRegenerateRate());
	
	return true;
}

void Player::consumeEnergyByLine()
{
	float amount = getConsumeRate();
    float offsetY = GameWorld::instance()->getCameraY();
	deductPlayerEnergy(amount);
}

void Player::deductPlayerEnergy(float amount)
{
    GameWorld::instance()->getEffectLayer()->showDeduceEnergyEffect(amount);
    mPlayerEnergy -= amount;
    if(mPlayerEnergy < 0){
		mPlayerEnergy = 0;
	}
}


void Player::addPlayerEnergy(float amount)
{
	float max = getTotalEnergy();
	if(mPlayerEnergy >= max) {
		return;		// nothing
	}
	
    mPlayerEnergy += amount;
    if(mPlayerEnergy > max) {
		mPlayerEnergy = max;
	}
    
}

void Player::reset()
{
	mCollectCoin = 0;
    mNumberOfCapsuleUsed = 0;
	mTravelDistance = 0;
	mIsReviving = false;
	mIsDying = false;
	mIsDashing = false;
	
	mTravelDistance = 0;
	mLastScoreDistanceMetre = 0;
    mSpeedUpOffset = 0;
	
    mDir = mDefaultDir;
    mNextDir = DirNone;
    setHoriLine(NULL);

	mSpeedX = kSpeedX;
	mSpeedY = kSpeedY;
	
	removeEffectStatus();
	
	resetItemEffect();
	clearAllPowerUpEffect();
    
	
	// Set the default Effect
	
	//
	GameManager::instance()->getMasteryManager()->setPlayerMasteryAttribute(this);
	
	// Setting the Dog Animation
	//int animeID = DogManager::instance()->getSelectedDogAnimeID();
	std::string csbName = GameRes::getSelectedCharacterCsb();
	setup(csbName);
	
    if(mMaxSpeedVfx){
        mMaxSpeedVfx->removeFromParent();
    }
    AnimeNode *animeNode = AnimeNode::create();
    animeNode->setup("particle_maxspeed_1.csb");
    animeNode->setPosition(0,-50);
    mUpperEffectNode->addChild(animeNode);
    mMaxSpeedVfx = animeNode;
    mMaxSpeedVfx->setVisible(false);

	// Special position for Astrodog
	mStatusPos = Vec2(-30, 40);

	updateActionByDir();
	
	changePlayerState(PlayerState::StateNormal);
	
	mPositionRecorder->reset();
	
	resetDistanceReward();
	
	//addTrailParticle();
}

void Player::setup(const std::string &csbName, Action defaultAction)
{
	// Clear up first
	if(mPackageBox != nullptr) {
		mRootNode->removeFromParent();
		mPackageBox = nullptr;
	}
	if(mRootNode != nullptr) {
		mRootNode->removeFromParent();
		mRootNode = nullptr;
	}
	
	
	//
	MovableModel::setup(csbName, defaultAction);
	
	setupPackageBox();
	
	// Setup the package animation
	//setupHitboxes();
	
	

}


void Player::addCoin(int newCoin)
{
	if(newCoin == 0) {
		return;		// nothing need to do
	}
	mCollectCoin += newCoin;
	
	//GameWorld::instance()->sendEvent(GameWorldEvent::EventUpdateCoin, mCollectCoin);
}


float Player::getTravelDistanceInMetre()
{
	return getTravelDistance() / 100;
}


std::string Player::infoMainAttribute()
{
	std::string result = "";
	
	result += StringUtils::format("MaxSpeed=%d", mMaxSpeed);
	result += StringUtils::format(" Acceleration=%d", mAcceleration);
	result += StringUtils::format(" TurnSpeed=%f", mTurnSpeed);
	result += StringUtils::format(" StunReduction=%f", mStunReduction);
	
	return result;
}

std::string Player::infoAttribute()
{
	std::string result = "";
	
	
	result += "TotalEnergy=" + FLOAT_TO_STR(mTotalEnergy) + "\n";
	result += "ConsumeRate=" + FLOAT_TO_STR(mConsumeRate) + "\n";
	result += "DoubleStarRate=" + FLOAT_TO_STR(mDoubleStarRate) + "\n";
	result += "RegenerateRate=" + FLOAT_TO_STR(mRegenerateRate) + "\n";
	result += "Status=" + INT_TO_STR(mEffectStatus) + "\n";
	
	std::string effect = (mItemEffect == nullptr) ? "none" : mItemEffect->toString();
	result += "Effect=" + effect + "\n";
	
	return result;
}


void Player::attachItemEffect(ItemEffect *effect)
{
	resetItemEffect();		// Reset Last Effect first
	
	//
	if(effect) {
		effect->setPlayer(this);
		effect->setPosition(Vec2::ZERO);
		addChild(effect, kZOrderItemEffect);
		effect->attach();
		
		GameWorld::instance()->enableCapsuleButton(effect->getType());
	}
	mItemEffect = effect;
}

void Player::detachItemEffect(ItemEffect *effect) {
	if(effect == mItemEffect) {
		resetItemEffect();
	}
}

ItemEffect *Player::getItemEffect() const
{
	return mItemEffect;
}

void Player::resetItemEffect()
{
	if(mItemEffect) {
		mItemEffect->detach();
		mItemEffect->removeFromParent();
		mItemEffect = nullptr;
	}
	GameWorld::instance()->disableCapsuleButton();
}

bool Player::hitBy(MovableModel *model) {		// Return true if player is die after hit
	
	
	// Case when player is invulnerable
	EffectStatus status = getEffectStatus();
	
	// ken: NEED REFACTOR HERE!!! shouldn't use any Behaviour subclass here!!!
	
    Enemy *enemy = dynamic_cast<Enemy *>(model);
    ReduceEnergyBehaviour *reduceEnergyBehaviour = dynamic_cast<ReduceEnergyBehaviour*>(enemy->getBehaviour());
    
    if(reduceEnergyBehaviour && enemy){
        Vec2 force = Vec2(0, getSpeedY() * 2);
        enemy->beHit();
        reduceEnergyBehaviour->onHit();
        
        return false;
    }
	
	if(status == EffectStatusInvulnerable)
	{
		if(enemy && enemy->getBehaviour()) {
            if(enemy->getBehaviour()->getData()->getDestroyable()){
                Vec2 force = Vec2(0, getSpeedY() * 2);
                enemy->beHit();
            }
		}
		return false;
	}

	if(status == EffectStatusShield)
	{
		sendEventToPowerUpEffect(ItemEffect::ItemEffectShield, ItemEffect::Event::PlayerIsHit);
		return false;
	}

	
	// Player is hitted
	GameSound::playSound(GameSound::Hit);
	log("DEBUG: hitted model=%s debug=%s", model->toString().c_str(),
						DebugInfo::instance()->info().c_str());


	return true;
}



void Player::setupDefaultItemEffect()
{
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectShield;
//	ItemEffect::Type type = ItemEffect::Type::ItemEffectGrabStar;
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectMissile;
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectShield;
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectInvulnerable;
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectSlow;
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectTimeStop;
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectTim
//	//ItemEffect::Type type = ItemEffect::Type::ItemEffectNone;
//
	//ItemEffect *effect = ItemEffectFactory::createEffectByType(type);
	ItemEffect *effect = GameManager::instance()->getSuitManager()->getPassiveItemEffect();
	if(effect) {
		//effect->setMainProperty(1);
		//effect->setMainProperty(2);
		//effect->setMainProperty(100);
		//effect->setMainProperty(30);
		//effect->setMainProperty(5);

		attachItemEffect(effect);
	}
}

void Player::onReviveDone()
{
			// Activate the Chaser when revive done
}

void Player::setReviving(float reviveDuration, bool reActivateChaser)
{
	if(reviveDuration <= 0) {
		reviveDuration = 0.1;
	}
	
	mIsReviving = true;
	mIsDying = false;
	
	std::function<void ()> func = [=]() {
		if(reActivateChaser) {
			// log("DEBUG: Player.setReviving: reActivateChaser called");
			GameWorld::instance()->activateChaser();
		}
		
		onReviveDone();
		mIsReviving = false;
	};
	
	AnimationHelper::setAlphaBlink(this, reviveDuration, 8, 100, 200, func);
//    Sequence *seq = Sequence::create(DelayTime::create(reviveDuration),CallFunc::create(func) , NULL);
//    this->runAction(seq);
}

void Player::showDyingAnimation()
{
	// Movement
	mDeathPosition = this->getPosition();
	// log("Player is dying: pos=%s", POINT_TO_STR(mDeathPosition).c_str());
    // Rotation effect
    int sign = isFaceLeft() ? 1 : -1;
    RotateBy *rotate = RotateBy::create(0.5, sign*20);
    //	Repeat *repeat = Repeat::create(rotate, 1);
    rotate->setTag(kTagDieAction1);
    runAction(rotate);
    
//    bool atRight = getPositionX() > VisibleRect::center().x;
    float toX = getPositionX() + (isFaceLeft() ?  30 : -30);
    float toY = getPositionY() + 70;
    
    auto moveTo = MoveTo::create(1.5, Vec2(toX, toY));
    auto easeMove = EaseOut::create(moveTo, 2);
    easeMove->setTag(kTagDieAction2);
    runAction(easeMove);

//	

	
//	GameSound::playSound(GameSound::VoiceBark);
	
    
    this->setAction(GameModel::Action::Die);

}


void Player::stopDyingAnimation()
{
    
    this->setAction(GameModel::Action::Climb);
    this->stopActionByTag(kTagDieAction1);
    this->stopActionByTag(kTagDieAction2);
    this->setRotation(0.0f);
    this->setPosition(mDeathPosition);
	
	// log("stopDyingAnimation: Player is dying end: pos=%s", POINT_TO_STR(getPosition()).c_str());
    
}



bool Player::isReviving()
{
	return mIsReviving;
}

int Player::getSpeed()
{
	return (int) mSpeedY;
}

void Player::setSpeed(int newValue)
{
	mSpeedX = newValue*2.5;
	mSpeedY = newValue;
}


void Player::increaseSpeed(int incValue, int maxValue)
{
	int newValue = mSpeedY + incValue;
	if(maxValue > 0 && newValue > maxValue) {
		newValue = maxValue;
	}
	
	
	mSpeedX = newValue;
	mSpeedY = newValue;
}

void Player::activateSpeedUp(int speed)
{
    mSpeedUpOffset = speed;
}

void Player::cancelSpeedUp()
{
	mSpeedUpOffset = 0;
}

bool Player::isInvulnerable()
{
	EffectStatus status = getEffectStatus();
	
	return (status == EffectStatusInvulnerable);
}

//	i
//	{
//		if(enemy && enemy->getBehaviour()) {
//			if(enemy->getBehaviour()->getData()->getDestroyable()){
//				Vec2 force = Vec2(0, getSpeedY() * 2);
//				enemy->beHit(force);
//			}
//		}
//		return false;
//	}
//	
//	if(status == EffectStatusShield)
//	{
//		if(mItemEffect) {
//			mItemEffect->onEvent(ItemEffect::Event::PlayerIsHit);
//		}
//		return false;
//	}
//	
//}

bool Player::hasShield()
{
	return (getEffectStatus() == EffectStatusShield);
}

void Player::handleShieldHitted()
{
	if(mItemEffect) {
		mItemEffect->onEvent(ItemEffect::Event::PlayerIsHit);
	}
}

bool Player::handleBeingHitted()
{
	if(mPlayerState == StateStun || mIsReviving) {
		return false;
	}
	
	if(hasShield()) {
		handleShieldHitted();
		return false;
	} else if(isInvulnerable()) {
		return false;
	} else {
		GameSound::playSound(GameSound::Hit);
		// player is died
		return true;
	}
	
}

void Player::setEffectStatus(EffectStatus status, float duration)
{
 //   MovableModel::setEffectStatus(status,duration);
    mEffectStatus = status;
    if(duration < 0) {
        mHasEffectStatusDuration = false;
    } else {
        mHasEffectStatusDuration = true;
        mEffectStatusRemainTime = duration;
    }

    
    GameRes::Particle particle = GameRes::Particle::None;
    switch (status) {
        case EffectStatusSlow:
            particle = GameRes::Particle::Slow;
            break;
            
        default:
            break;
    }
    
    if(particle!=GameRes::Particle::None){
        addParticleEffect(particle,Vec2(0,-120));
    }
    

    std::string csbName = getAnimeCsbNameByEffect(status);
    if(csbName == ""){
        return;
    }
    
    float scale = 0.5f;
    bool isFront;
    
    switch (status) {
        case EffectStatusInvulnerable:
        {
            scale = 0.4;
            isFront = false;
            break;
        }
            
        case EffectStatusSlow:
        {
            scale = 0.7;
            isFront = true;
            break;
        }
            
        default : {
            break;
        }
    }
    
    setStatusAnimationByEffect(status,scale,isFront);

}

void Player::removeEffectStatus()
{
    MovableModel::removeEffectStatus();
    removeVisualEffect();
}



ItemEffect *Player::createPowerUpItem(ItemEffect::Type type)
{
	ItemEffect *effect = ItemEffectFactory::createEffectByType(type);
	if(effect == nullptr) {
		return nullptr;
	}
	// Mark it as powerup
	effect->setIsPowerUp(true);
	
	// Add the attribute for the type
	ItemManager::instance()->defineItemEffectProperty(effect);
	
	
	// log("DEBUG: powerupItem=%s", effect->toString().c_str());
	
	
	return effect;
}


void Player::activatePowerUpByType(ItemEffect::Type effectType)
{
	// XXXXX
	// Create the ItemEffect
	ItemEffect *effect = createPowerUpItem(effectType);
	if(effect == nullptr) {
		log("Player::activatePowerUpByType: not effect for type=%d", effectType);
		return;
	}
	
	addPowerUpItemEffectToPlayer(effect);
	
}

void Player::updatePowerUpEffects(float delta)
{
	std::vector<int> mapKeyVec = mPowerUpEffectMap.keys();
	
	for(int key : mapKeyVec)
	{
		ItemEffect *effect = mPowerUpEffectMap.at(key);
		if(effect != nullptr) {
			effect->update(delta);
		}
	}

}

void Player::addPowerUpItemEffectToPlayer(ItemEffect *effect)
{
	ItemEffect::Type effectType = effect->getType();

	// Remove the existing power first
	removeExistingPowerUp(effectType);


	// Add the effect to player
	effect->setPlayer(this);
	effect->setPosition(Vec2::ZERO);
	addChild(effect, kZOrderItemEffect);
	effect->attach();
	
	
	
	// Add the effect
	mPowerUpEffectMap.insert((int) effectType, effect);
}

void Player::removeExistingPowerUp(ItemEffect::Type effect)
{
	ItemEffect *currentEffect = mPowerUpEffectMap.at((int) effect);
	
	if(currentEffect != nullptr) {
		currentEffect->detach();	// note: will call to detachPowerUp too
	}
}

void Player::detachPowerUp(ItemEffect::Type type)		// Call from Detach
{
	// TODO: !!!
	int key = (int) type;
	
	ItemEffect *currentEffect = mPowerUpEffectMap.at(key);
	
	if(currentEffect != nullptr) {
		currentEffect->removeFromParent();
	}
	
	// Remove from the PowerUp Map
	mPowerUpEffectMap.erase(key);
}

void Player::detachPowerUpEffect(ItemEffect *currentEffect)
{
	if(currentEffect == nullptr) {
		return;
	}
	
	int key = (int) currentEffect->getType();
	
	currentEffect->detach();
	currentEffect->removeFromParent();
	
	// Remove from the PowerUp Map
	mPowerUpEffectMap.erase(key);
	
	
}


void Player::sendEventToPowerUpEffect(ItemEffect::Type effect, ItemEffect::Event event)
{
	ItemEffect *targetEffect = mPowerUpEffectMap.at((int) effect);
	
	if(targetEffect == nullptr) {
		return;
	}
	
	targetEffect->onEvent(event);
}

void Player::clearAllPowerUpEffect()
{
	std::vector<int> mapKeyVec = mPowerUpEffectMap.keys();
	
	for(int key : mapKeyVec)
	{
		ItemEffect *effect = mPowerUpEffectMap.at(key);
		if(effect != nullptr) {
			effect->detach();
		}
	}
}



#pragma mark - Booster Item
void Player::activateCapsule()
{
	if(mItemEffect == nullptr) {
		log("activateCapsule: no effect yet");
		return;
	}
	
//	if(mIsDying) {
//		return;
//	}
	
	mNumberOfCapsuleUsed ++;
	
	mItemEffect->activate();
}


void Player::activateBooster(BoosterItemType type)
{
	
	ItemEffect::Type itemEffectType = (ItemEffect::Type) type;
	ItemEffect *effect = ItemEffectFactory::createEffectByType(itemEffectType);
	if(effect == nullptr) {
		log("Player::activateBooster: effect is null");
		return;
	}
	
	
	///
	BoosterData *boosterData = ItemManager::instance()->getBoosterData(type);
	if(boosterData == nullptr) {
		log("Player::activateBooster: boosterData is null. type=%d", type);
		return;
	}
	
	// Define the basic attribute
	boosterData->setItemEffectAttribute(effect);
	ItemManager::instance()->defineItemEffectProperty(effect);	// adjust the based on the gameplaySetting
	
	setupItemEffect(effect);
	
	activateCapsule();
}


void Player::setupItemEffect(ItemEffect *effect)
{
	resetItemEffect();		// Reset Last Effect first
	
	//
	if(effect) {
		effect->setPlayer(this);
		effect->setPosition(Vec2::ZERO);
		addChild(effect, kZOrderItemEffect);
		effect->attach();
	}
	mItemEffect = effect;
}

float Player::getItemEffectRemainTime(int type)
{
	if(type == ItemEffect::Type::ItemEffectCashOut
		|| type == ItemEffect::Type::ItemEffectStarMagnet
	    || type == ItemEffect::Type::ItemEffectSpeedUp) {
		return getPowerUpEffectRemainTime(type);
	} else {
		return getBoosterEffectRemainTime(type);
	}
	
}

float Player::getPowerUpEffectRemainTime(int type)
{
	ItemEffect *currentEffect = mPowerUpEffectMap.at((int) type);
	if(currentEffect == nullptr) {
		return 0;
	}
	
	return currentEffect->getRemainTime();
}

float Player::getBoosterEffectRemainTime(int type)
{
	if(mItemEffect == nullptr) {
		return 0;
	}
	
	if(mItemEffect->getType() != type) {
		return 0;
	}
	
	return mItemEffect->getRemainTime();
}

std::string Player::getName()
{
	return "player";
}


#pragma mark - PackageBox
void Player::setPackageVisible(bool flag)
{
	if(mPackageBox) {
		mPackageBox->setVisible(flag);
		
		if(flag) {
			updatePackageBoxAnimation();
		}
	}
}

void Player::updatePackageBoxAnimation()
{
	if(mPackageBox == nullptr) {
		return;
	}
	
	mPackageBox->showAnimation(PackageBox::Animation::MovingUp);
}

void Player::setupPackageBox()
{
	Node *parent = mRootNode->getChildByName("packageNode");
	if(parent == nullptr) {
		log("ERROR: Player:setupPackageBox: parent is null");
		return;
	}
	
	mPackageBox = PackageBox::create();
	parent->addChild(mPackageBox);		// Position 0, 0
	mPackageBox->setVisible(false);
}


void Player::showGivingPackage(const std::function<void()> &callback)
{
	if(mPackageBox == nullptr) {
		return;
	}
	
	mGivingCallback = callback;
	
	mPackageBox->showAnimation(PackageBox::Animation::Disappear, [&]{
		mPackageBox->setVisible(false);
		if(mGivingCallback) {
			mGivingCallback();
			mGivingCallback = nullptr;
		}
	});
}

void Player::showBreakingPackage(const std::function<void ()> &callback)
{
    if(mPackageBox == nullptr) {
        return;
    }
    
    Node *packageNode = mRootNode->getChildByName("packageNode");
    Vec2 packagePos = packageNode->getPosition();
    
    addCsbAnimeByName("particle_invshield_hit_2.csb", true, 0.5, packagePos);
    playCsbAnime("particle_invshield_hit_2.csb", "active", false, true);
    
    if(mRootNode->getScaleX()<0){
        mPackageBox->setScaleX(-1.0);
    }
    
    mPackageBox->showAnimation(PackageBox::Animation::MonsterChase, [=]{
        mPackageBox->setVisible(false);
        mPackageBox->setScaleX(1.0);
        if(callback) {
            callback();
        }
    });
}



#pragma mark - Dialogue
//void closeDialog();
//void showDialog(Dialog dialog, float delay, float duration);
//
//private:
//void setupDialog();
//
//private:
//Sprite *mDialogSprite;
void Player::closeDialog()
{
	mDialogSprite->setVisible(false);
}

void Player::showDialog(DialogType dialog, float delay, float duration)
{
	int dialogID = getDialogID(dialog);
	if(dialogID <= 0) {
		mDialogSprite->setVisible(false);
		return;
	}
	
	std::string dialogFile = StringUtils::format("dialogue/ingame_dialog_%02d.png", dialogID);
	mDialogSprite->setTexture(dialogFile);
	
	AnimationHelper::setToastEffect(mDialogSprite, kDialogScale, delay, duration, 0.2, [=]{
		mDialogSprite->setVisible(false);
	});
}

int Player::getDialogID(const DialogType &dialogType)
{
	switch(dialogType) {
		case DialogOnMyWay		: return 2;
		case DialogComming		: return 3;
		case DialogThankYou		: return 7;
		case DialogEnjoy		: return 9;
		default					: return 0;
	}
}

void Player::setupDialog()
{
	std::string spriteFile = "dialogue/ingame_dialog_02.png";
	Sprite *sprite = Sprite::create(spriteFile);
	
	sprite->setScale(kDialogScale);
	sprite->setAnchorPoint(Vec2(0.5, 0));
	sprite->setPosition(Vec2(0, 30));
	sprite->setLocalZOrder(kZOrderDialog);
	
	addChild(sprite);
	
	mDialogSprite = sprite;
	
	mDialogSprite->setVisible(false);
}


#pragma mark - Player Status
bool Player::isStunning()
{
	if(StateStun == mPlayerState && mStunCooldown >= 0) {
		return true;
	}
	
	return false;
}

#pragma mark - Player State
void Player::changePlayerState(const PlayerState &state)
{
	mPlayerState = state;
	if(StateNormal == mPlayerState) {
		enterPlayerStateNormal();
	} else if(StateStun == mPlayerState) {
		enterPlayerStateStun();
	}
	
}

void Player::enterPlayerStateNormal()
{
	GameWorld::instance()->setIsBraking(false);
}

void Player::updatePlayerStateNormal(float delta)
{
	GameModel::update(delta);
	// autoSpeedUpByTime(delta);
	updateSpeed(delta);
	
	handleEffectStatus(delta);
	
	//movement
	move(delta);
	
	// Record position
	//if(! isReviving()) {
		// Just record the position really can respawn  // TODO check the invulnerable position
		//mPositionRecorder->recordPosition(getPosition(), delta);
	//}
	
	// Power UP
	updatePowerUpEffects(delta);
	
	// Effect
	if(mItemEffect) {
		mItemEffect->update(delta);
	}
	
}

void Player::addStunAnimation()
{
    addCsbAnimeByName("particle_dizzy.csb", true, 0.5, Vec2(0,30));
    playCsbAnime("particle_dizzy.csb", "active", true);
}

void Player::removeStunAnimation()
{
    removeCsbAnimeByName("particle_dizzy.csb");
}

void Player::enterPlayerStateStun()
{
	Analytics::instance()->logPlayerHit(GameWorld::instance()->getCurrentMapID());
	
	//
	if(mItemEffect != nullptr && mItemEffect->isDetachWhenHit()) {
		if(mItemEffect->getIsPowerUp() == false) {
			detachItemEffect(mItemEffect);
		}
	}
	
	
	// Stop player
	resetSpeed();
	
	// Reset Cooldown counter
	mStunCooldown = getStunDuration();
	// log("Player.enterStateStun: stunDuration=%f", mStunCooldown);

	// TODO: Show Shaking Effect
	//Node *node, float duration, int repeatCount, int opacity1, int opacity2,
	Shake *shake = Shake::create(mStunCooldown, 4);
	mRootNode->runAction(shake);
    addStunAnimation();
    
    GameWorld::instance()->getModelLayer()->adjustChaserAppearOnBottom();
	GameWorld::instance()->getModelLayer()->increaseChaserSpeed();

	
//	AnimationHelper::setAlphaBlink(mRootNode, mStunCooldown, 5, 100, 180);

	
	mIsRespawning = false;		// turn to true after stun end
	mRespawnCooldown = 3.0;		// 2 seconds before moving
	// log("***DEBUG: recorded position:\n%s\n", mPositionRecorder->toString().c_str());
	
	// Braking
	GameWorld::instance()->setIsBraking(false);
	
	// clear map line and reset Touch
//	GameWorld::instance()->getMap()->cleanupPlayerRouteLine();
//	mDir = mDefaultDir;
//	mNextDir = DirNone;
//	setHoriLine(nullptr);
	GameWorld::instance()->getGameTouchLayer()->reset();
}

float Player::getStunDuration()
{
	float finalDuration = kStunDuration - mStunReduction;
	
	if(finalDuration < kStunDurationMin) {
		finalDuration = kStunDurationMin;
	}
	
	return finalDuration;
//	return 0.3;
}

void Player::updatePlayerStateStun(float delta)
{
//	if(mIsRespawning) {
//		// Player is respawning
//		mRespawnCooldown -= delta;
//		if(mRespawnCooldown > 0) {
//			return;		// nothing to do yet
//		}
//		
//		
//	} else {
	
	// Player is stunning
	mStunCooldown -= delta;
	if(mStunCooldown > 0) {
		return;		// nothing to do yet
	}
	
	// Start respawning
//	setRespawnPosition();
	
	resetSpeed();
	
	setReviving(mRespawnCooldown, false);
	removeStunAnimation();

	// End of Respawning
	changePlayerState(StateNormal);
		
		
//		mIsRespawning = true;
//	}
	
	
}




#pragma mark - Core Attribute

Vec2 Player::getFinalSpeed() const
{
	if(GameWorld::instance()->getIsBraking()) {
		// Testing: using speed up instead
		//Vec2 increasedSpeed = getSpeedVector() + Vec2(100, 150);
		//return increasedSpeed;
        mMaxSpeedVfx->setVisible(false);
		return Vec2(0.0f, 0.0f);		// Hold to brake
	}
	

	Vec2 originSpeed = getSpeedVector();
	// log("DEBUG: player: speed=%s", POINT_TO_STR(originSpeed).c_str());
//	if(getIsDashing()){
//		originSpeed = originSpeed * 2.3;
//	}
	
//	EffectStatus status = getEffectStatus();
//	switch(status) {
//		case EffectStatusNone				: return originSpeed;
//		case EffectStatusSlow				: return originSpeed * mSlowFactor;
//		case EffectStatusSpeedUp            : return originSpeed;
//		default								: return originSpeed;
//	}
    return originSpeed + Vec2(mSpeedUpOffset,mSpeedUpOffset);
}

Vec2 Player::getSpeedVector() const
{
	
	float speedX = mCurrentSpeed + mTurnSpeed;
	float speedY = mCurrentSpeed;
	
	Vec2 speedVec = Vec2(speedX, speedY);
	
	return speedVec;
}


void Player::resetSpeed()		// turn current speed to zero
{
	mCurrentSpeed = kInitialSpeed;
	
	mMotionStyle = ModelLayer::StyleNone;
//	GameWorld::instance()->getModelLayer()->changeMotionStyle(ModelLayer::StyleNone);
	
}

void Player::updateSpeed(float delta)
{
	//
	//updateMotionStyle();
	
	if(mCurrentSpeed >= mMaxSpeed) {
		mMaxSpeedVfx->setVisible(true);
		return;
	}
	
    mMaxSpeedVfx->setVisible(false);
	mCurrentSpeed += (mAcceleration * delta);
	
	//log("updateSpeed: speed=%f acce=%d max=%d", mCurrentSpeed, mAcceleration, mMaxSpeed);
	
	if(mCurrentSpeed > mMaxSpeed) {
		mCurrentSpeed = mMaxSpeed;
		
	}
	
	
}

bool Player::isSpeedingUp()
{
	return getEffectStatus() == EffectStatusSpeedUp;
}


#pragma mark - Particle & Effect
void Player::updateMotionStyle()
{
	
	int newMotion;
	
	if(StateStun == mPlayerState) {
		newMotion = ModelLayer::StyleNone;
	} else if(isSpeedingUp()) {
		newMotion = ModelLayer::StyleSpeedUp;
	} else if(mCurrentSpeed == mMaxSpeed) {
		newMotion = ModelLayer::StyleMaxSpeed;
	} else if(mCurrentSpeed > 100) {
		newMotion = ModelLayer::StyleNormal;
	} else {
		newMotion = ModelLayer::StyleNone;
	}
	
	if(mMotionStyle == newMotion) {
		return;
	}

	mMotionStyle = newMotion;
//	GameWorld::instance()->getModelLayer()->changeMotionStyle(
//								(ModelLayer::MotionStyle) mMotionStyle);

}

void Player::addTrailParticle()
{
	std::string filename = "particle/particle_speedy.plist";
	ParticleSystemQuad *particle = ParticleSystemQuad::create(filename);
	particle->setPosition(Vec2::ZERO);
	particle->setScale(0.4f);
	particle->setLocalZOrder(5);
	addChild(particle);
	
	mTrailParticle = particle;

}

void Player::removeTrailParticle()
{
	if(mTrailParticle != nullptr) {
		mTrailParticle->removeFromParent();
		mTrailParticle = nullptr;
	}
}

void Player::setRespawnPosition()
{
	Vec2 resultPos;
	bool isOkay = mPositionRecorder->findNearestSpawnPosition(getPosition(), resultPos);

	if(isOkay == false) {
		return;		// Don't change anything... suppose it is weird case
	}
	
	setPosition(resultPos);
	GameWorld::instance()->moveCameraToPlayer();
	
	// Also adjust the chaser
	GameWorld::instance()->adjustChaser();
	
	
	// Reset the recorder
	//mPositionRecorder->reset();
}


#pragma mark - Update Distance
void Player::setInitialPosition(const Vec2 &pos)
{
	setPosition(pos);
	markInitialPosition();
}

void Player::updateTravelDistance()
{
	float newY = getPositionY();
	if(newY <= mLastPositionY) {
		return;		// no need to update anything
	}
	
	float deltaDistance = newY - mLastPositionY;
	mLastPositionY = newY;
	
	// update the distance
	setTravelDistance(getTravelDistance() + deltaDistance);
	
	//log("TravelDistance: %f", getTravelDistance());
	
	// Check the Distance Reward when the travel distance has changed
	checkDistanceReward();
	
	updateDistanceScore();
}

void Player::updateDistanceScore()
{
	int newDistance = getTravelDistanceInMetre();
	if(newDistance <= mLastScoreDistanceMetre) {
		return;
	}
	
	GameWorld::instance()->addScoreWithGUI(kDistanceScore, ScoreTypeDistance, false);
	
	mLastScoreDistanceMetre = newDistance;
}

void Player::markInitialPosition()
{
	mStartPositionY = getPositionY();
	mLastPositionY = mStartPositionY;
	setTravelDistance(0);
}

//
//#pragma mark - Update Distance
//public:
//CC_SYNTHESIZE(float, mTravelDistance, TravelDistance);
//void markInitialPosition();
//
//private:
//void updateTravelDistance();
//private:
//float mStartPositionY;		// starting positionY
//float mLastPositionY;	//



#pragma mark - Distance Reward
void Player::resetDistanceReward()
{
	mNextRewardDistance = 99999;			// Run
	mDistanceRewardInterval = 999999;
	mDistanceRewardMoney = 0;
	
	
	// Setup the Reward Detail
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	if(setting == nullptr) {
		disableDistanceReward();
		return;
	}
	
	int rewardMoney = (int) setting->getAttributeValue(GameplaySetting::TravelReward);
	if(rewardMoney <= 0) {
		disableDistanceReward();
		return;
	}
	
	enableDistanceReward(rewardMoney, kTravelRewardInterval);		// 50 pixel
}
void Player::disableDistanceReward()
{
	mEnableDistanceReward = false;
}
								  
void Player::enableDistanceReward(int money, int distanceInterval)
{
	mEnableDistanceReward = true;
	mNextRewardDistance = distanceInterval;			// Run
	mDistanceRewardMoney = money;
	mDistanceRewardInterval = distanceInterval;
}

void Player::checkDistanceReward()		// Check and Give the Reward
{
	if(mEnableDistanceReward == false) {
		return;
	}
	
	if(getTravelDistance() < mNextRewardDistance) {
		return;		// not reach
	}
	
	mNextRewardDistance += mDistanceRewardInterval;
	
	giveDistanceReward();
}

void Player::giveDistanceReward()
{
	//log("DEBUG: currentDistance=%f nextRewardDistance=%d", getTravelDistance(), mNextRewardDistance);
	GameWorld::instance()->addCoinFromModel(this, mDistanceRewardMoney);
}


#pragma mark - Enter/ExitSlopeLine
void Player::onEnterSlopeLine()
{
	mPositionRecorder->recordPositionNow(getPosition());
}

void Player::onExitSlopeLine()
{
	mPositionRecorder->recordPositionNow(getPosition());
	
}
