//
//  Enemy.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2016.
//
//

#include "Enemy.h"
#include "Constant.h"
#include "GameWorld.h"
#include "GameSound.h"
#include "EnemyFactory.h"
#include "StringHelper.h"
#include "EnemyBehaviour.h"
#include "EnemyData.h"
#include "RandomHelper.h"
#include "CommonType.h"
#include "GameWorld.h"
#include "EffectLayer.h"
#include "EnemyData.h"
#include "VisibleRect.h"
#include "Player.h"
#include "ModelLayer.h"
#include "GameplaySetting.h"
#include <map>
#include "GeometryHelper.h"
#include "TimeMeter.h"

const float kCheckThreshold = 500;

namespace {
	void addScore(Enemy *enemy)
	{
		Vec2 pos = enemy->convertToWorldSpaceAR(Vec2::ZERO);
		// Add Score
		GameWorld::instance()->addScoreWithAnimation(kEnemyScore, ScoreTypeKill, 0.7f, pos);	// 10 is monster score
	}
	
	void addMoney(Enemy *enemy, int money)
	{
		Vec2 pos = enemy->convertToWorldSpaceAR(Vec2::ZERO);
		// Add Score
		GameWorld::instance()->addCoinWithAnimation(money, pos);
	}
}

const int kMaxID = 13;
const std::string kAlertCsbname = "ui_alert.csb";

static Dir sNextDir = DirLeft;

// Map Setting
//CC_SYNTHESIZE(float, mObjectScaleX, ObjectScaleX);
//CC_SYNTHESIZE(float, mObjectScaleY, ObjectScaleY);
//CC_SYNTHESIZE(bool, mObjectFlipX, ObjectFlipX);
//CC_SYNTHESIZE(bool, mObjectFlipY, ObjectFlipY);


Enemy::Enemy()
: mBehaviour(nullptr)
, mObjectID(0)
, mIsShowingAlert(false)
, mIsAnimationSetup(false)
, mObjectScaleX(1.0)
, mObjectScaleY(1.0)
, mObjectFlipX(false)
, mObjectFlipY(false)
, mIsSetup(false)
, mEffectToAdd(EffectStatusNone)
{
	
}

Enemy::~Enemy()
{
 	//log("Enemy Clean up");
    GameSound::stopEnemySound(getEnemyData()->getType(), mObjectID);
    
}

bool Enemy::init()
{
	bool isOk = MovableModel::init();	// calling the init of parent class
	if (isOk == false)
	{
		return false;
	}
	
	// Setting the appear
	
	this->setScale(1.0);
	this->setSpeed(0);
	
	// Initial setup
	mDefaultDir = DirDown;
	mDir = mDefaultDir;
	mNextDir = DirNone;
	mIsSetup = false;

	// internal state
	mSoundPlayed = false;
	setHoriLine(NULL);
	mIsVisible = false;
	mMoving = false;
	mState = State::NotShown;
	setVisible(false);
	
	return true;
}

bool Enemy::getVisibility()
{
    return mIsVisible;
}

void Enemy::updateVisibility()
{
//	if(mIsVisible) {
//		return; // no need to check anymore
//	}
    
	bool newVisibility = GameWorld::instance()->isModelVisible(this);
	
	if(mIsVisible == false && newVisibility == true) {
		this->modelWillVisible();
        std::string sfxKey = kEnemySfxKeyPrefix + StringUtils::format(kEnemyAppearSfxKeySuffix.c_str(),getEnemyData()->getType(),mObjectID);
        GameSound::playEnemyAppearSound(getEnemyData()->getType(), sfxKey);
	}
    
    if(mIsVisible == true && newVisibility == false) {
        GameSound::stopEnemySound(getEnemyData()->getType(), mObjectID);
    }
	
	mIsVisible = newVisibility;
}

void Enemy::playSoundWithAction(GameModel::Action action)
{
    bool isVisible = GameWorld::instance()->isModelVisible(this);
    if(!isVisible){
        return;
    }
    
    switch (action) {
        case GameModel::Attack:{
            std::string key = kEnemySfxKeyPrefix + StringUtils::format(kEnemyAttackSfxKeySuffix.c_str(),getEnemyData()->getType(),mObjectID);
            GameSound::playEnemyAttackSound(getEnemyData()->getType(),key);
            break;
        }
        
        case GameModel::Idle:{
            std::string key = kEnemySfxKeyPrefix + StringUtils::format(kEnemyIdleSfxKeySuffix.c_str(),getEnemyData()->getType(),mObjectID);
            GameSound::playEnemyIdleSound(getEnemyData()->getType(),key);
            break;
        }
        
        default:
        break;
    }
}

void Enemy::modelWillBeAdded()
{
	if(mBehaviour) {
		mBehaviour->onAddToWorld();
	}
}

void Enemy::modelWillVisible()
{
//	if(! mSoundPlayed) {
//		GameSound::playEnemyAppearSound(mResID);
//	}

//    Map<int,Sprite*> cautionMap = GameWorld::instance()->getEffectLayer()->getCautionMap();
//    
//    
//    
//    Map<int,Sprite*>::iterator iter = cautionMap.find(mObjectID);
//    Sprite* cautionSprite = (Sprite*)iter->second;
//    cautionSprite->removeFromParent();
//    cautionMap.erase(mObjectID);
	if(mBehaviour) {
//        if(mBehaviour->getAlertType() == AlertType::AlertOnScreen && mState>=State::Active){
//            GameWorld::instance()->getEffectLayer()->eraseCautionById(mObjectID);
//        }
		mBehaviour->onVisible();
//        log("enemy id:%d will visible",mBehaviour->getData()->getType());
	}
}

void Enemy::checkStartMoving()
{
	if(mMoving) {
		return;
	}
	float topY = GameWorld::instance()->getTopMapY();
	float myY = this->getPositionY();
	
	if(topY - myY > 50) {
		mMoving = true;
	}
}

 void Enemy::updateSpeed(float delta)
 {
    EffectStatus status = getEffectStatus();
     
    if(status == EffectStatusTimestop){
         return;
    }
     
	Vec2 accel = mBehaviour != nullptr ? mBehaviour->getAcceler() : Vec2::ZERO;

     mSpeedX += delta * accel.x;
     mSpeedY += delta * accel.y;
     
 }

bool Enemy::shouldSetupModel()
{
#if IS_ANDROID
	return willBecomeVisible();
#else
	return true;		// setup immediate in iOS, seem lazy loading not good here
#endif
	
}


bool Enemy::isStaticModel()
{
	return mBehaviour->isStatic();
}

void Enemy::update(float delta)
{
	// Setup the Animation and Model
	if(mIsSetup == false) {
		if(shouldSetupModel()) {
			setupModel();
			
		}
	}
	
	if(isStaticModel()) {
		// Just check inactive case
		if(mBehaviour && mBehaviour->shouldInActive()) {
			changeToInActive();
		}
		return;	// Nothing to do
	}
	
	// note: when the model will be visible, it will be setup
	if(mIsSetup == false) {
		return;
	}
	
	if(mState == State::InActive ||
       mState == State::Destroying) {
		return;	// Do do nothing!!!
	}
	
	GameModel::update(delta);		// animation update
	
	updateVisibility();				// may cause modelWillVisible, main play sound
	
	// Case when be hit
	if(mState == State::BeHit) {
		updateForBeHit(delta);
		
		return;
	}
	
	
	
	// refactor --> updateBehaviour
	if(mState == State::Idle || mState == State::NotShown) {
		if(mBehaviour && mBehaviour->shouldActive()) {
			changeToActive();
		}
		
		if(mState == State::NotShown) {
			return;		//
		}
	}
	
	
	
	// Inactive && NotShown will not flow to update check!
	
	if(mBehaviour) {
		mBehaviour->onUpdate(delta);
	}
	
	if(mState == State::Active) {
        updateSpeed(delta);
		if(mBehaviour && mBehaviour->shouldInActive()) {
			changeToInActive();
		}
	}
	
//	checkStartMoving();
//	
//	if(mMoving) {
//		move(delta);
//	}
}


std::string Enemy::getCsbFilename(int resID)
{
	return StringUtils::format("enemy/monster_%03d.csb", resID);
}

void Enemy::setResource(int resID)
{
	mResID = resID;

	// Set up the appearance
	std::string filename = getCsbFilename(mResID);
	//std::string filename = StringUtils::format("enemy/monster_%03d.csb", 1);
	if(FileUtils::getInstance()->isFileExist(filename) == false) {
		filename = "enemy/monster_001.csb";
	}
	
	setup(filename);
	setAction(GameModel::Action::Idle);		// Idle animation
	
	
	//
	
	
	// Update the global Zorder to improve the drawcall
	//updateChildrenZOrder();
}


bool Enemy::shouldPlaySoundEffect()
{

    if(mResID == 001 || mResID == 002 || mResID == 003 || mResID ==005)
    {
        return true;
    }


    return false;
}

#pragma mark - State Logic
void Enemy::changeState(State state)
{
	if(mState == state) {
		return;
	}
	
	switch (state) {
		case State::Active:		changeToActive();	break;
		case State::Idle:		changeToIdle();		break;
		case State::InActive:	changeToInActive(); break;
		case State::BeHit:		changeToBeHit();	break;
        case State::ChangeToMoney: changeToChangeToMoney(); break;
		default: {
			mState = state;
			break;
		}
	}
}

void Enemy::changeToIdle()
{
	setState(State::Idle);
	
	this->setVisible(true);
	
	setAction(GameModel::Action::Idle);
}

void Enemy::changeToBeHit()
{
	setState(State::BeHit);
	
	if(getBehaviour() != nullptr) {
		getBehaviour()->onHit(getPosition());
	}
	
	
	setAction(GameModel::Action::Idle);
	

}

void Enemy::changeToChangeToMoney()
{
    setState(State::ChangeToMoney);
    
    std::string csbName = "cashout1.csb";
    
    GameSound::playSound(GameSound::CashOutActive);

    addCsbAnimeByName(csbName,true,0.5,Vec2(0,0));
    playCsbAnime(csbName, "active", false, true, nullptr);
	
	int rewardMoney = GameWorld::instance()->getKillReward(false);
    setCsbAnimeFrameEventCallback(csbName, "active",
						[=](const std::string &eventName, EventFrame* event){
        if(eventName == "explode"){
			
			// log("DEBUG: monster is turing to money: %d", getObjectID());
			
			// Add Coin
			addMoney(this, rewardMoney);
			addScore(this);
			
			
			changeToInActive();		// Cannot remove the enemy directly!!!
            GameSound::playSound(GameSound::CashOutHit);
        }
    });

/* 
    setOnFrameEventCallback([=](const std::string &eventName, EventFrame* event){
        if(eventName == "explode"){
            Vec2 pos = getPosition();
            GameWorld::instance()->getEffectLayer()->showCollectStarEffect(1, pos);
            GameWorld::instance()->getModelLayer()->removeEnemy(this);
        }
    });
*/
    
}

void Enemy::showKnockoutEffect(const Vec2 &force, bool addRandom)
{
	float randX = addRandom ? aurora::RandomHelper::randomBetween(-20, 20) : 0;
	float randY = addRandom ? aurora::RandomHelper::randomBetween(0, 20) : 0;
	
	mSpeedX = force.x * 1.5f + randX;
	mSpeedY = force.y * 2 + randY;
	if(mSpeedY < 200) {
		mSpeedY = aurora::RandomHelper::randomBetween(200, 230);
	}
	
	setOpacity(100);
	
//	GameSound::playSound(GameSound::Burning);
//	GameSound::playSound(GameSound::Explosion);
	
	// Rotation effect
	RotateBy *rotate = RotateBy::create(1, 360);
	RepeatForever *repeat = RepeatForever::create(rotate);
	
	runAction(repeat);
}

AlertType Enemy::getAlertType()
{
	return (mBehaviour) ? mBehaviour->getAlertType() : AlertNone;
}

void Enemy::startActive()
{
	setState(State::Active);
	
	this->setVisible(true);
	
	GameSound::playEnemyActiveSound(getEnemyData()->getType(),kEnemySfxKeyPrefix + StringUtils::format(kEnemyActiveSfxKeySuffix.c_str(),getResID(),mObjectID));
	
	updateActionByDir();
	
	if(mBehaviour) {
//        showOnActiveAlert(mBehaviour->getAlertType());
//        log("enemy %d change to Active",mBehaviour->getData()->getType());
        

		// Tell the behaviour the model become active
		mBehaviour->onActive();
	}
}

void Enemy::changeToActive()
{
	AlertType alert = getAlertType();
    if(mIsShowingAlert){
        return;
    }
 //   log("enemy id:%d will active, AlertType:%d",mBehaviour->getData()->getType(),alert);
	if(alert == AlertOnModel) {		// active until the wakup animation done
		showWakeupAlert([=]() {
			startActive();
			mIsShowingAlert = false;
		});
	} else if(alert == AlertOnScreen && !mIsVisible){
			float x = getPositionX();
			bool isTop = mBehaviour->getEnemy()->getSpeedY() > 0 ? false : true;
            mIsShowingAlert = true;
            GameWorld::instance()->getEffectLayer()->showEnemyAlert(x, isTop, mObjectID,[&](){
                GameWorld::instance()->getEffectLayer()->eraseCautionById(mObjectID);
                startActive();
                mIsShowingAlert = false;
            });
    }else {
        startActive();
    }
	
	// moved to startActive
//	
//	setState(State::Active);
//	
//	this->setVisible(true);
//	
//	updateActionByDir();
//	
//	if(mBehaviour) {
//		//
//		showOnActiveAlert(mBehaviour->getAlertType());
//		
//		
//		// Tell the behaviour the model become active
//		mBehaviour->onActive();
//	}
}

void Enemy::changeToInActive()
{
	setState(State::InActive);
	
	this->setVisible(false);
	
	if(mBehaviour) {
		mBehaviour->onInActive();
	}
}


std::string Enemy::toString()
{
	std::string result = "";
	
	result += MovableModel::toString();
	result += " status=" + INT_TO_STR(getEffectStatus());
	result += " objID=" + INT_TO_STR(getObjectID());
	result += " resID=" + INT_TO_STR(mResID);
	result += " state=" + INT_TO_STR(mState);
	result += " tag=" + EnemyData::tagToString(getObjectTag());
	if(mBehaviour) {
		result += " behaviour=[" + mBehaviour->toString() + "]";
	} else {
		result += " behaviour=null";
	}
	if(getHoriLine()) {
		result += " atLine=" + getHoriLine()->toString();
	} else {
		result += " notAtLine";
	}

	//CC_SYNTHESIZE(int, mResID, ResID);
	
	return result;
}

Dir Enemy::getDefaultXDir()
{
	if(mSpeedX > 0) {
		return DirRight;
	} else if(mSpeedX < 0) {
		return DirLeft;
	} else {
		//log("position=%s", POINT_TO_STR(getPosition()).c_str());
		sNextDir = sNextDir == DirLeft ? DirRight : DirLeft;	// toggle
		
		return sNextDir;
	}
}

void Enemy::setupAttribute()
{
	//bool faceRight=true;
	if(mBehaviour) {
		Vec2 speedVec = mBehaviour->getSpeed();
		setSpeedX(speedVec.x);
		setSpeedY(speedVec.y);
	} else {
		log("DEBUG:Enemy.setupAttribute: ERROR!!! model-%d speedX=%f speedY=%f",
							getObjectID(), mSpeedX, mSpeedY);
	}
	
	
	// Set Default Dir and Dir
	Dir defaultYDir = mSpeedY > 0 ? DirUp : DirDown;
	Dir defaultXDir = getDefaultXDir();
	
	// Starting Dir
	mDir = getHoriLine() != nullptr ? defaultXDir : defaultYDir;
	mDefaultDir = defaultYDir;		// always use Y dir
	mNextDir = DirNone;				// always None
	
	
	
}


void Enemy::setupAnimation()
{
	setResource(getResID());
	
	
	if(mEffectToAdd != EffectStatusNone) {
		setEffectStatus(mEffectToAdd);
	}
	
	mIsAnimationSetup = true;
}

void Enemy::setupDirAndHitbox()
{
	updateSetDefaultDir();
	if(getResID() >= 100) {
		setupScaleAndFlip(getObjectScaleX(), getObjectScaleY(),
						  getObjectFlipX(), getObjectFlipY());
	} else {
		setFace(getObjectFlipX() == false);
	}
	setupHitboxes();		// is it not good??
}

void Enemy::setupModel()
{
	if(mIsAnimationSetup == false) {
		TSTART("Enemy.setupAnimation");
		setupAnimation();
		TSTOP("Enemy.setupAnimation");
		
		setupAttribute();
		
		setupDirAndHitbox();
	}
	
	
	if(mBehaviour) {
		mBehaviour->onCreate();		// The behaviour maybe change to Idle or Active or still NotShown
	} else {
		changeToActive();	//  Directly change to Active if there is no behaviour
	}
	mIsSetup = true;
}

void Enemy::resetState()
{
	mState = State::NotShown;
}


bool Enemy::isCollidable()
{
	return mState == State::Idle || mState == State::Active;
}

bool Enemy::canCollideItem()
{
	if(isCollidable() == false) {
		return false;
	}
	if(isObstacle()) {
		return false;
	}
	
	if(mBehaviour == nullptr) {
		return false;
	}
	
	return mBehaviour->canCollideItem();
}


void Enemy::beHit()
{
	
	changeState(State::BeHit);
	
	addScore(this);
	
	// Ability Effect
	// Ability for Reward Money
	int reward = GameWorld::instance()->getKillReward(false);
	if(reward > 0) {
		addMoney(this, reward);
	}
	
	//GameWorld::instance()->add
	
//	if(isWolf()) {
//		GameWorld::instance()->addKill();
//	}
}


bool Enemy::isActive()
{
	return mState == State::Idle || mState == State::Active;
}

bool Enemy::isInActive()
{
    return mState == State::InActive;
}

bool Enemy::isObstacle()
{
	int resID = getResID();
	if(resID < 100 || resID == kChaserEnemyID) {
		return false;
	}
	
	return getEnemyData()->isTrap();
}

void Enemy::updateForBeHit(float delta)
{
	moveFree(delta);
	
//	if(out-bound) {
//		changeState(InActive)
//	}
}

Vec2 Enemy::getFinalSpeed() const
{
	Vec2 originSpeed = MovableModel::getFinalSpeed();
	
	EffectStatus status = getEffectStatus();
	switch(status) {
		case EffectStatusNone				: return originSpeed;
		case EffectStatusTimestop			: return Vec2::ZERO;
		case EffectStatusSlow				: return originSpeed * mSlowFactor;
		default								: return originSpeed;
	}
}

void Enemy::showWakeupAlert(const std::function<void()> &callback)
{
	if(mIsShowingAlert) {
		return;
	}
	
	mIsShowingAlert = true;
	addCsbAnimeByName(kAlertCsbname, false, 2.0f, Vec2(0, 10));	// note: onMainSprite=true cause bug!!!
	playCsbAnime(kAlertCsbname, "active", false, true, callback);			// false: not loop  true: removeSelf
}

//void Enemy::showOnActiveAlert(AlertType alertType)
//{
//    if(alertType == AlertType::AlertNone){
//        return;
//    }else if(alertType == AlertType::AlertOnModel){
//        showWakeupAlert();
//    }else if(alertType == AlertType::AlertOnScreen && !mIsVisible){
//        GameWorld::instance()->getEffectLayer()->showEnemyAlert(getPositionX(),
//                                                                mBehaviour->getEnemy()->getSpeedY()>0?false:true,
//                                                                mObjectID);
//    }else{
//        log("cannot get correct alert type!");
//    }
//}


void Enemy::updateSetDefaultDir()
{
	if(mSpeedX != 0) {
		return;		// No need to set defaultDir
	}

	if(getEnemyData()->isTrap() == false) {
		bool atRight = getPositionX() > VisibleRect::center().x;
		setFace(atRight == false);
	}
	

	
}

bool Enemy::isWolf()		// Should change to enemy.json
{
	if(mResID >= 2 || mResID <= 7) {
		return true;
	}
	
	if(mResID >= 9 || mResID <= 9) {
		return true;
	}
	
	if(mResID >= 12 || mResID <= 12) {
		return true;
	}
	
	return false;
}


void Enemy::updateChildrenZOrder()
{
	if(mRootNode == nullptr) {
		return;
	}
	
	int zOrder = isObstacle() ? kZorderObstacle : kZorderEnemy;
	
	setLocalZOrder(zOrder);
	
//	Vector<Node *> children = mRootNode->getChildren();
//	for(Node *node : children) {
//		if(dynamic_cast<Sprite *>(node)) {
//			node->setGlobalZOrder(zOrder);
//		}
//		
//	}
}


const EnemyData *Enemy::getEnemyData()
{
    return mBehaviour->getData();
}


bool Enemy::collideWithPlayer(Player *player)	// assume enemy can collide with player
{
	if(mBehaviour == nullptr) {
		log("ERROR: collideWithPlayer: mBeaviour=null");
		return player->handleBeingHitted();
	}
	
	return mBehaviour->onCollidePlayer(player);
}

bool Enemy::collideWithItem(Item *item)
{
	if(mBehaviour == nullptr) {
		return false;
	}
	
	return mBehaviour->onCollideItem(item);
}

void Enemy::setEffectStatus(EffectStatus status, float duration)
{
    if(isObstacle()){
        return;
    }
    
    mEffectStatus = status;
    
    std::string iconName = "";
    
    float timeSpeed = 1;
    
    bool anyEffect = true;
    
    float scale = 0.5f;
    bool isFront;
    
    switch (status) {
        case EffectStatusSlow :
        {
            iconName = "status_slow.png";
            timeSpeed = 0.5f;
            break;
        }
            
        case EffectStatusTimestop :
        {
            //			name = "status_timestop.png";
            timeSpeed = 0;
            scale = 0.7;
            isFront = false;
            break;
        }
            
        case EffectStatusInvulnerable:
        {
            scale = 0.5;
            isFront = true;
            break;
        }
            
        default : {
            anyEffect = false;
            break;
        }
    }
    
    if(anyEffect == false) {
        return;
    }
    
    // Handle Effect Duration
    if(duration < 0) {
        mHasEffectStatusDuration = false;
    } else {
        mHasEffectStatusDuration = true;
        mEffectStatusRemainTime = duration;
    }
    
    setTimeSpeedByPercent(timeSpeed);
    
    if(iconName.length() > 0) {
        setStatusIconByName(iconName);
    }
    
    setStatusAnimationByEffect(status,scale,isFront);
    onApplyEffectStatus(status);
}

void Enemy::onApplyEffectStatus(EffectStatus status)
{
    getBehaviour()->onApplyEffectStatus(status);
}

void Enemy::onRemoveEffectStatus(EffectStatus status)
{
    getBehaviour()->onRemoveEffectStatus(status);
}

void Enemy::onFrameEvent(cocostudio::timeline::Frame *frame)
{
	//log("Enemy::onFrameEvent: called");
    GameModel::onFrameEvent(frame);
    if(frame == nullptr) {
        log("GameModel::onFrameEvent: fame is null");
        return;
    }
    
    EventFrame* event = dynamic_cast<EventFrame*>(frame);
    if(!event) {
        log("GameModel::onFrameEvent: event is null");
        return;
    }
    
    
    //handle SFX
    std::string eventKey = event->getEvent();
    onSFXFrameEvent(eventKey);

}

void Enemy::onSFXFrameEvent(std::string eventKey)
{
    const EnemyData* data = getEnemyData();
    std::string soundKey = kEnemySfxKeyPrefix + StringUtils::format(kEnemyAnimeSfxKeySuffix.c_str(),data->getType(),mObjectID,eventKey.c_str());
    
    if(eventKey.find("stop")!=std::string::npos){
        eventKey = eventKey.substr(4);
        soundKey = kEnemySfxKeyPrefix + StringUtils::format(kEnemyAnimeSfxKeySuffix.c_str(),data->getType(),mObjectID,eventKey.c_str());
        // log("key=%s",soundKey.c_str());
        GameSound::stopSound(soundKey);
        return;
    }
    
    bool isVisible = GameWorld::instance()->isModelVisible(this);
    if(!isVisible){
        return;
    }
    
    
    std::vector<std::string> valueList = data->getSfx(eventKey);
    if(valueList.empty()){
        return;
    }
    
    std::string sfxName = valueList[0];
    bool isLoop = valueList[1] == "true" ? true : false;
    
    // log("key=%s",soundKey.c_str());
    GameSound::playSoundFile(sfxName,isLoop,1.0,soundKey);
 
}


std::string Enemy::getName()
{
	return StringUtils::format("enemy_%d", getEnemyData()->getType());
}


ObjectTag Enemy::getObjectTag()
{
	const EnemyData* data = getEnemyData();
	return data == nullptr ? ObjectUnknown : data->getTag();
}


bool Enemy::hasSetup()
{
	return mIsSetup;
}


#pragma mark - Collision
bool Enemy::isCollideWithPlayerRect(const Rect &mapHitbox, const Rect &worldHitbox)	// this is worldSpace position
{
	TSTART("isCollideWithPlayerRect");
	
	if(getPositionY() > (mapHitbox.getMaxY() + kCheckThreshold)) {
		TSTOP("isCollideWithPlayerRect");
		return false;	// the object not yet reach the player
	}
	
	
	
	// My Hitbox
	TSTART("CalcHitbox");
	std::vector<Rect> myHitboxes;
	std::vector<Circle> myHitCircles;
	
	getHitboxRect(myHitboxes);
	getHitCircle(myHitCircles);
	TSTOP("CalcHitbox");
	
	TSTART("checkCollision");
	
	if(aurora::GeometryHelper::anyCollision(myHitboxes, worldHitbox)) {
		//log("collision:Rect2Rect:me=%s other=%s", getName().c_str(), model->getName().c_str());
		TSTOP("checkCollision");
		TSTOP("isCollideWithPlayerRect");
		return true;
	}
	
	if(aurora::GeometryHelper::anyCollision(myHitCircles, worldHitbox)) {
		//log("collision:Rect2Rect:me=%s other=%s", getName().c_str(), model->getName().c_str());
		TSTOP("checkCollision");
		TSTOP("isCollideWithPlayerRect");
		return true;
	}
	
	TSTOP("checkCollision");
	TSTOP("isCollideWithPlayerRect");
	return false;
	
//
//	if(aurora::GeometryHelper::anyCollision(myHitboxes, otherHitCircles)) {
//		//log("collision:Rect2Circle:me=%s other=%s", getName().c_str(), model->getName().c_str());
//		TSTOP("checkCollision");
//		return true;
//	}
//	
//	if(aurora::GeometryHelper::anyCollision(otherHitboxes, myHitCircles)) {
//		//log("collision:Circle2Rect:me=%s other=%s", getName().c_str(), model->getName().c_str());
//		TSTOP("checkCollision");
//		return true;
//	}
//	
//	if(aurora::GeometryHelper::anyCollision(myHitCircles, otherHitCircles)) {
//		//log("collision:Circle2Circle:me=%s other=%s", getName().c_str(), model->getName().c_str());
//		TSTOP("checkCollision");
//		return true;
//	}
//	
	
}
