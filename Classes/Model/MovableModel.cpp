//
//  MovableModel.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#include "MovableModel.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "MapLine.h"
#include "GeometryHelper.h"
#include "StringHelper.h"

#define kParticleScale 0.3

#pragma mark - Local Helper function
namespace {
}

#pragma mark - Class Implementation

MovableModel::MovableModel()
: GameModel()
, mSlowFactor(0.5f)
, mHasEffectStatusDuration(false)
, mEffectStatusRemainTime(0)
, mEffectStatus(EffectStatusNone)
{
	
}

bool MovableModel::init()
{
	bool isOk = GameModel::init();
	if (isOk == false)
	{
		log("MovableModel: fail to create");
		return false;
	}

	//log("MovableModel: success to create");
	// this->setTexture("smile.png");
	
	mSpeedX = 100;
	mSpeedY = 100;
	
	mDefaultDir = DirNone;
	
	// Initial setup
	mDir = DirUp;
	mNextDir = DirNone;
	mHoriLine = nullptr;
	
	return true;
}

void MovableModel::reset()
{
	mDir = mDefaultDir;
	mNextDir = DirNone;
	setHoriLine(nullptr);
	
	updateActionByDir();
}

void MovableModel::updateActionByDir()
{
	if(mDir == DirLeft || mDir == DirRight) {
		setAction(Action::Walk);
		
		setFace(mDir == DirLeft);
	} else if(mDir == DirUp || mDir == DirDown){
		setAction(Action::Climb);
	}
	
}

void MovableModel::update(float delta)
{
	move(delta);
}


void MovableModel::handleEffectStatus(float delta)
{	// some status will auto remove
	if(mHasEffectStatusDuration == false) {
		return;	// nothing to be done
	}
	mEffectStatusRemainTime -= delta;
	if(mEffectStatusRemainTime > 0) {
		return;
	}
	
	// Remove the effect
	mEffectStatusRemainTime = 0;
	mHasEffectStatusDuration = false;
	
	removeEffectStatus();
}

void MovableModel::move(float delta)
{
	Vec2 newPos = calculateMovement(delta);
	
	setPosition(newPos);
}

void MovableModel::setSpeed(float speed)
{
	setSpeedX(speed);
	setSpeedY(speed);
}

void MovableModel::resetToUpwardDir()		// For NPC Delivery
{
	mDir = DirUp;
	mNextDir = DirNone;

	MapLine *horiLine = getHoriLine();
	if(horiLine != nullptr) {
		GameWorld::instance()->getMap()->removeLineFromMap(horiLine->getLineObjectID());
		setHoriLine(nullptr);
	}
}

Vec2 MovableModel::calculateMovement(float delta)
{
	
	Vec2 curPos = this->getPosition();
	
	// Case 'Detected direction change in last move'
	if(mNextDir != DirNone) {
		// Reset Direction
		mDir = mNextDir;
		mNextDir = DirNone;
		
		updateActionByDir();
	}
	
	Vec2 newPos = getNewPosition(delta);
	
	
	// Current state: Up & Down
	if(mDir == DirUp || mDir == DirDown)
    {
		bool matchStart;
        const MapLine *mapLine;

		Vec2 startPos = curPos;		//+ (Vec2(0, 0.0001f) * (mDir == DirUp ? 1 : -1)) ;
		mapLine = GameWorld::instance()->getMap()->willCrossAnyHoriLine(startPos, newPos, matchStart);
		
		// log("DEBUG: UP/DOWN: start=%f,%f new=%f,%f cross=%d", startPos.x, startPos.y, newPos.x, newPos.y, (mapLine != nullptr));
		
		// Case 'No reach any cross line'
		if(mapLine == nullptr) {		// not crossing the horizontal line
			return newPos;
		} else {
			// Case 'Reach a cross line'
//			setHoriLine(MapLine::create(mapLine));
            setHoriLine(MapLine::cloneMapLine(mapLine));
			Vec2 startPoint = mapLine->getStartPoint();
			Vec2 endPoint = mapLine->getEndPoint();
			
			newPos = matchStart ? startPoint : endPoint;
			
			mNextDir = matchStart ? DirRight : DirLeft;
            
            //Gavin - Modify to support Slope Line Movement
            if(mapLine->isSlopeLine())
            {
                Vec2 destPos = (matchStart)? endPoint:startPoint;
                mNextDir = (destPos.x <= curPos.x)? DirLeft : DirRight;
				
				
				// Rotation the CSB effect
				float rotation = aurora::GeometryHelper::findCocosDegree(newPos, destPos);
				//log("DEBUG***: start=%s end=%s rotation=%f",
				//	POINT_TO_STR(newPos).c_str(),
				//	POINT_TO_STR(destPos).c_str(), rotation);
				
				setCsbAnimeRotation(rotation);

				onEnterSlopeLine();
            }
			
		}
	}
    else if(mDir == DirLeft || mDir == DirRight)
    {
		MapLine *horiLine = getHoriLine();
		if(horiLine != nullptr) {
			
			Vec2 endPoint = mDir == DirRight ?
								horiLine->getEndPoint()
								: horiLine->getStartPoint();
            
            
            Vec2 startPos = curPos; // + (Vec2(0.000001f, 0) * (mDir == DirRight ? 1 : -1)) ;
			bool reachEnd = aurora::GeometryHelper::isLineIntersect(startPos, newPos, endPoint, true, false);
			
            
            
            
            //Gavin - Modify to support Slope Line Movement
            if(horiLine->isSlopeLine())
            {
                bool movingUp = horiLine->isMovingUpwardAlongSlopeLine(mDir);
                endPoint =  horiLine->getSlopeDestinationPoint(mDir);
                if(!movingUp && (mDir == DirRight) &&  curPos.y <= endPoint.y){reachEnd = true;}
                else if( movingUp && (mDir == DirRight) && curPos.y >= endPoint.y){reachEnd = true;}
                else if( movingUp  && (mDir == DirLeft) && curPos.y >= endPoint.y){reachEnd = true;}
                else if( !movingUp  && (mDir == DirLeft)  && curPos.y <= endPoint.y){reachEnd = true;}
                if(reachEnd == true)
                {
                    GameWorld::instance()->getMap()->removeLineFromMap(horiLine->getLineObjectID());
                    setHoriLine(nullptr);
					setCsbAnimeRotation(0);
					
					onExitSlopeLine();
                }

            }

            
            
			//log("DEBUG: LEFT/RIGHT: start=%f,%f new=%f,%f cross=%d", startPos.x, startPos.y, newPos.x, newPos.y, (mapLine != nullptr));
			// Case 'Pass directly'
			if(reachEnd == false) {
				return newPos;
			}
			
            
            
			// Refine the newPos
			newPos = endPoint;
			mNextDir = mDefaultDir;
			
			return newPos;
		} else {
			// note: something wrong here!!!
			log("No horiLine found!!");
		}
	}
	
	return newPos;
}


Vec2 MovableModel::getNewPosition(float delta) const
{
	Vec2 speedVec = getFinalSpeed();	// it's possible to have -ve value
	
	// Make Y absolue
	speedVec.y = fabsf(speedVec.y);
	speedVec.x = fabsf(speedVec.x);
	
	
    //
    MapLine *horiLine = getHoriLine();
    bool isWalkingOnSlopeLine = false;
    float slope = 1.0f;

	
	if(horiLine != nullptr && horiLine->isSlopeLine())
    {
        isWalkingOnSlopeLine = true;
        slope = horiLine->getLineSlope(mDir);//log("horiLine %s", horiLine->toString().c_str());log("slope: %f", slope);
    }
    
    
	Vec2 pos = this->getPosition();
	
    
    
	float speed = (mDir == DirUp || mDir == DirDown) ? speedVec.y : speedVec.x;
    if(speed == 0) {return pos;}
    speed = fabs(speed);
    
    
    if(isWalkingOnSlopeLine)
    {
        pos = aurora::GeometryHelper::calculateNewTracePosition(pos,
											horiLine->getSlopeDestinationPoint(mDir),
											speed, delta);
        return pos;
    }

    


	float diff = speed * delta;

	switch (mDir)
    {
		case DirUp:
            pos.y += diff;
            break;
		case DirDown:
            pos.y -= diff;
            break;
		case DirLeft:
            if(isWalkingOnSlopeLine)
            {
                pos.y += (slope * diff);
            }
            pos.x -= diff;
            break;
		case DirRight:
            if(isWalkingOnSlopeLine)
            {
                pos.y += (slope * diff);
            }
            pos.x += diff;
            break;
		default:
            break;
	}
	
	return pos;
}


std::string MovableModel::toString()
{
	std::string result = "";
	
	
	
	Vec2 pos = getPosition();
	result += StringUtils::format("pos:(%.2f,%.2f)", pos.x, pos.y);
	
	Vec2 speedVec = getFinalSpeed();
	result += StringUtils::format(" speed:(%.2f,%.2f)", speedVec.x, speedVec.y);
	
	result += StringUtils::format(" speedX=%.2f speedY=%.2f", mSpeedX, mSpeedY);
	
	result += StringUtils::format(" defaultDir:%d", mDefaultDir);
	
	result += StringUtils::format(" dir:%d", mDir);
	
	result += StringUtils::format(" nextDir:%d", mNextDir);
	
	return result;
}

bool MovableModel::isCollide(Node *otherObject)
{
	Rect myRect = this->getBoundingBox();
	
	Rect otherRect = otherObject->getBoundingBox();
	
	
	return myRect.intersectsRect(otherRect);
}



void MovableModel::moveFree(float delta)
{
	
	Vec2 diff = getFinalSpeed() * delta;
	//log("debug: moveFree. diff=%f,%f speed=%f,%f", diff.x, diff.y, mSpeedX, mSpeedY);
	Vec2 newPos = getPosition() + diff;
	
	setPosition(newPos);
}

Vec2 MovableModel::getFinalSpeed() const
{
	return Vec2(mSpeedX, mSpeedY);
}

////Override Node::getBoundingBox()
//Rect MovableModel::getBoundingBox() const
//{
//	Rect rect(0, 0, _contentSize.width*0.7, _contentSize.height*0.7);
//    return RectApplyAffineTransform(rect, getNodeToParentAffineTransform());
//}


Vec2 MovableModel::getCenterPosition() const
{
	//Rect rect = getBoundingBox();
	
	return getPosition();
}


// Effect Status
EffectStatus MovableModel::getEffectStatus() const
{
	return mEffectStatus;
}

void MovableModel::setEffectStatus(EffectStatus status, float duration)
{
	mEffectStatus = status;
	
	std::string iconName = "";
    
	float timeSpeed = 1;
	
	bool anyEffect = true;
	
	switch (status) {
		case EffectStatusSlow :
		{
            iconName = "status_slow.png";
			timeSpeed = 0.5f;
			break;
		}
		
		case EffectStatusTimestop :
		{
//			name = "status_timestop.png";
			timeSpeed = 0;
			break;
		}
			
		default : {
			anyEffect = false;
			break;
		}
	}
	
	if(anyEffect == false) {
		return;
	}
	
	// Handle Effect Duration
	if(duration < 0) {
		mHasEffectStatusDuration = false;
	} else {
		mHasEffectStatusDuration = true;
		mEffectStatusRemainTime = duration;
	}
	
	
	//
	
	setTimeSpeedByPercent(timeSpeed);
	
	if(iconName.length() > 0) {
		setStatusIconByName(iconName);
	}
    
    onApplyEffectStatus(status);
//    setStatusAnimationByEffect(status);
}

void MovableModel::removeEffectStatus()
{
    EffectStatus status = getEffectStatus();
    
	mEffectStatus = EffectStatusNone;
	
	//
	setTimeSpeedByPercent(1);
	
	//
	removeStatusIcon();
    removeStatusAnimation(status);
    
    onRemoveEffectStatus(status);
}

void MovableModel::setStatusAnimationByEffect(EffectStatus effect,float scale, bool isFront)
{
    std::string csbName = getAnimeCsbNameByEffect(effect);
    if(csbName == ""){
        return;
    }
    addCsbAnimeByName(csbName, isFront, scale, Vec2(0,0));
    playCsbAnime(csbName, "active", true);
}

void MovableModel::removeStatusAnimation(EffectStatus effect)
{
    std::string csbName = getAnimeCsbNameByEffect(effect);
    if(csbName == ""){
        return;
    }
    removeCsbAnimeByName(csbName);
}


std::string MovableModel::getAnimeCsbNameByEffect(EffectStatus effect){
    switch (effect) {
        case EffectStatusTimestop:
            return "particle_timestop_hit_1.csb";
            
        case EffectStatusInvulnerable:
            return "particle_invshield_2.csb";
            
        case EffectStatusSlow:
            return "particle_doublecoins";
            
        default:
            return "";
    }
}


void MovableModel::initDir(Dir dir)
{
	
}
