//
//  Coin.hpp
//  GhostLeg
//
//  Created by Ken Lee on 21/3/2016.
//
//

#ifndef Coin_hpp
#define Coin_hpp

#include <stdio.h>
#include "Collectable.h"
#include "CommonMacro.h"

class Coin : public Collectable
{
public:
    
#pragma mark - Public static Method
    static const std::string kSmallCoinCsbFile;
    static const std::string kBigCoinCsbFile;
    
#pragma mark - Public Method and Properties
    enum CoinType{
        SmallCoin,
        LargeCoin,
    };
    
    CREATE_FUNC_ARG(Coin, CoinType);
    
    Coin(CoinType type);
    
    virtual bool init();	// virtual because different derived class have different init logic
    virtual bool use();
    
    Item::Type parseCoinType(CoinType type);
    
private:
    bool canTriggerDoubleCoin();
    void showDoubleCoinEffect();
    CoinType mCoinType;
    
};




#endif /* Coin_hpp */
