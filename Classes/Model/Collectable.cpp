//
//  Collectable.cpp
//  GhostLeg
//
//  Created by Calvin on 12/7/2017.
//
//

#include "Collectable.h"
#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"
#include "RandomHelper.h"
#include "ViewHelper.h"
#include "ModelLayer.h"
#include "GeometryHelper.h"
#include "EffectLayer.h"
#include "GameplaySetting.h"
#include "TimeMeter.h"

const int kAcceleration = 10.0;


Collectable::Collectable(Type type)
: Item(type)
, mStartMoving(false)
, mMoveToPlayer(false)
, mMovingToFreeSpace(false)
, mVelocity(300)
{
    
}


bool Collectable::init()
{
    if(Item::init() == false) {
        log("Collectable: init failed");
        return false;
    }
    
    return true;
}

bool Collectable::use()
{
    return Item::use();
}

void Collectable::update(float delta)
{
    if(mHasSetup && mMoveToPlayer) {
        moveTowardPlayer(delta);
    } else {
        Item::update(delta);
        //if(mMovingToFreeSpace){
        /*
         Rect myRect = getBoundingBox();
         myRect.size = Size(10, 10);
         
         if(!GameWorld::instance()->isRectVisible(myRect)){
         GameWorld::instance()->getModelLayer()->removeItem(this);
         GameWorld::instance()->collectCoin(1);
         }'
         */
        // }
    }
    
}




#pragma mark - Move toward Player
void Collectable::moveTowardPlayer(float delta)
{
    Player *player = GameWorld::instance()->getPlayer();
    if(player == nullptr) {
        log("Star.moveTowardPlayer: player is null");
        return;
    }
    
    Vec2 playerPos = player->getCenterPosition();
    
    Vec2 newPos = aurora::GeometryHelper::calculateNewTracePosition(getPosition(),
                                                                    playerPos,
                                                                    mVelocity, delta);
    setPosition(newPos);
    
    mVelocity += kAcceleration;
    
}
