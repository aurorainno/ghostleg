//
//  EnergyPotion.cpp
//  GhostLeg
//
//  Created by Gavin Chiu on 22/3/16.
//
//

#include <stdio.h>

#include "EnergyPotion.h"
#include "GameWorld.h"
#include "Player.h"
#include "GameSound.h"
#include "InvulnerableEffect.h"
#include "ShieldEffect.h"
#include "TimeStopEffect.h"
#include "DoubleCoinsEffect.h"
#include "MissileEffect.h"

EnergyPotion::EnergyPotion(): Item(Item::Type::ItemPotion)
{
    
}


bool EnergyPotion::init()
{
    if(Item::init() == false) {
        log("EnergyPotion: init failed");
        return false;
    }
	
	setScale(0.9f);
	setup("item01.csb");
	
    return true;
}

bool EnergyPotion::use()
{
    if(Item::use() == false) {
        return false;
    }
    
    GameSound::playSound(GameSound::Potion);
    GameWorld::instance()->consumeEnergyPotion();
			
	//
	
    return true;	
}
