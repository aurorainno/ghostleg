//
//  ReduceEnergyBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 21/11/2016.
//
//

#include "ReduceEnergyBehaviour.h"
#include "GameWorld.h"
#include "Player.h"

ReduceEnergyBehaviour::ReduceEnergyBehaviour()
:NormalBehaviour()
{
    
}

void ReduceEnergyBehaviour::onHit()
{
    Player* player = GameWorld::instance()->getPlayer();
    if(player){
        player->deductPlayerEnergy(50);
    }
}

std::string ReduceEnergyBehaviour::toString()
{
    return "ReduceEnergyBehaviour";
}
