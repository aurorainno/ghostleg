//
//  EmojiAnimeNode.hpp
//  GhostLeg
//
//  Created by Ken Lee on 20/3/2017.
//
//

#ifndef EmojiAnimeNode_hpp
#define EmojiAnimeNode_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

#include "AnimeNode.h"

class EmojiAnimeNode : public AnimeNode
{
public:
	typedef std::function<void()> EmojiEndCallback;
	
public:
	CREATE_FUNC(EmojiAnimeNode);
	
	EmojiAnimeNode();
	
	bool init();

	void showWaiting();
	void closeEmoji();
	void showEmoji(int value);		// 1 - Less Happy  5 - most happy
	
	void setEmojiEndCallback(const EmojiEndCallback &callback);
	
	//void setEmoji(1);
	
//	void setStartAnime(const std::string &animeName,
//					   bool isLoopAnimation = false,
//					   bool autoRemove = true);
//	
//	void setPlayEndCallback(const PlayEndCallback &callback);
//	
//	void playAnimation(const std::string &name, bool isLoop, bool autoRemove=false);
	
	virtual void onEnter();
	
private:

	void setEmojiGraphics(int value);
	void runAnimation(const std::string &name);
	bool isLoopAnimation(const std::string &name);

private:
	Sprite *mEmojiSprite;
	EmojiEndCallback mEmojiEndCallback;
};

#endif /* EmojiAnimeNode_hpp */
