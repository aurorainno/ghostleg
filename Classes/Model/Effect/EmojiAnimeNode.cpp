//
//  EmojiAnimeNode.cpp
//  GhostLeg
//
//  Created by Ken Lee on 20/3/2017.
//
//

#include "EmojiAnimeNode.h"
#include "AnimeNode.h"

// Different animation
//		idle			: when waiting
//		disable			: closing down
//		able			: open up

EmojiAnimeNode::EmojiAnimeNode()
: AnimeNode()
, mEmojiSprite(nullptr)
, mEmojiEndCallback(nullptr)
{
	
}

bool EmojiAnimeNode::init()
{
	if(AnimeNode::init() == false) {
		return false;
	}
	
	setup("Emoji/emoji_dialog.csb");
	
	mEmojiSprite = mRootNode->getChildByName<Sprite *>("emoji");
	
	setStartAnime("");	// no start animation
	
	setVisible(false);
	
	setPlayEndCallback([&](Ref *sender, const std::string &name) {
		if(name == "able") {
			if(mEmojiEndCallback) {
				mEmojiEndCallback();
			}
		}
	});
	
	return true;
}

void EmojiAnimeNode::showWaiting()
{
	//mEmojiSprite->setVisible(false);
	setVisible(true);
	runAnimation("idle");
}

void EmojiAnimeNode::showEmoji(int value)
{
	setEmojiGraphics(value);
	setVisible(true);
	runAnimation("able");
}

//void setEmoji(1);

//	void setStartAnime(const std::string &animeName,
//					   bool isLoopAnimation = false,
//					   bool autoRemove = true);
//
//	void setPlayEndCallback(const PlayEndCallback &callback);
//
//	void playAnimation(const std::string &name, bool isLoop, bool autoRemove=false);

void EmojiAnimeNode::setEmojiGraphics(int value)
{
	// Tune the value
	if(value <= 0) { value = 1; }
	if(value >= 5) { value = 5; }
	
	// make the filename
	std::string filename = StringUtils::format("Emoji/imgame_emoji_%d.png", value);
	
	// Change the sprite
	mEmojiSprite->setTexture(filename);
}

void EmojiAnimeNode::onEnter()
{
	Node::onEnter();
}

bool EmojiAnimeNode::isLoopAnimation(const std::string &name)
{
	if(name == "idle") {
		return true;
	}
	
	return false;
}

void EmojiAnimeNode::runAnimation(const std::string &name)
{
	playAnimation(name, isLoopAnimation(name), false);
}


void EmojiAnimeNode::setEmojiEndCallback(const EmojiEndCallback &callback)
{
	mEmojiEndCallback = callback;
}


void EmojiAnimeNode::closeEmoji()
{
	runAnimation("disable");
}
