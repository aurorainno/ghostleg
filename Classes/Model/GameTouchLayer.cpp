//
//  GameTouchLayer.cpp
//  GhostLeg
//
//  Created by Gavin Chiu on 16/3/16.
//
//

#include "GameTouchLayer.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "ViewHelper.h"
#include "EffectLayer.h"
#include "GameSound.h"
#include "Player.h"
#include "StringHelper.h"

#define kGridSize 18	
#define kSpaceBetweenLine	(5 * 18)
#define kHalfSpaceBetweenLine	(5 * 18 / 2)

namespace {
	void fixPosition(Vec2 &pos)
	{
		if(pos.x < GameMap::getFirstLineX()) {
			pos.x = GameMap::getFirstLineX();
		}
		
		if(pos.x > GameMap::getLastLineX()) {
			pos.x = GameMap::getLastLineX();
		}
		
		// Fix
	}
	
	void adjustDropPosition(Vec2 &pos1, Vec2 &pos2) {
		// Adjust
		Vec2 playerPos = GameWorld::instance()->getPlayer()->getPosition();
		
		if(pos1.x == playerPos.x) {
			if(pos1.y < playerPos.y + 5) {
				pos1.y = playerPos.y + 5;
			}
		}
		
		if(pos2.x == playerPos.x) {
			if(pos2.y < playerPos.y + 5) {
				pos2.y = playerPos.y + 5;
			}
		}
	}
}


#pragma mark - Class Logic
GameTouchLayer::GameTouchLayer()
: mEnable(true)
, mType(Type::Slide)
, mIsStarted(false)
, mDrawLineCallback(nullptr)
, mMapOffsetY(0)
, mHasBraked(false)
, mTimeTouched(0)
, mIsTouching(false)
{

}

bool GameTouchLayer::init()
{
    Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	bool isOk = XTLayer::init();
 //LayerColor::initWithColor(Color4B(0, 0, 0, 0), size.width, size.height);
    if (isOk == false)
    {
        return false;
    }
    
	xtDoubleTapTime(0);			// No allow double tap
	xtLongTapTime(0);			// No allow long tap
	xtSwipeTime(500);
	
    // Line Layer
    mTouchLineLayer = DrawNode::create();
    mTouchLineLayer->setPosition(Vec2(0, 0));
    addChild(mTouchLineLayer);
    
    
    mSlopeLineStartPoint.setZero();

    
    mTouchPointSprite = Sprite::create("general_touch.png");
    mTouchPointSprite->setScale(0.4f);
    mTouchPointSprite->setOpacity(0);
    this->addChild(mTouchPointSprite, 0);
    
    
    
    mEndPointSprite = Sprite::create("general_touch.png");
    mEndPointSprite->setScale(0.4f);
    mEndPointSprite->setOpacity(0);
    this->addChild(mEndPointSprite, 0);
    

    
    // addGameMapTouchListener();
    
    return true;
}


void GameTouchLayer::addGameMapTouchListener()
{
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->onTouchBegan = CC_CALLBACK_2(GameTouchLayer::onTouchBegan, this);
    touchListener->onTouchEnded = CC_CALLBACK_2(GameTouchLayer::onTouchEnded, this);
    touchListener->onTouchMoved = CC_CALLBACK_2(GameTouchLayer::onTouchMoved, this);
    touchListener->onTouchCancelled = CC_CALLBACK_2(GameTouchLayer::onTouchCancelled, this);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
    
}

float GameTouchLayer::getOffsetY()
{
	return mMapOffsetY;
}

void GameTouchLayer::setOffsetY(float y){
    if(mSlopeLineStartPoint.y <= 0){
        clearTouchIndicator();
        mIsStarted = false;
    }
    
	mMapOffsetY = y;
	
	updateLineStartPosition();
	
//    Vec2 pos = getPosition();
//        
//    pos.y = -y;
//        
//    setPosition(pos);
}

void GameTouchLayer::reset()
{
	mIsStarted = false;
	resetData();
	hideTouchIndicator();
}


#pragma mark - Helper Function
void GameTouchLayer::showTouchIndicator(Vec2 touchedPoint)
{
	
	
    mTouchPointSprite->setPosition(touchedPoint.x, touchedPoint.y);
    mTouchPointSprite->setOpacity(255.0f);
}

void GameTouchLayer::showDrawingSlopeLine(Vec2 startPoint, Vec2 endPoint)
{
    
    mTouchLineLayer->clear();
    
    float diffX = endPoint.x - startPoint.x;
    float diffY = endPoint.y - startPoint.y;
    Vec2 targetPos;
    
    if(fabs(diffX) > kHalfSpaceBetweenLine) {
        targetPos.x = startPoint.x + diffX * 2;
        targetPos.y = startPoint.y + diffY * 2;
    } else {
        targetPos = endPoint;
    }

    
    Color4F lineColorRed = Color4F(0.941f, 0.196f, 0.196f, 1);
    Color4F lineColorGreen = Color4F(0.0f, 1.0f, 0.811f, 1);
    Color4F lineColor;
//    Vec2 intersectPoint = GameWorld::instance()->getMap()->getIntersectPoint(startPoint, endPoint);
	Vec2 intersectPoint = getIntersectPoint(startPoint,targetPos);
	
    if(!intersectPoint.isZero())
    {
        lineColor = lineColorGreen;
        mEndPointSprite->setPosition(intersectPoint);
        mEndPointSprite->setOpacity(255.0f);
    }
    else
    {
        lineColor = lineColorRed;
        mEndPointSprite->setOpacity(0);
    }
	
	bool showLine = startPoint != Vec2::ZERO;	// Quick fix to touch line remaining issue!
	
	if(showLine) {
		mTouchLineLayer->setVisible(true);
        Vec2 endPosToDraw = intersectPoint.isZero() ? targetPos : intersectPoint;
		mTouchLineLayer->drawSegment(startPoint, endPosToDraw, 1.5,lineColor);
		mTouchLineLayer->visit();
	} else {
		mTouchLineLayer->setVisible(false);
	}
	
	
}

void GameTouchLayer::hideTouchIndicator()
{
    auto fadeOut = FadeOut::create(0.3f);
    auto callbackRotate = CallFunc::create([](){});
    auto seq = Sequence::create(fadeOut, callbackRotate, nullptr);
    mTouchPointSprite->runAction(seq);
    
    
    
    auto delay = DelayTime::create(0.2f);
    auto fadeOut2 = FadeOut::create(0.3f);
    auto callbackRotate2 = CallFunc::create([](){});
    auto seq2 = Sequence::create(delay,fadeOut2, callbackRotate2, nullptr);
    mEndPointSprite->runAction(seq2);

    
    mTouchLineLayer->clear();
    mTouchLineLayer->visit();

}


void GameTouchLayer::clearTouchIndicator()
{
	// log("GameTouchLayer: clearTouchIndicator is called");
    mSlopeLineStartPoint.setZero();
    mTouchPointSprite->setOpacity(0);
    mEndPointSprite->setOpacity(0);
    mTouchLineLayer->clear();
}



void GameTouchLayer::handleSlopeStart(const Vec2 &touchPos)	// touchPosition (screenPos)
{
//	if(GameWorld::instance()->canPlayerDrawALine() == false) {
//		GameSound::playSound(GameSound::Button5);
//		GameWorld::instance()->getEffectLayer()->showNotEnoughEnergyAlert();
//		return;
//	}

	
	Vec2 position = touchPos;
	// Key Data
	//		mSlopeLineStartPoint	screenPosition (line start point)	this will be modified when map move
	//		mScreenYWhenStart		y position on the screen when start
	//		mOffsetYWhenStart		map positionY when start
	
	fixPosition(position);
	
	
	float startX = GameWorld::instance()->getMap()->getClostestLineX(position.x);
	mScreenYWhenStart = position.y;
	
	mSlopeLineStartPoint.set(startX, mScreenYWhenStart);	// screen position
	
	showTouchIndicator(mSlopeLineStartPoint);
	
    mDeltaPos = position;

	mOffsetYWhenStart = mMapOffsetY;		// Map Position (Y) when start
    
	mIsStarted = true;
	
	mLastTouchPos = mSlopeLineStartPoint;
	
	//mOffsetYWhenStart
}


void GameTouchLayer::handleSlopeEnd(const Vec2 &touchPos)
{
	
	if(mIsStarted == false) {
		return;
	}
	
	
    Vec2 initPos = mSlopeLineStartPoint;
 //   initPos.y += getPositionY();
	
 //   targetPos.y += getPositionY();
	
	// Convert the touchPosition to final drop position
	Vec2 pos = touchPos;
	fixPosition(pos);
	
	// note
	//log("initPos=%s endPos=%s", POINT_TO_STR(initPos).c_str(),
	//								POINT_TO_STR(pos).c_str());
	
	Vec2 targetPos;
	float diffX = pos.x - initPos.x;
	float diffY = pos.y - initPos.y;
	
	if(fabs(diffX) < kSpaceBetweenLine) {
		targetPos.x = initPos.x + diffX * 2;
		targetPos.y = initPos.y + diffY * 2;
	} else {
		targetPos = pos;	// the line over the spacing
	}
	
	
	
	
	Vec2 intersectPoint = getIntersectPoint(initPos,targetPos);
	if(!intersectPoint.isZero())
	{
		mEndPointSprite->setPosition(intersectPoint.x, intersectPoint.y);
		mEndPointSprite->setOpacity(255.0f);
//		GameWorld::instance()->drawAPlayingSlopeLine(mSlopeLineStartPoint, intersectPoint);
		
		//
		// Testing Code  FIX
		if(intersectPoint.y == initPos.y) {
			intersectPoint.y++;
		}
		
		// Fix 
		

		
		handleDropLine(initPos, intersectPoint);
	} else {
		GameSound::playSound(GameSound::CancelDrawLine);
	}
	
	hideTouchIndicator();
	
	mIsStarted = false;
}


void GameTouchLayer::handleDropLine(const Vec2 &start, const Vec2 &end)
{
	if(mDrawLineCallback != nullptr) {	// For testing and better decouple
		mDrawLineCallback(this, start, end);
	} else {
		
		
		GameWorld::instance()->drawAPlayingSlopeLine(start, end);
	}
}

Vec2 GameTouchLayer::getIntersectPoint(const Vec2 &initPos, const Vec2 &pos)
{
    Vec2 intersectPoint = GameWorld::instance()->getMap()->getIntersectPoint(initPos, pos);
    return intersectPoint;
}


void GameTouchLayer::handleSlopeMove(const Vec2 &pos)
{
	
}

bool GameTouchLayer::onTouchBegan(Touch *touch, Event* event)
{
	if(mEnable == false) {
		return false;
	}

	return XTLayer::onTouchBegan(touch, event);
}

void GameTouchLayer::handleTapLine(const Vec2 &pos)
{
	//handleTapLine(pos)
	if(GameWorld::instance()->canPlayerDrawALine()) {
		GameWorld::instance()->drawAPlayingHoriLine(pos);
	} else {
        GameSound::playSound(GameSound::CancelDrawLine);
        GameWorld::instance()->getEffectLayer()->showNotEnoughEnergyAlert();
	}
}

void GameTouchLayer::resetData()
{
	mIsTouching = false;
	mHasBraked = false;
	mTimeTouched = 0;
}

#pragma mark - Touch Callback
void GameTouchLayer::xtTouchesBegan(Vec2 position)
{
	//log("GameTouchLayer: touchBegan");
	
	if(mEnable == false) {
		return;
	}
	
	resetData();
	mIsTouching = true;

	if(mType == Type::Slide) {		// Draw Line
		handleSlopeStart(position);
	} else {						// Tap Line
		showTouchIndicator(position);
	}
}

void GameTouchLayer::xtTouchesEnded(Vec2 position)  
{
	//log("GameTouchLayer: touchEnd");
	
	if(mEnable == false) {
		return;
	}
	mIsTouching = false;
	
	resetTouchInfo();
	GameWorld::instance()->setIsBraking(false);

	
	if(mType == Type::Slide) {
		handleSlopeEnd(position);
	} else {
		hideTouchIndicator();
	}
	

}

void GameTouchLayer::xtTouchesMoved(Vec2 position)
{
	if(mIsStarted == false) {
		return;
	}
	
	
	if(mType == Type::Slide) {
		mLastTouchPos = position;
//		showDrawingSlopeLine(mSlopeLineStartPoint, position);
	}
}

void GameTouchLayer::xtTapGesture(Vec2 position)
{
	if(mEnable == false) {
		return;
	}
	
    if(mType == Type::Tap) {
        handleTapLine(position);
    }
}


void GameTouchLayer::xtDoubleTapGesture(Vec2 position)
{
	if(mEnable == false) {
		return;
	}
	
	//handleTapLine(position);
}

void GameTouchLayer::xtSwipeGesture(XTTouchDirection direction, float distance, float speed)
{
	if(mEnable == false) {
		return;
	}
	
	// ken: now game change to drag line and we should not use swipe to activate
	// GameWorld::instance()->activateCapsule();
}

void GameTouchLayer::setDrawLineCallback(const GameTouchDrawLineCallback &callback)
{
	mDrawLineCallback = callback;
}


void GameTouchLayer::updateLineStartPosition()
{
	if(mIsStarted == false) {
		return;	// not drop, no need to handle anything
	}
	
	float diff = mMapOffsetY - mOffsetYWhenStart;		// Suppose mapOffsetY is large
														// diff map moved after line is start
	
	mSlopeLineStartPoint.y = mScreenYWhenStart - diff;
	
	showTouchIndicator(mSlopeLineStartPoint);
	showDrawingSlopeLine(mSlopeLineStartPoint, mLastTouchPos);
}

//
//void GameTouchLayer::onTouchCancelled(Touch* touch, Event* event)
//{
//    //cocos2d::log("touch cancelled");
//}








//GameTouchLayer::xtTouchesEnded
//    if(GameWorld::instance()->isSlopeModeEnabled())
//    {
//        Vec2 intersectPoint =  GameWorld::instance()->getMap()->getIntersectPoint(mSlopeLineStartPoint, touch->getLocation());
//        if(!intersectPoint.isZero())
//        {
//            mEndPointSprite->setPosition(intersectPoint.x, intersectPoint.y);
//            mEndPointSprite->setOpacity(255.0f);
//            GameWorld::instance()->drawAPlayingSlopeLine(mSlopeLineStartPoint,intersectPoint);
//        }
//    }
//    else
//    {
//		if(GameWorld::instance()->canPlayerDrawALine()) {
//			GameWorld::instance()->drawAPlayingHoriLine(touch->getLocation());
//		} else {
//			//ViewHe
//			Vec2 uiPos(0, 200);
//			Vec2 glPos = Director::getInstance()->convertToGL(uiPos);
//			ViewHelper::showFadeAlert(this, "Not enough energy!\nCollect some more!", glPos.y, 12);
//		}
//    }
//

void GameTouchLayer::resetTouchInfo()
{
	mIsTouching = false;
	mTimeTouched = 0;
}

void GameTouchLayer::update(float delta)
{
	if(mIsTouching) {
		mTimeTouched += delta;
		if(mHasBraked == false && mTimeTouched > 0.2 ) {
			GameWorld::instance()->setIsBraking(true);
			mHasBraked = true;
            GameWorld::instance()->getPlayer()->addCsbAnimeByName("particle_breakspeed_1.csb", false, 1.0, Vec2(0,0));
            GameWorld::instance()->getPlayer()->playCsbAnime("particle_breakspeed_1.csb", "active", false, true);
            GameSound::playSound(GameSound::Brake);
		}
	}
}


std::string GameTouchLayer::statInfo()
{
	return StringUtils::format("touch=%ld", getChildrenCount());
}
