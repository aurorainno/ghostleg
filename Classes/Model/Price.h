//
//  Price.hpp
//  GhostLeg
//
//  Created by Ken Lee on 20/1/2017.
//
//

#ifndef Price_hpp
#define Price_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

// The class structure of Price is similar to Vec2.h
class Price
{
public:
	static std::string typeToString(MoneyType type);

public:		// Constructor
	Price();
	Price(MoneyType _type, int _amount);
	
public:
	MoneyType type;
	int amount;
	
public:
	
	std::string toString();

};


#endif /* Price_hpp */
