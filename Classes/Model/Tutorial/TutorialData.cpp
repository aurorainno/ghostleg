//
//  TutorialData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 4/7/2016.
//
//

#include "TutorialData.h"
#include "TutorialLayer.h"
#include "GameWorld.h"
#include "Player.h"
#include "NPCMapLogic.h"
#include "VisibleRect.h"
#include "GameScene.h"
#include "GameSound.h"
#include "GameTouchLayer.h"
#include "GameMap.h"
#include "StringHelper.h"

namespace {	// local method
	bool isTouchDropAccept(const Rect &touchArea, bool atRight, const Vec2 &touchPos)
	{
		if(atRight) {
			float xThreshold = touchArea.getMinX();
			return touchPos.x >= xThreshold;
		} else {
			float xThreshold = touchArea.getMaxX();
			return touchPos.x <= xThreshold;
		}
	}
	
	Rect convertMapAreaToScreen(const Rect &mapArea)
	{
		Rect result = mapArea;
		
		result.origin.y += GameWorld::instance()->getMapLogic()->getStartOffset();
		result.origin.y -= GameWorld::instance()->getCameraY();
		
		return result;
	}
	
	//
	// mapTouchArea the area based on the relative Map position from the 1st map
	// touchPos : the screenPosition
	bool checkTouchArea(const Rect &mapTouchArea, const Vec2 &touchPos)	//
	{
//		Rect screenTouchArea = mapTouchArea;
//		screenTouchArea

		return false;
	}
	
	
	
}

TutorialData *TutorialData::create()
{
	TutorialData *data = new TutorialData();
	
	data->autorelease();
	
	return data;
}

TutorialData::TutorialData()
: mStep(0)
, mStartCondition(StartCondition::AutoStart)
, mStartConditionArg(0)
, mDisplayCsb("")
, mDisplayType(DisplayType::DisplayTypeTip)
, mDisplayPosType(PositionType::MapGridPos)
, mDisplayPosition(Vec2(0, 0))
, mEndCondition(EndCondition::TapAnywhere)
, mHitArea(Rect(70, 180, 90, 60))
, mSlopeEndHitArea(Rect(-15, 150, 90, 60))
, mEndConditionArg(0)
, mIsLoopAnimation(false)
, mShowNpcDialog(false)
, mDialogMessage("")
, mCsbRotation(0)
, mCommand("")
{
	
}

std::string TutorialData::toString()
{
	std::string result = "";
	
	result += "csb=" + mDisplayCsb;
	result += StringUtils::format(" posType=%d", mDisplayPosType);
	result += " displayPos=" + POINT_TO_STR(mDisplayPosition);
	result += StringUtils::format(" showDialog=%d", getShowNpcDialog());
	result += StringUtils::format(" dialogMsg=%s", mDialogMessage.c_str());

	return result;
}

TutorialData::StartCondition TutorialData::getStartConditionByName(std::string name){
    if("mapY" == name){
        return TutorialData::StartCondition::PlayerY;
	} else if("distance" == name){
		return TutorialData::StartCondition::TravelDistance;
    } else {
        // log("fail to retrieve correct startCond string from json");
        return TutorialData::StartCondition::AutoStart;
    }
}

TutorialData::PositionType TutorialData::getPositionTypeByName(std::string name){
    if("MapPos" == name) {
        return TutorialData::PositionType::MapPos;
    } else if("PlayerPos" == name) {
        return TutorialData::PositionType::PlayerPos;
	} else if("ScreenPos" == name) {
		return TutorialData::PositionType::ScreenPos;
    } else{
        return TutorialData::PositionType::MapGridPos;
    }
}

TutorialData::EndCondition TutorialData::getEndConditionByName(std::string name){
    if(name.compare("tapAnywhere")==0){
        return TutorialData::EndCondition::TapAnywhere;
    }else if(name.compare("dropLine")==0){
        return TutorialData::EndCondition::DropLine;
    }else{
        return TutorialData::EndCondition::DeltaTime;
    }
}

void TutorialData::setHitAreaByRadius(const Vec2 &centerPos, Vec2 size)
{
	Rect rect;
	
	rect.origin = centerPos;
	rect.size = Size(size);
	
	setHitArea(rect);
}

#pragma mark - TutorialStep 

TutorialStep::TutorialStep()
: mData(nullptr)
, mState(State::NotStart)
, mUILayer(nullptr)
, mNodeList()
, mLineEndArea(Rect::ZERO)
, mLineStartArea(Rect::ZERO)
{
	mNodeList.clear();
}


TutorialStep *TutorialStep::create()
{
	TutorialStep *data = new TutorialStep();
	
	data->autorelease();
	
	return data;

}

void TutorialStep::reset()
{
	mState = State::NotStart;
}

bool TutorialStep::update(float delta)
{
	//log("TutorialStep: update. mState=%d delta=%f step=%d", mState, delta, mData->getStep());
	
	switch(mState) {
		case NotStart: {
			logicNotStart(delta);
			break;
		}
		case Started: {
			logicStarted(delta);
			break;
		}
			
		default:
			break;
	}
	
	return isDone();
}


void TutorialStep::setState(TutorialStep::State state)
{
	switch(state) {
		case Started: {
			changeToStart();
			break;
		}
			
		case Done: {
			changeToDone();
			break;
		}
			
		default:
			break;
	}
}

bool TutorialStep::canStart()
{
	if(mData == nullptr) {
		log("TutorialStep: error: mData is null");
		return false;
	}
	
	switch (mData->getStartCondition()) {
		case TutorialData::PlayerY: {
			float playerY = GameWorld::instance()->getPlayer()->getPositionY();
			
			return playerY >= mData->getStartConditionArg();
		}
			
		case TutorialData::TravelDistance: {
			float travelDist = GameWorld::instance()->getMapLogic()->getTravelDistance();
			// log("DEBUG: Distance Travel=%f", travelDist);
			
			return travelDist >= mData->getStartConditionArg();
		}
			
		default:
			return true;
	};
}

void TutorialStep::logicNotStart(float delta)
{
	if(canStart() == false) {
		return;
	}
	
	setState(State::Started);
}

void TutorialStep::logicStarted(float delta)
{
	// logic to check if the tutorial step ended
	mTimeElapse += delta;
	
	// Checking the Done condition
	if(mData == nullptr) {
		return;
	}
	
	if(mData->getEndCondition() == TutorialData::EndCondition::DeltaTime) {
		if(mTimeElapse >= mData->getEndConditionArg()) {
			setState(State::Done);
		}
	}
}


void TutorialStep::showDebugUI()
{
	std::string text = StringUtils::format("Tutorial %d", mData->getStep());
	Label *label = Label::createWithTTF(text, "arial.ttf", 20);
	
	if(mUILayer) {
		label->setPosition(Vec2(200, 300));
		mUILayer->addChild(label);
		mNodeList.pushBack(label);
		
		
		if(mData->getEndCondition() == TutorialData::EndCondition::DropLine)
        {
            Rect area = mData->getHitArea();
            float screenOffset = GameWorld::instance()->getCameraY();
            Vec2 mapPos = area.origin + Vec2(0, -screenOffset);
			Color4B color = Color4B::RED;
			color.a = 150;
			LayerColor *box = LayerColor::create(color, area.size.width, area.size.height);
			box->setPosition(mapPos);
			mUILayer->addChild(box);
			mNodeList.pushBack(box);
            
            
            Rect slopeEndArea = mData->getSlopeEndHitArea();
            Color4B color2 = Color4B::GREEN;
            color2.a = 150;
            Vec2 mapPos2 =  slopeEndArea.origin + Vec2(0, -screenOffset);
            LayerColor *box2 = LayerColor::create(color2, slopeEndArea.size.width, slopeEndArea.size.height);
            box2->setPosition(mapPos2);
            mUILayer->addChild(box2);
            mNodeList.pushBack(box2);
            
		}
	}
}


Vec2 TutorialStep::getFinalDisplayPosition()
{
	if(mData == nullptr) {
		return Vec2::ZERO;
	}
	
	Vec2 givenPos = mData->getDisplayPosition();
	
	TutorialData::PositionType posType = mData->getDisplayPosType();
	
	switch(posType) {
		case TutorialData::PositionType::PlayerPos:
		{
			Vec2 pos = GameWorld::instance()->getPlayer()->getPosition();
			
			pos.y = pos.y - GameWorld::instance()->getCameraY() + 35;
			
			return pos;
		}
			
		case TutorialData::PositionType::MapPos:
		{
			Vec2 screenPos = givenPos;		// given is the map position
			screenPos.y -= GameWorld::instance()->getCameraY();
		
			return screenPos;
		}
			
		default:		// ScreenPosition
		{
			return givenPos;
		}
	}
}

void TutorialStep::showCheckArea(const Rect &touchArea)	// this is the map coordinate
{
	if(touchArea.size.width == 0 || touchArea.size.height == 0) {
		return;
	}
	
	
	
	Rect screenRect = convertMapAreaToScreen(touchArea);
	
	float width = screenRect.size.width;
	float height = screenRect.size.height;
	
	LayerColor *checkBox = LayerColor::create(Color4B(200, 0, 0, 100), width, height);
	checkBox->setPosition(screenRect.origin);
	
	if(mUILayer != nullptr) {
		mUILayer->addChild(checkBox);
		
		mRefNodeList.pushBack(checkBox);
	}
}


//
// Case 1: Show the Tutorial Csb Node on top of player
//			Load csb node
//			setPosition near the player
// Case 2: Show on specific map position
//			gx,gy -> screen x, y		* TutorialManager::gridToScreenPos *
//
void TutorialStep::showTutorial()
{
	if(mData == nullptr) {
		return;
	}
	if(mUILayer == nullptr) {
		return;
	}
	
	//showCheckArea
	//showCheckArea(getLineEndArea());
	//showCheckArea(getLineStartArea());
	
	
	
	if(mData->getDisplayType() == TutorialData::DisplayType::DisplayTypeBanner) {
		showBanner();
		return;
	}
	
	//showDebugUI();
	
	showTutorialCsb();

	// Show dialog
	if(mData->getShowNpcDialog()) {
		std::string msg = mData->getDialogMessage();
		
		mUILayer->showDialog(msg);
	}
	
	if(mData->getCommand() != "") {
		runCommand(mData->getCommand());
	}
	
		//
    if(mData->getStep() > 0){		// TODO: Change to sound
		GameSound::playSound(GameSound::Sound::Button2);
	}
}

void TutorialStep::runCommand(const std::string &cmd)
{
	if("showChaser" == cmd)
	{
		GameWorld::instance()->activateChaser();
	}
}

void TutorialStep::changeToStart()
{
	mState = State::Started;
	mTimeElapse = 0;
	showTutorial();
	
	//
}

void TutorialStep::changeToDone()
{
	mState = State::Done;
	
	// Remove the node created
	for(int i=0; i<mNodeList.size(); i++) {
		Node *node = mNodeList.at(i);
		node->removeFromParent();
	}
	mNodeList.clear();
	
	clearRefNodeList();
}

bool TutorialStep::handleDropLine(const Vec2 &screenPos)
{
	if(mData == nullptr) {
		return true;
	}
	
	
	float screenOffset = GameWorld::instance()->getCameraY();
	
	Vec2 mapPos = screenPos + Vec2(0, screenOffset);
	
	//if(mData->getHitArea().containsPoint(mapPos) == false) {
	if(mLineStartArea.containsPoint(mapPos) == false) {
		return false;
	}
	
	GameWorld::instance()->drawAPlayingHoriLine(screenPos);
	
	return true;
}

Rect TutorialStep::getLineEndArea()
{
	return mLineEndArea;
}

Rect TutorialStep::getLineStartArea()
{
	return mLineStartArea;
}

Vec2 TutorialStep::getSlopeLineMidPoint()
{
    //Rect temp = mData->getHitArea();
	Rect screenArea = convertMapAreaToScreen(mLineStartArea);
	//float offsetY = GameWorld::instance()->getCameraY();
    return Vec2(screenArea.getMidX(), screenArea.getMidY());

}


bool TutorialStep::canSlopeLineStart(const Vec2 &screenPos)
{
	Rect requiredArea = convertMapAreaToScreen(mLineStartArea);
	
	// Check the side of the startArea
	//
	float startMid = mLineStartArea.getMaxX();
	float endMid = mLineEndArea.getMaxX();
	bool atRight = startMid > endMid;
	
	
	//
	
	return isTouchDropAccept(requiredArea, atRight, screenPos);
	
	//return requiredArea.containsPoint(screenPos);		// strict checking
}

bool TutorialStep::canSlopeLineEnd(const Vec2 &screenPos)
{
	Rect requiredArea = convertMapAreaToScreen(mLineEndArea);
	
	// Check the side of the startArea
	//
	float startMid = mLineStartArea.getMaxX();
	float endMid = mLineEndArea.getMaxX();
	bool atRight = startMid < endMid;	
	
	//
	
	return isTouchDropAccept(requiredArea, atRight, screenPos);
	
	//return requiredArea.containsPoint(screenPos);		// strict checking
}


bool TutorialStep::shouldAcceptSlopeLineDrop(const Vec2 &screenPos)
{
    if(mData == nullptr) {
		return true;
	}
	
	Rect requireScreenArea = convertMapAreaToScreen(mLineEndArea);
	
    // if(mData->getSlopeEndHitArea().containsPoint(mapPos) == false)
	//if(requireScreenArea.containsPoint(screenPos) == false)
	if(canSlopeLineEnd(screenPos) == false)
    {
        return false;
    }
	
	float startX = convertMapAreaToScreen(mLineStartArea).getMidX();
	float startY = convertMapAreaToScreen(mLineStartArea).getMidY();
	
	float endX = convertMapAreaToScreen(mLineEndArea).getMidX();
	float endY = convertMapAreaToScreen(mLineEndArea).getMidY();
	
	Vec2 startPoint = Vec2(startX, startY);
	Vec2 endPoint = Vec2(endX, endY);
	
	
//	Vec2 startMid = Vec2(mLineStartArea.getMinX(), mLineStartArea.getMidY());
//	Vec2 endMid = Vec2(mLineEndArea.getMinX(), mLineEndArea.getMidY());
//	
//	float startPointX = GameWorld::instance()->getMap()->getClostestLineX(startMid.x);
//	
//	Vec2 startPoint = Vec2(startPointX, startMid.y - screenOffset);
//        
//        
//	float endPointX = GameWorld::instance()->getMap()->getClostestLineX(endMid.x);
//	
//	
//	Vec2 endPoint = Vec2(endPointX, endMid.y - screenOffset);

	GameWorld::instance()->drawAPlayingSlopeLine(startPoint, endPoint);
	
	setState(State::Done);
        
	return true;
}





void TutorialStep::handleInput(const Vec2 &screenPos)
{
	// Check if the input match 'end' condition
	if(mData == nullptr) {
		log("TutorialStep: error: data not set!");
		return;
	}
	
	bool isDone = false;
	
	//log("Tutorial.handleInput: screenPos=%s", POINT_TO_STR(screenPos).c_str());
	
	switch (mData->getEndCondition())
    {
		case TutorialData::TapAnywhere:
        {
            if(isWaitForPlayer() && mTimeElapse>0.5){
                isDone = true;
            }
			break;
		}
		case TutorialData::DropLine:	// draw a line
        {
			// log("Tutorial.handleInput: handleDropLine");
            GameWorld::instance()->getGameTouchLayer()->hideTouchIndicator();
            shouldAcceptSlopeLineDrop(screenPos);
            break;
        }
			
		case TutorialData::TapHitArea:
		{
			if(isHitAreaTouched(screenPos)) {
				isDone = true;
			}
			break;
		}
			
		default:						// tap a line
        {
			isDone = handleDropLine(screenPos);
			break;
		}
	}
	
	if(isDone) {
		mUILayer->closeDialog();
		mUILayer->setDialogVisible(false);
		setState(State::Done);
	}
}

bool TutorialStep::isDone()
{
	return mState == State::Done;
}


std::string TutorialStep::toString()
{

	std::string result = "";
	
	
	return result;
	
}

bool TutorialStep::isWaitForPlayer()
{
    return mState == State::Started && mData->getEndCondition() != TutorialData::EndCondition::DeltaTime;
}



void TutorialStep::clearRefNodeList()
{
	for(Node *node : mRefNodeList) {
		node->removeFromParent();
	}
	
	mRefNodeList.clear();
	
}

void TutorialStep::setData(TutorialData *data)
{
	mData = data;
	
	if(mData != nullptr) {
		// Setting the mLine Start and End Area
		
		mLineStartArea = adjustRectForMidAnchor(data->getHitArea());
		
		mLineEndArea = adjustRectForMidAnchor(data->getSlopeEndHitArea());
	}
}

TutorialData *TutorialStep::getData()
{
	return mData;
}

Rect TutorialStep::adjustRectForMidAnchor(const Rect &rect)
{
	Rect result = rect;
	
	result.origin.x -= rect.size.width / 2;
	result.origin.y -= rect.size.height / 2;
	
	return result;
}

bool TutorialStep::isDropLine()
{
	if(mData == nullptr) {
		return false;
	}
	
	return mData->getEndCondition() == TutorialData::EndCondition::DropLine;
	
}

bool TutorialStep::isHitAreaTouched(const Vec2 &screenPos)
{
	Rect hitArea = adjustRectForMidAnchor(mData->getHitArea());
	
	return hitArea.containsPoint(screenPos);
}



#pragma mark - Different Tutorial Display Logic
void TutorialStep::showBanner()
{
	GameSceneLayer *gameUILayer = GameWorld::instance()->getGameUILayer();
	if(gameUILayer != nullptr) {
		gameUILayer->addOneTimeAnimation(mData->getDisplayCsb(), VisibleRect::center());
	}
}

Vec2 TutorialStep::getDisplayScreenPosition()
{
	Vec2 pos;
	
	// Find the screen display position (pos is the coordinate based on Screen Left Bottom Corner)
	if(mData->getDisplayPosType()==TutorialData::PositionType::PlayerPos){
		pos = GameWorld::instance()->getPlayer()->getPosition();
		pos.y = pos.y-GameWorld::instance()->getCameraY()+35;
		
	} else if (mData->getDisplayPosType()==TutorialData::PositionType::MapPos){
		pos = mData->getDisplayPosition();
		pos.y += GameWorld::instance()->getMapLogic()->getStartOffset();
		pos.y -= GameWorld::instance()->getCameraY();		// Convert from MapY to ScreenY
		
	} else {		// ScreenPos
		pos = mData->getDisplayPosition();
	}
	
	return pos;
}

void TutorialStep::showTutorialCsb()
{
	
	std::string csbName = mData->getDisplayCsb();
    float rotation = mData->getCsbRotation();
	if("" == csbName) {
		return;		// No need to show
	}
	
	Vec2 displayPos = getDisplayScreenPosition();
	
//	log("showTutorialCsb: screenPos: %s mapPos: %s"
//						, POINT_TO_STR(displayPos).c_str()
//						, POINT_TO_STR(mData->getDisplayPosition()).c_str());
	
	
	if(mData->getIsLoopAnimation()) {
		mUILayer->addLoopAnimation(csbName, displayPos, 0.5,rotation);
	} else {
		mUILayer->addOneTimeAnimation(csbName, displayPos, 0.5, false, NULL, rotation);
	}
}
