//
//  TutorialData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 4/7/2016.
//
//

#ifndef TutorialData_hpp
#define TutorialData_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "CommonMacro.h"

USING_NS_CC;

class TutorialLayer;

class TutorialData : public Ref
{
public:
	enum StartCondition {
		PlayerY = 0,
		TravelDistance = 1,
		AutoStart,
	};
	
	
	// PositionType
	// PlayerPos:
	
	enum PositionType {
		PlayerPos,
		MapGridPos,		// Grid position of the map
		MapPos,
		ScreenPos,
	};
	
	enum DisplayType {
		DisplayTypeTip,
		DisplayTypeBanner,		// End tutorial banner
	};
	
	enum EndCondition {
		TapAnywhere,
		DropLine,
		DeltaTime,
		TapHitArea,
		//MapY,
	};
	
public:
	static TutorialData *create();
	
    static StartCondition getStartConditionByName(std::string name);
    static PositionType getPositionTypeByName(std::string name);
    static EndCondition getEndConditionByName(std::string name);
    
	TutorialData();
	
	CC_SYNTHESIZE(int, mStep, Step);
	CC_SYNTHESIZE(StartCondition, mStartCondition, StartCondition);
	CC_SYNTHESIZE(int, mStartConditionArg, StartConditionArg);
	
	CC_SYNTHESIZE(std::string, mDisplayCsb, DisplayCsb);
    CC_SYNTHESIZE(float, mCsbRotation, CsbRotation);
	CC_SYNTHESIZE(std::string, mCommand, Command);
	CC_SYNTHESIZE(bool, mIsLoopAnimation, IsLoopAnimation);
	
	CC_SYNTHESIZE(PositionType, mDisplayPosType, DisplayPosType);
	CC_SYNTHESIZE(DisplayType, mDisplayType, DisplayType);
	CC_SYNTHESIZE(Vec2, mDisplayPosition, DisplayPosition);		// For InGame Tutorial
    //CC_SYNTHESIZE(Vec2, mCsbPosition, CsbPosition);				// For CoachMark
	
	CC_SYNTHESIZE(EndCondition, mEndCondition, EndCondition);
	CC_SYNTHESIZE(Rect, mHitArea, HitArea);
    CC_SYNTHESIZE(Rect, mSlopeEndHitArea, SlopeEndHitArea);
	CC_SYNTHESIZE(float, mEndConditionArg, EndConditionArg);

	CC_SYNTHESIZE(bool, mShowNpcDialog, ShowNpcDialog);
	CC_SYNTHESIZE(std::string, mDialogMessage, DialogMessage);

	
	void setHitAreaByRadius(const Vec2 &centerPos, Vec2 size);
	
	std::string toString();
//	
//	void reset();
	
	
private:
	
//	std::vector<float> mLevelValue;		// idx: level  value=Effect value of mastery
//	std::vector<int> mLevelPrice;		// idx: level  value=Price to upgrade
};


class TutorialStep : public Ref
{
public:
	enum State {
		NotStart,
		Started,
		Done,
	};
	
public:
	static TutorialStep *create();
	
	TutorialStep();
	CC_SYNTHESIZE(TutorialLayer *, mUILayer, UILayer);		// TutorialLayer owned by GameWorld, not need to retain
	
	
	
	
	void setData(TutorialData *data);
	TutorialData *getData();
	
	bool update(float delta);		// return true if the tutorial already done
	void setState(State state);
	void handleInput(const Vec2 &screenPos);
	void showTutorial();
	bool isDone();
	bool isWaitForPlayer();
	void reset();
	bool isDropLine();
	void runCommand(const std::string &cmd);
	
	Rect getLineEndArea();
    Rect getLineStartArea();		// Start Area
	
    Vec2 getSlopeLineMidPoint();
    bool canSlopeLineStart(const Vec2 &screenPos);
	bool canSlopeLineEnd(const Vec2 &screenPos);
    bool shouldAcceptSlopeLineDrop(const Vec2 &screenPos);
	
	
	bool isHitAreaTouched(const Vec2 &screenPos);
	
	void showCheckArea(const Rect &touchArea);
	
	Vec2 getFinalDisplayPosition();
	
	
protected:
	bool canStart();
	void onStart();
	
	void logicNotStart(float delta);
	void logicStarted(float delta);
	
	void changeToStart();
	void changeToDone();
	bool handleDropLine(const Vec2 &screenPos);
	
	
	
	void clearRefNodeList();

	void showDebugUI();
	
	std::string toString();
	
	Rect adjustRectForMidAnchor(const Rect &rect);
	
private:
	TutorialData *mData;
	State mState;
	Vector<Node *> mNodeList;
	float mTimeElapse;
	Vector<Node *> mRefNodeList;
	Rect mLineStartArea;		// Adjust the with the anchorPoint, new origin = old - size/2
	Rect mLineEndArea;			// Adjust the with the anchorPoint, new origin = old - size/2

#pragma mark - Different Tutorial Display Logic
protected:
	Vec2 getDisplayScreenPosition();
	void showTutorialCsb();
	void showBanner();

};

#endif /* TutorialData_hpp */
