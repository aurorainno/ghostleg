//
//  Collectable.hpp
//  GhostLeg
//
//  Created by Calvin on 12/7/2017.
//
//

#ifndef Collectable_hpp
#define Collectable_hpp

#include <stdio.h>
#include "Item.h"


class Collectable : public Item
{
public:
    
#pragma mark - Public static Method
//    CREATE_FUNC(Collectable);
//    static const std::string kCsbFile;
    
    
#pragma mark - Public Method and Properties
    Collectable(Type type);
    
    CC_SYNTHESIZE(bool, mMoveToPlayer, MoveToPlayer);
    CC_SYNTHESIZE(bool, mStartMoving, StartMoving);
    CC_SYNTHESIZE(bool, mMovingToFreeSpace, MovingToFreeSpace);
    CC_SYNTHESIZE(int, mVelocity, Velocity);
    
    virtual bool init();	// virtual because different derived class have different init logic
    virtual bool use();
    virtual void update(float delta);
    
    
private:
    void moveTowardPlayer(float delta);
    
    
};


#endif /* Collectable_hpp */
