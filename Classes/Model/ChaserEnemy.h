//
//  ChaserEnemy.hpp
//  GhostLeg
//
//  Created by Ken Lee on 9/5/2017.
//
//

#ifndef ChaserEnemy_hpp
#define ChaserEnemy_hpp

#include <stdio.h>
#include <map>
#include "Enemy.h"

class ChaserBehaviour;

class ChaserEnemy : public Enemy
{
public:
#pragma mark - Public static Method
	CREATE_FUNC(ChaserEnemy);
	
#pragma mark - Public Method and Properties
	ChaserEnemy();
	~ChaserEnemy();
	
	virtual bool init();	// virtual because different derived class have different init logic
	
	void setSpeedAttribute(const std::map<std::string, int> &attribute);
	void increaseSpeed();
	
private:
	void setupEnemy();
	void setupMockAttribute();
	float adjustSpeed(float originSpeed);
	
private:
	ChaserBehaviour *mBehaviour;
};

#endif /* ChaserEnemy_hpp */
