//
//  EffectLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/6/2016.
//
//

#include "EffectLayer.h"
#include "AnimationHelper.h"
#include "VisibleRect.h"
#include "ItemEffect.h"
#include "ViewHelper.h"
#include "EnergyEffectNode.h"
#include "GameWorld.h"
#include "GameScene.h"
#include "GameSound.h"
#include "StringHelper.h"
#include "AnimeNode.h"

#define kParticleScale 0.25


const int kFrontEffectZOrder = 1000;
const int kIndicatorPosX[] = {95,143,191};
const int kIndicatorOffsetY = 27;

namespace {
	
	
}


EffectLayer::EffectLayer()
: mScorePosition(VisibleRect::top())
, mCoinPosition(VisibleRect::rightTop())
, mWindEffect(nullptr)
{
	
}

//
bool EffectLayer::init()
{
	Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	
	bool isOk = LayerColor::initWithColor(Color4B(0, 0, 0, 0), size.width, size.height);
	if (isOk == false)
	{
		return false;
	}
	
    mItemEffectList.clear();
    
	mOffsetY = 0;
	mStarCollected = 0;
	mCandyCollected = 0;
    mAnimeID = 0;

	
	// ScrollLayer
	mScrollLayer = Layer::create();
	addChild(mScrollLayer);
	
	// ScreenLayer
	mScreenLayer = Layer::create();
	addChild(mScreenLayer);
	
    mCautionMap.clear();
    mTimelineMap.clear();
    
    resetIndicator();
	
	return true;
}


void EffectLayer::cleanup()	// ken: no need now
{

}

void EffectLayer::reset()
{
	setOffsetY(0);
    eraseAllCaution();
	mScrollLayer->removeAllChildren();
    mScreenLayer->removeAllChildren();
	setupWindEffect();
    resetIndicator();

	
	mStarCollected = 0;
	mCandyCollected = 0;
    mAnimeID = 0;
    mTimelineMap.clear();
    mCallbackMap.clear();
    mCsbNodeMap.clear();
    
    scheduleUpdate();
}

void EffectLayer::resetIndicator()
{
    for(ItemEffectIndicator* indicator : mItemEffectList){
        if(!indicator){
            continue;
        }
        if(!indicator->getParent()){
            continue;
        }
        
        indicator->removeFromParent();
    }
    
    mItemEffectList.clear();
    
/*   
    mLeftIndicator = ItemEffectIndicator::create();
    mLeftIndicator->setAnchorPoint(Vec2(0,1));
    mLeftIndicator->setPosition(Vec2(94,mScreenLayer->getContentSize().height - 27));
    mCenterIndicator = ItemEffectIndicator::create();
    mCenterIndicator->setAnchorPoint(Vec2(0,1));
    mCenterIndicator->setPosition(Vec2(143,mScreenLayer->getContentSize().height - 27));
    mRightIndicator = ItemEffectIndicator::create();
    mRightIndicator->setAnchorPoint(Vec2(0,1));
    mRightIndicator->setPosition(Vec2(190,mScreenLayer->getContentSize().height - 27));
    addNodeToScreenLayer(mRightIndicator);
    addNodeToScreenLayer(mCenterIndicator);
    addNodeToScreenLayer(mLeftIndicator);
 */
}

void EffectLayer::update(float delta)
{
    Map<int, cocostudio::timeline::ActionTimeline *>::iterator it;
    std::vector<int> keyList{};
    
    for(it=mTimelineMap.begin();it!=mTimelineMap.end();it++){
        cocostudio::timeline::ActionTimeline *timeline = it->second;
        if(timeline->getCurrentFrame()>=timeline->getEndFrame()){
            keyList.push_back(it->first);
        }
    }
    
    for(int key : keyList){
        std::function<void()> callback = mCallbackMap.at(key);
        if(callback){
            callback();
        }
    }
    
}

void EffectLayer::addNodeToScrollLayer(Node *node)
{
	if(mScrollLayer) {
		mScrollLayer->addChild(node);
	}
	
}

void EffectLayer::addNodeToScreenLayer(Node *node)
{
	if(mScreenLayer) {
		mScreenLayer->addChild(node);
	}
}

void EffectLayer::addPowerUpIndicator(EffectLayer::PowerUpType type, float time)
{
    for(ItemEffectIndicator* indicator : mItemEffectList){
        if(indicator->isMatchWithEffect(type)){
            indicator->config(indicator->getEffectType(), time);
            indicator->activate();
            return;
        }
    }
    
    //do not add more than 3 indicator currently!
    if(mItemEffectList.size() >= 3){
        return;
    }
    
    ItemEffectIndicator *indicator = ItemEffectIndicator::create();
    int posXIndex = (int) mItemEffectList.size();
    indicator->setAnchorPoint(Vec2(0,1));
    indicator->setPosition(Vec2(kIndicatorPosX[posXIndex], mScreenLayer->getContentSize().height - kIndicatorOffsetY));
    indicator->setOnFinishCallback([=](ItemEffectIndicator* indicator){
        reorganizeIndicatorPos(indicator);
    });
    addNodeToScreenLayer(indicator);
    mItemEffectList.pushBack(indicator);
    
    switch (type) {
        case SpeedUp:{
            indicator->config(ItemEffectIndicator::SpeedUp, time);
            indicator->activate();
            break;
        }
 
        case Magnet:{
            indicator->config(ItemEffectIndicator::Magnet, time);
            indicator->activate();
            break;
        }
            
        case CashOut:{
            indicator->config(ItemEffectIndicator::CashOut, time);
            indicator->activate();
            break;
        }
            
        default:
            break;
    }
}



void EffectLayer::addBoosterIndicator(EffectLayer::BoosterType type, float time)
{
    for(ItemEffectIndicator* indicator : mItemEffectList){
        if(indicator->isMatchWithEffect(type)){
            indicator->config(indicator->getEffectType(), time);
            indicator->activate();
 //           log("%.2f.%.2f",indicator->getPositionX(),indicator->getPositionY());
            return;
        }
    }
    
    //do not add more than 3 indicator currently!
    if(mItemEffectList.size() >= 3){
        return;
    }
    
    ItemEffectIndicator *indicator = ItemEffectIndicator::create();
    int posXIndex = (int) mItemEffectList.size();
    indicator->setAnchorPoint(Vec2(0,1));
    indicator->setPosition(Vec2(kIndicatorPosX[posXIndex], mScreenLayer->getContentSize().height - kIndicatorOffsetY));
    indicator->setOnFinishCallback([=](ItemEffectIndicator* indicator){
        reorganizeIndicatorPos(indicator);
    });
    addNodeToScreenLayer(indicator);
    mItemEffectList.pushBack(indicator);
    
    switch (type) {
        case Shield:{
            indicator->config(ItemEffectIndicator::Shield, time);
            indicator->activate();
            break;
        }
            
        case TimeStop:{
            indicator->config(ItemEffectIndicator::TimeStop, time);
            indicator->activate();
            break;
        }
            
        default:
            break;
    }
}

void EffectLayer::reorganizeIndicatorPos(ItemEffectIndicator* finishedIndicator)
{
    Vector<ItemEffectIndicator*>::iterator it = std::find(mItemEffectList.begin(), mItemEffectList.end(), finishedIndicator);
    if(it == mItemEffectList.end()){
        return;
    }
    
    Vector<ItemEffectIndicator*>::iterator backIt = it+1;
    for(;backIt!=mItemEffectList.end();backIt++){
        ItemEffectIndicator* indicator = *backIt;
        indicator->setPositionX(indicator->getPositionX() - 48);
    }
    
    mItemEffectList.erase(it);
    finishedIndicator->removeFromParent();
}

void EffectLayer::playCsbAnime(const std::string &csbName, const std::string &animeName,
                             bool isLoop, bool removeSelf,float scale, const Vec2 &pos,
                             const std::function<void ()> endAnimeCallback)
{
    Node *csbNode = CSLoader::createNode(csbName);
    if(csbNode == nullptr) {
        log("addCsbEffectByName. cannot create the node: %s", csbName.c_str());
        return;
    }
    
    //csbNode->setAnchorPoint(Vec2(0.5, 0.5));
    csbNode->setScale(scale);
    csbNode->setPosition(pos);
    
    addNodeToScrollLayer(csbNode);
    
    // Setting the animation
    cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
    if(timeline == nullptr) {
        log("addCsbAnimeByName:. cannot create the timeLine: %s", csbName.c_str());
        return;
    }
    
    csbNode->runAction(timeline);
    
    int animeID = mAnimeID;
    
    // Save the timeline
    mTimelineMap.insert(animeID, timeline);
    mCsbNodeMap.insert(animeID, csbNode);
    
 //   setCsbAnimeVisible(csbName, true);		// Turn on visibility before play
    timeline->play(animeName, isLoop);
    
    std::function<void()> callback = [=](){
        if(removeSelf){
            removeCsbAnimeByID(animeID);
        }
        if(endAnimeCallback){
            endAnimeCallback();
        }
    };
    
    mCallbackMap[animeID] = callback;
    
    mAnimeID++;
}


void EffectLayer::removeCsbAnimeByID(int animeID)
{
    Node *node = mCsbNodeMap.at(animeID);
    if(node != nullptr) {
        node->removeFromParent();
    }
    
    // Erase from the map
    mTimelineMap.erase(animeID);
    mCsbNodeMap.erase(animeID);
    mCallbackMap.erase(animeID);
}

void EffectLayer::showEffect(GameRes::Particle particle, const Vec2 &pos)
{
	std::string name = GameRes::getParticleName(particle);
	bool autoRemove = GameRes::isLoopParticle(particle) == false;
	
	if(name.length() == 0) {
		return;	// nothing to do
	}
	
	
	ParticleSystem *emitter = ParticleSystemQuad::create(name);
	if(emitter == nullptr) {
		log("showEffect: fail to load particle. name=%s", name.c_str());
		return;
	}
	
	emitter->setAutoRemoveOnFinish(autoRemove);
	emitter->setPosition(pos);
	emitter->setScale(kParticleScale);
	addNodeToScrollLayer(emitter);
}

void EffectLayer::showNotEnoughEnergyAlert(){
    Vec2 uiPos(0, 200);
    Vec2 glPos = Director::getInstance()->convertToGL(uiPos);
    ViewHelper::showFadeAlert(mScreenLayer, "Not enough energy!\nCollect some more!", glPos.y, 12);
}

void EffectLayer::showDoubleStarEffect(){
    std::string csbName = "text_2coins_1.csb";
    Vec2 pos = GameWorld::instance()->getPlayer()->getPosition();
    this->playCsbAnime(csbName, "active", false, true, 0.5, pos);
}

void EffectLayer::showDeduceEnergyEffect(int energy){
    EnergyEffectNode* node = EnergyEffectNode::create();
    node->setEnergyText(energy);
    node->setPosition(Vec2(220,45));
    node->setScale(0.8);
    
    FadeOut* fadeOut = FadeOut::create(1.0);
    RemoveSelf* removeSelf = RemoveSelf::create();
    node->runAction(Sequence::create(fadeOut, removeSelf, NULL));
    addNodeToScreenLayer(node);
}

void EffectLayer::showEnemyAlert(float x, bool isTop, int id,const std::function<void()> &callback)
{
    Sprite* caution = Sprite::create("ui_caution.png");
    float posY;
    if(isTop){
        posY = Director::getInstance()->getVisibleSize().height - caution->getContentSize().height*0.3/2 - 40;
    }else{
        posY = caution->getContentSize().height*0.3/2+50;
    }
    
    Vec2 pos = Vec2(x,posY);
    caution->setScale(0.3);
    caution->setPosition(pos);
    Blink* blink = Blink::create(1.0, 3);
    auto _callback = CallFunc::create(callback);
    caution->runAction(Sequence::create(blink,_callback,NULL));
    caution->setTag(id);
    mCautionMap.insert(std::pair<int, Sprite*>(id,caution));
    addNodeToScreenLayer(caution);
}

void EffectLayer::eraseCautionById(int id){
    
//    Sprite* cautionSprite = mCautionMap[id];
    std::map<int,Sprite*>::iterator iter = mCautionMap.find(id);
    log("id: %d,cautionSprite iter->first: %d",id,iter->first);
    if(mScreenLayer->getChildByTag(iter->first)){
        Sprite* cautionSprite = iter->second;
        cautionSprite->removeFromParent();
    }
    int n = (int) mCautionMap.erase(id);
}

void EffectLayer::eraseAllCaution(){
    std::map<int,Sprite*>::iterator iter;
    //log("caution map size: %d",(int)mCautionMap.size());
    for(iter = mCautionMap.begin();iter!=mCautionMap.end();){
       // log("caution id: %d",iter->first);
        if(mScreenLayer->getChildByTag(iter->first)){
            Sprite* sprite = iter->second;
            sprite->removeFromParent();
        }
        mCautionMap.erase(iter++);
    }
}

void EffectLayer::pauseAllCaution(){
    std::map<int,Sprite*>::iterator iter;
    log("caution map size: %d",(int)mCautionMap.size());
    for(iter = mCautionMap.begin();iter!=mCautionMap.end();iter++){
        if(mScreenLayer->getChildByTag(iter->first)){
            log("caution id: %d",iter->first);
            Sprite* sprite = iter->second;
            sprite->pause();
        }
    }
}

void EffectLayer::resumeAllCaution(){
    std::map<int,Sprite*>::iterator iter;
    for(iter = mCautionMap.begin();iter!=mCautionMap.end();iter++){
        if(mScreenLayer->getChildByTag(iter->first)){
            Sprite* sprite = iter->second;
            sprite->resume();
        }
    }
}

void EffectLayer::setOffsetY(float y)
{
	mOffsetY = y;
	mScrollLayer->setPositionY(-mOffsetY);
}


void EffectLayer::addStarToPlayer(int amount)
{
	GameSound::playSound(GameSound::Star2);
	GameWorld::instance()->collectCoin(amount);
	
	mStarCollected -= amount;
	
	log("Adding Star to player: mStarCollected=%d", mStarCollected);
}

void EffectLayer::addCandyToPlayer(int amount)
{
	GameSound::playSound(GameSound::Potion);
	GameWorld::instance()->collectCandy();
	
	mCandyCollected -= amount;

	log("Adding Candy to player: candyCollected=%d", mCandyCollected);

}

void EffectLayer::showCollectManyStarEffect(const std::vector<int> &amountList, const Vec2 &startPos)
{
	for(int amount : amountList) {
		showCollectStarEffect(amount, startPos);
	}
}

void EffectLayer::showCollectManyStarEffect(int totalAmount, const Vec2 &startPos)
{
	for(int i=0; i<totalAmount; i++) {
		float offsetX = RandomHelper::random_real(-5.0f, 5.0f) * 6;
		float offsetY = RandomHelper::random_real(-5.0f, 5.0f) * 6;
		
		Vec2 pos = startPos + Vec2(offsetX, offsetY);
		
		showCollectStarEffect(1, pos);
	}
}


void EffectLayer::showCollectStarEffect(int amount, const Vec2 &startPos)
{
	mStarCollected += amount;
	
	std::string image = "guiImage/ingame_ui_coin.png";
	float scale = 0.6;
	Vec2 toPos = GameWorld::instance()->getGameUILayer()->getStarCounterPosition();
	
	showCollectItemEffect(image, scale, startPos, toPos, [&, amount]{
		addStarToPlayer(amount);
	});
}



void EffectLayer::showCollectManyCandyEffect(int totalAmount, const Vec2 &startPos)
{
	for(int i=0; i<totalAmount; i++) {
		float offsetX = RandomHelper::random_real(-5.0f, 5.0f) * 6;
		float offsetY = RandomHelper::random_real(-5.0f, 5.0f) * 6;
		
		Vec2 pos = startPos + Vec2(offsetX, offsetY);
		
		showCollectCandyEffect(1, pos);
	}
}

void EffectLayer::showCollectCandyEffect(int amount, const Vec2 &startPos)
{
	mCandyCollected += amount;
	
	std::string image = "guiImage/item_candystick.png";
	float scale = 0.4;
	Vec2 toPos = GameWorld::instance()->getGameUILayer()->getCandyCounterPosition();
	
	showCollectItemEffect(image, scale, startPos, toPos, [&, amount]{
		addCandyToPlayer(amount);
	});
}

void EffectLayer::showCollectItemEffect(const std::string &image,
										float scale,
										const Vec2 &startPos,
										const Vec2 &toPos,
										const std::function<void()> &callback)
{
	// Screen position From where To where
	Vec2 fromPos = toScreenPosition(startPos);	// increase Y a bit make effect better
	
	// Create the sprite
	Sprite *sprite = Sprite::create(image);
	sprite->setScale(scale);
	sprite->setPosition(fromPos);
	addNodeToScreenLayer(sprite);
	
	//log("starEffect: from=%s to=%s", POINT_TO_STR(fromPos).c_str(), POINT_TO_STR(toPos).c_str());
	
	float animationDuration = RandomHelper::random_real(0.5f, 0.9f);
	
	ActionInterval *visualAction = MoveTo::create(animationDuration, toPos);
	//ActionInterval *visualAction = AnimationHelper::getCurveAction(fromPos,
	//								toPos, animationDuration);
	Action *finalAction;
	RemoveSelf *removeAction = RemoveSelf::create();
	
	if(callback) {
		CallFunc *callFunc = CallFunc::create(callback);
		finalAction = Sequence::create(visualAction, callFunc, removeAction, nullptr);
	} else {
		finalAction = Sequence::create(visualAction, removeAction, nullptr);
	}
	
	sprite->runAction(finalAction);
}

void EffectLayer::showCollectCapsuleEffect(int itemEffectType, const Vec2 &callerPos,
										   const std::function<void()> &callback)
{
	std::string image = GameRes::getCapsuleButtonImageName(itemEffectType);

	// No effect if image is empty, just do the callback
	if(image == "") {
		log("collectCapsule: image is empty, value=%d", itemEffectType);
		if(callback) {
			callback();
		}
		return;
	}
	
	Vec2 startPos = toScreenPosition(callerPos + Vec2(0, 40));	// increase Y a bit make effect better
	
	// Create the sprite
	Sprite *sprite = Sprite::create(image);
	sprite->setScale(0.4f);
	sprite->setPosition(startPos);
	addNodeToScreenLayer(sprite);
	
	
	float animationDuration = 0.1f;
	
	ActionInterval *visualAction;
	if(ItemEffect::isActiveable((ItemEffect::Type) itemEffectType) == false) {
		visualAction = AnimationHelper::getScaleUpAction(2, animationDuration-0.05,0.05);
	} else {
		Vec2 toPos = Vec2(VisibleRect::right().x-30, 30);	// Right Bottom Corner: the capsule button position
		visualAction = AnimationHelper::getCurveAction(startPos, toPos, animationDuration);
	}

	Action *finalAction;
	RemoveSelf *removeAction = RemoveSelf::create();
	
	if(callback) {
		CallFunc *callFunc = CallFunc::create(callback);
		finalAction = Sequence::create(visualAction, callFunc, removeAction, nullptr);
	} else {
		finalAction = Sequence::create(visualAction, removeAction, nullptr);
	}
	
	sprite->runAction(finalAction);
}

Vec2 EffectLayer::toScreenPosition(const Vec2 &mapPosition)
{
	Vec2 result = mapPosition;
	
	result.y -= mOffsetY;
	
	return result;
}

void EffectLayer::showSlowEffect()
{
	Vec2 pos = Vec2(VisibleRect::center().x, 50);
	showScreenParticle("particle_slowapply.plist", pos);
}

void EffectLayer::showScreenParticle(const std::string &particle, const Vec2 &position)
{
	ParticleSystemQuad *particleSystem = ParticleSystemQuad::create(particle);
	if(particleSystem == nullptr) {
		return;
	}
	
	
	particleSystem->setPosition(position);
	particleSystem->setAutoRemoveOnFinish(true);
	
	
	addNodeToScreenLayer(particleSystem);
	
}


void EffectLayer::runAddScoreAnimation(const int &score, const Vec2 &startPos,
									   const std::function<void ()> &callback,
									   const float &duration)
{
	int fontSize = 15;
	// Create the Text
	std::string str = StringUtils::format("+%d", score);
	ui::Text *text = ViewHelper::createOutlineText(str, fontSize,
								Color3B::WHITE, 2, Color3B::BLACK);
	text->setGlobalZOrder(kFrontEffectZOrder);
	text->setPosition(startPos);
	addChild(text);
	
	// Run the Animation
	
	ActionInterval *curveMove = AnimationHelper::getCurveAction(
						startPos, mScorePosition, duration, 1);
	
	CallFunc *callbackAction = callback == nullptr ? nullptr : CallFunc::create(callback);
	
	RemoveSelf *removeSelf = RemoveSelf::create();

	
	
	Action *action = Sequence::create(curveMove, removeSelf, callbackAction, nullptr);
	
	
	text->runAction(action);
	
}






//
//void EffectLayer::showCollectManyStarEffect(int totalAmount, const Vec2 &startPos)
//{
//	for(int i=0; i<totalAmount; i++) {
//		float offsetX = RandomHelper::random_real(-5.0f, 5.0f) * 6;
//		float offsetY = RandomHelper::random_real(-5.0f, 5.0f) * 6;
//		
//		Vec2 pos = startPos + Vec2(offsetX, offsetY);
//		
//		showCollectStarEffect(1, pos);
//	}
//}
//

void EffectLayer::runAddCoinAnimation(const Vec2 &startPos,
									  const std::function<void ()> &callback,
									  const float &duration)
{
	
//	int fontSize = 15;
//	// Create the Text
//	std::string str = StringUtils::format("+%d", score);
//	ui::Text *text = ViewHelper::createOutlineText(str, fontSize,
//												   Color3B::WHITE, 2, Color3B::BLACK);

	Sprite *sprite = Sprite::create("guiImage/ingame_ui_coin.png");
	sprite->setScale(0.8f);
	sprite->setGlobalZOrder(kFrontEffectZOrder);
	sprite->setPosition(startPos);
	addChild(sprite);
	
	// Run the Animation	
	ActionInterval *curveMove = AnimationHelper::getCurveAction(
									startPos, mCoinPosition, duration, 1);
	
	CallFunc *callbackAction = callback == nullptr ? nullptr : CallFunc::create(callback);
	
	RemoveSelf *removeSelf = RemoveSelf::create();
	
	
	
	Action *action = Sequence::create(curveMove, removeSelf, callbackAction, nullptr);
	sprite->runAction(action);

	
	
//	std::string image = "guiImage/ingame_ui_coin.png";
//	float scale = 0.6;
//	Vec2 toPos = GameWorld::instance()->getGameUILayer()->getStarCounterPosition();
//	
//	showCollectItemEffect(image, scale, startPos, toPos, [&, amount]{
//		addStarToPlayer(amount);
//	});
}

#pragma mark - SpeedUp
void EffectLayer::setupWindEffect()
{
	if(mScreenLayer == nullptr) {
		return;
	}
	
	
	mWindEffect = Sprite::create("background/ingame_wind.png");
	mWindEffect->setOpacity(0);	// not visible
	mWindEffect->setPosition(VisibleRect::center());
	mScreenLayer->addChild(mWindEffect);
}

void EffectLayer::showWindEffect()
{
	if(mWindEffect == nullptr) {
		return;
	}
	
	
	FadeTo *fadeIn = FadeTo::create(0.4, 255);
	DelayTime *delay = DelayTime::create(0.5);
	FadeTo *fadeOut = FadeTo::create(0.1, 0);
	
	
	Sequence *seq = Sequence::create(fadeIn, delay, fadeOut, nullptr);
	mWindEffect->runAction(seq);

}

void EffectLayer::hideWindEffect()
{
	if(mWindEffect == nullptr) {
		return;
	}
	
	FadeTo *fadeOut = FadeTo::create(0.4, 0);
	mWindEffect->runAction(fadeOut);
}


void EffectLayer::showConsumeCoinEffect(const Vec2 &pos)
{
	AnimeNode *animeNode = AnimeNode::create();
	
	animeNode->setup("particle_consume_coin.csb");
	animeNode->setScale(0.5f);
	animeNode->setStartAnime("active", false, true);
	animeNode->setPosition(pos);

	mScrollLayer->addChild(animeNode);
}


std::string EffectLayer::statInfo()
{
	std::string info = "";
	info += StringUtils::format("effect=%ld/%ld/%ld", getChildrenCount(),
							   mScrollLayer->getChildrenCount(),
							   mScreenLayer->getChildrenCount());
	
	info += StringUtils::format(" effData=%ld/%ld/%ld/%ld",
									mCautionMap.size(), mTimelineMap.size(),
									mCsbNodeMap.size(), mItemEffectList.size()
								);

	return info;
}
