//
//  Mastery.cpp
//  GhostLeg

//
//Maximum Storage			inital = 100
//Increases maximum resource storage.
//10
//+5
//Resource Generator		initial = 2
//Increases resource regeneration per second.
//10
//+0.5
//Resource Refinery			initial = 15
//Reduces resource spending cost.
//10
//-0.5
//Research Lab				initial = 0
//Gives a small chance to get double stardust.
//10
//+3%

//
//  Created by Ken Lee on 16/5/2016.
//
//

#include "Mastery.h"
#include "StringHelper.h"

MasteryData *MasteryData::create()
{
	MasteryData *data = new MasteryData();
	
	data->autorelease();
	
	return data;
}

std::string MasteryData::toString()
{
	std::string result = "";
	
	result += "name=" + getName();
	result += " desc=" + getInfo();
	result += " maxLv=" + INT_TO_STR(getMaxLevel());
	result += "\n";
	
	for(int i=0; i<getMaxLevel(); i++) {
		int level = i;
		float value = getLevelValue(level);
		int price = getUpgradePrice(level);
		
		char tempstr[50];
		sprintf(tempstr, "level=%3d value=%5.2f price=%5d hasPercent=%d\n",
								level, value, price, mPercentFlag);
		result += tempstr;
	}
	
	return result;
}

float MasteryData::getLevelValue(int level)
{
	if(level < 0) {
		return 0;							
	}else if(level >= mLevelValue.size()) {	 // prevent out of bound
		level = (int)(mLevelValue.size() - 1);
	}
	
	return mLevelValue[level];
}


void MasteryData::setLevelValue(int level, float value)
{
	if(level < 0 || level >= mLevelValue.size()) {
		return;
	}
	mLevelValue[level] = value;
}

int MasteryData::getUpgradePrice(int level)
{
	if(level <= 0) {
		return 0;
	}else if(level >= mLevelPrice.size()) {
		level = (int)(mLevelPrice.size() - 1);
	}
	
	return mLevelPrice[level];
}

void MasteryData::reset()
{
	mLevelPrice.clear();
	mLevelValue.clear();
}

void MasteryData::addLevelValue(float value)
{
	mLevelValue.push_back(value);
}

void MasteryData::addLevelPrice(int price)
{
	mLevelPrice.push_back(price);
}

std::string MasteryData::getDisplayInfo(int level)
{
//    std::vector<std::string> tokens = aurora::StringHelper::split(mInfo, '#');
//    if(tokens.size()==1){
//        return tokens[0] + INT_TO_STR(level);
//    }else if(tokens.size()>1){
//        std::string result = tokens[0] + INT_TO_STR(level) + tokens[1];
//        return result;
//    }else{
//        return mInfo;
//    }
	float firstValue = getLevelValue(0);

	float value = getLevelValue(level);
    float valueDiff = std::abs(value - firstValue);
	float diffPercent = (firstValue == 0) ? 0 : roundf(valueDiff * 1000 / firstValue) / 10 ;
	
	//log("getDisplayInfo: masteryID=%d value=%f diff=%f percent=%f", mMasteryID, value, valueDiff, diffPercent);
	
	std::string valueString = aurora::StringHelper::formatDecimal(value);
	std::string valueDiffString = aurora::StringHelper::formatDecimal(valueDiff);
	std::string valueDiffPercentStr = aurora::StringHelper::formatDecimal(diffPercent);
	
	
	//log("getDisplayInfo: value=%s diff=%s diffp=%s", valueString.c_str(), valueDiffString.c_str(), valueDiffPercentStr.c_str());
    std::string info = mInfo;
	aurora::StringHelper::replaceString(info, "#diffp", valueDiffPercentStr);
    aurora::StringHelper::replaceString(info, "#diff", valueDiffString);
    aurora::StringHelper::replaceString(info, "#", valueString);
    
    return info;
}

std::string PlayerMastery::toString()
{
	std::string result = "";
	
	result += "mastery=" + INT_TO_STR(getMasteryID());
	result += " level=" + INT_TO_STR(getMasteryLevel());
	
	return result;
}

void PlayerMastery::parse(const std::string &string)
{
	int mastery;
	int level;
	
	sscanf(string.c_str(), "mastery=%d level=%d", &mastery, &level);
	
	
	setMasteryID(mastery);
	setMasteryLevel(level);	
}
