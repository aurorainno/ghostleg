//
//  Star.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/5/2016.
//
//

#ifndef Star_hpp
#define Star_hpp

#include <stdio.h>
#include "Collectable.h"
#include "CommonMacro.h"

class Star : public Collectable
{
public:
	
#pragma mark - Public static Method
    static const std::string kSmallStarCsbFile;
    static const std::string kMediumStarCsbFile;
    static const std::string kLargeStarCsbFile;
	
#pragma mark - Public Method and Properties
    enum StarType{
        SmallStar,
        MediumStar,
        LargeStar,
    };
    
    CREATE_FUNC_ARG(Star,StarType);
    Star(StarType type);
	
	virtual bool init();	// virtual because different derived class have different init logic
	virtual bool use();
 
	Item::Type parseStarType(StarType type);
	
private:
    bool canTriggerDoubleStar();
    void showDoubleStarEffect();
    StarType mStarType;
	
};

#endif /* Star_hpp */
