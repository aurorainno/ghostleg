//
//  PlayerLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#include "ModelLayer.h"
#include "Constant.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "Player.h"
#include "Enemy.h"
#include "NPC.h"
#include "Item.h"
#include "Star.h"
#include "PackageBox.h"
#include "EnemyData.h"
#include "CommonMacro.h"
#include "ViewHelper.h"
#include "GameScene.h"
#include "AnimationHelper.h"
#include "EnemyFactory.h"
#include "ChaserEnemy.h"
#include "StageManager.h"
#include "StageData.h"
#include "AttackBehaviour.h"
#include "BulletBehaviour.h"
#include "TimeMeter.h"
//
const int kZOrderNpc = 101;
const int kZOrderObstacle = 90;
const int kZOrderChaser = 95;
const int kZOrderEnemy = 100;			// using enemy.plist
const int kZOrderPlayer = 200;			// using player.plist
const int kZOrderBullet = 300;
const int kZOrderItem = 99;			// using item.plist
const int kZOrderPackage = 110;


//bool mHasDefend;			// true if there are any blocking
//int mBulletBlocker;			// number of blocker for the bullet
//int mLaserBlocker;			// number of blocker for the laser
//int mBombBlocker;			// number of blocker for the bomb
//int mChanceBlocking;		// Preventing block

//
ModelLayer::ModelLayer()
: mModelList()
, mItemList()
, mNpcList()
, mPlayer(nullptr)
, mChaserEnemy(nullptr)
, mMotion(nullptr)
, mHasDefend(false)
, mEnemyBlockerMap()
, mEnemyBlockingChance(0)
{
	
}

bool ModelLayer::init()
{
	Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	
	// log("World size=%s", StringHelper::strSize(size).c_str());
	
	bool isOk = LayerColor::initWithColor(Color4B(0, 0, 0, 0), size.width, size.height);
	if (isOk == false)
	{
		return false;
	}


	// Add the Player
	Player *model = Player::create();
	addPlayer(model);
	setupInitialPlayerPos();
	
	
	
	// setupMotion
	// setupChaserEnemy();

	
	return true;
}

void ModelLayer::reset()
{
	// Remove the item
	removeAllItems();
	
	// Clear NPC
	clearNPC();
	
	
	// Clean up the current model list
	for(Enemy *enemy : mModelList) {
		if(enemy != nullptr) {
			removeChild(enemy);
		}
	}
	mModelList.clear();
	
	
	// Clean up player
	if(getPlayer() != nullptr) {
		getPlayer()->removeFromParent();
		setPlayer(nullptr);
	}
	
	
	// Reset the Status applied in the EnemyFactory
	EnemyFactory::instance()->resetDefaultEffectStatus();
	
	//
	
	// Recreate the player
	Player *model = Player::create();
	addPlayer(model);
	setupInitialPlayerPos();

	// Reset the Enemy
	resetChaserEnemy();
	
	//
	//setupMotionStreak();
}


void ModelLayer::addPlayer(Player *player)
{
	
	addChild(player, kZOrderPlayer);
    //addModel(player, kZOrderPlayer);
	setPlayer(player);
}

void ModelLayer::resetPlayer()
{
	if(mPlayer != NULL) {
		setupInitialPlayerPos();
        mPlayer->resetPlayerEnergy();
	}
}

void ModelLayer::addEnemy(Enemy *enemy)
{
	if(enemy == nullptr) {
		return;
	}
	enemy->modelWillBeAdded();
	
    int zOrder;
    if(enemy->isObstacle()){
        zOrder = kZOrderObstacle;
    }else if (dynamic_cast<BulletBehaviour*>(enemy->getBehaviour())){
        zOrder = kZOrderBullet;
    }else{
        zOrder = kZOrderEnemy;
    }
	
	addModel(enemy, zOrder);
	
	//if(enemy->shou)
}

void ModelLayer::removeModel(Enemy *model)
{
	mModelList.eraseObject(model, true);
	removeChild(model);
}

void ModelLayer::addModel(Enemy *model)
{
	addChild(model);
	mModelList.pushBack(model);
}

void ModelLayer::addModel(Enemy *model,int zOrder)
{
    addChild(model,zOrder);
    mModelList.pushBack(model);
}


void ModelLayer::setEnemyPosition(Enemy *enemy, int vertLineIdx)
{
	std::vector<int> linePosArray = GameWorld::instance()->getMap()->getVertLines();
	
	float posX = linePosArray[vertLineIdx];
	
	enemy->setPosition(Vec2(posX, 600));
}


void ModelLayer::setupInitialPlayerPos()
{
	Player *player = getPlayer();
	
	std::vector<int> linePosArray = GameWorld::instance()->getMap()->getVertLines();
	
	float posX = linePosArray[1];
	
	player->reset();
	player->setInitialPosition(Vec2(posX, 100));
	
	
}


void ModelLayer::update(float delta)
{
	TSTART("ModelLayer.updateModel");
	for(Enemy *model : mModelList) {
		if(model != NULL) {
			model->update(delta);
		}
	}
	TSTOP("ModelLayer.updateModel");
	
	if(mPlayer != nullptr) {
		mPlayer->update(delta);
	}
	
	if(mChaserEnemy != nullptr) {
		mChaserEnemy->update(delta);
	}
	
	
	TSTART("updateItem");
	for(int i=0; i<mItemList.size(); i++) {
		Item *item = mItemList.at(i);
		
		if(item != NULL) {
			item->update(delta);
		}
	}
	TSTOP("updateItem");
	
	
    cleanupInactiveEnemyAndItem();
	
    // removeUsedItem();		// ken: bugs?? 
}



void ModelLayer::setOffsetY(float y)
{
	Vec2 pos = getPosition();
	
	pos.y = -y;
	
	setPosition(pos);
}



void ModelLayer::clearNPC()
{
	for(NPC *npc : mNpcList) {
		npc->removeFromParent();
	}
	
	
	mNpcList.clear();
	
}

void ModelLayer::deactiveAllEnemy()
{
	for(int i=0; i<mModelList.size(); i++) {
		Enemy *enemy = dynamic_cast<Enemy *>(mModelList.at(i));
		if(enemy == nullptr){
			continue;
		}
		if(enemy->getResID() >= 100){	// they are obstacle
			continue;
		}
		
		enemy->changeState(Enemy::InActive);
	}
}

void ModelLayer::removePlayer() {
	if(mPlayer != nullptr) {
		removeChild(mPlayer);
		setPlayer(nullptr);
	}
}

void ModelLayer::clearEnemy()
{
	for(Enemy *enemy : mModelList) {
		removeChild(enemy);	// remove from the layer
	}
	
	mModelList.clear();
}

void ModelLayer::removeEnemy(Enemy *enemy)
{
    if(enemy->getParent()){
        enemy->removeFromParent();
    }
    
    if(mModelList.find(enemy) != mModelList.end()){
        mModelList.eraseObject(enemy);
    }
}


void ModelLayer::cleanupInactiveEnemyAndItem()
{
	// Clean Enemy
	std::vector<Enemy *> removeEnemyList;
    for(Enemy *enemy : mModelList) {
        if(enemy == nullptr){
            continue;
        }
        if(enemy->isInActive()) {
            removeChild(enemy);	// remove from the layer
			removeEnemyList.push_back(enemy);
			
        }
    }
	for(Enemy *enemy : removeEnemyList) {
		mModelList.eraseObject(enemy);
	}
	removeEnemyList.clear();
	
	
	// Remove Item
	std::vector<Item *> removeItemList;
	for(Item* item : mItemList) {
        if(item == nullptr){
            continue;
        }
        
        if(item->shouldBeCleanUp()){
            item->removeFromParent();
			removeItemList.push_back(item);
        }
    }
	for(Item *item : removeItemList) {
		mItemList.eraseObject(item);
	}
	removeItemList.clear();
	
	// NPC
	std::vector<NPC *> removeNpcList;
	for(NPC *npc : mNpcList) {
		if(npc == nullptr){
			continue;
		}
		
		if(npc->shouldBeCleanUp()){
			npc->removeFromParent();
			removeNpcList.push_back(npc);
		}
	}
	for(NPC *npc : removeNpcList) {
		mNpcList.eraseObject(npc);
	}

	removeNpcList.clear();

	
}

void ModelLayer::addEnemyList(const Vector<Enemy *> &enemyList)
{
	for(Enemy *e : enemyList) {
		addEnemy(e);
	}

//	for(int i=0; i<enemyList.size(); i++) {
//		Enemy *e = enemyList.at(i);
//		ViewHelper::createRedSpot(this, e->getPosition());
//	}

}


void ModelLayer::addNpcList(const Vector<NPC *> &npcList)
{
	for(NPC *npc : npcList) {
		addNpc(npc);
	}
}


void ModelLayer::addNpc(NPC *npc)
{
	if(npc == nullptr) {
		return;
	}
	
	addChild(npc, kZOrderNpc);
	mNpcList.pushBack(npc);
	
}

NPC *ModelLayer::getLastNpc()
{
	if(mNpcList.size() == 0) {
		return nullptr;
	}
	
	int lastIdx = (int) mNpcList.size() - 1;
	
	return mNpcList.at(lastIdx);
}


void ModelLayer::handleItemCollision()
{
	TSTART("handleItemCollision");
	// Handle item collide with enemy
	int checkCount = 0;
	int checkCount2 = 0;
	
	TSTART("handleItemEnemyCollision");
	Vector<Enemy *> enemyList = getItemCollidableEnemyList();
	for(Enemy *enemy : enemyList) {
		for(Item *item : mItemList) {
			checkCount++;
			if(item->isUsed()) {
				continue;
			}
			if(item->isWeapon() == false) {
				continue;
			}

			checkCount2++;
			if(enemy->isCollideModel(item) == false) {
				continue;
			}
			
			enemy->collideWithItem(item);
		}
	}
	//log("enemyItemCheckCount: %d/%d enemyList=%ld", checkCount, checkCount2, enemyList.size());
	TSTOP1("handleItemEnemyCollision", StringUtils::format("%d/%d", checkCount, checkCount2));
	
	// Handle item
	Player *player = getPlayer();
	Rect playerRect = player->getFinalMainBoundingBox(false);
	
	float playerY = player->getPositionY();
	int count = 0; int count2 = 0;
	for(Item *item : mItemList) {
		float itemY = item->getPositionY();
		count++;
		
		if((playerY - itemY) > 50) {
			continue;
		}
		if(item->isUsed()) {
			continue;
		}
		count2++;
		
		if(item->isCollidePlayer(playerRect)) {
			TSTART("item.use");
			item->use();
			TSTOP1("item.use", item->getName());
		}
		
		// Quick Exit for those far from player
		
		if((itemY - playerY) > 50) {
			break;
		}
	}
	//log("Number of itemChecked=%d/%d", count, count2);
	TSTOP1("handleItemCollision", StringUtils::format("%d/%d", count, count2));
}


CollisionType ModelLayer::handlePlayerCollision()	// return true if player is killed
{
	Player *player = getPlayer();

	if(handleChaserCollision()){
		return HitByChaser;
	}

	if(player->isReviving()) {
		return HitByNone;
	}
	
	if(player->isStunning()) {
		return HitByNone;
	}
	
	
	int checkCount = 0;
	int count = 0;
	
	CollisionType result = HitByNone;
	TSTART("handlePlayerCollision.model");
	Rect playerWorldRect = player->getFinalMainBoundingBox(true);
	Rect playerMapRect = player->getFinalMainBoundingBox(false);
	
	for(Enemy *enemy : mModelList) {
		count++;
		if(enemy == nullptr) {
			continue;
		}
		if(enemy->isActive() == false) {
			continue;
		}

		if(enemy->isCollidable() == false) {
			continue;
		}
		
		checkCount++;
		TSTART("model.isCollidePlayer");
		bool anyCollision = enemy->isCollideWithPlayerRect(playerMapRect, playerWorldRect);
		TSTOP("model.isCollidePlayer");
		if(anyCollision == false) {
			continue;
		}
		
		//log("Hitting Enemy [%s]", enemy->toString().c_str());
		
		// enemy is hitting player
		TSTART("isEnemyBlocked");
		BlockType blockType = isEnemyBlocked(enemy);
		TSTOP("isEnemyBlocked");
		if(blockType != BlockTypeNone) {
			//log("handlePlayerCollision: block: count=%d check=%d", count, checkCount);
			
			handleEnemyBlocking(blockType, player, enemy);
			result = HitByNone;
			break;
			//break;		// ken: 'break' because enemy is blocked and suppose no more collision happened
		}
		
		
		TSTART("collideWithPlayer.model");
		bool isKilled = enemy->collideWithPlayer(player);
		TSTOP("collideWithPlayer.model");
		if(isKilled) {
			//log("handlePlayerCollision: kill: count=%d check=%d", count, checkCount);
			// log("Player is hit by [%s]", enemy->toString().c_str());
			result = HitByEnemy;
			break;
		}
	}
	TSTOP1("handlePlayerCollision.model", StringUtils::format("#check=%d/%d", checkCount, count));
	
	//log("handlePlayerCollision: nothing: count=%d check=%d", count, checkCount);
	
	return result;
}


MovableModel *ModelLayer::getPlayerCollision()
{
	Player *player = getPlayer();
	
	if(player->isReviving()) {
		return nullptr;
	}
	
	for(int i=0; i<mModelList.size(); i++) {
		MovableModel *model = mModelList.at(i);
		
		if(model == player) {
			continue;
		}
		
		Enemy *enemy = dynamic_cast<Enemy *>(model);
		if(enemy) {
			if(enemy->isCollidable() == false) {
				continue;
			}
		}
		
		if(player->isCollide(model)) {
			return model;
		}
	}
	
	return nullptr;
}

#pragma mark - Item Operation
void ModelLayer::addItem(Item *item)
{
	item->setLocalZOrder(kZOrderItem);
	mItemList.pushBack(item);
	addChild(item);
}

void ModelLayer::addItemList(const Vector<Item *> &itemList)
{
	for(int i=0; i<itemList.size(); i++) {
		Item *obj = itemList.at(i);
		
		addItem(obj);
	}
}

void ModelLayer::removeItem(Item *item)
{
    if(item->getParent()){
        item->removeFromParent();		// remove from the layer
    }

    if(mItemList.find(item) != mItemList.end()){
        mItemList.eraseObject(item);	// remove from the list
    }
    
}

void ModelLayer::removeAllItems()
{
	auto begin = mItemList.begin();
	auto end = mItemList.end();
	
	for(auto it=begin; it != end; it++) {
		Item *item = *it;
		item->removeFromParent();
	}
	
	mItemList.clear();
}


void ModelLayer::removeUsedItem()
{
	
	auto begin = mItemList.begin();
	auto end = mItemList.end();
	
	for(auto it=begin; it != end; it++) {
		Item *item = *it;
		if(item == nullptr) {
			continue;
		}
		if(item->shouldBeCleanUp() == false) {
			continue;
		}
		
		removeItem(item);
	}
}

#pragma mark - Clean up
void ModelLayer::clearData()
{
	removeAllItems();
	clearEnemy();
	clearNPC();
	removePlayer();
}

#pragma mark - Object Information
std::string ModelLayer::info()
{
	std::string result = "";

	result += "Current Items:\n";
	
	CCVECTOR_INFO(Item, mItemList, result);
	
	result += "Current Enemies:\n";
	
	CCVECTOR_INFO(MovableModel, mModelList, result);
	
	return result;
}

#pragma mark - Pause / Resume Model
void ModelLayer::pauseAllModel()
{
	for(Enemy *enemy : mModelList) {
		if(enemy) {
			enemy->pause();
		}
	}
	if(getPlayer() != nullptr) {
		getPlayer()->pause();
	}
}

void ModelLayer::resumeAllModel()
{
	for(Enemy *enemy : mModelList) {
		if(enemy) {
			enemy->resume();
		}
	}
	if(getPlayer() != nullptr) {
		getPlayer()->resume();
	}
	
}

Vector<Enemy *> ModelLayer::getItemCollidableEnemyList()
{
	Vector<Enemy*> result;
	
	for(Enemy *enemy : mModelList) {
		if(enemy == nullptr) {
			continue;
		}
		if(enemy->canCollideItem() == false) {
			continue;
		}
		result.pushBack(enemy);
	}
	
	return result;
}

Vector<Enemy*> ModelLayer::getEnemyList()
{
    Vector<Enemy*> result;
    
    for(Enemy *enemy : mModelList) {
        if(enemy == nullptr) {
            continue;
        }else{
            result.pushBack(enemy);
        }
    }
    
    return result;
}

Vector<Enemy *> ModelLayer::getNearestEnemy(int distance, int maxCount, std::set<int> excludeSet)
{
	Vector<Enemy *> result;
	
	if(mPlayer == nullptr) {
		return result;		// Nothing
	}
	
	Vec2 playerPos = mPlayer->getPosition();
	
	for(Enemy *enemy : mModelList) {
		if(enemy == nullptr) {
			//log("debug: i=%d: not enemy", i);
			continue;
		}
        
        if(enemy->isObstacle()){
            continue;
        }
        
        
		// Find match
		if(excludeSet.find(enemy->getObjectID()) != excludeSet.end()) {
			//log("debug: enemy is excluded: %d", enemy->getObjectID());
			continue;
		}
		
		if(! enemy->isActive()) {
			continue;
		}
        
        if(enemy->getBehaviour()){
			if(enemy->getBehaviour()->getData()->getDestroyable() == false) {
                continue;
			}
        }
		
		Vec2 enemyPos = enemy->getPosition();
        log("enemyResID:%d",enemy->getResID());
        
        if(enemy->getBehaviour()->getData()->getType() == 7){
            if(enemyPos.y - playerPos.y < -100){
                continue;
            }
        }else if(enemyPos.y < playerPos.y) {
			continue;
		}
		
		float distanceBetween = mPlayer->getPosition().distance(enemyPos);
		if(distanceBetween > distance) {
			continue;
		}
		
		result.pushBack(enemy);
	}
	

	
	// Sort the result
	std::sort(result.begin(), result.end(), [playerPos](Enemy *a, Enemy *b) {
        if(a->getPosition().y<playerPos.y&&b->getPosition().y>playerPos.y){
            return false;
        }else if(a->getPosition().y>playerPos.y&&b->getPosition().y<playerPos.y){
            return true;
        }
        
		Vec2 pos1 = a->getPosition();
		float dist1 = playerPos.distance(pos1);
		
		Vec2 pos2 = b->getPosition();
		float dist2 = playerPos.distance(pos2);
		
		return dist1 < dist2;
	});
	
	
	if(maxCount >= result.size()) {
		return result;		// No need to erase
	}
	
	// Erase the extra data
	Vector<Enemy *>::iterator first = result.begin() + maxCount;
	
	result.erase(first, result.end());
	
	
	return result;
}


Vector<Collectable *> ModelLayer::getVisibleCollectableList()
{
	Vector<Collectable *> result;
	
	for(int i=0; i<mItemList.size(); i++) {
		Item *item = mItemList.at(i);
		Collectable *star = dynamic_cast<Collectable *>(item);
		if(star == nullptr) {
			continue;
		}
		
		if(! GameWorld::instance()->isRectVisible(star->getBoundingBox())) {
			continue;
		}
		
		result.pushBack(star);
	}
	
	return result;
}

Vector<Star *> ModelLayer::getStarList(){
    Vector<Star *> result;
    
    for(int i=0; i<mItemList.size(); i++) {
        Item *item = mItemList.at(i);
        Star *star = dynamic_cast<Star *>(item);
        if(star == nullptr) {
            continue;
        }
        
        result.pushBack(star);
    }
    
    return result;
}

void ModelLayer::applyEffectStatusToAllEnemy(EffectStatus status)
{
	for(Enemy *enemy : mModelList) {
		if(enemy == nullptr) {
			// log("debug: i=%d: not enemy", i);
			continue;
		}
		if(enemy->hasSetup() == false) {
			continue;
		}
		
		enemy->setEffectStatus(status);
	}
}

void ModelLayer::removeEffectStatusFromAllEnemy()
{
	for(Enemy *enemy : mModelList) {
		if(enemy == nullptr) {
			// log("debug: i=%d: not enemy", i);
			continue;
		}
		enemy->removeEffectStatus();
	}
}


Vector<Enemy *> ModelLayer::getVisualEnemyList(float upperOffset, float lowerOffset)
{
	Vector<Enemy *> result;
	
	if(mPlayer == nullptr) {
		return result;		// Nothing
	}
	
	Vec2 playerPos = mPlayer->getPosition();
	
	float topY = GameWorld::instance()->getTopMapY() + upperOffset;
	float bottomY = GameWorld::instance()->getBottomMapY() + lowerOffset;
	
	for(Enemy *enemy : mModelList) {
		if(enemy == nullptr) {
			//log("debug: i=%d: not enemy", i);
			continue;
		}
	
		Vec2 pos = enemy->getPosition();
		if(pos.y >= bottomY && pos.y <= topY) {
			result.pushBack(enemy);
		}
	}
	
	return result;
}


int ModelLayer::moveAllStarsToPlayer()
{
	Vector<Collectable *> starList = getVisibleCollectableList();
	
	int count = 0;
	
	for(Collectable *star : starList) {
		if(star->getMoveToPlayer()) {
			continue;		// No need
		}
		
		star->setMoveToPlayer(true);
		
		count++;
	}
	
	return count;
}


#pragma mark - Package handling
void ModelLayer::addPackageBox(const Vec2 &pos, std::function<void()> func)
{
	//
	PackageBox *packageBox = PackageBox::create();
	packageBox->setPosition(pos);
	addItem(packageBox);
	packageBox->setLocalZOrder(kZOrderPackage);
	packageBox->showAnimation(PackageBox::Animation::DropDown, func);
	
	
//	packageBox->setAnimationEndCallFunc(<#GameModel::Action action#>, <#std::function<void ()> func#>)
}


#pragma mark - Motion
void ModelLayer::setupMotionStreak()
{
	if(mMotion != nullptr) {
		mMotion->removeFromParent();
		mMotion = nullptr;
	}
	
	
	std::string textureName = StringUtils::format("motion/tail_009.png");
	Texture2D *image = Director::getInstance()->getTextureCache()->addImage(textureName);
	
	// float fade, float minSeg, float stroke,
	MotionStreak *motion = MotionStreak::create(1.3f, 5, 30, Color3B::WHITE, image);
	motion->setFastMode(true);
	addChild(motion);
	mMotion = motion;
	mMotion->setPosition(Vec2::ZERO);
	mMotion->setVisible(false);
}

void ModelLayer::changeMotionStyle(MotionStyle style)
{
	if(style == MotionStyle::StyleNone) {
		setMotionEnable(false);
		return;
	}
	setMotionEnable(true);
	
	// Define the tailID
	int tailID;
	if(MotionStyle::StyleMaxSpeed == style) {
		tailID = 9;
	} else if(MotionStyle::StyleSpeedUp == style) {
		tailID = 10;
	} else {
		tailID = 8;
	}
	
	
	std::string textureName = StringUtils::format("motion/tail_%03d.png", tailID);
	Texture2D *image = Director::getInstance()->getTextureCache()->addImage(textureName);
	mMotion->setTexture(image);
}


void ModelLayer::setMotionEnable(bool flag)
{
	if(mMotion) {
		mMotion->setVisible(flag);
		Vec2 pos = mPlayer->getPosition();
		mMotion->setPosition(pos);
	}
}
void ModelLayer::updateMotion(float delta)
{
	if(mMotion) {
//		if(mMotion->isVisible() == false) {
//			return;
//		}
		ViewHelper::createDebugSpot(mMotion, Vec2::ZERO, Color4B::RED, 10);
		mMotion->setPosition(mPlayer->getPosition());
		mMotion->update(delta);
		
	}
}



#pragma mark - Chaser Enemy
void ModelLayer::setupChaserEnemy()
{
	
	// Add the Chaser
	
	resetChaserEnemy();
}

void ModelLayer::resetChaserPosition()
{
	// Define the default position of the Chaser Enemy
	// float defaultOffset = 60; //kCameraPosition - 100;		// Number of pixel showing
	
	float defaultOffset = 150;
	
	Vec2 playerPos = getPlayer()->getPosition();
	Vec2 enemyPos = playerPos - Vec2(0, defaultOffset);
	//Vec2 enemyPos = playerPos - Vec2(0, 500);
	
	mChaserEnemy->setPosition(enemyPos);
}

void ModelLayer::resetChaserEnemy()
{
	// Remove the old one before add the new
	//		if keeping the same object, it cause a strange bug: (Back to Main -> Play -> invisible)
	if(mChaserEnemy != nullptr) {
		mChaserEnemy->removeFromParent();
		mChaserEnemy = nullptr;
	}
	
	
	mChaserEnemy = ChaserEnemy::create();
	//mChaserEnemy = EnemyFactory::instance()->createEnemy(1000);
	
	addChild(mChaserEnemy,kZOrderChaser);
	
	
	// Set state to not Shown
	mChaserEnemy->resetState();
	
	// Reset to a position near player
	resetChaserPosition();
	//setChaserEnemyInActive();
	
	
	// Setting the Chaser Attribute
	StageData *stageData = StageManager::instance()->getCurrentStageData();
	if(stageData != nullptr) {
		setChaserAttribute(stageData->getChaserAttribute());
	}
}

void ModelLayer::setChaserEnemyActive()
{
	resetChaserPosition();
	mChaserEnemy->changeToActive();
}

void ModelLayer::setChaserEnemyInActive()
{
	mChaserEnemy->changeToInActive();
}

bool ModelLayer::handleChaserCollision()
{
	if(mChaserEnemy == nullptr) {
		return false;
	}
	
	if(mChaserEnemy->isActive() == false) {
		return false;
	}
	
	float hitPos = getPlayer()->getPositionY() - 10;
	if(mChaserEnemy->getPositionY() >= hitPos) {
        mChaserEnemy->getBehaviour()->onCollidePlayer(GameWorld::instance()->getPlayer());
		return true;
	}
	
	return false;
}

void ModelLayer::setChaserAttribute(const std::map<std::string, int> &attribute)
{
	if(mChaserEnemy == nullptr) {
		return;
	}
	
	mChaserEnemy->setSpeedAttribute(attribute);
	
	//log("Debug: %s", mChaserEnemy->info)
}

void ModelLayer::adjustChaserPosition()
{
	if(mChaserEnemy == nullptr) {
		return;
	}
	
	float threshold = 100;	// make the chaser not too close			// old setting
	//float threshold = 400;
	
	Vec2 playerPos = getPlayer()->getPosition();
	
	Vec2 chaserPos = mChaserEnemy->getPosition();
	
	if((playerPos.y - chaserPos.y) > threshold) {
		return;		// No need
	}
	
	chaserPos = playerPos - Vec2(0, threshold);
	
	mChaserEnemy->setPosition(chaserPos);
}

void ModelLayer::adjustChaserAppearOnBottom()
{
    if(mChaserEnemy == nullptr) {
        return;
    }
    
    float screenBtmPosY = GameWorld::instance()->getBottomMapY();
    float chaserPosY = mChaserEnemy->getPositionY();
    
    if(chaserPosY > screenBtmPosY){
        return;
    }else{
        mChaserEnemy->setPositionY(screenBtmPosY);
    }
    
}

void ModelLayer::applyEffectStatusToChaser(EffectStatus status)
{
    mChaserEnemy->setEffectStatus(status);
}

void ModelLayer::removeEffectStatusFromChaser()
{
    mChaserEnemy->removeEffectStatus();
}



void ModelLayer::increaseChaserSpeed()
{
	if(mChaserEnemy == nullptr) {
		return;
	}
	
	mChaserEnemy->increaseSpeed();
}



int ModelLayer::getBlockerCount(BlockType blockType)
{
	if(mEnemyBlockerMap.find(blockType) == mEnemyBlockerMap.end()) {
		return 0;
	}
	
	return mEnemyBlockerMap.at(blockType);
}

float ModelLayer::getBlockAttribute(const std::map<GameplaySetting::Attribute, float> &attributeMap,
									GameplaySetting::Attribute attribute)
{
	if(attributeMap.find(attribute) == attributeMap.end()) {
		return 0;
	}
	
	return attributeMap.at(attribute);
}

#pragma mark - Defend Mechanic
void ModelLayer::setupDefendAttribute(GameplaySetting *playSetting)
{
	// Setting the internal Data
	mEnemyBlockerMap.clear();
	mHasDefend = false;
	float value;
    BlockType blockType = BlockTypeNone;
	
	// Enemy Blocker
	value = playSetting->getAttributeValue(GameplaySetting::Attribute::ChanceEnemyBlocker);
	if(value > 0) {
		mHasDefend = true;
		mEnemyBlockingChance = (int) (value);
        blockType = BlockTypeEnemy;
	}
	
	// Laser Blocker
	value = playSetting->getAttributeValue(GameplaySetting::Attribute::LaserBlocker);
	if(value > 0) {
		mHasDefend = true;
		mEnemyBlockerMap[BlockTypeLaser] = (int) value;		// number of count
        blockType = BlockTypeLaser;
	}
	
	// Bomb Blocker
	value = playSetting->getAttributeValue(GameplaySetting::Attribute::BombBlocker);
	if(value > 0) {
		mHasDefend = true;
		mEnemyBlockerMap[BlockTypeBomb] = (int) value;		// number of count
        blockType = BlockTypeBomb;
	}
	
	// Bullet Blocker
	value = playSetting->getAttributeValue(GameplaySetting::Attribute::BulletBlocker);
	if(value > 0) {
		mHasDefend = true;
		mEnemyBlockerMap[BlockTypeBullet] = (int) value;		// number of count
        blockType = BlockTypeBullet;
	}
	
	
	// Setup Visual Indicator in the Game GUI
	// TODO!!!
    if(blockType > BlockTypeNone){
        setupBlockUI(blockType);
    }
	
}

void ModelLayer::testDefendAttribute()
{
	GameplaySetting *setting = GameplaySetting::create();
	setting->setAttributeValue(GameplaySetting::Attribute::LaserBlocker, 1);
	setting->setAttributeValue(GameplaySetting::Attribute::BulletBlocker, 1);
	setting->setAttributeValue(GameplaySetting::Attribute::BombBlocker, 1);
	setting->setAttributeValue(GameplaySetting::Attribute::ChanceEnemyBlocker, 10);
	
	setupDefendAttribute(setting);
}


// returnValue: true if blocking happened
BlockType ModelLayer::isEnemyBlocked(Enemy *enemy)
{
	if(enemy == nullptr) {
		return BlockTypeNone;
	}
	
	if(mHasDefend == false) {
		return BlockTypeNone;
	}
	
	ObjectTag tag = enemy->getObjectTag();
	
	// Different Case
	
	// General Enemy Blocking
	if(ObjectEnemy == tag) {
		if(mEnemyBlockingChance <= 0) {
			return BlockTypeNone;
		}
		int randValue = RandomHelper::random_int(1, 100);
		return randValue <= mEnemyBlockingChance ? BlockTypeEnemy : BlockTypeNone;
	}
	
	// Other Attack projectile
	BlockType blockType;
	if(ObjectLaser == tag) { blockType = BlockTypeLaser; }
	else if(ObjectBullet == tag) { blockType = BlockTypeBullet; }
	else if(ObjectBomb == tag) { blockType = BlockTypeBomb; } //
	else { blockType = BlockTypeNone; }
	
	if(blockType == BlockTypeNone) {
		return BlockTypeNone;
	}
	
	int blockingCount = getBlockerCount(blockType);
	if(blockingCount <= 0) {
		return BlockTypeNone;
	}
	
	return blockType;
	
//	// Lazer, Bullet , Bomb
//	
//	else if(ObjectLazer == tag) {
//		
//		int blockingCount =
//	}
	
	return BlockTypeNone;
}


void ModelLayer::showBlockAnimation()
{
	std::string csbName = "particle_block.csb";
	getPlayer()->addCsbAnimeByName(csbName, GameModel::ObjectNode, 0.5,Vec2::ZERO);
	getPlayer()->playCsbAnime(csbName, "active", false, true);
}


void ModelLayer::handleEnemyBlocking(BlockType blockType,
									 Player *player, Enemy *enemy)
{
	if(enemy == nullptr) {
		return;
	}

	if(blockType != BlockTypeEnemy) {
		reduceBlockerCount(blockType);
	}

	// log("try to block enemy [%s]", enemy->toString().c_str());
	player->setReviving(0.5f, false);		// not that good!!
	
	// Play One time animation
	showBlockAnimation();
}

void ModelLayer::reduceBlockerCount(BlockType blockType)
{
	int count = getBlockerCount(blockType);
	if(count <= 0) {
		return;
	}
	
	int newCount = count - 1;
	if(newCount < 0) { newCount = 0; }
		
	mEnemyBlockerMap[blockType] = newCount;
	
	
	updateBlockUI(blockType, newCount);
}

void ModelLayer::setupBlockUI(BlockType blockType)
{
	// TODO
    GameWorld::instance()->getGameUILayer()->showBlockCounter(blockType, mEnemyBlockerMap[blockType]);
    
}

void ModelLayer::updateBlockUI(BlockType blockType, int currentCount)
{
	// TODO
	//		note: tell the GameUI to update the icon value
	//				if currentCount is zero, make the indicator invisible
    if(blockType == BlockTypeEnemy){
        return;
    }
    
    if(currentCount <= 0){
        GameWorld::instance()->getGameUILayer()->hideBlockCounter();
        return;
    }
    
    GameWorld::instance()->getGameUILayer()->showBlockCounter(blockType, currentCount);
}


#pragma mark - Setup ModelLayer with GameplaySetting
void ModelLayer::setupWithGameplaySetting(GameplaySetting *setting)
{
	// Defend Attribute
	setupDefendAttribute(setting);

	
	
	
	// For Testing
	//setting->setAttributeValue(GameplaySetting::Attribute::TravelReward, 50);
	// setting->setAttributeValue(GameplaySetting::Attribute::ChaserSlowRatio, 0.5);
	//setting->setAttributeValue(GameplaySetting::Attribute::KillEnemyReward, 5);
}

std::string ModelLayer::statInfo()
{
	return StringUtils::format("model=%ld modelData=%ld/%ld/%ld",
							   getChildrenCount(), mModelList.size(),
							   mNpcList.size(), mItemList.size());
}
