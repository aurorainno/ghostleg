//
//  Enemy.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2016.
//
//

#ifndef Enemy_hpp
#define Enemy_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "MovableModel.h"
#include "EnemyBehaviour.h"
#include "EnemyData.h"

USING_NS_CC;

class Player;
class Item;

class Enemy : public MovableModel
{
public:
	enum State {
		NotShown,		// Not appear, no collision or any action
		Idle,			// Appear, but no action (e.g attack or move)
		Active,			// Appear, will move or attack (rename as Waked)
		InActive,		// Waiting for destroy
		BeHit,			// Being hit
        ChangeToMoney,   // changing to money
        Destroying
	};
	
#pragma mark - Public static Method
	CREATE_FUNC(Enemy);
	
	static std::string getCsbFilename(int resID);
	
#pragma mark - Public Method and Properties
	Enemy();
	~Enemy();
	
	CC_SYNTHESIZE(State, mState, State);
	CC_SYNTHESIZE(int, mObjectID, ObjectID);
	CC_SYNTHESIZE(EffectStatus, mEffectToAdd, EffectToAdd)
	
	CC_SYNTHESIZE_RETAIN(EnemyBehaviour *, mBehaviour, Behaviour);
	
	// Map Setting
	CC_SYNTHESIZE(float, mObjectScaleX, ObjectScaleX);
	CC_SYNTHESIZE(float, mObjectScaleY, ObjectScaleY);
	CC_SYNTHESIZE(bool, mObjectFlipX, ObjectFlipX);
	CC_SYNTHESIZE(bool, mObjectFlipY, ObjectFlipY);
	
	
	
	//
	virtual bool init();	// virtual because different derived class have different init logic
	virtual void update(float delta);
	void resetState();
	void setResource(int resID);
    bool shouldPlaySoundEffect();
	void updateVisibility();
	void modelWillVisible();
	bool shouldSetupModel();
	void modelWillBeAdded();
	void checkStartMoving();
	bool isCollidable();
	bool isActive();		// the enemy is active
    bool isInActive();
	bool canCollideItem();
    
    bool isObstacle();
	bool isStaticModel();
	
	bool collideWithPlayer(Player *player);
	bool collideWithItem(Item *item);
	
	void updateChildrenZOrder();
	
	void beHit();

	
	ObjectTag getObjectTag();
	
	
	void changeState(State state);

	void showWakeupAlert(const std::function<void()> &callback=nullptr);
	
	virtual Vec2 getFinalSpeed() const;
	
	virtual std::string toString();
	
	void updateSetDefaultDir();
	
	void showKnockoutEffect(const Vec2 &force, bool addRandom=true);
	
	bool isWolf();
	
	const EnemyData *getEnemyData();
	
	virtual std::string getName();
    
    bool getVisibility();
    
    virtual void setEffectStatus(EffectStatus status, float duration = -1);
    
    void onApplyEffectStatus(EffectStatus status)override;
    void onRemoveEffectStatus(EffectStatus status)override;
	
    virtual void onFrameEvent(Frame *frame) ;
    void onSFXFrameEvent(std::string eventKey);
    
    void playSoundWithAction(Action action) override;

	bool hasSetup();
    
private:
    CC_SYNTHESIZE(bool, mSoundPlayed, SoundPlayed);

    CC_SYNTHESIZE(int, mResID, ResID);

	CC_SYNTHESIZE(bool, mMoving, Moving);


	
	bool mIsVisible;
	bool mIsSetup;
	bool mIsShowingAlert;		// true when showing the alert
	

	
#pragma mark - State logic
	AlertType getAlertType();
	
	void changeToIdle();
	void changeToActive();
	void changeToInActive();
	void changeToBeHit();
    void changeToChangeToMoney();
	
	void startActive();
	
	void updateForBeHit(float delta);
    void updateSpeed(float delta);
	
	
	
	
	//
	void setupAttribute();
	void setupModel();
	void setupAnimation();
	void setupDirAndHitbox();
	Dir getDefaultXDir();
	
	
private:
	bool mIsAnimationSetup;


#pragma mark - Collision
public:
	bool isCollideWithPlayerRect(const Rect &mapHitbox, const Rect &worldHitbox);
};





#endif /* Enemy_hpp */
