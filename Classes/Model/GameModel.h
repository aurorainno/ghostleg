//
//  GameModel.hpp
//  GhostLeg
//
//  Created by Ken Lee on 6/5/2016.
//
//

#ifndef GameModel_hpp
#define GameModel_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "GameRes.h"
#include "Circle.h"
#include <vector>
#include <string>

// Cocos Studio
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
using namespace cocostudio::timeline;


USING_NS_CC;

class GameModel : public Node
{
public: // enum
	enum Action {
		Walk,			// Moving left and right
		Climb,			// Moving up and down
		Idle,			// Standing and do nothing
		Die,			// The model is died
		Consume,
		Invisible,
		Attack,
        Destroy,
        Jump
	};
	
	enum NodeType
	{
		ObjectNode,		// the GameModel node
		UpperEffectNode,
		LowerEffectNode,
		RootNode
	};
	
public:
	static void showHitbox(bool flag);
	static bool isShowingHitbox();
	
public:
	CREATE_FUNC(GameModel);
	
	GameModel();
	virtual ~GameModel();
	
	virtual bool init();
	CC_SYNTHESIZE(float, mRootScale, RootScale);
	CC_SYNTHESIZE_RETAIN(cocostudio::timeline::ActionTimeline *, mTimeLine, TimeLine);
	
    typedef std::function<void(const std::string &eventName, EventFrame* event)> EventFrameCallback;
    
	virtual std::string getName();
	
	virtual void setup(const std::string &csbName, Action defaultAction=Idle);
	void setAction(Action action);
	void runAnimation(const std::string &animeName, bool isLoop);
	float getDefaultScale();
	void setFace(bool toLeft);
	bool isFaceLeft();
	
	void setFlipX(bool flip);
	void setFlipY(bool flip);
	void setupScaleAndFlip(float scaleX, float scaleY, bool flipX, bool flipY);
	
	virtual Rect getBoundingBox() const override;
	Vec2 getCenterPoint();
	
	void setHitboxVisible(bool flag);
	void testHitBox();
	
	virtual void setActionManager(ActionManager *manager);
	
	virtual void setAnimationEndCallFunc(Action action, std::function<void()> func);
	virtual void setAnimationEndCallFunc(const std::string &animeName,
											std::function<void()> func);
	void pause();
	void resume();
	void stepAction();			// ken: this mainly for debug, used after pause();
	void setTimeSpeedByPercent(float percent);	// 0.5 = 50% 
	
	virtual void setOpacity(GLubyte opacity);
	
	// Visual Effect
	void addSpriteEffectByName(const std::string &spriteFrameName, bool onMainSprite, float scale=1.0f);
	void addParticleEffect(GameRes::Particle particle, const Vec2 &offset=Vec2::ZERO);
	void removeVisualEffect();
	
	void setStatusIconByName(const std::string &spriteFrameName);
	void removeStatusIcon();
	
	Dir getHorizontalDir();
	Dir getVerticalDir();
	
	// Indicator
	void addCustomIndicator(Node *node, const Vec2 &offset,
							float scale=2.0f, bool onMainSprite=true);
	void removeCustomIndicator();
	
	// Csb Effect
	void setCsbAnimeRotation(float degree);
	void addCsbAnimeByName(const std::string &csbName,
									  NodeType parentNodeType,
									  float scale, const Vec2 &offset);

	void addCsbAnimeByName(const std::string &csbName, bool isFront,
						   float scale=2.5f, const Vec2 &offset = Vec2::ZERO);
    void adjustAnimeNode(const std::string &csbName, float angle, Vec2 offset);
	void playCsbAnime(const std::string &csbName, const std::string &animeName,
					  bool isLoop, bool removeSelf=false,
					  const std::function<void()> &endAnimeCallback=nullptr);
	void removeCsbAnimeByName(const std::string &csbName);
	void setCsbAnimeVisible(const std::string &csbName,bool flag);
	void addOneTimeCsbAnime(const std::string &csbName, const std::string &animeName,
								bool onMainSprite, float scale=1.0f, const Vec2 &offset = Vec2::ZERO);
    
    void setCsbAnimeFrameEventCallback(std::string csbName, std::string animation, EventFrameCallback callback);
    
    virtual void playSoundWithAction(Action action){};
    
    
    float getAnimationDuration();
    float getStartFrame();
    float getEndFrame();
	
	std::string info();
	std::string infoComponentPos();
    
    void setOnFrameEventCallback(EventFrameCallback callback);
    
    Node* getNodeWithName(std::string name);
	
private:
	Node *getNodeByNodeType(const NodeType &type);
	
protected:
	Node *mRootNode;
	Rect mRelativeBoundRect;
    Node *mUpperEffectNode, *mLowerEffectNode;
	Sprite *mMainSprite;
	Sprite *mBoundingImage;
	ActionManager *mActionManager;
	// Node *mVisualEffectNode;
	Node *mStatusIconNode;
	Vec2 mStatusPos;
	float mOriginTimeSpeed;		// Original Timeline Speed
	Map<std::string, cocostudio::timeline::ActionTimeline *> mTimelineMap;	// key: csbName
	Map<std::string, Node *> mCsbNodeMap;
	
	CC_SYNTHESIZE_RETAIN(Node *, mVisualEffectNode, VisualEffectNode);

	Node *mIndicatorNode;
	
protected:
	void cleanup();
	void setupBoundingBox(Node *rootNode);
	void setupContentSize(Node *rootNode);
	void setupStatusPos(Node *rootNode);
	void changeVisualEffect(Node *newNode);
	
	Node *getParentNodeForEffect(bool onMainSprite, Vec2 &outPos);

	
#pragma mark - FrameEvent Handling
	virtual void onFrameEvent(Frame *frame);	// note: override method should use it too
	void handlePlayParticleEvent(EventFrame *event);


	
#pragma mark - Hitbox handling
public:
	void setupHitboxes();		// note: call after the setupScaleAndFlip
	void setupSingleHitBox(Node *rootNode);
	
	std::string infoColliders();

	void getHitCircle(std::vector<Circle> &circleList);
	void getHitboxRect(std::vector<Rect> &hitboxList);
	Rect getDefaultHitbox();
	virtual Rect getMainHitboxRect() const;		// bounding box of the main sprite
	
	std::string infoHitbox();
	
private:
	void setupHitboxesForNode(Node *node);
	void addHitbox(Node *node, bool isCircle);
	Size getHitboxSize(Node *hitbox, bool doAbs=true);
	std::string infoColliderList(const std::string &title, const Vector<Node *> &colliderList);
	
	
private:
	Vector<Node *> mHitboxNodeList;			// List of the BOX Collider
	Vector<Node *> mHitCircleNodeList;		// List of the Circle Collider
	
	std::vector<Size> mHitboxSizeList;		// Scaled size of the hitbox		// no need
    
private:
    EventFrameCallback mEventFrameCallback;


#pragma mark - Collision Check
public:
	bool isCollideModel(GameModel *model);
	bool isCollideWithRect(const Rect &rectWorldCoord);
	bool isCollideWithCircle(const Circle &circle);
	std::string infoMainBoundingBox();
	Rect getFinalMainBoundingBox(bool isWorldCoord = false);
	
private:
	CC_SYNTHESIZE(bool, mUseQuickHitboxCheck, UseQuickHitboxCheck);
	Rect mMainBoundingBox;
	
	
#pragma mark - Visibility
public:
	virtual bool willBecomeVisible();
};

#endif /* GameModel_hpp */
