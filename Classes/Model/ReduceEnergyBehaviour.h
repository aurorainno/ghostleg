//
//  ReduceEnergyBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 21/11/2016.
//
//

#ifndef ReduceEnergyBehaviour_hpp
#define ReduceEnergyBehaviour_hpp

#include <stdio.h>
#include "NormalBehaviour.h"
#include "cocos2d.h"

USING_NS_CC;

class ReduceEnergyBehaviour : public NormalBehaviour
{
public:
    CREATE_FUNC(ReduceEnergyBehaviour);
    
    ReduceEnergyBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onHit();
    
};

#endif /* ReduceEnergyBehaviour_hpp */
