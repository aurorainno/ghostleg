//
//  SnowBallEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 28/11/2016.
//
//

#include "SnowBallEffect.h"

#include "Player.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "CommonType.h"
#include "EnemyFactory.h"
#include "GameSound.h"
#include "SnowBall.h"

SnowBallEffect::SnowBallEffect()
: ItemEffect(Type::ItemEffectSnowball)
, mLayer(nullptr)
, mStart(false)
, mRemain(3)
{
	// TODO
}


Node *SnowBallEffect::onCreateUI()
{
//	ItemEffectUILayer *layer = ItemEffectUILayer::create();
//	
//	layer->configAsTimer("star.png", "## sec", mMaxDuration);
//	
//	setLayer(layer);
//	mLayer->setVisible(false);
	
	return nullptr;
}

void SnowBallEffect::onAttach()
{
//	mRemain = mMaxDuration;
//    mRemain = 3;
}

void SnowBallEffect::onDetach()
{
	setLayer(nullptr);
	
	// GameWorld::instance()->getModelLayer()->removeEffectStatusFromAllEnemy();
	// EnemyFactory::instance()->resetDefaultEffectStatus();
	
}

void SnowBallEffect::onUpdate(float delta)
{
	//log("SnowBallEffect: onUpdate is called!");
	if(mStart) {
		mRemain -= delta;
			
		
		// update GUI
		updateUI();
	}
}

void SnowBallEffect::setMainProperty(float value)
{
    mVelocity = value;
}

void SnowBallEffect::setProperty(const std::string &name, float value)
{
	if(name == "velocity") {
		mVelocity = value;;
	}
    if(name == "duration") {
        mRemain = value;;
    }
}


bool SnowBallEffect::shouldDetach()
{
	return mRemain <= 0;
}


void SnowBallEffect::updateUI()
{
	if(mLayer) {
		mLayer->setVisible(true);
		mLayer->setValue(mRemain);
	}
}

float SnowBallEffect::getScaleByLevel(int level)
{
	float resultScale = 0.4f + (level - 1) * 0.05f;
	
	//log("**DEBUG: level=%d scale=%f", level, resultScale);
	
	return resultScale;
}

void SnowBallEffect::activate()
{
	mStart = true;
	
	GameSound::playSound(GameSound::GrabStar);
	
    
	
//	GameWorld::instance()->getModelLayer()->applyEffectStatusToAllEnemy(EffectStatusTimestop);
//	EnemyFactory::instance()->setDefaultEffectStatus(EffectStatusTimestop);
    ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
    //Player *player = modelLayer->getPlayer();
    
    SnowBall *snowBall = SnowBall::create();
    //snowBall->setPosition(player->getPositionX(),player->getPositionY());
	snowBall->setupMoveLogic();
    snowBall->setAcceler(mVelocity);
	snowBall->setScale(getScaleByLevel(getLevel()));
    snowBall->setDuration(mRemain);

    modelLayer->addItem(snowBall);
    
	log("SnowBallEffect is activated");
	
	GameWorld::instance()->disableCapsuleButton();
    
 //   mRemain--;
	
//	updateUI();
}
