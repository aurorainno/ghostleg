//
//  StarMagnetEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 28/11/2016.
//
//

#include "StarMagnetEffect.h"

#include "Player.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "CommonType.h"
#include "EnemyFactory.h"
#include "GameSound.h"
#include "EffectLayer.h"

const std::string kAnimeCsbName = "particle_magnet.csb";
const std::string kActiveAnimeName = "active";

StarMagnetEffect::StarMagnetEffect()
: ItemEffect(Type::ItemEffectStarMagnet)
, mLayer(nullptr)
, mMaxDuration(1)
, mStart(false)
{
	// TODO
}


Node *StarMagnetEffect::onCreateUI()
{
//	ItemEffectUILayer *layer = ItemEffectUILayer::create();
//	
//	layer->configAsTimer("star.png", "## sec", mMaxDuration);
//	
//	setLayer(layer);
//	mLayer->setVisible(false);
	
//	return layer;
	return nullptr;
}

void StarMagnetEffect::onAttach()
{
	mRemain = mMaxDuration;
}

void StarMagnetEffect::onDetach()
{
    GameSound::stopSound(GameSound::MagnetActive);
    GameSound::playSound(GameSound::MagnetOut);
    
	setLayer(nullptr);
	
	removePlayerVFX();
	
	// GameWorld::instance()->getModelLayer()->removeEffectStatusFromAllEnemy();
	// EnemyFactory::instance()->resetDefaultEffectStatus();
	
}

void StarMagnetEffect::onUpdate(float delta)
{
	//log("StarMagnetEffect: onUpdate is called!");
	if(mStart) {
		mRemain -= delta;
		
		// Collect stars	// ken: not sure it is good or not!
		GameWorld::instance()->getModelLayer()->moveAllStarsToPlayer();
		
		
		// update GUI
		updateUI();
	}
}

void StarMagnetEffect::setMainProperty(float value)
{
	setMaxDuration(value);
}

bool StarMagnetEffect::shouldDetach()
{
	return mRemain <= 0;
}


void StarMagnetEffect::updateUI()
{
	if(mLayer) {
		mLayer->setVisible(true);
		mLayer->setValue(mRemain);
	}
}

void StarMagnetEffect::addPlayerVFX()			// player visual effect
{
	Player *player = GameWorld::instance()->getPlayer();
	if(player) {
		player->addCsbAnimeByName(kAnimeCsbName, false, 0.8f);
		player->playCsbAnime(kAnimeCsbName, kActiveAnimeName, true);

	}
}

void StarMagnetEffect::removePlayerVFX()
{
	Player *player = GameWorld::instance()->getPlayer();
	if(player) {
		player->removeCsbAnimeByName(kAnimeCsbName);
	}

}

void StarMagnetEffect::activate()
{
	mStart = true;
	
	
//	GameSound::playSound(GameSound::GrabStar);
    GameSound::playSound(GameSound::MagnetActive,true);
	
    GameWorld::instance()->getEffectLayer()->addPowerUpIndicator(EffectLayer::Magnet, mMaxDuration);
	addPlayerVFX();
	
//	GameWorld::instance()->getModelLayer()->applyEffectStatusToAllEnemy(EffectStatusTimestop);
//	EnemyFactory::instance()->setDefaultEffectStatus(EffectStatusTimestop);
	
	log("StarMagnetEffect is activated");
	
//  GameWorld::instance()->disableCapsuleButton();
	
	updateUI();
}

std::string StarMagnetEffect::toString()
{
	return StringUtils::format("Magnet: duration=%f", mMaxDuration);
}


float StarMagnetEffect::getRemainTime()
{
	return mRemain;
}
