//
//  InvulnerableEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "InvulnerableEffect.h"
#include "Player.h"
#include "ViewHelper.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "EffectLayer.h"

InvulnerableEffect::InvulnerableEffect()
: ItemEffect(Type::ItemEffectInvulnerable)
, mLayer(nullptr)
, mStart(false)
{
	mMaxDuration = 6.0f;	// 6 second
}


Node *InvulnerableEffect::onCreateUI()
{
//    ItemEffectUILayer *layer = ItemEffectUILayer::create();
//    
//    layer->configAsTimer("icon_invulnerable.png", "## sec", mMaxDuration);
//    setLayer(layer);
//    mLayer->setVisible(false);
//	
//    return layer;

	return nullptr;
}

void InvulnerableEffect::onAttach()
{
	
	
	//
	mRemain = mMaxDuration;
	
}

void InvulnerableEffect::onDetach()
{
    GameSound::stopSound(GameSound::ShieldLoop);
    GameSound::playSound(GameSound::ShieldEnd);
    
	if(mPlayer) {
		mPlayer->removeEffectStatus();
		mPlayer->removeVisualEffect();
		mPlayer->removeCustomIndicator();
	}
	setLayer(nullptr);
}

void InvulnerableEffect::setMainProperty(float value)
{
	setMaxDuration(value);
}

void InvulnerableEffect::setProperty(const std::string &name, float value)
{
	if(name == "duration") {
		setMaxDuration(value);
	}
}


void InvulnerableEffect::onUpdate(float delta)
{
    if(mStart){
        mRemain -= delta;
        updateUI();
    }
}


float InvulnerableEffect::getRemainTime()
{
	return mRemain;
}

bool InvulnerableEffect::shouldDetach()
{
	return mRemain <= 0;
}


void InvulnerableEffect::updateUI()
{
	if(mLayer) {
        mLayer->setVisible(true);
        mLayer->setValue(mRemain);
	}
}


void InvulnerableEffect::addIndicator()
{
	if(mPlayer == nullptr) {
		log("InvulnerableEffect.addIndicator: player is null");
		return;
	}
	ItemEffectUILayer *layer = ItemEffectUILayer::create();
	layer->configAsTimer("icon_invulnerable.png", "## sec", mMaxDuration);
	
	setLayer(layer);
	mLayer->setVisible(true);
	
	mPlayer->addCustomIndicator(mLayer, Vec2(-40, 30), 1.0 ,false);
}

void InvulnerableEffect::activate()
{
    mStart = true;
    if(mPlayer) {
        Vec2 offset = Vec2(0, -120);
        mPlayer->setEffectStatus(EffectStatusInvulnerable);
    }
    
    GameWorld::instance()->getEffectLayer()->addBoosterIndicator(EffectLayer::Shield, mMaxDuration);
    
    GameSound::playSound(GameSound::ShieldLoop,true);
    GameSound::playSound(GameSound::ShieldActive);
    
    GameWorld::instance()->disableCapsuleButton();
	
//	addIndicator();
    updateUI();
}

float InvulnerableEffect::getProperty(const std::string &name)
{
	if(name == "duration") {
		return mMaxDuration;
	}
	
	return 0.0f;
}
