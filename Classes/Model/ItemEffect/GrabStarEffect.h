//
//  GrabStarEffect.hpp
//  GhostLeg
//
//  Created by Script
//
//

#ifndef GrabStarEffect_hpp
#define GrabStarEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class GrabStarEffect : public ItemEffect
{

public:

#pragma mark - Public static Method
	CREATE_FUNC(GrabStarEffect);

	GrabStarEffect();
	
	CC_SYNTHESIZE(float, mDuration, Duration);

	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void setMainProperty(float value);

	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);


private:
	void updateUI();
	void activateGrabEffect();
	
	void resetCooldown();
	void reduceCooldown(float delta);

private:
	// Internal Data
	float mCooldown;
};





#endif /* InvulnerableEffect_hpp */
