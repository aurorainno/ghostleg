//
//  StarMagnetEffect.hpp
//  GhostLeg
//
//  Created by Ken Lee on 28/11/2016.
//
//

#ifndef StarMagnetEffect_hpp
#define StarMagnetEffect_hpp

#include <stdio.h>



#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class StarMagnetEffect : public ItemEffect
{
	
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(StarMagnetEffect);
	
	StarMagnetEffect();
	
	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void setMainProperty(float value);
	virtual void activate();
	virtual std::string toString();
	virtual float getRemainTime();
	
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
	CC_SYNTHESIZE(float, mMaxDuration, MaxDuration);
	
private:
	void updateUI();
	void addPlayerVFX();			// player visual effect
	void removePlayerVFX();
	
private:
	// Internal Data
	bool mStart;
	float mRemain;
};


#endif /* StarMagnetEffect_hpp */
