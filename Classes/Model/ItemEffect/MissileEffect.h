//
//  MissileEffect.hpp
//  GhostLeg
//
//  Created by Script
//
//

#ifndef MissileEffect_hpp
#define MissileEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"
#include <set>

USING_NS_CC;

class Enemy;

class MissileEffect : public ItemEffect
{

public:

#pragma mark - Public static Method
	CREATE_FUNC(MissileEffect);

	MissileEffect();

	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
	virtual void activate();
	
	CC_SYNTHESIZE(int, mMaxCount, MaxCount);
	
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);


private:
	void updateUI();
	void fireMissileToEnemy(Enemy *enemy);
    int mCurrentMissileID;
	
private:
	// Internal Data
	int mRemain;
	std::set<int> mHitEnemySet;
    std::map<int,float> mAngleMap;
};





#endif /* InvulnerableEffect_hpp */
