//
//  ItemEffect.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#ifndef ItemEffect_hpp
#define ItemEffect_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"


USING_NS_CC;

class Player;

class ItemEffect : public Node
{
public:
	enum Type
	{
		ItemEffectNone			= 0,
		
		ItemEffectShield		= 2,			// note: cannot change the following order
		ItemEffectInvulnerable	= 5,
		ItemEffectTimeStop		= 7,
		ItemEffectDoubleCoins	= 6,
		ItemEffectGrabStar		= 4,		// OLD style grab every N second (passive)
		ItemEffectMissile		= 3,
		ItemEffectStarMagnet	= 8,
		ItemEffectSnowball		= 9,
        ItemEffectSpeedUp       = 10,
        ItemEffectCashOut       = 11,

		ItemEffectEnd,		// This is not real effect, just for termination checking
	};
	
	enum Event
	{
		PlayerIsHit,
	};
	
public:
	static std::string getTypeName(const ItemEffect::Type &type);
	static Type getTypeByName(const std::string &name);
	static bool isCapsuleItemEffect(const ItemEffect::Type &type);
	static bool isActiveable(const ItemEffect::Type &type);
	
	
	
#pragma mark - Public Method and Properties
	ItemEffect(Type type);
	~ItemEffect();
	
	CC_SYNTHESIZE(int, mLevel, Level);
	CC_SYNTHESIZE(bool, mIsPowerUp, IsPowerUp);	// Power Up item will auto activated and not capsule button
	
	Type getType();
	bool isCapsuleItem();
	bool isActiveable();
	
	virtual bool isDetachWhenHit() { return false; }
	
	void setPlayer(Player *player);
	// Creation and Setup
	virtual bool init();
	virtual void setupNode();
	
	// Game Loop
	void update(float delta);
	
	// Attach and Detach
	void attach();
	void detach();
	
	virtual std::string toString();
	
	// Things to implements
	virtual Node *onCreateUI() = 0;
	virtual void onAttach() = 0;
	virtual void onDetach() = 0;
	virtual void onUpdate(float delta) = 0;
	virtual bool shouldDetach() = 0;
	virtual void onEvent(Event event);
	virtual void activate();
	
	virtual float getRemainTime() { return 0; }
	
	virtual void setMainProperty(float value);
	virtual float getProperty(const std::string &name) { return 0.0f; }
	virtual void setProperty(const std::string &name, float value);
	
#pragma mark - Internal Data
protected:
	CC_SYNTHESIZE(Node *, mUINode, UINode);	//
	
	Type mType;
	Player *mPlayer;
	
	
};



#endif /* ItemEffect_hpp */
