//
//  TimeStopEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "TimeStopEffect.h"
#include "Player.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "CommonType.h"
#include "EnemyFactory.h"
#include "GameSound.h"
#include "EffectLayer.h"

TimeStopEffect::TimeStopEffect()
: ItemEffect(Type::ItemEffectTimeStop)
, mLayer(nullptr)
, mMaxDuration(1)
, mStart(false)
{
  // TODO
}


Node *TimeStopEffect::onCreateUI()
{
    ItemEffectUILayer *layer = ItemEffectUILayer::create();
    
    layer->configAsTimer("icon_timestop.png", "## sec", mMaxDuration);
    
    setLayer(layer);
	mLayer->setVisible(false);
    
    mPlayer = GameWorld::instance()->getPlayer();
	
    return nullptr;
}

void TimeStopEffect::onAttach()
{
	mRemain = mMaxDuration;
}

void TimeStopEffect::onDetach()
{
	setLayer(nullptr);
    GameSound::stopSound(GameSound::TimeStopActive);
    GameSound::playSound(GameSound::TimeStopOut);
    
	
	GameWorld::instance()->getModelLayer()->removeEffectStatusFromAllEnemy();
    GameWorld::instance()->getModelLayer()->removeEffectStatusFromChaser();
    mPlayer->removeCustomIndicator();
	EnemyFactory::instance()->resetDefaultEffectStatus();

    GameWorld::instance()->resumeTime();
}

void TimeStopEffect::onUpdate(float delta)
{
    //log("TimeStopEffect: onUpdate is called!");
	if(mStart) {
		mRemain -= delta;
		updateUI();
	}
}

void TimeStopEffect::setMainProperty(float value)
{
	setMaxDuration(value);
}

void TimeStopEffect::setProperty(const std::string &name, float value)
{
	if(name == "duration") {
		setMaxDuration(value);
	}
}

bool TimeStopEffect::shouldDetach()
{
	return mRemain <= 0;
}


void TimeStopEffect::updateUI()
{
	if(mLayer) {
		mLayer->setVisible(true);
        mLayer->setValue(mRemain);
	}
}

void TimeStopEffect::activate()
{
	mStart = true;
	
    GameSound::playSound(GameSound::TimeStopActive,true);
    
    ItemEffectUILayer *layer = ItemEffectUILayer::create();
    layer->configAsTimer("icon_timestop.png", "## sec", mMaxDuration);
    
    setLayer(layer);
    mLayer->setVisible(true);
    
//    mPlayer->addCustomIndicator(mLayer, Vec2(-40, 30), 1.0 ,false);

	
	GameWorld::instance()->getModelLayer()->applyEffectStatusToAllEnemy(EffectStatusTimestop);
    GameWorld::instance()->getModelLayer()->applyEffectStatusToChaser(EffectStatusTimestop);
    
	EnemyFactory::instance()->setDefaultEffectStatus(EffectStatusTimestop);
	
    GameWorld::instance()->disableCapsuleButton();
    
    GameWorld::instance()->pauseTime();
    
    GameWorld::instance()->getEffectLayer()->addBoosterIndicator(EffectLayer::TimeStop, mMaxDuration);
    
	updateUI();
}


float TimeStopEffect::getProperty(const std::string &name)
{
	if(name == "duration") {
		return mMaxDuration;
	}
	
	return 0.0f;
}


std::string TimeStopEffect::toString()
{
	return StringUtils::format("TimeStop: duration=%f", mMaxDuration);
}

float TimeStopEffect::getRemainTime()
{
	return mRemain;
}

