//
//  SlowEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "DoubleCoinsEffect.h"
#include "Player.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "EnemyFactory.h"
#include "Enemy.h"
#include "EffectLayer.h"
#include "GameSound.h"

const std::string kDoubleCoinsCsbName = "particle_doublecoins.csb";

DoubleCoinsEffect::DoubleCoinsEffect()
: ItemEffect(Type::ItemEffectDoubleCoins)
, mLayer(nullptr)
, mIsActivated(false)
, mRemain(5)
{
  // TODO
}


Node *DoubleCoinsEffect::onCreateUI()
{
//    ItemEffectUILayer *layer = ItemEffectUILayer::create();
//	layer->configAsTimer("icon_timestop.png", "## sec", mRemain);
//	setLayer(layer);
//	layer->setVisible(false);
//    
//    GameWorld::instance()->getPlayer()->addCustomIndicator(mLayer, Vec2(-40, 30), 1.0 ,false);
	
	return nullptr;
}

void DoubleCoinsEffect::onAttach()
{
	//mRemain = mMaxDuration;
	
	mIsActivated = false;
}

void DoubleCoinsEffect::onDetach()
{
    GameWorld::instance()->getPlayer()->removeCustomIndicator();
    GameWorld::instance()->getPlayer()->removeEffectStatus();
    mPlayer->removeCsbAnimeByName(kDoubleCoinsCsbName);
    GameWorld::instance()->setDoubleCoins(false);
	setLayer(nullptr);
}

float DoubleCoinsEffect::getProperty(const std::string &name)
{
	if(name == "duration") {
		return mMaxDuration;
	}
	
	return 0.0f;
}

void DoubleCoinsEffect::setMainProperty(float value)
{
	
}

void DoubleCoinsEffect::setProperty(const std::string &name, float value)
{
	if(name == "duration") {
//		setMainProperty(value);
		setMaxDuration(value);
        mRemain = mMaxDuration;
		
	}
}

void DoubleCoinsEffect::onUpdate(float delta)
{
     mRemain -= delta;
 	 updateUI();
}


bool DoubleCoinsEffect::shouldDetach()
{
    return mRemain <= 0;
}

bool DoubleCoinsEffect::isDetachWhenHit()
{
	return true;
}


void DoubleCoinsEffect::updateUI()
{
//    if(mLayer) {
//        mLayer->setVisible(true);
//        mLayer->setValue(mRemain);
//    }
}

void DoubleCoinsEffect::activate()
{
	if(mIsActivated == false) {
		activateEffect();
	}
}

void DoubleCoinsEffect::activateEffect()
{
	mIsActivated = true;
	
	mRemain = mMaxDuration;
	
	// Show the particle effect
//	GameWorld::instance()->getEffectLayer()->showSlowEffect();
	
	GameSound::playSound(GameSound::DoubleCoins);
    GameWorld::instance()->getPlayer()->addCsbAnimeByName(kDoubleCoinsCsbName, false, 0.5,Vec2::ZERO);
    GameWorld::instance()->getPlayer()->playCsbAnime(kDoubleCoinsCsbName, "active", true);
//    GameWorld::instance()->getPlayer()->setEffectStatus(EffectStatus::EffectStatusSlow);
/*	//
	Vector<Enemy *> enemyList = GameWorld::instance()->getModelLayer()->getVisualEnemyList();
	for(int i=0; i<enemyList.size(); i++) {
		Enemy *enemy = enemyList.at(i);
		enemy->setSlowFactor(mSlowFactor);
		enemy->setEffectStatus(EffectStatusSlow);
	}
 */
    GameWorld::instance()->setDoubleCoins(true);
    GameWorld::instance()->disableCapsuleButton();
	
	//GameWorld::instance()->getEffectLayer()->addBoosterIndicator(EffectLayer::BoosterType::DoubleCoin, mMaxDuration);
}

float DoubleCoinsEffect::getRemainTime()
{
	return mRemain;
}
