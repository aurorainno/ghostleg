//
//  SlowEffect.hpp
//  GhostLeg
//
//  Created by Script
//
//

#ifndef SlowEffect_hpp
#define SlowEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class DoubleCoinsEffect : public ItemEffect
{

public:

#pragma mark - Public static Method
	CREATE_FUNC(DoubleCoinsEffect);

	DoubleCoinsEffect();

	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
	virtual void activate();
	virtual bool isDetachWhenHit();
	virtual float getRemainTime();
	float getProperty(const std::string &name);
	
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
	CC_SYNTHESIZE(float, mMaxDuration, MaxDuration);
	

private:
	void updateUI();
	void activateEffect();

private:
	// Internal Data
	float mIsActivated;
	float mSlowFactor;
    float mRemain;
};





#endif /* InvulnerableEffect_hpp */
