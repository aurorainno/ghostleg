//
//  InvulnerableEffect.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#ifndef InvulnerableEffect_hpp
#define InvulnerableEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class InvulnerableEffect : public ItemEffect
{
	
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(InvulnerableEffect);
	
	InvulnerableEffect();

	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
    virtual void activate();
	virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
	virtual float getProperty(const std::string &name);
	virtual float getRemainTime();
	
	CC_SYNTHESIZE(float, mMaxDuration, MaxDuration);
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
	
	
private:
	void updateUI();
	void addIndicator();
	
private:
	float mRemain;			// Effect remain Time
    bool mStart;
};





#endif /* InvulnerableEffect_hpp */
