//
//  ItemEffectUILayer.cpp
//  GhostLeg
//
//  Created by Calvin on 16/6/2016.
//
//

#include <stdio.h>
#include "ItemEffectUILayer.h"
#include "cocostudio/CocoStudio.h"
#include "StringHelper.h"

ItemEffectUILayer::ItemEffectUILayer()
: mIsUseSpriteFrame(true)
{
	
}

bool ItemEffectUILayer::init(){
    
    if(!Layer::init()){
        return false;
    }
    
    mRootNode = CSLoader::createNode("ItemEffectLayer.csb");
    addChild(mRootNode);
    setupUI();
    
    return true;
}

void ItemEffectUILayer::setupUI(){
    setContentSize(mRootNode->getContentSize());
    mTimerPanel = mRootNode->getChildByName("TimerPanel");
    mCounterPanel = mRootNode->getChildByName("CounterPanel");
    for(int i=0;i<mCounterPanel->getChildrenCount();i++){
        Sprite* currentItem = (Sprite*) mCounterPanel->getChildren().at(i);
       // log("ItemEffectLayer: itemName: %s, index: %d",currentItem->getName().c_str(),i);
        mCounterItem.pushBack(currentItem);
    }
    mTimerIcon = mTimerPanel->getChildByName<Sprite*>("icon");
    mTimerValue = mTimerPanel->getChildByName<Text*>("text");
}

void ItemEffectUILayer::configAsCounter(std::string icon, int maxCount){
    if(maxCount>5)
        maxCount = 5;
    mType = Type::Counter;
    mCurrentNumOfCount = maxCount;
    mMaxNumOfCount = maxCount;
    int count = maxCount;
    mCounterPanel->setVisible(true);
    for(Sprite* currentItem : mCounterItem){
        if(count!=0){
            currentItem->setSpriteFrame(icon);
            currentItem->setVisible(true);
            count--;
        }
    }
}

void ItemEffectUILayer::configAsTimer(std::string icon, std::string format,float value){
    mType = Type::Timer;
    mFormat = format;
	
	if(mIsUseSpriteFrame) {
		mTimerIcon->setSpriteFrame(icon);
	} else {
		mTimerIcon->setTexture(icon);
	}
    mTimerValue->setString(aurora::StringHelper::getFormattedString(value,format));
    mTimerPanel->setVisible(true);
}


void ItemEffectUILayer::setValue(float currentValue){
    if(mType == Type::Counter){
		if(currentValue > 5) {
            currentValue = 5;
		}

		mCurrentNumOfCount = currentValue;
		log("Debug: EffectUI: currentValue=%f", currentValue);
		for(int i=0; i<mCounterItem.size(); i++) {
			Sprite *icon = mCounterItem.at(i);
			icon->setVisible(i < currentValue);
		}

		
		
//        int totalCount = mCurrentNumOfCount - (int)currentValue;
//        for(int i=0;i<totalCount;i++){
//            Sprite *item = mCounterItem.at(mCurrentNumOfCount-i-1);
//            item->setVisible(false);
//            mCounterItem.eraseObject(item);
//        }
		
		
		
    } else if(mType == Type::Timer){
		std::string info = aurora::StringHelper::getFormattedString(currentValue, mFormat);
        mTimerValue->setString(info);
    }
}

void ItemEffectUILayer::setMessage(const std::string &msg)
{
	if(mType == Type::Timer){
		mTimerValue->setString(msg);
	}
}


