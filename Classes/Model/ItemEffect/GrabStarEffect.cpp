//
//  GrabStarEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "GrabStarEffect.h"
#include "Player.h"
#include "ViewHelper.h"
#include "Star.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "GameSound.h"

const std::string kMagnetCsbname = "status_magnet.csb";
const std::string kMagnetAnimeName = "active";

GrabStarEffect::GrabStarEffect()
: ItemEffect(Type::ItemEffectGrabStar)
, mLayer(nullptr)
, mDuration(5)
{
  // TODO
}


Node *GrabStarEffect::onCreateUI()
{
//	note: final decision. don't show any
//	ItemEffectUILayer *layer = ItemEffectUILayer::create();
//	layer->configAsTimer("star.png", "## sec", mDuration);
//	setLayer(layer);
	
	return nullptr;
}

void GrabStarEffect::onAttach()
{
	resetCooldown();
	
	if(mPlayer) {
		mPlayer->addCsbAnimeByName(kMagnetCsbname, false, 5.0f, Vec2(0, 50));
		mPlayer->setCsbAnimeVisible(kMagnetCsbname, false);	// hide by default!
	}


	updateUI();
}

void GrabStarEffect::onDetach()
{
	if(mPlayer) {
		mPlayer->removeCsbAnimeByName(kMagnetCsbname);
	}
	setLayer(nullptr);
}

void GrabStarEffect::onUpdate(float delta)
{
	// update cooldown value and UI
	reduceCooldown(delta);
	updateUI();
	
	
	
	//
	if(mCooldown == 0) {
		activateGrabEffect();
	}
	
}


bool GrabStarEffect::shouldDetach()
{
	return false;
}

void GrabStarEffect::resetCooldown()
{
	mCooldown = mDuration;
}

void GrabStarEffect::reduceCooldown(float delta)
{
	mCooldown -= delta;
	if(mCooldown < 0) {
		mCooldown = 0;
	}
}

void GrabStarEffect::updateUI()
{
	if(mLayer) {
        mLayer->setValue(mCooldown);
	}
}

void GrabStarEffect::setMainProperty(float value)
{
	setDuration((int) value);
}


void GrabStarEffect::activateGrabEffect()	// note: no one using
{
	log("GrabStarEffect is activate!!");
	
	// Visual Effect
	if(mPlayer) {
		mPlayer->playCsbAnime(kMagnetCsbname, kMagnetAnimeName, false);		
	}
	
	//GameSound::playSound(GameSound::GrabStar);		// something wrong when play this!!
	
	// Select the star
	ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
	Vector<Collectable *> starList = modelLayer->getVisibleCollectableList();
	Collectable *selectedStar = nullptr;
	
	for(Collectable *star : starList) {
		if(star == nullptr) {
			continue;
		}
		if(star->getPositionY() < mPlayer->getPositionY()) {	// Not Grab those behind player
			continue;
		}
		
		if(! star->getMoveToPlayer()) {
			selectedStar = star;
			break;
		}
	}
	
	if(selectedStar == nullptr) {
		// No star found
		return;
	}
	
	selectedStar->setMoveToPlayer(true);
	
	// Reset the cooldown counter
	resetCooldown();
}
