//
//  ShieldEffect.hpp
//  GhostLeg
//
//  Created by Script
//
//

#ifndef ShieldEffect_hpp
#define ShieldEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class ShieldEffect : public ItemEffect
{

public:

#pragma mark - Public static Method
	CREATE_FUNC(ShieldEffect);

	ShieldEffect();

	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void onEvent(Event event);
	virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
	virtual float getProperty(const std::string &name);
	virtual std::string toString();
	
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);

	CC_SYNTHESIZE(float, mDuration, Duration);
	CC_SYNTHESIZE(int, mMaxCount, MaxCount);
	
	
	bool hasDuration();
private:
	void updateUI();
	void activateShield();
	void handleShieldDuration(float delta);
	
private:
	// Internal Data
	int mRemain;
	float mCooldown;
	bool mActivated;
	float mRemainTime;
};





#endif /* InvulnerableEffect_hpp */
