//
//  SnowBallEffect.hpp
//  GhostLeg
//
//  Created by Ken Lee on 28/11/2016.
//
//

#ifndef SnowBallEffect_hpp
#define SnowBallEffect_hpp

#include <stdio.h>



#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class SnowBallEffect : public ItemEffect
{
	
public:
	
#pragma mark - Public static Method
	CREATE_FUNC(SnowBallEffect);
	
	SnowBallEffect();
	
	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
	
	virtual void activate();
	
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
	
private:
	void updateUI();
	float getScaleByLevel(int level);
	
private:
	// Internal Data
	bool mStart;
	float mRemain;
    float mVelocity;
};


#endif /* SnowBallEffect_hpp */
