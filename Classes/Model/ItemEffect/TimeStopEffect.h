//
//  TimeStopEffect.hpp
//  GhostLeg
//
//  Created by Script
//
//

#ifndef TimeStopEffect_hpp
#define TimeStopEffect_hpp

#include <stdio.h>
#include "ItemEffect.h"
#include "ItemEffectUILayer.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class TimeStopEffect : public ItemEffect
{

public:

#pragma mark - Public static Method
	CREATE_FUNC(TimeStopEffect);

	TimeStopEffect();

	virtual Node *onCreateUI();
	virtual void onAttach();
	virtual void onDetach();
	virtual void onUpdate(float delta);
	virtual bool shouldDetach();
	virtual void setMainProperty(float value);
	virtual void setProperty(const std::string &name, float value);
	
	virtual void activate();
	virtual std::string toString();
	virtual float getRemainTime();
	
	CC_SYNTHESIZE_RETAIN(ItemEffectUILayer *, mLayer, Layer);
	CC_SYNTHESIZE(float, mMaxDuration, MaxDuration);

	virtual float getProperty(const std::string &name);
	
private:
	void updateUI();

private:
	// Internal Data
	bool mStart;
	float mRemain;
};





#endif /* InvulnerableEffect_hpp */
