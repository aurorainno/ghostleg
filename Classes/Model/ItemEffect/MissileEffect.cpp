//
//  MissileEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "MissileEffect.h"
#include "Player.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "Enemy.h"
#include "Missile.h"
#include "GameSound.h"
#include "AttackBehaviour.h"

const int kBulletID = 80;

MissileEffect::MissileEffect()
: ItemEffect(Type::ItemEffectMissile)
, mLayer(nullptr)
, mHitEnemySet()
, mMaxCount(0)
, mCurrentMissileID(0)
{
  // TODO
}


Node *MissileEffect::onCreateUI()
{
    ItemEffectUILayer *layer = ItemEffectUILayer::create();
    
    layer->configAsCounter("icon_missile.png", mMaxCount);
    
    setLayer(layer);
    
    return layer;
}

void MissileEffect::onAttach()
{
	mRemain = mMaxCount;
    
    mAngleMap.clear();
    mAngleMap[0] = 90;
    mAngleMap[1] = 67.5;
    mAngleMap[2] = 112.5;
    mAngleMap[3] = 45;
    mAngleMap[4] = 135;

	updateUI();
}


void MissileEffect::activate()
{
	ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
	Vector<Enemy *> nearbyEnemyList = modelLayer->getNearestEnemy(400, mMaxCount, mHitEnemySet);
	
	// log("onUpdate: Number of enemy: %ld", nearbyEnemyList.size());
	
	if(nearbyEnemyList.size() >= 1) {
        for(int i=0;i<nearbyEnemyList.size();i++){
            Enemy* enemy = nearbyEnemyList.at(i);
            if(enemy->getEnemyData()->getType() == kBulletID){
                continue;
            }
            
            AttackBehaviour* attackBehaviour = dynamic_cast<AttackBehaviour*>(enemy->getBehaviour());
            if(attackBehaviour){
                if(!attackBehaviour->isColliable()){
                    continue;
                }
            }
            
            
            fireMissileToEnemy(nearbyEnemyList.at(i));
        }
	}
    
    int count = mRemain;
    for(int i=0; i<count; i++){
        
//        GameSound::playSound(GameSound::Missile);
        
        ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
        Player *player = modelLayer->getPlayer();
        
        // Create the missile
        
        Missile *missile = Missile::create();
        missile->setMissileType(Missile::RandomEmit);
        int angle = mAngleMap[i];
        missile->setRotation(-angle);
        missile->setPosition(player->getPosition());
        missile->setID(mCurrentMissileID);
        
        GameSound::playSound(GameSound::MissileActive);
        modelLayer->addItem(missile);
        
        mRemain--;
        mCurrentMissileID++;
    }
	
    
	updateUI();
}

void MissileEffect::onDetach()
{	
	setLayer(nullptr);
}

void MissileEffect::fireMissileToEnemy(Enemy *enemy)
{
	if(mRemain <= 0) {
		return;
	}
	
	if(enemy == nullptr) {
		return;
	}
	
//    GameSound::playSound(GameSound::Missile);
	
	ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
	Player *player = modelLayer->getPlayer();
	
	// Create the missile
	
	Missile *missile = Missile::create();
    missile->setMissileType(Missile::Homing);
	missile->setPosition(player->getPosition());
	missile->setTargetEnemy(enemy);

    
	modelLayer->addItem(missile);

	// Mark the enemy is hit
	mHitEnemySet.insert(enemy->getObjectID());
	
	// Decrease the count
	mRemain--;
    
    //increament missile ID
    mCurrentMissileID++;
}


void MissileEffect::setMainProperty(float value)
{
	setMaxCount((int) value);
}

void MissileEffect::setProperty(const std::string &name, float value)
{
	if(name == "missileCount") {
		setMaxCount((int) value);
	}
}

void MissileEffect::onUpdate(float delta)
{
	
	
	
}


bool MissileEffect::shouldDetach()
{
	return mRemain <= 0;
}


void MissileEffect::updateUI()
{
	if(mLayer) {
        mLayer->setValue(mRemain);
	}
}
