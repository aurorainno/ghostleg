//
//  ItemEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "ItemEffect.h"
#include "Player.h"
#include "GameWorld.h"
#include "StringHelper.h"

#pragma mark - Public Method and Properties
ItemEffect::ItemEffect(Type type)
: mType(type)
, mUINode(nullptr)
, mLevel(1)
, mIsPowerUp(false)
{
	
}

ItemEffect::~ItemEffect()
{
	setUINode(nullptr);
	setPlayer(nullptr);
}

// Creation and Setup
bool ItemEffect::init()
{
	bool isOkay = Node::init();
	if(!isOkay) {
		return false;
	}
	setupNode();
	
	
	return true;
}

void ItemEffect::setupNode()
{
	// Todo: the graphics attaching the player
}

// Game Loop
void ItemEffect::update(float delta)
{
	if(mPlayer == nullptr) {
		return;
	}
	
	// if(mPlayer->isDie
	
	onUpdate(delta);
	
	if(shouldDetach()) {
		if(mPlayer) {
			if(getIsPowerUp()) {
				detach();
			} else {
				mPlayer->detachItemEffect(this);
 //               GameWorld::instance()->getGameUILayer()->getBoosterControl()->setIsEffectDone(true);
			}
		}
	}
}

void ItemEffect::attach() {
	// GUI Stuff
	Node *node = onCreateUI();
	if(node) {
		setUINode(node);
		GameWorld::instance()->addItemEffectUI(node);
//        scheduleUpdate();
	}
	
	// Logic stuff
	onAttach();
	
	
	if(getIsPowerUp()) {
		activate();
	}
}


void ItemEffect::detach()
{
	// Logic Stuff
	onDetach();
	
	// GUI Stuff
	if(mUINode) {
		mUINode->removeFromParent();
		setUINode(nullptr);
	}
	
	if(getIsPowerUp()) {
		if(mPlayer != nullptr) {
			mPlayer->detachPowerUp(getType());
		}
	}
	
	// Unlink
	setPlayer(nullptr);
}


void ItemEffect::setPlayer(Player *player)
{
	mPlayer = player;		// weak reference, strong reference is not good !!
}


void ItemEffect::onEvent(Event event)
{
	// Nothing to do by default
}


void ItemEffect::activate()
{
	// To be implemented
}

void ItemEffect::setProperty(const std::string &name, float value)
{
	// TO be implemented
}

void ItemEffect::setMainProperty(float value)
{
	// TO be implemented
}

#pragma mark - Static
std::string ItemEffect::getTypeName(const ItemEffect::Type &type)
{
	switch(type) {
		case Type::ItemEffectShield:		return "shield";
		case Type::ItemEffectDoubleCoins:	return "slow";
		case Type::ItemEffectTimeStop:		return "timeStop";
		case Type::ItemEffectGrabStar:		return "grabStar";
		case Type::ItemEffectMissile:		return "missile";
		case Type::ItemEffectInvulnerable:	return "invulnerable";
		case Type::ItemEffectStarMagnet:	return "magnet";
		case Type::ItemEffectSnowball:		return "snowball";
        case Type::ItemEffectSpeedUp:       return "speedup";
        case Type::ItemEffectCashOut:       return "cashout";
		default:
			return StringUtils::format("unknown_%d", (int) type);
	}
}

ItemEffect::Type ItemEffect::getTypeByName(const std::string &name)
{
	// note: ItemEffectInvulnerable is the first type
	for(int type=ItemEffectShield; type < ItemEffectEnd; type++) {
		ItemEffect::Type effectType = (ItemEffect::Type) type;		
		
		std::string checkName = getTypeName(effectType);
		if(checkName == name) {
			return effectType;
		}
	}
	
	return Type::ItemEffectNone;
	
	// No need the following code, just for reference
//	
//	
//	if("shield" == name) {
//		return Type::ItemEffectShield;
//	}
//	
//	if("slow" == name) {
//		return Type::ItemEffectSlow;
//	}
//	
//	if("timeStop" == name) {
//		return Type::ItemEffectTimeStop;
//	}
//	
//	if("grabStar" == name) {
//		return Type::ItemEffectGrabStar;
//	}
//	
//	if("missile" == name) {
//		return Type::ItemEffectMissile;
//	}
//	
//	if("invulnerable" == name) {
//		return Type::ItemEffectInvulnerable;
//	}
//	
//	if("magnet" == name) {
//		return Type::ItemEffectStarMagnet;
//	}
//
//	
//	return Type::ItemEffectNone;
}

std::string ItemEffect::toString()
{
	std::string info = "";
	
	info += "EffectType=" + INT_TO_STR(mType);
	
	return info;
}

ItemEffect::Type ItemEffect::getType()
{
	return mType;
}

bool ItemEffect::isCapsuleItem()
{
	return isCapsuleItemEffect(mType);
}

bool ItemEffect::isActiveable()
{
	return isActiveable(mType);
}

// result = true: will show capsule button at the right bottom corner
bool ItemEffect::isActiveable(const ItemEffect::Type &type)
{
	switch(type) {
		case ItemEffectMissile:
		case ItemEffectTimeStop:
		case ItemEffectDoubleCoins:
		case ItemEffectInvulnerable:
		case ItemEffectStarMagnet:
		case ItemEffectSnowball:
        case ItemEffectSpeedUp:
			return true;
		default:
			return false;
	}
}

// result = true: will show capsule in the GameMap / in Space
bool ItemEffect::isCapsuleItemEffect(const ItemEffect::Type &type)
{
	switch(type) {
		case ItemEffectMissile:
		case ItemEffectTimeStop:
		case ItemEffectInvulnerable:
		case ItemEffectDoubleCoins:
		case ItemEffectStarMagnet:
		case ItemEffectSnowball:
        case ItemEffectSpeedUp:
        case ItemEffectCashOut:
			return true;
		default:
			return false;
	}
}
