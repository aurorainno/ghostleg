//
//  ShieldEffect.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/6/2016.
//
//

#include "ShieldEffect.h"
#include "Player.h"
#include "ViewHelper.h"
#include "GameWorld.h"
#include "EffectLayer.h"
#include "GameSound.h"

const char *kShieldCsbName = "status_shield.csb";

ShieldEffect::ShieldEffect()
: ItemEffect(Type::ItemEffectShield)
, mLayer(nullptr)
, mMaxCount(10)		// cannot be 0
, mRemain(10)		// cannot be 0
, mCooldown(1)
, mActivated(false)
, mDuration(1)
{
  // TODO
}


Node *ShieldEffect::onCreateUI()
{

    ItemEffectUILayer *layer = ItemEffectUILayer::create();
	layer->configAsCounter("icon_shield.png", mMaxCount);
    setLayer(layer);
    
	return layer;
}

void ShieldEffect::onAttach()
{
    GameSound::playSound(GameSound::ShieldActive);
    GameSound::playSound(GameSound::ShieldLoop,true);
	mRemain = mMaxCount;
	
	if(mPlayer) {
		mPlayer->setEffectStatus(EffectStatusShield);
		//mPlayer->addParticleEffect(GameRes::Particle::Shield);
		mPlayer->addCsbAnimeByName(kShieldCsbName, true);
		mPlayer->playCsbAnime(kShieldCsbName, "active", true);
	}

	mRemainTime = mDuration;
	
	updateUI();
}

void ShieldEffect::onDetach()
{
    GameSound::stopSound(GameSound::ShieldLoop);
    GameSound::playSound(GameSound::ShieldEnd);

	if(mPlayer) {
		mPlayer->removeEffectStatus();
		mPlayer->removeCsbAnimeByName(kShieldCsbName);
	}
	GameSound::playSound(GameSound::ShieldBreak);
	
	setLayer(nullptr);
	
}

void ShieldEffect::onUpdate(float delta)
{
	if(mActivated) {
		mCooldown -= delta;
		if(mCooldown <= 0) {
			mActivated = false;
		}
	}
	
	handleShieldDuration(delta);
	
	//updateUI();
	
	
}

void ShieldEffect::setMainProperty(float value)
{
	setMaxCount((int) value);
}

void ShieldEffect::onEvent(Event event)
{
	if(event == Event::PlayerIsHit) {
		if(! mActivated) {
			activateShield();
			
		}
		
		update(0);
	}
}

void ShieldEffect::handleShieldDuration(float delta)
{
	if(hasDuration() == false) {
		return;		// Nothing to be done
	}
	
	mRemainTime -= delta;
}

bool ShieldEffect::hasDuration() {
	return mDuration > 0;
}

void ShieldEffect::activateShield()
{
	
	GameSound::playSound(GameSound::ShieldHit);
	mActivated = true;
	mCooldown = 1;
	
	mRemain--;
	
	if(mPlayer) {
		mPlayer->addParticleEffect(GameRes::Particle::ShieldBreak);
	}
	updateUI();
}

void ShieldEffect::setProperty(const std::string &name, float value)
{
	if("duration" == name) {
		setDuration(value);
	} else if("maxHit" == name) {
		setMaxCount((int) value);
	}
}


bool ShieldEffect::shouldDetach()
{
	return (mRemain <= 0 || mRemainTime <= 0) && mActivated == false;
}


void ShieldEffect::updateUI()
{
	if(mLayer) {
        mLayer->setValue(mRemain);
	}
}

float ShieldEffect::getProperty(const std::string &name)
{
	if(name == "duration") {
		return mDuration;
	}
	
	return 0.0f;
}

std::string ShieldEffect::toString()
{
	return StringUtils::format("ShieldEffect: duration=%f", mDuration);
}
