//
//  ItemEffectUILayer.h
//  GhostLeg
//
//  Created by Calvin on 16/6/2016.
//
//

#ifndef ItemEffectUILayer_h
#define ItemEffectUILayer_h

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;

class ItemEffectUILayer : public Layer{
public:
    CREATE_FUNC(ItemEffectUILayer);

	ItemEffectUILayer();
	
    virtual bool init();
    
    enum Type {
        Timer,
	    Counter
    };
public:
    CC_SYNTHESIZE(Type, mType, Type);
	CC_SYNTHESIZE(bool, mIsUseSpriteFrame, UseSpriteFrame);
	

	void configAsTimer(std::string icon, std::string format, float value);
    void configAsCounter(std::string icon, int maxCount);
	
	void setMessage(const std::string &msg);
    void setValue(float currentValue);
	
private:
    Node *mRootNode,*mTimerPanel,*mCounterPanel;
    Vector<Sprite*> mCounterItem;
    Sprite* mTimerIcon;
    Text* mTimerValue;
    std::string mFormat;
    int mCurrentNumOfCount, mMaxNumOfCount;
    void setupUI();
};

#endif /* ItemEffectUILayer_h */
