//
//  energyEffectNode.cpp
//  GhostLeg
//
//  Created by Calvin on 19/10/2016.
//
//

#include "EnergyEffectNode.h"
#include "cocostudio/CocoStudio.h"
#include "StringHelper.h"

bool EnergyEffectNode::init()
{
    if(!Node::init()){
        return false;
    }
    
    mRootNode = CSLoader::createNode("energyEffectNode.csb");
    addChild(mRootNode);
    
    mEnergyText = (Text*) mRootNode->getChildByName("energyText");
    
    return true;
}

void EnergyEffectNode::setEnergyText(int energy)
{
    std::string energyText = StringUtils::format("%d",energy);
    mEnergyText->setString(energyText);
}

void EnergyEffectNode::setOpacity(GLubyte opacity)
{
    Vector<Node*> &list = mRootNode->getChildren();
    for(Node* node : list){
        node->setOpacity(opacity);
    }
}
