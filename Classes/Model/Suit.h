//
//  PlayerSuit.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/6/2016.
//
//

#ifndef PlayerSuit_hpp
#define PlayerSuit_hpp

#include <stdio.h>
#include <stdio.h>
#include "cocos2d.h"

#include "ItemEffect.h"

USING_NS_CC;

class SuitData : public Ref
{
public:
	
	static SuitData *create();
	
	CC_SYNTHESIZE(int, mSuitID, SuitID);
	CC_SYNTHESIZE(int, mMaxLevel, MaxLevel);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mInfo, Info);
    CC_SYNTHESIZE(std::string, mCharIcon, CharIcon);
    CC_SYNTHESIZE(std::string, mItemIcon, ItemIcon);
	CC_SYNTHESIZE(std::string, mDisplayFormat, DisplayFormat);
	
	CC_SYNTHESIZE(ItemEffect::Type, mPassiveSkill, PassiveSkill);
	CC_SYNTHESIZE(ItemEffect::Type, mItemEffect, ItemEffect);
	
    std::string getSnowBallLevelValue(int level);
	float getLevelValue(int level);
	int getUpgradePrice(int level);
	
	void addLevelValue(float value);
	void addLevelPrice(int price);
	void setLevelValue(int level, float value);
	
	std::string toString();
    
    std::string getDisplayInfo(int level);
	
	void reset();
	
private:
	std::vector<float> mLevelValue;		// idx: level  value=Effect value of mastery
	std::vector<int> mLevelPrice;		// idx: level  value=Price to upgrade
};

class PlayerSuit : public Ref
{
public:
	
	CC_SYNTHESIZE(int, mSuitID, SuitID);
	CC_SYNTHESIZE(int, mLevel, Level);
	
	void parse(const std::string &string);
	
	std::string toString();
};


#endif /* PlayerSuit_hpp */
