//
//  MapLine.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2016.
//
//

#include "MapLine.h"

//const Color4F kMainColor(0.0000, 0.4216, 0.7000, 1.0);
const Color4F kMainColor(50.0f/255, 215.0f/255, 210.0f/255, 1.0);

static int linesNumberCounter = 100;


MapLine::MapLine()
{
	
}

MapLine *MapLine::create(const MapLine *mapLine)
{
	Vec2 start = mapLine->getStartPoint();
	Vec2 end = mapLine->getEndPoint();
	
	return create(mapLine->getType(), start.x, start.y, end.x, end.y);

}

MapLine *MapLine::create(MapLine::Type type, float x1, float y1, float x2, float y2)
{
    
	MapLine *line = new MapLine();
    
    
	line->setType(type);
	
	LineSegment segment;
	segment.x1 = x1;
	segment.y1 = y1;
	segment.x2 = x2;
	segment.y2 = y2;
	
	line->setLine(segment);
    
    
    linesNumberCounter++;
    line->setLineObjectID(linesNumberCounter);
	
	line->autorelease();
	return line;
}


MapLine *MapLine::cloneMapLine(const MapLine *mapLine)
{
    MapLine *line = new MapLine();
    LineSegment segment;
    line->setType(mapLine->getType());
    segment.x1 = mapLine->getStartPoint().x;
    segment.y1 = mapLine->getStartPoint().y;
    segment.x2 =  mapLine->getEndPoint().x;
    segment.y2 = mapLine->getEndPoint().y;
    line->setLine(segment);
    line->setLineObjectID(mapLine->getLineObjectID());
    line->autorelease();
    return line;

}



Vec2 MapLine::getStartPoint() const
{
	return Vec2(mLine.x1, mLine.y1);
}

Vec2 MapLine::getEndPoint() const
{
	return Vec2(mLine.x2, mLine.y2);
}


std::string MapLine::toString()
{
	std::string result = "";
	
	result += StringUtils::format("type=%d", mType);
	result += StringUtils::format(" start=[%f,%f]", mLine.x1, mLine.y1);
	result += StringUtils::format(" end=[%f,%f]", mLine.x2, mLine.y2);
	result += StringUtils::format(" lineType=[%d]", mLineType);
	
	
	return result;
}


float MapLine::getMaxY()
{
	return MAX(mLine.y1, mLine.y2);
}


bool MapLine::isSlopeLine() const
{
    return (mType == eTypeSlope);
}

bool MapLine::isLineCreatedByUser()
{
    return (mType == eTypeCreate || mType == eTypeSlope);
}

bool MapLine::isSlopeUpward() const
{
    Vec2 start = getStartPoint();
    Vec2 end = getEndPoint();
    
    return (start.y < end.y);
}




float MapLine::getLineSlope(Dir movingDirection)
{
    Vec2 start = getStartPoint();
    Vec2 end = getEndPoint();
    float result = (end.y - start.y)/(end.x - start.x);
    result =  fabs(result);
    
    if(this->isMovingUpwardAlongSlopeLine(movingDirection))return  result;
    else return -result;
    
}

Color4F MapLine::getLineColor()
{
    
    if(mType == eTypeCreate)
    {
        return Color4F(0.0f, 1.0f, 0.7f, 1);
    }
//    else if (mType == eTypeSlope)
//    {
//        return Color4F(0.4f, 0.55f, 0.33f, 1);
//    }
	
	return kMainColor;	//Color4F(0.0000, 0.7216, 1.0000, 1.0);


}

#pragma mark - Helper Function
bool MapLine::isMovingUpwardAlongSlopeLine(Dir movingDirection) const
{
    Vec2 start = getStartPoint();
    Vec2 end = getEndPoint();
    
    if(isSlopeUpward())
    {
        
        if(end.x > start.x)
        {
            if(movingDirection == DirRight) return true;
            else if(movingDirection == DirLeft) return false;
            
        }
        else
        {
            if(movingDirection == DirRight) return false;
            else if(movingDirection == DirLeft) return true;
            
        }
        
    }
    else
    {
        if(start.x > end.x)
        {
            if(movingDirection == DirLeft) return false;
            else if(movingDirection == DirRight) return true;
        }
        else
        {
            if(movingDirection == DirLeft) return true;
            else if(movingDirection == DirRight) return false;
        }
        
    }
    
    return true;
    
}


Vec2 MapLine::getSlopeDestinationPoint(Dir movingDirection)const
{
    Vec2 start = getStartPoint();
    Vec2 end = getEndPoint();
    
    
    if(isSlopeUpward())
    {
        if(end.x > start.x)
        {
            if(movingDirection == DirRight) return getEndPoint();
            else if(movingDirection == DirLeft) return getStartPoint();
        }
        else
        {
            if(movingDirection == DirRight) return getStartPoint();
            else if(movingDirection == DirLeft) return getEndPoint();
        }
    }
    else
    {
        if(start.x > end.x)
        {
            if(movingDirection == DirRight) return getStartPoint();
            else if(movingDirection == DirLeft) return getEndPoint();
        }
        else
        {
            if(movingDirection == DirRight) return getEndPoint();
            else if(movingDirection == DirLeft) return getStartPoint();
        }
    }
    
    
    return getStartPoint();
    
}

MapLine *MapLine::clone()
{
	return MapLine::cloneMapLine(this);
}

