//
//  GameProduct.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/5/2016.
//
//

#ifndef GameProduct_hpp
#define GameProduct_hpp

#include <stdio.h>



#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class GameProduct : public Ref
{
public:
	static GameProduct *create();
    int getStarsAmount();
	void getMoneyDetail(MoneyType &moneyType, int &amount);
	
	
	GameProduct();
	
	CC_SYNTHESIZE(std::string, mName, Name);					// iap name - for communication to Apple or Google IAP
	CC_SYNTHESIZE(std::string, mProductID, ProductID);			// reference ID
	CC_SYNTHESIZE(std::string, mAction, Action);				// Tell the system what to do, star:100 , noad
	CC_SYNTHESIZE(std::string, mDisplayTag, DisplayTag);		// icon of the tag
	CC_SYNTHESIZE(std::string, mDisplayName, DisplayName);
	CC_SYNTHESIZE(std::string, mGUIKey, GUIKey);
	// CC_SYNTHESIZE(std::string, mDisplayPrice, DisplayPrice);	// the display price
	CC_SYNTHESIZE(std::string, mIAPPrice, IAPPrice);			// the IAP price
																//	IAP: HKD$ 19   Diamond: Diamond:10
	CC_SYNTHESIZE(float, mIncome, Income);						// Income in HKD from this product, for analytics purpose
	CC_SYNTHESIZE(bool, mIsConsumable, IsConsumable);			// Is consume
	CC_SYNTHESIZE(bool, mIsOwned, IsOwned);						// Flag for non conumable
	CC_SYNTHESIZE(bool, mIsReady, IsReady);						// true when update product info succesfully
	CC_SYNTHESIZE(bool, mIsEventItem, IsEventItem);
	
    CC_SYNTHESIZE(bool, mIsBlueAction, IsBlueAction);			// true when it is faceook-liking or appstore-rating.
																	// ^-- ken: this variable name is poor, need refactoring
	CC_SYNTHESIZE(MoneyType, mCostType, CostType);				// usually IAP or Diamond
	CC_SYNTHESIZE(int, mCostValue, CostValue);					// e.g costType=Diamond costValue=10 --> 10 Diamond to buy this product
	CC_SYNTHESIZE(bool, mGiveAdFree, GiveAdFree);				// if true: set AD to no too
	
	std::string getDisplayPrice();
    int getAmount();
	
	std::string toString();
};


#endif /* GameProduct_hpp */
