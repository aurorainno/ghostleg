//
//  GameProduct.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/5/2016.
//
//

#include "GameProduct.h"
#include "StringHelper.h"

//CC_SYNTHESIZE(std::string, mName, Name);				// iap name - for communication to Apple or Google IAP
//CC_SYNTHESIZE(std::string, mProduct, ProductID);		// reference ID
//CC_SYNTHESIZE(std::string, mAction, Action);			// Tell the system what to do, star:100 , noad
//CC_SYNTHESIZE(std::string, mDisplayTag, DisplayTag);		// icon of the tag
//CC_SYNTHESIZE(std::string, mDisplayName, DisplayName);
//CC_SYNTHESIZE(std::string, mDisplayPrice, DisplayPrice);	// the display price
//CC_SYNTHESIZE(bool, mIsConsumable, IsConsumable);			// Is consume

GameProduct::GameProduct()
: mName("")
, mProductID("")
, mAction("")
, mDisplayTag("")
, mIAPPrice("")
, mDisplayName("")
, mGUIKey("")
, mIsConsumable(false)
, mIsOwned(false)
, mIsReady(false)
, mIsEventItem(false)
, mIsBlueAction(false)			// ????? 
, mCostType(MoneyTypeIAP)
, mCostValue(0)
{
	
}

GameProduct *GameProduct::create()
{
    
    GameProduct *gameProduct = new GameProduct();
    
    gameProduct->autorelease();
    
    return gameProduct;
    
}

std::string GameProduct::toString()
{
	std::string result = "";
	
	result += "name=" + mName;
	result += " action=" + mAction;
	result += " displayPrice=" + getDisplayPrice();
	result += " displayName=" + mDisplayName;
	result += " iapPrice=" + mIAPPrice;
	result += " displayTag=" + mDisplayTag;
	result += " productID=" + mProductID;
	result += " income=" + FLOAT_TO_STR(mIncome);
	result += " isConsumable=" + BOOL_TO_STR(mIsConsumable);
	result += " isOwned=" + BOOL_TO_STR(mIsOwned);
	result += " isReady=" + BOOL_TO_STR(mIsReady);
    result += " mIsBlueAction=" + BOOL_TO_STR(mIsBlueAction);
	result += " mCostType=" + INT_TO_STR(mCostType);
	result += " mCostValue=" + INT_TO_STR(mCostValue);
	result += " mGUIKey=" + mGUIKey;
	
	return result;
}


void GameProduct::getMoneyDetail(MoneyType &moneyType, int &amount)
{
	if(this->getIsConsumable() == false)
	{
		moneyType = MoneyTypeUnknown;
		amount = 0;
		return;
	}
	
	std::vector<std::string> tokens = aurora::StringHelper::split(getDisplayName(), ':');
	
	std::string typeStr = tokens[0];
	std::string valueStr = tokens.size() <= 1 ? "0" : tokens[1];
	int value = STR_TO_INT(valueStr);
	
	if("star" == typeStr) {
		moneyType = MoneyTypeStar;
		amount = value;
	} else if("candy" == typeStr) {
		moneyType = MoneyTypeCandy;
		amount = value;
	} else if("diamond" == typeStr) {
		moneyType = MoneyTypeDiamond;
		amount = value;
	} else {
		moneyType = MoneyTypeUnknown;
		amount = 0;
	}
}

int GameProduct::getStarsAmount()
{
    
    if(this->getIsConsumable())
    {
        if(this->getDisplayName().find("star")!=std::string::npos)
        {
            std::string amount = this->getDisplayName().substr(5);
            if(amount.empty()){ return 0;}
            else  {return atoi(amount.c_str());}
        }
        
    }
    
    return 0;

}


std::string GameProduct::getDisplayPrice()
{
	if(mCostType == MoneyTypeIAP) {
		return mIAPPrice;
	} else if(mCostType == MoneyTypeDiamond) {
		return StringUtils::format("diamond:%d", mCostValue);
	} else {
		return "Unknown";
	}
}

int GameProduct::getAmount()
{
    std::vector<std::string> tokens = aurora::StringHelper::split(mAction, ':');
    int amount = aurora::StringHelper::parseInt(tokens[1]);
    return amount;
}
