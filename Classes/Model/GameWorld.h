//
//  GameWorld.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#ifndef GameWorld_hpp
#define GameWorld_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "GameRes.h"
#include "CommonMacro.h"

USING_NS_CC;


class NPCMapLogic;
class MovableModel;
class GameModel;
class GameMap;
class ModelLayer;
class GameTouchLayer;
class Player;
class Enemy;
class GameOverDialog;
class StateLogic;
class GameSceneLayer;
class EffectLayer;
class ParallaxLayer;
class TutorialLayer;
class GameplaySetting;
class Item;
class PlayerGameResult;

class GameWorld : public cocos2d::LayerColor
{
friend class StateLogicRunning;
friend class StateLogicGameOver;
friend class StateLogicNewGame;
friend class StateLogicPause;
friend class StateLogicTutorial;
friend class StateLogicOrderSelect;
friend class StateLogicOrderComplete;
friend class StateLogicFinishGame;

public:
	enum class State {
		None,				// No State
		NotStart,			// Initial State
		NewGame,
		Running,
		Paused,
		Tutorial,
		GameOver,
		OrderSelect,
		OrderComplete,
		FinishGame,
	};
	
	enum class Status {		// Special Status
		Normal,
		Timestop,
		Slow,
	};
	
	typedef std::function<void(Ref *, GameWorldEvent, float arg1)> EventCallback;
	
#pragma mark - Static Method
	static GameWorld *instance();
	
	
	
#pragma mark - Public Method & Property
	bool init();
	GameMap *getMap();
	ModelLayer *getModelLayer();
	Player *getPlayer();
	GameplaySetting *getGameplaySetting();
	void setGameplaySetting(GameplaySetting *newSetting);
	void updateGameplaySetting(bool forTutorial=false);
	
	int getCurrentMapID();
	
	CC_SYNTHESIZE(int, mMapID, MapID);
	CC_PROPERTY(float, mCameraY, CameraY);
    CC_SYNTHESIZE(float, mEnergyRegenTime, EnergyRegenTime);
    CC_SYNTHESIZE(bool, mEnableSlopeMode, EnableSlopeMode);
	CC_SYNTHESIZE(Layer *, mEffectUILayer, EffectUILayer);
	
	CC_SYNTHESIZE(int, mInitialMapOffset, InitialMapOffset);		// the distance betweem y=0 and the first map generation
	
	CC_SYNTHESIZE(int, mReviveCount, ReviveCount);
    CC_SYNTHESIZE(int, mLineNumberDrawn, LineNumberDrawn);
    CC_SYNTHESIZE(int, mEnergyPotionUsed, EnergyPotionUsed);
    
    CC_SYNTHESIZE(float , mTotalTime, TotalTime);
    CC_SYNTHESIZE(float , mRemainTime, RemainTime);
	CC_SYNTHESIZE(bool, mCountDownStarted, CountDownStarted);		// is started the countdown effect
	CC_SYNTHESIZE(bool, mIsBraking, IsBraking);
	
    CC_SYNTHESIZE(bool , mDoubleCoins, DoubleCoins);
	
    SYNTHESIZE_BOOL(mShowTutorial, ShowTutorial);
	
	void setStage(int stage);
	
	void moveCameraToPlayer();
	
    bool canPlayerDrawALine();
    void drawAPlayingHoriLine(Vec2 touchLocation);
    void drawAPlayingSlopeLine(Vec2 startPoint, Vec2 endPoint);
    void removeLineFromMap(int lineObjectID);
    void consumeEnergyPotion();
    
	void setEventCallback(const EventCallback &callback);
	bool isGameRunning();
	
	void start();
	void stop();
	
	void forceUpdate();
	void update(float delta);
	void resetGame();
	void resetTutorialState();
	
	bool isModelVisible(MovableModel *model);
	void resetInitialMapOffset();
	
	void generateNewMapData();
	void addKill();
	
	void sendEvent(GameWorldEvent worldEvent, float arg);

	int getPlayerDistance();
    float getPlayerDistanceFloat();
    
    int getPlayerCollectedCoins();
    int getPlayerCollectedCandy();
    
    int getPlayerScore();
    
    bool isSlopeModeEnabled();
    void setIfAllowSlopeMode(bool enable);
	void setUserTouchEnable(bool enable);
	
	EffectLayer *getEffectLayer();
	GameSceneLayer *getGameUILayer();
	
	void setParallaxLayer(ParallaxLayer *layer);
	
	void updateParallax();

	
	// Item
	void addItemToMap(Item *item);
	
	void addItemEffectUI(Node *node);
	
	void setMapRouteLine(int resID);
	
	// Status
	Status getStatus();
	void setStatus(Status status);
	void unsetStatus(Status status);
	void resetStatus();
	
	
	// Capsule
	void enableCapsuleButton(int itemEffect);
	void disableCapsuleButton();
	void activateCapsule();
	
	// Testing Helper
	void addEnemyAtLine(Enemy *enemy, int lineIndex, float y);

	void revivePlayer();
    
    void pauseTime();
    void resumeTime();
    
    void shakeBackground(float duration);
    
#pragma mark - Bonus Star
	int getBonusStar();
	
#pragma mark - Information
	// Distance
	float getDistanceToPlayer(const Vec2 &myPosition);
	float getYDistanceToPlayer(const Vec2 &myPosition);
	float getXDistanceToPlayer(const Vec2 &myPosition);
	
	float getTopMapY();
	float getBottomMapY();
	bool isRectVisible(const Rect &myRect);
	
#pragma mark - State Logic
	void changeState(State newState);
	void resetState();
//	void changeState(int state);
//	void updateState(int state, float delta);

#pragma mark - For Testing
	void setGameUILayer(GameSceneLayer *layer);
	void setModelLayer(ModelLayer *layer);
    
    GameTouchLayer* getGameTouchLayer();
	
	std::string statInfo();
	
private:
#pragma mark - Internal Property
	int mStage;
	GameMap *mMap;
	GameplaySetting *mGameplaySetting;
	ModelLayer *mModelLayer;
	EffectLayer *mEffectLayer;
    GameTouchLayer *mGameTouchLayer;
	GameSceneLayer *mUILayer;
	ParallaxLayer *mParallaxLayer;	//	 Parallax Background
	
	int mLastGenerationPos;		// the yPos which last generation end
	EventCallback mEventCallback;
	bool mIsGameOver;
	float mInitPlayerX;		// this used to calculate the scrollX of parallax
	
	GameOverDialog *mGameOverDialog;
	
	State mState;
	State mNextState;		// use when
	StateLogic *mStateLogic;
	std::map<State, StateLogic *> mStateLogicMap;
	
	bool mIsFirstLineDropped;
	float mParaxOffset;
	Vec2 mItemIndicatorPos;
    Vec2 mItemIndicatorPosWithCandyStick;
    
    bool mIsCountingTime;
	
#pragma mark - Internal Method
public:
	bool needNewMapData();
	CollisionType handleCollisionLogic();
    bool checkTimeout();
	void handleGameOver();
	void handleLineDropped();
	bool isFirstLineDropped();
	void cleanupItemEffectUI();
	void generateTutorialMap();
	
#pragma mark - State Stuff
private:
	void setupStateLogic();
	void setNewState(State state);
	StateLogic *getStateLogic(State state);
	
	Status mStatus;

	
#pragma mark - PlayerGameResult
public:
	PlayerGameResult *getGameResult();
	void resetGameResult();
	void updatePlayerRecord();
	
private:
	PlayerGameResult *mGameResult;


#pragma mark - Collected Coin (Star) Logic
public:
	void updateScore();
	void updateCoins(bool animation = true);	// Tell the GUI to update
	void collectCoin(int numCoin);
    void updateCandy(bool animation = true);	// Tell the GUI to update
    void collectCandy();
    void updateScore(bool animation = false);
	
	
	void addCoin(int coin);
	void addCoinWithScore(int coin);
	void addCoinWithAnimation(int addValue, const Vec2 &startPos=Vec2::ZERO, float duration=0.5f);
	void addMultipleCoinWithAnimation(const std::vector<int> coinValues,
						const Vec2 &startPos, float duration=0.5f, const int &randomRadius=5);
	
	void addScore(int score, ScoreType type);
	void addScoreWithAnimation(int addScore, ScoreType type, const float &duration=0.8f,
							   const Vec2 &startPos=Vec2::ZERO);
	void addScoreWithGUI(int addScore, ScoreType type, bool showAnimation = true);
	
	
	void addScoreFromModel(GameModel *fromModel, ScoreType scoreType, int scoreValue);
	void addCoinFromModel(GameModel *fromModel, int money);
	
	void collectPuzzle();
	int getPuzzleCount();
	void resetPuzzleCount();
	
	
private:
	int mCollectedCoin;
    int mCollectedCandy;
	int mCollectedPuzzle;
	int mKillCount;

#pragma mark - Last Best
public:
	int getLastBestDistance();
	
private:
	int mLastBestDistance;
    int mCurrentBestDistance;
    int mCurrentScore;
	
	
#pragma mark - Round Time Elapse
public:
	void addTimeElapse(float secToAdd);
	float getTimeElapse();
	void resetTimeElapse();
	void updateGameClock(float delta);
	
private:
	float mTimeElapse;
	
#pragma mark - Update Remain Time
public:
	void addRemainTime(float secToAdd);
	void resumeGameClock();
	void pauseGameClock();
	
private:
	void updateRemainTime(float delta);
	void startClockCountDown();
	void stopClockCountDown();
	
private:
	
	
	
#pragma mark - Map Generation
public:
	NPCMapLogic *getMapLogic();
	
private:
	NPCMapLogic *mMapLogic;


#pragma mark - Chaser
public:
	void resetChaser();
	void activateChaser();
	void adjustChaser();
	void deActivateChaser();

	
#pragma mark - Gameplay setting and Player Attribute
public:
	void updatePlayerMainAttribute();
	
#pragma mark - Bonus Score
	void handleBonusScore();


#pragma mark - Starting ItemEffect( Booster or PowerUp)
public:
	void setupStartingItemEffect();
	void applyStartingItemEffect();

private:
	void applyStartingBooster(int value);
	void applyStartingPowerUp(int value);
	
private:
	bool mStartingItemEffectApplied;
	int mStartingBooster;	// BoosterItemType or ItemEffect::Type
	int mStartingPowerUp;
	

#pragma mark - Reward Coin and Score
public:
	int getKillReward(bool isCashoutEffect);
	
};

#endif /* GameWorld_hpp */
