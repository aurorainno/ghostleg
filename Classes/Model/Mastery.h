//
//  Mastery.hpp
//  GhostLeg
//
//	MasteryData - A class storing the data of the mastery
//
//	PlayerMastery - A class storing the player mastery level
//  Created by Ken Lee on 16/5/2016.
//
//

#ifndef Mastery_hpp
#define Mastery_hpp

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class MasteryData : public Ref
{
public:
	static MasteryData *create();
	
	CC_SYNTHESIZE(int, mMasteryID, MasteryID);
	CC_SYNTHESIZE(int, mMaxLevel, MaxLevel);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mInfo, Info);
    CC_SYNTHESIZE(std::string, mItemIcon, ItemIcon);
	CC_SYNTHESIZE(bool, mPercentFlag, PercentFlag);
	CC_SYNTHESIZE(bool, mFloatFlag, FloatFlag);
    
	float getLevelValue(int level);
	int getUpgradePrice(int level);
	
	void addLevelValue(float value);
	void addLevelPrice(int price);
	void setLevelValue(int level, float value);
	
	std::string toString();
    std::string getDisplayInfo(int level);
	
	void reset();
	
private:
	std::vector<float> mLevelValue;		// idx: level  value=Effect value of mastery
	std::vector<int> mLevelPrice;		// idx: level  value=Price to upgrade
};

class PlayerMastery : public Ref
{
public:
	
	CC_SYNTHESIZE(int, mMasteryID, MasteryID);
	CC_SYNTHESIZE(int, mMasteryLevel, MasteryLevel);
	
	void parse(const std::string &string);
	
	std::string toString();
};

#endif /* Mastery_hpp */
