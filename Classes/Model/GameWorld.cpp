//
//  GameWorld.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#include "GameWorld.h"
#include "GameMap.h"
#include "TimeMeter.h"
#include "NPCMapLogic.h"

#include "ModelLayer.h"
#include "GameTouchLayer.h"
#include "EffectLayer.h"
#include "EnemyFactory.h"
#include "LevelData.h"
#include "DogManager.h"
#include "PlanetManager.h"

#include "MapDataGenerator.h"
#include "GameOverDialog.h"
#include "Player.h"
#include "Enemy.h"
#include "GameSound.h"
#include "Constant.h"
#include "DebugInfo.h"
#include "GameScene.h"
#include "VisibleRect.h"

#include "GameManager.h"
#include "UserGameData.h"

#include "ParallaxLayer.h"

#include "StateLogic.h"
#include "StateLogicGameOver.h"
#include "StateLogicRunning.h"
#include "StateLogicNewGame.h"
#include "StateLogicPause.h"
#include "StateLogicTutorial.h"
#include "StateLogicOrderSelect.h"
#include "StateLogicOrderComplete.h"
#include "StateLogicFinishGame.h"


#include "ItemEffect.h"
#include "SuitManager.h"
#include "ItemEffect.h"
#include "ItemEffectFactory.h"
#include "ItemManager.h"

#include "Analytics.h"
#include "PlayerManager.h"
#include "PlayerGameResult.h"
#include "PlayerRecord.h"

#include "GameplaySetting.h"
#include "StageManager.h"

static GameWorld *sInstance = NULL;

const float kEnergyRegenerationTime = 8.0f;	// 8 second to generate

// Bonus Star formula	(distance - 40) + 10 // e.g 100 = 40 + 30
const int kBonusStarDistanceThreshold = 30;		//

// Tag Value
const int kZOrderItemEffectUI = 10000;


// Tag Value
const int kTagItemEffectUI = 10000;


#pragma mark - Class Implementation 

GameWorld *GameWorld::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new GameWorld();
	sInstance->init();
	
	return sInstance;
}

bool GameWorld::init()
{
	mShowTutorial = false;
	mIsBraking = false;
	
	mInitialMapOffset = kInitialMapDataOffsetY;

	mCollectedCoin = 0;
    mCollectedCandy = 0;
	mCollectedPuzzle = 0;
	mReviveCount = 0;
	mKillCount = 0;
    mCurrentBestDistance = 0;
	
	// State
	mState = State::NotStart;
	mStateLogic = nullptr;
	mNextState = State::None;
	setupStateLogic();

	// Item Indicator
	Vec2 refPos = Vec2(0, VisibleRect::top().y);
	mItemIndicatorPos = refPos + Vec2(10, -70);
	mItemIndicatorPosWithCandyStick = refPos + Vec2(10, -100);
    
	// Setting
	mParallaxLayer = nullptr;
	mEffectUILayer = this;
	mEffectLayer = nullptr;
	
	
	//
	Size size = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	
	// log("World size=%s", StringHelper::strSize(size).c_str());
	
	bool isOk = LayerColor::initWithColor(Color4B(0, 0, 0, 0), size.width, size.height);
	if (isOk == false)
	{
		return false;
	}
	
	// mLastGenerationPos = -1;		// ken: no need
    mEnergyRegenTime = kEnergyRegenerationTime;
	
	mMap = GameMap::create();
	addChild(mMap);
	
	// Add the model layer
	mModelLayer = ModelLayer::create();
	addChild(mModelLayer);
	
	// Effect Layer
	mEffectLayer = EffectLayer::create();
	addChild(mEffectLayer);
    
    // Add the GameTouch layer
    mGameTouchLayer = GameTouchLayer::create();
    addChild(mGameTouchLayer);

	// player result
	mGameResult = new PlayerGameResult();

	//
	mIsGameOver = false;
	
	
	// Map Logic
	mMapLogic = new NPCMapLogic();
	
	
	// Gameplay setting
	mGameplaySetting = GameplaySetting::create();
	CC_SAFE_RETAIN(mGameplaySetting);
	
	
	return true;
}

void GameWorld::forceUpdate()
{
	update(0);
}

void GameWorld::update(float delta)
{
	// Transition when exiting a state
	if(mNextState != State::None) {	// Wait for current state finish
		if(mStateLogic == nullptr || mStateLogic->isEnd()) {
			setNewState(mNextState);		// when setNewState, mStateLogic will change too!
			mNextState = State::None;
		}
	}
	
	//
	if(mStateLogic) {
		mStateLogic->update(delta);
	}

	mGameTouchLayer->update(delta);
	
	// update para offset
	mParaxOffset += delta * 50;
	
	// Stat information
	// log("%s", statInfo().c_str());
}



void GameWorld::resetGame()
{
    GameWorld::instance()->stop();
	
	//
	mMapID = -1;		// not yet set anything
	mParaxOffset = 0;
	mIsGameOver = false;
    mIsCountingTime = true;
	mCollectedCoin = 0;
    mCollectedCandy = 0;
	mCollectedPuzzle = 0;
	mCurrentScore = 0;
	mKillCount = 0;
	mIsFirstLineDropped = false;
	mReviveCount = 0;
    mCurrentBestDistance = 0;
	mIsBraking = false;

    mDoubleCoins = false;
    
    //hardcode for testing
//    mTotalTime = 10;
//    mRemainTime = mTotalTime;

    mIsCountingTime = true;
	mCountDownStarted = false;
	
	int planetID = PlanetManager::instance()->getSelectedPlanet();
	mLastBestDistance = PlanetManager::instance()->getBestDistance(planetID);

	// Set the booster
	if(getGameUILayer() != nullptr) {
		getGameUILayer()->setSelectedBooster(GameManager::instance()->getSelectedBooster());
	}
	
	//
	EnemyFactory::instance()->reset();
	
	// Remove All enemy, item and recreate the player object
	mModelLayer->reset();
	mEffectLayer->reset();
	updatePlayerMainAttribute();		// need to define player attribute after modelLayer reset
	//mTutorialLayer->reset
	
	
	// Clean up Item Effect UI
	cleanupItemEffectUI();
	
	// Remove All Line
	mMap->clearData();
	
	// Reset Player Position
	mInitPlayerX = getPlayer()->getPositionX();
	
	//
	DebugInfo::instance()->reset();
	
	setupStartingItemEffect();
	
	//
	// mLastGenerationPos = -1; // ken: no need
	

	
	// Old Logic in Astrodog 
//	// Generate initial Map
//	if(isShowTutorial()) {
//		MapLevelManager::instance()->setup(kMapHeight);		//
//		generateTutorialMap();
//	} else {
//		MapLevelManager::instance()->setup(kInitialMapDataOffsetY);
//		generateNewMapData();
//	}
//	

	// Update the distance information
	sendEvent(EventUpdateTime, mRemainTime);
    sendEvent(EventUpdateBarEnergy, getPlayer()->getPlayerEnergy());

	
	// Give the default Effect
	// getPlayer()->setupDefaultItemEffect();

	// Move Camera
	moveCameraToPlayer();
	
	// Reset the game result
	resetGameResult();
	
	//
	updateCoins(false);
	
	// Start the scheduler
	start();
	
	mGameTouchLayer->setEnable(true);
}


void GameWorld::start()
{
	scheduleUpdate();
}

void GameWorld::stop()
{
	unscheduleUpdate();
}


void GameWorld::setEventCallback(const EventCallback &callback)
{
	mEventCallback = callback;
}

void GameWorld::sendEvent(GameWorldEvent worldEvent, float arg)
{
	if(mEventCallback == nullptr) {
		log("sendEvent: mCallback is null");
		return;
	}
	
	//log("sendEvent: event=%d arg=%d isGameOver=%d", worldEvent, arg, mIsGameOver);
	mEventCallback(this, worldEvent, arg);
}

int GameWorld::getCurrentMapID()
{
	return mMapLogic == nullptr ? 0 : mMapLogic->getCurrentMap();
}



GameMap *GameWorld::getMap()
{
	return mMap;
}

Player *GameWorld::getPlayer()
{
	return mModelLayer->getPlayer();
}

ModelLayer *GameWorld::getModelLayer()
{
	return mModelLayer;
}

void GameWorld::setCameraY(float y)
{
	mMap->setOffsetY(y);	
	mModelLayer->setOffsetY(y);
	mEffectLayer->setOffsetY(y);
    mGameTouchLayer->setOffsetY(y);
	
	mCameraY = y;
}

float GameWorld::getCameraY() const
{
	return mCameraY;
}


void GameWorld::moveCameraToPlayer()
{
	MovableModel *player = getPlayer();
	if(player == nullptr) {
		return;
	}
	
	float playerY = player->getPosition().y;
	
	float offset = playerY - kCameraPosition;
	
	if(offset < 0) {
		offset = 0;
	}
	
	setCameraY(offset);
	
	// update the background as well
	updateParallax();
}


void GameWorld::updateParallax()
{
	if(mParallaxLayer == nullptr) {
		return;
	}

	float scrollY = mCameraY;
	//mParallaxLayer->setScrollY(scrollY * 2 + mParaxOffset);
	mParallaxLayer->setScrollY(scrollY);
	
	float scrollX = getPlayer()->getPositionX() - mInitPlayerX;
	mParallaxLayer->setScrollX(scrollX);
}


void GameWorld::generateTutorialMap()
{
	// Setup the LevelManager first
	MapDataGenerator *generator = new MapDataGenerator();
	
	
	// Initiailization
	GameMap *map = this->getMap();
	Player *player = this->getPlayer();
	
	int tutorialMap = MapLevelManager::instance()->getTutorialMap();
	generator->init(map, player, 0);
	generator->generateByMapID(tutorialMap);		// TODO: make it easy to config
	
	GameWorld::instance()->getModelLayer()->addEnemyList(generator->getEnemy());
	GameWorld::instance()->getModelLayer()->addItemList(generator->getItemList());
	GameWorld::instance()->getMap()->addSlopeLines(generator->getHoriLine()); 
	setMapID(generator->getMapID());
	
	// some buffer between tutorial and game
	MapLevelManager::instance()->setGeneratedDistance(generator->getRangeEnd() + 300);
	
	// clean up the generator
	delete generator;
}



void GameWorld::generateNewMapData()
{
	// Setup the LevelManager first
	
	// Get the information from level manager
	int startDistance = MapLevelManager::instance()->getGeneratedDistance();
	MapLevelManager::instance()->updateMapCycle(startDistance);		// will cause next cycle if need
	
	MapCycleInfo *currentCycle = MapLevelManager::instance()->getCurrentCycleInfo();
	int mapID = currentCycle->getMapID(startDistance);
	int debugMap = GameManager::instance()->getDebugMap();
	if(debugMap >= 0) {
		mapID = debugMap;
	}
	
	//log("distance: %d cycle=%d mapID=%d", startDistance, currentCycle->getCycle(), mapID);
	
	// Assume Map is generated!!
	
	static int sLastCapsulePos = -1;
	if(startDistance < kMapHeight) {
		sLastCapsulePos = -1;
	}
	
	MapDataGenerator *generator = new MapDataGenerator();
	
	
	// Initiailization
	GameMap *map = this->getMap();
	Player *player = this->getPlayer();
	log("DEBUG: Generate Map start from %d", startDistance);
	generator->init(map, player, startDistance);
	
	// generator->setInitialOffset(mInitialMapOffset);
	
	
	
	
	///
	generator->setLastCapsulePos(sLastCapsulePos);
	
	// Setting for Capsule Item
//	ItemEffect::Type effectType;
//	float effectValue;
//	GameManager::instance()->getSuitManager()
//			->getCapsuleItemEffectAndValue(effectType, effectValue);
//	int level = GameManager::instance()->getSuitManager()->getSelectedSuitLevel();
//	
//	generator->setCapsuleType(effectType, level, effectValue);


	// Generate the enemy, item, lines
	// generator->generate();
	generator->generateByMapID(mapID);
	
	
	GameWorld::instance()->getModelLayer()->addEnemyList(generator->getEnemy());
	GameWorld::instance()->getModelLayer()->addItemList(generator->getItemList());
	//GameWorld::instance()->getMap()->addHoriLines(generator->getHoriLine());
	GameWorld::instance()->getMap()->addSlopeLines(generator->getHoriLine());
	setMapID(generator->getMapID());
	
	// update the newRange
	// mLastGenerationPos = generator->getRangeEnd();	//	no need
	sLastCapsulePos = generator->getLastCapsulePos();

	
	//  Tell level manager where generation end
	MapLevelManager::instance()->setGeneratedDistance(generator->getRangeEnd());
	
	
	
	// clean up the generator
	delete generator;
}

bool GameWorld::needNewMapData()
{
	return MapLevelManager::instance()->needNewMap(mCameraY);
	
//	return (mLastGenerationPos <= mCameraY + 600);
}

int GameWorld::getPlayerDistance()
{
    float dis = mModelLayer->getPlayer()->getTravelDistanceInMetre();
    return std::ceil(dis);
}

float GameWorld::getPlayerDistanceFloat()
{
    return mModelLayer->getPlayer()->getTravelDistance();
}

int GameWorld::getPlayerCollectedCoins()
{
	return mCollectedCoin;
}

int GameWorld::getPlayerCollectedCandy()
{
    return mCollectedCandy;
}

int GameWorld::getPlayerScore()
{
    return mCurrentScore;
}

void GameWorld::setGameUILayer(GameSceneLayer *layer)
{
	mUILayer = layer;
}

GameSceneLayer *GameWorld::getGameUILayer()
{
	return mUILayer;
}


void GameWorld::addItemToMap(Item *item)
{
	if(mState != State::Running) {
		return;
	}
	
	ModelLayer *modelLayer = getModelLayer();
	if(modelLayer == nullptr) {
		return;
	}
	
	modelLayer->addItem(item);
}

bool GameWorld::checkTimeout()
{
    if(mRemainTime<=0){
        return true;
    }else{
        return false;
    }
}

CollisionType GameWorld::handleCollisionLogic()
{
	TSTART("world.itemCollision");
	mModelLayer->handleItemCollision();
	TSTOP("world.itemCollision");
	
	bool neverCollide = GameManager::instance()->getDebugProperty("noCollision");
	if(neverCollide) {
		return HitByNone;		/// Always f
	}

	
	
	TSTART("world.playerCollision");
	CollisionType result = mModelLayer->handlePlayerCollision();	// collide handling to move Enemy->collidePlayer
													//   & enemyBehaviour->onCollidePlayer(..)
	TSTOP("world.playerCollision");
	
	
	return result;
}

void GameWorld::handleGameOver()
{
	changeState(State::GameOver);
	
	// Logging
	// std::string info = GameWorld::instance()->getMapLogic()->infoMap();
	int currentMap = getMapLogic()->getCurrentMap();
	int travel = getPlayer()->getTravelDistanceInMetre();
	Analytics::instance()->logDieMap(currentMap, travel);
	
//	mIsGameOver = true;
//
//	// Save the score
//	int newScore = getPlayerDistance();
//
//	GameManager::instance()->getUserData()->setScore(newScore);
//	
//	//
//	sendEvent(EventGameOver, 0);
}

#pragma mark - Helper Functions
bool GameWorld::canPlayerDrawALine()
{
//	Player *player = getPlayer();
//	if(player == nullptr) {
//		return false;
//	}
//	
//	return player->getPlayerEnergy() >= player->getConsumeRate();
    return true;
}

void GameWorld::drawAPlayingHoriLine(Vec2 touchLocation)
{
    GameWorld::instance()->getMap()->addHoriLines(touchLocation);
	handleLineDropped();
}


void GameWorld::drawAPlayingSlopeLine(Vec2 startPoint, Vec2 endPoint)
{
//	if(getPlayer()->isStunning()) {
//		return;
//	}
//	
	
    GameWorld::instance()->getMap()->addSlopeLine(startPoint,endPoint);
	
	handleLineDropped();
}


void GameWorld::removeLineFromMap(int lineObjectID)
{
    GameWorld::instance()->getMap()->removeLineFromMap(lineObjectID);

}

void GameWorld::handleLineDropped()
{
    mLineNumberDrawn++;
    
	if(!mIsFirstLineDropped) {
		mIsFirstLineDropped = true;
	}
    
	
	
	GameSound::playSound(GameSound::DrawLine);
//	getPlayer()->consumeEnergyByLine();
//	sendEvent(EventUpdateBarEnergy, getPlayer()->getPlayerEnergy());
}

bool GameWorld::isFirstLineDropped()
{
	return mIsFirstLineDropped;
}


void GameWorld::consumeEnergyPotion()
{
    mEnergyPotionUsed++;
    
    GameWorld::instance()->getPlayer()->resetPlayerEnergy();
	
	getGameUILayer()->playEnergyParticle();
    sendEvent(EventUpdateBarEnergy, getPlayer()->getPlayerEnergy());

}


void GameWorld::addEnemyAtLine(Enemy *enemy, int lineIndex, float y)
{
	float x = getMap()->getVertLineX(lineIndex);
	
	enemy->setPosition(Vec2(x, y));
	
	mModelLayer->addEnemy(enemy);
}


bool GameWorld::isSlopeModeEnabled()
{
    return mEnableSlopeMode;
}

void GameWorld::setIfAllowSlopeMode(bool enable)
{
     mEnableSlopeMode = enable;
}



bool GameWorld::isModelVisible(MovableModel *model)
{
	if(model == nullptr) {
		return false;
	}
	
	Size screenSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	
	float mapTopY = screenSize.height + getCameraY();
    float mapBotY = getCameraY();
	
	return mapTopY > model->getPositionY()-model->getContentSize().height/2 &&
           mapBotY < model->getPositionY()+model->getContentSize().height/2;
}


#pragma mark - State Logic 
//void GameWorld::enterState(int state)
//{
//	
//}

void GameWorld::changeState(State newState)
{
	if(mState == newState) {
		return;	// No change
	}
	
	bool endStateNow = (mStateLogic == nullptr) ? true : mStateLogic->exitState();
	
	if(endStateNow) {
		setNewState(newState);
	} else {
		mNextState = newState;
	}
}

void GameWorld::setupStateLogic()
{
	mStateLogicMap.clear();
	
	StateLogic *logic;
	
	//
	logic = new StateLogicRunning();
	logic->setGameWorld(this);
	mStateLogicMap[State::Running] = logic;
	
	logic = new StateLogicGameOver();
	logic->setGameWorld(this);
	mStateLogicMap[State::GameOver] = logic;
	
	logic = new StateLogicNewGame();
	logic->setGameWorld(this);
	mStateLogicMap[State::NewGame] = logic;
	
	logic = new StateLogicPause();
	logic->setGameWorld(this);
	mStateLogicMap[State::Paused] = logic;

	logic = new StateLogicTutorial();
	logic->setGameWorld(this);
	mStateLogicMap[State::Tutorial] = logic;
	
	logic = new StateLogicOrderSelect();
	logic->setGameWorld(this);
	mStateLogicMap[State::OrderSelect] = logic;
	
	logic = new StateLogicOrderComplete();
	logic->setGameWorld(this);
	mStateLogicMap[State::OrderComplete] = logic;
	
	logic = new StateLogicFinishGame();
	logic->setGameWorld(this);
	mStateLogicMap[State::FinishGame] = logic;
}

StateLogic *GameWorld::getStateLogic(State state)
{
	return mStateLogicMap[state];
}

void GameWorld::setNewState(State newState)
{
	mStateLogic = getStateLogic(newState);
	mState = newState;
	if(mStateLogic) {
		mStateLogic->enterState();
	}
}

void GameWorld::resetTutorialState()
{
	StateLogic *logic = getStateLogic(State::Tutorial);
	if(logic == nullptr) {
		return;
	}
	StateLogicTutorial *tutorialState = dynamic_cast<StateLogicTutorial *>(logic);
	if(tutorialState != nullptr) {
		tutorialState->reset();
	}
}

void GameWorld::resetState()
{
	mStateLogic = nullptr;
	mState = State::NotStart;
	mNextState = State::None;
    mLineNumberDrawn = 0;
    mEnergyPotionUsed = 0;
}

void GameWorld::setUserTouchEnable(bool enable)
{
	mGameTouchLayer->setEnable(enable);
}


void GameWorld::setParallaxLayer(ParallaxLayer *layer)
{
	mParallaxLayer = layer;
}


#pragma mark - Information
// Distance
float GameWorld::getDistanceToPlayer(const Vec2 &myPosition)
{
	Player *player = getPlayer();
	if(player == nullptr) {
		return 100000;		// An impossible value
	}
	
	return myPosition.getDistance(player->getPosition());
}

float GameWorld::getYDistanceToPlayer(const Vec2 &myPosition)
{
	Player *player = getPlayer();
	if(player == nullptr) {
		return 100000;		// An impossible value
	}
	
	return fabs(myPosition.y - player->getPosition().y);
}

float GameWorld::getXDistanceToPlayer(const Vec2 &myPosition)
{
	Player *player = getPlayer();
	if(player == nullptr) {
		return 100000;		// An impossible value
	}
	
	return fabs(myPosition.x - player->getPosition().x);
}

float GameWorld::getTopMapY()
{
	Size screenSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	
	float mapTopY = screenSize.height + getCameraY();
	
	
	return mapTopY;
}


float GameWorld::getBottomMapY()
{
	return getCameraY();
}


void GameWorld::addItemEffectUI(Node *node)
{
	if(node == nullptr) {
		return;
	}
    Vec2 pos;
    
    int planetID = PlanetManager::instance()->getSelectedPlanet();
    PlanetData* planetData = PlanetManager::instance()->getPlanetData(planetID);
    
    if(planetData->getPlanetType() == PlanetData::Event){
        pos = mItemIndicatorPosWithCandyStick;
    }else{
        pos = mItemIndicatorPos;
    }
    
	node->setPosition(pos);
	node->setTag(kTagItemEffectUI);
	if(mEffectUILayer) {
		mEffectUILayer->addChild(node, kZOrderItemEffectUI);
	}
}

void GameWorld::cleanupItemEffectUI()
{
	if(mEffectUILayer) {
		mEffectUILayer->removeChildByTag(kTagItemEffectUI);
	}
}


void GameWorld::setStatus(Status status)
{
	mStatus = status;
}

void GameWorld::unsetStatus(Status status)
{
	if(mStatus == status) {
		mStatus = Status::Normal;
	}
	
}

void GameWorld::resetStatus()
{
	mStatus = Status::Normal;
}

GameWorld::Status GameWorld::getStatus()
{
	return mStatus;
}


bool GameWorld::isRectVisible(const Rect &myRect)
{
	Rect screenRect = VisibleRect::getVisibleRect();
	
	screenRect.origin.y = getBottomMapY();
	
	return myRect.intersectsRect(screenRect);
}


void GameWorld::setModelLayer(ModelLayer *layer)
{
	mModelLayer = layer;
}

GameTouchLayer*  GameWorld::getGameTouchLayer()
{
    return mGameTouchLayer;
}

EffectLayer *GameWorld::getEffectLayer()
{
	return mEffectLayer;
}

void GameWorld::activateCapsule()
{
	log("Activate Capsule");
	Player *player = getPlayer();
	if(player == nullptr) {
		return;
	}
	
	//if(player->get)
	
	if(getPlayer()) {
		getPlayer()->activateCapsule();
	}
//	disableCapsuleButton();		// when capsule is used, remove the capsule button immediately
}

void GameWorld::enableCapsuleButton(int itemEffect)
{
	ItemEffect::Type type = (ItemEffect::Type) itemEffect;
	if(ItemEffect::isActiveable(type) == false) {
		return;
	}
	
	if(mUILayer) {
		mUILayer->enableCapsuleButton(itemEffect);
	}
}

void GameWorld::disableCapsuleButton(){
	if(mUILayer) {
		mUILayer->disableCapsuleButton();
	}
}

bool GameWorld::isGameRunning()
{
	return mState == State::Running;
}

void GameWorld::revivePlayer()
{
	mReviveCount++;
	
	// Show the UI again
	if(getGameUILayer()) {
		getGameUILayer()->showGameUI();
	}
	
	getModelLayer()->resumeAllModel();
	
	//
	getPlayer()->resetPlayerEnergy();
	sendEvent(EventUpdateBarEnergy, getPlayer()->getPlayerEnergy());
	getPlayer()->setupDefaultItemEffect();
	
	getPlayer()->setReviving(2, true);
	
	
	changeState(GameWorld::State::Running);
}

void GameWorld::resetInitialMapOffset()
{
	setInitialMapOffset(kInitialMapDataOffsetY);
}


#pragma mark - PlayerGameResult
PlayerGameResult *GameWorld::getGameResult()
{
	return mGameResult;
}

void GameWorld::resetGameResult()
{
	int selectDog = DogManager::instance()->getSelectedDogID();
	int selectStage = StageManager::instance()->getCurrentStage();
	
	mGameResult->setDogPlayed(selectDog);
	mGameResult->setPlanetPlayed(selectStage);
	mGameResult->reset();
}

void GameWorld::updatePlayerRecord()
{
	//
	if(mGameResult->getPlanetPlayed() == 999) {			// No need to update tutorial stage
		return;
	}
	
	mGameResult->setKill(mKillCount);
	mGameResult->setStarCollected(mCollectedCoin);
	mGameResult->setDistance(getPlayerDistance());

	handleBonusScore();
	
	mCurrentScore = mGameResult->getTotalScore();
	
	//
	// log("PlayerGameResult: %s", mGameResult->toString().c_str());
	
	//
	GameManager::instance()->updatePlayerRecord(mGameResult);
    GameManager::instance()->getPlayerRecord()->updatePlayTimeRecord();
    
	
	log("PlayerRecord:\n%s\n", GameManager::instance()->infoPlayerRecord().c_str());
	
	
		
}


void GameWorld::updateScore()
{
	// TODO
	// getGameUILayer()
	GameSceneLayer *uiLayer = getGameUILayer();
	if(uiLayer != nullptr) {
		uiLayer->updateScore(getPlayerScore());
	}
}

void GameWorld::updateCoins(bool animation)
{
	// TODO
	// getGameUILayer()
	GameSceneLayer *uiLayer = getGameUILayer();
	if(uiLayer != nullptr) {
		uiLayer->updateCoins(animation);
	}
}

void GameWorld::updateCandy(bool animation)
{
    GameSceneLayer *uiLayer = getGameUILayer();
    if(uiLayer != nullptr) {
        uiLayer->updateCandy(animation);
    }
}



void GameWorld::addScoreWithGUI(int score, ScoreType type, bool showAnimation)
{
	addScore(score, type);
	int newScore = getPlayerScore();
	GameSceneLayer *uiLayer = getGameUILayer();
	if(uiLayer != nullptr) {
		uiLayer->updateScore(newScore, showAnimation);
	}
}

void GameWorld::addScoreWithAnimation(int score, ScoreType type,
									  const float &duration,
									  const Vec2 &startPos)
{
	
	addScore(score, type);
	int newScore = getPlayerScore();
	
	Vec2 pos = (startPos == Vec2::ZERO) ? VisibleRect::center() : startPos;
	
	// log("GameWorld: score=%d newScore=%d", score, newScore);
	
//	// Show the fly text
	std::function<void()> callback = [&, newScore]() {
		// Tell the GUI to popup the score
		GameSceneLayer *uiLayer = getGameUILayer();
		if(uiLayer != nullptr) {
			uiLayer->updateScore(newScore, true);
		}
	};
	mEffectLayer->runAddScoreAnimation(score, pos, callback, duration);
	
	
}

void GameWorld::addScoreFromModel(GameModel *fromModel, ScoreType scoreType, int scoreValue)
{
	Vec2 pos = fromModel->convertToWorldSpaceAR(Vec2::ZERO);
	// Add Score
	GameWorld::instance()->addScoreWithAnimation(scoreValue, scoreType, 0.7f, pos);	// 10 is monster score
}

void GameWorld::addCoinFromModel(GameModel *fromModel, int money)
{
	Vec2 pos = fromModel->convertToWorldSpaceAR(Vec2::ZERO);
	// Add Score
	GameWorld::instance()->addCoinWithAnimation(money, pos);
}

void GameWorld::addScore(int addScore, ScoreType type)
{
	mGameResult->addScore(addScore, type);
	
	int score = mCurrentScore + addScore;
	

	
	mCurrentScore = score;
}

void GameWorld::updateScore(bool animation)
{
    int currentDistance = getPlayerDistance();
    int currentCollectedStar = getPlayerCollectedCoins();
    
    if(currentDistance>mCurrentBestDistance){
        mCurrentBestDistance = currentDistance;
    }
    
    int score = mCurrentBestDistance + currentCollectedStar;
    if(score<0){
        score = 0;
    }
    
    GameSceneLayer *uiLayer = getGameUILayer();
    if(uiLayer != nullptr) {
        uiLayer->updateScore(score,animation);
    }

    mCurrentScore = score;
}


void GameWorld::addCoinWithScore(int coin)
{
	addCoin(coin);
	
	addScoreWithGUI(coin, ScoreTypeCollect, true);
}

void GameWorld::addCoin(int coin)
{
	mCollectedCoin += coin;
	
	// Add player coin record
	GameManager::instance()->getUserData()->addCoin(coin, false);
}

void GameWorld::addCoinWithAnimation(int addValue, const Vec2 &startPos, float duration)
{
	addCoinWithScore(addValue);
	
	int newTotal = mCollectedCoin;
	
	if(duration > 0) {
		// Do the animation
		std::function<void()> callback = [&, newTotal]() {
			// Tell the GUI to popup the score
			GameSceneLayer *uiLayer = getGameUILayer();
			if(uiLayer != nullptr) {
				uiLayer->updateCoinWithValue(newTotal, true);
			}
		};
		
		mEffectLayer->runAddCoinAnimation(startPos, callback, duration);
	} else {
		GameSceneLayer *uiLayer = getGameUILayer();
		if(uiLayer != nullptr) {
			uiLayer->updateCoinWithValue(newTotal, true);
		}
	}
	
	
	// int new
}

void GameWorld::addMultipleCoinWithAnimation(const std::vector<int> coinValues,
							const Vec2 &startPos, float baseDuration, const int &randomRadius)
{
	float duration = baseDuration;
	for(int coin : coinValues) {
		float offsetX = RandomHelper::random_int(-randomRadius, randomRadius) * 5;
		float offsetY = RandomHelper::random_int(-randomRadius, randomRadius) * 5;
		
		Vec2 pos = startPos + Vec2(offsetX, offsetY);

		addCoinWithAnimation(coin, pos, duration);
		
		duration += 0.05f;
	}
}



void GameWorld::collectCoin(int numCoin)
{
	// update the collected coin record
    if(mDoubleCoins){
        numCoin *= 2;
    }
	
	addCoinWithAnimation(numCoin, Vec2::ZERO, 0);	// No fly coin animation
	
//	mCollectedCoin += numCoin;
//	
//	
//	
//	// Add player coin record
//	GameManager::instance()->getUserData()->addCoin(numCoin, false);
//	// Update the GUI
//	updateCoins();
}

void GameWorld::collectCandy()
{
    // update the collected coin record
    mCollectedCandy += 1;
    
    // Add player candy record
    GameManager::instance()->getUserData()->addCandy(1);
    
    // Update the GUI
    updateCandy();
}

void GameWorld::addKill()
{
	mKillCount++;
}

int GameWorld::getBonusStar()
{
	int distance = getPlayerDistance();
	if(distance < kBonusStarDistanceThreshold) {
		return 0;
	}
	
	return distance;
}

int GameWorld::getLastBestDistance()
{
	return mLastBestDistance;
}


void GameWorld::setStage(int stage)
{
	mStage = stage;
	mMap->setStage(stage);
}


void GameWorld::setMapRouteLine(int resID)
{
	if(mMap) {
		mMap->setStage(resID);
	}
}


NPCMapLogic *GameWorld::getMapLogic()
{
	return mMapLogic;
}


#pragma mark - Update Remain Time
void GameWorld::resumeGameClock()
{
	mIsCountingTime = true;
}

void GameWorld::pauseGameClock()
{
	mIsCountingTime = false;
}
void GameWorld::startClockCountDown()
{
	getGameUILayer()->startClockCountDownEffect();
	mCountDownStarted = true;
}

void GameWorld::stopClockCountDown()
{
	getGameUILayer()->stopClockCountDownEffect();
	mCountDownStarted = false;
}

void GameWorld::addRemainTime(float secToAdd)
{
	mRemainTime += secToAdd;
	
}


void GameWorld::updateRemainTime(float delta)
{
    if(!mIsCountingTime){
        return;
    }
	
	if(getPlayer()->isReviving()) {
		return;
	}
    
	mRemainTime -= delta;
	
	if(mRemainTime < 10) {
		if(mCountDownStarted == false) {
			startClockCountDown();
		}
	}

}


void GameWorld::pauseTime()
{
    mIsCountingTime = false;
}

void GameWorld::resumeTime()
{
    mIsCountingTime = true;
}

void GameWorld::shakeBackground(float duration)
{
    mParallaxLayer->shake(duration);
}

#pragma mark - Chaser
void GameWorld::resetChaser()
{
	getModelLayer()->resetChaserEnemy();
	

}

void GameWorld::activateChaser()
{
	getModelLayer()->setChaserEnemyActive();
}

void GameWorld::deActivateChaser()
{
	getModelLayer()->setChaserEnemyInActive();
}

void GameWorld::adjustChaser()
{
	getModelLayer()->adjustChaserPosition();
}




#pragma mark - Collected Coin (Star) Logic
void GameWorld::collectPuzzle()
{
	mCollectedPuzzle++;
	if(getGameUILayer() != nullptr) {
		getGameUILayer()->updatePuzzleCount();
	}
	//if(getUILayer()		// update the gui
}

int GameWorld::getPuzzleCount()
{
	return mCollectedPuzzle;
}

void GameWorld::resetPuzzleCount()
{
	mCollectedPuzzle = 0;
}


#pragma mark - Round Time Elapse
void GameWorld::addTimeElapse(float secToAdd)
{
	mTimeElapse += secToAdd;
}

float GameWorld::getTimeElapse()
{
	return mTimeElapse;
}
void GameWorld::resetTimeElapse()
{
	mTimeElapse = 0;
	getGameUILayer()->updateTimeUsed();
}

void GameWorld::updateGameClock(float delta)
{
    if(!mIsCountingTime){
        return;
    }
    
	addTimeElapse(delta);
	getGameUILayer()->updateTimeUsed();
}

#pragma mark - Gameplay setting and Player Attribute
void GameWorld::updatePlayerMainAttribute()
{
	
	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
	profile->updateWithSetting(mGameplaySetting);	// speed, acceleration, ... changed
	
	
	Player *player = getPlayer();
	
	player->setMaxSpeed(profile->getMaxSpeed());
	player->setTurnSpeed(profile->getTurnSpeed());
	player->setAcceleration(profile->getAcceleration());
	player->setStunReduction(profile->getStunReduction());
	
	log("Attribute: %s", player->infoMainAttribute().c_str());
	
}

void GameWorld::setGameplaySetting(GameplaySetting *newSetting)
{
	if(newSetting == nullptr) {
		log("GameWorld.setGameplaySetting: setting is null");
		return;
	}
	
	// Reset the current Data
	GameplaySetting *oldSetting = mGameplaySetting;
	
	// update the new setting
	newSetting->retain();
	mGameplaySetting = newSetting;
	
	// release the oldsetting
	CC_SAFE_RELEASE(oldSetting);
	
	// Update the reference in StageManager
	StageManager::instance()->setGameplaySetting(mGameplaySetting);
	
	// Update the internal item setting
	ItemManager::instance()->setupBoosterGameSetting();
}

void GameWorld::updateGameplaySetting(bool forTutorial)
{
	GameplaySetting *newSetting;
 
	if(forTutorial) {
		newSetting = GameplaySetting::create();	// an empty setting
		newSetting->setMaxSpeed(200);
		newSetting->setAcceleration(120);
	} else {
		newSetting = PlayerManager::instance()->getCurrentGameplaySetting();
	}
	
	if(newSetting == nullptr) {
		return;
	}
	setGameplaySetting(newSetting);
	
	
	// Setting corresponding Module
	//mModelLayer->testDefendAttribute();	// For testing
	updatePlayerMainAttribute();
	
	
	mModelLayer->setupWithGameplaySetting(mGameplaySetting);
	
	// For Testing
	// mGameplaySetting->setAttributeValue(GameplaySetting::StartPowerUp, ItemEffect::ItemEffectSpeedUp);
	// mGameplaySetting->setAttributeValue(GameplaySetting::StartBooster, BoosterItemUnbreakable);
	// mGameplaySetting->setAttributeValue(GameplaySetting::BonusScore, 0.5);
}

GameplaySetting *GameWorld::getGameplaySetting()
{
	return mGameplaySetting;
}

#pragma mark - Bonus Score
void GameWorld::handleBonusScore()
{
	if(mGameplaySetting == nullptr) {
		return;
	}
	
	float bonusPercent = mGameplaySetting->getAttributeValue(GameplaySetting::BonusScore);
	mGameResult->setBonusScore(bonusPercent / 100);
}


#pragma mark - Starting PowerUp
#pragma mark - Starting ItemEffect( Booster or PowerUp)
//public:
//void setupStartingItemEffect();
//void applyStartingItemEffect();
//


void GameWorld::setupStartingItemEffect()
{
	if(mGameplaySetting == nullptr) {
		mStartingBooster = 0;
		mStartingPowerUp = 0;
		return;
	}
	
	mStartingBooster = (int) mGameplaySetting->getAttributeValue(GameplaySetting::StartBooster);
	mStartingPowerUp = (int) mGameplaySetting->getAttributeValue(GameplaySetting::StartPowerUp);

	mStartingItemEffectApplied = false;
}

void GameWorld::applyStartingItemEffect()
{
	if(mStartingBooster == 0 && mStartingPowerUp == 0) {
		return;
	}
	
	if(mStartingItemEffectApplied) {	// already applied
		return;
	}
	
	if(mStartingBooster > 0) {
		applyStartingBooster(mStartingBooster);
	} else if(mStartingPowerUp > 0) {
		applyStartingPowerUp(mStartingPowerUp);
	}
	
	mStartingItemEffectApplied = true;
	
}

void GameWorld::applyStartingBooster(int value)
{
	getPlayer()->activateBooster((BoosterItemType) value);
}

void GameWorld::applyStartingPowerUp(int value)
{
	getPlayer()->activatePowerUpByType((ItemEffect::Type) value);
}

#pragma mark - Reward Coin and Score
int GameWorld::getKillReward(bool isCashoutEffect)
{
	// Ability Effect
	GameplaySetting *setting = getGameplaySetting();
	
	int reward = setting == nullptr ? 0 : setting->getAttributeValue(GameplaySetting::KillEnemyReward);
	if(isCashoutEffect) {
		reward += 1;
	}
	
	return reward;
}


std::string GameWorld::statInfo()
{
	std::string info = mModelLayer->statInfo();
	
	info += " " + mEffectLayer->statInfo();
	info += " " + mGameTouchLayer->statInfo();
	if(mUILayer != nullptr) {
		info += " " + mUILayer->statInfo();
	}
	
	return info;
}
