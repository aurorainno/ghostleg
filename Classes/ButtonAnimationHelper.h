//
//  ButtonAnimationHelper.hpp
//  GhostLeg
//
//  Created by Calvin on 18/1/2017.
//
//

#ifndef ButtonAnimationHelper_hpp
#define ButtonAnimationHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;
using namespace cocos2d;

class ButtonAnimationHelper {
public:
    static void setButtonAnimation(Button* btn,float duration, Vec2 oriPos, Vec2 targetPos);
    

    
};



#endif /* ButtonAnimationHelper_hpp */
