//
//  ParallaxTileLayer.cpp
//  TapToJump
//
//  Created by Ken Lee on 10/7/15.
//
//

#include "ParallaxLayer.h"
#include "ParallaxSubLayer.h"
#include "SpriteParallaxSubLayer.h"
#include "ActionShake.h"

namespace {	// TODO: Turn Layer creation to configuration
//	ParallaxTileSubLayer *createBackLayer(const Size &parentSize, const float speed) {
//		ParallaxTileSubLayer *layer = new ParallaxTileSubLayer();
//		
//		std::vector<int> pattern;
//		
//		
//		int numCol = 3;
//		float scale = 0.7f;
//		float tileSize = 256;	// size of the tile
//		float scaleTileSize = tileSize * scale;
//		float totalWidth = numCol * scaleTileSize;
//		
//		layer->init(Color4B::BLACK, Size(totalWidth, parentSize.height),
//					pattern, numCol, tileSize, scale, speed);
//		float x = (parentSize.width - totalWidth) / 2;
//		
//		layer->setPosition(Vec2(x, 0));
//		
//		layer->autorelease();
//		
//		return layer;
//	};
//	
//	ParallaxTileSubLayer *createFrontLayer(const Size &parentSize, const float speed) {
//		ParallaxTileSubLayer *layer = new ParallaxTileSubLayer();
//		
//		std::vector<int> pattern;
////		
////		//
////		pattern.push_back(eResBgLeft);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResBgRight);
////		
////		pattern.push_back(eResBgRight);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResNone);
////		pattern.push_back(eResBgLeft);
////		
////		
////
//		
//		
//		
//		//
//		
//		// Create the layer and return
//		int numCol = 6;
//		float tileSize = 70;
//		
//		layer->init(Color4B(0, 0, 0, 0), Size(numCol * tileSize, parentSize.height),
//					pattern, numCol, tileSize, 1.0f, speed);
//		float x = (parentSize.width - numCol * tileSize) / 2;
//		
//		layer->setPosition(Vec2(x, 0));
//		
//		layer->autorelease();
//		
//		return layer;
//
//	};
}

ParallaxLayer *ParallaxLayer::create()
{
	const Size size = Director::getInstance()->getVisibleSize();
	return create(Color4B::GRAY, size);
}

ParallaxLayer *ParallaxLayer::create(const Color4B &color, const Size &size)
{
	ParallaxLayer *layer = new ParallaxLayer();
	if(layer->init(color, size) == false) {
		layer->release();
		return nullptr;
	}
	
	layer->autorelease();
	
	return layer;
}


bool ParallaxLayer::init(const Color4B &color, const Size &size)

{
	bool isOk = LayerColor::initWithColor(color, size.width, size.height);
	if (isOk == false)
	{
		return false;
	}
	
	mScrollX = 0;
	mScrollY = 0;
	mAutoScrollSpeed = 0;
	mAutoScrollStarted = false;
	
	mSubLayerList.clear();
	return true;
}

void ParallaxLayer::shake(float duration)
{
    //StageParallaxLayer
    Shake *shake = Shake::create(duration, 10);
    mBackgroundLayer->runAction(shake);
}

void ParallaxLayer::addSubLayer(ParallaxSubLayer *layer)
{
	if(layer == nullptr) {
		return;
	}
	
	mSubLayerList.pushBack(layer);
	this->addChild(layer);
}

ParallaxLayer::~ParallaxLayer()
{
	mSubLayerList.clear();
}

void ParallaxLayer::setScrollY(float y)
{
	mScrollY = y;
	for(int i=0; i<mSubLayerList.size(); i++) {
		ParallaxSubLayer *layer = mSubLayerList.at(i);
		layer->setScrollY(mScrollY);
	}
}

void ParallaxLayer::setScrollX(float x)
{
	mScrollX = x;
	for(int i=0; i<mSubLayerList.size(); i++) {
		ParallaxSubLayer *layer = mSubLayerList.at(i);
		layer->setScrollX(mScrollX);
	}
}

Vec3 ParallaxLayer::getPosition3D() const
{
	return Vec3(0, mScrollY, 0);
}
void ParallaxLayer::setPosition3D(const Vec3 &position)
{
	setPositionZ(position.z);
	setPosition(Vec2(position.x, position.y));
}

void ParallaxLayer::setPosition(const Vec2 &position)
{
	setScrollY(position.y);
}

void ParallaxLayer::setLayerPosition(const Vec2 &position)
{
	Node::setPosition(position);
}

void ParallaxLayer::setAutoScroll(float speed)
{
	mAutoScrollSpeed = speed;
}

void ParallaxLayer::onEnter()
{
	Layer::onEnter();
	
	if(mAutoScrollSpeed > 0) {
		scheduleUpdate();
		mAutoScrollStarted = true;
	}
}

void ParallaxLayer::update(float delta)
{
	// log("ParallaxLayer: update. speed=%f", mAutoScrollSpeed);
	setScrollY(mAutoScrollSpeed * delta);
}

void ParallaxLayer::onExit()
{
	if(mAutoScrollStarted) {
		unscheduleUpdate();
	}
	
	Layer::onExit();
}
