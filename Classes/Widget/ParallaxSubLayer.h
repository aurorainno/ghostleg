//
//  ParallaxTileSubLayer.h
//  TapToJump
//
//  Created by Ken Lee on 7/7/15.
//
//

#ifndef __TapToJump__ParallaxTileSubLayer__
#define __TapToJump__ParallaxTileSubLayer__

#include <stdio.h>
#include <stdlib.h>
#include "cocos2d.h"

USING_NS_CC;

class ParallaxSubLayer : public LayerColor
{
public:
	virtual bool init(const Size &parentSize, const Vec2 &speed);
	virtual ~ParallaxSubLayer();
	CC_SYNTHESIZE(float, mSpeedY, SpeedY);
	CC_SYNTHESIZE(float, mSpeedX, SpeedX);
	
	void setInfiniteSetting(bool infiniteX, bool infiniteY);
	
	virtual void setScrollX(float x);
	virtual void setScrollY(float y);
	
protected:
	Size mParentSize;
	Size mSectionSize;
	CC_SYNTHESIZE(Vec2, mOriginOffset, OriginOffset);
	bool mIsFiniteX;
	bool mIsFiniteY;
	
	
protected:
	void defineSectionSize(const Size &size);	// Must define
	
};

#endif /* defined(__TapToJump__ParallaxTileSubLayer__) */
