//
//  ADPageView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/10/2016.
//
//

#ifndef ADPageView_hpp
#define ADPageView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;

class AILPageView : public PageView
{
public:
	//typedef std::function<void(Ref*, EventType)> ccPageViewCallback;
	typedef std::function<void(Ref *ref, Vec2 scrollOffset)> ViewScrollCallback;
	
	
	
public:
	CREATE_FUNC(AILPageView);
	
	AILPageView();

	
	virtual void addEventListener(const ccPageViewCallback& callback);


#pragma mark - Scroll
public:
	virtual void setScrollCallback(const ViewScrollCallback &callback);

private:
	void onViewScrolled();
	
private:
	ViewScrollCallback mScrollCallback;
};

#endif /* ADPageView_hpp */
