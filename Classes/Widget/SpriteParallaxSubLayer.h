//
//  SpriteParallaxSubLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 5/5/2016.
//
//

#ifndef SpriteParallaxSubLayer_hpp
#define SpriteParallaxSubLayer_hpp

#include <stdio.h>
#include <vector>

#include "ParallaxSubLayer.h"

class SpriteParallaxSubLayer : public ParallaxSubLayer
{
public:
	void setSpriteByName(const std::string &name, float scale=1.0f, float spacing=0);
	void setTintColor(const Color3B &color);
	virtual void setOpacity(GLubyte opacity) override;

	void setupParallax(const std::string &headerSprite,
				  const std::string &repeatSprite,
				  float scale);
	virtual void setScrollY(float y) override;
	
private:
	void fillSprite(std::vector<Sprite *> spriteArray, const Size &spriteSize, float spacing);
	void addSpriteToLayer(
					 const std::string &headerSprite,
					 const std::string &sectionSprite,
					 int numSection,
					 float scale);
private:
	float mHeaderHeight;

};

#endif /* SpriteParallaxSubLayer_hpp */
