//
//  AnimeNode.hpp
//  GhostLeg
//
//	A handy Node that help to play one time or loop animation with callback support
//
//  Created by Ken Lee on 23/11/2016.
//
//

#ifndef AnimeNode_hpp
#define AnimeNode_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;
using namespace cocostudio::timeline;

class AnimeNode : public Node
{
public:
	typedef std::function<void(Ref *sender, std::string animeName)> PlayEndCallback;
public:
	CREATE_FUNC(AnimeNode);
	
	AnimeNode();
	
	bool init();
	
	void setup(const std::string &csbName);
	void setStartAnime(const std::string &animeName,
			   bool isLoopAnimation = false,
			   bool autoRemove = true);
	
	void setPlayEndCallback(const PlayEndCallback &callback);
	
	void playAnimation(const std::string &name, bool isLoop, bool autoRemove=false);
    
    void stopAnimation();
    
    void setFrameEventCallback(std::function<void(EventFrame* event)> callback);
	
	void setTimeSpeed(float speed);

	virtual void onEnter();
	
private:
	void onPlayEnd(const std::string &name, bool needRemove);
	void playStartAnimation();
	
protected:
	Node *mRootNode;
	cocostudio::timeline::ActionTimeline *mTimeline;
    std::function<void(EventFrame*)> mFrameEventCallback;
	
	// For the start animation
	std::string mStartAnimeName;		// the name of the animation to be played when onEnter
	bool mStartAnimeLoop;				// is the starting animation loop??
	bool mStartAutoRemoveAfterPlayed;	// Auto remove the play the animation
	
	PlayEndCallback mPlayEndCallback;
};

#endif /* AnimeNode_hpp */
