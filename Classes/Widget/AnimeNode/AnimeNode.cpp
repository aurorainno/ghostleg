//
//  AnimeNode.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/11/2016.
//
//

#include "AnimeNode.h"
#include "ViewHelper.h"
#include "CommonMacro.h"

using namespace cocostudio::timeline;

AnimeNode::AnimeNode()
: mRootNode(nullptr)
, mStartAnimeName("")
, mTimeline(nullptr)
, mPlayEndCallback()
, mFrameEventCallback()
{
	
}


bool AnimeNode::init()
{
	if(Node::init() == false) {
		return false;
	}
	
	return true;
}


//void setup(const std::string &csbName);

void AnimeNode::setup(const std::string &csbName)
{
    
    //clean up first
    removeAllChildren();
    
	// Create the csb node and add it here!!
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);
	FIX_UI_LAYOUT(rootNode);
	
	mRootNode = rootNode;
	
	
	// Setup the animation timeline
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
    
    mTimeline->setFrameEventCallFunc([&](Frame *frame) {
        EventFrame *event = dynamic_cast<EventFrame*>(frame);
        if(!event) {
            return;
        }
        if(mFrameEventCallback){
            mFrameEventCallback(event);
        }
    });
}

void AnimeNode::setFrameEventCallback(std::function<void(EventFrame* event)> callback)
{
    mFrameEventCallback = callback;
}


void AnimeNode::setStartAnime(const std::string &animeName,
				   bool isLoopAnimation,
				   bool autoRemove)
{
	mStartAnimeName = animeName;
	mStartAnimeLoop = isLoopAnimation;
	mStartAutoRemoveAfterPlayed = autoRemove;
}

void AnimeNode::playAnimation(const std::string &name, bool isLoop, bool autoRemove)
{
	if(mTimeline == nullptr) {
		return;
	}
	
	
	if(isLoop == false) {
		mTimeline->setAnimationEndCallFunc(name, [&, name, autoRemove]() {
			onPlayEnd(name, autoRemove);
		});
	}
	
	mTimeline->play(name, isLoop);
	
}

void AnimeNode::stopAnimation()
{
    if(mTimeline){
        mTimeline->stop();
    }
}


void AnimeNode::setPlayEndCallback(const PlayEndCallback &callback)
{
	mPlayEndCallback = callback;
}

void AnimeNode::onPlayEnd(const std::string &name, bool needRemove)
{
//	log("AnimeNode.onPlayEnd: called.");
	
	if(mPlayEndCallback) {
		mPlayEndCallback(this, name);
	}
	
	
	if(needRemove) {
		removeFromParent();
	}
}

void AnimeNode::playStartAnimation()
{
	if(mStartAnimeName == "") {
		return;
	}
	
	playAnimation(mStartAnimeName, mStartAnimeLoop, mStartAutoRemoveAfterPlayed);
}

void AnimeNode::onEnter()
{
	Node::onEnter();
	
	playStartAnimation();
}


void AnimeNode::setTimeSpeed(float speed)
{
	if(mTimeline != nullptr) {
		mTimeline->setTimeSpeed(speed);
	}
}
