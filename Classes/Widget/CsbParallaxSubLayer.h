//
//  CsbParallaxSubLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 19/10/2016.
//
//

#ifndef CsbParallaxSubLayer_hpp
#define CsbParallaxSubLayer_hpp

#include <stdio.h>
#include "ParallaxSubLayer.h"

class CsbParallaxSubLayer : public ParallaxSubLayer
{
public:
	
	
	void setupCsbByName(const std::string &name);
	
	void setSpriteByName(const std::string &name, float scale=1.0f, float spacing=0);
	void setTintColor(const Color3B &color);
	virtual void setOpacity(GLubyte opacity) override;
	
	void setupParallax(const std::string &headerSprite,
					   const std::string &repeatSprite,
					   float scale);
	virtual void setScrollY(float y) override;
	
private:
	void fillSprite(std::vector<Sprite *> spriteArray, const Size &spriteSize, float spacing);
	void addSpriteToLayer(
						  const std::string &headerSprite,
						  const std::string &sectionSprite,
						  int numSection,
						  float scale);
	
	void addCsbToLayer(const std::string &csbName, int numSection);
private:
	float mHeaderHeight;
	
};

#endif /* CsbParallaxSubLayer_hpp */
