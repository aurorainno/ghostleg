//
//  FireworkLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 29/11/2016.
//
//

#include "FireworkLayer.h"
#include "cocostudio/CocoStudio.h"


using namespace cocos2d::ui;


FireworkLayer::FireworkLayer()
: mInterval(1.0f)
, mCooldown(0)
, mCounter(0)
{
	
}

bool FireworkLayer::init()
{
	if(Layout::init() == false) {
		return false;
	}
	
	std::string csb = "animation/FireworkLayer.csb";
	setupCsb(csb);
	
	
	return true;
}


void FireworkLayer::setupCsb(const std::string &csbName)
{
	Node *rootNode = CSLoader::createNode(csbName);
	addChild(rootNode);

	setContentSize(rootNode->getContentSize());		// update the layout size the csb size
	
	
	// Define the EmitterList and
	// By default
	int numFirework = 8;
	
	
	for(int i=1; i<=numFirework; i++) {
		std::string nodeName = StringUtils::format("firework%d", i);
		
		ParticleSystemQuad *particle = rootNode->getChildByName<ParticleSystemQuad *>(nodeName);
		
		mEmitterList.pushBack(particle);
	}
	
	// Prevent all emitter being activated
	for(ParticleSystemQuad *particle : mEmitterList) {
		particle->stopSystem();
	}
}

void FireworkLayer::update(float delta)
{
	mCooldown -= delta;
	
	if(mCooldown > 0) {	// Not yet activate
		return;
	}

	mCooldown = mInterval;
	activateEmitter(mCounter);
	updateCounter();
}

void FireworkLayer::startFirework()
{
	scheduleUpdate();
}

void FireworkLayer::stopFirework()
{
	unscheduleUpdate();
	
	// Prevent all emitter being activated
	for(ParticleSystemQuad *particle : mEmitterList) {
		particle->stopSystem();
	}
}

void FireworkLayer::activateEmitter(int index)
{
	if(index < 0 || index >= mEmitterList.size()) {
		log("FireworkLayer::activateEmitter: out of bound. index=%d", index);
		return;
	}
	
	ParticleSystemQuad *particle = mEmitterList.at(index);
	particle->resetSystem();
}


int FireworkLayer::getEmitterCount()
{
	return (int) mEmitterList.size();
}

void FireworkLayer::updateCounter()
{
	mCounter = (mCounter + 1) % getEmitterCount();
}
