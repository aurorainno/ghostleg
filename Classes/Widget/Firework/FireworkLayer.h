//
//  FireworkLayer.hpp
//  GhostLeg
//
//  Created by Ken Lee on 29/11/2016.
//
//

#ifndef FireworkLayer_hpp
#define FireworkLayer_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class FireworkLayer : public Layout
{
public:
	CREATE_FUNC(FireworkLayer);
	
	FireworkLayer();
	
	CC_SYNTHESIZE(float, mInterval, Interval);
	
	virtual bool init();
	
	void setupCsb(const std::string &csbFile);
	
	void activateEmitter(int index);

	int getEmitterCount();
	
	
	// Automatically playing
	virtual void update(float delta);
	
	void startFirework();
	
	void stopFirework();
	
private:
	void updateCounter();
	
private:
	Vector<ParticleSystemQuad *> mEmitterList;
	
	int mCounter;
	float mCooldown;
};

#endif /* FireworkLayer_hpp */
