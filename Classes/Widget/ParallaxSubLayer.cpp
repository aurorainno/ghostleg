//
//  ParallaxTileSubLayer.cpp
//  TapToJump
//
//  Created by Ken Lee on 7/7/15.
//
//

#include <math.h>
#include "ParallaxSubLayer.h"
// #include "<#header#>"
namespace  {
	
	float getModValue(float value, float modValue)
	{
		float quotient = floorf(value / modValue);
		
		return quotient * modValue - value;
	}


}

bool ParallaxSubLayer::init(const Size &parentSize, const Vec2 &speed)										 
{
	Color4B clearColor(0, 0, 0, 0);
	mParentSize = parentSize;
	mSpeedY = speed.y;
	mSpeedX = speed.x;
	mIsFiniteX = true;
	mIsFiniteY = true;
	
	
	bool isOk = LayerColor::initWithColor(clearColor, mParentSize.width, mParentSize.height);
	if (isOk == false)
	{
		return false;
	}
	
	return true;
}

ParallaxSubLayer::~ParallaxSubLayer()
{
}

void ParallaxSubLayer::defineSectionSize(const Size &size)
{
	mSectionSize = size;
}

void ParallaxSubLayer::setInfiniteSetting(bool infiniteX, bool infiniteY)
{
	mIsFiniteX = infiniteX;
	mIsFiniteY = infiniteY;
}


void ParallaxSubLayer::setScrollX(float scrollX)
{
	float modScrollX = -scrollX * mSpeedX;
	
	
	setPositionX(modScrollX + mOriginOffset.x);

}

void ParallaxSubLayer::setScrollY(float scrollY)
{
	float modScrollY = getModValue(scrollY * mSpeedY, mSectionSize.height);
	
	setPositionY(modScrollY + mOriginOffset.y);
}