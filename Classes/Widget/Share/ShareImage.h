//
//  ShareImage.h
//  TapToJump
//
//	The template to generate Share Image
// 
//  Created by Ken Lee on 9/9/15.
//
//

#ifndef __TapToJump__ShareImage__
#define __TapToJump__ShareImage__

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d::ui;

class ShareImage : public Layer
{
public:
	CREATE_FUNC(ShareImage);
	typedef std::function<void (RenderTexture*, const std::string&)> ShareImageCallback;
	
	virtual bool init();
	void setScore(int score);
    void setStage(int stageID);
    
	RenderTexture *createRenderTexture();
	void saveToLocal();
	void saveImage(const std::string &filename, bool callbackAfterSave=true);
	
	Sprite *getSprite();		// For Testing
	
	void setCallback(const ShareImageCallback &callback);
	
	std::string getFilename();

private:
	Text *mScoreText;
    Text *mStageText;
	ShareImageCallback mCallback;
//    Sprite* mCharacter;
//    void setCharacter();
	CustomCommand asyncCommand;

	CC_SYNTHESIZE_RETAIN(RenderTexture *, mRenderTexture, RenderTexture);
	
private:
	void onSaveImageInAndroid(const std::string &filename, bool callbackAfterSave);
	void saveImageInAndroid(const std::string &filename, bool callbackAfterSave);

	
};


#endif /* defined(__TapToJump__ShareImage__) */
