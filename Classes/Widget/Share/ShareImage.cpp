//
//  ShareImage.cpp
//  TapToJump
//
//  Created by Ken Lee on 9/9/15.
//
//

#include "ShareImage.h"

#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "DeviceHelper.h"
#include "StageParallaxLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "SuitManager.h"
#include "DogManager.h"
#include "CommonMacro.h"

#define kFilename	"/monsters_ex_share.png"

using namespace cocostudio::timeline;


// on "init" you need to initialize your instance
bool ShareImage::init()
{
	//////////////////////////////
	// 1. super init first
	if ( !Layer::init() )
	{
		return false;
	}

	mCallback = nullptr;


	// Setup UI
	//

	// The background
//    StageParallaxLayer *parallaxLayer = StageParallaxLayer::create();
//	//parallaxLayer->setBackground(bg, bg);
//	parallaxLayer->setup();
//	parallaxLayer->setScrollY(1600);
//    addChild(parallaxLayer);
	// parallaxLayer->setScale(2);

	// parallaxLayer->setScrollY(scrollY);


	// The main UI
	Node *rootNode = CSLoader::createNode("ShareImage.csb");
	addChild(rootNode);

	//
	this->setContentSize(rootNode->getContentSize());

 //   mCharacter = rootNode->getChildByName<Sprite*>("character");
	mScoreText = (Text *) rootNode->getChildByName("scoreText");
    mStageText = (Text *) rootNode->getChildByName("stageText");

//    this->setCharacter();


	return true;
}

//void ShareImage::setCharacter(){
//	int dogID = DogManager::instance()->getSelectedDogID();
//
//
//	// filename dog/dselect_dog1_unlock.png
//
//	std::string textureName = StringUtils::format("dog/dselect_dog%d_unlock.png", dogID);
//
//	mCharacter->setTexture(textureName);
//}

void ShareImage::setScore(int score)
{
	char scoreStr[20];

	sprintf(scoreStr, "%d", score);

	mScoreText->setString(scoreStr);
}

void ShareImage::setStage(int stageID)
{
    char stageStr[20];

    sprintf(stageStr, "stage %d", stageID);

    mStageText->setString(stageStr);
}

Sprite *ShareImage::getSprite()
{
	RenderTexture *texture = createRenderTexture();

    Sprite *sprite = Sprite::createWithTexture(texture->getSprite()->getTexture());

	sprite->setScale(0.5, -0.5);
	//sprite->setScale(0.3, -0.3);

	return sprite;
//	int x = Director::getInstance()->getWinSize().width / 2;
//	sprite->setPosition(Vec2(x, 200));
//	addChild(sprite);
//
//	Size size = this->getContentSize();
//
//	RenderTexture *texture = RenderTexture::create(size.width, size.height,
//												   Texture2D::PixelFormat::RGBA8888);
//
//	//log("testShare: size=%f,%f", size.width, size.height);
//	// http://www.learn-cocos2d.com/2011/12/how-to-use-ccrendertexture-motion-blur-screenshots-drawing-sketches/
//
//	//this->setScaleY(-1);
//	texture->setClearColor(Color4F::BLUE);
//	texture->begin();
//	this->visit();
//	texture->end();
//
//	return texture;
}


RenderTexture *ShareImage::createRenderTexture()
{
	Size size = this->getContentSize();

	RenderTexture *texture = RenderTexture::create(size.width, size.height,
												   Texture2D::PixelFormat::RGBA8888);

	//log("testShare: size=%f,%f", size.width, size.height);
	// http://www.learn-cocos2d.com/2011/12/how-to-use-ccrendertexture-motion-blur-screenshots-drawing-sketches/

	//this->setScaleY(-1);
	//texture->setClearColor(Color4F::BLUE);
	texture->begin();
	this->visit();
	texture->end();

    texture->getSprite()->getTexture()->setAntiAliasTexParameters();
    texture->setScale(0.5);
    texture->getSprite()->setScale(0.5);


	return texture;
}

std::string ShareImage::getFilename()
{
#if IS_ANDROID
	std::string path = DeviceHelper::getStoragePath();	// /internalMemory/astrodog
	FileUtils::getInstance()->createDirectory(path);	// create the directory if not exists

	return path + kFilename;
#else
	return kFilename;
#endif
}

void ShareImage::saveToLocal()
{
	std::string filename = getFilename();

	// log("saveToLocal: %s", filename.c_str());
	saveImage(filename);
}

void ShareImage::saveImageInAndroid(const std::string &filename, bool callbackAfterSave)
{
	log("ShareImage:saveImageInAndroid");
	RenderTexture *texture = createRenderTexture();


	setRenderTexture(texture);



	// Command
	std::string outFile = filename;

	std::function<void()> commandFunc = [&, outFile, callbackAfterSave](){
		log("ShareImage:commandFunc try to save image in android");
		onSaveImageInAndroid(outFile, callbackAfterSave);
	};

	asyncCommand.init(0.0f);
	asyncCommand.func = commandFunc;

	Director::getInstance()->getRenderer()->addCommand(&asyncCommand);

//	saveCommand.func = [](void()){
//
//	};
//

	//text->get

}

void ShareImage::onSaveImageInAndroid(const std::string &filename, bool callbackAfterSave)
{
	RenderTexture *texture = getRenderTexture();
	if(texture == nullptr) {
		log("ShareImage.onSaveImageInAndroid: texture is nullptr");
		return;
	}


	Image *image = texture->newImage(true);
	image->saveToFile(filename, false);
	CC_SAFE_DELETE(image);

	if(callbackAfterSave) {
		if(mCallback) {
			mCallback(texture, filename);
		}
	}

	setRenderTexture(nullptr);
}


void ShareImage::saveImage(const std::string &filename, bool callbackAfterSave)
{
#if IS_ANDROID
	saveImageInAndroid(filename, callbackAfterSave);
#else
	RenderTexture *text = createRenderTexture();

	std::function<void (RenderTexture*, const std::string&)> callback =
										callbackAfterSave ? mCallback : nullptr;

	text->saveToFile(filename, true, callback);
#endif
}



void ShareImage::setCallback(const ShareImageCallback &callback)
{
	mCallback = callback;
}
