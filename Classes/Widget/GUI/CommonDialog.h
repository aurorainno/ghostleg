//
//  CommonDialog.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#ifndef CommonDialog_hpp
#define CommonDialog_hpp

#include <stdio.h>

#include "cocos2d.h"
#include <stdio.h>
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class CommonDialog : public Layer
{
public:
	enum Action {
		ActionCancel,		// user click the 'x'
		ActionOkay			// user click okay
	};
	
	typedef std::function<void(Ref *, Action)> ActionCallback;
	
public:
	CREATE_FUNC(CommonDialog);
	
	virtual bool init();
	
	CommonDialog();
	
	void setupUI(Node *rootNode);
	void closeDialog(Action action=ActionCancel);
	
	void setTitle(const std::string &title);
	void setContent(const std::string &content);
	
	void setActionCallback(const ActionCallback &callback);

private:
	void onOkayClicked();
	
private:
	ActionCallback mActionCallback;
	Text *mTitleText;
	Text *mContentText;
};


#endif /* CommonDialog_hpp */
