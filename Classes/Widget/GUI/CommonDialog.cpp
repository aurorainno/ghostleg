//
//  CommonDialog.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/3/2017.
//
//

#include "CommonDialog.h"

#include "cocostudio/CocoStudio.h"
#include "CommonMacro.h"
#include "ui/CocosGUI.h"
#include "ViewHelper.h"

CommonDialog::CommonDialog():
mActionCallback(nullptr)
{
}


bool CommonDialog::init()
{
	if(Layer::init() == false) {
		return false;
	}
	
	Node *rootNode = CSLoader::createNode("gui/CommonDialog.csb");
	
	setupUI(rootNode);
	
	addChild(rootNode);
	
	return true;
}

void CommonDialog::setupUI(Node *rootNode)
{
	FIX_UI_LAYOUT(rootNode);
	
	Node *mainPanel = rootNode->getChildByName("mainPanel");
	
	//
	ui::Button *backButton = (Button *) mainPanel->getChildByName("backBtn");
	if(backButton) {
		backButton->addClickEventListener([&](Ref *sender){
			closeDialog(ActionCancel);
		});
	}
	
	ui::Button *okayBtn = (Button *) mainPanel->getChildByName("okayBtn");
	if(okayBtn) {
		okayBtn->addClickEventListener([&](Ref *sender){
			closeDialog(ActionOkay);
		});
	}

	mTitleText = mainPanel->getChildByName<ui::Text *>("title");
	mContentText = mainPanel->getChildByName<ui::Text *>("content");
}

void CommonDialog::setTitle(const std::string &title)
{
	mContentText->setString(title);
}

void CommonDialog::setContent(const std::string &content)
{
	mTitleText->setString(content);
}


void CommonDialog::closeDialog(Action action)
{
	if(mActionCallback) {
		mActionCallback(this, action);
	}
	
	removeFromParent();
}


void CommonDialog::setActionCallback(const ActionCallback &callback)
{
	mActionCallback = callback;
}
