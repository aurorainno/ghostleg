//
//  CircularMaskNode.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/10/2016.
//
//

#ifndef CircularMaskNode_hpp
#define CircularMaskNode_hpp

#include <stdio.h>

#include "cocos2d.h"

USING_NS_CC;

class CircularMaskNode : public ClippingNode
{
public:
	CREATE_FUNC(CircularMaskNode);
	
	CircularMaskNode();
	
	bool init();
	
	CC_SYNTHESIZE(float, mStartAngle, StartAngle);
	CC_SYNTHESIZE(float, mEndAngle, EndAngle);
	
	void updateAngle(float start, float end);
	void updateAngleByPercent(float percent);
	
private:
	void updateStencil();
	
private:
	DrawNode *mStencil;
};


#endif /* CircularMaskNode_hpp */

