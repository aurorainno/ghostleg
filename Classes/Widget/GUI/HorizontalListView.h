//
//  HorizontalListView.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/4/2017.
//
//

#ifndef HorizontalListView_hpp
#define HorizontalListView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <stdio.h>
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class HorizontalListView : public ui::ListView
{
public:
	/**
	 * Page turn event type.
	 */
	enum class EventType
	{
		TURNING
	};
	
public:
	CREATE_FUNC(HorizontalListView);
	
	virtual bool init();
	
	HorizontalListView();
	
	void addItem(Widget *itemView);
	//void setItemSize(
	int getItemCount();
	void scrollToItem(ssize_t itemIndex, float timeToScroll=1.0f);
	void setSelectedItem(ssize_t itemIndex);
	
	ssize_t getSelectedIndex() const;
	
// Some override
protected:
	virtual void remedyLayoutParameter(Widget* item)override;
	
private:
	Widget *getLastItem();
	Vec2 getNewItemPosition();
	void calculateFirstPosition();	// Also the margin
	
private:
//	Vector<Widget *> mItemList;
	Size mItemSize;
	Vec2 mFirstPos;
	Vec2 mMargin;		// X, Y Margin (Assume left,right  top&bottom are the same)


	
#pragma mark - Scroll and Event handling
public:
	typedef std::function<void(Ref *, EventType)> EventCallback;
	typedef std::function<void(Ref *ref, Vec2 scrollOffset)> ViewScrollCallback;
public:
	virtual void addEventListener(const EventCallback& callback);
	virtual void setScrollCallback(const ViewScrollCallback &callback);
	
private:
	void onViewScrolled();
	
private:
	ViewScrollCallback mScrollCallback;
	EventCallback mEventCallback;
};

#endif /* HorizontalListView_hpp */
