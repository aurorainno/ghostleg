//
//  HorizontalListView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/4/2017.
//
//

#include "HorizontalListView.h"

#include "cocostudio/CocoStudio.h"
#include "CommonMacro.h"
#include "ui/CocosGUI.h"
#include "ViewHelper.h"

HorizontalListView::HorizontalListView()
: ui::ListView()
, mScrollCallback(nullptr)
, mEventCallback(nullptr)
{
}


bool HorizontalListView::init()
{
	if(ListView::init() == false) {
		return false;
	}
	
	setDirection(Direction::HORIZONTAL);
	setMagneticType(MagneticType::CENTER);							//
	setGravity(ListView::Gravity::CENTER_VERTICAL);		// the item are align center
	setScrollBarEnabled(false);
	
	return true;
}

Widget *HorizontalListView::getLastItem()
{
	if(getItemCount() == 0) {
		return nullptr;
	}
	
	int lastIdx = (int) (getItemCount() - 1);
	
	return getItem(lastIdx);
}

Vec2 HorizontalListView::getNewItemPosition()
{
	Widget *lastWidget = getLastItem();
	if(lastWidget == nullptr) {
		return mFirstPos;
	}
	
	Size size = lastWidget->getContentSize();
	Vec2 pos = lastWidget->getPosition();
	pos.x += size.width;
	
	return pos;
}

void HorizontalListView::calculateFirstPosition()
{
	Size contentSize = getContentSize();
	
	// First Pos
	float x = (contentSize.width - mItemSize.width) / 2;
	float y = 0;	//(contentSize.height - mItemSize.height) / 2;
	
	mFirstPos = Vec2(x, y);
	
	// Margin
	mMargin = mFirstPos;
}

void HorizontalListView::scrollToItem(ssize_t itemIndex, float timeToScroll)
{
	ListView::scrollToItem(itemIndex, Vec2::ANCHOR_MIDDLE, Vec2::ANCHOR_MIDDLE, timeToScroll);
}

void HorizontalListView::setSelectedItem(ssize_t itemIndex)
{
	ListView::jumpToItem(itemIndex, Vec2::ANCHOR_MIDDLE, Vec2::ANCHOR_MIDDLE);
}

void HorizontalListView::addItem(Widget *itemView)
{
	mItemSize = itemView->getContentSize();
	if(getItemCount() == 0) {
		calculateFirstPosition();	// Also the margin
	}
	
	//Vec2 localPos = Vec2(mItemList.size() * )
	Vec2 pos = getNewItemPosition();
	itemView->setPosition(pos);
	

	// Save the ListView
	pushBackCustomItem(itemView);
	
	
	// update the inner container size
	float lastX = pos.x + mItemSize.width + mMargin.x;
	Size innerSize = Size(lastX, getContentSize().height);
	setInnerContainerSize(innerSize);
	
	
}

void HorizontalListView::remedyLayoutParameter(Widget* item)
{
	item->setContentSize(mItemSize);
	ListView::remedyLayoutParameter(item);
}

int HorizontalListView::getItemCount()
{
	return (int) getItems().size();
}



ssize_t HorizontalListView::getSelectedIndex() const
{
	Widget* widget = ListView::getCenterItemInCurrentView();
	return getIndex(widget);
}


#pragma mark - callback handling

void HorizontalListView::addEventListener(const EventCallback &callback)
{
	mEventCallback = callback;
	
	
	ccScrollViewCallback scrollViewCallback = [=](Ref* ref, ScrollView::EventType type) -> void {
		if (type == ScrollView::EventType::AUTOSCROLL_ENDED) {	//&& _previousPageIndex != _currentPageIndex) {
			if (mEventCallback)
			{
				mEventCallback(this, EventType::TURNING);
			}
			// pageTurningEvent();
		}else if(ScrollView::EventType::SCROLLING == type) {
			onViewScrolled();
		}
	};
	ScrollView::addEventListener(scrollViewCallback);
}

void HorizontalListView::onViewScrolled()
{
	if(mScrollCallback) {
		Vec2 offset = this->getInnerContainerPosition();
		mScrollCallback(this, offset);
	}
}

void HorizontalListView::setScrollCallback(const ViewScrollCallback &callback)
{
	mScrollCallback = callback;
}
