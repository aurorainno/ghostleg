//
//  CircularMaskNode.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/10/2016.
//
//

#include "CircularMaskNode.h"


#include "ViewHelper.h"


const Size kDefaultSize = Size(100, 100);		// tempority


CircularMaskNode::CircularMaskNode()
: mStartAngle(0)
, mEndAngle(180)	// degree
, mStencil(nullptr)
{
	
}

bool CircularMaskNode::init()
{
	if(ClippingNode::init() == false) {
		return false;
	}
	//
	setContentSize(kDefaultSize);
	
	
	// Setup Stencil
	mStencil = DrawNode::create();
	mStencil->clear();
	setStencil(mStencil);
	//setInverted(true);
	
	
	// update stencil
	updateStencil();
	
	return true;
}

void CircularMaskNode::updateStencil()
{
	float radius = getContentSize().width + getContentSize().height;
	
	mStencil->clear();
	ViewHelper::drawPie(mStencil, radius, mStartAngle, mEndAngle, Color4F::RED);
}


void CircularMaskNode::updateAngle(float start, float end)
{
	
	
	setStartAngle(start);
	setEndAngle(end);
	
	updateStencil();
}

void CircularMaskNode::updateAngleByPercent(float percent)
{
	float end = mStartAngle + 360.0 * percent / 100;
	
	updateAngle(mStartAngle, end);
}


