//
//  CsbParallaxSubLayer.cpp
//  GhostLeg
//
//  Created by Ken Lee on 19/10/2016.
//
//

#include "CsbParallaxSubLayer.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "StringHelper.h"


using namespace cocostudio::timeline;
namespace {
	Size getSpriteSize(const std::string &spriteName)
	{
		Sprite *sprite = Sprite::create(spriteName);
		if(sprite == nullptr) {
			return Size::ZERO;
		}
		Size spriteSize = sprite->getContentSize();
		return spriteSize;
	}
	
	int calculateSectionCount(const Size &sectionSize, const Size &parentSize)
	{
		int numRow = (parentSize.height + sectionSize.height) / sectionSize.height + 1;
		
		return numRow;
	}
}


void CsbParallaxSubLayer::setupCsbByName(const std::string &name)
{
	Node *rootNode = CSLoader::createNode(name);
	addChild(rootNode);
	
	Size csbSize = rootNode->getContentSize();
	log("DEBUG: CsbParallax: %s", SIZE_TO_STR(csbSize).c_str());
	//Size size
	
	float contentWidth = csbSize.width;
	mSectionSize = csbSize;
	
//	Node *fileNode = rootNode->getChildByName("titleFileNode");
//	ActionTimeline *itemAction =
//	(ActionTimeline *) fileNode->getActionByTag(fileNode->getTag());
//	itemAction->play("anime1", true);
//	log("itemAction is null? %d", (itemAction == nullptr));
	int numSection = calculateSectionCount(mSectionSize, mParentSize);
	addCsbToLayer(name, numSection);
	
	
	// set x position by center
	float x = (mParentSize.width - contentWidth) / 2;
	setPositionX(x);
	
	setOriginOffset(Vec2(x, 0));
}

void CsbParallaxSubLayer::setupParallax(const std::string &headerSprite,
										   const std::string &sectionSprite,
										   float scale)
{
	Size headerSize = getSpriteSize(headerSprite) * scale;
	Size sectionSize = getSpriteSize(sectionSprite) * scale;
	
	// Set to class property
	mHeaderHeight = headerSize.height;
	defineSectionSize(sectionSize);
	
	int numSection = calculateSectionCount(sectionSize, mParentSize);
	log("debug: numSection=%d", numSection);
	
	// Define the content size
	int contentHeight = mHeaderHeight + numSection * sectionSize.height;
	int contentWidth = sectionSize.width;
	setContentSize(Size(contentWidth, contentHeight));
	
	addSpriteToLayer(headerSprite, sectionSprite, numSection, scale);
	
	
	// set x position by center
	float x = (mParentSize.width - contentWidth) / 2;
	setPositionX(x);
	
	setOriginOffset(Vec2(x, 0));
}


void CsbParallaxSubLayer::setSpriteByName(
											 const std::string &name, float scale, float spacing)
{
	mHeaderHeight = 0;
	
	Sprite *sprite = Sprite::create(name);
	
	// Error Case
	if(sprite == nullptr) {
		mSpeedX = 0;
		mSpeedY = 0;
		return;
	}
	
	Size spriteSize = sprite->getContentSize();
	Size scaledSize = spriteSize * scale;
	Size totalSize = scaledSize + Size(0, spacing);
	
	
	this->defineSectionSize(totalSize);
	
	int numRow = (mParentSize.height + totalSize.height) / totalSize.height + 1;
	
	int contentHeight = numRow * totalSize.height;
	int contentWidth = totalSize.width;
	
	std::vector<Sprite *> spriteArray;
	
	for(int i=0; i<numRow; i++) {
		Sprite *sprite = Sprite::create(name);
		sprite->setScale(scale);
		spriteArray.push_back(sprite);
	}
	
	// Fill the sprite
	fillSprite(spriteArray, scaledSize, spacing);
	
	// resize
	setContentSize(Size(contentWidth, contentHeight));
	
	// set x position by center
	float x = (mParentSize.width - contentWidth) / 2;
	setPositionX(x);
	
	setOriginOffset(Vec2(x, 0));
}


void CsbParallaxSubLayer::addSpriteToLayer(
											  const std::string &headerSprite,
											  const std::string &sectionSprite,
											  int numSection,
											  float scale)
{
	Vec2 pos(0, 0);
	Sprite *sprite;
	
	int nTotal = numSection + 1;
	
	// Adding the header
	for(int i=0; i<nTotal; i++) {
		const std::string &spriteName = i == 0 ? headerSprite : sectionSprite;
		
		sprite = Sprite::create(spriteName);
		if(sprite) {
			sprite->setScale(scale);
			sprite->setAnchorPoint(Vec2(0, 0));
			sprite->setPosition(pos);
			addChild(sprite);
			
			Size size = sprite->getContentSize() * scale;
			
			pos.y += size.height;
		}
	}
	
	//	for(int i=0; i<numSection; i++) {
	//
	//	}
	//
	//
	//
	//	for(int i=0; i<spriteArray.size(); i++) {
	//		Sprite *sprite = spriteArray[i];
	//
	//		sprite->setAnchorPoint(Vec2(0, 0));
	//		sprite->setPosition(pos);
	//
	//		addChild(sprite);
	//
	//		pos.y += spriteSize.height + spacing;
	//	}
}


void CsbParallaxSubLayer::addCsbToLayer(const std::string &csbName, int numSection)
{
	Vec2 pos(0, 0);
	
	int nTotal = numSection;
	
	// Adding the header
	for(int i=0; i<nTotal; i++) {
		Node *csbNode = CSLoader::createNode(csbName);
		csbNode->setAnchorPoint(Vec2(0, 0));
		csbNode->setPosition(pos);
		addChild(csbNode);
		
		Size size = csbNode->getContentSize();
		pos.y += size.height;
	}
	
}


void CsbParallaxSubLayer::fillSprite(std::vector<Sprite *> spriteArray,
										const Size &spriteSize, float spacing)
{
	Vec2 pos(0, 0);
	
	for(int i=0; i<spriteArray.size(); i++) {
		Sprite *sprite = spriteArray[i];
		
		sprite->setAnchorPoint(Vec2(0, 0));
		sprite->setPosition(pos);
		
		addChild(sprite);
		
		pos.y += spriteSize.height + spacing;
	}
}

void CsbParallaxSubLayer::setTintColor(const Color3B &color)
{
	Vector<Node *> spriteArray = getChildren();
	for(int i=0; i<spriteArray.size(); i++) {
		Node *node = spriteArray.at(i);
		Sprite *sprite = dynamic_cast<Sprite *>(node);
		if(sprite) {
			sprite->setColor(color);
		}
	}
}

void CsbParallaxSubLayer::setOpacity(GLubyte opacity)
{
	Vector<Node *> spriteArray = getChildren();
	for(int i=0; i<spriteArray.size(); i++) {
		Node *node = spriteArray.at(i);
		Sprite *sprite = dynamic_cast<Sprite *>(node);
		if(sprite) {
			sprite->setOpacity(opacity);
		}
	}
}

void CsbParallaxSubLayer::setScrollY(float scrollY)
{
	float offsetY;
	
	float scaleScroll = scrollY * mSpeedY;
	
//	if(scaleScroll <= mHeaderHeight) {	// the scroll still in the header area
//		offsetY = -scaleScroll;
//	} else {
	float modY = fmodf(scaleScroll, mSectionSize.height);
		
	offsetY = - modY;
//	}
	
	setPositionY(offsetY + mOriginOffset.y);
}