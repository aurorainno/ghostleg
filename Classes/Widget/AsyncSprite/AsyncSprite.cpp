//
//  AsyncSprite.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/1/2017.
//
//

#include "AsyncSprite.h"
#include "network/HttpClient.h"
#include "StringHelper.h"

AsyncSprite::AsyncSprite()
: Sprite()
, mHttp(nullptr)
{
	//log("Construct the sprite");
}

AsyncSprite::~AsyncSprite()
{
	//log("Destruct the Sprite");
	
	if(getHttp() != nullptr) {
		getHttp()->setResponseCallback(nullptr);
	}
	
	setHttp(nullptr);
}
bool AsyncSprite::init()
{
	if(Sprite::init() == false) {
		return false;
	}
	
	// Setup the Request
	network::HttpRequest *request = new network::HttpRequest();
	setHttp(request);
	request->release();
	
	
	
	return true;
}

void AsyncSprite::setPlaceHolderImage(const std::string &imageName)
{
	setTexture(imageName);
}


void AsyncSprite::updateSpriteTextureWithData(unsigned char *rawdata, size_t dataLen)
{
	Image img;
	bool isOkay = img.initWithImageData(rawdata, dataLen);
	
	if(isOkay == false) {
		return;
	}

	Texture2D *texture = new Texture2D();
	texture->initWithImage(&img);
	
	log("Size of the texture: w=%d h=%d ", texture->getPixelsWide(), texture->getPixelsHigh());
	
	updateSpriteTexture(texture);
	
	// release the texture
	texture->release();
	
	log("ContentSize after updated: %s", SIZE_TO_STR(getContentSize()).c_str());
}

void AsyncSprite::handleResponseData(std::vector<char> *buffer)
{
	//
	onDataLoadedFromHttp(buffer);
	
	//
	unsigned char * rawData = reinterpret_cast<unsigned char*>(&(buffer->front()));
	ssize_t dataLen = buffer->size();
	
	updateSpriteTextureWithData(rawData, dataLen);
	
	// create sprite with texture
	onWebTextureUpdate();
	
}


void AsyncSprite::updateSpriteTexture(Texture2D *texture)
{
	Vec2 originAnchor = getAnchorPoint();
	initWithTexture(texture);		// note: cannot use setTexture(texture);
	setAnchorPoint(originAnchor);
}

void AsyncSprite::loadFromURL(const std::string &imageURL)
{
	if(getHttp() == nullptr) {
		log("AsyncSprite: http not yet init");
		return;
	}
	
	network::HttpRequest *request = getHttp();
	
	request->setUrl(imageURL);
	request->setRequestType(network::HttpRequest::Type::GET);
	
	
	request->setResponseCallback([=](network::HttpClient* client, network::HttpResponse* response) {
		if (!response->isSucceed()) {
			log("AsyncSprite: fail to load image");
			return;
		}
		
		handleResponseData(response->getResponseData());
	});
	
	network::HttpClient::getInstance()->send(request);
	
	// TODO: Handle the
	
//	network::HttpRequest* request = new network::HttpRequest();
//	request->setUrl(url.data());
//	request->setRequestType(network::HttpRequest::Type::GET);
//	request->setResponseCallback([=](network::HttpClient* client, network::HttpResponse* response) {
//		if (!response->isSucceed()) {
//			CCLOG("ERROR, remote sprite load image failed");
//			return ;
//		}
//		
//		std::vector<char> *buffer = response->getResponseData();
//		Image img;
//		img.initWithImageData(reinterpret_cast<unsigned char*>(&(buffer->front())), buffer->size());
//		
//		if (1)
//		{
//			// save image file to device.
//			std::string path = FileUtils::getInstance()->getWritablePath()+"p.png";
//			log("save image path = %s", path.data());
//			bool ret = img.saveToFile(path);
//			log("save file %s", ret ? "success" : "failure");
//			
//			this->initWithFile(path);
//		} else {
//			
//			// create sprite with texture
//			Texture2D *texture = new Texture2D();
//			texture->autorelease();
//			texture->initWithImage(&img);
//			
//			this->initWithTexture(texture);
//			if (0 != _image_size.x) {
//				auto size = getContentSize();
//				setScaleX(_image_size.x/size.width);
//				setScaleY(_image_size.y/size.height);
//			}
//		}
//	});
//	network::HttpClient::getInstance()->send(request);
//	request->release();
}
