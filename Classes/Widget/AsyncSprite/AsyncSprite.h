//
//  AsyncSprite.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/1/2017.
//
//

#ifndef AsyncSprite_hpp
#define AsyncSprite_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "network/HttpRequest.h"

USING_NS_CC;

class AsyncSprite : public Sprite
{
public:
	CREATE_FUNC(AsyncSprite);
	AsyncSprite();
	~AsyncSprite();
	virtual bool init();

	void setPlaceHolderImage(const std::string &imageName);
	void loadFromURL(const std::string &imageURL);
	
protected:
	virtual void onDataLoadedFromHttp(std::vector<char> *textureData) {}
	virtual void onWebTextureUpdate() {}
	void updateSpriteTextureWithData(unsigned char *rawdata, size_t dataLen);
	void updateSpriteTexture(Texture2D *texture);
private:
	void handleResponseData(std::vector<char> *buffer);
	
	
	
private:
	CC_SYNTHESIZE_RETAIN(network::HttpRequest *, mHttp, Http);
//	network::HttpRequest* request;
};

#endif /* AsyncSprite_hpp */
