//
//  ParallaxTileLayer.h
//  TapToJump
//
//  Created by Ken Lee on 10/7/15.
//
//

#ifndef __TapToJump__ParallaxTileLayer__
#define __TapToJump__ParallaxTileLayer__

#include <stdio.h>
#include <stdlib.h>
#include "cocos2d.h"

#include "ParallaxSubLayer.h"

USING_NS_CC;

class SpriteParallaxSubLayer;

class ParallaxLayer : public LayerColor
{
public:
	static ParallaxLayer *create();
	static ParallaxLayer *create(const Color4B &color, const Size &size);
	
public:
	bool init(const Color4B &color, const Size &size);
	
	virtual ~ParallaxLayer();

	virtual Vec3 getPosition3D() const;
	virtual void setPosition3D(const Vec3 &position);
	
	//
	virtual void setPosition(const Vec2 &position);
	void setLayerPosition(const Vec2 &position);
	
	void addSubLayer(ParallaxSubLayer *layer);
	
	void setScrollY(float y);
	void setScrollX(float x);
	
	void setAutoScroll(float speed);
	
	virtual void onEnter();
	virtual void onExit();
	virtual void update(float delta);
	
    virtual void shake(float duration);
    

protected:
    SpriteParallaxSubLayer *mBackgroundLayer;
	
private:
	Vector<ParallaxSubLayer *> mSubLayerList;
	float mScrollY;
	float mScrollX;
	
	float mAutoScrollSpeed;
	bool mAutoScrollStarted;
};

#endif /* defined(__TapToJump__ParallaxTileLayer__) */
