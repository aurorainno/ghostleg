//
//  ADPageView.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/10/2016.
//
//

#include "AILPageView.h"


AILPageView::AILPageView()
: PageView()
, mScrollCallback(nullptr)
{
	
}

void AILPageView::addEventListener(const PageView::ccPageViewCallback& callback)
{
	_eventCallback = callback;
	ccScrollViewCallback scrollViewCallback = [=](Ref* ref, ScrollView::EventType type) -> void {
		if (type == ScrollView::EventType::AUTOSCROLL_ENDED) {	//&& _previousPageIndex != _currentPageIndex) {
			pageTurningEvent();
		}else if(ScrollView::EventType::SCROLLING == type) {
			onViewScrolled();
		}
	};
	ScrollView::addEventListener(scrollViewCallback);
}

void AILPageView::onViewScrolled()
{
	if(mScrollCallback) {
		Vec2 offset = this->getInnerContainerPosition();
		mScrollCallback(this, offset);
	}
}

void AILPageView::setScrollCallback(const AILPageView::ViewScrollCallback &callback)
{
	mScrollCallback = callback;
}


