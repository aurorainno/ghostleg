//
//  FBProfilePic.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/1/2017.
//
//

#ifndef FBProfilePic_hpp
#define FBProfilePic_hpp

#include <stdio.h>
#include "AsyncSprite.h"

#include "cocos2d.h"
#include "network/HttpRequest.h"

USING_NS_CC;

class FBProfilePic : public AsyncSprite
{
public:		// static
	static void clearProfilePicCache(const std::string &fbid);
	static std::string getProfilePicCacheFilename(const std::string &fbid);
public:
	CREATE_FUNC(FBProfilePic);
	FBProfilePic();
	~FBProfilePic();
	
	virtual bool init();
	
	void defineSize(const Size &size);		// define the size of the profilepic
	
	void setWithFBId(const std::string &fbid);
	
	std::string getFacebookProfileImageUrl(const std::string &fbid);
	
	
private:
	void setupPlaceholder();
	void setupCachePath();
	void fixContentSize();
	
	void saveTextureData(std::vector<char> *textureData);
	void saveTextureData(std::vector<char> *textureData, const std::string &filename);
	
	std::string getTextureFilename();
	
	bool hasCachedTexture();
	
	bool localTextureFromCache();
	
	virtual void onDataLoadedFromHttp(std::vector<char> *textureData);
	virtual void onWebTextureUpdate();
private:
	std::string mFacebookID;
	Size mDefinedSize;
};


#endif /* FBProfilePic_hpp */
