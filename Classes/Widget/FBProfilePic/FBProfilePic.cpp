//
//  FBProfilePic.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/1/2017.
//
//

#include "FBProfilePic.h"
#include "DeviceHelper.h"
#include "StringHelper.h"

#define kDefaultSize	100

void FBProfilePic::clearProfilePicCache(const std::string &fbid)
{
	std::string filename = getProfilePicCacheFilename(fbid);
	
	FileUtils::getInstance()->removeFile(filename);
}

std::string FBProfilePic::getProfilePicCacheFilename(const std::string &fbid)
{
#if IS_ANDROID
	std::string path = DeviceHelper::getStoragePath();	// /internalMemory/astrodog
	FileUtils::getInstance()->createDirectory(path);	// create the directory if not exists
	
	path = path + "/profilepic";
	FileUtils::getInstance()->createDirectory(path);
	
	
	return path + "/" + fbid;
#else
	
	std::string path = FileUtils::getInstance()->getWritablePath();
	path = path + "/profilepic";
	FileUtils::getInstance()->createDirectory(path);
	
	path = path + "/" + fbid;
	
	return path;
#endif
}


FBProfilePic::FBProfilePic()
: AsyncSprite()
, mDefinedSize(Size(0, 0))
{
	setContentSize(mDefinedSize);
}

FBProfilePic::~FBProfilePic()
{
	
}

bool FBProfilePic::init()
{
	if(AsyncSprite::init()) {
		setContentSize(mDefinedSize);
		
		return true;
	}
	
	return false;
}

void FBProfilePic::setWithFBId(const std::string &fbid)
{
	// check for cache first
	// load from internet
	mFacebookID = fbid;
	log("facebookID=%s", mFacebookID.c_str());
	
	//fixContentSize();		// make sure the contentsize not zero
	
	setupPlaceholder();		// Setting the default placeholder
	
	if(mFacebookID == "") {
		return;
	}
	
	if(hasCachedTexture()) {
		bool isOkay = localTextureFromCache();
		if(isOkay) {
			return;
		}
	}

	std::string fbUrl = getFacebookProfileImageUrl(mFacebookID);
	
	
	loadFromURL(fbUrl);
}

void FBProfilePic::defineSize(const Size &size)
{
	mDefinedSize = size;
	
	setContentSize(mDefinedSize);
}

void FBProfilePic::fixContentSize()
{
	if(mDefinedSize.width == 0) {
		return;	// undefined contentSize, no need to resize
	}
	
	
	Size latestSize = getContentSize();
	
	float scaleWidth = mDefinedSize.width / latestSize.width;
	float scaleHeight = mDefinedSize.height / latestSize.height;
	
	float scale = MIN(scaleWidth, scaleHeight);
	
	setScale(scale);
}

std::string FBProfilePic::getFacebookProfileImageUrl(const std::string &fbid)
{
	int width = kDefaultSize;	// (int) getContentSize().width;
	int height = kDefaultSize;	// (int) getContentSize().height;
	
	std::string fbLink = "https://graph.facebook.com/%s/picture?type=large&width=%d&height=%d";

	// Size turning
	return StringUtils::format(fbLink.c_str(), fbid.c_str(), width, height);
}

void FBProfilePic::setupPlaceholder()
{
	// Origin size of profilepic is 100x100
	
	setPlaceHolderImage("guiImage/default_profilepic.png");
	
	fixContentSize();
}

std::string FBProfilePic::getTextureFilename()
{
	return getProfilePicCacheFilename(mFacebookID);
}

bool FBProfilePic::hasCachedTexture()
{
	std::string filename = getTextureFilename();
	
	return FileUtils::getInstance()->isFileExist(filename);
}

//private:
//void setupCachePath();
void FBProfilePic::saveTextureData(std::vector<char> *textureData)
{
	std::string filename = getTextureFilename();
	log("dbug: FBProfilePic: %s", filename.c_str());
	
	saveTextureData(textureData, filename);
}

void FBProfilePic::saveTextureData(std::vector<char> *textureData, const std::string &filename)
{
	
//	texture->
	unsigned char * rawData = reinterpret_cast<unsigned char*>(&(textureData->front()));
	ssize_t dataLen = textureData->size();
	
	Data data;
	data.copy(rawData, dataLen);

	
	FileUtils::getInstance()->writeDataToFile(data, filename);
}


void FBProfilePic::onDataLoadedFromHttp(std::vector<char> *textureData)
{
	log("Load from HTTP");
	saveTextureData(textureData);
}

bool FBProfilePic::localTextureFromCache()
{
	std::string filename = getTextureFilename();
	Data data = FileUtils::getInstance()->getDataFromFile(filename);
	
	if(data.getSize() == 0) {
		return false;
	}
	
	unsigned char * rawData = data.getBytes();
	ssize_t dataLen = data.getSize();
	
	Size originSize = getContentSize();
	log("Load from Cache: originSize=%s", SIZE_TO_STR(getContentSize()).c_str());
	
	//updateSpriteTextureWithData(rawData, dataLen);
	
	fixContentSize();
	
	return true;
}


void FBProfilePic::onWebTextureUpdate()
{
	fixContentSize();
}
