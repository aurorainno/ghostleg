//
//  TimeHelper.cpp
//  GhostLeg
//
//  Created by Calvin on 22/11/2016.
//
//

#include "TimeHelper.h"
#include "cocos2d.h"
#include <time.h>
#include <math.h>

#define kMillisPerWeek 604800000
#define kMillisPerDay 86400000
#define kMillisPerHour 3600000
#define kMillisPerMinutes 60000

const long long secInMay2017SinceEPoch = 1493568000;

int TimeHelper::getCurrentTimeStampInDays()
{
    // Getting Timestamp of NOW
    time_t timer;
    struct tm defaultTime = {0};
    double seconds;
    int days;
    
    defaultTime.tm_hour = 0;   defaultTime.tm_min = 0; defaultTime.tm_sec = 0;
    defaultTime.tm_year = 116; defaultTime.tm_mon = 0; defaultTime.tm_mday = 1;
    
    time(&timer);  /* get current time; same as: timer = time(NULL)  */
    
    seconds = difftime(timer,mktime(&defaultTime));
    // seconds = difftime(mktime(&virtualTime),mktime(&y2k));
    days = floor(seconds/86400);
    
    return days;
}

long long TimeHelper::getCurrentTimeStampInMillis()
{
    std::chrono::milliseconds ms = std::chrono::duration_cast< std::chrono::milliseconds >(
         std::chrono::system_clock::now().time_since_epoch()
    );
    
    return ms.count();
}

int TimeHelper::getCurrentTimeStampInSecSinceMay2017()
{
    long long currentTimeInMillis = getCurrentTimeStampInMillis();
    long long currentTimeInSec = floor(currentTimeInMillis / 1000);
    int timeDiff = (int) currentTimeInSec - secInMay2017SinceEPoch;
    if(timeDiff < 0){
        timeDiff = 0;
    }
    return timeDiff;
}

map<string,int> TimeHelper::parseMillis(long long millis)
{
    long long remains = millis;
    map<string,int> result{};
    
    int days = floor(remains / kMillisPerDay);
    remains -= kMillisPerDay * days;
    int hours = floor(remains / kMillisPerHour);
    remains -= kMillisPerHour * hours;
    int minutes = floor(remains / kMillisPerMinutes);
    remains -= kMillisPerMinutes * minutes;
    int seconds = floor(remains/1000);
    
    result["days"] = days;
    result["hours"] = hours;
    result["minutes"] = minutes;
    result["seconds"] = seconds;
    
    return result;
}


//std::string TimeHelper::getRemainTimeString(long long millis)
//{
//	std::map<std::string, int> result = TimeHelper::parseMillis(millis);
//	int days = result["days"];
//	int hours = result["hours"];
//	int minutes = result["minutes"];
//	
//	if(days>0 || hours>0 || minutes>0 ){
//		std::string dateStr = "";
//		
//		dateStr = StringUtils::format("%dd %dh %dm", days, hours, minutes);
//		
//		return dateStr;
//	}
//	
//	return "Time Up";
//}

// OLD version
//
std::string TimeHelper::getRemainTimeString(long long millis)
{
	std::map<std::string, int> result = TimeHelper::parseMillis(millis);
	int days = result["days"];
	int hours = result["hours"];
	int minutes = result["minutes"];

    if(days>0 || hours>0 || minutes>0 ){
        std::string dateStr = "";
    
        std::string dayStr = days <= 1 ? "day" : "days";
    
        dateStr = StringUtils::format("%d %s %02d:%02d", days, dayStr.c_str(),hours,minutes);
    
        return dateStr;
    }
    
	return "Time Up";
}
