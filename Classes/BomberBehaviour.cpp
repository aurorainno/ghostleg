//
//  BomberBehaviour.cpp
//  GhostLeg
//
//  Created by Calvin on 8/3/2017.
//
//

#include "BomberBehaviour.h"
#include "Enemy.h"
#include "StringHelper.h"
#include "EnemyFactory.h"
#include "BulletBehaviour.h"
#include "Player.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "GameSound.h"

#define kBulletEnemyID 81

const int kUpperInactiveOffset = 300;

BomberBehaviour::BomberBehaviour()
: mCooldown(0)
, mCounter(0)
, mAppearTime(0)
, mDisplacementY(0)
, mStartLeaving(false)
{}


void BomberBehaviour::onCreate()
{
    updateProperty();
//    getEnemy()->setAnimationEndCallFunc(GameModel::Attack, CC_CALLBACK_0(FireBulletBehaviour::onAttack, this));
//    getEnemy()->setOnFrameEventCallback([=](const std::string &eventName, EventFrame* event){
//        if(eventName == "EmitBullet"){
//            fireBullet();
//        }
//    });
//
    getEnemy()->changeToIdle();
}

void BomberBehaviour::onVisible()
{

}

void BomberBehaviour::onActive()
{
 //   changeToAttack();
    getEnemy()->setAction(GameModel::Idle);
}

void BomberBehaviour::onUpdate(float delta)
{
    if(getEnemy()->getEffectStatus() == EffectStatusTimestop||
       getEnemy()->getState() !=  Enemy::State::Active ||
       getEnemy()->getVisibility() == false){
        return;
    }
    
    bool isFaceLeft = getEnemy()->isFaceLeft();
    float speedX = isFaceLeft ? -fabsf(getSpeedX()) : fabsf(getSpeedX());
    float newPosX = getEnemy()->getPositionX() + speedX * delta;
    if(newPosX < 100 || newPosX > 220){
        getEnemy()->setFace(!isFaceLeft);
        newPosX = newPosX < 100 ? 100 : 220;
    }
    
    float topY = Director::getInstance()->getOpenGLView()->getVisibleSize().height;
    float mapBottomY = GameWorld::instance()->getCameraY();
    float oldPosY = getEnemy()->getPositionY();
    float targetPosY = topY + mapBottomY - 75;
    float newPosY = oldPosY < targetPosY ? targetPosY : oldPosY;
    
    if(mStartLeaving){
        float speedY = fabsf(getSpeedY());
        mDisplacementY += speedY * delta;
        newPosY += mDisplacementY;
    }
    
    getEnemy()->setPosition(newPosX, newPosY);
    
    if(mStartLeaving){
        return;
    }
    
    mCounter += delta;
    mAppearTime += delta;
    if(mCounter >= mCooldown){
        mCounter = 0;
        fireBullet();
    }
    
    if(mAppearTime > 10){
        mStartLeaving = true;
    }
}

void BomberBehaviour::fireBullet()
{
    //TODO
    //    log("fire bullet!");

    Vec2 bulletPos = getEnemy()->getPosition();
    bulletPos.y -= 50;
    
    Enemy *enemy = Enemy::create();
    enemy->setObjectID(EnemyFactory::instance()->nextObjectID());
    
    // Attach a behaviour to the enemy
    EnemyBehaviour *enemyBehaviour = EnemyFactory::instance()->createBehaviourByMonsterID(kBulletEnemyID);
    BulletBehaviour *behaviour = dynamic_cast<BulletBehaviour* >(enemyBehaviour);
    if(behaviour) {
        behaviour->setEnemy(enemy);
        enemy->setBehaviour(behaviour);
        enemy->setResource(behaviour->getRes());
    } else {
        log("createEnemy: error: changing the id [%d] -> [%d]", kBulletEnemyID, 1);
        enemy->setResource(1);	// resID = monsterId
    }
    
    enemy->setupAttribute();
    
    if(EnemyFactory::instance()->getCurrentEffectStatus() != EffectStatusNone) {
        enemy->setEffectStatus(EnemyFactory::instance()->getCurrentEffectStatus());
    }
    
    enemy->setPosition(bulletPos);
    enemy->setupHitboxes();
    
    
    GameWorld::instance()->getModelLayer()->addEnemy(enemy);
    GameSound::playSound(GameSound::BomberDrop);
}

void BomberBehaviour::updateProperty()
{
    if(mData == nullptr) {
        log("BomberBehaviour::updateProperty :data is null");
        return;
    }
    
    std::map<std::string, std::string> property = mData->getMapProperty("bomberSetting");
    
    if(property.find("cooldown") == property.end()) {	// not found
        mCooldown = 2.0f;
    } else {
        mCooldown = aurora::StringHelper::parseFloat(property["cooldown"]);
    }
    
//    mIsLeft = getEnemy()->isFaceLeft();
}

std::string BomberBehaviour::toString()
{
    return "BomberBehaviour";
}
