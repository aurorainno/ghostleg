//
//  GalleryView.cpp
//  GhostLeg
//
//  Created by Calvin on 15/3/2017.
//
//

#include "GalleryView.h"

GalleryView::GalleryView():
 mState(State::Idle)
,mCounter(0)
,mRotationTime(0.3)
,mPeriod(4)
,mFirstNode(nullptr)
,mSecondNode(nullptr)
,mIndex(0)
{
}


void GalleryView::setRotationTime(float time)
{
    mRotationTime = time;
}

void GalleryView::setPeriod(float period)
{
    mPeriod = period;
}

void GalleryView::setTargetPosition(cocos2d::Vec2 pos)
{
    mTargetPos = pos;
}

void GalleryView::onEnter()
{
    Layer::onEnter();
    prepareView();
    mRotationSpeed = 320 / mRotationTime;
    if(mNodeList.size()>1){
        scheduleUpdate();
    }
}

void GalleryView::update(float delta)
{
    switch (mState) {
        case Idle:{
            mCounter += delta;
            if(mCounter>mPeriod){
                changeToRotate();
            }
            break;
        }
        
        case Rotate:{
            float displacement = mRotationSpeed * delta;
            mFirstNode->setPositionX(mFirstNode->getPositionX()-displacement);
            mSecondNode->setPositionX(mSecondNode->getPositionX()-displacement);
            
            if(mSecondNode->getPositionX()<=mTargetPos.x){
                mSecondNode->setPositionX(mTargetPos.x);
                changeToIdle();
            }
            break;
        }
            
        default:
            break;
    }
}

void GalleryView::prepareView()
{
    if(mNodeList.empty()){
        return;
    }
    
    if(mIndex >= mNodeList.size()){
        mIndex = 0;
    }
    
    if(mFirstNode==nullptr){
        Node* first = mNodeList.at(mIndex);
        mIndex++;
        mFirstNode = first;
        addChild(mFirstNode);
        mFirstNode->setPosition(mTargetPos);
        if(mIndex >= mNodeList.size()){
            return;
        }
    }
    
    Node* second = mNodeList.at(mIndex);
    mIndex++;
    mSecondNode = second;
    addChild(mSecondNode);
    mSecondNode->setPosition(mTargetPos.x+320, mTargetPos.y);
}

void GalleryView::changeToRotate()
{
    mCounter = 0;
    mState = Rotate;
}

void GalleryView::changeToIdle()
{
    mState = Idle;
    
    //swap first and second node
    Node* ref = nullptr;
    ref = mFirstNode;
    mFirstNode = mSecondNode;
    mSecondNode = ref;
    
    if(mSecondNode){
 //       mSecondNode->release();
        mSecondNode->removeFromParent();
        mSecondNode = nullptr;
    }
    
    prepareView();
}

