//
//  PetrolBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 21/2/2017.
//
//

#ifndef PetrolBehaviour_hpp
#define PetrolBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class PatrolBehaviour : public EnemyBehaviour
{
public:
    enum MovingMode{
        Vertical = 0,
        Horizontal
    };
    
    CREATE_FUNC(PatrolBehaviour);
    
    PatrolBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onCreate();
    virtual void onVisible();
    virtual void onActive();
    virtual void onUpdate(float delta);
    
    void updateEnemyFaceDir();
    void updateProperty();
    
private:
    float mDisplacement;
    float mMaxDisplacement;
    int mCurrentDir;
    MovingMode mMode;
    
};


#endif /* PetrolBehaviour_hpp */
