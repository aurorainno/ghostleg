//
//  ChangeMapSpeedDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 24/11/2016.
//
//

#include "ChangeMapSpeedDialog.h"
#include "LevelData.h"
#include "CommonMacro.h"

bool ChangeMapSpeedDialog::init()
{
    //////////////////////////////
    // 1. super init first
    if (!Layer::init())
    {
        return false;
    }
    
    
    // Setup UI
    setupUI();
    setupListener();
    updateText();
    //setupAnimation(csb);
    
    return true;
}

void ChangeMapSpeedDialog::setupUI()
{
    Node *rootNode = CSLoader::createNode("ChangeSpeedDialog.csb");
    FIX_UI_LAYOUT(rootNode);
    
    mAddBtn = rootNode->getChildByName<Button*>("add");
    mResetBtn = rootNode->getChildByName<Button*>("reset");
    mReduceBtn = rootNode->getChildByName<Button*>("reduce");
    mBackBtn = rootNode->getChildByName<Button*>("backBtn");
    mOffsetText = rootNode->getChildByName<Text*>("offsetText");
    
    addChild(rootNode);
}

void ChangeMapSpeedDialog::setupListener()
{
    mAddBtn->addClickEventListener([&](Ref*){
        int oriSpeed = MapLevelManager::instance()->getOffsetSpeed();
        MapLevelManager::instance()->setOffsetSpeed(oriSpeed+5);
        updateText();
    });
    
    mReduceBtn->addClickEventListener([&](Ref*){
        int oriSpeed = MapLevelManager::instance()->getOffsetSpeed();
        MapLevelManager::instance()->setOffsetSpeed(oriSpeed-5);
        updateText();
    });
    
    mResetBtn->addClickEventListener([&](Ref*){
        MapLevelManager::instance()->setOffsetSpeed(0);
        updateText();
    });
    
    mBackBtn->addClickEventListener([&](Ref*){
        this->removeFromParent();
    });
}

void ChangeMapSpeedDialog::updateText()
{
    if(mOffsetText){
        int offset = MapLevelManager::instance()->getOffsetSpeed();
        std::string offsetStr = StringUtils::format("current offset:%d",offset);
        mOffsetText->setString(offsetStr);
    }
}





