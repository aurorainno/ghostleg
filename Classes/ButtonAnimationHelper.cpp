//
//  ButtonAnimationHelper.cpp
//  GhostLeg
//
//  Created by Calvin on 18/1/2017.
//
//

#include "ButtonAnimationHelper.h"

void ButtonAnimationHelper::setButtonAnimation(cocos2d::ui::Button *btn, float duration, cocos2d::Vec2 oriPos, cocos2d::Vec2 offset)
{
    btn->addTouchEventListener([&,oriPos,duration,offset](Ref *sender, Widget::TouchEventType type) {
        switch(type){
            case ui::Widget::TouchEventType::BEGAN: {
                Button* thisBtn = dynamic_cast<Button*>(sender);
                MoveBy *moveBy = MoveBy::create(duration, offset);
                if(thisBtn){
                    thisBtn->setPosition(oriPos);
                    thisBtn->runAction(moveBy);
                }
                break;
            }
            case ui::Widget::TouchEventType::ENDED: {
                Button* thisBtn = dynamic_cast<Button*>(sender);
                if(thisBtn){
                    thisBtn->stopAllActions();
                    thisBtn->setPosition(oriPos);
                }
                break;
            }
            case ui::Widget::TouchEventType::CANCELED: {
                Button* thisBtn = dynamic_cast<Button*>(sender);
                if(thisBtn){
                    thisBtn->stopAllActions();
                    thisBtn->setPosition(oriPos);
                }
                break;
            }
        }
    });

    
}
