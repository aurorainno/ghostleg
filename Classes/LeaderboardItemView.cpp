//
//  LeaderboardItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 13/1/2017.
//
//

#include "LeaderboardItemView.h"
#include "GameRes.h"
#include "FBProfilePic.h"
#include "AstrodogClient.h"
#include "StringHelper.h"
#include "ViewHelper.h"

namespace {
	// Note: XXXX
	std::string getModifiedName(AstrodogRecord *playerData) {
		
		if(playerData == nullptr) {
			return "";
		}
		
		std::string name = playerData->getName();
		if("" == name) {
			name = "Player";
		}
		
		if("Player" != name) {
			return name;
		}
		
//		int uid = playerData->getUid();
//		int hashedID = uid % 123 * 1000 + uid % 13 * 100 +  uid % 17 * 500 + uid % 10;
//		
//		return StringUtils::format("Player%d", hashedID);
		return name;
		
		
		//		static int counter = 0;
//		
//		std::vector<std::string> nameList;
//		nameList.push_back("Mark Fish");
//		nameList.push_back("Lucie Keating");
//		nameList.push_back("Robert Hansen");
//		nameList.push_back("Jennifer Blatt");
//		nameList.push_back("Ana Palmer");
//		nameList.push_back("Ray Ellis");
//		nameList.push_back("Serena Paulus");
//		nameList.push_back("Mirta Keenan");
//		nameList.push_back("Laura Bulmers");
//		nameList.push_back("Annie");
//		nameList.push_back("Jenny");
//		nameList.push_back("Solomon");
//		nameList.push_back("FriendyGuy");
//		nameList.push_back("PinkLady");
//		
//		name = nameList[counter % nameList.size()];
//	
//		counter++;
//		
//		return name;
	}
}

LeaderboardItemView::LeaderboardItemView():
mRootNode(nullptr)
{}


bool LeaderboardItemView::init()
{
    if(Layout::init() == false) {
        return false;
    }
    //
    mRootNode = CSLoader::createNode("LeaderboardItemView.csb");
    
    
    setupUI();
    addChild(mRootNode);
    //
    //    setVisible(false);
    return true;
}

void LeaderboardItemView::setupUI()
{
    //    setSizeType(Widget::SizeType::ABSOLUTE);
    setContentSize(mRootNode->getContentSize());
    
    mCountrySprite = mRootNode->getChildByName<Sprite*>("countrySprite");
	if(mCountrySprite != nullptr) {  mCountrySprite->setVisible(false); } // ken: not showing now
	
	//
    mNameText = mRootNode->getChildByName<Text*>("nameText");
    mRankText = mRootNode->getChildByName<Text*>("rankText");
    mScoreText = mRootNode->getChildByName<Text*>("scoreText");
    mBgNormal = mRootNode->getChildByName<ImageView*>("bgNormal");
    mBgPlayer = mRootNode->getChildByName<ImageView*>("bgPlayer");
    mRankUpSprite = mRootNode->getChildByName<Sprite*>("rankUpSprite");
    mRankSprite = mRootNode->getChildByName<Sprite*>("rankSprite");
    mRankUpSprite->setVisible(false);
    
    // Profile Pic
    mProfilePicFrame = mRootNode->getChildByName<Sprite*>("profilePicFrame");
    
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    //    mProfilePic->setPlaceHolderImage("guiImage/default_profilepic.png");
    
    mProfilePicFrame->addChild(mProfilePic);
    //    int zOrder = profilePicFrame->getLocalZOrder();
    mProfilePic->setLocalZOrder(-1);
}

bool LeaderboardItemView::setItemView(AstrodogRecord *playerData)
{
	// bool isMe = user
	bool isThisPlayer = AstrodogClient::instance()->isMe(playerData->getPlayerID());
	// log("DEBUG: playerData=%s isMe=%d", playerData->toString().c_str(), isThisPlayer);
    std::string countryCode = playerData->getCountry();
    std::string fileName = GameRes::getCountryFlag(countryCode);
    
    
    std::string fbid = playerData->getFbID();
    int rank = playerData->getRank();
    int score = playerData->getScore();
    
	if(isThisPlayer) {
        mBgNormal->setVisible(false);
        mBgPlayer->setVisible(true);
        mNameText->setTextColor(Color4B(0x00,0x46,0x9a,0xff));
        mNameText->disableEffect(LabelEffect::SHADOW);
    }else{
        mBgNormal->setVisible(true);
        mBgPlayer->setVisible(false);
        mNameText->setTextColor(Color4B(0xff,0xff,0xff,0xff));
    }
    

    mProfilePic->setWithFBId(fbid);
	
	
	std::string name = getModifiedName(playerData);
	
//	//
//	if("Player" == name) {
//		int encodeID = playerData->getUid() * 1712313 % 9171;
//		name = StringUtils::format("P%d", encodeID);
//	}
	ViewHelper::setUnicodeText(mNameText, name);
	
//    if(aurora::StringHelper::containCJKChar(name)){
//        mNameText->setFontName("");
//    }else{
//        mNameText->setFontName("ObelixPro-cyr.ttf");
//    }
//    mNameText->setString(name);
	
	
    mScoreText->setString(StringUtils::format("%d",score));
    mRankText->setString(StringUtils::format("%d",rank));
    mCountrySprite->setTexture(fileName);
    
    if(rank == 1){
        mRankSprite->setTexture("guiImage/ui_result_leaderboard_badge1.png");
        mProfilePicFrame->setTexture("guiImage/ui_result_leaderboard_profilepicBorder1.png");
    }else if(rank == 2){
        mRankSprite->setTexture("guiImage/ui_result_leaderboard_badge2.png");
        mProfilePicFrame->setTexture("guiImage/ui_result_leaderboard_profilepicBorder2.png");
    }else if(rank == 3){
        mRankSprite->setTexture("guiImage/ui_result_leaderboard_badge3.png");
        mProfilePicFrame->setTexture("guiImage/ui_result_leaderboard_profilepicBorder3.png");
    }
    
    return isThisPlayer;
}

void LeaderboardItemView::resetContentSize(const cocos2d::Size &contentSize)
{
    mRootNode->setContentSize(contentSize);
    cocos2d::ui::Helper::doLayout(mRootNode);
    setContentSize(mRootNode->getContentSize());
}

void LeaderboardItemView::setRankStatus(AstrodogRankStatus status)
{
    if(mRankUpSprite == nullptr) {
        return;
    }
    
    if(AstrodogRankNone == status) {
        mRankUpSprite->setVisible(false);
        return;
    }
    
    mRankUpSprite->setVisible(true);
    
    if(AstrodogRankUp == status) {
        mRankUpSprite->setTexture("guiImage/ui_result_up.png");
        
    } else {	// Rank Down
        mRankUpSprite->setTexture("guiImage/ui_result_down.png");
    }
}

