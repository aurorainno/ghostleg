//
//  NPCRatingItemView.hpp
//  GhostLeg
//
//  Created by Calvin on 15/3/2017.
//
//

#ifndef NPCRatingItemView_hpp
#define NPCRatingItemView_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NPCRating;

class NPCRatingItemView : public Layout
{
public:
    CREATE_FUNC(NPCRatingItemView);
    virtual bool init();
    
    void setupUI(Node* rootNode);
    
    void setItemView(NPCRating* data);

    
private:
    void setRatingGUI(int rating);
    
private:
    Node *mRootNode;
    Sprite* mNpcSprite;
    Sprite* mEmojiSprite;
    Text* mCommentText;
    Text* mTimeText;
    Text* mTimeUnit;

};


#endif /* NPCRatingItemView_hpp */
