#include "AppDelegate.h"
#include "HelloWorldScene.h"
#ifdef SDKBOX_ENABLED
#include "PluginFacebook/PluginFacebook.h"
#include "PluginIAP/PluginIAP.h"
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#include "PluginSdkboxPlay/PluginSdkboxPlay.h"
#include "PluginReview/PluginReview.h"
#endif
#include "MainScene.h"
#include "GameManager.h"
#include "GameSound.h"
#include "AdManager.h"
#include "RateAppHelper.h"
#include "Analytics.h"
#include "GameCenterManager.h"
#include "TitleScene.h"
#include "AudioEngine.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
const int kFPS = 30;
#else
const int kFPS = 50;
#endif


USING_NS_CC;

static cocos2d::Size designResolutionSize = cocos2d::Size(320, 568);
//static cocos2d::Size designResolutionSize = cocos2d::Size(320, 420);
static cocos2d::Size smallResolutionSize = cocos2d::Size(320, 480);
static cocos2d::Size mediumResolutionSize = cocos2d::Size(768, 1024);
static cocos2d::Size largeResolutionSize = cocos2d::Size(1536, 2048);

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate()
{
}

//if you want a different context,just modify the value of glContextAttrs
//it will takes effect on all platforms
void AppDelegate::initGLContextAttrs()
{
    //set OpenGL context attributions,now can only set six attributions:
    //red,green,blue,alpha,depth,stencil
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8};

    GLView::setGLContextAttrs(glContextAttrs);
}

// If you want to use packages manager to install more packages,
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}


void setupResolutionPolicy(float designW, float designH)
{
	GLView *view = Director::getInstance()->getOpenGLView();
	Size screenSize = view->getFrameSize();

	float designRatio = designW / designH;
	float screenRatio = screenSize.height / screenSize.width;

//	ResolutionPolicy resolutionPolicy = screenRatio < designRatio ?
//	ResolutionPolicy::FIXED_HEIGHT : ResolutionPolicy::FIXED_WIDTH;

	//ResolutionPolicy resolutionPolicy = ResolutionPolicy::FIXED_WIDTH;
	ResolutionPolicy resolutionPolicy = ResolutionPolicy::SHOW_ALL;

	view->setDesignResolutionSize(designW, designH, resolutionPolicy);
}

bool AppDelegate::applicationDidFinishLaunching() {
#ifdef SDKBOX_ENABLED
    sdkbox::PluginFacebook::init();
#endif

#ifdef SDKBOX_ENABLED
	sdkbox::IAP::init();
	AdManager::instance()->init();
#endif

    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();
    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("GhostLeg", Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("GhostLeg");
#endif
        director->setOpenGLView(glview);
    }

    // turn on display FPS
	bool showStat = GameManager::instance()->isDebugMode();
    director->setDisplayStats(showStat);

    // set FPS. the default value is 1.0/60 if you don't call this
    //director->setAnimationInterval(1.0 / 60);
	director->setAnimationInterval(1.0 / kFPS);

	setupResolutionPolicy(designResolutionSize.width, designResolutionSize.height);

	// ken: the original resolution policy handling

//    // Set the design resolution
//    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::NO_BORDER);
//    Size frameSize = glview->getFrameSize();
//    // if the frame's height is larger than the height of medium size.
//    if (frameSize.height > mediumResolutionSize.height)
//    {
//        director->setContentScaleFactor(MIN(largeResolutionSize.height/designResolutionSize.height, largeResolutionSize.width/designResolutionSize.width));
//    }
//    // if the frame's height is larger than the height of small size.
//    else if (frameSize.height > smallResolutionSize.height)
//    {
//        director->setContentScaleFactor(MIN(mediumResolutionSize.height/designResolutionSize.height, mediumResolutionSize.width/designResolutionSize.width));
//    }
//    // if the frame's height is smaller than the height of medium size.
//    else
//    {
//        director->setContentScaleFactor(MIN(smallResolutionSize.height/designResolutionSize.height, smallResolutionSize.width/designResolutionSize.width));
//    }

    register_all_packages();

	// Setup Search path
	setupSearchPath();

	GameManager::instance()->setup();

	// logging
	Analytics::instance()->logScreen(Analytics::Screen::app_launch);


	// Debugging Setting (Remove before commit)
	//Analytics::instance()->setDebug(true);
	//GameManager::instance()->setDebugMap(2001);
	//GameManager::instance()->setDebugMap(10001);
	//GameManager::instance()->setDebugMap(10000);
	//GameManager::instance()->setDebugProperty("noBackground", 1);	// uncomment to make render faster
	//GameManager::instance()->resetTutorial();		// uncomment when want to test tutorial
	//GameManager::instance()->setDebugProperty("noCollision", 1);


    // create a scene. it's an autorelease object
    //auto scene = HelloWorld::createScene();
	// auto scene = MainSceneLayer::createScene();
	auto scene = TitleSceneLayer::createScene();

    // run
    director->runWithScene(scene);

	std::string version = this->getInstance()->getVersion();
	//Application::get
	// log("DEBUG: version=%s", version.c_str());

	GameSound::setup();

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    // SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	experimental::AudioEngine::pauseAll();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    // SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
	experimental::AudioEngine::resumeAll();
}


void AppDelegate::setupSearchPath()
{
	FileUtils::getInstance()->addSearchPath("fonts");
	FileUtils::getInstance()->addSearchPath("res");
	FileUtils::getInstance()->addSearchPath("modelRes");
	FileUtils::getInstance()->addSearchPath("level");
	FileUtils::getInstance()->addSearchPath("sound");
	FileUtils::getInstance()->addSearchPath("spritesheet");
	FileUtils::getInstance()->addSearchPath("background");
	FileUtils::getInstance()->addSearchPath("animation");
}
