//
//  LeaderboardDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 16/1/2017.
//
//

#include "LeaderboardDialog.h"
#include "LeaderboardItemView.h"
#include "AstrodogClient.h"
#include "GameManager.h"
#include "UserGameData.h"

const float kListItemSpacing = 5.0f;

LeaderboardDialog::LeaderboardDialog()
: mMyType(Endgame)
, mRootNode(nullptr)
, mMyRecordIndex(-1)
{}

bool LeaderboardDialog::init()
{
    if(Layout::init() == false) {
        return false;
    }
    //
    
    //    mRootNode = CSLoader::createNode(csbName);
    //    setupUI();
    //    addChild(mRootNode);
    //
    //    setVisible(false);
    return true;
}

void LeaderboardDialog::setupUI()
{
    setContentSize(mRootNode->getContentSize());
    
    Node* mainPanel = mRootNode->getChildByName("leaderboardPanel");
    mListView = mainPanel->getChildByName<ListView*>("leaderboardListView");
    
    mFriendBtn = (Button*) mainPanel->getChildByName("friendBtn");
    mNationalBtn = (Button*) mainPanel->getChildByName("nationalBtn");
    mGlobalBtn = (Button*) mainPanel->getChildByName("globalBtn");
    
    mFBConnectBtn = (Button*) mainPanel->getChildByName("fbBtn");
    
    
    mFriendBorder = mainPanel->getChildByName("friendBorderPanel");
    mNationalBorder = mainPanel->getChildByName("nationalBorderPanel");
    mGlobalBorder = mainPanel->getChildByName("globalBorderPanel");
    
    
    mLoadingText = (Text*) mainPanel->getChildByName("loadingText");
    mEmptyFriendAlert = (Text*) mainPanel->getChildByName("emptyFriendRecordAlert");
    
    mFriendBtn->addClickEventListener([&](Ref *){
        //       setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardFriend);
        if(mFriendCallback){
            mFriendCallback();
        }
    });
    mNationalBtn->addClickEventListener([&](Ref *){
        //        setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
        if(mNationalCallback){
            mNationalCallback();
        }
    });
    mGlobalBtn->addClickEventListener([&](Ref *){
        //        setLeaderboardGUI(AstrodogLeaderboardType::AstrodogLeaderboardGlobal);
        if(mGlobalCallback){
            mGlobalCallback();
        }
    });
    
    mFBConnectBtn->addClickEventListener([&](Ref *){
        if(mFbBtnCallback){
            mFbBtnCallback();
        }
    });
    
    
}

std::string LeaderboardDialog::getCsbName(LeaderboardUIType type){
    if(type == LeaderboardUIType::Endgame){
        return "LeaderboardDialog_endgame.csb";
    }else if(type == LeaderboardUIType::Mainscene){
        return "LeaderboardDialog_mainscene.csb";
    }
    return "";
}

void LeaderboardDialog::setLeaderboardUIType(LeaderboardUIType type)
{
    mMyType = type;
    if(mRootNode){
        mRootNode->removeFromParent();
    }
    
    std::string csbName = getCsbName(mMyType);
    mRootNode = CSLoader::createNode(csbName);
    setupUI();
    addChild(mRootNode);
}

void LeaderboardDialog::showLoading()
{
    if(mListView){
        mListView->removeAllItems();
    }
    mEmptyFriendAlert->setVisible(false);
    mFBConnectBtn->setVisible(false);
    mLoadingText->setVisible(true);
}

ListView* LeaderboardDialog::getListView()
{
    return mListView;
}

void LeaderboardDialog::setLeaderboardGUI(AstrodogLeaderboardType type)
{
    mAstrodogLBType = type;
    
    bool isFriendLBSelected = type == AstrodogLeaderboardType::AstrodogLeaderboardFriend;
    bool isNationLBSelected = type == AstrodogLeaderboardType::AstrodogLeaderboardCountry;
    bool isGlobalLBSelected = type == AstrodogLeaderboardType::AstrodogLeaderboardGlobal;
    
    mFriendBorder->setVisible(isFriendLBSelected);
    
    if(isFriendLBSelected){
        mFriendBtn->loadTextures("guiImage/ui_main_rankingtab_2_on.png", "guiImage/ui_main_rankingtab_2_on.png");
    }else{
        mFriendBtn->loadTextures("guiImage/ui_main_rankingtab_2_off.png", "guiImage/ui_main_rankingtab_2_off.png");
    }
    
    mGlobalBorder->setVisible(isGlobalLBSelected);
    
    if(isGlobalLBSelected){
        mGlobalBtn->loadTextures("guiImage/ui_main_rankingtab_3_on.png", "guiImage/ui_main_rankingtab_3_on.png");
    }else{
        mGlobalBtn->loadTextures("guiImage/ui_main_rankingtab_3_off.png", "guiImage/ui_main_rankingtab_3_off.png");
    }
    
    mNationalBorder->setVisible(isNationLBSelected);
    
    if(isNationLBSelected){
        mNationalBtn->loadTextures("guiImage/ui_main_rankingtab_1_on.png", "guiImage/ui_main_rankingtab_1_on.png");
    }else{
        mNationalBtn->loadTextures("guiImage/ui_main_rankingtab_1_off.png", "guiImage/ui_main_rankingtab_1_off.png");
    }
}

void LeaderboardDialog::updateWithLeaderboard(AstrodogLeaderboard *board, int currentPlanet)
{
    if(board == nullptr) {
        return;
    }
    
    mCurrentPlanet = currentPlanet;
    mEmptyFriendAlert->setVisible(false);
    updateLBListView(board->recordList);
   
}


void LeaderboardDialog::updateRankStatus(AstrodogRankStatus status)
{
    if(mMyRecordIndex < 0) {
        return;	// My record not yet show up
    }
    
    LeaderboardItemView *itemView = (LeaderboardItemView *) mListView->getItem(mMyRecordIndex);
    if(itemView != nullptr) {
        itemView->setRankStatus(status);
    }
}

void LeaderboardDialog::setupRankStatus(int newRank, LeaderboardItemView *itemView)
{
    int oldRank;
    if(mAstrodogLBType == AstrodogLeaderboardCountry){
        oldRank = GameManager::instance()->getUserData()->getCurrentLocalRank(mCurrentPlanet);
    }else if(mAstrodogLBType == AstrodogLeaderboardFriend){
        oldRank = GameManager::instance()->getUserData()->getCurrentFriendRank(mCurrentPlanet);
    }
    
    if(oldRank == -1){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankUp);
    }else if(newRank < oldRank){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankUp);
    }else if(newRank > oldRank){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankDown);
    }else if(newRank == oldRank){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankNone);
    }
    
    if(mAstrodogLBType == AstrodogLeaderboardCountry){
        GameManager::instance()->getUserData()->setCurrentLocalRank(mCurrentPlanet, newRank);
    }else if(mAstrodogLBType == AstrodogLeaderboardFriend){
        GameManager::instance()->getUserData()->setCurrentFriendRank(mCurrentPlanet, newRank);
    }
    
}

void LeaderboardDialog::updateLBListView(Vector<AstrodogRecord *> recordList)
{
    if(mListView == nullptr){
        return;
    }
    
    mLoadingText->setVisible(false);
    mListView->removeAllItems();
    mListView->setItemsMargin(kListItemSpacing);//spacing between item
    
    mMyRecordIndex = -1;
    if(recordList.empty() && mAstrodogLBType == AstrodogLeaderboardFriend){
        mEmptyFriendAlert->setVisible(true);
        if(!AstrodogClient::instance()->isFBConnected()){
            mFBConnectBtn->setVisible(true);
        }
    }else{
        for(int i=0;i<recordList.size();i++){	// ken: for(AstrodogRecord *record : recordList)  // more egligant
            LeaderboardItemView *itemView = LeaderboardItemView::create();
            bool isThisPlayer = itemView->setItemView(recordList.at(i));	// ken: this is not good, violate, SRP
            if(isThisPlayer){
                setupRankStatus(recordList.at(i)->getRank(),itemView);
                mMyRecordIndex = i;
            }
            mListView->pushBackCustomItem(itemView);
        }
    }
    
    if(mMyRecordIndex >= 0) {
        mListView->jumpToItem(mMyRecordIndex, Vec2(1,0.65), Vec2(0,1));
    }
}

void LeaderboardDialog::setGlobalBtnCallback(std::function<void ()> callback)
{
    mGlobalCallback = callback;
}

void LeaderboardDialog::setFriendBtnCallback(std::function<void ()> callback)
{
    mFriendCallback = callback;
}

void LeaderboardDialog::setNationalBtnCallback(std::function<void ()> callback)
{
    mNationalCallback = callback;
}

void LeaderboardDialog::setFbBtnCallback(std::function<void ()> callback)
{
    mFbBtnCallback = callback;
}
