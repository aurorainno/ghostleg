//
//  FireBulletBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 6/3/2017.
//
//

#ifndef FireBulletBehaviour_hpp
#define FireBulletBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

class FireBulletBehaviour : public EnemyBehaviour
{
public:
    enum State{
        Unknown,
        Idle,
        Attack,
    };
    
    CREATE_FUNC(FireBulletBehaviour);
    
    FireBulletBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onCreate();
    virtual void onVisible();
    virtual void onActive();
    virtual void onUpdate(float delta);
    
    void changeToIdle();
    void changeToAttack();
    void onAttack();
    void fireBullet();
    
    void updateProperty();
    
private:
    float mCooldown;
    float mOffsetX;
    float mOffsetY;
    
    float mAccumTime;
    State mState;
    
};

#endif /* FireBulletBehaviour_hpp */
