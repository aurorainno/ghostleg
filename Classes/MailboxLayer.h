//
//  MailboxLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 17/2/2017.
//
//

#ifndef MailboxLayer_hpp
#define MailboxLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "AstrodogClient.h"
#include "CharacterItemView.h"
#include "MailItemView.h"

USING_NS_CC;
using namespace cocos2d::ui;

class FBProfilePic;

class MailboxLayer : public Layer, public AstrodogClientListener
{
public:
    CREATE_FUNC(MailboxLayer);
    virtual bool init();
    
    void setupUI(Node* rootNode);
    void onEnter();
    
    void loadMailListView(Vector<AstrodogMail*> mailList);
    void updateMailItemView(int mailID);
    
    void setCloseCallback(std::function<void()> callback);
    
    void updateStars();
    void updateDiamond();
    void setupFbProfile();
    
    void playAnimation(AstrodogAttachment attachment);

    
#pragma mark - AstrodogClient
public:
    virtual void onMailReceived(int status);
    virtual void onMailboxLoaded(int status);
    virtual void onFBConnect(int status);
    
private:
    ListView* mListView;
    Button* mCancelBtn;
    Node* mInProgressLayer;
    Text* mEmptyHint;
    
    Text* mDiamondText;
    Text* mStarText;
    
    Button *mFbConnectBtn;
    Text *mFbName;
    Sprite *mProfilePicFrame;
    FBProfilePic *mProfilePic;
    
    int mSelectedMailID;
    
    std::function<void()> mCloseCallback;
    
    Map<int,MailItemView*> mMailItemViewMap;

};


#endif /* MailboxLayer_hpp */
