//
//  LeaderboardRewardManager.cpp
//  GhostLeg
//
//  Created by Calvin on 27/2/2017.
//
//

#include "LeaderboardRewardManager.h"
#include "LeaderboardReward.h"


static LeaderboardRewardManager *sInstance = nullptr;

LeaderboardRewardManager *LeaderboardRewardManager::instance()
{
    if(sInstance != nullptr) {
        return sInstance;
    }
    
    sInstance = new LeaderboardRewardManager();
    
    return sInstance;
}

LeaderboardRewardManager::LeaderboardRewardManager()
{
    mRewardMap.clear();
}

void LeaderboardRewardManager::setup()
{
    loadRewardData();
}

void LeaderboardRewardManager::loadRewardData()
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/leaderboardReward.json");
    
    // log("leaderboard json content:\n%s", content.c_str());
    rapidjson::Document jsonArray;
    jsonArray.Parse(content.c_str());
    
    if (jsonArray.HasParseError())
    {
        CCLOG("LeaderboardRewardManager: GetParseError %d\n", jsonArray.GetParseError());
        return;
    }
    
    mRewardMap.clear();
    mRewardMapWithPlanetID.clear();
    
    for(rapidjson::Value::ConstMemberIterator iter = jsonArray.MemberBegin();
        iter != jsonArray.MemberEnd(); ++iter)
    {
        std::string name = iter->name.GetString();
        const rapidjson::Value &dataArray = iter->value;
        
        LeaderboardRewardType type;
        
        
        if(name == "Local"){
            type = LeaderboardRewardType::Local;
        }else{
            type = LeaderboardRewardType::Global;
        }
        
        for(rapidjson::SizeType i=0;i<dataArray.Size();i++)
        {
            std::vector<LeaderboardReward*> rewardList;
            
            rewardList.clear();
        
            int planetID = dataArray[i]["planetID"].GetInt();
            const rapidjson::Value &rewardArray = dataArray[i]["rewardList"];
        
            for(rapidjson::SizeType i=0;i<rewardArray.Size();i++){
                const rapidjson::Value &currentReward = rewardArray[i];
                LeaderboardReward *data = LeaderboardReward::create();
                data->parseJSON(currentReward);
                data->retain();
                rewardList.push_back(data);
            }
            
            mRewardMapWithPlanetID[planetID] = rewardList;
        }
        
        mRewardMap[type] = mRewardMapWithPlanetID;
    }
}

map<LeaderboardRewardManager::LeaderboardRewardType,map<int,vector<LeaderboardReward*>>> LeaderboardRewardManager::getRewardMap()
{
    return mRewardMap;
}



