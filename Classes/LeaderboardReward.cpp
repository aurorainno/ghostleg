//
//  LeaderboardReward.cpp
//  GhostLeg
//
//  Created by Calvin on 24/2/2017.
//
//

#include "LeaderboardReward.h"
#include "StringHelper.h"
#include "JSONHelper.h"

LeaderboardReward::LeaderboardReward()
: mAmount(0)
{
    
}
 void LeaderboardReward::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
    //TODO
}

bool LeaderboardReward::parseJSON(const rapidjson::Value &json)		// JSON DAta -> this object value
{
    std::string type = aurora::JSONHelper::getString(json, "type", "star");
    if(type == "star"){
        mRewardType = MoneyTypeStar;
    }else{
        mRewardType = MoneyTypeDiamond;
    }
    
    int amount = aurora::JSONHelper::getInt(json, "amount", 0);
    mAmount = amount;
    
    return true;
}

std::string LeaderboardReward::toString()
{
    std::string result = "";
    
//    result += "type=" + rewardTypeToString(getRewardType());
//    result += " itemName=" + getItemName();
//    result += StringUtils::format(" quantity=%d", getQuantity());
//    result += StringUtils::format(" probability=%d", getProbability());
//    result += StringUtils::format(" itemID=%d", getItemID());
//    result += " rarity=" + rarityTypeToString(getRarityType());
//    result += StringUtils::format(" range=[%d %d]", getRangeStart(), getRangeEnd());
    
    return result;
}
