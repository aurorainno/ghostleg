//
//  UserGameData.cpp
//
//	An object handling users persistence data
//
//  GhostLeg
//
//  Created by Ken Lee on 24/4/2016.
//
//

#include "UserGameData.h"
#include "StringHelper.h"
//#include "GameManager.h"
//#include "SuitManager.h"

//
//  GameData.cpp
//  TapToJump
//
//  Created by Ken Lee on 23/4/15.
//
//

#include "UserGameData.h"
#include "cocos2d.h"

USING_NS_CC;

#define kKeyBestScore           "ghostleg.bestScore"
#define kKeyBestDistance        "ghostleg.bestDistance"
#define kKeyLastDistance        "ghostleg.lastDistance"
#define kKeyTotalCoin           "ghostleg.totalCoin"
#define kKeyPlayCount           "ghostleg.playcount"
#define kKeyTutorialPlayed      "ghostleg.tutorialPlayed"
#define kKeyBoughtNoAd          "ghostleg.boughtNoAd"
#define kKeyBGMisOn             "ghostleg.BGMisOn"
#define kKeySEisOn              "ghostleg.SEisOn"
#define kKeyGmMode              "ghostleg.gmMode"
#define kKeyHasLikedFacebook    "ghostleg.asLikedFacebook"
#define kKeySelectedDog			"ghostleg.selectedDog"
#define kKeyTotalCandy          "ghostleg.totalCandy"
#define kKeyTotalDiamond        "ghostleg.totalDiamond"

#define kKeyCoachmarkVisitDog    "ghostleg.coachmark.visitDog"
#define kKeyCoachmarkVisitPlanet "ghostleg.coachmark.visitPlanet"
#define kKeyCoachmarkSelectChar    "ghostleg.coachmark.selectChar"

#define kKeyGachaPlayAdTime       "ghostleg.gachaPlayAd"

//static UserGameData *sInstance = NULL;

const int kDefaultStar = 1000;
const int kDefaultDiamond = 8;
const std::string kKeyCurrentRankLocal =  "ghostleg.currentRankLocal/planet%d";
const std::string kKeyCurrentRankFriend = "ghostleg.currentRankFriend/planet%d";

namespace  {
	
	// note: cannot use get bool using getBoolForKey in Android
	bool getUserBool(UserDefault *userData, const char *key, bool defaultValue)
	{
		if(userData == nullptr) {
			log("UserGameData: setUserBool. userData is null");
			return defaultValue;
		}
		
		int defaultInt = defaultValue ? 1 : 0;
		bool isOkay = userData->getIntegerForKey(key, defaultInt) == 1;
		
		return isOkay;
	}
	
	void setUserBool(UserDefault *userData, const char *key, bool value)
	{
		if(userData == nullptr) {
			log("UserGameData: setUserBool. userData is null");
			return;
		}
		userData->setIntegerForKey(key, value ? 1 : 0);
		userData->flush();
	}
}

UserGameData::UserGameData()
: mLastDistance(0)
, mBestDistance(0)
, mTotalCoin(0)
, mPlayCount(0)
, mEnergyMax(100)
, mBoughtNoAd(false)
, mDefaultStar(kDefaultStar)
, mHasLikedFacebook(false)
, mSelectedDog(1)
, mLastGachaPlayAdTime(0)
{
    
#if(PAID_APP == 1)
    mDefaultStar = 500;
    mBoughtNoAd = true;
#else
    mDefaultStar = kDefaultStar;
#endif
	
}


bool UserGameData::isPaidApp()
{
#if(PAID_APP == 1)
    return true;
#endif
    return false;
}

int UserGameData::getLastDistance()
{
	return mLastDistance;
}

bool UserGameData::isNewRecord()
{
	return mIsNewRecord;
}

int UserGameData::getBestDistance()
{
	return mBestDistance;
}

int UserGameData::getPlayCount()
{
	return mPlayCount;
}

int UserGameData::getScore()
{
    return mBestScore;
}

void UserGameData::addCoin(int coin, bool autoSave)
{
	mTotalCoin += coin;
	
	if(mTotalCoin <= 0) {
		mTotalCoin = 0;
	}
	
	if(autoSave) {
		saveMoney();
	}
}

int UserGameData::getTotalCoin()
{
	return mTotalCoin;
}

void UserGameData::addCandy(int num)
{
    mTotalCandy += num;
    
    if(mTotalCandy <= 0) {
        mTotalCandy = 0;
    }
    
    saveCandy();
}

int UserGameData::getTotalCandy()
{
    return mTotalCandy;
}

void UserGameData::addDiamond(int num)
{
    mTotalDiamond += num;
    
    if(mTotalDiamond <= 0) {
        mTotalDiamond = 0;
    }
    
    saveDiamond();
}

int UserGameData::getTotalDiamond()
{
    return mTotalDiamond;
}

void UserGameData::updateScore(int newScore)
{
    if(newScore > mBestScore){
        mBestScore = newScore;
        saveScore();
    }
}

void UserGameData::setScore(int score)
{
    mBestScore = score;
    if(mBestScore <= 0) {
        mBestScore = 0;
    }
    
    saveScore();
}

void UserGameData::setDistance(int newScore)
{
	mLastDistance = newScore;
	mIsNewRecord = false;
	if(mLastDistance > mBestDistance) {
		mBestDistance = mLastDistance;
		mIsNewRecord = true;
	}
	
	mPlayCount++;
	
	
	saveData();
	
	
	//log("setScore: last=%d best=%d", mLastScore, mBestScore);
}

void UserGameData::clearRecord()
{
	mBestDistance = 0;
	mLastDistance = 0;
	mPlayCount = 0;
	
	saveData();
}

void UserGameData::saveMoney()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("UserGameData:saveData: userDefault not available");
		return;
	}
	
	//log("UserGameData:start save data");
	userData->setIntegerForKey(kKeyTotalCoin, mTotalCoin);
}

void UserGameData::saveCandy()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("UserGameData:saveData: userDefault not available");
        return;
    }
    
    //log("UserGameData:start save data");
    userData->setIntegerForKey(kKeyTotalCandy, mTotalCandy);
}

void UserGameData::saveScore()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("UserGameData:saveData: userDefault not available");
        return;
    }
    
    //log("UserGameData:start save data");
    userData->setIntegerForKey(kKeyBestScore, mBestScore);

}

void UserGameData::saveDiamond()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("UserGameData:saveData: userDefault not available");
        return;
    }
    
    //log("UserGameData:start save data");
    userData->setIntegerForKey(kKeyTotalDiamond, mTotalDiamond);
}


void UserGameData::saveData()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("UserGameData:saveData: userDefault not available");
		return;
	}
	
	//log("UserGameData:start save data");
	userData->setIntegerForKey(kKeyBestDistance, mBestDistance);
	userData->setIntegerForKey(kKeyLastDistance, mLastDistance);
	userData->setIntegerForKey(kKeyPlayCount, mPlayCount);
	userData->setIntegerForKey(kKeyTotalCoin, mTotalCoin);
	userData->setIntegerForKey(kKeySelectedDog, mSelectedDog);
    userData->setIntegerForKey(kKeyTotalCandy, mTotalCandy);
    userData->setIntegerForKey(kKeyTotalDiamond, mTotalDiamond);
    userData->setIntegerForKey(kKeyBestScore, mBestScore);
    userData->setIntegerForKey(kKeyGachaPlayAdTime, mLastGachaPlayAdTime);
    
    for(int i=0;i<5;i++){
        int currentRankLocal = mLocalCurrentRankMap.at(i);
        std::string keyLocal = StringUtils::format(kKeyCurrentRankLocal.c_str(),i);
        int currentRankFriend = mLocalCurrentRankMap.at(i);
        std::string keyFriend = StringUtils::format(kKeyCurrentRankFriend.c_str(),i);
        
        userData->setIntegerForKey(keyLocal.c_str(), currentRankLocal);
        userData->setIntegerForKey(keyFriend.c_str(), currentRankFriend);
    }
    
	userData->flush();

	setUserBool(userData, kKeyTutorialPlayed, mTutorialPlayed);
	setUserBool(userData, kKeyCoachmarkVisitDog, mCoachmarkVisitDog);
	setUserBool(userData, kKeyCoachmarkVisitPlanet, mCoachmarkVisitPlanet);
    setUserBool(userData, kKeyCoachmarkSelectChar, mCoachmarkSelectChar);
	
	
	
}

void UserGameData::loadData()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("UserGameData:loadData: userDefault not available");
		return;
	}

	mBestDistance = userData->getIntegerForKey(kKeyBestDistance, 0);
	mLastDistance = userData->getIntegerForKey(kKeyLastDistance, 0);
	mPlayCount = userData->getIntegerForKey(kKeyPlayCount, 0);
	mTotalCoin = userData->getIntegerForKey(kKeyTotalCoin, mDefaultStar);
    mTotalCandy = userData->getIntegerForKey(kKeyTotalCandy, 0);
    mTotalDiamond = userData->getIntegerForKey(kKeyTotalDiamond, kDefaultDiamond);
	mSelectedDog = userData->getIntegerForKey(kKeySelectedDog, 1);
    mBestScore = userData->getIntegerForKey(kKeyBestScore, 0);

	
	// bool
	mTutorialPlayed = getUserBool(userData, kKeyTutorialPlayed, false);
	mBoughtNoAd = getUserBool(userData, kKeyBoughtNoAd, false);
    mBGMisOn = getUserBool(userData, kKeyBGMisOn, true);
    mSEisOn = getUserBool(userData, kKeySEisOn, true);
	mGmMode = getUserBool(userData, kKeyGmMode, false);
    mHasLikedFacebook = getUserBool(userData, kKeyHasLikedFacebook, false);
    
    mLastGachaPlayAdTime = userData->getIntegerForKey(kKeyGachaPlayAdTime);
	
	mCoachmarkVisitDog = getUserBool(userData, kKeyCoachmarkVisitDog, false);
	mCoachmarkVisitPlanet = getUserBool(userData, kKeyCoachmarkVisitPlanet, false);
    mCoachmarkSelectChar = getUserBool(userData, kKeyCoachmarkSelectChar, false);
    
    for(int i=0;i<5;i++){
        std::string keyLocal = StringUtils::format(kKeyCurrentRankLocal.c_str(),i);
        std::string keyFriend = StringUtils::format(kKeyCurrentRankLocal.c_str(),i);
        
        int currentRankFriend = userData->getIntegerForKey(keyFriend.c_str(),-1);
        int currentRankLocal = userData->getIntegerForKey(keyLocal.c_str(),-1);
        
        mLocalCurrentRankMap[i] = currentRankLocal;
        mFriendCurrentRankMap[i] = currentRankFriend;
    }
}

void UserGameData::setGachaLastPlayAdTime(int timeStamp)
{
    mLastGachaPlayAdTime = timeStamp;
    saveData();
}

int UserGameData::getGachaLastPlayAdTime()
{
    return mLastGachaPlayAdTime;
}

void UserGameData::setCurrentLocalRank(int planetID,int rank)
{
    mLocalCurrentRankMap[planetID] = rank;
    std::string keyLocal = StringUtils::format(kKeyCurrentRankLocal.c_str(),planetID);
    UserDefault::getInstance()->setIntegerForKey(keyLocal.c_str(), rank);
}

int UserGameData::getCurrentLocalRank(int planetID)
{
    if(mLocalCurrentRankMap.find(planetID)!=mLocalCurrentRankMap.end()){
        return mLocalCurrentRankMap.at(planetID);
    }else{
        return -1;
    }
}

void UserGameData::setCurrentFriendRank(int planetID,int rank)
{
    mFriendCurrentRankMap[planetID] = rank;
    std::string keyFriend = StringUtils::format(kKeyCurrentRankFriend.c_str(),planetID);
    UserDefault::getInstance()->setIntegerForKey(keyFriend.c_str(), rank);
}

int UserGameData::getCurrentFriendRank(int planetID)
{
    return mFriendCurrentRankMap.at(planetID);
}


void UserGameData::resetCoachmark()
{
	mCoachmarkVisitDog = false;
	mCoachmarkVisitPlanet = false;
    mCoachmarkSelectChar = false;
}

bool UserGameData::isCoachmarkPlayed(const std::string &tutorial)
{
	if("visitDog" == tutorial) {
		return mCoachmarkVisitDog;
	} else if("visitPlanet" == tutorial) {
		return mCoachmarkVisitPlanet;
	} else if("selectChar" == tutorial) {
        return mCoachmarkSelectChar;
    }
	
	return true;
}


void UserGameData::setCoachmarkPlayed(const std::string &tutorial, bool flag)
{
	if("visitDog" == tutorial) {
		mCoachmarkVisitDog = flag;
	} else if("visitPlanet" == tutorial) {
		mCoachmarkVisitPlanet = flag;
    } else if("selectChar" == tutorial) {
        mCoachmarkSelectChar = flag;
    }
	
	saveData();	// TODO
}




bool UserGameData::isTutorialPlayed()
{
	return mTutorialPlayed;
}


void UserGameData::markTutorialPlayed(bool flag)
{
	mTutorialPlayed = flag;
	
	log("markTutorialPlayed: flag=%d", flag);
	
	saveData();
}

int UserGameData::getEnergyMax()
{
	return mEnergyMax;
}

// No Ad
bool UserGameData::isNoAd()
{
 if (UserGameData::isPaidApp())
 {
     return true;
 }
 else {
     return mBoughtNoAd;
 }
}

void UserGameData::setNoAd(bool flag)
{
	mBoughtNoAd = flag;

	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("UserGameData:setNoAd: userDefault not available");
		return;
	}
	
	setUserBool(userData, kKeyBoughtNoAd, flag);
}



bool UserGameData::hasLikedFacebook()
{
    return mHasLikedFacebook;

}
void UserGameData::setLikedFacebook(bool flag)
{
    mHasLikedFacebook = flag;
	
	UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
		return;
	}
	
	setUserBool(userData, kKeyHasLikedFacebook, flag);

}

bool UserGameData::isBGMOn(){
    return mBGMisOn;
}

void UserGameData::setBGMState(bool state){
    mBGMisOn = state;
    
    UserDefault *userData = UserDefault::getInstance();
	setUserBool(userData, kKeyBGMisOn, state);
}

bool UserGameData::isSEOn(){
    return mSEisOn;
}

void UserGameData::setSEState(bool state){
    mSEisOn = state;
	
	UserDefault *userData = UserDefault::getInstance();
	setUserBool(userData, kKeySEisOn, state);
	
}


bool UserGameData::isGmMode()
{
	return mGmMode;
	//return true;
}

void UserGameData::setGmModel(bool enable)
{
	mGmMode = enable;
	UserDefault *userData = UserDefault::getInstance();
	setUserBool(userData, kKeyGmMode, mGmMode);
}


// Selected Dog
int UserGameData::getSelectedDog()
{
	return mSelectedDog;
}

void UserGameData::setSelectedDog(int dogID)
{
	mSelectedDog = dogID;
	
	saveData();
}

std::string UserGameData::info()
{
	std::string result = "";
	
	result += "BestDistance=" + INT_TO_STR(mBestDistance) + "\n";
	result += "mLastDistance=" + INT_TO_STR(mLastDistance) + "\n";
	result += "mPlayCount=" + INT_TO_STR(mPlayCount) + "\n";
	result += "mTotalCoin=" + INT_TO_STR(mTotalCoin) + "\n";
	result += "mTutorialPlayed=" + INT_TO_STR(mTutorialPlayed) + "\n";
	result += "mBoughtNoAd=" + INT_TO_STR(mBoughtNoAd) + "\n";
	result += "mBGMisOn=" + INT_TO_STR(mBGMisOn) + "\n";
	result += "mSEisOn=" + INT_TO_STR(mSEisOn) + "\n";
	result += "mGmMode=" + INT_TO_STR(mGmMode) + "\n";
	result += "mSelectedDog=" + INT_TO_STR(mSelectedDog) + "\n";
	
	
	return result;
}
