//
//  ShaderHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 12/5/2016.
//
//

#ifndef ShaderHelper_hpp
#define ShaderHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "CommonMacro.h"

USING_NS_CC_EXT;
USING_NS_CC;

NS_AURORA_BEGIN

class ShaderHelper {
	public:
	static void setShaderToDrawNode(DrawNode *node, const std::string &vert, const std::string &frag);
};
	
	
NS_AURORA_END

#endif /* ShaderHelper_hpp */
