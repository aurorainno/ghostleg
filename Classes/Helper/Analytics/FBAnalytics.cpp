//
//  FBAnalytics.cpp
//  GhostLeg
//
//  Created by Ken Lee on 27/10/2016.
//
//

#include "FBAnalytics.h"
#include "Constant.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#pragma mark - Android Implementation

#include "AndroidHelper.h"
void FBAnalytics::logEvent(const std::string &msg)
{
	std::string callingClass = kAndroidPackage + ".util.FBAnalytics";
	
	AndroidHelper::callVoidStaticMethod(callingClass, "logEvent", msg.c_str());
}


#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "FBAnalyticsiOS.h"
void FBAnalytics::logEvent(const std::string &msg)
{
	FBAnalyticsiOS::logEvent(msg);
}


#else

#pragma mark - Other Platform Implementation

void FBAnalytics::logEvent(const std::string &msg)
{
}


#endif
