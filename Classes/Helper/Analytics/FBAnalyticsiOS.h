//
//  FBAnalyticsiOS.h
//  GhostLeg
//
//  Created by Ken Lee on 27/10/2016.
//
//

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)


class FBAnalyticsiOS
{
public:
	static void logEvent(const std::string &msg);
};
#endif
