//
//  FBAnalytics.h
//
//	A Helper class for using FB Analytics
//
//	Reference:
//		https://developers.facebook.com/docs/analytics/overview
//
//	Required Library
//		iOS:  ~ https://developers.facebook.com/docs/analytics/quickstart
//			FBSDKCoreKit
//			Bolts
//
//	Dashboard for checking
//			https://www.facebook.com/analytics
//
//
//  Related Code
//		ShareHelperXXX.*
//		AppDelegate.cpp		// iOS: initialize the FB analytics
//
//
//  Created by Ken Lee on 27/10/2016.
//
//

#ifndef FBAnalytics_hpp
#define FBAnalytics_hpp

#include <stdio.h>
#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class FBAnalytics {
public:
	static void logEvent(const std::string &msg);
};

#endif /* FBAnalytics_hpp */
