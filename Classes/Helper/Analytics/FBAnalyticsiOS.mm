//
//  FBAnalyticsiOS.m
//  GhostLeg
//
//  Created by Ken Lee on 27/10/2016.
//
//

#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

// Useful Macro
#define STD_STR_TO_NS_STRING(x)		[NSString stringWithUTF8String:x.c_str()]

//
#import "FBAnalyticsiOS.h"

#pragma mark - Objective C Code
#import "FBSDKCoreKit/FBSDKCoreKit.h"

@interface iOS_FBAnalytics : NSObject
+ (void)logEvent:(NSString *) msg;
@end;


@implementation iOS_FBAnalytics
+ (void)logEvent:(NSString *) msg
{
	NSLog(@"logEvent: %@", msg);
	[FBSDKAppEvents logEvent:msg];
}
@end

#pragma mark - C++ Code


//@implementation FBAnalyticsiOS
//
//@end
void FBAnalyticsiOS::logEvent(const std::string &msg)
{
	[iOS_FBAnalytics logEvent:STD_STR_TO_NS_STRING(msg)];
}

#endif
