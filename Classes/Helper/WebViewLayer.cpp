//
//  WebViewHelper.cpp
//  GhostLeg
//
//  Created by Calvin on 27/6/2016.
//
//

#include "WebViewLayer.h"


bool WebViewLayer::init(){
    if(!Layer::init()){
        return false;
    }
    
    
    Size  winSize = Director::getInstance()->getVisibleSize();
    
    mWebView = cocos2d::experimental::ui::WebView::create();
    mWebView->setPosition(winSize/2);
    mWebView->setContentSize(winSize * 0.7);
    mWebView->loadURL("http://test.aurorainno.com:12046/game/like.html");
    mWebView->setScalesPageToFit(true);
    
    mBackBtn = Button::create();
    mBackBtn->setTitleText("back");
    mBackBtn->setTitleColor(Color3B(0, 255, 0));
    mBackBtn->setPosition(Vec2(mWebView->getPosition().x-mWebView->getContentSize().width/2+mBackBtn->getContentSize().width/2,
                              mWebView->getPosition().y+mWebView->getContentSize().height/2+
                              mBackBtn->getContentSize().height));
    mBackBtn->addClickEventListener([&](Ref* ref){
        if(mWebView->canGoBack())
            mWebView->goBack();
    });
    
    mCloseBtn = Button::create();
    mCloseBtn->setTitleText("close");
    mCloseBtn->setTitleColor(Color3B(255, 0, 0));
    mCloseBtn->setPosition(Vec2(mWebView->getPosition().x+mWebView->getContentSize().width/2-mCloseBtn->getContentSize().width/2,
                               mWebView->getPosition().y+mWebView->getContentSize().height/2+
                               mCloseBtn->getContentSize().height));
    mCloseBtn->addClickEventListener([&,this](Ref* ref){
        mCloseBtn->removeFromParent();
        mWebView->removeFromParent();
        mBackBtn->removeFromParent();
    });
    
    
    this->addChild(mWebView);
    this->addChild(mCloseBtn);
    this->addChild(mBackBtn);
    
    return true;
}