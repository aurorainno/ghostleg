//
//  TouchController.cpp
//  quadro
//
//  Created by EAA van Poeijer on 12-02-13.
//  Updated by Argas on 21-08-14
//

#include "XTLayer.h"
#include <time.h>

XTLayer::XTLayer()
{
    this->_xtSwipeThreshold = 30;
    this->_xtSwipeTime = 500;
    this->_xtTapThreshold =10;
    this->_xtTapTime = 250;
    this->_xtLongTapTreshold = 10;
    this->_xtLongTapTime = 2000;
    this->_xtDoubleTapTime = 250;
    this->_xtNumberOfTaps = 0;
}

XTLayer::~XTLayer()
{
    this->getEventDispatcher()->removeEventListener(eventListener) ;    
}

bool XTLayer::init() {
    if (!Layer::init()) {
        return false;
    }
    // listen for touch events
	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(XTLayer::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(XTLayer::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(XTLayer::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(XTLayer::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
	
//	eventListener = EventListenerTouchOneByOne::create();
//    eventListener->onTouchesBegan = CC_CALLBACK_2(XTLayer::onTouchesBegan, this);
//    eventListener->onTouchesMoved= CC_CALLBACK_2(XTLayer::onTouchesMoved, this);
//    eventListener->onTouchesEnded = CC_CALLBACK_2(XTLayer::onTouchesEnded, this);
//    eventListener->onTouchesCancelled = CC_CALLBACK_2(XTLayer::onTouchesEnded, this);
//    this->getEventDispatcher()->addEventListenerWithFixedPriority(eventListener, 100) ;
//    
    return true;
}

long XTLayer::millisecondNow()
{
    timeval tv;
    gettimeofday(& tv, nullptr);
    return (tv.tv_sec * 1000 + tv.tv_usec / 1000);
}

// Override of touches
bool XTLayer::onTouchBegan(Touch *touch, Event *event)
{
	this->_touchHasMoved = false;
	this->_xtTouchStart = this->millisecondNow();
	
	Touch *fingerOne =touch;
	Vec2  Vec2One = CCDirector::getInstance()->convertToUI(fingerOne->getLocationInView());
	
	Vec2 location = this->convertToNodeSpace(Vec2One);
	this->xtGestureStartVec2= location;//touch locationInView:touch->view);
	this->xtPreviousVec2 = location;
	
	// Prepare touches
	std::vector<Touch *> touches;
	touches.push_back(touch);
	
	// Passthrough
	this->xtTouchesBegan(location);
	this->xtTouchesBegan(touches, event);
	
	return true;

}

void XTLayer::onTouchEnded(Touch *touch, Event *event)
{
	// Prepare touches
	std::vector<Touch *> touches;
	touches.push_back(touch);

	
	long endTime = this->millisecondNow();
	long deltaTime = endTime - this->_xtTouchStart;
	//CCLOG("Deltatime %ld",deltaTime);
	
	Touch* fingerOne = touches.at(0);
	Vec2 Vec2One = CCDirector::getInstance()->convertToUI(fingerOne->getLocationInView());
	
	Vec2 location = this->convertToNodeSpace(Vec2One);
	this->xtGestureEndVec2= location;//touch locationInView:touch->view);
	
	
	bool enableLongTap = _xtLongTapTime > 0;
	bool enableDoubleTap = _xtDoubleTapTime > 0;
	
	// Calculate the distance
	float deltaX = this->xtGestureStartVec2.x - this->xtGestureEndVec2.x;
	float deltaY = this->xtGestureStartVec2.y - this->xtGestureEndVec2.y;
	
	Size screenSize = CCDirector::getInstance()->getWinSize();
	
	float horSwipeDistancePercentage = fabs((deltaX / screenSize.width) * 100);
	float verSwipeDistancePercentage = fabs((deltaY / screenSize.height) * 100);
	
	// Calculate the direction
	// First horizontal or vertical
	if (fabs(deltaX) > fabs(deltaY) && horSwipeDistancePercentage > this->_xtSwipeThreshold && deltaTime < this->_xtSwipeTime) // horizontal
	{
		if (deltaX < 0)
			this->_xtTouchDirection = LEFT;
		if (deltaX > 0)
			this->_xtTouchDirection = RIGHT;
		float speed = fabs(deltaX) / deltaTime;
		this->xtSwipeGesture(this->_xtTouchDirection, fabs(deltaX), speed);
		
		// log("swipeX: delta=%ld", deltaTime);
	}
	else if (fabs(deltaX) < fabs(deltaY) && verSwipeDistancePercentage > this->_xtSwipeThreshold && deltaTime <this->_xtSwipeTime)// Vertical
	{
		if (deltaY < 0)
			this->_xtTouchDirection = DOWN;
		if (deltaY > 0)
			this->_xtTouchDirection = UP;
		float speed = fabs(deltaY) / deltaTime;
		this->xtSwipeGesture(this->_xtTouchDirection, fabs(deltaY), speed);
		
		log("swipeY: delta=%ld", deltaTime);
	}
	else if (enableLongTap && deltaTime >= this->_xtLongTapTime)// No movement, tap detected
	{
		this->xtLongTapGesture(location);
		log("longTap: delta=%ld", deltaTime);
	}
	else
	{
		if(! enableDoubleTap) {
			// Non Double Tap Handling
			this->xtTapGesture(location);
			// log("noDoubleTap: delta=%ld", deltaTime);
		} else {
			// Double Tap Handling
			if (this->_xtNumberOfTaps == 0)
			{
				this->_xtTouchStart = this->millisecondNow();
				this->schedule(schedule_selector(XTLayer::tapHandler), (this->_xtDoubleTapTime / 1000), 1, 0);
			}
			this->_xtNumberOfTaps++;
		}
	}
	
	this->xtTouchesEnded(location);
	this->xtTouchesEnded(touches, event);
}

void XTLayer::onTouchMoved(Touch *touch, Event *event)
{
	this->_touchHasMoved = true;
	
	// Prepare touches
	std::vector<Touch *> touches;
	touches.push_back(touch);

	
	Touch* fingerOne = touches.at(0);
	Vec2 Vec2One = Director::getInstance()->convertToUI(fingerOne->getLocationInView());
	
	Vec2 location = this->convertToNodeSpace(Vec2One);
	this->xtActualVec2 = location;
	
	//CCLOG("Position:%f,%f",location.x,location.y);
	// Passthrough
	this->xtTouchesMoved(location);
	this->xtTouchesMoved(touches, event);

}

void XTLayer::onTouchCancelled(Touch *touch, Event *event)
{
	
}


void XTLayer::tapHandler(float dt)
{
    this->unschedule(schedule_selector(XTLayer::tapHandler));

    if (this->_xtNumberOfTaps == 1 && !this->_touchHasMoved) // singletap
    {
        this->_xtNumberOfTaps = 0;
        this->xtTapGesture(this->xtGestureEndVec2);
    }
    else if (this->_xtNumberOfTaps == 2) // double tap
    {
        this->_xtNumberOfTaps = 0;
        this->xtDoubleTapGesture(this->xtGestureEndVec2);
    }
    else
    {
        this->_xtNumberOfTaps = 0;
    }
	
	log("tapHandler: delta=%f", dt);
}
