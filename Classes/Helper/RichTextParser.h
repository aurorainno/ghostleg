//
//  RichTextParser.hpp
//  GhostLeg
//
//  Created by Ken Lee on 30/11/2016.
//
//

#ifndef RichTextParser_hpp
#define RichTextParser_hpp

#include <stdio.h>
#include <vector>
#include <string>

#include "cocos2d.h"

USING_NS_CC;


class RichTextToken {
public:
	enum RichTextTokenType {
		TokenTypeText,
		TokenTypeTag,
		TokenTypeNewLine,
	};
	
	RichTextTokenType type;
	std::string tag;
	std::string content;
};

class RichTextParser
{
public:
	RichTextParser();
	void parse(const std::string &str);
	
	std::vector<RichTextToken> &getTokenList();
	
public: // static method
	static std::string getTokenString(const RichTextToken &token);
	
private:
	enum ParserState
	{
		StateText,
		StateTag
	};
	
private:
	ParserState mState;
	std::string mTag;
	std::string mBeginTag;
	std::string mContent;
	std::vector<RichTextToken> mTokenList;

	
	
private:
	void enterStateText();
	void exitStateText();
	void enterStateTag();
	void exitStateTag();
	void addNewLineToken();
	void addTextToken();
	void addTagToken(const std::string &tag, const std::string &content);
	void handleStateText(char ch);
	void handleStateTag(char ch);
};

#endif /* RichTextParser_hpp */
