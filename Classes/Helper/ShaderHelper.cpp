//
//  ShaderHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 12/5/2016.
//
//

#include "ShaderHelper.h"

NS_AURORA_BEGIN

void ShaderHelper::setShaderToDrawNode(DrawNode *node,
									   const std::string &vert,
									   const std::string &frag)
{
	auto fileUtiles = FileUtils::getInstance();
	
	// frag
	auto fragmentFilePath = fileUtiles->fullPathForFilename(frag);
	auto fragSource = fileUtiles->getStringFromFile(fragmentFilePath);
	
	// vert
	std::string vertSource;
	if (vert.empty()) {
		vertSource = ccPositionTextureColor_vert;
	} else {
		std::string vertexFilePath = fileUtiles->fullPathForFilename(vert);
		vertSource = fileUtiles->getStringFromFile(vertexFilePath);
	}
	
	auto glprogram = GLProgram::createWithByteArrays(vertSource.c_str(), fragSource.c_str());
	auto glprogramstate = GLProgramState::getOrCreateWithGLProgram(glprogram);
	
	
	if(glprogram) {
		node->setGLProgram(glprogram);
		node->setGLProgramState(glprogramstate);
		glprogram->use();
	}
}

NS_AURORA_END