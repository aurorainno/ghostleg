//
//  TimeMeter.hpp
//  GhostLeg
//
//  Created by Ken Lee on 7/7/2017.
//
//

#ifndef TimeMeter_hpp
#define TimeMeter_hpp

#include <stdio.h>

#include <map>
#include "cocos2d.h"

USING_NS_CC;

#define ENABLE	0

#if(ENABLE == 0)
#define TSTART(__x)
#define TSTOP(__x)
#define TSTOP1(__x, __y)
#else
#define TSTART(__x)		TimeMeter::instance()->start(__x);
#define TSTOP(__x)		TimeMeter::instance()->stop(__x);
#define TSTOP1(__x, __y)		TimeMeter::instance()->stop(__x, __y);
#endif

class TimeMeter
{
public:
	static TimeMeter *instance();

public:
	CC_SYNTHESIZE(std::string, mFilter, Filter);
	CC_SYNTHESIZE(int, mTimeFilter, TimeFilter);
	
public:
	TimeMeter();
	void reset();
	void start(const std::string &key);
	void stop();
	void stop(const std::string &key, const std::string &info = "");
	std::string summary();
	std::string summaryTotal();
	std::string summaryMax();
	
	void showSummary();
	
private:
	std::map<std::string, long long> mStartTimeMap;
	std::map<std::string, long long> mTotalTimeMap;
	std::map<std::string, long long> mMaxTimeMap;
	std::map<std::string, std::string> mMaxTimeInfo;
	std::string mLastKey;
	
};

#endif /* TimeMeter_hpp */
