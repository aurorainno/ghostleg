//
//  DeviceHelper.h
//  TapToJump
//
//  Created by Ken Lee on 10/9/15.
//
//

#ifndef __TapToJump__DeviceHelper__
#define __TapToJump__DeviceHelper__

#include <stdio.h>

#include <string>
#include "cocos2d.h"

USING_NS_CC;

class DeviceHelper {
public:
	static std::string getStoragePath();
	static std::string getFilePathAtStorage(const std::string &shortName);
    static bool isConnectedToInternet();
    static int getBuildNumber();
    static std::string getCountryCode();

};

#endif /* defined(__TapToJump__DeviceHelper__) */
