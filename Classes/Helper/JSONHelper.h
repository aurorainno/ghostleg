//
//  JSONHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/7/2016.
//
//

#ifndef JSONHelper_hpp
#define JSONHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "external/json/document.h"
#include "CommonMacro.h"


USING_NS_CC;

NS_AURORA_BEGIN

class JSONHelper {
public:
	static int getInt(const rapidjson::Value &parent, const char *name, int defaultValue);
	static long getLong(const rapidjson::Value &parent, const char *name, long defaultValue);
    static long long getLongLong(const rapidjson::Value &parent, const char *name, long long defaultValue);
	static bool getBool(const rapidjson::Value &parent, const char *name, bool defaultValue);
	static float getFloat(const rapidjson::Value &parent, const char *name, float defaultValue);
	
	static std::string getString(const rapidjson::Value &parent, const char *name,
						  const std::string &defaultValue = "");
	
	static void getJsonStrArray(const rapidjson::Value &parent,
								const char *name, std::vector<std::string> &outArray,
								bool allowInt = false);
	
	static void getJsonIntArray(const rapidjson::Value &parent, const char *name, std::vector<int> &outArray);
	
	static void getJsonFloatArray(const rapidjson::Value &parent, const char *name, std::vector<float> &outArray);
	
	static void getJsonStrMap(const rapidjson::Value &parent, const std::string &key,
							  std::map<std::string, std::string> &outMap);
    
    static void getJsonStrStrListMap(const rapidjson::Value &parent, const std::string &key,
                                     std::map<std::string, std::vector<std::string>> &outMap);
    
    static void getJsonIntMap(const rapidjson::Value &parent, const std::string &key,
                              std::map<int, int> &outMap);
    
    static void getJsonStrIntMap(const rapidjson::Value &parent, const std::string &key,
                                 std::map<std::string, int> &outMap);
	
	static void getJsonStrFloatMap(const rapidjson::Value &parent, const std::string &key,
								 std::map<std::string, float> &outMap);
	
};

NS_AURORA_END

// Easy to use macro
//#define RECT_TO_STR(__rect__)		aurora::StringHelper::rectToStr(__rect__)
//#define SIZE_TO_STR(__size__)		aurora::StringHelper::sizeToStr(__size__)
//#define POINT_TO_STR(__point__)		aurora::StringHelper::pointToStr(__point__)
//#define FLOAT_TO_STR(__value__)		aurora::StringHelper::floatToStr(__value__)
//#define INT_TO_STR(__value__)		StringUtils::format("%d", __value__)
//#define UNSIGNED_TO_STR(__value__)	StringUtils::format("%zd", __value__)
//#define BOOL_TO_STR(__value__)		std::string(__value__ ? "true" : "false")
//#define VECTOR_TO_STR(__value__)	aurora::StringHelper::vectorToString(__value__)
//

#endif /* JSONHelper_hpp */
