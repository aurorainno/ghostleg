//
//  DataHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 20/4/2017.
//
//

#include "DataHelper.h"

NS_AURORA_BEGIN

int DataHelper::getMapValue(const std::map<std::string, int> &properties,
							const std::string &key, int defaultValue)
{
	if(properties.find(key) == properties.end()) {
		return defaultValue;
	}
	
	return properties.at(key);
}

NS_AURORA_END
