//
//  Mock.cpp
//  GhostLeg
//
//  Created by Ken Lee on 16/5/2016.
//
//

#include "Mock.h"

MasteryData *Mock::createMasteryData(int masteryId,
									  const std::string &name,
									  const std::string &info)
{
	const int maxLevel = 5;
	
	MasteryData *data = MasteryData::create();
	
	data->setMasteryID(masteryId);
	data->setName(name);
	data->setInfo(info);
	data->setMaxLevel(maxLevel);
    data->setFloatFlag(masteryId == 2);
	data->setPercentFlag(masteryId == 3);
	
	const int numLevel = maxLevel + 1;
	float valueArray[numLevel] = {0, 2.5, 3.0, 3.5, 4.5, 5.5};
	int priceArray[numLevel] = {0, 100, 200, 300, 400, 500};
	
	for(int i=0; i<numLevel; i++) {
		data->addLevelPrice(priceArray[i]);
		data->addLevelValue(valueArray[i]);
	}
	
	return data;
}
