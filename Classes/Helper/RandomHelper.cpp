//
//  RandomHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#include "RandomHelper.h"
#include "cocos2d.h"

using namespace aurora;

int RandomHelper::randomBetween(int start, int end)
{
	if(start >= end) {
		return start;		/// exceptional case
	}
	
	int range = end - start;
	
	float randRange = roundf(CCRANDOM_0_1() * range);
	
	return start + (int)randRange;
}

void RandomHelper::shuffleVector(std::vector<int> &vec, int numShuffle)
{
	
	int start = 0;
	int end = (int) vec.size()-1;
	
	for(int i=0; i<numShuffle; i++){
		int pos1 = randomBetween(start, end);
		int pos2 = randomBetween(start, end);
		
		if(pos2 == pos1) {
			pos2 = (pos2 + 1) % vec.size();
		}
		
		CC_SWAP(vec[pos1], vec[pos2], int);
	}
	
}

std::vector<int> RandomHelper::getRandomizeList(std::vector<int> &vec, int numReturn, int numShuffle)
{
	std::vector<int> copyList = vec;
    if(numShuffle > 0) {
        shuffleVector(copyList, numShuffle);
    }
	
	unsigned long numSize = MIN(copyList.size(), numReturn);
	
	std::vector<int>::const_iterator first = copyList.begin();
	std::vector<int>::const_iterator last = copyList.begin() + numSize;
	return std::vector<int>(first, last);
}


int RandomHelper::findRandomValue(std::map<int, int> &chanceTable)
{
	
	int count = 0;
	int totalRange = 0;
	for (std::map<int,int>::iterator it=chanceTable.begin(); it!=chanceTable.end(); ++it) {
		totalRange += it->second;
		count++;
	}
	
	if(count == 0) {
		return 0;
	}
	if(count == 1) {
		return chanceTable.begin()->first;	// first element
	}
	
	
	// Find the random pos
	int randomValue = cocos2d::RandomHelper::random_int(0, totalRange);
	
	
	// Pick the value fall in that range contain the given randomValue
	int currentRange = 0;
	for (std::map<int,int>::iterator it=chanceTable.begin(); it!=chanceTable.end(); ++it) {
		currentRange += it->second;
		
		if(randomValue < currentRange) {
			return it->first;
		}
	}
	
	return chanceTable.begin()->first;	// safe result
}

std::vector<int> RandomHelper::findMultiRandomValue(std::map<int, int> &chanceTable,
													int returnCount, bool allowRepeat)
{
	//
	int totalRow = (int) chanceTable.size();
	
	int nLoop = allowRepeat ? returnCount : MIN(totalRow, returnCount);
	
	std::vector<int> result;
	for(int i=0; i<nLoop; i++) {
		int randomValue = findRandomValue(chanceTable);
		
		result.push_back(randomValue);
		
		
		if(allowRepeat == false) {
			chanceTable[randomValue] = 0;
		}
	}
	
	return result;
}
