//
//  RateAppHelper.hpp
//  GhostLeg
//
//  Created by Calvin on 24/6/2016.
//
//

#ifndef RateAppHelper_hpp
#define RateAppHelper_hpp

#include <stdio.h>
#include "PluginReview/PluginReview.h"
#include "cocos2d.h"

class RateAppHelper : public sdkbox::ReviewListener{
    
public:
    RateAppHelper();
    static RateAppHelper* getInstance();
    void showRatePrompt(bool isForcedShow);
    void increaseUserEventCounter();
    void init(bool force=false);
	void reset();
private:
    virtual void onDisplayAlert() override;
    virtual void onDeclineToRate()override;
    virtual void onRate()override;
    virtual void onRemindLater()override;

private:
	bool mHasInit;		// already init ??
};


#endif /* RateAppHelper_hpp */










