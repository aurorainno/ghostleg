#include "ActionShake.h"

inline float rangedRand(float min, float max)
{
	return RandomHelper::random_real(min, max);
}

Shake* Shake::create(float d, float strength)
{
	return create(d, Vec2(strength, strength));
}

Shake* Shake::create(float duration, cocos2d::Vec2 strength)
{
	Shake* action = new Shake();
	action->initWithDuration(duration, strength);
	action->autorelease();
	
	return action;
}

bool Shake::initWithDuration(float duration, cocos2d::Vec2 strength)
{
	if (ActionInterval::initWithDuration(duration))
	{
		_strength = strength;
		return true;
	}
	
	return false;
}

void Shake::update(float time)
{
	Vec2 rand = Vec2(rangedRand(-_strength.x, _strength.x),
					   rangedRand(-_strength.y, _strength.y));
	
	_target->setPosition(_initial + rand);
}

void Shake::startWithTarget(Node* target)
{
	ActionInterval::startWithTarget(target);
	
	_initial = target->getPosition();
}

void Shake::stop(void)
{
	_target->setPosition(_initial);
	
	ActionInterval::stop();
}
