
#ifndef ActionShake_h
#define ActionShake_h

#pragma once

#include "cocos2d.h"

USING_NS_CC;

class Shake : public cocos2d::ActionInterval
{
public:
	static Shake *create(float duration, float strength);
	static Shake *create(float duration, Vec2 strength);
	
	bool initWithDuration(float d, Vec2 strength);
	
	virtual void startWithTarget(Node *target);
	virtual void update(float time);
	virtual void stop(void);
	
protected:
	cocos2d::Vec2 _initial;
	cocos2d::Vec2 _strength;
};

#endif
