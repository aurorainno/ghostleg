//
//  UpdateChecker.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/11/2016.
//
//

#ifndef UpdateChecker_hpp
#define UpdateChecker_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "network/HttpClient.h"

USING_NS_CC;

using namespace cocos2d::network;

class UpdateChecker
{
public:
	static UpdateChecker *instance();
	
	UpdateChecker();

	CC_SYNTHESIZE(std::string, mGUIFile, GUIFile);		// the csb of the popup windows
	CC_SYNTHESIZE(std::string, mCheckURL, CheckURL);
	
	bool startChecking();
	void showPopup(bool forceUpdate=false);
	void gotoDownloadLink();
	
	
	bool isEventOn();
	
	std::string info();
	
	void testParseResponse();

protected:
	void sendRequest();
	void parseResponse(const std::string &content);
	void handleCheckVersion();
	
	virtual void onHttpRequestCompleted(HttpClient *sender, HttpResponse *response);
	
	void setupButtonListener(Node *rootNode, bool forceUpdate);
	
	void closePopupDialog();
	
private:
	int mClientVersion;
	int mLatestVersion;
	int mRequiredVersion;
	bool mIsEventOn;
	std::string mDownloadURL;
	std::string mMessage;
	Node *mPopupDialog;
	bool mHasChecked;
};

#endif /* UpdateChecker_hpp */
