//
//  UpdateChecker.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/11/2016.
//
//

#include "UpdateChecker.h"

#include "StringHelper.h"
#include "AppHelper.h"
#include "Constant.h"
#include "GameManager.h"
#include "JSONHelper.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

// Using JSON
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

using namespace rapidjson;
using namespace cocos2d::ui;

static UpdateChecker *sInstance = nullptr;

const bool kDefaultEventFlag = false;		// note: set as true because this is the christmas version
											//		after that, set as false

UpdateChecker *UpdateChecker::instance()
{
	if(!sInstance) {
		sInstance = new UpdateChecker();
	}
	return sInstance;
}


UpdateChecker::UpdateChecker()
: mClientVersion(0)
, mLatestVersion(0)
, mRequiredVersion(0)
, mDownloadURL("")
, mGUIFile("")
, mCheckURL("")
, mMessage("")
, mHasChecked(false)
, mIsEventOn(kDefaultEventFlag)
, mPopupDialog(nullptr)
{
	// Update the Curr
	mClientVersion = AppHelper::getBuildNumber();
	setCheckURL(kVersionCheckUrl);
}

bool UpdateChecker::startChecking()
{
	if(mHasChecked) {
		return true;
	}
	
	// Sending Request 
	sendRequest();
	
	return true;
}

void UpdateChecker::sendRequest()
{
	HttpRequest* request = new (std::nothrow) HttpRequest();
	
	// required fields
	request->setUrl(mCheckURL);
	request->setRequestType(HttpRequest::Type::GET);
	request->setResponseCallback(CC_CALLBACK_2(UpdateChecker::onHttpRequestCompleted, this));
	request->setTag("Get the version info");
	HttpClient::getInstance()->sendImmediate(request);
	
	// don't forget to release it, pair to new
	request->release();

}

void UpdateChecker::showPopup(bool forceUpdate)
{
//	if(mParent == nullptr) {
//		log("UpdateChecker: parent not yet set");
//		return;
//	}
	Node *parent = Director::getInstance()->getRunningScene();
	
	// Setup UI
	Node *rootNode = CSLoader::createNode(mGUIFile);
	if(rootNode == nullptr) {
		log("UpdateChecker: fail to load the popup gui");

		return;
	}
	parent->addChild(rootNode);
	mPopupDialog = rootNode;
	
	setupButtonListener(rootNode, forceUpdate);
}


// Note:
//		the button (close & download) are under the main panel
//		Name of node
//			main panel		: mainPanel
//			close button	: closeButton
//			download now	: updateButton
void UpdateChecker::setupButtonListener(Node *rootNode, bool forceUpdate)
{
	if(rootNode == nullptr) {
		log("UpdateChecker: rootNode is null");
		return;
	}
	
	
	Node *mainNode = rootNode->getChildByName("mainPanel");
	if(mainNode == nullptr) {
		log("UpdateChecker: mainPanel is null");
		return;
	}
	
	
	Button *button;
	
	// Close button
	button = mainNode->getChildByName<Button *>("closeButton");
	if(button) {
		if(forceUpdate == true) {
			// No Close button
			button->setVisible(false);
		} else {
			// Allow to close the Update Popup
			button->addClickEventListener([&](Ref *sender) {
				closePopupDialog();
			});
		}
	}
	
	// Download Button
	button = mainNode->getChildByName<Button *>("updateButton");
	if(button) {
		button->addClickEventListener([&](Ref *sender) {
			gotoDownloadLink();
			closePopupDialog();
		});
	}
	
}

void UpdateChecker::gotoDownloadLink()
{
	log("going to the download link: %s", mDownloadURL.c_str());
	Application::getInstance()->openURL(mDownloadURL);
	
}

void UpdateChecker::closePopupDialog()
{
	if(mPopupDialog == nullptr) {
		return;
	}
	mPopupDialog->removeFromParent();
	mPopupDialog = nullptr;
}


std::string UpdateChecker::info()
{
	std::string result = "";
	
	result += StringUtils::format("clientVer=%d latestVer=%d\n", mClientVersion, mLatestVersion);
	result += "CheckURL		: " + mCheckURL + "\n";
	result += "DownloadURL  : " + mDownloadURL + "\n";
	result += "GUI			: " + mGUIFile + "\n";
	result += "Message      : " + mMessage + "\n";
	result += "Event        : " + BOOL_TO_STR(mIsEventOn) + "\n";
	
	return result;
}


void UpdateChecker::onHttpRequestCompleted(HttpClient *sender, HttpResponse *response)
{
	if (!response)
	{
		return;
	}
	
	if(response->isSucceed() == false) {
		return;
	}
	
	log("response returned status=%ld", response->getResponseCode());
	
	
	std::vector<char> *buffer = response->getResponseData();
	std::string result = aurora::StringHelper::charVectorToString((*buffer));
	
	
	// log("Result: %s\n", result.c_str());
	
	parseResponse(result);
	
	log("Result: %s\n", info().c_str());
	if(mDownloadURL == "") {
		log("UpdateChecker: missing the download URL");
		return;
	}
	
	// Update the event flag
	GameManager::instance()->setEventOnOff(false);
	
	//
	handleCheckVersion();
}

void UpdateChecker::parseResponse(const std::string &content)
{
	rapidjson::Document doc;
	doc.Parse<0>(content.c_str());
	if (doc.HasParseError())
	{
		log("parseResponse: %d", doc.GetParseError());
		return;
	}
	
	mLatestVersion = aurora::JSONHelper::getInt(doc, "latest", 0);
	mRequiredVersion = aurora::JSONHelper::getInt(doc, "required", 0);
	mDownloadURL = aurora::JSONHelper::getString(doc, "link");
	mIsEventOn = aurora::JSONHelper::getBool(doc, "event", kDefaultEventFlag);
	
	// message
	std::vector<std::string> msgList;
	aurora::JSONHelper::getJsonStrArray(doc, "note", msgList);
	std::string info = "";
	for(int i=0; i<msgList.size(); i++) {
		if(i > 0) {
			info += "\n";
		}
		info += msgList[i];
	}
	mMessage = info;
	
	
//	log("latest   : %d", mLatestVersion);
//	log("download : %s", mDownloadURL.c_str());
}

void UpdateChecker::testParseResponse()
{
	std::string content = "{";
	content += " \"latest\":10, ";
	content += " \"required\":10, ";
	content += " \"link\":\"https://itunes.apple.com/us/app/astrodog/id1110636077\",";
	content += " \"note\":[";
	content += " \"Line 1\", ";
	content += " \"Line 2\" ";
	content += " ] ";
	content += "} ";
	
	parseResponse(content);
	
	log("debug: %s", info().c_str());
}

void UpdateChecker::handleCheckVersion()
{
	mHasChecked = true;
	
	if(mClientVersion >= mLatestVersion) {
		return;	// nothing to do
	}
	
	bool forceUpdate = mClientVersion < mRequiredVersion;
	
	
	// Show up the GUI
	showPopup(forceUpdate);
}

bool UpdateChecker::isEventOn()
{
	return mIsEventOn;
}
