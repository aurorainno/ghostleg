//
//  RandomHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#ifndef RandomHelper_hpp
#define RandomHelper_hpp

#include <vector>
#include "CommonMacro.h"
#include <map>

NS_AURORA_BEGIN
class RandomHelper {
public:
	static int randomBetween(int start, int end);	// inclusive
	static void shuffleVector(std::vector<int> &intVector, int numShuffle=50);
	static int findRandomValue(std::map<int, int> &chanceTable);	// mapkey=value mapValue=prob
	
	static std::vector<int> getRandomizeList(std::vector<int> &vec, int numReturn, int numShuffle);
	
	// note: the given chanceTable will be modify during the execution
	static std::vector<int> findMultiRandomValue(
				std::map<int, int> &chanceTable, int returnCount, bool allowRepeat=false);
};
NS_AURORA_END




#endif /* RandomHelper_hpp */
