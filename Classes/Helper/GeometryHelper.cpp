//
//  GeometryHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#include "GeometryHelper.h"

using namespace aurora;

namespace {
	//   4 | 1
	//  --------
	//   3 | 2
	typedef enum {
		AngleQuarterXAxis,
		AngleQuarterYAxis,
		AngleQuarter1,
		AngleQuarter2,
		AngleQuarter3,
		AngleQuarter4,
	} AngleQuarter;
	
	// used by findCocosDegree
	AngleQuarter getAngleQuarter(const float &diffX, const float &diffY) {
		if(diffX == 0) {
			return AngleQuarterYAxis;   // diffY > 0: 0  diffY < 0: 180
		} else if(diffY == 0) {
			return AngleQuarterXAxis;   // diffX > 0: 90  diffY < 0: 270
		} else if(diffX > 0 && diffY > 0) {
			return AngleQuarter1;
		} else if(diffX > 0 && diffY < 0) {
			return AngleQuarter2;
		} else if(diffX < 0 && diffY < 0) {
			return AngleQuarter3;
		} else {
			return AngleQuarter4;
		}
	}
	//<#declarations#>
}

bool GeometryHelper::isLineIntersect(const Vec2 &point1, const Vec2 &point2, const Vec2 &check,
									 bool excludePoint1, bool excludePoint2)
{

	if(excludePoint1) {	// No true if point1 or point2 same as the check point
		if(point1 == check) {
			return false;
		}
	}

	if(excludePoint2) {	// No true if point1 or point2 same as the check point
		if(point2 == check) {
			return false;
		}
	}


	// Match vertically
	if(check.x == point1.x && check.x == point2.x) {
		float minY = MIN(point1.y, point2.y);
		float maxY = MAX(point1.y, point2.y);

		return check.y >= minY && check.y <= maxY;
	}

	// Match horizontally
	if(check.y == point1.y && check.y == point2.y) {
		float minX = MIN(point1.x, point2.x);
		float maxX = MAX(point1.x, point2.x);

		return check.x >= minX && check.x <= maxX;
	}

	return false;
}

Rect GeometryHelper::scaleRect(const Rect &inputRect, const float &scale)
{
	float absScale = fabs(scale);		// make it absolute

	Vec2 origin = inputRect.origin * absScale;
	Size size = inputRect.size * absScale;
//	log("scale=%f origin=%s size=%s", scale,
//				POINT_TO_STR(origin).c_str(), SIZE_TO_STR(size).c_str());

	return Rect(origin, size);
}


float GeometryHelper::findAngleRadian(const Vec2 &point1, const Vec2 &point2)
{
	float opposite = point2.y - point1.y;
	float adjacent = point2.x - point1.x;

	float plus = 0;
	if(point1.x > point2.x) {
		plus = MATH_DEG_TO_RAD(180);
	} else {
		plus = point2.y >= point1.y ? 0 : MATH_DEG_TO_RAD(360);
	}

	return plus + atanf(opposite / adjacent);
}

float GeometryHelper::findCocosDegree(const Vec2 &point1, const Vec2 &point2)
{
	float diffX = point2.x - point1.x;
	float diffY = point2.y - point1.y;
	
	
	//   4 | 1
	//  --------
	//   3 | 2
	// used by findCocosDegree
	AngleQuarter quarter = getAngleQuarter(diffX, diffY);
	if(AngleQuarterYAxis == quarter) {				// direction lay on Y-axis
		return diffY > 0 ? 0 : 180;
	} else if(AngleQuarterXAxis == quarter) {		// direction lay on X-axis
		return diffX > 0 ? 90 : 270;
	}
	
	//
	float h = fabsf(point2.y - point1.y);
	float w = fabsf(point2.x - point1.x);
	float degree = CC_RADIANS_TO_DEGREES(atan(w/h));
	
	if(AngleQuarter1 == quarter) {
		return degree;
	} else if(AngleQuarter2 == quarter) {
		return 180 - degree;
	} else if(AngleQuarter3 == quarter) {
		return 180 + degree;
	} else {	// Quarter 4
		return 360 - degree;
	}
}


float GeometryHelper::findAngleDegree(const Vec2 &point1, const Vec2 &point2)
{
	float rad = findAngleRadian(point1, point2);
	return MATH_RAD_TO_DEG(rad);
}

Vec2 GeometryHelper::resolveVec2(float distance, float angleRadian)
{
	Vec2 result;

	result.x = distance * cosf(angleRadian);
	result.y = distance * sinf(angleRadian);

	return result;
}

Vec2 GeometryHelper::calculateNewTracePosition(
					const Vec2 &myPos, const Vec2 &targetPos,
					float velocity, float timeDelta, bool stopAtTarget)
{
	float distance = targetPos.distance(myPos);

	if(distance <= velocity * timeDelta) {
		return targetPos;
	}


	float angle = aurora::GeometryHelper::findAngleRadian(myPos, targetPos);
	Vec2 diff = aurora::GeometryHelper::resolveVec2(timeDelta * velocity, angle);
	return myPos + diff;
}

//static Vec2 checkIfCrossXAxis(const Vec2 &line1a,
//							  const Vec2 &line1b, float xAxis);


bool GeometryHelper::checkIfCrossXAxis(const Vec2 &point1,
									   const Vec2 &point2,
									   float xAxis)
{
	float startX = point1.x < point2.x ? point1.x : point2.x;
	float endX = point1.x > point2.x ? point2.x : point1.x;
	
	
	
	return startX >= xAxis && endX <= xAxis;
}


Vec2 GeometryHelper::getNodeFinalScale(Node *checkNode, Node *rootNode)
{
	if(checkNode == nullptr || rootNode == nullptr) {
		//log("error: getNodeFinalScale: input are null");
		return Vec2(1.0f, 1.0f);
	}
	
	float totalScaleX = checkNode->getScaleX();
	float totalScaleY = checkNode->getScaleY();
	
	int maxLoop = 10;		// guarding mechanic against infinite loop
	
	Node *currentNode = checkNode;
	for(int i=0; i<maxLoop; i++) {
		currentNode = currentNode->getParent();
		if(currentNode == nullptr) {
			break;
		}
		
		totalScaleX *= currentNode->getScaleX();
		totalScaleY *= currentNode->getScaleY();
		
		if(currentNode == rootNode) {
			break;	// already reach the destination
		}
	}
	
	
	return Vec2(totalScaleX, totalScaleY);
}



bool GeometryHelper::anyCollision(const std::vector<Rect> &rectList1, const std::vector<Rect> &rectList2)
{
//	if(rectList1.size() == 0 || rectList2.size() == 0) {
//		return false;
//	}
//	
	for(Rect rect1 : rectList1) {
		for(Rect rect2 : rectList2) {
			if(rect1.intersectsRect(rect2)) {
				return true;
			}
		}
	}
//
	
	return false;
}

bool GeometryHelper::anyCollision(const std::vector<Rect> &rectList, const std::vector<Circle> &circleList)
{
	for(Rect rect : rectList) {
		for(Circle circle : circleList) {
			if(rect.intersectsCircle(circle.origin, circle.radius)){
				return true;
			}
		}
	}
	
	return false;
}

bool GeometryHelper::anyCollision(const std::vector<Circle> &circleList1, const std::vector<Circle> &circleList2)
{
	
	for(Circle circle1 : circleList1) {
		for(Circle circle2 : circleList2) {
			if(circle1.intersectsCircle(circle2)) {
				return true;
			}
		}
	}

	
	return false;
}

bool GeometryHelper::anyCollision(const std::vector<Circle> &circleList, const Rect &hitbox)
{
	for(Circle circle : circleList) {
		if(hitbox.intersectsCircle(circle.origin, circle.radius)){
			return true;
		}
	}
	return false;
}

bool GeometryHelper::anyCollision(const std::vector<Rect> &rectList, const Rect &hitbox)
{
	for(Rect rect : rectList) {
		if(rect.intersectsRect(hitbox)) {
			return true;
		}
	}
	return false;
}
