//
//  JSONHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/7/2016.
//
//

#include "JSONHelper.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include "StringHelper.h"

using namespace aurora;

namespace {
	std::string getStringFromStrOrInt(const rapidjson::Value &value) {
		if(value.IsInt()) {
			return StringUtils::format("%d", value.GetInt());
		} else {
			return value.GetString();
		}
	}
}

int JSONHelper::getInt(const rapidjson::Value &parent, const char *name, int defaultValue)
{
	if(parent.HasMember(name) == false) {
		return defaultValue;
	}
	
	if(parent[name].IsNull()) {
		return defaultValue;
	}
	
	return parent[name].GetInt();
}

long JSONHelper::getLong(const rapidjson::Value &parent, const char *name, long defaultValue)
{
	if(parent.HasMember(name) == false) {
		return defaultValue;
	}
	
	return parent[name].GetInt64();
}

long long JSONHelper::getLongLong(const rapidjson::Value &parent, const char *name, long long defaultValue)
{
    if(parent.HasMember(name) == false) {
        return defaultValue;
    }
    
	return parent[name].GetUint64();
}

bool JSONHelper::getBool(const rapidjson::Value &parent, const char *name, bool defaultValue)
{
	if(parent.HasMember(name) == false) {
		return defaultValue;
	}
	
	return parent[name].GetBool();
}

float JSONHelper::getFloat(const rapidjson::Value &parent, const char *name, float defaultValue)
{
	if(parent.HasMember(name) == false) {
		return defaultValue;
	}
	
	return parent[name].GetDouble();
}

std::string JSONHelper::getString(const rapidjson::Value &parent, const char *name,
					  const std::string &defaultValue)
{
	if(parent.HasMember(name) == false) {
		return defaultValue;
	}
	
	if(parent[name].IsNull()) {
		return defaultValue;
	}
	
	return parent[name].GetString();
}

void JSONHelper::getJsonIntArray(const rapidjson::Value &parent, const char *name, std::vector<int> &outArray)
{
	if(parent.HasMember(name) == false) {
		return;
	}
	
	const rapidjson::Value &arrayValue = parent[name];
	
	if(arrayValue.IsArray() == false) {
		return;
	}
	
	for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
	{
		outArray.push_back(arrayValue[i].GetInt());
	}
}



void JSONHelper::getJsonStrArray(const rapidjson::Value &parent,
					const char *name, std::vector<std::string> &outArray,
					bool allowInt)
{
	if(parent.HasMember(name) == false) {
		return;
	}
	
	const rapidjson::Value &arrayValue = parent[name];
	if(arrayValue.IsArray() == false) {
		return;
	}
	
	for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
	{
		
		std::string str;
		if(allowInt == false) {
			str = arrayValue[i].GetString();
		} else {
			str = getStringFromStrOrInt(arrayValue[i]);
		}
		
		outArray.push_back(str);
	}
}

void JSONHelper::getJsonFloatArray(const rapidjson::Value &parent, const char *name, std::vector<float> &outArray)
{
	if(parent.HasMember(name) == false) {
		return;
	}
	
	const rapidjson::Value &arrayValue = parent[name];
	if(arrayValue.IsArray() == false) {
		return;
	}
	
	for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
	{
		outArray.push_back((float) arrayValue[i].GetDouble());
	}
}

void JSONHelper::getJsonStrIntMap(const rapidjson::Value &parent, const std::string &key,
                                  std::map<std::string, int> &outMap)
{
    if(parent.HasMember(key.c_str()) == false) {
        return;
    }
    
    const rapidjson::Value &jsonValue = parent[key.c_str()];
    
    for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
         itr != jsonValue.MemberEnd();
         ++itr)
    {
        std::string key = itr->name.GetString();
        const rapidjson::Value &object = jsonValue[key.c_str()];
        int value = object.GetInt();
        
        //log("getJsonStrMap: key=%s value=%s", key.c_str(), value.c_str());
        outMap[key] = value;
    }
}


void JSONHelper::getJsonStrFloatMap(const rapidjson::Value &parent, const std::string &key,
								  std::map<std::string, float> &outMap)
{
	if(parent.HasMember(key.c_str()) == false) {
		return;
	}
	
	const rapidjson::Value &jsonValue = parent[key.c_str()];
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr)
	{
		std::string key = itr->name.GetString();
		const rapidjson::Value &object = jsonValue[key.c_str()];
		float value = (float) object.GetDouble();
		
		//log("getJsonStrMap: key=%s value=%s", key.c_str(), value.c_str());
		outMap[key] = value;
	}
}



void JSONHelper::getJsonIntMap(const rapidjson::Value &parent, const std::string &key,
                               std::map<int, int> &outMap)
{
    if(parent.HasMember(key.c_str()) == false) {
        return;
    }
    
    const rapidjson::Value &jsonValue = parent[key.c_str()];
    
    for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
         itr != jsonValue.MemberEnd();
         ++itr)
    {
        std::string key = itr->name.GetString();
        const rapidjson::Value &object = jsonValue[key.c_str()];
        int value = object.GetInt();
        
        //log("getJsonStrMap: key=%s value=%s", key.c_str(), value.c_str());
        int k = StringHelper::parseInt(key);
        outMap[k] = value;
    }
}

void JSONHelper::getJsonStrMap(const rapidjson::Value &parent, const std::string &key,
							   std::map<std::string, std::string> &outMap)
{
	if(parent.HasMember(key.c_str()) == false) {
		return;
	}
	
	const rapidjson::Value &jsonValue = parent[key.c_str()];
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr)
	{
		std::string key = itr->name.GetString();
		const rapidjson::Value &object = jsonValue[key.c_str()];
		std::string value = object.GetString();
		
		//log("getJsonStrMap: key=%s value=%s", key.c_str(), value.c_str());
		outMap[key] = value;
	}
	
	//jsonValue.
	
	//		for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
	//		{
	//			outArray.push_back((float) arrayValue[i].GetDouble());
	//		}
}

void JSONHelper::getJsonStrStrListMap(const rapidjson::Value &parent, const std::string &key,
                                      std::map<std::string, std::vector<std::string> > &outMap)
{
    if(parent.HasMember(key.c_str()) == false) {
        return;
    }
    
    const rapidjson::Value &jsonValue = parent[key.c_str()];
    
    for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
         itr != jsonValue.MemberEnd();
         ++itr)
    {
        std::string key = itr->name.GetString();
        const rapidjson::Value &object = jsonValue[key.c_str()];
        
        std::vector<std::string> strList{};
        
        for (int i=0;i<object.Size();i++){
            std::string value = object[i].GetString();
            strList.push_back(value);
        }
        
        //log("getJsonStrMap: key=%s value=%s", key.c_str(), value.c_str());
        outMap[key] = strList;
    }
    

}
