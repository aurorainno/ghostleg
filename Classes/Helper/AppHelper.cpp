//
//  AppHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 4/5/2016.
//
//

#include "AppHelper.h"
#include "cocos2d.h"
#include "DeviceHelper.h"

USING_NS_CC;

std::string AppHelper::getVersionString()		// complete version string
{
	return Application::getInstance()->getVersion();
}

int AppHelper::getBuildNumber()
{
	return DeviceHelper::getBuildNumber();		// TODO: not yet know!!!
}
