//
//  ViewHelper.h
//  RPSGame
//
//	A Helper class that help to do somethings on the View
//
//  Created by Ken Lee on 30/5/15.
//
//

#ifndef __RPSGame__ViewHelper__
#define __RPSGame__ViewHelper__

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC_EXT;
USING_NS_CC;

// Cocos Studio
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
using namespace cocostudio::timeline;


#define kDEFAULT_COLOR Color4B(0x31, 0xd8, 0xd4, 0xff)

// 
#define kMaskSpriteTag	10000

class ViewHelper
{
public:
	enum BackHandleType {
		PopScene,
		RemoveFromParent,
	};
public:
	static void setWidgetSwallowTouches(Node *node, bool needSwallow);
	static void showFadeAlert(Node *parent, const std::string &msg, int y,
							  int fontSize = 12,
							  const Color4B &color = Color4B(0x33, 0x33, 0x99, 0xff)
							  );
	
	static Node *createDebugSpot(Node *parent, const Vec2 &pos, const Color4B &color, int size=5);
	static Node *createRedSpot(Node *parent, const Vec2 &pos);
	
	static void showFadeAlert(Node *parent, const std::string &msg, Vec2 pos,
							   int fontSize = 15,
							   const Color4B &color = kDEFAULT_COLOR);
	
	static void adjustPanelSize(Node *uiPanel, const Size &newSize, const std::vector<std::string> &adjustList);
	
	static MenuItem *createMenuItemWithFont(const char *name, const char *font,
												 const ccMenuCallback& callback);
	static Menu *createMenu(Point pos, const char *name, const ccMenuCallback& callback);
	
	static Label *createLabel(const std::string &msg, int fontSize = 15,
							  const Color4B &color = kDEFAULT_COLOR);
	
	// Owner is the master node contain the button and its parent
	static void addBackButtonListener(Node *owner, ui::Button *button, BackHandleType backType);
	
	static ui::Button *createButton(const std::string &name, const int fontSize,
									const Color3B &textColor = Color3B::WHITE);

	
	static ClippingNode *createMaskLayer(const Color4B &bgColor,
						const std::string &maskSpriteName, const Vec2 &maskPosition);
	
	static Node *createAnimationNode(const std::string &csbName, const std::string &animationName,
                                     bool isRepeat, bool removeSelf, const std::function<void()> &endCallback = NULL);
	
	static Vec2 convertBearingCoordinate(float radius, float degree);
	
	static void drawPie(DrawNode *node, float radius,
						float startDegree, float endDegree,
						const Color4F &color,
						const Vec2 &orgin = Vec2::ZERO);
	
	
	static void drawLine(DrawNode *drawNode, const Vec2 &start, const Vec2 &end,
						 float thickness, const Color4F &color);

	static void drawAxis(DrawNode *drawNode, const Vec2 &origin, const Color4B &color);

	static Texture2D *getMaskTexture(const Size &textureSize, float greyLevel,
						  float circleRadius, const Vec2 &maskPos);

	
	static void setSolidBackground(ui::Layout *layout, const Color4B &color);

	
	
	static ui::Text *createOutlineText(const std::string &textStr, const int fontSize,
									   const Color3B &textColor,
									   const int &borderSize,
									   const Color3B &borderColor);

	// unicodeText mean the text may contain chinese
	static void setUnicodeText(ui::Text *textUI, const std::string &textStr);
};

#endif /* defined(__RPSGame__ViewHelper__) */
