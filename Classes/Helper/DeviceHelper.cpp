//
//  DeviceHelper.cpp
//  TapToJump
//
//  Created by Ken Lee on 10/9/15.
//
//

#include "DeviceHelper.h"
#include "ShareHelperIOS.h"
#include "StringHelper.h"
#include "Constant.h"

#pragma mark # Platform Dependent #
#pragma mark ### Android ####
#if CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID
#include "AndroidHelper.h"

std::string DeviceHelper::getStoragePath()
{
	std::string callingClass = kAndroidPackage + ".util.DeviceHelper";
	std::string result = AndroidHelper::getStaticVoidMethodResult(callingClass, "getExternalPath");
	
	return result;
}

bool DeviceHelper::isConnectedToInternet()
{
	std::string callingClass = kAndroidPackage + ".util.DeviceHelper";
	std::string result = AndroidHelper::getStaticVoidMethodResult(callingClass, "getNetworkConnectStatus");

	log("Network connect result: %s", result.c_str());
	
	
    return result == "connected";
}

int DeviceHelper::getBuildNumber()
{
	std::string callingClass = kAndroidPackage + ".util.DeviceHelper";
    std::string result = AndroidHelper::getStaticVoidMethodResult(callingClass, "getBuildNumber");
    
    int num = aurora::StringHelper::parseInt(result);
    
//    log("build number: %d", num);
    
    return num;
}

std::string DeviceHelper::getCountryCode()
{
	std::string callingClass = kAndroidPackage + ".util.DeviceHelper";
    std::string result = AndroidHelper::getStaticVoidMethodResult(callingClass, "getCountryCode");
   
    return result;
}

#else
#pragma mark ### Cocos ####
std::string DeviceHelper::getStoragePath()
{
	return FileUtils::getInstance()->getWritablePath();
}

bool DeviceHelper::isConnectedToInternet ()
{
    return ShareHelperiOS::isNetworkAvaliable();
}

int DeviceHelper::getBuildNumber()
{
    return ShareHelperiOS::getBuildNumber();
}

std::string DeviceHelper::getCountryCode()
{
    return ShareHelperiOS::getCountryCode();
}

#endif



#pragma mark # Platform Independent #

std::string DeviceHelper::getFilePathAtStorage(const std::string &shortName)
{
	std::string path = getStoragePath();
	
	
	// path.append("/");
	path.append(shortName);
	
	
	return path;
}
