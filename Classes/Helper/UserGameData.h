//
//  UserGameData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 24/4/2016.
//
//

#ifndef UserGameData_hpp
#define UserGameData_hpp

#include <stdio.h>
#include <string>

#include "cocos2d.h"

USING_NS_CC;

class UserGameData
{
public: // Type and Enum
	// static UserGameData *instance();
	CC_SYNTHESIZE(int, mDefaultStar, DefaultStar);
	
    static bool isPaidApp();
	
    
    void updateScore(int newScore);
    void setScore(int score);
    int  getScore();
    
	void setDistance(int newScore);
	int getLastDistance();
	int getBestDistance();
	int getPlayCount();
	bool isNewRecord();	// is the last record new record
	void saveData();
	void saveMoney();		// a lite version of saveData
    void saveCandy();
    void saveDiamond();
    void saveScore();
	void loadData();
	void clearRecord();
    
    
    //gacha playAd Time
    void setGachaLastPlayAdTime(int timeStamp);
    int getGachaLastPlayAdTime();
	
	// Gm Mode
	void setGmModel(bool enable);
	bool isGmMode();
	
	// Tutorial
	bool isTutorialPlayed();
	void markTutorialPlayed(bool flag);
	
	// GUI Tutorial
	bool isCoachmarkPlayed(const std::string &tutorial);
	void setCoachmarkPlayed(const std::string &tutorial, bool flag);
	
	
    //rank
    void setCurrentLocalRank(int planetID, int rank);
    int getCurrentLocalRank(int planetID);
    void setCurrentFriendRank(int planetID, int rank);
    int getCurrentFriendRank(int planetID);
    
	// Coin
	void addCoin(int coin, bool autoSave=true);
	int getTotalCoin();
    
    // Candy
    void addCandy(int num);
    int getTotalCandy();
	
    //Diamond
    void addDiamond(int num);
    int getTotalDiamond();

	// Energy
	int getEnergyMax();
	
	// No Ad
	bool isNoAd();
	void setNoAd(bool flag);
    
    //Has Facebook Like
    bool hasLikedFacebook();
    void setLikedFacebook(bool flag);
    
    //music (BGM)
    bool isBGMOn();
    void setBGMState(bool state);
    
    //sound effect
    bool isSEOn();
    void setSEState(bool state);
	
	// Selected Dog
	int getSelectedDog();
	void setSelectedDog(int dogID);
	
	void resetCoachmark();
	
	std::string info();
	
public:
	UserGameData();
	
	
private:
    int mBestScore;
	int mBestDistance;
	int mLastDistance;
	int mTotalCoin;
    int mTotalCandy;
    int mTotalDiamond;
	bool mIsNewRecord;
	int mPlayCount;
	bool mTutorialPlayed;
	int mEnergyMax;
	bool mBoughtNoAd;
    bool mBGMisOn;
    bool mSEisOn;
	bool mGmMode;
    bool mHasLikedFacebook;
	int mSelectedDog;
	
	bool mCoachmarkVisitDog;
	bool mCoachmarkVisitPlanet;
    bool mCoachmarkSelectChar;
    
    std::map<int,int> mLocalCurrentRankMap;
    std::map<int,int> mFriendCurrentRankMap;
    
    int mLastGachaPlayAdTime;
};
#endif /* UserGameData_hpp */
