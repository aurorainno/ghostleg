//
//  WebViewHelper.hpp
//  GhostLeg
//
//  Created by Calvin on 27/6/2016.
//
//

#ifndef WebViewLayer_hpp
#define WebViewLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/UIWebView.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class WebViewLayer : public Layer{
public:
    CREATE_FUNC(WebViewLayer);
    virtual bool init();
private:
    cocos2d::experimental::ui::WebView* mWebView;
    Button* mBackBtn;
    Button* mCloseBtn;
    
};



#endif /* WebViewLayer_hpp */
