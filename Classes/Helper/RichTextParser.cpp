//
//  RichTextParser.cpp
//  GhostLeg
//
//  Created by Ken Lee on 30/11/2016.
//
//

#include "RichTextParser.h"


#include <iostream>
#include <string>
#include <vector>
#include <regex>

using namespace std;


RichTextParser::RichTextParser()
{
	mState = StateText;
	mTag = "";
	mContent = "";
}
	
void RichTextParser:: enterStateText()
{
	mContent = "";
	mState = StateText;
}
	
void RichTextParser:: exitStateText()
{

}
	
void RichTextParser:: enterStateTag()
{
	mState = StateTag;
	mTag = "";
}
	
void RichTextParser:: exitStateTag()
{
	if(mTag.at(0) == '/') { // End of the Tag
		addTagToken(mBeginTag, mContent);
	} else {  // Save the content before the tag
		addTextToken();
		
		mContent = "";
		mBeginTag = mTag;
	}
	
	
	//cout << "tag=" << mTag << "\n";
}


void RichTextParser::addTagToken(const std::string &tag, const std::string &content)
{
	if(content == "") {
		return;
	}
	
	RichTextToken token;
	token.type = RichTextToken::TokenTypeTag;
	token.tag = tag;
	token.content = content;
	mTokenList.push_back(token);
}

void RichTextParser:: addNewLineToken()
{
	RichTextToken token;
	token.type = RichTextToken::TokenTypeNewLine;
	token.tag = "";
	token.content = "";
	mTokenList.push_back(token);
}
	
void RichTextParser:: addTextToken() {
	if(mContent == "") {
		return;
	}
		
	RichTextToken token;
	token.type = RichTextToken::TokenTypeText;
	token.tag = "";
	token.content = mContent;
	mTokenList.push_back(token);
}
	
	
void RichTextParser:: handleStateText(char ch)
{
	if(ch == '<') {
		exitStateText();
		enterStateTag();
		return;
	}
	
		
	if(ch == '\n') {
		exitStateText();
			
		addTextToken();
		addNewLineToken();
			
		enterStateText();
		return;
	}
		
	mContent += ch;
}
	
void RichTextParser:: handleStateTag(char ch)
{
	if(ch == '>') {
		exitStateTag();
		enterStateText();
		return;
	}
	
	mTag += ch;
}

	
void RichTextParser::parse(const std::string &inStr)
{
	std::string str = inStr;
	
	mTokenList.clear();
	
	std::string tag = "";
	std::string content = "";
	for ( std::string::iterator it=str.begin(); it!=str.end(); ++it)
	{
		char ch = *it;
		switch(mState) {
			case StateText: {
				handleStateText(ch);
				break;
			}
				
			case StateTag: {
				handleStateTag(ch);
				break;
			}
		}
		// std::cout << ch;
		// std::cout << '\n';
	}
	addTextToken();
	
//	for(int i=0; i<mTokenList.size(); i++) {
//		RichTextToken token = mTokenList[i];
//		
//		cout << "tag=" << token.tag << " content=[" << token.content << "]\n";
//	}
	
}


std::vector<RichTextToken> &RichTextParser::getTokenList()
{
	return mTokenList;
}

std::string RichTextParser::getTokenString(const RichTextToken &token)
{
	return StringUtils::format("type=%d tag=%s content=%s",
				(int) token.type, token.tag.c_str(), token.content.c_str());
}
