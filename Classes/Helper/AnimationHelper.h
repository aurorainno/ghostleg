//
//  AnimationHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 17/6/2016.
//
//

#ifndef AnimationHelper_hpp
#define AnimationHelper_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "GameSound.h"

USING_NS_CC_EXT;
USING_NS_CC;
using namespace cocos2d::ui;

class AnimationHelper
{
public:
    static void setMoveUpEffect(Node* node, float cycleDuration, float offsetY, bool moveUpFirst, int repeatCount);
	static void setJellyEffect(Node *node, float cycleDuration, float scale, int repeatCount);
	static void setBlinkEffect(Node *node, float cycleDuration, int repeatCount);
	static void setAlphaBlink(Node *node, float duration, int repeatCount,
							  int opacity1, int opacity2,
							  const std::function<void ()> &func = nullptr);
	
	static void setForeverBlink(Node *node, float duration, int opacity);
    
    static void popTextEffect(Node* node, Text *text, int originValue, int finalValue, std::function<void()> callback = nullptr,
                              float finishTime = 0.8, float targetScale = 1.7, std::string displayFormat = "%d",
                              GameSound::Sound sound = GameSound::Star5);
	
	static ActionInterval *getCurveAction(const Vec2 &fromPos, const Vec2 &toPos, float duration, int style=0);
	
	static ActionInterval *getScaleUpAction(const float &scale, const float &scaleDuration,
											const float &fadeDuration);
	
	static ActionInterval *getPopupAction(const float &initialScale,
										  const float &finalScale,
										  const float &duration,
										  const std::function<void ()> &callback = nullptr);
	
	
	static void runPopupAction(Node *targetNode,
							   const float &initialScale,
                               const float &finalScale,
							   const float &duration,
							   const std::function<void ()> &callback = nullptr);
    
    static void runPopupAndPopbackAction(Node *targetNode,
                               const float &initialScale,
                               const float &finalScale,
                               float duration,
                               const std::function<void ()> &callback = nullptr);


	
	static ActionInterval *getClosingAction(const float &duration,
											bool removeOnClose = true,
											const std::function<void ()> &callback = nullptr);


	static void runClosingAction(Node *targetNode,
								 const float &duration,
								 bool removeOnClose = true,
								 const std::function<void ()> &callback = nullptr);
	
	static RepeatForever *getRotateForever(const float &cycleDuration, bool isClockwise = true);

	static void runRotateForeverAction(Node *targetNode,
								 const float &duration, bool isClockwise);
	
	static void addAuraRotateAction(Node *parentNode, const float &duration,
									int numAura = 2,
									const std::string &nodePattern = "unlock_aura_%d");
	
	static void addRotatingAura(Node *parentNode,
								const float &duration,
								int numAura,
								const std::string &auraImage,
								const Vec2 &pos = Vec2::ZERO);

	static void setToastEffect(cocos2d::Node *targetNode,		// Visible for a short time and then hide
									 const float &finalScale,
									 float delay,
							         float showDuration,
									 float popDuration,
									 const std::function<void ()> &callback = nullptr);


	
	static ActionInterval *createMoveToAction(float duration, const Vec2 &finalPos);
	
	static ActionInterval *createEaseAction(ActionInterval *originAction, const std::string &easeName);
};

#endif /* AnimationHelper_hpp */
