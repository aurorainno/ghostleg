//
//  ShareHelper.cpp
//  TapToJump
//
//  Created by Ken Lee on 10/9/15.
//
//

#include "ShareHelper.h"
#include "Constant.h"

#if(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include "AndroidHelper.h"
void ShareHelper::share(const std::string &msg, const std::string &image)
{
	//std::string callingClass = kAndroidPackage + ".util.ShareHelper";
	AndroidHelper::callVoidStaticMethod(
						ANDROID_CLASS(util.ShareHelper), "shareScoreImage", msg.c_str());
}


#elif(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "ShareHelperiOS.h"
void ShareHelper::share(const std::string &msg, const std::string &image)
{
	std::string imagePath = image; // FileUtils::getInstance()->getWritablePath();
	// imagePath.append(image);

	ShareHelperiOS::shareTextAndImage(msg, imagePath);
}



#else
void ShareHelper::share(const std::string &msg, const std::string &image)
{
	log("showScore: Feature not support!");
}

#endif
