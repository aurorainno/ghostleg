//
//  ShareHelperiOS.h
//  TapToJump
//
//  Created by Ken Lee on 25/9/15.
//
//

#include "cocos2d.h"
#include <string>

// only iOS can view ShareHelperiOS
#if(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

// Implementation at ShareHelperiOS.mm
//
class ShareHelperiOS	// rename iOSNativeHelper later
{
public:
	static void shareTextAndImage(const std::string &text, const std::string &imagePath);
    static bool isNetworkAvaliable();
    static int getBuildNumber();
    static std::string getCountryCode();
};

#endif
