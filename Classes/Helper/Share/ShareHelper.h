//
//  ShareHelper.h
//  TapToJump
//
//  Created by Ken Lee on 10/9/15.
//
//

#ifndef __TapToJump__ShareHelper__
#define __TapToJump__ShareHelper__

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class ShareHelper {
public:
	static void share(const std::string &msg, const std::string &image = "");
};

#endif /* defined(__TapToJump__ShareHelper__) */
