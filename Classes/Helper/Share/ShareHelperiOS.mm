//
//  ShareHelperiOS.m
//  TapToJump
//
//  Created by Ken Lee on 25/9/15.
//
//

#import "ShareHelperiOS.h"



#pragma mark - Objective C Code
#import "AppController.h"
#import <UIKit/UIKit.h>
#import "Reachability.h"

#pragma mark - Interface Definition
@interface iOSShareHelper : NSObject {
}

+ (void)shareByAirDropWithText:(NSString *)text image:(NSString *)imagePath;
+ (bool)isNetworkAvaliable;

@end;


@implementation iOSShareHelper

+ (void)shareByAirDropWithText:(NSString *)text image:(NSString *)imagePath			// note: it is not airdrop
{
	NSLog(@"Share by AirDrop: text=%@ image=%@", text, imagePath);
	
	UIImage *image = [UIImage imageWithContentsOfFile:imagePath];
	
	NSArray *objectsToShare = [NSArray arrayWithObjects:text, image, text,  nil];
	
	
	AppController *appController = (AppController *)[[UIApplication sharedApplication] delegate];
	
	
	UIActivityViewController *controller = [[UIActivityViewController alloc]
											initWithActivityItems:objectsToShare
											applicationActivities:nil];

	UIViewController *vc = (UIViewController *) appController.viewController;
	
	[vc presentViewController:controller animated:YES completion:nil];

}

+ (int)getBuildNumber
{
	NSDictionary *infoDictionary = [[NSBundle mainBundle]infoDictionary];
	
	return [infoDictionary[@"CFBundleVersion"] intValue];
}

+ (std::string)getCountryCode
{
    NSString *countryCode = [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode];
    std::string codeStr = std::string([countryCode UTF8String]);
    return codeStr;
}

+ (bool)isNetworkAvaliable
{
    Reachability *aReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus aNetworkStatus = [aReachability currentReachabilityStatus];
    bool haveNetwork = (aNetworkStatus != 0);
    return haveNetwork;
}




@end




#pragma mark - C Code

void ShareHelperiOS::shareTextAndImage(const std::string &text, const std::string &imagePath)
{
	NSString *textStr = [NSString stringWithUTF8String:text.c_str()];
	NSString *imagePathStr = [NSString stringWithUTF8String:imagePath.c_str()];
	
	//	[iOSShareHelper share
	[iOSShareHelper shareByAirDropWithText:textStr image:imagePathStr];
}


bool ShareHelperiOS::isNetworkAvaliable()
{
    return [iOSShareHelper isNetworkAvaliable];
}

int ShareHelperiOS::getBuildNumber()
{
    return [iOSShareHelper getBuildNumber];
}

std::string ShareHelperiOS::getCountryCode()
{
    return [iOSShareHelper getCountryCode];
}
