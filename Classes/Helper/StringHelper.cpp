//
//  StringHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#include "StringHelper.h"
#include <iomanip>

// Macro

NS_AURORA_BEGIN
// BEGIN OF NAMESPACE
	
std::string &StringHelper::ltrim(std::string &s)
{
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
	return s;
}

// trim from end
std::string &StringHelper::rtrim(std::string &s)
{
	s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
	return s;
}

// trim from both ends
std::string &StringHelper::trim(std::string &s)
{
	return ltrim(rtrim(s));
}


std::string StringHelper::circleToStr(const Circle &circle)
{
	return StringUtils::format("(%.2f,%.2f,%.2f", circle.origin.x, circle.origin.y, circle.radius);
}

std::string StringHelper::rectToStr(const Rect &rect)
{
	char temp[100];
	
	sprintf(temp, "(%.2f,%.2f)(%.2f,%.2f)", rect.origin.x, rect.origin.y,
							rect.size.width, rect.size.height);
	
	return temp;
}
	
std::string StringHelper::sizeToStr(const Size &size)
{
	char temp[100];
	
	sprintf(temp, "(%.2f,%.2f)", size.width, size.height);
	
	return temp;
}
	
std::string StringHelper::pointToStr(const Point &point)
{
	char temp[100];
	
	sprintf(temp, "(%.2f,%.2f)", point.x, point.y);
	
	return temp;

}

	
std::string StringHelper::floatToStr(const float &value)
{
	char temp[100];
	
	sprintf(temp, "%.2f", value);
	
	return temp;
}

float StringHelper::parseFloat(const std::string &str)
{
	float result;
	
	sscanf(str.c_str(), "%f", &result);
	
	
	return result;
}

int StringHelper::parseInt(const std::string &str)
{
	int result;
	
	sscanf(str.c_str(), "%d", &result);
	
	
	return result;
}

// @see:
//	http://stackoverflow.com/questions/236129/split-a-string-in-c
void StringHelper::split(const std::string &s, char delim, std::vector<std::string> &elems)
{
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
}


std::vector<std::string> StringHelper::split(const std::string &s, char delim)
{
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}


std::string StringHelper::getFormattedString(float value, std::string format){
        std::string s ;
    
        int first = (int)format.find_first_of("#");
        int last = (int)format.find_last_of("#");
        std::string preUnit = format.substr(0,first);
        std::string unit = format.substr(last+1);
    
        if(last-first!=0){
            int n = last-first;
            std::stringstream stream;
            stream << std::fixed << std::setprecision(n)<< value;
            s = preUnit+stream.str()+unit;
        }else{
            s = preUnit+INT_TO_STR((int)value)+unit;
        }
    
//        log("StringHelper getFormattedString result: %s",s.c_str());
    
        return s;
}

std::string StringHelper::vectorToString(const std::vector<float> array, const std::string &sep)
{
	std::stringstream ss;
	
	for(int i=0; i<array.size(); i++) {
		float value = array[i];
		if(i > 0) {
			ss << sep;
		}
		
		ss << value;
	}
	
	return ss.str();
}

std::string StringHelper::vectorToString(const std::vector<int> array, const std::string &sep)
{
	std::stringstream ss;
	
	for(int i=0; i<array.size(); i++) {
		int value = array[i];
		if(i > 0) {
			ss << sep;
		}
		
		ss << value;
	}
	
	return ss.str();
}

std::string StringHelper::mapToString(const std::map<std::string, std::string> &userMap, const std::string &sep)
{
	
	std::map<std::string, std::string> myMap = userMap;
	std::stringstream ss;
	
	bool isFirstPair = true;
	std::map<std::string, std::string>::iterator it;
	for(it = myMap.begin(); it != myMap.end(); it++) {
		std::string key = it->first;
		std::string value = it->second;
		
		if(!isFirstPair) {
			ss << sep;
		}
		
		ss << key;
		ss << "=";
		ss << value;
		
		isFirstPair = false;
	}
 
	
	return ss.str();
}

std::string StringHelper::mapToString(const std::map<std::string, float> &userMap,
									  const std::string &sep)
{
	
	std::map<std::string, float> myMap = userMap;
	std::stringstream ss;
	
	bool isFirstPair = true;
	std::map<std::string, float>::iterator it;
	for(it = myMap.begin(); it != myMap.end(); it++) {
		std::string key = it->first;
		float value = it->second;

		if(!isFirstPair) {
			ss << sep;
		}

		ss << key;
		ss << "=";
		ss << value;

		isFirstPair = false;
	}
 
	
	return ss.str();
}

std::string StringHelper::mapToString(const std::map<std::string, int> &userMap,
									  const std::string &sep)
{
	
	std::map<std::string, int> myMap = userMap;
	std::stringstream ss;
	
	bool isFirstPair = true;
	std::map<std::string, int>::iterator it;
	for(it = myMap.begin(); it != myMap.end(); it++) {
		std::string key = it->first;
		float value = it->second;
		
		if(!isFirstPair) {
			ss << sep;
		}
		
		ss << key;
		ss << "=";
		ss << value;
		
		isFirstPair = false;
	}
 
	
	return ss.str();
}



std::string StringHelper::mapToString(const std::map<int, int> &userMap, const std::string &sep)
{
	
	std::map<int, int> myMap = userMap;
	std::stringstream ss;
	
	bool isFirstPair = true;
	std::map<int, int>::iterator it;
	for(it = myMap.begin(); it != myMap.end(); it++) {
		int key = it->first;
		int value = it->second;
		
		if(!isFirstPair) {
			ss << sep;
		}
		
		ss << key;
		ss << "=";
		ss << value;
		
		isFirstPair = false;
	}
 
	
	return ss.str();
}

// this method work with HttpRequest response
std::string StringHelper::charVectorToString(const std::vector<char> &array)
{
	std::string result = "";
	
	for(char ch : array) {
		result += ch;
	}
	
	return result;
}


bool StringHelper::replaceString(std::string &src, const std::string &sym, const std::string &val)
{
    std::size_t found = src.find(sym);
    if (found!=std::string::npos){
        src.replace(found,sym.length(),val);
        return true;
    }else{
        //log("StringHelper::replaceString: cannot find symbol to replace!");
        return false;
    }
}

std::string StringHelper::formatDecimal(float value)
{
	bool hasDecimal = value - floorf(value) > 0;
	
	if(hasDecimal) {
		std::string str = StringUtils::format("%.2f", value);
				
		str.erase(str.find_last_of('0'), std::string::npos );
		
		return str;
	} else {
		return StringUtils::format("%.0f", value);
		
	}
	
}

bool StringHelper::contains(const std::string &src, const std::string &find)
{
	return src.find(find) != std::string::npos;
}

bool StringHelper::endsWith(const std::string &src, const std::string &find)
{
	if(find.length() == 0) {
		return true;
	}
	
	return true;
}


bool StringHelper::startsWith(const std::string &src, const std::string &find)
{
	return src.find(find) == 0;
}

bool StringHelper::containCJKChar(const std::string &s)
{
	std::string myStr = s;
	
    // ref: https://en.wikipedia.org/wiki/UTF-8
	for(std::string::iterator it = myStr.begin();it!=myStr.end();it++) {
		uint8_t byte = *it;
        if(byte >= 0xe0){
            return true;
        }
	}
	return false;
}

bool StringHelper::containNonAlpha(const std::string &s)
{
	std::string myStr = s;
	
	// ref: https://en.wikipedia.org/wiki/UTF-8
	for(std::string::iterator it = myStr.begin();it!=myStr.end();it++) {
		uint8_t byte = *it;
		if(isspace(byte)) {
			continue;
		}
		if(isalnum(byte) == false){
			return true;
		}
//		if(byte >= 0xe0){
//			return true;
//		}
	}
	return false;
}

// END OF NAMESPACE
NS_AURORA_END


