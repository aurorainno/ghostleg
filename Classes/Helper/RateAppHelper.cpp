//
//  RateAppHelper.cpp
//  GhostLeg
//
//  Created by Calvin on 24/6/2016.
//
//
#include "RateAppHelper.h"


static RateAppHelper *sInstance = NULL;


RateAppHelper::RateAppHelper()
: mHasInit(false)
{
}

RateAppHelper *RateAppHelper::getInstance()
{
    if(sInstance != NULL) {
        return sInstance;
    }
    
    sInstance = new RateAppHelper();
    
    return sInstance;
}

void RateAppHelper::init(bool force){
	if(force == true || mHasInit == false) {
		sdkbox::PluginReview::init();
		sdkbox::PluginReview::setListener(this);
		mHasInit = true;
	}
}


void RateAppHelper::showRatePrompt(bool isForcedShow){
    // printf("showRatePrompt is triggered.\n");
    
    sdkbox::PluginReview::show(isForcedShow);
    
}

void RateAppHelper::increaseUserEventCounter(){
    sdkbox::PluginReview::userDidSignificantEvent(true);
}

void RateAppHelper::onDisplayAlert(){
    //printf("onDisplayAlert is triggered.\n");
}

void RateAppHelper::onDeclineToRate(){
    //printf("onDeclineToRate is triggered.\n");
}

void RateAppHelper::onRate(){
    //printf("onRate is triggered.\n");
}

void RateAppHelper::onRemindLater(){
    //printf("onRemindLater is triggered.\n");
}

void RateAppHelper::reset()
{
	//sdkbox::PluginReview::
}





