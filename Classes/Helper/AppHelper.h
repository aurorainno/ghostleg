//
//  AppHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 4/5/2016.
//
//

#ifndef AppHelper_hpp
#define AppHelper_hpp

#include <stdio.h>
#include <string>

class AppHelper
{
public:
	static std::string getVersionString();		// complete version string
	static int getBuildNumber();
};

#endif /* AppHelper_hpp */
