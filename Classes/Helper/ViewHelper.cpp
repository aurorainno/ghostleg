//
//  ViewHelper.cpp
//  RPSGame
//
//  Created by Ken Lee on 30/5/15.
//
//

#include "ViewHelper.h"

// For cocos 2d
using namespace cocos2d;


// For cocos studio
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "StringHelper.h"

const std::string kDefaultFont = "ObelixPro-cyr.ttf";


using namespace cocos2d::ui;

void ViewHelper::adjustPanelSize(Node *uiPanel, const Size &newSize, const std::vector<std::string> &adjustList)
{
	if(uiPanel == nullptr) {
		return;
	}
	
	float diffY = uiPanel->getContentSize().height - newSize.height;
	for(int i=0; i<adjustList.size(); i++) {
		std::string nodeName = adjustList[i];
		
		Node *node = uiPanel->getChildByName(nodeName);
		if(node != nullptr) {
			Vec2 pos = node->getPosition();
			pos.y -= diffY;
			node->setPosition(pos);
		}
	}
	
}



void ViewHelper::setWidgetSwallowTouches(Node *node, bool needSwallow)
{
	Widget *widget = dynamic_cast<Widget *>(node);
	if(widget == nullptr) {
		return;
	}
	
	widget->setSwallowTouches(needSwallow);
}


void ViewHelper::showFadeAlert(Node *parent, const std::string &msg, int y,
							   int fontSize,
							   const Color4B &color)
{
	auto size = Director::getInstance()->getWinSize();
	Vec2 pos = Vec2(size.width/2, y);

	showFadeAlert(parent, msg, pos, fontSize, color);
}


void ViewHelper::showFadeAlert(Node *parent, const std::string &msg, Vec2 pos,
							   int fontSize,
							   const Color4B &color)
{
	TTFConfig ttfConfig("ObelixPro-cyr.ttf", fontSize * 2);
	
	auto size = Director::getInstance()->getWinSize();
	auto label = Label::createWithTTF(ttfConfig, msg.c_str(), TextHAlignment::CENTER, size.width);
	label->setPosition(pos);
	label->setTextColor(color);
	label->setScale(0.5f);
	parent->addChild(label);

	DelayTime *delay = DelayTime::create(1);
	FadeOut *fadeOut = FadeOut::create(0.5);
	RemoveSelf *remove = RemoveSelf::create();
	
	Sequence *sequence = Sequence::create(delay, fadeOut, remove, nullptr);
	
	label->runAction(sequence);
}

Label *ViewHelper::createLabel(const std::string &msg, int fontSize,
							   const Color4B &color)
{
	TTFConfig ttfConfig("Caviar_Dreams_Bold.ttf", fontSize * 2);
	
	auto label = Label::createWithTTF(ttfConfig, msg.c_str(),
									  TextHAlignment::CENTER, 0);
	label->setTextColor(color);
	label->setScale(0.5f);
	
	return label;
}


MenuItem *ViewHelper::createMenuItemWithFont(const char *name, const char *font,
											const ccMenuCallback& callback)
{
	float fontSize = 20;
	Label *label = Label::createWithSystemFont(name, font, fontSize);
	label->setTextColor(Color4B::WHITE);
	
	return MenuItemLabel::create(label, callback);
}


Menu *ViewHelper::createMenu(Point pos, const char *name, const ccMenuCallback& callback)
{
	
	auto menuItem = createMenuItemWithFont(name, "arial", callback);
	Menu *menu = Menu::create(menuItem, NULL);
	menu->setPosition(pos);
	
	return menu;
}

Node *ViewHelper::createDebugSpot(Node *parent, const Vec2 &pos, const Color4B &color, int size)
{
	//int size = 30;
	LayerColor *dot = LayerColor::create(color, size, size);
	
	Vec2 finalPos = Vec2(pos.x - size/2, pos.y - size/2);
	dot->setPosition(finalPos);
	
	if(parent) {
		parent->addChild(dot);
	}
	
	return dot;
}

Node *ViewHelper::createRedSpot(Node *parent, const Vec2 &pos)
{
	return createDebugSpot(parent, pos, Color4B::RED);
}


void ViewHelper::addBackButtonListener(Node *owner, ui::Button *button, BackHandleType backType)
{
	if(button == nullptr) {
		return;
	}
	
	//set lisenter for back button
	button->addTouchEventListener([owner, backType](Ref *sender, Widget::TouchEventType type) {
		switch(type){
			case ui::Widget::TouchEventType::ENDED: {
				if(backType == BackHandleType::PopScene) {
					Director::getInstance()->popScene();
					
				} else if(backType == BackHandleType::RemoveFromParent) {
					if(owner) {
						owner->removeFromParent();
					}
				} else {
					log("addBackButtonListener: unknown handleType");
				}
				
			}
		}
	});
}

ui::Button *ViewHelper::createButton(const std::string &name, const int fontSize,
									 const Color3B &textColor)
{
	cocos2d::ui::Button *button = cocos2d::ui::Button::create();
	button->setTitleText(name);
	button->setTitleFontSize(fontSize);
	button->setTitleColor(textColor);
	
	return button;
}

ClippingNode * ViewHelper::createMaskLayer(const Color4B &bgColor, const std::string &maskSpriteName, const Vec2 &maskPosition)
{
	// Mask Layer
	LayerColor *maskLayer = LayerColor::create(bgColor);	// Own by maskNode
	
	
	// Mask Sprite
	Sprite *mask = Sprite::create(maskSpriteName);			// Own by stencil
	mask->setPosition(maskPosition);
	
	// Create stencil
	Node *stencil = Node::create();		// Own by maskNode
	stencil->addChild(mask);
	
	
	// The mask (clipping node)
	ClippingNode *maskNode = ClippingNode::create();
	maskNode->setInverted(true);
	maskNode->setAlphaThreshold(0);
	maskNode->setStencil(stencil);
	maskNode->addChild(maskLayer);
	
	return maskNode;
}

Node *ViewHelper::createAnimationNode(const std::string &csbName,
									  const std::string &animationName,
									  bool isRepeat, bool removeSelf,
                                      const std::function<void()> &endCallback)
{
	Node *csbNode = CSLoader::createNode(csbName);
	if(csbNode == nullptr) {
		log("addCsbEffectByName. cannot create the node: %s", csbName.c_str());
		return nullptr;
	}
	
	// Setting the animation
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
	if(timeline == nullptr) {
		log("addCsbAnimeByName:. cannot create the timeLine: %s", csbName.c_str());
		return nullptr;
	}
	
	csbNode->runAction(timeline);
	csbNode->setUserObject(timeline);
	
	timeline->play(animationName, isRepeat);
	
	
	if(isRepeat == false && removeSelf) {
		timeline->setAnimationEndCallFunc(animationName, [&,csbNode,endCallback]() {
			csbNode->removeFromParent();
            if(endCallback){
                endCallback();
            }
		});
	}
	
	return csbNode;
}


void ViewHelper::drawPie(DrawNode *node, float radius,
						 float startDegree, float endDegree,
						 const Color4F &color,
						 const Vec2 &origin)
{
	
	int nPoints = (endDegree - startDegree) / 2;		// todo
	float borderWidth = 0;
	Color4F fillColor = color;
	Color4F borderColor = color;
	
	if(nPoints < 5) { nPoints = 5; }
	
	
	float angleStep = (endDegree - startDegree) / nPoints;
	
	std::vector<cocos2d::Point> circle;
	
	circle.emplace_back(origin);
	
	float angle = startDegree;
	for(int i=0; i<=nPoints; i++) {
		Vec2 point = convertBearingCoordinate(radius, angle) + origin;
		circle.emplace_back(point);
		
		angle += angleStep;
	}
	
	node->drawPolygon(circle.data(), circle.size(), fillColor, borderWidth, borderColor);
	
//	
//	const cocos2d::Vec2 start = origin + Vec2(radius, 0);
//	const auto angle_step = 2 * M_PI * angleDegree / 360.f / num_of_points;
//	
//	std::vector<cocos2d::Point> circle;
//	
//	circle.emplace_back(origin);
//	for (int i = 0; i <= num_of_points; i++)
//	{
//		auto rads = angle_step * i;
//		auto x = origin.x + radius * cosf(rads);
//		auto y = origin.y + radius * sinf(rads);
//		
//		circle.emplace_back(x, y);
//	}
//	
//	node->drawPolygon(circle.data(), circle.size(), fillColor, borderWidth, borderColor);
}

// The degree start from Y-axis
Vec2 ViewHelper::convertBearingCoordinate(float radius, float degree)
{
	float rads = CC_DEGREES_TO_RADIANS(degree);
	
	float y = radius * cosf(rads);
	float x = radius * sinf(rads);
	
	return Vec2(x, y);
}


void ViewHelper::drawLine(DrawNode *drawNode, const Vec2 &start, const Vec2 &end,
							float thickness, const Color4F &color)
{
	if(drawNode == nullptr) {
		return;
	}
	
	drawNode->drawSegment(start, end, thickness, color);
}

void ViewHelper::drawAxis(DrawNode *drawNode, const Vec2 &origin, const Color4B &color)
{
	Color4F colorf = Color4F(color);
	
	
	Vec2 start, end;
 
	// X Axis
	start = Vec2(0, origin.y);
	end = Vec2(1000, origin.y);
	drawLine(drawNode, start, end, 1, colorf);

	// Y Axis
	start = Vec2(origin.x, 0);
	end = Vec2(origin.x, 1000);
	drawLine(drawNode, start, end, 1, colorf);

	
	//drawLine(drawNode, )
}

Texture2D *ViewHelper::getMaskTexture(const Size &textureSize, float greyLevel,
							float circleRadius, const Vec2 &maskPos)
{
	Color4F color = Color4F(greyLevel, greyLevel, greyLevel, 1);
	Vec2 topRight = Vec2(textureSize.width, textureSize.height);
	
	DrawNode *drawNode = DrawNode::create();
	drawNode->drawSolidRect(Vec2(0, 0), topRight, color);	// shadow area
	drawNode->drawSolidCircle(maskPos, circleRadius, circleRadius, 100, 1, 1, Color4F::WHITE);					// light area
	
	RenderTexture *texture = RenderTexture::create(
									textureSize.width, textureSize.height,
									Texture2D::PixelFormat::RGBA8888);
	
	texture->begin();
	drawNode->visit();
	texture->end();	// New texture created!!!
	
	
	return texture->getSprite()->getTexture();
}


void ViewHelper::setSolidBackground(ui::Layout *layout, const Color4B &color)
{
	layout->setBackGroundColorType(Layout::BackGroundColorType::SOLID);
	layout->setBackGroundColor(Color3B(color));
}

ui::Text *ViewHelper::createOutlineText(const std::string &textStr, const int fontSize,
										const Color3B &textColor,
										const int &borderSize,
										const Color3B &borderColor)
{
	ui::Text *text = ui::Text::create(textStr, kDefaultFont, fontSize);
	
	text->enableOutline(Color4B(borderColor), borderSize);
	
	return text;
}



void ViewHelper::setUnicodeText(ui::Text *textUI, const std::string &textStr)
{
	if(textUI == nullptr) {
		return;
	}
	
	
	if(aurora::StringHelper::containNonAlpha(textStr)){
		textUI->setFontName("");
	}else{
		textUI->setFontName("ObelixPro-cyr.ttf");
	}
	
	
	textUI->setString(textStr);
}
