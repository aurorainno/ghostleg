//
//  GeometryHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/3/2016.
//
//

#ifndef GeometryHelper_hpp
#define GeometryHelper_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "Circle.h"

USING_NS_CC;

namespace aurora {
	class GeometryHelper {
		public:
		static bool isLineIntersect(const Vec2 &point1, const Vec2 &point2,
									const Vec2 &check,
									bool excludePoint1 = false,
									bool excludePoint2 = false);

		static Rect scaleRect(const Rect &inputRect, const float &scale);
		
		static float findAngleDegree(const Vec2 &point1, const Vec2 &point2);
		static float findAngleRadian(const Vec2 &point1, const Vec2 &point2);
		static float findCocosDegree(const Vec2 &point1, const Vec2 &point2);
		
		static Vec2 resolveVec2(float distance, float angleRadian);
		static Vec2 calculateNewTracePosition(const Vec2 &myPos,
						const Vec2 &targetPos,
						float velocity, float timeDelta, bool stopAtTarget=true);

		static bool checkIfCrossXAxis(const Vec2 &line1a,
						const Vec2 &line1b, float xAxis);	// 0 1 2 3 s

		static Vec2 getNodeFinalScale(Node *checkNode, Node *rootNode);
		
		
		static bool anyCollision(const std::vector<Rect> &rectList1, const std::vector<Rect> &rectList2);
		static bool anyCollision(const std::vector<Rect> &rectList, const std::vector<Circle> &circleList);
		static bool anyCollision(const std::vector<Circle> &circleList1, const std::vector<Circle> &circleList2);
		
		static bool anyCollision(const std::vector<Circle> &circleList, const Rect &hitbox);
		static bool anyCollision(const std::vector<Rect> &rectList, const Rect &hitbox);
	};
}

#endif /* GeometryHelper_hpp */
