//
//  DataHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 20/4/2017.
//
//

#ifndef DataHelper_hpp
#define DataHelper_hpp

#include <stdio.h>

#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <map>

#include "cocos2d.h"
#include "CommonMacro.h"

USING_NS_CC;




//template<typename T>
//void foo(T s)
//{
//	log("DEBUG: %d", s);
//}
//

#define DEQUEUE_VECTOR(_QUEUE_V_, _OUT_V_, _SIZE_, _T_) \
{\
	unsigned long size = MIN(_QUEUE_V_.size(), _SIZE_); \
	_OUT_V_.clear(); \
	if(size > 0) {  \
		int count = 0; \
		for(_T_ item : _QUEUE_V_) { \
		   _OUT_V_.push_back(item); \
		   count++; \
		   if(count >= size) { \
			  break; \
		   } \
		}\
		_QUEUE_V_.erase(_QUEUE_V_.begin(), _QUEUE_V_.begin()+size);\
	}\
}



NS_AURORA_BEGIN

class DataHelper {
public:
	
	
	static int getMapValue(const std::map<std::string, int> &properties,
						   const std::string &key, int defaultValue);
	
	
	// Vector Dequeue
	template <typename T>
	static void dequeueVector(std::vector<T> &queueVector,
					   std::vector<T> &outVector, int dequeueSize)
	{
		int size = MIN((int) queueVector.size(), dequeueSize);
		
		outVector.clear();
		
		if(size == 0) {
			return;
		}
		
		int count = 0;
		for(T item : queueVector) {
			outVector.push_back(item);
			count++;
			if(count >= size) {
				break;
			}
		}
		
		
		// Clear the vector
		queueVector.erase(queueVector.begin(), queueVector.begin()+size);
	}
	
	
	// Vector one element Dequeue
	template <typename T>
	static void dequeueVector(std::vector<T> &queueVector, T &value)
	{
		if(queueVector.size() == 0) {
			return;
		}
		
		value = queueVector.at(0);
		
		// Clear the vector
		queueVector.erase(queueVector.begin(), queueVector.begin()+1);
	}
	
};

NS_AURORA_END

#endif /* DataHelper_hpp */
