//
//  TimeMeter.cpp
//  GhostLeg
//
//  Created by Ken Lee on 7/7/2017.
//
//

#include "TimeMeter.h"

static TimeMeter *sInstance = nullptr;

namespace {
	long long getCurrentTime() {
		auto timeNow = std::chrono::system_clock::now().time_since_epoch();
		
		std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(timeNow);
		
		long long timeMillis = ms.count();

		return timeMillis;
	}
}

TimeMeter *TimeMeter::instance()
{
	if(!sInstance) {
		sInstance = new TimeMeter();
	}
	return sInstance;
}

TimeMeter::TimeMeter()
: mTimeFilter(0)
, mFilter("Collision")
{
	
}

void TimeMeter::reset()
{
	mStartTimeMap.clear();
	mMaxTimeMap.clear();
	mTotalTimeMap.clear();
}

std::string TimeMeter::summary()
{
	std::string result = "";
	result = "Total Time Used:\n";
	for (auto it=mTotalTimeMap.begin(); it!=mTotalTimeMap.end(); ++it) {
		result += StringUtils::format("%s: %lld\n", it->first.c_str(), it->second);
	}
	
	result += "---\n";
	result += "Max Time Used:\n";
	for (auto it=mMaxTimeMap.begin(); it!=mMaxTimeMap.end(); ++it) {
		std::string info = mMaxTimeInfo[it->first];
		result += StringUtils::format("%s: %lld %s\n", it->first.c_str(), it->second, info.c_str());
	}

	return result;
}


std::string TimeMeter::summaryMax()
{
	std::string result = "";
	result += "Max Time Used:\n";
	for (auto it=mMaxTimeMap.begin(); it!=mMaxTimeMap.end(); ++it) {
		std::string info = mMaxTimeInfo[it->first];
		result += StringUtils::format("%s: %lld %s\n", it->first.c_str(), it->second, info.c_str());
	}
	
	return result;
}


std::string TimeMeter::summaryTotal()
{
	std::string result = "";
	result = "Total Time Used:\n";
	for (auto it=mTotalTimeMap.begin(); it!=mTotalTimeMap.end(); ++it) {
		result += StringUtils::format("%s: %lld\n", it->first.c_str(), it->second);
	}
	return result;
}


void TimeMeter::start(const std::string &key)
{
	mStartTimeMap[key] = getCurrentTime();
	mLastKey = key;
}

void TimeMeter::stop() {
	stop(mLastKey);
}

void TimeMeter::stop(const std::string &key, const std::string &info)
{
	if(mStartTimeMap.find(key) == mStartTimeMap.end()){
		return;
	}
	
	long long startTime = mStartTimeMap[key];
	
	long long timeDiff = getCurrentTime() - startTime;
	
	// Record the total
	if(mTotalTimeMap.find(key) == mTotalTimeMap.end()){
		mTotalTimeMap[key] = timeDiff;
	} else {
		mTotalTimeMap[key] += timeDiff;
	}
	
	// Record the max
	if(mTotalTimeMap.find(key) == mTotalTimeMap.end()){
		mTotalTimeMap[key] = timeDiff;
	} else {
		mTotalTimeMap[key] += timeDiff;
	}
	
	// Record the max
	if(mMaxTimeMap.find(key) == mMaxTimeMap.end()){
		mMaxTimeMap[key] = timeDiff;
		mMaxTimeInfo[key] = info;
	} else {
		long long oldTime = mMaxTimeMap[key];
		if(timeDiff > oldTime) {
			mMaxTimeMap[key] = timeDiff;
			mMaxTimeInfo[key] = info;
		}
	}
	

	
	if(mTimeFilter <= 0 || timeDiff >= mTimeFilter) {
		std::string infoStr = info == "" ? "" : StringUtils::format("\t [%s]", info.c_str());
		
		if(mFilter == "" || mFilter == key) {
			log("[%s: %lldms] %s", key.c_str(), timeDiff, infoStr.c_str());
		}
	}
}

void TimeMeter::showSummary()
{
	log("=====================");
	log("TimeMeter: Summary ");
	log("=====================");
	log("%s", summaryTotal().c_str());
	log("%s", summaryMax().c_str());
}
