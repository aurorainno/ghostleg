//
//  Mock.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/5/2016.
//
//

#ifndef Mock_hpp
#define Mock_hpp

#include <stdio.h>
#include <string.h>
#include "Mastery.h"

class Mock {
public:
	static MasteryData *createMasteryData(int masteryId,
							const std::string &name,
							const std::string &info);
	
};


#endif /* Mock_hpp */
