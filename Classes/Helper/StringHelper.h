//
//  StringHelper.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#ifndef StringHelper_hpp
#define StringHelper_hpp

#include <stdio.h>
#include <vector>
#include <string>
#include <sstream>
#include <map>

#include "cocos2d.h"
#include "CommonMacro.h"
#include "Circle.h"

USING_NS_CC;

NS_AURORA_BEGIN

class StringHelper {
public:
	static bool containCJKChar(const std::string &s);
	static bool containNonAlpha(const std::string &s);
	
	
	static std::string &ltrim(std::string &s);
	
	// trim from end
	static std::string &rtrim(std::string &s);
	
	// trim from both ends
	static std::string &trim(std::string &s);

	static std::string rectToStr(const Rect &rect);
	static std::string circleToStr(const Circle &circle);
	static std::string sizeToStr(const Size &size);
	static std::string pointToStr(const Point &size);
	static std::string floatToStr(const float &value);
    
    
	// Value from string
	static int parseInt(const std::string &str);
	static float parseFloat(const std::string &str);
	
	
	// Split a string
	static void split(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> split(const std::string &s, char delim);
    
    /*
     get a string with a desired format:
     format: sign + num of digit + unit (eg: +## sec)
     note: when num of # = 1, display as integer.
     Example: take 3.1417 as value, when format =:
     "+# sec", return "+3 sec"
     "##%", return "3.1%"
     "###", return "3.14"
    */
    static std::string getFormattedString(float value,std::string format);
	
	static std::string vectorToString(const std::vector<float> array, const std::string &sep = ",");
	static std::string vectorToString(const std::vector<int> array, const std::string &sep = ",");
	
	
	static std::string mapToString(const std::map<std::string, std::string> &map, const std::string &sep = ",");
	static std::string mapToString(const std::map<int, int> &map, const std::string &sep = ",");
	static std::string mapToString(const std::map<std::string, int> &userMap,
										  const std::string &sep = ",");
	static std::string mapToString(const std::map<std::string, float> &userMap,
										  const std::string &sep = ", ");
	
	static std::string charVectorToString(const std::vector<char> &array);
    
    static bool replaceString(std::string &src,const std::string &sym, const std::string &val);
	
	static bool endsWith(const std::string &src, const std::string &find);
	static bool startsWith(const std::string &src, const std::string &find);
	static bool contains(const std::string &src, const std::string &find);
	
	static std::string formatDecimal(float value);	//
	
//    bool eraseString(std::string &src,const std::string &sym, const std::string &val);
};

NS_AURORA_END

// Easy to use macro
#define RECT_TO_STR(__rect__)		aurora::StringHelper::rectToStr(__rect__)
#define CIRCLE_TO_STR(__circle__)	aurora::StringHelper::circleToStr(__circle__)
#define SIZE_TO_STR(__size__)		aurora::StringHelper::sizeToStr(__size__)
#define POINT_TO_STR(__point__)		aurora::StringHelper::pointToStr(__point__)
#define FLOAT_TO_STR(__value__)		aurora::StringHelper::floatToStr(__value__)
#define INT_TO_STR(__value__)		StringUtils::format("%d", __value__)
#define UNSIGNED_TO_STR(__value__)	StringUtils::format("%zd", __value__)
#define BOOL_TO_STR(__value__)		std::string(__value__ ? "true" : "false")
#define VECTOR_TO_STR(__value__)	aurora::StringHelper::vectorToString(__value__)


#define STR_TO_INT(__value__)		aurora::StringHelper::parseInt(__value__)
#define STR_TO_FLOAT(__value__)		aurora::StringHelper::parseFloat(__value__)


#endif /* StringHelper_hpp */
