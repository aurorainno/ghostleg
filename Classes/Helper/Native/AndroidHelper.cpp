//
//  AndroidHelper.cpp
//  SDKTest
//
//  Created by Ken Lee on 17/8/15.
//
//

#include "AndroidHelper.h"

#include "cocos2d.h"

USING_NS_CC;

#pragma mark - Android
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)


#include "platform/android/jni/JniHelper.h"
#include <jni.h>


void AndroidHelper::callVoidStaticMethod(const std::string &className, const std::string &method)
{
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t, className.c_str(), method.c_str(), "()V"))
	{
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID);
		t.env->DeleteLocalRef(t.classID);
	}
}

void AndroidHelper::callVoidStaticMethod(const std::string &className, const std::string &method,
										 const std::string &param)
{
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t,
								className.c_str(), method.c_str(), "(Ljava/lang/String;)V"))
	{
		jstring stringArg1 = t.env->NewStringUTF(param.c_str());
		
		t.env->CallStaticVoidMethod(t.classID, t.methodID, stringArg1);
		t.env->DeleteLocalRef(t.classID);
	}
}



//public boolean isNetworkOnline()
//{
//    boolean status=false;
//    try{
//        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo netInfo = cm.getNetworkInfo(0);
//        if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
//            status= true;
//        }else {
//            netInfo = cm.getNetworkInfo(1);
//            if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
//                status= true;
//        }
//    }catch(Exception e){
//        e.printStackTrace();
//        return false;
//    }
//    return status;
//    
//}



// http://stackoverflow.com/questions/14036004/how-to-convert-jobject-to-jstring
// See: android/jni/Java_org_cocos2dx_lib_Cocos2dxHelper.cpp
std::string AndroidHelper::getStaticVoidMethodResult(const std::string &className, const std::string &method)
{
	cocos2d::JniMethodInfo t;
	if (cocos2d::JniHelper::getStaticMethodInfo(t,
							className.c_str(), method.c_str(), "()Ljava/lang/String;"))
	{
		
		
		jstring str = (jstring) t.env->CallStaticObjectMethod(t.classID, t.methodID);
		
		t.env->DeleteLocalRef(t.classID);

		std::string ret = "";
		ret = JniHelper::jstring2string(str);
		t.env->DeleteLocalRef(str);
		
		return ret;
	}  else {
		log("getStaticMethodInfo: fail to get the method %s", method.c_str());
		return "";
	}
}


#else

void AndroidHelper::callVoidStaticMethod(const std::string &className, const std::string &method)
{
	log("AndroidHelper::callVoidMethod: not supporting");
}
void AndroidHelper::callVoidStaticMethod(const std::string &className, const std::string &method,
										 const std::string &param)
{
	log("AndroidHelper::callVoidMethod: not supporting");
}


std::string AndroidHelper::getStaticVoidMethodResult(const std::string &className, const std::string &method)
{
	log("AndroidHelper::getStaticVoidMethodResult: not supporting");
	return "not support";
}
/*
std::string AndroidHelper::getStringResult(const char *input)
{
	return "";
}
*/

#endif
