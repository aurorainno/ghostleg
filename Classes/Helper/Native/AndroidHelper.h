//
//  AndroidHelper.h
//  SDKTest
//
//  Created by Ken Lee on 17/8/15.
//
//

#ifndef __SDKTest__AndroidHelper__
#define __SDKTest__AndroidHelper__

#include <stdio.h>
#include <string>

class AndroidHelper
{
public:
	static void callVoidStaticMethod(const std::string &className, const std::string &method);
	static void callVoidStaticMethod(const std::string &className, const std::string &method,
									 const std::string &param);
	static std::string getStaticVoidMethodResult(const std::string &className, const std::string &method);
	
	
	
};

#endif /* defined(__SDKTest__AndroidHelper__) */
