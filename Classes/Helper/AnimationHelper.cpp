//
//  AnimationHelper.cpp
//  GhostLeg
//
//  Created by Ken Lee on 17/6/2016.
//
//

#include "AnimationHelper.h"
#include "PopTextAction.h"


#define kStarRollingFinishTime 0.8
#define kStarRollingRefreshRate 0.05

void AnimationHelper::setJellyEffect(Node *node, float cycleDuration, float scale, int repeatCount)
{
	if(node == nullptr) {
		return;
	}
	
	float originScale = node->getScaleX();
	
	ScaleBy *scaleUp = ScaleBy::create(cycleDuration/2, scale);
	ScaleBy *scaleDown = scaleUp->reverse();
	
	
	Sequence *sequence = Sequence::create(scaleUp, scaleDown, nullptr);
	
	
	Action *action;
	if(repeatCount <= 0) {
		action = RepeatForever::create(sequence);
	} else {
		action = Repeat::create(sequence, repeatCount);
	}
								
	
	node->runAction(action);
}

void AnimationHelper::setMoveUpEffect(cocos2d::Node *node, float cycleDuration, float offsetY, bool moveUpFirst, int repeatCount)
{
    if(node == nullptr) {
        return;
    }
    
    float deltaPosY = moveUpFirst ? fabsf(offsetY) : -fabsf(offsetY);
    
    MoveBy *moveBy = MoveBy::create(cycleDuration, Vec2(0,deltaPosY));
    MoveBy *moveBack = moveBy->reverse();
	
	ActionInterval *anime1 = EaseOut::create(moveBy, 1);
	ActionInterval *anime2 = EaseIn::create(moveBack, 1);
	
    Sequence *sequence = Sequence::create(anime1, anime2, nullptr);
    
    Action *action;
    if(repeatCount <= 0) {
        action = RepeatForever::create(sequence);
    } else {
        action = Repeat::create(sequence, repeatCount);
    }
    
    node->runAction(action);
}

void AnimationHelper::setBlinkEffect(Node *node, float cycleDuration, int repeatCount)
{
	if(node == nullptr) {
		return;
	}
	
	Action *action = Blink::create(cycleDuration, repeatCount);
	
	node->runAction(action);
}


void AnimationHelper::setAlphaBlink(Node *node, float duration, int repeatCount,
									int opacity1, int opacity2,
									const std::function<void ()> &func)
{
	if(node == nullptr) {
		return;
	}
	
	float cycleDuration = duration / 2;
	if(repeatCount > 1) {
		cycleDuration = cycleDuration / repeatCount;
	}
	// log("cycleDuration=%f", cycleDuration);
	
	FadeTo *fade1 = FadeTo::create(cycleDuration, opacity1);
	FadeTo *fade2 = FadeTo::create(cycleDuration, opacity2);

	
	Sequence *blinkSequence = Sequence::create(fade1, fade2, nullptr);
	ActionInterval *blinkAction;
	if(repeatCount <= 1) {
		blinkAction = blinkSequence;		// No repeat!
	} else {
		blinkAction = Repeat::create(blinkSequence, repeatCount);	// 4x
	}
	
	Action *finalAction;
	if(repeatCount > 0) {
		FadeTo *fullOpacity = FadeTo::create(0, 255);
		CallFunc *callback = func == nullptr ? nullptr : CallFunc::create(func);
		finalAction = Sequence::create(blinkAction, fullOpacity, callback, nullptr);
	} else {
		finalAction = RepeatForever::create(blinkAction);	// blickAction;
	}
	
	node->runAction(finalAction);
}

void AnimationHelper::setForeverBlink(Node *node, float duration, int opacity)
{
	if(node == nullptr) {
		return;
	}
	
	float cycleDuration = duration / 2;
	FadeTo *fade = FadeTo::create(cycleDuration, opacity);
	Action *unFade = FadeTo::create(cycleDuration, node->getOpacity());
	
	
	Sequence *blinkAction = Sequence::create(fade, unFade, nullptr);
	
	Action *finalAction = RepeatForever::create(blinkAction);	// blickAction;
	
	node->runAction(finalAction);
}

void AnimationHelper::popTextEffect(Node* node,cocos2d::ui::Text *text, int originValue, int finalValue,
									std::function<void()> callback,
                                    float finishTime, float targetScale, std::string displayFormat, GameSound::Sound soundEffect)
{
//    CallFunc* function = CallFunc::create([=](){
//        if(callback){
//            callback();
//        }
//    });
    PopTextAction* action = PopTextAction::create(text, originValue, finalValue, callback, finishTime, targetScale, displayFormat, soundEffect);
//    Sequence* sequence = Sequence::create(action, function,NULL);
    node->runAction(action);
}


// http://cubic-bezier.com/#.17,.67,.83,.67

ActionInterval *AnimationHelper::getCurveAction(const Vec2 &fromPos,
								const Vec2 &toPos, float duration, int style)
{
	ActionInterval *action;
	
	ccBezierConfig c;
	c.endPosition = toPos;
	
	switch(style) {
		case 1: {		//
			c.controlPoint_1 = Vec2(fromPos.x - 100, fromPos.y);		// Vec2(fromPos.x, toPos.y);
			c.controlPoint_2 = Vec2(toPos.x - 100, toPos.y);
			break;
		}
			
		default: {
			c.controlPoint_1 = Vec2(toPos.x, fromPos.y);		// Vec2(fromPos.x, toPos.y);
			c.controlPoint_2 = toPos;
			break;
		}
	}
	
	
	
	action = BezierTo::create(duration, c);
	
	return action;
}

ActionInterval *AnimationHelper::getScaleUpAction(const float &scale, const float &scaleDuration,
												  const float &fadeDuration)
{
	Sequence *seq = Sequence::create(
									 EaseOut::create(ScaleBy::create(scaleDuration, scale), 5.0f),
									 EaseIn::create(FadeOut::create(fadeDuration), 2.0f),
									 nullptr);
	
	return seq;
}

RepeatForever *AnimationHelper::getRotateForever(const float &cycleDuration, bool isClockwise)
{
	int sign = isClockwise ? 1 : -1;
	RotateBy *rotate = RotateBy::create(cycleDuration, 360 * sign);

	RepeatForever *action = RepeatForever::create(rotate);
	
	return action;
}


ActionInterval *AnimationHelper::getPopupAction(const float &initialScale,
												const float &finalScale,
												const float &duration,
												const std::function<void ()> &callback)
{
//	ScaleTo *setScaleAction = ScaleTo::create(0, initialScale);
	ScaleTo *scaleToAction = ScaleTo::create(duration, finalScale);
	EaseBackOut *easeScaleToAction = EaseBackOut::create(scaleToAction);
	CallFunc *callbackAction = callback == nullptr ? nullptr : CallFunc::create(callback);
	
	Sequence *seq = Sequence::create(easeScaleToAction, callbackAction, nullptr);
	
	return seq;
}



void AnimationHelper::runPopupAction(Node *targetNode,
						   const float &initialScale,
                           const float &finalScale,
						   const float &duration,
						   const std::function<void ()> &callback)
{
	
	if(targetNode == nullptr) {
		log("runPopupAction: targetNode is nullptr");
		return;
	}
    
    float targetScale = finalScale == -1 ? targetNode->getScaleX() : finalScale;
    
    targetNode->setScale(initialScale);
	 
	Action *action = AnimationHelper::getPopupAction(initialScale,
													 targetScale,
													 duration,
													 callback);

	targetNode->runAction(action);
}

void AnimationHelper::runPopupAndPopbackAction(cocos2d::Node *targetNode, const float &initialScale, const float &finalScale,
                                               float duration, const std::function<void ()> &callback)
{
    if(targetNode == nullptr) {
        log("runPopupAndPopbackAction: targetNode is nullptr");
        return;
    }
    
    float scaleFactor = finalScale / initialScale;
    duration = duration/2;
    
    ScaleBy* scaleBy = ScaleBy::create(duration, scaleFactor);
    
    CallFunc* callFunc = CallFunc::create([=](){
        if(callback){
            callback();
        }
    });
    
    Sequence* sequence = Sequence::create(scaleBy, scaleBy->reverse(),callFunc,NULL);
    targetNode->runAction(sequence);
    
}



ActionInterval *AnimationHelper::getClosingAction(const float &duration,
												  bool removeOnClose,
												  const std::function<void ()> &callback)
{
	ScaleTo *scaleDownAction = ScaleTo::create(duration, 0.01);	// zoom to zero
	
	EaseQuadraticActionIn *easeScaleDownAction = EaseQuadraticActionIn::create(scaleDownAction);
	
	CallFunc *callbackAction = callback == nullptr ? nullptr : CallFunc::create(callback);
	
	//RemoveSelf
	Vector<FiniteTimeAction *> actionList;
	
	actionList.pushBack(easeScaleDownAction);
	if(removeOnClose) {
		actionList.pushBack(RemoveSelf::create());
	}
	if(callback) {
		actionList.pushBack(callbackAction);
	}
	
	return Sequence::create(actionList);
}


void AnimationHelper::runClosingAction(Node *targetNode,
									   const float &duration,
									   bool removeOnClose,
									   const std::function<void ()> &callback)
{
	if(targetNode == nullptr) {
		log("runPopupAction: targetNode is nullptr");
		return;
	}
	
	Action *action = AnimationHelper::getClosingAction(duration,
													   removeOnClose,
													   callback);
	
	targetNode->runAction(action);
}


void AnimationHelper::runRotateForeverAction(Node *targetNode,
								   const float &duration, bool isClockwise)
{
	if(targetNode == nullptr) {
		return;
	}
	
	Action *rotate = getRotateForever(duration, isClockwise);
	
	targetNode->runAction(rotate);
}


void AnimationHelper::addAuraRotateAction(Node *parentNode, const float &duration,
										  int numAura,
										  const std::string &nodePrefix)
{
	if(parentNode == nullptr) {
		return;
	}
	
	for(int i=1; i<=numAura; i++) {
		std::string name = StringUtils::format(nodePrefix.c_str(), i);
		Node *aura = parentNode->getChildByName(name);
		if(aura) {
			bool isClockwise = i % 2 == 0;
			AnimationHelper::runRotateForeverAction(aura, duration, isClockwise);
		}
	}
}

void AnimationHelper::addRotatingAura(Node *parentNode,
									  const float &duration,
									  int numAura,
									  const std::string &auraImage,
									  const Vec2 &pos)
{
	if(parentNode == nullptr) {
		return;
	}
	
	float rotation = 0;
	float rotationStep = 360.0f / numAura;
	
	// Create the sprite
	std::vector<Sprite *> spriteList;
	
	for(int i=0; i<numAura; i++) {
		Sprite *sprite = Sprite::create(auraImage);
		sprite->setRotation(rotation);
		sprite->setPosition(pos);
		parentNode->addChild(sprite);
		
		rotation += rotationStep;
		
		spriteList.push_back(sprite);
	}
	
	bool isClockwise = true;
	for(Sprite *sprite : spriteList) {
		isClockwise = !isClockwise;
		AnimationHelper::runRotateForeverAction(sprite, duration, isClockwise);
	}
	
}



void AnimationHelper::setToastEffect(cocos2d::Node *targetNode,
									 const float &finalScale,
									 float delay,
									 float showDuration,
									 float popDuration,
									 const std::function<void ()> &callback)
{
	if(targetNode == nullptr) {
		log("runPopupAndPopbackAction: targetNode is nullptr");
		return;
	}
	
	
	
	Vector<FiniteTimeAction *> actionList;
	
	targetNode->setVisible(true);
	targetNode->setScale(0.001f);
	
	// Delay
	if(delay > 0) {
		DelayTime *delayAction = DelayTime::create(delay);
		actionList.pushBack(delayAction);
	}
	
	//
	ScaleTo *popUp = ScaleTo::create(popDuration, finalScale);
	actionList.pushBack(popUp);
	
	if(showDuration > 0) {
		DelayTime *delayAction = DelayTime::create(showDuration);
		actionList.pushBack(delayAction);
	}
	
	
	ScaleTo *closeDown = ScaleTo::create(popDuration, 0.001f);
	actionList.pushBack(closeDown);
	
	CallFunc* callFunc = CallFunc::create([=](){
		if(callback){
			callback();
		}
	});
	actionList.pushBack(callFunc);
	
	Sequence *sequence = Sequence::create(actionList);
	targetNode->runAction(sequence);
	
}


ActionInterval *AnimationHelper::createMoveToAction(float duration, const Vec2 &finalPos)
{
	MoveTo *move = MoveTo::create(duration, finalPos);

	return move;
	
//	 *moveBy = MoveBy::create(cycleDuration, Vec2(0,deltaPosY));
}

ActionInterval *AnimationHelper::createEaseAction(ActionInterval *originAction, const std::string &easeName)
{
	if("easeInBack" == easeName) {
		return EaseBackIn::create(originAction);
	} else {
		return originAction;
	}
	
}
