//
//  EventPopUpLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 15/12/2016.
//
//

#include "EventPopUpLayer.h"
#include "CommonMacro.h"
#include "AnimationHelper.h"
#include "GameManager.h"

bool EventPopUpLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    
    Node *rootNode = CSLoader::createNode("EventPopUpLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}

void EventPopUpLayer::setupUI(cocos2d::Node *node)
{
    FIX_UI_LAYOUT(node);
    mMainPanel = node->getChildByName("mainPanel");
    
    mOKBtn = mMainPanel->getChildByName<Button*>("confirmBtn");
    mOKBtn->addClickEventListener([&](Ref*){
        if(mCallback){
            mCallback(this);
        }
    });
}

void EventPopUpLayer::onEnter()
{
    Layer::onEnter();
    AnimationHelper::runPopupAction(mMainPanel, 0.1,1.0, 0.5f);
    GameManager::instance()->setEventPopUpShown(true);
}

void EventPopUpLayer::setCallback(std::function<void (Node *)> callback)
{
    mCallback = callback;
}


