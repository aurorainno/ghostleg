//
//  LeaderboardItemView.hpp
//  GhostLeg
//
//  Created by Calvin on 13/1/2017.
//
//

#ifndef LeaderboardItemView_hpp
#define LeaderboardItemView_hpp

#include <stdio.h>
#include "AstrodogData.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class FBProfilePic;

class LeaderboardItemView : public ui::Layout
{
public:
    CREATE_FUNC(LeaderboardItemView);
    
    LeaderboardItemView();
    
    virtual bool init()override;
    virtual void resetContentSize(const cocos2d::Size &contentSize);
    
    //return true if this item is player record (not other players)
    bool setItemView(AstrodogRecord *playerData);
    
    void setRankStatus(AstrodogRankStatus status);
    
private:
    void setupUI();
    Node* mRootNode;
    Sprite* mCountrySprite;
    Sprite* mProfilePicFrame;
    Sprite *mRankUpSprite;
    Sprite *mRankSprite;
    FBProfilePic* mProfilePic;
    Text* mRankText;
    Text* mScoreText;
    Text* mNameText;
    ui::ImageView *mBgNormal, *mBgPlayer;
    
};



#endif /* LeaderboardItemView_hpp */
