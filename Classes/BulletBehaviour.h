//
//  BulletBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 6/3/2017.
//
//

#ifndef BulletBehaviour_hpp
#define BulletBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

class BulletBehaviour : public EnemyBehaviour
{
public:
    CREATE_FUNC(BulletBehaviour);
    
    BulletBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onCreate();
    virtual void onVisible();
    virtual void onActive();
    virtual void onUpdate(float delta);
    
    virtual bool onCollidePlayer(Player *player);
    void setNewSpeed(Vec2 oriPos, Vec2 targetPos);
    
    void updateProperty();
    
private:
    bool mIsExploded;
    float mMagnitude;
    Vec2 mAcceleration;
    
};


#endif /* BulletBehaviour_hpp */
