//
//  SpeedUpEffect.cpp
//  GhostLeg
//
//  Created by Calvin on 8/12/2016.
//
//

#include "SpeedUpEffect.h"
#include "GameSound.h"
#include "GameWorld.h"
#include "ModelLayer.h"
#include "Player.h"
#include "EffectLayer.h"

#define kSpeedUpCsbname "SpeedUp.csb"
#define kSpeedUpAnimeName "active"

SpeedUpEffect::SpeedUpEffect()
: ItemEffect(Type::ItemEffectSpeedUp)
, mLayer(nullptr)
, mStart(false)
, mVelocity(100)
, mBonusVelocity(0)
{
    // TODO
}


Node *SpeedUpEffect::onCreateUI()
{
    
    return nullptr;
}

void SpeedUpEffect::onAttach()
{
	
	
    mRemain = mMaxDuration;
    ModelLayer *modelLayer = GameWorld::instance()->getModelLayer();
    mPlayer = modelLayer->getPlayer();
	
}

void SpeedUpEffect::onDetach()
{
    // GameWorld::instance()->getModelLayer()->removeEffectStatusFromAllEnemy();
    // EnemyFactory::instance()->resetDefaultEffectStatus();
    if(mPlayer) {
//        mPlayer->removeCustomIndicator();
//        mPlayer->removeEffectStatus();
//        mPlayer->removeVisualEffect();
		mPlayer->removeCsbAnimeByName(kSpeedUpCsbname);
		mPlayer->cancelSpeedUp();
    }
	
	
	//GameWorld::instance()->getModelLayer()->setMotionEnable(false);
    setLayer(nullptr);
}

void SpeedUpEffect::onUpdate(float delta)
{
    if(mStart){
        mRemain -= delta;
        updateUI();
		
		
		Dir dir = mPlayer->getDir();
		
		
//        if(dir == Dir::DirUp || dir == Dir::DirDown){
//            mPlayer->adjustAnimeNode(kSpeedUpCsbname, 0, Vec2(0,-150));
//        }else if(dir == Dir::DirRight){
//            mPlayer->adjustAnimeNode(kSpeedUpCsbname, 60, Vec2(-80,-100));
//        }else if(dir == Dir::DirLeft){
//            mPlayer->adjustAnimeNode(kSpeedUpCsbname, -60, Vec2(80,-100));
//        }
    }
}

void SpeedUpEffect::setProperty(const std::string &name, float value)
{
	if("bonus" == name) {
		mBonusVelocity = value;
		if(mBonusVelocity > 0) {
			mVelocity += mVelocity * mBonusVelocity / 100;	// mBonusVelocity is a percentage
		}
	}
}

void SpeedUpEffect::setMainProperty(float value)
{
    mMaxDuration = value;
}

bool SpeedUpEffect::shouldDetach()
{
    return mRemain <= 0;
}


void SpeedUpEffect::updateUI()
{
    if(mLayer) {
//        mLayer->setVisible(true);
        mLayer->setValue(mRemain);
    }
}

void SpeedUpEffect::activate()
{
    mStart = true;
    
//    GameSound::playSound(GameSound::GrabStar);
	GameWorld::instance()->getEffectLayer()->showWindEffect();
	
    GameSound::playSound(GameSound::SpeedUp);
	//GameWorld::instance()->getModelLayer()->setMotionEnable(true);
	//GameWorld::instance()->getModelLayer()->changeMotionStyle(ModelLayer::MotionStyle::StyleSpeedUp);
	
    // Timer indicator
//    ItemEffectUILayer *layer = ItemEffectUILayer::create();
//	layer->setUseSpriteFrame(false);
//    layer->configAsTimer("icon_speedup.png", "## sec", mMaxDuration);
//    
//    setLayer(layer);
//    mLayer->setVisible(true);
	
    if(mPlayer) {
//        mPlayer->removeEffectStatus();
//        mPlayer->setEffectStatus(EffectStatusSpeedUp);
        mPlayer->activateSpeedUp(mVelocity);
		
		mPlayer->addCsbAnimeByName(kSpeedUpCsbname, false, 1.0f, Vec2(0, -20));
		mPlayer->playCsbAnime(kSpeedUpCsbname, kSpeedUpAnimeName, false);
        //mPlayer->addCustomIndicator(mLayer, Vec2(0, 80), 2.0,false);
    }
    
    GameWorld::instance()->getEffectLayer()->addPowerUpIndicator(EffectLayer::SpeedUp, mMaxDuration);
    

    GameWorld::instance()->disableCapsuleButton();
    
    updateUI();
}


std::string SpeedUpEffect::toString()
{
	return StringUtils::format("SpeedUp: duration=%f velocity=%f", mMaxDuration, mVelocity);
}


float SpeedUpEffect::getRemainTime()
{
	return mRemain;
}
