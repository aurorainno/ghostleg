//
//  NPCOrderSelectDialog.cpp
//  GhostLeg
//
//  Created by Calvin on 14/3/2017.
//
//

#include "NPCOrderSelectDialog.h"
#include "NPCOrderItemView.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "NPCOrder.h"

#define CALL_LISTENER(_method_)	\
if(mListener != nullptr) {	\
mListener->_method_;	\
}

NPCOrderSelectDialog::NPCOrderSelectDialog():
mListener(nullptr)
{}

bool NPCOrderSelectDialog::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("NPCOrderSelectDialog.csb");
    setupAnimation("NPCOrderSelectDialog.csb");
    setupUI(rootNode);
    addChild(rootNode);

    //
    //    setVisible(false);
    return true;
}

void NPCOrderSelectDialog::onEnter()
{
    Layer::onEnter();

//    this->scheduleUpdate();

    runAnimation("popUp");
}

void NPCOrderSelectDialog::onExit()
{
    Layer::onExit();
    CALL_LISTENER(onDialogClosed());
}

void NPCOrderSelectDialog::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    Node* contentNode = rootNode->getChildByName("contentPanel");
    
    mChangeBtn = (Button*) contentNode->getChildByName("changeBtn");
    mFirstNPCPanel =  contentNode->getChildByName("firstNPCPanel");
    mSecondNPCPanel = contentNode->getChildByName("secondNPCPanel");
    
    mChangeBtn->addClickEventListener([=](Ref*){
        CALL_LISTENER(onRefreshNPC());
    });
    
}

void NPCOrderSelectDialog::setItemView(Vector<NPCOrder*> orderList)
{
    NPCOrderItemView* firstView = NPCOrderItemView::create();
	
    firstView->setNPCOrder(orderList.at(0));
    firstView->setCallback([=](NPCOrder* order){
        CALL_LISTENER(onNPCOrderSelected(order));
    });
    
    NPCOrderItemView* secondView = NPCOrderItemView::create();
    secondView->setNPCOrder(orderList.at(1));
    secondView->setCallback([=](NPCOrder* order){
        CALL_LISTENER(onNPCOrderSelected(order));
    });
    
    
    mFirstNPCPanel->removeAllChildren();
    mSecondNPCPanel->removeAllChildren();
    
    
    if(orderList.at(0)){
        Size contentSize = mFirstNPCPanel->getContentSize();
        firstView->setPosition(Vec2(contentSize.width/2,contentSize.height/2));
        mFirstNPCPanel->addChild(firstView);
    }
    
    if(orderList.at(1)){
        Size contentSize = mSecondNPCPanel->getContentSize();
        secondView->setPosition(Vec2(contentSize.width/2,contentSize.height/2));
        mSecondNPCPanel->addChild(secondView);
    }
}


void NPCOrderSelectDialog::setListener(NPCOrderSelectDialogListener *listenser)
{
    mListener = listenser;
}

void NPCOrderSelectDialog::setupAnimation(const std::string &csbName)
{
    cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csbName);
    runAction(timeline);		// retain is done here!
    
    mTimeline = timeline;
}

void NPCOrderSelectDialog::runAnimation(const std::string &name)
{
    if(mTimeline) {
        AnimationClip info = mTimeline->getAnimationInfo(name);
        log("animeInfo:%s %d,%d", info.name.c_str(), info.startIndex, info.endIndex);
        mTimeline->start();
        mTimeline->play(name, false);
    }
}

