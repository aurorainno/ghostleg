//
//  MainSceneInfoLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 8/12/2016.
//
//

#ifndef MainSceneInfoLayer_hpp
#define MainSceneInfoLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class MainSceneInfoLayer : public Layer
{
public:
    enum InfoType{
        EventInfo,
        LockedInfo,
        UnlockInfo
    };
    
    CREATE_FUNC(MainSceneInfoLayer);
    
    MainSceneInfoLayer();
    virtual bool init() override;
    
    void setup(int planetID);
    
    void setupUI(InfoType type);
    
    void setCallback(std::function<void(int)> callback);

    
private:
    void setupLockedInfo(Node* rootNode);
    void setupUnlockInfo(Node* rootNode);
    void setupEventInfo(Node* rootNode);
    
    std::string getCsbNameByType(InfoType type);
    InfoType mInfoType;
    Node* mRootNode;
    int mPlanetID;
    
    std::function<void(int planetID)> mCallBack;
    
};

#endif /* MainSceneInfoLayer_hpp */
