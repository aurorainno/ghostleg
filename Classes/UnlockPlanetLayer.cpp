//
//  UnlockPlanetLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 2/11/2016.
//
//

#include "UnlockPlanetLayer.h"
#include "CommonMacro.h"
#include "MainScene.h"
#include "AnimationHelper.h"
#include "FireworkLayer.h"

namespace {
    void setScaleTo(Node *node, float initScale, float finalScale, float duration)
    {
        node->setScale(initScale);
        ScaleTo * scaleto = ScaleTo::create(duration, finalScale);
        node->runAction(scaleto);
    }
}


bool UnlockPlanetLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
	mParentLayer = nullptr;
	
	std::string csb = "UnlockPlanetLayer.csb";
	
	setupGUI(csb);
	setupAnimation(csb);
    //setScaleTo(mMainPanel,0.1,1.0,0.2);
    
    return true;
}

void UnlockPlanetLayer::runAnimation(const std::string &name)
{
	if(mTimeline) {
		mTimeline->play(name, false);
	}
}

void UnlockPlanetLayer::setupAnimation(const std::string &csb)
{
	cocostudio::timeline::ActionTimeline *timeline = CSLoader::createTimeline(csb);
	runAction(timeline);		// retain is done here!
	
	mTimeline = timeline;
}

void UnlockPlanetLayer::setupGUI(const std::string &csb)
//void UnlockPlanetLayer::setupUI(cocos2d::Node *node)
{
	Node* rootNode = CSLoader::createNode(csb);
	addChild(rootNode);

	
    FIX_UI_LAYOUT(rootNode);
    
    Node *bgPanel = rootNode->getChildByName("bgPanel");
	Node *mainPanel = bgPanel->getChildByName("mainPanel");
	
    mMainPanel = mainPanel;
	
	
	// Planet CSB Node
    mPlanetNodeParent = mainPanel->getChildByName("planetNode");
	
	// Planet Name Sprite
	mNameSprite = (Sprite*)mainPanel->getChildByName("planetNameSprite");
	
	//
    mOKBtn = mainPanel->getChildByName<Button*>("okBtn");
    mOKBtn->addClickEventListener([&](Ref*){
        mPlanetList.erase(mPlanetList.begin());
		
		if(mParentLayer == nullptr && getParent() == nullptr) {
			log("UnlockPlanetLayer: parentLayer is nullptr");
            return;
        }else if(mParentLayer==nullptr){
            mParentLayer = getParent();
        }
		
		
        if(mPlanetList.size() > 0) {		// Show the new one
            UnlockPlanetLayer* layer = UnlockPlanetLayer::create();
			
			layer->setPlanetList(mPlanetList);
            layer->setParentLayer(mParentLayer);
			layer->setCloseCallback(mCloseCallback);
			
			mParentLayer->addChild(layer);
			
        }else{
            if(mCloseCallback){
                mCloseCallback(mPlanetID);
            }
        }
		
        this->removeFromParent();
    });
	
	
	// VFX
	attachFireworkEffect(mainPanel);
	
}

void UnlockPlanetLayer::onEnter()
{
	Layer::onEnter();
	
	runAnimation("startUp");
}

void UnlockPlanetLayer::setCloseCallback(const std::function<void (int)> &callback)
{
    mCloseCallback = callback;
}

void UnlockPlanetLayer::setPlanetList(std::vector<int> planetList)
{
    mPlanetList = planetList;
    mPlanetID = mPlanetList.front();
	
	log("debug: showing unlock planet=%d", mPlanetID);
	
    updatePlanetVisual();
}

void UnlockPlanetLayer::updatePlanetVisual()
{
    
    // std::string imageFile = prefix + (mIsLocked ? "_locked" : "") + ".png";
    // the file of the planet name
    std::string nameFile = StringUtils::format("planet/planet_%03d_name.png", mPlanetID);
    std::string csbFile = StringUtils::format("planetGUI/planet_gui_%03d.csb", mPlanetID);
    
    
    //	// update the image
    //	if(mPlanetSprite) {
    //		mPlanetSprite->setTexture(imageFile);
    //	}
    //
    setPlanetCsb(csbFile, false);
    
    if(mNameSprite) {
        mNameSprite->setTexture(nameFile);
    }
}

void UnlockPlanetLayer::setPlanetCsb(const std::string &csbName, bool isLocked)
{
    if(mPlanetNode) {	// Clean up the old planet node first
        mPlanetNode->removeFromParent();
        mPlanetNode = nullptr;
    }
    
    // Setup the Csb Node
    Node *newNode = CSLoader::createNode(csbName);
    if(newNode == nullptr) {
        return;
    }
    
    Size size = newNode->getContentSize();
    
    newNode->setPosition(Vec2(-size.width/2, -size.height/2));
    mPlanetNodeParent->addChild(newNode);
    
    
    mPlanetNode = newNode;
    
    
    
    // Setup Animation
    ActionTimeline *timeline = CSLoader::createTimeline(csbName);
    if(timeline == nullptr) {
        log("UnlockPlanetLayer: cannot create the timeLine: %s", csbName.c_str());
        return;
    }
    timeline->setTimeSpeed(0.3f);
    
    setPlanetAction(timeline);		// retain take place in the setter
    mPlanetNode->runAction(timeline);
    
    
    // Modify the state
    setPlanetCsbAnimationForLockState(isLocked);
}



void UnlockPlanetLayer::setPlanetCsbAnimation(const std::string &name)
{
    if(mPlanetNode == nullptr) {
        return;
    }
    
    ActionTimeline *timeline = getPlanetAction();
    if(timeline == nullptr) {
        return;
    }
    
    timeline->play(name, true);
}

void UnlockPlanetLayer::setPlanetCsbAnimationForLockState(bool isLocked)
{
    std::string name = isLocked ? "locked" : "unlock";
    
    setPlanetCsbAnimation(name);
}

void UnlockPlanetLayer::attachFireworkEffect(Node *mainNode)
{
	//
	AnimationHelper::addAuraRotateAction(mainNode, 40, 2);
	
	
	// Add the firework
	Node *attachNode = mainNode->getChildByName("fireworkNode");
	
	if(attachNode->isVisible()) {
		FireworkLayer *firework = FireworkLayer::create();
		firework->setAnchorPoint(Vec2(0.5, 0.5));
		attachNode->addChild(firework);
		firework->startFirework();
	}
	
}
