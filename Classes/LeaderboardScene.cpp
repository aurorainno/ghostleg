//
//  LeaderboardLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 19/1/2017.
//
//

#include "LeaderboardScene.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "FBProfilePic.h"
#include "PlanetManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "LeaderboardItemView.h"
#include "StageParallaxLayer.h"
#include "TimeHelper.h"
#include "LeaderboardRewardLayer.h"
#include "Analytics.h"

#include "PluginFacebook/PluginFacebook.h"

#define kListItemSpacing 5

LeaderboardLayer::LeaderboardLayer():
mCurrentLBType(AstrodogLeaderboardType::AstrodogLeaderboardCountry),
mLeaderboardEndTime(-1),
mAccumTime(0),
mIsStartCounting(false),
mMode(LayerMode)
{}

Scene* LeaderboardLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    LeaderboardLayer *layer = LeaderboardLayer::create();
    layer->mMode = SceneMode;
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool LeaderboardLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    StageParallaxLayer *parallaxLayer = StageParallaxLayer::create();
    parallaxLayer->setBGSpeed(0);
    parallaxLayer->setSpaceObjectID(0);
    addChild(parallaxLayer);
    parallaxLayer->scheduleUpdate();

    mRootNode = CSLoader::createNode("LeaderboardLayer.csb");
    addChild(mRootNode);
    setupUI();
    scheduleUpdate();
    
    return true;
}

void LeaderboardLayer::update(float delta)
{
    if(mAccumTime>0){
        mAccumTime -= delta;
        return;
    }
    
    if(mIsStartCounting){
        mCounterPanel->setVisible(true);
        updateLeaderboardRemainTime();
        mAccumTime = 10;
    }
}


void LeaderboardLayer::setupUI()
{
    FIX_UI_LAYOUT(mRootNode);
    
    mStarText = (Text*) mRootNode->getChildByName("totalCoinText");
    mDiamondText = mRootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mFbConnectBtn = (Button*) mRootNode->getChildByName("fbBtn");
    mFbName = (Text *) mRootNode->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) mRootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
    mCounterPanel = mRootNode->getChildByName("counterPanel");
    mSecondValue = (Text*) mCounterPanel->getChildByName("secondValue");
    mFirstValue = (Text*) mCounterPanel->getChildByName("firstValue");
    mFirstUnit = (Text*) mCounterPanel->getChildByName("firstUnit");
    
    Node* leaderboardPanel = mRootNode->getChildByName("leaderboardPanel");
    
    mCancelBtn = (Button*) mRootNode->getChildByName("cancelBtn");
    mCancelBtn->addClickEventListener([&](Ref*){
        if(mMode == SceneMode){
            Director::getInstance()->popScene();
        }else{
            if(mCloseCallback){
                mCloseCallback();
            }
            this->removeFromParent();
        }
    });

    
    mFriendBtn = (Button*) leaderboardPanel->getChildByName("friendBtn");
    mNationalBtn = (Button*) leaderboardPanel->getChildByName("nationalBtn");
    mGlobalBtn = (Button*) leaderboardPanel->getChildByName("globalBtn");
    
    mFriendBtn->addClickEventListener([&](Ref*){
        loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardFriend);
    });
    mNationalBtn->addClickEventListener([&](Ref*){
        loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardCountry);
    });
    mGlobalBtn->addClickEventListener([&](Ref*){
        loadLeaderboard(AstrodogLeaderboardType::AstrodogLeaderboardGlobal);
    });
    
    mFriendBorder = leaderboardPanel->getChildByName("friendUpperBorder");
    mNationalBorder = leaderboardPanel->getChildByName("nationalUpperBorder");
    mGlobalBorder = leaderboardPanel->getChildByName("globalUpperBorder");
    
    mLoadingText = leaderboardPanel->getChildByName<Text*>("loadingText");
    mListView = leaderboardPanel->getChildByName<ListView*>("leaderboardListView");
    
    for(int i=0;i<=5;i++){
        std::string btnName = StringUtils::format("planet_%02dBtn",i);
        Button* planetBtn = leaderboardPanel->getChildByName<Button*>(btnName);
        if(!planetBtn){
            log("cannot get Button:%s!",btnName.c_str());
            break;
        }
        planetBtn->addClickEventListener([&](Ref* sender){
            Button* thisBtn = dynamic_cast<Button*>(sender);
            if(!thisBtn){
                return;
            }
            
            std::string btnName = thisBtn->getName();
            int planetID = 1;
            sscanf(btnName.c_str(), "planet_%02dBtn", &planetID);
            this->onPlanetSelected(planetID);
        });
    }
    
    Node* bannerPanel = leaderboardPanel->getChildByName("bannerPanel");
    mBannerBg = bannerPanel->getChildByName<Sprite*>("bannerSprite");
    mBannerBtn = bannerPanel->getChildByName<Button*>("bannerBtn");
    mBannerFbBtn = bannerPanel->getChildByName<Button*>("bannerFbBtn");
    mBannerText = bannerPanel->getChildByName<Text*>("bannerDescription");
    mBannerDiamondSprite = bannerPanel->getChildByName<Sprite*>("diamondSprite");
    
    mBannerFbBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });
    
    mFbConnectBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });
    
}

void LeaderboardLayer::onEnter()
{
    Layer::onEnter();
    mCurrentPlanet = PlanetManager::instance()->getSelectedPlanet();
    mCurrentLBType = AstrodogLeaderboardCountry;
    
    updateDiamond();
    updateStars();
    setupBanner(mCurrentLBType);
    updatePlanetBtnGUI(mCurrentPlanet);
    loadLeaderboard(mCurrentLBType);
    
    AstrodogClient::instance()->setListener(this);
    setupFbProfile();
	
	LOG_SCREEN(Analytics::scn_ranking);
}

void LeaderboardLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void LeaderboardLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void LeaderboardLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mFbConnectBtn->setVisible(false);
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        //mFbName->setString(user->getName());
		ViewHelper::setUnicodeText(mFbName, user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
//        mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void LeaderboardLayer::updateLBListView(Vector<AstrodogRecord *> recordList)
{
    if(mListView == nullptr){
        return;
    }
    
    mLoadingText->setVisible(false);
    mListView->removeAllItems();
    mListView->setItemsMargin(kListItemSpacing);//spacing between item
    
    int targetItemIndex = 0;
    if(recordList.empty() && mCurrentLBType == AstrodogLeaderboardFriend){
//        mEmptyFriendAlert->setVisible(true);
//        if(!AstrodogClient::instance()->isFBConnected()){
//            mFBConnectBtn->setVisible(true);
//        }
    }else{
        for(int i=0;i<recordList.size();i++){
            LeaderboardItemView *itemView = LeaderboardItemView::create();
            itemView->resetContentSize(Size(320,40));
            bool isThisPlayer = itemView->setItemView(recordList.at(i));
            if(isThisPlayer){
                setupRankStatus(recordList.at(i)->getRank(),itemView);
                targetItemIndex = i;
            }
            mListView->pushBackCustomItem(itemView);
        }
    }
    
    mListView->jumpToItem(targetItemIndex, Vec2(1,0.65), Vec2(0,1));
    
    setupBanner(mCurrentLBType);
}

void LeaderboardLayer::setupRankStatus(int newRank, LeaderboardItemView *itemView)
{
    int oldRank;
    if(mCurrentLBType == AstrodogLeaderboardCountry){
        oldRank = GameManager::instance()->getUserData()->getCurrentLocalRank(mCurrentPlanet);
    }else if(mCurrentLBType == AstrodogLeaderboardFriend){
        oldRank = GameManager::instance()->getUserData()->getCurrentFriendRank(mCurrentPlanet);
    }
    
    if(oldRank == -1){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankUp);
    }else if(newRank < oldRank){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankUp);
    }else if(newRank > oldRank){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankDown);
    }else if(newRank == oldRank){
        itemView->setRankStatus(AstrodogRankStatus::AstrodogRankNone);
    }
    
    if(mCurrentLBType == AstrodogLeaderboardCountry){
        GameManager::instance()->getUserData()->setCurrentLocalRank(mCurrentPlanet, newRank);
    }else if(mCurrentLBType == AstrodogLeaderboardFriend){
        GameManager::instance()->getUserData()->setCurrentFriendRank(mCurrentPlanet, newRank);
    }

}


void LeaderboardLayer::startLoading()
{
    mListView->removeAllItems();
    mLoadingText->setVisible(true);
}

void LeaderboardLayer::loadLeaderboard(AstrodogLeaderboardType type)
{
    mCurrentLBType = type;
    int planet = mCurrentPlanet;
    AstrodogClient::instance()->getRanking(type, planet);
    onLBTypeChangeGUIUpdate();
    startLoading();
}

void LeaderboardLayer::onLBTypeChangeGUIUpdate()
{
    updateBtnAndBorderGUI(mCurrentLBType);
 //   setupBanner(mCurrentLBType);
}

void LeaderboardLayer::updateBtnAndBorderGUI(AstrodogLeaderboardType type)
{
    bool isFriendLBSelected = type == AstrodogLeaderboardType::AstrodogLeaderboardFriend;
    bool isNationLBSelected = type == AstrodogLeaderboardType::AstrodogLeaderboardCountry;
    bool isGlobalLBSelected = type == AstrodogLeaderboardType::AstrodogLeaderboardGlobal;
    
    mFriendBorder->setVisible(isFriendLBSelected);
    
    if(isFriendLBSelected){
        mFriendBtn->loadTextures("guiImage/ui_main_rankingtab_2_on.png", "guiImage/ui_main_rankingtab_2_on.png");
    }else{
        mFriendBtn->loadTextures("guiImage/ui_main_rankingtab_2_off.png", "guiImage/ui_main_rankingtab_2_off.png");
    }
    
    mGlobalBorder->setVisible(isGlobalLBSelected);
    
    if(isGlobalLBSelected){
        mGlobalBtn->loadTextures("guiImage/ui_main_rankingtab_3_on.png", "guiImage/ui_main_rankingtab_3_on.png");
    }else{
        mGlobalBtn->loadTextures("guiImage/ui_main_rankingtab_3_off.png", "guiImage/ui_main_rankingtab_3_off.png");
    }
    
    mNationalBorder->setVisible(isNationLBSelected);
    
    if(isNationLBSelected){
        mNationalBtn->loadTextures("guiImage/ui_main_rankingtab_1_on.png", "guiImage/ui_main_rankingtab_1_on.png");
    }else{
        mNationalBtn->loadTextures("guiImage/ui_main_rankingtab_1_off.png", "guiImage/ui_main_rankingtab_1_off.png");
    }
}

void LeaderboardLayer::setupBanner(AstrodogLeaderboardType type)
{
    switch(type){
        case AstrodogLeaderboardType::AstrodogLeaderboardFriend:
        {
            if(AstrodogClient::instance()->isFBConnected()){
                mBannerBg->setTexture("guiImage/ui_ranking_frd_banner.png");
 //               mBannerDiamondSprite->setVisible(true);
                mBannerFbBtn->setVisible(false);
                mBannerBtn->setVisible(true);
                Text* btnName = mBannerBtn->getChildByName<Text*>("title");
                btnName->setString("INVITE");
                mBannerText->setString("INVITE YOUR FRIENDS to play");
				
				
				mBannerBtn->addClickEventListener([&](Ref*){
					showInviteDialog();
				});
				
            }else{
                mBannerBg->setTexture("guiImage/ui_leaderboard_rewardbanner_fb.png");
//                mBannerDiamondSprite->setVisible(true);
                mBannerFbBtn->setVisible(true);
                mBannerBtn->setVisible(false);
                mBannerText->setString("Connect to play with your friends");
            }
            
            break;
        }
			
			
			
			
        case AstrodogLeaderboardType::AstrodogLeaderboardCountry:
        {
            mBannerBg->setTexture("guiImage/ui_ranking_nation_banner.png");
            mBannerDiamondSprite->setVisible(false);
            mBannerFbBtn->setVisible(false);
            mBannerBtn->setVisible(true);
            mBannerBtn->addClickEventListener([&](Ref*){
                LeaderboardRewardLayer *layer = LeaderboardRewardLayer::create();
                layer->setLeaderboardType(LeaderboardRewardLayer::Local);
                layer->setPlanetID(mCurrentPlanet);
                addChild(layer);
            });
            
            Text* btnName = mBannerBtn->getChildByName<Text*>("title");
            btnName->setString("REWARDS");
            mBannerText->setString("CHALLENGE LOCAL PLAYERS\nAND GET REWARDS");
            break;
        }
        case AstrodogLeaderboardType::AstrodogLeaderboardGlobal:
        {
            mBannerBg->setTexture("guiImage/ui_ranking_global.png");
            mBannerDiamondSprite->setVisible(false);
            mBannerFbBtn->setVisible(false);
            mBannerBtn->setVisible(true);
            mBannerBtn->addClickEventListener([&](Ref*){
                LeaderboardRewardLayer *layer = LeaderboardRewardLayer::create();
                layer->setLeaderboardType(LeaderboardRewardLayer::Global);
                layer->setPlanetID(mCurrentPlanet);
                addChild(layer);
            });
            
            Text* btnName = mBannerBtn->getChildByName<Text*>("title");
            btnName->setString("REWARDS");
            mBannerText->setString("CHALLENGE WORLD PLAYERS\nAND GET SUPER REWARDS");
            break;
        }
    }
}

void LeaderboardLayer::updatePlanetBtnGUI(int selectedPlanet)
{
    Node* lbPanel = mRootNode->getChildByName("leaderboardPanel");
    for(int i=0;i<=5;i++){
        std::string btnName = StringUtils::format("planet_%02dBtn",i);
        Button* planetBtn = lbPanel->getChildByName<Button*>(btnName);
        std::string on_Name = StringUtils::format("guiImage/ui_ranking_planet%d_on.png",i);
        std::string off_Name = StringUtils::format("guiImage/ui_ranking_planet%d_off.png",i);
        
        if(i == selectedPlanet){
            planetBtn->loadTextures(on_Name,on_Name);
        }else{
            planetBtn->loadTextures(off_Name,off_Name);
        }
    }
}

void LeaderboardLayer::onPlanetSelected(int planetID)
{
    mCurrentPlanet = planetID;
    
    //refresh button texture
    updatePlanetBtnGUI(planetID);
    
    loadLeaderboard(mCurrentLBType);
}

void LeaderboardLayer::updateLeaderboardRecords()
{
    AstrodogLeaderboardType type = mCurrentLBType;
    
    int planet = mCurrentPlanet;
    

    
    AstrodogLeaderboard *board = AstrodogClient::instance()->getLeaderboard(type, planet);
    if(board == nullptr) {
        log("LeaderboardLayer: Fail to get the leaderboard");
        return;
    }
    
    // TODO update the GUI
    updateLBListView(board->getRecordList());
    
    if(mLeaderboardEndTime==-1){
        mLeaderboardEndTime = board->getEndTime();
        mIsStartCounting = true;
    }
    
    //
    log("Leaderboard: recordCount=%ld", board->recordList.size());
    for(AstrodogRecord *record : board->recordList) {
        log("  %s", record->toString().c_str());
    }

}

void LeaderboardLayer::updateLeaderboardRemainTime()
{
    long long remainMillis = mLeaderboardEndTime - TimeHelper::getCurrentTimeStampInMillis();
    if(remainMillis<=0){
        //TODO: reset leaderboard end time
        return;
    }
    
    std::map<std::string,int> result = TimeHelper::parseMillis(remainMillis);
    int days = result["days"], hours = result["hours"], minutes = result["minutes"];
    
    if(days>0){
        mFirstUnit->setVisible(true);
        mFirstValue->setVisible(true);
        mSecondValue->setVisible(false);
        std::string displayStr = StringUtils::format("%d",days);
        mFirstValue->setString(displayStr);
    }else{
        mFirstUnit->setVisible(false);
        mFirstValue->setVisible(false);
        mSecondValue->setVisible(true);
        std::string displayStr = StringUtils::format("%02d:%02d",hours,minutes);
        mSecondValue->setString(displayStr);
    }
}

void LeaderboardLayer::onGetRanking(int status, AstrodogLeaderboardType type, int planet,
									const std::map<std::string, int> &metaData)
{
    // stopLoading
    
    if(status != STATUS_SUCCESS) {
        return;
    }
    
    if(type != mCurrentLBType || planet != mCurrentPlanet){
        return;
    }
    //
    // updateLeaderboard(type, planet);
    updateLeaderboardRecords();
}

void LeaderboardLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
    loadLeaderboard(mCurrentLBType);
}

void LeaderboardLayer::setCloseCallback(std::function<void()> callback)
{
    mCloseCallback = callback;
}


void LeaderboardLayer::showInviteDialog()
{
	std::string appLinkUrl = "https://fb.me/1831229720537597";
	std::string previewImageUrl = "http://aurorainnoapps.com:9280/static-doc/monsterEx/fb_invite_preview.png";
	
	LOG_SCREEN(Analytics::scn_fb_invite);
	
	sdkbox::PluginFacebook::inviteFriends(appLinkUrl, previewImageUrl);
}
