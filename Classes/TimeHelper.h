//
//  TimeHelper.hpp
//  GhostLeg
//
//  Created by Calvin on 22/11/2016.
//
//

#ifndef TimeHelper_hpp
#define TimeHelper_hpp

#include <stdio.h>
#include <map>
#include "cocos2d.h"

USING_NS_CC;

using namespace std;

class TimeHelper
{
public:
    //get current days since 2016/01/01 00:00 local time
    static int getCurrentTimeStampInDays();
    
    //get current time in millisec since Jan 1 ,1970, 00:00:00
    static long long getCurrentTimeStampInMillis();
    
    //get current time in sec since May 1 ,2017, 00:00:00
    static int getCurrentTimeStampInSecSinceMay2017();
    
    //return a map containing days,hours,minutes,seconds
    //key:  "days" -> day value
    //      "hours" -> hours value
    //      "minutes" -> minutes value
    //      "seconds" -> seconds value
    static map<string,int> parseMillis(long long millis);
	
	static std::string getRemainTimeString(long long millis);
};


#endif /* TimeHelper_hpp */
