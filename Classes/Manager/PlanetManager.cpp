//
//  PlanetManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#include "PlanetManager.h"
#include "GameManager.h"
#include "PlayerRecord.h"
#include "DogManager.h"
#include "JSONHelper.h"
#include "LevelData.h"
#include "UserGameData.h"
#include "StageManager.h"
#include "Analytics.h"

static const char *KeyPlayerSelected = "planet_selected";
static const char *KeyPlayerUnlock = "planet_unlock";

const std::string kDefaultBg = "BGMain.png";

static PlanetManager *sInstance = nullptr;

static const int kDefaultPlanetID = 0;		// the one already given to player

PlanetManager *PlanetManager::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}else{
		sInstance = new PlanetManager();
		return sInstance;
	}
}

PlanetManager::PlanetManager()
: mUnlockMap()
{
	
}

void PlanetManager::setup()
{
	loadData();
	
	loadGameData();
}

void PlanetManager::loadData()			// Load Data from JSON
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/planet.json");
    
    //log("product json content:\n%s", content.c_str());
    rapidjson::Document planetJSON;
    planetJSON.Parse(content.c_str());
    if (planetJSON.HasParseError())
    {
        CCLOG("GetParseError %d\n", planetJSON.GetParseError());
        return;
    }
    
    for (rapidjson::Value::ConstMemberIterator iter = planetJSON.MemberBegin(); iter != planetJSON.MemberEnd(); ++iter){
        int planetID;
		
		sscanf(iter->name.GetString(), "planet_%02d", &planetID);
		
		const rapidjson::Value &planetJSON = iter->value;
		
		int isEnable = aurora::JSONHelper::getInt(planetJSON, "enable", 1);
		if(isEnable == false) {
			continue;
		}
		
        PlanetData *data = generateSinglePlanetFromJSON(planetJSON);
        data->setPlanetID(planetID);
        addPlanetData(data);
    }
    
 //   log("%s",infoDogData().c_str());
}

PlanetData* PlanetManager::generateSinglePlanetFromJSON(const rapidjson::Value &planetJSON)
{
    PlanetData* data = PlanetData::create();
    data->setName(aurora::JSONHelper::getString(planetJSON, "name",""));
    data->setmUnlockStatement(aurora::JSONHelper::getString(planetJSON, "unlockInfo",""));
	
	
	// PlanetType
	std::string typeStr = aurora::JSONHelper::getString(planetJSON, "type", "");
	PlanetData::PlanetType planetType = PlanetData::parseType(typeStr);
    data->setPlanetType(planetType);
	
	// Dog data 
    const rapidjson::Value& dogsJSON = planetJSON["dogs"];
    std::vector<int> dogList;
    for(int i=0;i<dogsJSON.Size();i++){
        int dogID = dogsJSON[i].GetInt();
        dogList.push_back(dogID);
    }
    data->setDogList(dogList);
	
	
	// Banner image
	data->setBannerImage(aurora::JSONHelper::getString(planetJSON, "bannerImage", ""));
	
	// Background Data
	std::string bg1 = aurora::JSONHelper::getString(planetJSON, "background1", kDefaultBg);
	std::string bg2 = aurora::JSONHelper::getString(planetJSON, "background2", bg1);
	data->setFirstBg(bg1);
	data->setSecondBg(bg2);
    
    int price = aurora::JSONHelper::getInt(planetJSON, "price", 0);
    data->setPrice(price);

	
    return data;
}

void PlanetManager::setupMock()
{
	PlanetData *data;
	
	std::vector<int> dogList2;
	dogList2.push_back(1);
	dogList2.push_back(4);
	dogList2.push_back(6);
	dogList2.push_back(2);
	dogList2.push_back(7);
	
	std::vector<int> dogList3;
	dogList3.push_back(5);
	dogList3.push_back(3);
	
	
	//
	data = PlanetData::create();
	data->setName("Earth");
	data->setPlanetID(1);
	data->setmUnlockStatement("");
	//data->setDogList();
	
	addPlanetData(data);
	
	//
	data = PlanetData::create();
	data->setName("Venus");
	data->setPlanetID(2);
	data->setmUnlockStatement("Get all Venus Dogs to unlock");
	data->setDogList(dogList2);
	
	addPlanetData(data);


	data = PlanetData::create();
	data->setName("Saturn");
	data->setPlanetID(3);
	data->setmUnlockStatement("Get all Saturn Dogs to unlock");
	data->setDogList(dogList3);
	addPlanetData(data);
}

bool PlanetManager::isEventPlanet(int planetID)
{
	PlanetData *data = getPlanetData(planetID);
	if(data == nullptr) {
		return false;
	}
	
	return data->getPlanetType() == PlanetData::Event;
}

void PlanetManager::checkAndFixSelectedPlanet()
{
	// Fix those event planet
	if(isEventPlanet(mSelectedPlanet)) {
		if(GameManager::instance()->isEventOn() == false) {
			selectPlanet(1);	// Reset to Earth
		}
		
		return;
	}
	
}

void PlanetManager::loadGameData()
{
	// Record of selected planet
	
	
//	static const char *KeyPlayerSelected = "planet_selected";
//	static const char *KeyPlayerUnlock = "planet_unlock";
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("PlanetManager.load: userDefault not available");
		return;
	}
	
	mSelectedPlanet = userData->getIntegerForKey(KeyPlayerSelected, kDefaultPlanetID);	// default planetID = 1
	
	// Record of planet unlock status
	mUnlockMap.clear();
	
	std::vector<int> keyVec = mPlanetDataMap.keys();
	for(int planetID : keyVec) {
		std::string keyStr = StringUtils::format("%s_%d", KeyPlayerUnlock, planetID);
		int unlockVal = userData->getIntegerForKey(keyStr.c_str(), 0);		// 0 - locked   1 - unlocked
		
		mUnlockMap[planetID] = unlockVal;
        
        PlanetData* data = mPlanetDataMap.at(planetID);
        if(data->getPlanetType() == PlanetData::Event){
            mUnlockMap[planetID] = true;
        }
	}
	

//	mUnlockMap[3] = true;
//	mUnlockMap[2] = true;
	mUnlockMap[kDefaultPlanetID] = true;		// always true;
	
	// Update the level setting

	fixStageData();
	
}

void PlanetManager::fixStageData()
{
	bool isStage1Unlocked = isPlanetUnLocked(1);
	if(isStage1Unlocked) {
		return;
	}
	
	// ken: if stage 1 is selected --> mean stage 1 should be unlocked
	//		if stage 2 is unlocked --> mean stage 1 should be unlocked too
	if(mSelectedPlanet == 1 || isPlanetUnLocked(2)) {
		unlockPlanet(1);
	}
}

int PlanetManager::getSelectedPlanet()
{
	return mSelectedPlanet;
}

void PlanetManager::selectPlanet(int planetID)
{
	mSelectedPlanet = planetID;
	StageManager::instance()->setCurrentStage(planetID);
	
	
	// Save data
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("setSelectedPlanet: userDefault not available");
		return;
	}
	

	userData->setIntegerForKey(KeyPlayerSelected, mSelectedPlanet);
	userData->flush();
}


bool PlanetManager::isPlanetUnLocked(int planetID)
{
	if(planetID == kDefaultPlanetID) {
		return true;
	}
	
	return mUnlockMap[planetID];
}

void PlanetManager::unlockPlanet(int planetID)
{
	setPlanetLockStatus(planetID, true);	// true mean unlocked
}



void PlanetManager::setPlanetLockStatus(int planetID, bool flag)
{
	if(planetID == kDefaultPlanetID) {
		return;	// cannot alter unlock status of the default planet
	}
	
	// Always unlocked
	PlanetData::PlanetType type = getPlanetTypeByID(planetID);
	if(type == PlanetData::PlanetType::Event) {
		mUnlockMap[planetID] = true;
		return;
	}
	
	

	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("PlanetManager.setPlanetLockStatus: userDefault not available");
		return;
	}

	int unlockValue = flag ? 1 : 0;
	
	mUnlockMap[planetID] = flag;
	
	std::string keyStr = StringUtils::format("%s_%d", KeyPlayerUnlock, planetID);
	userData->setIntegerForKey(keyStr.c_str(), unlockValue);
							   
	userData->flush();

}

void PlanetManager::unlockAllPlanet()
{
    std::vector<int> planetList = getPlanetList();
    for(int planetID : planetList) {
        unlockPlanet(planetID);
    }
}

void PlanetManager::lockAllPlanet()
{
	// lock all except the default 
    std::vector<int> planetList = getPlanetList();
    for(int planet : planetList){
        PlanetData* data = PlanetManager::instance()->getPlanetData(planet);
        if(data->getPlanetType() == PlanetData::Normal){
            setPlanetLockStatus(planet, false);
        }
    }
//    setPlanetLockStatus(kDefaultPlanetID, true);
}

std::vector<int> PlanetManager::getNewUnlockedPlanets()
{
    std::vector<int> result;
	std::vector<int> planetList = getUnlockablePlanetList();
	
	// Note (ken): Not more unlock by achievement
//    for(int planet : planetList){
//        if(mUnlockMap[planet] == false && isPlanetReadyToUnlock(planet)){
//            unlockPlanet(planet);
//            result.push_back(planet);
//        }
//    }
    return result;
}

bool PlanetManager::isPlanetReadyToUnlock(int planetID)
{
    PlanetData* data = mPlanetDataMap.at(planetID);
    if(data){
        std::vector<int> dogList = data->getDogList();
        for(int dogID : dogList){
            if(DogManager::instance()->getUnlockStatus(dogID) != DogManager::StatusUnlocked){
                return false;
            }
        }
        return true;
    }else return false;
}

int PlanetManager::getBestDistance(int planetID)
{
	return GameManager::instance()->getPlayerRecord()->getPlanetRecord(
									PlayerRecord::KeyBestDistance, planetID);
}


PlanetData::PlanetType PlanetManager::getPlanetTypeByID(int planetID)
{
	PlanetData *data = getPlanetData(planetID);
	if(data == nullptr) {
		return PlanetData::Default;
	}
	
	
	return data->getPlanetType();
}

std::vector<int> PlanetManager::getPlanetList(const PlanetData::PlanetType &typeFilter)
{
	std::vector<int> result;
	
	
	std::vector<int> allPlanets = mPlanetDataMap.keys();
	for(int planetID : allPlanets) {
		PlanetData::PlanetType type = getPlanetTypeByID(planetID);
		
		if(PlanetData::isTypeMatched(type, typeFilter) == false) {
			continue;
		}
		
		result.push_back(planetID);
	}
	
	
	// Do the sorting
	std::sort(result.begin(), result.end(), [&](int a, int b) {
        PlanetData* planetA = mPlanetDataMap.at(a);
        PlanetData* planetB = mPlanetDataMap.at(b);
        
        int planetAType = planetA->getPlanetType()==PlanetData::Event ? -1 : (int)planetA->getPlanetType();
        int planetBType = planetB->getPlanetType()==PlanetData::Event ? -1 : (int)planetB->getPlanetType();
        
        if(planetAType!=planetBType){
            return planetAType < planetBType;
        }else{
            return b > a;
        }
	});

	return result;
}


std::vector<int> PlanetManager::getDogTeamList()
{
	PlanetData::PlanetType typeSet = (PlanetData::PlanetType) (
							PlanetData::Normal
							| PlanetData::DailyReward
							| PlanetData::Event);
	
	std::vector<int> result = getPlanetList(typeSet);

	// Already sorted in getPlanetList
//	std::sort(result.begin(), result.end(), [&](int a, int b) {
//		PlanetData *planetA = getPlanetData(a);
//		PlanetData *planetB = getPlanetData(b);
//		
//		int idA = planetA->getPlanetID();
//		int idB = planetB->getPlanetID();
//		
//		return idB > idA;
//	});
//	
	return result;
}

std::vector<int> PlanetManager::getUnlockablePlanetList()
{
	PlanetData::PlanetType typeSet = (PlanetData::PlanetType) (PlanetData::Normal);
	
	std::vector<int> result = getPlanetList(typeSet);
	
	
	return result;
}



std::vector<int> PlanetManager::getPlayablePlanetList()
{
	PlanetData::PlanetType typeSet = (PlanetData::PlanetType) (
								PlanetData::Default | PlanetData::Normal);
 
	if(GameManager::instance()->isEventOn()) {
		typeSet = (PlanetData::PlanetType) (typeSet | PlanetData::Event);
	}
	
	std::vector<int> result = getPlanetList(typeSet);
	
	
	return result;
}



std::vector<int> PlanetManager::getPlanetList()
{
	std::vector<int> keyVec = mPlanetDataMap.keys();

	// sorting the key vector
	
	std::sort(keyVec.begin(), keyVec.end(), [&](int a, int b) {
        PlanetData* planetA = mPlanetDataMap.at(a);
        PlanetData* planetB = mPlanetDataMap.at(b);
        
        int planetAType = planetA->getPlanetType()==PlanetData::Event ? -1 : (int)planetA->getPlanetType();
        int planetBType = planetB->getPlanetType()==PlanetData::Event ? -1 : (int)planetB->getPlanetType();
        
        if(planetAType!=planetBType){
            return planetAType < planetBType;
        }else{
            return b > a;
        }
	});
	
	return keyVec;
}

std::string PlanetManager::infoGameData()
{
	std::string result = "Game Information:\n";
	
	result += StringUtils::format("Selected Planet: %d\n", mSelectedPlanet);
	
	std::vector<int> planetList = getPlanetList();
	for(int planetID : planetList) {
		bool isUnlocked = isPlanetUnLocked(planetID);
		
		result += StringUtils::format("planet %d: unlocked=%d\n", planetID, isUnlocked);
	}
	
	return result;
}

std::string PlanetManager::infoPlanetData()
{
	std::string result = "InfoPlanet";
	
	result = StringUtils::format("infoPlanetData: count=%d\n", (int) mPlanetDataMap.size());
	
	std::vector<int> keyVec = mPlanetDataMap.keys();
	
	for(int key : keyVec) {
		PlanetData *data = getPlanetData(key);
		result += data->toString() + "\n";
	}
	
	return result;
}

PlanetData *PlanetManager::getPlanetData(int planetID)
{
	return mPlanetDataMap.at(planetID);
}

int PlanetManager::getPlanetWithDog(int dogID)
{
    for(auto& map : mPlanetDataMap){
        PlanetData* data = map.second;
        std::vector<int> dogList = data->getDogList();
        if (std::find(dogList.begin(), dogList.end(), dogID) != dogList.end()){
            return data->getPlanetID();
        }
    }
    log("cannot find planet with dog%02d!",dogID);
    return -1;
}


std::vector<int> PlanetManager::getLockedDogList(int planetID)
{
	std::vector<int> result;
	
	PlanetData *planet = mPlanetDataMap.at(planetID);
	std::vector<int> dogList = planet->getDogList();
	for(int dogID : dogList){
		
		DogManager::UnlockStatus status = DogManager::instance()->getUnlockStatus(dogID);
		if(status == DogManager::StatusLocked) {
			result.push_back(dogID);
		}
	}
	
	return result;
}


int PlanetManager::getUnlockedDogs(int planetID)
{
    int count = 0;
    PlanetData *planet = mPlanetDataMap.at(planetID);
    std::vector<int> dogList = planet->getDogList();
    for(int dogID : dogList){
        DogManager::UnlockStatus status = DogManager::instance()->getUnlockStatus(dogID);
        if(status == DogManager::StatusUnlocked){
            count++;
        }
    }
    return count;
}

void PlanetManager::addPlanetData(PlanetData *data)
{
	mPlanetDataMap.insert(data->getPlanetID(), data);
}

void PlanetManager::getUnlockProgress(int planetID, int &currentValue, int &requiredValue)
{
	PlanetData *data = getPlanetData(planetID);
	if(data == nullptr) {
		currentValue = 0;
		requiredValue = 100;
		log("PlanetManager.getUnlockProgress: no data for planet=%d", planetID);
		return;
	}
	
	std::vector<int> dogList = data->getDogList();
	
	requiredValue = (int) dogList.size();		// number of dogs in team
	
	// TODO
	currentValue = 2;		// number of dogs unlocked
	
}

std::string PlanetManager::getPlanetName(int planetID)
{
	PlanetData *data = getPlanetData(planetID);
	if(data == nullptr) {
		return "Unknown";
	}
	
	return data->getName();
}


PlanetManager::UnlockResult PlanetManager::unlockNewPlanet(int planetID)
{
	// Refactor: unlockPlanet
	PlanetData *data = getPlanetData(planetID);
	if(data == nullptr) {
		log("UnlockPlanet: data is null. planet=%d", planetID);
		return UnlockFail;
	}
	
    bool isStageClear = GameManager::instance()->isStageClear(planetID-1);
    if(!isStageClear){
        return UnlockFailStageNotClear;
    }
	
	int userCoin = GameManager::instance()->getUserData()->getTotalCoin();
	int price = data->getPrice();
	
	if(price > userCoin){
		return UnlockFailNoMoney;
	}
	
	PlanetManager::instance()->unlockPlanet(planetID);
	PlanetManager::instance()->selectPlanet(planetID);
	GameManager::instance()->getUserData()->addCoin(-price);
	
	std::string stageName = data->getName();
	Analytics::instance()->logConsume(Analytics::act_unlock_stage,
									  stageName, price, MoneyTypeStar);
	
	return UnlockSuccess;
}
