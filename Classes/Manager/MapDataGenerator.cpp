//
//  MapDataGenerator.cpp
//  GhostLeg
//
//  Created by Ken Lee on 17/3/2016.
//
//

#include "MapDataGenerator.h"
#include "Player.h"
#include "TimeMeter.h"
#include "GameMap.h"
#include "GameWorld.h"
#include "ModelLayer.h"

#include "Enemy.h"
#include "NPC.h"
#include "MapLine.h"
#include "RandomHelper.h"
#include "DataHelper.h"
#include "CommonMacro.h"
#include "Constant.h"
#include "LevelData.h"
#include "EnemyFactory.h"
#include "DebugInfo.h"
#include "GameManager.h"
#include "ItemEffect.h"
#include "ItemEffectFactory.h"
#include "Constant.h"
#include "ViewHelper.h"

// different items
#include "Item.h"
#include "ItemPuzzle.h"
#include "Coin.h"
#include "Star.h"
#include "EnergyPotion.h"
#include "Capsule.h"
#include "Cashout.h"
#include "ShieldCapsule.h"
#include "MagnetCapsule.h"
#include "SpeedUpCapsule.h"


const int kHeightGeneration = 1000;		// Height of the area to be generated
const int kTopMarginLine = 100;				// a margin for randomness for line
const int kTopMarginEnemy = 80;

const int kCoinHeight = 35;
const int kCoinSpacing = 10;
const int kPotionHeight = 35;

const int kNumCoin = 5;
const int kNumPotion = 2;


const int kMaxLine = 12;
const int kNumLine = 5;	// 5;

const int kMaxEnemy = 16;
const int kNumEnemy = 5;

const int kCapsuleSpacing = 900;	// minimum distance between capsule


//class Player;
//class GameMap;
//class Enemy;
//class MapLine;

#pragma mark - Local functions
namespace {
	bool isTutorialMap(int mapID) {
		return kTutorialMapID == mapID;
	}
	
	Item *createItemByType(int itemType)
	{
        if(itemType == Item::Type::ItemSmallCoin) {		// Coin!!!
            return Coin::create(Coin::SmallCoin);					// -> Coin::create(Coin::SmallCoin);
        } else if(itemType == Item::Type::ItemBigCoin) {
            return Coin::create(Coin::LargeCoin);
        } else if(itemType == Item::Type::ItemSmallStar) {
            return Star::create(Star::SmallStar);
        } else if(itemType == Item::Type::ItemMediumStar) {
            return Star::create(Star::MediumStar);
        } else if(itemType == Item::Type::ItemBigStar) {
            return Star::create(Star::LargeStar);
        } else if(itemType == Item::Type::ItemPuzzle) {
			return ItemPuzzle::create();
		} else if(itemType == Item::Type::ItemPotion) {
//			return EnergyPotion::create();
            return nullptr;
		} else if(itemType == Item::Type::ItemCashout) {
			return Cashout::create();
		} else if(itemType == Item::Type::ItemSpeedUp) {
			return SpeedUpCapsule::create();
		} else if(itemType == Item::Type::ItemShield) {
			//return ShieldCapsule::create();
			return nullptr;		// no more !!
		} else if(itemType == Item::Type::ItemMagnet) {
			return MagnetCapsule::create();
		}
		
		return nullptr;
	}
	
	int getEnemySpeedByType(int enemyType) {
		if(enemyType == 100) {
			return 50;
		} else if(enemyType == 101) {
			return 30;
		} else {
			return 50;
		}
	}
	
	int getEnemySpeed(int playerDistance) {
		// Random Value
		int random = aurora::RandomHelper::randomBetween(1, 10);		// 1 - 10
		
		int chance;		// 0:0% 1: 10%	10: 100%
		if(playerDistance > 500) {
			chance = 5;
		} else {
			chance = 0;
		}
		
		if(random < chance) {
			return 50;
		} else {
			return 0;
		}
	}
	
	void generateIntArray(int start, int size, std::vector<int> &array) {
		int value = start;
		for(int i=0; i<size; i++) {
			array.push_back(value);
			
			value++;
		}
	}

	bool isAdjacentPos(int lastPos, int newPos) {
		if(lastPos == -1) {
			return false;
		}
		
		int leftPos = lastPos - 4;
		int rightPos = lastPos + 4;
		
		if(leftPos >= 0 && leftPos == newPos) {
			return true;
		}
		
		if(rightPos >= 0 && rightPos == newPos) {
			return true;
		}
		
		
		return false;
	}


	bool isAdjacentToNearbyLine(std::vector<int> linePos, int newPos) {
		if(linePos.size() == 0) {
			return false;
		}
		
		for(int i=0; i<linePos.size(); i++) {
			if(isAdjacentPos(linePos[i], newPos)) {
				return true;
			}
		}
		
		return false;
	}
	

}


#pragma mark - Class Implementation
MapDataGenerator::MapDataGenerator()
: mMap(NULL)
, mPlayer(NULL)
, mRangeStart(0)
, mRangeEnd(0)
, mMapID(0)
, mCustomNpcAnime(0)
, mIsGenerationDone(true)
{
	
}

MapDataGenerator::~MapDataGenerator()
{
	CC_SAFE_RELEASE_NULL(mPlayer);
	CC_SAFE_RELEASE_NULL(mMap);
	
	mHoriLine.clear();	// clear the array content
	mEnemy.clear();		// clear the array content
}

void MapDataGenerator::init(GameMap *map, Player *player, int rangeStart)
{
	setGameMap(map);
	setPlayer(player);
	
	mRangeStart = rangeStart;
	mRangeEnd = mRangeStart + kMapHeight;
	
	mPlayerY = (mPlayer == NULL) ? 0 : mPlayer->getPosition().y;
}

void MapDataGenerator::setCapsuleType(int effectType, int level, float value)
{
	mCapsuleEffect = effectType;
	mCapsuleValue = value;
	mCapsuleLevel = level;
	
}

void MapDataGenerator::setPlayer(Player *var)
{
	CC_SAFE_RETAIN(var);
	CC_SAFE_RELEASE(mPlayer);
	mPlayer = var;
}

void MapDataGenerator::setGameMap(GameMap *map)
{
	CC_SAFE_RETAIN(map);
	CC_SAFE_RELEASE(mMap);
	mMap = map;
}

Player *MapDataGenerator::getPlayer() const
{
	return mPlayer;
}

GameMap *MapDataGenerator::getGameMap() const
{
	return mMap;
}

void MapDataGenerator::generate()
{
	generateByLevel();
}

int MapDataGenerator::getRangeStart()
{
	return mRangeStart;
}

int MapDataGenerator::getRangeEnd()
{
	return mRangeEnd;
}

const Vector<MapLine *>& MapDataGenerator::getHoriLine()
{
	return mHoriLine;
}

//const Size& getWinSize() const;
const Vector<Enemy *>& MapDataGenerator::getEnemy()
{
	return mEnemy;
}

const Vector<NPC *>& MapDataGenerator::getNpcList()
{
	return mNpcList;
}

int MapDataGenerator::getMapLevelByDistance(int distance)
{
	
	int offset = distance - mInitialOffset;
	int level = offset / kMapHeight;	// total map height
	
	
	int maxLevel = MapLevelDataCache::instance()->getMaxMapLevel();
	if(level >= maxLevel) {
		level = maxLevel;
	}else if(level < 0) {
		level = 0;
	}

	return level;
}

int MapDataGenerator::determineMapByDistance(int distance)
{
	
	// find the level (or zone) based on the distance travelled;
	int level = getMapLevelByDistance(distance);
	
	const MapLevelConfig *config = MapLevelDataCache::instance()->getMapLevelConfig(level);
	if(config == nullptr) {
		return 0;
	}
	
	
	int mapStart = config->mapStart;
	int mapEnd = config->mapEnd;
	
	
	int mapID = aurora::RandomHelper::randomBetween(mapStart, mapEnd);
	
	log("DEBUG: start=%d end=%d mapID=%d initialDist=%d", mapStart, mapEnd, mapID, mInitialOffset);
	
	if(MapLevelDataCache::instance()->hasMap(mapID) == false) {
		mapID = 0;
	}
	
	return mapID;
}

void MapDataGenerator::generateByLevel()
{
	int debugMap = GameManager::instance()->getDebugMap();
	
	int mapID = debugMap >= 0 ? debugMap : determineMapByDistance(mRangeStart);
	
	generateByMapID(mapID);
}

void MapDataGenerator::generateByNpcMapID(int mapID)
{
	generateByMapID(mapID, true);
}


void MapDataGenerator::generateByMapID(int mapID, bool isNpcMap)
{
	// log("***DEBUG: GenerateByMapID");
	resetData();
	
	MapLevelData *mapData = isNpcMap ?
				MapLevelDataCache::instance()->getNpcMap(mapID)
				: MapLevelDataCache::instance()->getMap(mapID);
	if(mapData == nullptr) {
		log("generateByMapID: fail to find map %d", mapID);
		return;
	}
	
	generateByMapData(mapID, mapData);
}

void MapDataGenerator::resetData()
{
	mEnemy.clear();
	mHoriLine.clear();
	mNpcList.clear();
	mItemList.clear();
	

}





void MapDataGenerator::generateByMapData(int mapID, MapLevelData *mapData)
{
	
	mMapID = mapID;
	
	resetData();
	
	generateMapLinesFromLevel(mapData);
	generateItemsFromLevel(mapData);
	generateEnemiesFromLevel(mapData);
	
	if(mapData->isNpcMap()) {
		generateNpcFromLevel(mapData);
	}
	
}

void MapDataGenerator::generateByRandom()
{
	generateLineByRandom();
	generateEnemyByRandom();
	generateItemByRandom();
}


void MapDataGenerator::generateLineByRandom()
{
	mHoriLine.clear();
	
	// Make a random sequence
	std::vector<int> sequence;
	generateIntArray(0, kMaxLine, sequence);
	aurora::RandomHelper::shuffleVector(sequence);
	
	
	
	std::vector<int> positionList;
	
	int count = 0;
	//
	for(int i=0; i<kMaxLine; i++) {
		int pos = sequence[i];
		
		bool isAdj = isAdjacentToNearbyLine(positionList, pos);
//		log("DEBUG: lastPos=%d newPos=%d isAdj=%d", lastPos, pos, isAdj);
		if(isAdj) {
			continue;
		}
		
		addLineForPosition(pos);
		positionList.push_back(pos);
		
		count++;
		if(count >= kNumLine) {
			break;
		}
	}
}

void MapDataGenerator::generateEnemyByRandom()
{
	mEnemy.clear();

	// Make a random sequence
	std::vector<int> sequence;
	generateIntArray(0, kMaxEnemy, sequence);
	aurora::RandomHelper::shuffleVector(sequence);
	
	
	//
	for(int i=0; i<kNumEnemy; i++) {
		int pos = sequence[i];
		
		addEnemyForPosition(pos);
	}
}


void MapDataGenerator::generateItemByRandom()
{
	mItemList.clear();
    generateCoins();
    generatePotions();

}

void MapDataGenerator::generateCoins()
{
    //	int numVertLine = mMap->get
    // Location for X
    int lastPos = mMap->getVertLineCount() - 1;
    int pos = aurora::RandomHelper::randomBetween(0, lastPos);
    float x = mMap->getVertLineX(pos);
    
    // Location for Y
    int coinHeight = kNumCoin * (kCoinHeight + kCoinSpacing);		// 35: coin image size  10: spacing
    int randRange = kHeightGeneration - coinHeight;
    float y = (float) mRangeStart + kCoinHeight / 2 +
    aurora::RandomHelper::randomBetween(0, randRange);
    
    
    for(int i=0; i<kNumCoin; i++) {
        Coin *coin = Coin::create(Coin::SmallCoin);
        coin->setPosition(Vec2(x, y));
        
        mItemList.pushBack(coin);
        
        y += kCoinHeight + kCoinSpacing;
    }
    

}

void MapDataGenerator::generatePotions()
{
    for(int i=0; i<kNumPotion; i++)
    {
        
        int lastPos = mMap->getVertLineCount() - 1;
        float x = mMap->getVertLineX(aurora::RandomHelper::randomBetween(0, lastPos));
        
        int potionHeight = kNumCoin * (kCoinHeight + kCoinSpacing);
        int randRange = kHeightGeneration - potionHeight;
        float y = (float) mRangeStart + kPotionHeight / 2 + aurora::RandomHelper::randomBetween(0, randRange);
        
        EnergyPotion *potion = EnergyPotion::create();
        potion->setPosition(Vec2(x, y));
        mItemList.pushBack(potion);
    }
    
}


void MapDataGenerator::generateMapLinesFromLevel(MapLevelData *mapData)
{
	mHoriLine.clear();

	if(mMap == NULL) {
		log("ERROR:MapDataGenerator:generateMapLinesFromLevel: mMap is null");
		return;
	}

	//Vector<LevelLineData *> lineData = mapData->getLineData();
	Vector<LevelLineData *> randomLine;
	
	mapData->getRandomLineData(randomLine);
	
	//log("debug: %d: %s", i, line->toString().c_str());
	
	for(int i=0; i<randomLine.size(); i++) {
		LevelLineData *line = randomLine.at(i);
		log("debug: %d: %s", i, line->toString().c_str());
		
		float x1 = mMap->getVertLineX(line->xLine);
		float x2 = mMap->getVertLineX(line->xLine+1);
		float y = mRangeStart + line->y;
		
		MapLine *mapLine = MapLine::create(MapLine::eTypeFix, x1, y, x2, y);
		mapLine->setLineType(line->type);
		mHoriLine.pushBack(mapLine);
	}
}

MapLine *MapDataGenerator::getEnemyStandingLine(const Vec2 &position)
{
	for(int i=0; i<mHoriLine.size(); i++) {
		MapLine *line = mHoriLine.at(i);
		
		Vec2 startPoint = line->getStartPoint();
		Vec2 endPoint = line->getEndPoint();
		
		if(startPoint.y != position.y) {	// assume start / end point's y are same
			continue;
		}
		
		if(position.x > startPoint.x && position.x < endPoint.x) {
			return line;
		}
	}
	
	return nullptr;
}

void MapDataGenerator::addEnemyWithLevelData(LevelEnemyData *enemyData, int startX)
{
	float x = startX + enemyData->x;
	float y = mRangeStart + enemyData->y;
	
	//log("MapDataGenerator: debug: startX=%d x=%f y=%f enemyData=%s", startX, x, y, enemyData->toString().c_str());
	
	Enemy *enemy = EnemyFactory::instance()->createEnemy(enemyData->type);
	enemy->setPosition(Vec2(x, y));
	enemy->updateSetDefaultDir();
	if(enemy->getResID() >= 100) {
		enemy->setupScaleAndFlip(enemyData->scaleX, enemyData->scaleY,
								 enemyData->flipX, enemyData->flipY);
	} else {
		enemy->setFace(enemyData->flipX == 0);
	}
	enemy->setupHitboxes();		// is it not good??
	
	
	// XXXX: Temp.
	MapLine *standingLine = getEnemyStandingLine(enemy->getPosition());	// ????
	// log("debug: type=%d line=%s", enemyData->type, (standingLine == nullptr ? "na" : standingLine->toString().c_str()));
	if(standingLine != nullptr) {
		enemy->setHoriLine(standingLine);
		enemy->setDir(DirRight);
	}
	
	
	mEnemy.pushBack(enemy);
}


Vec2 MapDataGenerator::getDropPosition()
{
	return mDropPosition;
}

Vec2 MapDataGenerator::getStopPosition()
{
	return mStopPosition;
}

void MapDataGenerator::generateNpcFromLevel(MapLevelData *map)
{
	if(mMap == NULL) {
		log("ERROR:MapDataGenerator:generateNpcFromLevel: mMap is null");
		return;
	}
	
	Vector<LevelNpcData *> npcDataList = map->getNpcData();
	if(npcDataList.size() == 0) {
		log("generateNpcFromLevel: no npc in NPC map");
		return;
	}
	
	LevelNpcData *npcData = npcDataList.at(0);
	addNpcWithLevelData(npcData);
	
//	CC_SYNTHESIZE(Vec2, mDropPosition, DropPosition);
//	CC_SYNTHESIZE(Vec2, mStopPosition, StopPosition);

	//
	mDropPosition = map->getDropPosition() + Vec2(0, mRangeStart);
	mStopPosition = map->getStopPosition() + Vec2(0, mRangeStart);
}



void MapDataGenerator::addNpcWithLevelData(LevelNpcData *npcData)
{
    float x = npcData->x;
    float y = mRangeStart + npcData->y;
    
    // final Npc Anime
    int finalNpcAnime = npcData->npcID;
    if(getCustomNpcAnime() > 0) {
        finalNpcAnime = getCustomNpcAnime();
    }
	
	NPC *npc = NPC::create();
	npc->setNpc(finalNpcAnime);
	npc->setPosition(Vec2(x, y));
	npc->update(0);
	npc->setFace(npcData->faceDir == 1 ? true : false);
	//enemy->setDir(DirRight);
	
	mNpcList.pushBack(npc);
}




void MapDataGenerator::generateEnemiesFromLevel(MapLevelData *mapData)
{
	if(mMap == NULL) {
		log("ERROR:MapDataGenerator:addEnemy: mMap is null");
		return;
	}

	Vector<LevelEnemyData *> enemyData = mapData->getEnemyData();
	
	float startX = 0;		// mMap->getVertLineX(0);		// map editor already handle that
	
	for(int i=0; i<enemyData.size(); i++) {
		LevelEnemyData *enemy = enemyData.at(i);
		
		addEnemyWithLevelData(enemy, startX);
	}
}

void MapDataGenerator::generateStarFromHoriLine()
{
	if(mMap == NULL) {
		return;
	}
	
	for(int i=0; i<mHoriLine.size(); i++) {
		MapLine *line = mHoriLine.at(i);
		
		//
		if(isTutorialMap(mMapID) && line->getLineType() == 2) {
			continue;		// Skip making a star for that line
		}
		
		
		//
		Vec2 posArray[2];
		Item *itemObj;
		
		posArray[0] = line->getStartPoint();
		posArray[1] = line->getEndPoint();
		
		for(int i=0; i<2; i++) {
			Vec2 pos = posArray[i];
			
            itemObj = Star::create(Star::SmallStar);
			if(itemObj != nullptr) {
				itemObj->setPosition(pos);
				mItemList.pushBack(itemObj);
			}
		}
	}
}

void MapDataGenerator::generateItemsFromLevel(MapLevelData *mapData)
{
	if(mMap == NULL) {
		log("ERROR:MapDataGenerator:generateItem: mMap is null");
		return;
	}
	
	Vector<LevelItemData *> itemData = mapData->getItemData();
	
	float startX = 0;	// mMap->getVertLineX(0);		// note: map editor already handle
	for(int i=0; i<itemData.size(); i++) {
		LevelItemData *item = itemData.at(i);
		
		float x = startX + item->x;
		float y = mRangeStart + item->y;
		
		y += 20/2;	// half grid size
		
		Item *itemObj = createItemByType(item->type);

		if(itemObj != nullptr) {
			itemObj->setPosition(Vec2(x, y));
			mItemList.pushBack(itemObj);
		}
	}
	
	//generateCashPo(mapData);
}

void MapDataGenerator::generateCapsules(MapLevelData *mapData)
{
	if(mCapsuleEffect <= 0) {
		return;
	}
	
	Vector<LevelItemData *> itemData = mapData->getItemData();
	
	
	std::vector<Vec2> capsulePosArray;
	
	// Find the possible capsule position
	float startX = mMap->getVertLineX(0);
	for(int i=0; i<itemData.size(); i++) {
		LevelItemData *item = itemData.at(i);
		//log("DEBUG: item=%s", item->toString().c_str());
		if(item->type != Item::Type::ItemCapsule) {
			continue;
		}
		
		float x = startX + item->x;
		float y = mRangeStart + item->y;
		if(mLastCapsulePos > 0) {	// It's not the first map
			if((y - mLastCapsulePos) < kCapsuleSpacing) {
				log("CapsulePos Too close");
				continue;		// Too near last capsule
			}
		}
		capsulePosArray.push_back(Vec2(x, y));
	}
	
	if(capsulePosArray.size() == 0) {
		log("capsulePosArray is zero");
		return;
	}
	
	
	int index = aurora::RandomHelper::randomBetween(0, (int)capsulePosArray.size()-1);
	Vec2 capsulePos = capsulePosArray[index];
	
	// Create the capsule
	Capsule *capsule = Capsule::create();
	
	ItemEffect::Type effectType = (ItemEffect::Type) mCapsuleEffect;
	capsule->setItemEffectType(effectType);
	capsule->setItemEffectValue(mCapsuleValue);
	capsule->setLevel(mCapsuleLevel);
	
	capsule->setPosition(capsulePos);
	mItemList.pushBack(capsule);
	
	log("capsule created: %s", capsule->toString().c_str());
	
	// Update the last Capsule Pos
	mLastCapsulePos = capsulePos.y;
}



std::string MapDataGenerator::info()
{
	std::string result = "";
	
	char temp[1000];
	
	// Range
	sprintf(temp, "range: %d - %d\n", mRangeStart, mRangeEnd);
	result += temp;
	
	//
	result += "Line Information\n-------------\n";
	result += infoLine();

	result += "Enemy Information:\n-------------\n";
	result += infoEnemy();
	
	result += "Npc Information:\n-------------\n";
	result += infoNpc();

	result += "Item Information:\n-------------\n";
	result += infoItem();

	return result;
}

std::string MapDataGenerator::infoNpc()
{
	std::string result = "";

	result += StringUtils::format("Npc count: %ld\n", mNpcList.size());
	
	for(NPC *npc : mNpcList) {
		result += npc->toString();
		result += "\n";
	}
	
	
	return result;
}

std::string MapDataGenerator::infoEnemy()
{
	std::string result = "";

	char temp[100];
	
	sprintf(temp, "Enemy count: %ld\n", mEnemy.size());
	result += temp;
	
	for(int i=0; i<mEnemy.size(); i++) {
		Enemy *enemy = mEnemy.at(i);
		
		result += enemy->toString();
		result += "\n";
	}
	
	
	return result;
}

std::string MapDataGenerator::infoLine()
{
	std::string result = "";

	char temp[100];
	
	sprintf(temp, "Line count: %ld\n", mHoriLine.size());
	result += temp;
	
	for(int i=0; i<mHoriLine.size(); i++) {
		MapLine *line = mHoriLine.at(i);
		
		result += line->toString();
		result += "\n";
	}
	
	return result;
}

const Vector<Item *>& MapDataGenerator::getItemList()
{
	return mItemList;
}

std::string MapDataGenerator::infoItem()
{
	//
	std::string result = "Item ";
	
	CCVECTOR_INFO(Item, mItemList, result);
	
	return result;
}


// pos is a value indicate a position in the gameMap
// Here it is
// Position
//
//				3	7	11	15
//				2	6	10	14
//				1	5	9	13
//				0   4	8	12
//			 ---------------------
// lineIdx:		0	1	2	3

void MapDataGenerator::addEnemyForPosition(int pos)
{
	if(mMap == NULL) {
		log("ERROR:MapDataGenerator:addEnemy: mMap is null");
		return;
	}
	
	int xIndex = pos / 4;
	int yIndex = pos % 4;
	
	int ySpacing = (mRangeEnd - mRangeStart - kTopMarginLine) / 4;
	int randomY = aurora::RandomHelper::randomBetween(30, 50);
	
	float x = mMap->getVertLineX(xIndex);
	float y = mRangeStart + ySpacing * yIndex + randomY;
	
	
	Enemy *enemy = Enemy::create();
	enemy->setPosition(Vec2(x, y));
	enemy->setSpeed(getEnemySpeed(mPlayerY));

	mEnemy.pushBack(enemy);
}

// pos is a value indicate a position in the gameMap
// Here it is
// Position:
//
//				3	7	11
//				2	6	10
//				1	5	9
//				0   4	8
//			 -------------
// lineIdx:		0	1	2
//

void MapDataGenerator::addLineForPosition(int pos)
{
	if(mMap == NULL) {
		log("ERROR:MapDataGenerator:addEnemy: mMap is null");
		return;
	}
	
	int xIndex = pos / 4;
	int yIndex = pos % 4;
	
	int ySpacing = (mRangeEnd - mRangeStart - kTopMarginLine) / 4;
	int randomY = aurora::RandomHelper::randomBetween(0, 10) * 5;
	
	float x1 = mMap->getVertLineX(xIndex);
	float x2 = mMap->getVertLineX(xIndex+1);
	float y = mRangeStart + ySpacing * yIndex + randomY;
	
	MapLine *line = MapLine::create(MapLine::eTypeFix, x1, y, x2, y);
	
	log("DEBUG: line: pos=%d detail=%s", pos, line->toString().c_str());
	
	mHoriLine.pushBack(line);
}





#pragma mark - Internal Testing Code
// Testing
void MapDataGenerator::testAddEnemyForPosition()
{
	const int numPos = 2;
	const int testArr[numPos] = {0, 5};
	
	for(int i=0; i<numPos; i++) {
		int pos = testArr[i];
		
		addEnemyForPosition(pos);
	}
}


void MapDataGenerator::testAddLineForPosition()
{
	const int numPos = 2;
	const int testArr[numPos] = {3, 5};
	
	for(int i=0; i<numPos; i++) {
		int pos = testArr[i];
		
		addLineForPosition(pos);
	}
}

int MapDataGenerator::getMapID()
{
	return mMapID;
}

void MapDataGenerator::addModelToWorld(GameWorld *world)
{
	log("***DEBUG: addModelToWorld");
	if(world == nullptr)
	{
		log("MapDataGenerator.addModelToWorld: world is null");
		return;
	}
	
	// log("DEBUG------>\n%s\n", info().c_str());
	
	world->getMap()->addSlopeLines(getHoriLine());
	world->getModelLayer()->addEnemyList(getEnemy());
	world->getModelLayer()->addItemList(getItemList());
	world->getModelLayer()->addNpcList(getNpcList());
	
}

void MapDataGenerator::setRangeStart(int offset)
{
	mRangeStart = offset;
}


#pragma mark - Async Map Generation
void MapDataGenerator::generateGameMapAsync(int mapID, bool isNpcMap)		// just define the list of elements to be generate
{
	//log("***DEBUG: generateGameMapAsync");
	resetData();
	
	MapLevelData *mapData = isNpcMap ?	MapLevelDataCache::instance()->getNpcMap(mapID)
										: MapLevelDataCache::instance()->getMap(mapID);
	if(mapData == nullptr) {
		log("generateGameMapAsync: fail to find map %d", mapID);
		return;
	}
	
	generateGameMapByData(mapID, mapData);
	
}


std::string MapDataGenerator::infoMapElementQueue()
{
	std::string result = "";
	
	result += StringUtils::format("isGenerationDone: %d\n", mIsGenerationDone);
	
	result += "ItemQueue:\n";
	for(LevelItemData *data : mItemQueue) {
		result += data->toString() + "\n";
	}
	
	result += "EnemyQueue:\n";
	for(LevelEnemyData *data : mEnemyQueue) {
		result += data->toString() + "\n";
	}
	
	result += "NpcQueue:";
	result += (mNpcData == nullptr) ? "null" : mNpcData->toString();


	
//	// Enemy Queue
//	Vector<LevelEnemyData *> enemyData = mapData->getEnemyData();
//	for(LevelEnemyData *data : enemyData) {
//		mEnemyQueue.push_back(data);
//	}
//	
//	// NPC
//	if(mapData->isNpcMap()) {
//		Vector<LevelNpcData *> npcDataList = mapData->getNpcData();
//		if(npcDataList.size() > 0) {
//			mNpcData = npcDataList.at(0);
//		}
//	}
	
	return result;
}

void MapDataGenerator::generateGameMapByData(int mapID, MapLevelData *mapData)
{
	
	if(mapData == nullptr) {
		log("generateGameMapByData: data is null");
		return;
	}
	
	//
	mMapID = mapID;
	
	
	// Reset Data
	mIsGenerationDone = false;
	mItemQueue.clear();
	mEnemyQueue.clear();
	mNpcData = nullptr;
	
	// Update the queue
	
	// item queue
	Vector<LevelItemData *> itemData = mapData->getItemData();
	for(LevelItemData *data : itemData) {
		mItemQueue.push_back(data);
	}
	
	// Enemy Queue
	Vector<LevelEnemyData *> enemyData = mapData->getEnemyData();
	for(LevelEnemyData *data : enemyData) {
		mEnemyQueue.push_back(data);
	}
	
	// NPC
	if(mapData->isNpcMap()) {
		Vector<LevelNpcData *> npcDataList = mapData->getNpcData();
		if(npcDataList.size() > 0) {
			mNpcData = npcDataList.at(0);
		}
	}
}



void MapDataGenerator::generateMapElements(GameWorld *world)
{
	if(mIsGenerationDone) {
		return;
	}
	
	TSTART("mapData.createItem");
	createItemFromQueue(world, 3);
	TSTOP("mapData.createItem");
	
	TSTART("mapData.createEnemy");
	createEnemyFromQueue(world, 3);
	TSTOP("mapData.createEnemy");
	
	if(mNpcData != nullptr) {
		createNpc(world, mNpcData);
		mNpcData = nullptr;
	}
	
	if(mEnemyQueue.size() == 0 && mItemQueue.size() == 0 && mNpcData == nullptr) {
		mIsGenerationDone = true;
	}
}


void MapDataGenerator::createItemFromQueue(GameWorld *world, int count)
{
	if(mItemQueue.size() <= 0) {
		return;
	}
	
	std::vector<LevelItemData *> dataList;
	
	aurora::DataHelper::dequeueVector(mItemQueue, dataList, count);
	
	for(LevelItemData *data : dataList) {
		createItem(world, data);
	}

}

void MapDataGenerator::createEnemyFromQueue(GameWorld *world, int count)
{
	if(mEnemyQueue.size() <= 0) {
		return;
	}
	
	std::vector<LevelEnemyData *> dataList;
	
	aurora::DataHelper::dequeueVector(mEnemyQueue, dataList, count);
	
	for(LevelEnemyData *data : dataList) {
		createEnemy(world, data);
	}
}


void MapDataGenerator::createEnemy(GameWorld *world, LevelEnemyData *enemyData)
{
	float x = enemyData->x;
	float y = mRangeStart + enemyData->y;
	
	//log("MapDataGenerator: debug: x=%f y=%f enemyData=%s", x, y, enemyData->toString().c_str());
	
	Enemy *enemy = EnemyFactory::instance()->createEnemy(enemyData->type);
	enemy->setPosition(Vec2(x, y));
	enemy->setObjectScaleX(enemyData->scaleX);
	enemy->setObjectScaleY(enemyData->scaleY);
	enemy->setObjectFlipX(enemyData->flipX);
	enemy->setObjectFlipY(enemyData->flipY);

	// Moved to setupModel
//	enemy->updateSetDefaultDir();
//	if(enemy->getResID() >= 100) {
//		enemy->setupScaleAndFlip(enemyData->scaleX, enemyData->scaleY,
//								 enemyData->flipX, enemyData->flipY);
//	} else {
//		enemy->setFace(enemyData->flipX == 0);
//	}
//	enemy->setupHitboxes();		// is it not good??
	
	MapLine *standingLine = getEnemyStandingLine(enemy->getPosition());	// ????
	// log("debug: type=%d line=%s", enemyData->type, (standingLine == nullptr ? "na" : standingLine->toString().c_str()));
	if(standingLine != nullptr) {
		enemy->setHoriLine(standingLine);
		enemy->setDir(DirRight);
	}
//
	
	
//#if IS_IOS
//	enemy->setupModel();
//#endif
	

	// Add to world
	world->getModelLayer()->addEnemy(enemy);
}

void MapDataGenerator::createItem(GameWorld *world, LevelItemData *item)
{
	float x = item->x;
	float y = mRangeStart + item->y + 20/2;
	
	Item *itemObj = createItemByType(item->type);
	
	if(itemObj == nullptr) {
		return;		// something wrong here!!
	}
	itemObj->setPosition(Vec2(x, y));
	world->getModelLayer()->addItem(itemObj);
}

void MapDataGenerator::createNpc(GameWorld *world, LevelNpcData *npcData)
{
	float x = npcData->x;
	float y = mRangeStart + npcData->y;
	
	// final Npc Anime
	int finalNpcAnime = npcData->npcID;
	if(getCustomNpcAnime() > 0) {
		finalNpcAnime = getCustomNpcAnime();
	}
	
	NPC *npc = NPC::create();
	npc->setNpc(finalNpcAnime);
	npc->setPosition(Vec2(x, y));
	npc->update(0);
	npc->setFace(npcData->faceDir == 1 ? true : false);
	//enemy->setDir(DirRight);
	
	world->getModelLayer()->addNpc(npc);
}
