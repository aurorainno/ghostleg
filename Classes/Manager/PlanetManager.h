//
//  PlanetManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#ifndef PlanetManager_hpp
#define PlanetManager_hpp

#include <stdio.h>
#include <map>
#include <string>

#include "cocos2d.h"
#include "PlanetData.h"
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

USING_NS_CC;



class PlanetManager
{
public:
	enum UnlockResult {
		UnlockSuccess,
		UnlockFailNoMoney,
        UnlockFailStageNotClear,
		UnlockFail,
	};
public:
	static PlanetManager *instance();
	
	
public:
	PlanetManager();
	
	void setup();
	void loadData();			// Load Data from JSON
	void setupMock();

	
	void loadGameData();
    PlanetData* generateSinglePlanetFromJSON(const rapidjson::Value &planetJSON);
	
	bool isPlanetUnLocked(int planetID);
	void unlockPlanet(int planetID);
	void setPlanetLockStatus(int planetID, bool flag);
	
	int getBestDistance(int planetID);
    int getUnlockedDogs(int planetID);
    int getPlanetWithDog(int dogID);
	
	PlanetData::PlanetType getPlanetTypeByID(int planetID);
	bool isEventPlanet(int planetID);
	
	void checkAndFixSelectedPlanet();
	
	std::vector<int> getPlanetList(const PlanetData::PlanetType &typeFilter);
	
	std::vector<int> getPlanetList();
	std::vector<int> getPlayablePlanetList();
	std::vector<int> getUnlockablePlanetList();
	
	std::vector<int> getDogTeamList();
	
	std::vector<int> getLockedDogList(int planetID);
	
	std::string infoGameData();
	std::string infoPlanetData();
	std::string getPlanetName(int planetID);
	PlanetData *getPlanetData(int planetID);
	
	int getSelectedPlanet();
	void selectPlanet(int planetID);
	void getUnlockProgress(int planetID, int &currentValue, int &requiredValue);
    std::vector<int> getNewUnlockedPlanets();
    bool isPlanetReadyToUnlock(int planetID);
    
    void lockAllPlanet();
	void unlockAllPlanet();
	
	UnlockResult unlockNewPlanet(int planetID);
	
private:
	void addPlanetData(PlanetData *data);
	void fixStageData();

private:
	std::map<int, bool> mUnlockMap;
	Map<int, PlanetData *> mPlanetDataMap;
	int mSelectedPlanet;
    
};

#endif /* PlanetManager_hpp */
