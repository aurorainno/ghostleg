//
//  LevelDataLoader.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#include "LevelData.h"
#include "StringHelper.h"
#include "RandomHelper.h"
#include "JSONHelper.h"
#include "Constant.h"
#include <map>

#include "MapLine.h"
#include "Enemy.h"
#include "Item.h"
#include "Player.h"
#include "GameWorld.h"
#include "EnemyFactory.h"

#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

#define kParseStageNone     0
#define kParseStageMain     1
#define kParseStageLine     2
#define kParseStageEnemy    3
#define kParseStageItem     4
#define kParseStageNpc		5
#define kParseStageNpcSetting     6

const int kInitialSpeed = 130;
const int kMaxSpeed = 300;


#define kConfigFile			"levelConfig.cfg"

static MapLevelDataCache *sInstance = nullptr;
static MapLevelManager *sMapLevelManager = nullptr;

namespace {
	bool isStageEnd(std::string &line)
	{
		std::string trimLine = aurora::StringHelper::trim(line);
		
		
		return trimLine.length() == 0;
	}
	
	int determineStage(std::string &line)
	{
		std::string trimLine = aurora::StringHelper::trim(line);
		
		if(trimLine.compare("[Main]") == 0) {
			return kParseStageMain;
		} else if(trimLine.compare("[Line]") == 0) {
			return kParseStageLine;
		} else if(trimLine.compare("[Enemy]") == 0) {
			return kParseStageEnemy;
		} else if(trimLine.compare("[Item]") == 0) {
			return kParseStageItem;
		} else if(trimLine.compare("[Npc]") == 0) {
			return kParseStageNpc;
		} else if(trimLine.compare("[NpcSetting]") == 0) {
			return kParseStageNpcSetting;
		}
		
		
		return kParseStageNone;
	}
	
	typedef struct
	{
		int type;
		int probability;
		int numLine;
		int requireLine;    // number of line required
	} LineProbability;
	
	
	//
	// probabilityMap: key=type LineProbability
	void setupProbabilityMap(std::map<int, LineProbability> &probabilityMap,
									Vector<LevelLineData *> lineArray)
	{
		// Type=1 (Green Lines) app prob
		LineProbability probAllLines;
		
		int type = 1;
		probAllLines.probability = 100;
		probAllLines.type = type;
		probAllLines.numLine = 0;
		probAllLines.requireLine = 0;
		probabilityMap[type] = probAllLines;

		// Other lines
		for(int type=2; type<=6; type++) {
			LineProbability prob;
			prob.probability = 50;
			prob.requireLine = 1;
			prob.numLine = 0;
			prob.type = type;
			
			probabilityMap[type] = prob;
		}
	}

	
	void setupProbabilityMapComplex(std::map<int, LineProbability> &probabilityMap,
							 Vector<LevelLineData *> lineArray)
	{
		
		// collect the probablity and count first
		for(int i=0; i<lineArray.size(); i++) {
			LevelLineData *line = lineArray.at(i);
			
			//log("DEBUG: line=%s", line->toString().c_str());
			
			int type = line->type;
			int prob = line->probability;
			
			if(probabilityMap.find(type) == probabilityMap.end()) {
				LineProbability newProp;
				
				newProp.probability = prob;
				newProp.numLine = 1;
				newProp.type = type;
				
				probabilityMap[type] = newProp;
			} else {
				LineProbability &lineProp = probabilityMap.at(type);
				lineProp.numLine++;
			}
		}
		
		// Calculate the requireLine
		std::map<int, LineProbability>::reverse_iterator it;
		for (it = probabilityMap.rbegin(); it != probabilityMap.rend(); ++it)
		{
			LineProbability &prob = it->second;
			
			float chance = (float) prob.probability * prob.numLine / 100;
			prob.requireLine = (int)(floorf(chance));
		}
		
		// Debug
//		for (it = probabilityMap.rbegin(); it != probabilityMap.rend(); ++it)
//		{
//			int type = it->first;
//			LineProbability prob = it->second;
//			
//			log("type=%d prop=%d numLine=%d requireLine=%d", type, prob.probability, prob.numLine, prob.requireLine);
//		}
		
		
	}
	
	
	
	void shuffleLineVector(Vector<LevelLineData *> &vec, int numShuffle)
	{
		// No need to shuffle if the random only have one
		if(vec.size() <= 1) {
			return;
		}
		
		int start = 0;
		int end = (int) vec.size()-1;
		
		for(int i=0; i<numShuffle; i++){
			int pos1 = aurora::RandomHelper::randomBetween(start, end);
			int pos2 = aurora::RandomHelper::randomBetween(start, end);
			
			if(pos2 == pos1) {
				pos2 = (pos2 + 1) % vec.size();
			}
			
			vec.swap(pos1, pos2);
			//CC_SWAP(vec[pos1], vec[pos2], int);
		}
		
	}
}


#pragma mark - Class and Type using
void LevelLineData::parse(const char *dataLine)
{
	
	// sscanf(dataLine, <#const char *, ...#>)

	int myID, myType, myXLine, myY, myProb;
	
	sscanf(dataLine, "id=%d type=%d xLine=%d y=%d prob=%d",
			&myID, &myType, &myXLine, &myY, &myProb);

	this->id = myID;
	this->type = myType;
	this->xLine = myXLine;
	this->y = myY;
	this->probability = myProb;

}

std::string LevelLineData::toString()
{
	char result[50];
	
	sprintf(result, "id=%d type=%d xLine=%d y=%d prob=%d", id, type, xLine, y, probability);
	
	return result;
}

void LevelEnemyData::parse(const char *dataLine)
{
	int myType, myX, myY, _flipX, _flipY;
	float _scaleX, _scaleY;
	
	// type=111 x=196 y=509 flipX=1 flipY=0 scaleX=1.670000 scaleY=0.727273
	sscanf(dataLine, "type=%d x=%d y=%d flipX=%d flipY=%d scaleX=%f scaleY=%f",
			&myType, &myX, &myY, &_flipX, &_flipY, &_scaleX, &_scaleY);
	
	this->type = myType;
	this->x = myX;
	this->y = myY;
	this->flipX = _flipX == 1;
	this->flipY = _flipY == 1;
	this->scaleX = _scaleX;
	this->scaleY = _scaleY;
}

std::string LevelEnemyData::toString()
{
	char result[100];
	
	sprintf(result, "type=%d x=%d y=%d scaleX=%f scaleY=%f flipX=%d flipY=%d",
				type, x, y, scaleX, scaleY, flipX, flipY);
	
	return result;
}


void LevelItemData::parse(const char *dataLine)
{
	int myType, myX, myY;
	
	sscanf(dataLine, "type=%d x=%d y=%d", &myType, &myX, &myY);

	this->type = myType;
	this->x = myX;
	this->y = myY;
}

std::string LevelItemData::toString()
{
	char result[50];
	sprintf(result, "type=%d x=%d y=%d", type, x, y);
	return result;
}

#pragma mark - LevelNpcData

void LevelNpcData::parse(const char *dataLine)
{
	int myID, myX, myY, myDir;
	
	// npcID=1 x=120 y=770 faceDir=0 ref=100
	sscanf(dataLine, "npcID=%d x=%d y=%d faceDir=%d", &myID, &myX, &myY, &myDir);
	
	this->npcID = myID;
	this->x = myX;
	this->y = myY;
	this->faceDir = myDir;
}

std::string LevelNpcData::toString()
{
	return StringUtils::format("npcID=%d x=%d y=%d face=%d", npcID, x, y, faceDir);
}




#pragma mark - Map Data Main objects
MapLevelData::MapLevelData()
: mMapID(0)
, mIsNpcMap(false)
{
	
}
int MapLevelData::parseMainData(std::string &line, int currentStage)
{
	std::string trimLine = aurora::StringHelper::trim(line);
	
	// Detect the end condition
	if(trimLine.length() == 0) {	return kParseStageNone;		}
	
	return currentStage;
}

int MapLevelData::parseNpcData(std::string &line, int currentStage)
{
	// log("debug: parseNpcData: line=[%s]", line.c_str());
	std::string trimLine = aurora::StringHelper::trim(line);
	// Detect the end condition
	if(trimLine.length() == 0) {	return kParseStageNone;		}

	LevelNpcData *data = new LevelNpcData();
	
	data->parse(line.c_str());
	
	mNpcArray.pushBack(data);		// note: retain is called by array
	
	//
	data->release();
	

	return currentStage;
}

int MapLevelData::parseNpcSetting(std::string &line, int currentStage)
{
	std::string trimLine = aurora::StringHelper::trim(line);
	// Detect the end condition
	if(trimLine.length() == 0) {	return kParseStageNone;		}

//	isNpcMap=1
//	dropPosition=160,770
//	stopPostion=200,805
	std::vector<std::string> tokens = aurora::StringHelper::split(line, '=');
	std::string name = tokens[0];
	std::string value = tokens.size() <= 1 ? "" : tokens[1];
	
	int x, y;
	if(name == "isNpcMap") {
		mIsNpcMap = value == "1";
	} else if(name == "dropPosition") {
		sscanf(value.c_str(), "%d,%d", &x, &y);
		setDropPosition(Vec2(x, y));
	} else if(name == "stopPostion") {
		sscanf(value.c_str(), "%d,%d", &x, &y);
		setStopPosition(Vec2(x, y));
	}
	
	
	return currentStage;
}

std::vector<int> MapLevelData::getEnemyIDList()
{
	std::vector<int> enemyList;
	for(LevelEnemyData *enemyData : mEnemyArray) {
		enemyList.push_back(enemyData->type);
	}
	
	return enemyList;
}

std::vector<int> MapLevelData::getNpcIDList()
{
	std::vector<int> npcList;
	for(LevelNpcData *npcData : mNpcArray) {
		npcList.push_back(npcData->npcID);
	}
	
	return npcList;
	
}

//void MapLevelData::preloadCsb()
//{
////	Vector<LevelLineData *> mLineArray;
////	Vector<LevelEnemyData *> mEnemyArray;
////	Vector<LevelItemData *> mItemArray;
////	Vector<LevelNpcData *> mNpcArray;
//	if(mCsbSetLoaded == false) {	// Not Loaded
//		
//		for(LevelEnemyData *enemyData : mEnemyArray) {
//			int type = enemyData->type;
//			EnemyData *data = EnemyFactory::instance()->getEnemyDataByType(type);
//			log("enemyID=%d data=%s", type, (data == nullptr ? "null" : data->toString().c_str()));
//		}
//		
//		for(LevelNpcData *npcData : mNpcArray) {
//			log("npcID=%d", npcData->npcID);
//		}
//		
//		mCsbSetLoaded = true;
//	}
//	
//	
//}

int MapLevelData::parseLineData(std::string &line, int currentStage)
{
	std::string trimLine = aurora::StringHelper::trim(line);
	
	// Detect the end condition
	if(trimLine.length() == 0) {
		return kParseStageNone;
	}

	
	LevelLineData *data = new LevelLineData();
	
	data->parse(line.c_str());
	
	mLineArray.pushBack(data);		// note: retain is called by array
	
	//
	data->release();
	
	
	return currentStage;
}

//void MapLevelData::d

int MapLevelData::parseEnemyData(std::string &line, int currentStage)
{
	std::string trimLine = aurora::StringHelper::trim(line);
	
	// Detect the end condition
	if(trimLine.length() == 0) {	return kParseStageNone;		}

	
	LevelEnemyData *data = new LevelEnemyData();
	
	data->parse(line.c_str());
	
	mEnemyArray.pushBack(data);		// note: retain is called by array
	
	//
	data->release();

	
	return currentStage;
}


int MapLevelData::parseItemData(std::string &line, int currentStage)
{
	std::string trimLine = aurora::StringHelper::trim(line);
	
	// Detect the end condition
	if(trimLine.length() == 0) {	return kParseStageNone;		}
	
	LevelItemData *data = new LevelItemData();
	
	data->parse(line.c_str());
	
	mItemArray.pushBack(data);		// note: retain is called by array
	
	//
	data->release();
	
	
	return currentStage;
}




bool MapLevelData::load(const char *filename)
{
	std::string content = FileUtils::getInstance()->getStringFromFile(filename);
	
	
	// log("Content:\n%s\n", content.c_str());
	
	std::stringstream ss(content);	// turn input to stream
	std::string line;
	
	int stage = kParseStageNone;
	while(std::getline(ss, line, '\n')) {
	//	log("line [%s]", line.c_str());
		
		switch (stage) {
			case kParseStageMain	: stage = parseMainData(line, stage); break;
			case kParseStageEnemy	: stage = parseEnemyData(line, stage); break;
			case kParseStageLine	: stage = parseLineData(line, stage); break;
			case kParseStageItem	: stage = parseItemData(line, stage); break;
			case kParseStageNpc		: stage = parseNpcData(line, stage); break;
			case kParseStageNpcSetting	:
					stage = parseNpcSetting(line, stage);
					break;
			default:
				stage = determineStage(line);
				break;
		}
	}
	
	
	return true;
}

const int MapLevelData::getMapID()
{
	return mMapID;
}

void MapLevelData::getRandomLineData(Vector<LevelLineData *> &outLineArray)
{
	// Find the probability and required line of each lineType
	std::map<int, LineProbability> probabilityMap;
	
	//
	setupProbabilityMap(probabilityMap, mLineArray);
	
	outLineArray.clear();
	
	std::map<int, int> countMap;
	
	
	
	shuffleLineVector(mLineArray, 15);	// the array is small, no need shuffle much
	
	
	// Select corresponding lines
	for(int i=0; i<mLineArray.size(); i++) {
		LevelLineData *lineData = mLineArray.at(i);
		int type = lineData->type;
		
		
		
		LineProbability prob = probabilityMap[type];
		
		//log("type=%d line=[%s] prob=[prob=%d, numLine=%d reqLine=%d]", type,
		//	lineData->toString().c_str(),
		//	prob.probability, prob.numLine, prob.requireLine);
		
		
		if(prob.probability == 100) {		// the case for the green lines
			outLineArray.pushBack(lineData);
			continue;
		}
		
		// Add the line data if the count not reach max
		if(countMap[type] < prob.requireLine) {
			outLineArray.pushBack(lineData);
			countMap[type]++;
			continue;
		}
				
		// log("type=%d count=%d", type, countMap[type]);
		
	}
}
	
const Vector<LevelLineData *>& MapLevelData::getLineData()
{
	return mLineArray;
}

const Vector<LevelEnemyData *>& MapLevelData::getEnemyData()
{
	return mEnemyArray;
}

const Vector<LevelItemData *>& MapLevelData::getItemData()
{
	return mItemArray;
}

const Vector<LevelNpcData *>& MapLevelData::getNpcData()
{
	return mNpcArray;
}

bool MapLevelData::isNpcMap()
{
	return mIsNpcMap;
}

std::string MapLevelData::toString()
{
	std::string result;
	
	// Line Data
	result += StringUtils::format("lineData count=%ld\n", mLineArray.size());
	for(int i=0; i<mLineArray.size(); i++) {
		LevelLineData *data = mLineArray.at(i);
		result += data->toString().c_str();
		result += "\n";
	}
	result += "\n";
	
	// Enemy Data
	result += StringUtils::format("enemyData count=%ld\n", mEnemyArray.size());
	for(int i=0; i<mEnemyArray.size(); i++) {
		LevelEnemyData *data = mEnemyArray.at(i);
		result += data->toString().c_str();
		result += "\n";
	}
	result += "\n";

	// Item Data
	result += StringUtils::format("itemData count=%ld\n", mItemArray.size());
	for(int i=0; i<mItemArray.size(); i++) {
		LevelItemData *data = mItemArray.at(i);
		result += data->toString().c_str();
		result += "\n";
	}
	result += "\n";

	// NPC Data
	result += StringUtils::format("npcData count=%ld\n", mNpcArray.size());
	for(int i=0; i<mNpcArray.size(); i++) {
		LevelNpcData *data = mNpcArray.at(i);
		result += data->toString().c_str();
		result += "\n";
	}
	result += "\n";
	
	// NPC Setting
	result += StringUtils::format("isNpcMap=%d\n", isNpcMap());
	result += StringUtils::format("dropPosition=%s\n", POINT_TO_STR(getDropPosition()).c_str());
	result += StringUtils::format("stopPosition=%s\n", POINT_TO_STR(getStopPosition()).c_str());

	
	
	return result;
}

std::string MapLevelConfig::toString()
{
	char temp[100];
	
	sprintf(temp, "level=%d startMap=%d endMap=%d", level, mapStart, mapEnd);
	
	return temp;
}


#pragma mark - Data Cache - a singleton class to load all Level Data

MapLevelDataCache::MapLevelDataCache()
{
	
}


MapLevelDataCache *MapLevelDataCache::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new MapLevelDataCache();
	
	return sInstance;
}

bool MapLevelDataCache::loadConfig()
{
	const char *filename = kConfigFile;
	if(FileUtils::getInstance()->isFileExist(filename) == false) {
		return false;
	}
	
	std::string content = FileUtils::getInstance()->getStringFromFile(filename);
	
	
	std::stringstream ss(content);	// turn input to stream
	std::string line;
	
	mMapLevelConfig.clear();
	mMaxLevel = 0;
	
	while(std::getline(ss, line, '\n')) {
		if(line.length() == 0) {
			continue;
		}
		
		if(line[0] == '#') {		// this is a comment
			continue;
		}
		
		int level, start, end;
		sscanf(line.c_str(), "%d\t%d-%d", &level, &start, &end);
	
		MapLevelConfig *config = new MapLevelConfig();
		
		config->level = level;
		config->mapStart = start;
		config->mapEnd = end;
		
		mMapLevelConfig.insert(level, config);
		
		config->release();
		
		// update the max level
		mMaxLevel = MAX(mMaxLevel, level);
		// log("DEBUG: level=%d start=%d end=%d", level, start, end);
	}
	
	return true;
}

MapLevelData *MapLevelDataCache::loadMap(const std::string &filename)
{
	MapLevelData *mapData = new MapLevelData();
	mapData->load(filename.c_str());
	
	mapData->autorelease();
	
	return mapData;
}

bool MapLevelDataCache::loadNpcMap(int mapID)
{
	std::string filename = StringUtils::format("mapData/npc%d.dat", mapID);
	if(FileUtils::getInstance()->isFileExist(filename) == false) {
		return false;
	}
	
	// Load the Map
	MapLevelData *mapData = new MapLevelData();
	bool isLoaded = mapData->load(filename.c_str());
	
	if(isLoaded) {
		mNpcMapCache.insert(mapID, mapData);
	}
	
	// clean up
	mapData->release();
	
	return true;
}

bool MapLevelDataCache::loadMap(int mapID)
{
	char filename[125];
	sprintf(filename, "mapData/map%d.dat", mapID);
	
	if(FileUtils::getInstance()->isFileExist(filename) == false) {
		return false;
	}
	
	// Load the Map
	MapLevelData *mapData = new MapLevelData();
	bool isLoaded = mapData->load(filename);
	
	if(isLoaded) {
		mMapCache.insert(mapID, mapData);
	}
	
	// clean up
	mapData->release();
	
	return true;
}


std::vector<int> MapLevelDataCache::getAvailableMaps() const
{
	return mMapCache.keys();
}

void MapLevelDataCache::loadAllMap()
{
	//char filename[125];
	
	mMapCache.clear();
	
	
	std::vector<int> mapList;
	MapLevelManager::instance()->getMapList(mapList);
	
	// Special Maps
	mapList.push_back(0);
	mapList.push_back(10000);
	mapList.push_back(10001);
	mapList.push_back(10002);
	mapList.push_back(10003);
	mapList.push_back(10004);
	mapList.push_back(10005);
	mapList.push_back(11011);
    mapList.push_back(99999);
	
	// Tutorial Map
	for(int mapID=20000; mapID<=20007; mapID++) {
		mapList.push_back(mapID);
	}
	
	
	for(int i=0; i<mapList.size(); i++) {
		int mapID = mapList[i];
		
		bool isOkay = loadMap(mapID);
		if(! isOkay) {
			log("loadAllMap: fail to load %d", mapID);
		}
	}
	
	
	std::vector<int> npcMapList;
	npcMapList.push_back(100);
	npcMapList.push_back(101);
	npcMapList.push_back(102);
	npcMapList.push_back(200);
	npcMapList.push_back(300);
	npcMapList.push_back(400);
	npcMapList.push_back(500);

	for(int mapID : npcMapList)
	{
		bool isOkay = loadNpcMap(mapID);
		if(! isOkay) {
			log("loadAllMap: fail to load npcMap %d", mapID);
		}
	}
	
//	
//	loadMap(0);		// sample map for testing and default
//	loadMap(20000);	// Another map for testing
//	loadMap(10001);
//	
//	
//	Map<int, MapLevelConfig *>::iterator it = mMapLevelConfig.begin();
//	while(it != mMapLevelConfig.end()) {
//		MapLevelConfig *config = (*it).second;	// dereference the pointer
//		int start = config->mapStart;
//		int end = config->mapEnd;
//		
//		for(int mapID=start; mapID<=end; mapID++) {
//			loadMap(mapID);
//		}
//		it++;
//	}
}

void MapLevelDataCache::loadGameMap(const std::vector<int> &inMapList)
{
	std::vector<int> mapList = inMapList;
	
	for(int mapID : mapList) {
		//log("DEBUG: loadGameMap: map=%d", mapID);
		if(getMap(mapID) != nullptr) {
			continue;
		}
		
		bool isOkay = loadMap(mapID);
		if(! isOkay) {
			log("loadGameMap: fail to load %d", mapID);
		}
	}
}

void MapLevelDataCache::loadNpcMap(const std::vector<int> &inMapList)
{
	std::vector<int> npcList = inMapList;
	for(int mapID : npcList) {
		log("DEBUG: loadNpcMap: map=%d", mapID);
		if(getNpcMap(mapID) != nullptr) {
			continue;
		}
		
		bool isOkay = loadNpcMap(mapID);
		if(! isOkay) {
			log("loadGameMap: fail to load %d", mapID);
		}
	}
}

MapLevelData *MapLevelDataCache::getNpcMap(int mapID)
{
	if(mMapCache.empty()) {
		return nullptr;
	}
	
	
	return mNpcMapCache.at(mapID);
}

MapLevelData *MapLevelDataCache::getMap(int mapID)
{
	
	if(mMapCache.empty()) {
		return nullptr;
	}
	
	
	return mMapCache.at(mapID);
}

bool MapLevelDataCache::hasLevel(int level)
{
	return mMapLevelConfig.find(level) != mMapLevelConfig.end();
}

bool MapLevelDataCache::hasMap(int mapID)
{
	return mMapCache.find(mapID) != mMapCache.end();
}


const MapLevelConfig *MapLevelDataCache::getMapLevelConfig(int level) const
{
	if(mMapLevelConfig.empty()) {
		return nullptr;
	}
	
	
	return mMapLevelConfig.at(level);
}

int MapLevelDataCache::getMaxMapLevel()
{
	return mMaxLevel;
}

std::string MapLevelDataCache::infoConfig()
{
	std::string result;
	
	Map<int, MapLevelConfig *>::iterator it = mMapLevelConfig.begin();
	while(it != mMapLevelConfig.end()) {
		MapLevelConfig *config = (*it).second;	// dereference the pointer
		
		//log("map=%s", config->toString().c_str());
		result += config->toString();
		result += "\n";
		
		it++;
	}
	
//	const_iterator
	//MapRef:: begin = mMapLevelConfig.begin();
	
	return result;
}


#pragma mark - MapSection
int MapSection::getRandomMap()
{
	int mapSelected = aurora::RandomHelper::randomBetween(mMapStart, mMapEnd);
	
	// TODO: Check again the availability
	
	return mapSelected;
}


std::string MapSection::toString()
{
	std::string result = "";
	
	result += "id=" + INT_TO_STR(getSectionID());
	result += " start=" + INT_TO_STR(mMapStart);
	result += " end=" + INT_TO_STR(mMapEnd);
	
	return result;
}

#pragma mark - MapCycle
std::string MapCycle::toString()
{
	std::string result = "";
	
	result += "name=" + getName();
	result += " initialSpeed=" + INT_TO_STR(mInitialSpeed);
	result += " increaseSpeed=" + INT_TO_STR(mIncreaseSpeed);
	result += " maxSpeed=" + INT_TO_STR(mMaxSpeed);
	result += " totalDistance=" + INT_TO_STR(getTotalDistance());
	result += " section=[" + VECTOR_TO_STR(mSectionList) + "]";
	
	return result;
}

MapCycle::MapCycle()
: mTotalDistance(0)
, mSectionList()
{
	
}

int MapCycle::getTotalDistance()
{
	return mTotalDistance;
}

int MapCycle::getRandomMapByDistance(int distance)	// input is relative distance to the cycle star
{
	int index = distance / kMapHeight;
	
	int sectionID = getSectionIDByIndex(index);
	
	MapSection *section = MapLevelManager::instance()->getSectionByID(sectionID);
	
	return section->getRandomMap();
}

int MapCycle::getSectionIDByIndex(int index)
{
	if(mSectionList.size() == 0) {
		return 0;	// special map
	}
	
	// correction
	if(index < 0) {
		index = 0;
	} else if(index >= mSectionList.size()) {
		index = (int) mSectionList.size() - 1;
	}
	
	return mSectionList[index];
}

void MapCycle::setSection(const std::vector<int> &sections)
{
	mSectionList.clear();
	
	int totalDistance  = 0;
	for(int i=0; i<sections.size(); i++) {
		mSectionList.push_back(sections[i]);
		totalDistance += kMapHeight;
	}
	
	// update the total distance
	mTotalDistance = totalDistance;
}


#pragma mark - MapLevelManager
MapLevelManager::MapLevelManager()
: mPlanetCycleMap()
, mSelectedPlanet(-1)
, mOffsetSpeed(0)
, mTutorialMap(0)
, mTestMapList()
{
	mCycleInfo = new MapCycleInfo();
}

MapLevelManager::~MapLevelManager()
{
	delete mCycleInfo;
}

void MapLevelManager::getMapList(std::vector<int> &outMapList)
{
	
	for(auto &keyval : mSectionMap) {
		int sectionID = keyval.first;
		MapSection *section = keyval.second;
		
		int start = section->getMapStart();
		int end = section->getMapEnd();
		
		for(int map=start; map <= end; map++) {
			//log("DEBUG: MapSection=%s MAP=%d", section->toString().c_str(), map);
			outMapList.push_back(map);
		}
	}
}

MapLevelManager *MapLevelManager::instance()
{
	if(sMapLevelManager == nullptr) {
		sMapLevelManager = new MapLevelManager();
	}
	
	return sMapLevelManager;
}



bool MapLevelManager::loadConfig()
{
	std::string jsonFile = "level.json";
	std::string content = FileUtils::getInstance()->getStringFromFile(jsonFile);
	
	//
	rapidjson::Document jsonDoc;
	jsonDoc.Parse(content.c_str());
	
	if (jsonDoc.HasParseError())
	{
		CCLOG("loadConfig: json error. %d %s\n", jsonDoc.GetParseError(), jsonFile.c_str());
		return false;
	}
	
	const rapidjson::Value &version = jsonDoc["version"];
	setVersion(version.GetString());
	
	// Parsing tutorial Map
	setTutorialMap(aurora::JSONHelper::getInt(jsonDoc, "tutorialMap", 10001));
	
	// Parse the test map
	std::vector<int> mapList;
	aurora::JSONHelper::getJsonIntArray(jsonDoc, "testMap", mapList);
	for(int mapID : mapList) {
		mTestMapList.insert(mapID);
	}
	
	// Parsing the section
	const rapidjson::Value &sectionList = jsonDoc["sectionList"];
	
	mSectionMap.clear();
	for (rapidjson::SizeType i = 0; i < sectionList.Size(); i++)
	{
		MapSection *section = parseMapSection(sectionList[i]);
		mSectionMap.insert(section->getSectionID(), section);
	}

	
	// Parsing the Cycle
	const rapidjson::Value &cycleList = jsonDoc["cycleList"];
	
	mCycleMap.clear();
	int cycle = 0;
	for (rapidjson::SizeType i = 0; i < cycleList.Size(); i++)
	{
		MapCycle *cycleObject = parseMapCycle(cycleList[i]);
		mCycleMap.insert(cycle, cycleObject);
		
		cycle++;
	}
	
	// Parse Planet Data
	const rapidjson::Value &planetData = jsonDoc["planetMap"];
	parsePlanetDataMap(planetData);
	
	
	// Set the planet
	setMapCycleForPlanet(1);	// default
//	
//	// Loading the Section List
//	for (rapidjson::Value::ConstMemberIterator itr = jsonDoc.MemberBegin();
//		 itr != jsonDoc.MemberEnd();
//		 ++itr)
//	{
//		const rapidjson::Value &object = jsonDoc[itr->name.GetString()];
//		
//	
//	
//	// Loading the Cycle List
	return true;
}

std::vector<int> MapLevelManager::getTestMapList()
{
	std::vector<int> result;
	
	for(int mapID : mTestMapList) {
		result.push_back(mapID);
	}

	return result;
}

void MapLevelManager::parsePlanetDataMap(const rapidjson::Value &jsonValue)
{
	//object.
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr)
	{
		std::string key = itr->name.GetString();
		//const rapidjson::Value &object = jsonValue[key.c_str()];
		//int value = object.GetInt();
		
		int planetID = 0;
		sscanf(key.c_str(), "planet_%03d", &planetID);
		log("debug: key=%s planetID=%d", key.c_str(), planetID);
		
		const rapidjson::Value &object = jsonValue[key.c_str()];
		PlanetMapCycle *planetMap = parsePlanetMapCycle(planetID, object);
		log("debug:\n%s\n", planetMap->info().c_str());
		
		addPlantMapCycle(planetMap);
	}
}

PlanetMapCycle *MapLevelManager::parsePlanetMapCycle(int planetID, const rapidjson::Value &object)
{
	PlanetMapCycle *planetMap = PlanetMapCycle::create();
	planetMap->setPlanetID(planetID);
	planetMap->clearCycleList();

	for (rapidjson::SizeType i = 0; i < object.Size(); i++)
	{
		MapCycle *cycleObject = parseMapCycle(object[i]);
		planetMap->addCycle(cycleObject);
	}

	return planetMap;
}

MapSection *MapLevelManager::parseMapSection(const rapidjson::Value &object)
{
	MapSection *section = MapSection::create();
	
	section->setSectionID(aurora::JSONHelper::getInt(object, "id", 0));
	section->setMapStart(aurora::JSONHelper::getInt(object, "start", 1));
	section->setMapEnd(aurora::JSONHelper::getInt(object, "end", 5));
	
	log("MapSection: %s", section->toString().c_str());
	
	return section;
}

MapCycle *MapLevelManager::parseMapCycle(const rapidjson::Value &object)
{
	MapCycle *cycle = MapCycle::create();
	
	cycle->setName(aurora::JSONHelper::getString(object, "name", "unknown"));
	
	std::vector<int> sections;
	
	// Speed Information
	cycle->setIncreaseSpeed(aurora::JSONHelper::getInt(object, "increaseSpeed", 0));
	cycle->setInitialSpeed(aurora::JSONHelper::getInt(object, "initialSpeed", kInitialSpeed));
	cycle->setMaxSpeed(aurora::JSONHelper::getInt(object, "maxSpeed", kMaxSpeed));
	
	// Section
	aurora::JSONHelper::getJsonIntArray(object, "sectionList", sections);
	
	cycle->setSection(sections);
	
//	
//	section->setMapStart(aurora::JSONHelper::getInt(object, "start", 1));
//	section->setMapEnd(aurora::JSONHelper::getInt(object, "end", 5));
	
	return cycle;
}

void MapLevelManager::addPlantMapCycle(PlanetMapCycle *planetCycle)
{
	if(planetCycle == nullptr) {
		return;
	}
	
	mPlanetCycleMap.insert(planetCycle->getPlanetID(), planetCycle);
}

PlanetMapCycle *MapLevelManager::getPlanetCycle(int planetID)
{
	return mPlanetCycleMap.at(planetID);
}

std::string MapLevelManager::infoCycleData()
{
	std::string result = "";
	
	result += "Cycle Count: " + INT_TO_STR((int) mCycleMap.size());
	result += "\n";
	
	for(auto &keyval : mCycleMap) {
		int cycleID = keyval.first;
		MapCycle *cycle = keyval.second;
		
		result += "id=" + INT_TO_STR(cycleID);
		result += " data=[" + cycle->toString() + "]";
		result += "\n";
	}
	
	return result;
}

MapSection *MapLevelManager::getSectionByID(int sectionID)
{
	return mSectionMap.at(sectionID);
}

MapCycle *MapLevelManager::getMapCycle(int cycle)
{
	// correction
	if(cycle < 0) {
		cycle = 0;
	} else if(cycle >= mCycleMap.size()) {
		cycle = (int) mCycleMap.size() - 1;
	}
	
	return mCycleMap.at(cycle);
}

void MapLevelManager::setup(int initialDistance)
{
	mInitialDistance = initialDistance;
	setGeneratedDistance(mInitialDistance);		// Should be same
	
	// TODO set cycle data
	int cycle = 0;
	updateCycleInfo(cycle, initialDistance);
	
	
	// Set the initial speed
	setInitialSpeed(GameWorld::instance()->getPlayer());
}




void MapLevelManager::setInitialSpeed(Player *player)
{
	if(player == nullptr) {
		return;
	}
	
	//
	MapCycle *cycleData = getMapCycle(0);
	if(cycleData == nullptr) {
		return;
	}
	player->setSpeed(cycleData->getInitialSpeed()+mOffsetSpeed);
}


void MapLevelManager::updateMapCycle(int checkDistance)	// the distance to be generate new map
{
	if(mCycleInfo->isInRange(checkDistance)) {
		return;	// no need to update
	}
	//log("New Cycle: checkDistance=%d", checkDistance);
	setNextMapCycle(); // suppose will fall in the range
}

//int MapLevelManager::getMapForDistance(int distance)
//{
//	
//}
//

int MapLevelManager::getCurrentCycle()
{
	return mCycleInfo->getCycle();
}

void MapLevelManager::setNextMapCycle()
{
	int cycle = mCycleInfo->getCycle() + 1;
	int start = mCycleInfo->getEndDistance();
	
	updateCycleInfo(cycle, start);
	
	if(cycle > 0) {
//		updatePlayerSpeed(GameWorld::instance()->getPlayer());
	}
}

void MapLevelManager::updateCycleInfo(int cycle, int startDistance)
{
	MapCycle *cycleData = getMapCycle(cycle);
	if(cycleData == nullptr) {
		log("updateCycleInfo: cycleData is nullptr");
		return;
	}
	
	//
	int start = startDistance;
	int end = start + cycleData->getTotalDistance();
	
	mCycleInfo->setCycle(cycle);
	mCycleInfo->setStartDistance(start);
	mCycleInfo->setEndDistance(end);
	mCycleInfo->setHasSpeedUp(false);	
	mCycleInfo->setSpeedUpValue(cycleData->getIncreaseSpeed());
	mCycleInfo->setSpeedMaxValue(cycleData->getMaxSpeed());
}

bool MapLevelManager::needNewMap(int distance)
{
	if(distance + kMapHeight > mGeneratedDistance) {
		return true;
	}
	
	return false;
}

MapCycleInfo *MapLevelManager::getCurrentCycleInfo()
{
	return mCycleInfo;
}



int MapLevelManager::getMapForDistance(int distance)
{
	
}

bool MapLevelManager::needSpeedUp(float playerDistance)
{
	if(mCycleInfo->getHasSpeedUp()) {
		return false;
	}
	
	if(playerDistance >= mCycleInfo->getStartDistance()) {
		return true;
	}
	return false;
}

void MapLevelManager::updatePlayerSpeed(Player *player)
{
	if(mCycleInfo == nullptr) {
		return;
	}
	
	int incValue = mCycleInfo->getSpeedUpValue();
	int maxValue = mCycleInfo->getSpeedMaxValue();
	
	player->increaseSpeed(incValue, maxValue);
	
	mCycleInfo->setHasSpeedUp(true);
	
}

void MapLevelManager::setMapCycleForPlanet(int planetID)
{
	if(mSelectedPlanet == planetID) {
		return;	// nothing todo
	}
	
	// Ready the data
	PlanetMapCycle *planetData = getPlanetCycle(planetID);
	if(planetData == nullptr) {
		log("MapLevelManager: cycle is nullptr. planetID=%d", planetID);
		return;
	}
	
	// Change the planet
	mSelectedPlanet = planetID;
	
	mCycleMap.clear();
	
	int idx = 0;
	for(MapCycle *cycleData : planetData->getCycleList())
	{
		mCycleMap.insert(idx, cycleData);
		idx++;
	}
}


std::string MapLevelManager::infoMap()
{
	std::string result = StringUtils::format("TutorialMap: %d\n", mTutorialMap);
	
	result += "TestMap:";
	for(int mapID : mTestMapList) {
		result += StringUtils::format(" %d", mapID);
	}
	result += "\n";
	
	return result;
}

std::string MapLevelManager::infoSectionData()
{
	
	std::string result = "";
	
	result += "Section Count: " + INT_TO_STR((int) mSectionMap.size());
	result += "\n";
	
	for(auto &keyval : mSectionMap) {
		int sectionID = keyval.first;
		MapSection *section = keyval.second;
		
		result += "id=" + INT_TO_STR(sectionID);
		result += " data=[" + section->toString() + "]";
		result += "\n";
	}
	

	
	return result;
}


#pragma mark - MapCycleInfo
bool MapCycleInfo::isInRange(int distance)
{
	return distance >= mStartDistance && distance < mEndDistance;
}

int MapCycleInfo::getMapID(int distance)
{
	// Determine the section Index
	int offsetDistance = distance - mStartDistance;
	
	MapCycle *cycleData = MapLevelManager::instance()->getMapCycle(mCycle);
	if(cycleData == nullptr) {
		return 0;
	}
	
	return cycleData->getRandomMapByDistance(offsetDistance);
}


std::string MapCycleInfo::toString()
{
	std::string result = "";
	
	int rangeLen = mEndDistance - mStartDistance;
	
	result += "cycle=" + INT_TO_STR(mCycle);
	result += " range=[" + INT_TO_STR(mStartDistance);
	result += "," + INT_TO_STR(mEndDistance) + "]";
	result += " rangeLen=" + INT_TO_STR(rangeLen);
	result += " speedUpValue=" + INT_TO_STR(getSpeedUpValue());
	result += " hasSpeedUp=" + BOOL_TO_STR(getHasSpeedUp());
	
	return result;
}


#pragma mark - Planet Level Data
PlanetMapCycle::PlanetMapCycle()
: Ref()
, mPlanetID(0)
, mCycleList()
{
	
}

PlanetMapCycle::~PlanetMapCycle()
{
	mCycleList.clear();
}

const Vector<MapCycle *> &PlanetMapCycle::getCycleList()
{
	return mCycleList;
}

void PlanetMapCycle::clearCycleList()
{
	mCycleList.clear();
}

void PlanetMapCycle::addCycle(MapCycle *cycle)
{
	mCycleList.pushBack(cycle);
}
	
	
std::string PlanetMapCycle::info()
{
	std::string result = StringUtils::format("Planet %d\n", mPlanetID);
	
	for(MapCycle *cycle : mCycleList) {
		result += cycle->toString() + "\n";
	}
	
	return result;
}
	

