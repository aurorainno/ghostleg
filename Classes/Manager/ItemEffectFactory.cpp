//
//  ItemManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/6/2016.
//
//

#include "ItemEffectFactory.h"
#include "InvulnerableEffect.h"
#include "MissileEffect.h"
#include "GrabStarEffect.h"
#include "DoubleCoinsEffect.h"
#include "TimeStopEffect.h"
#include "ShieldEffect.h"
#include "StarMagnetEffect.h"
#include "SnowBallEffect.h"
#include "SpeedUpEffect.h"
#include "CashOutEffect.h"

ItemEffect *ItemEffectFactory::createEffectByType(ItemEffect::Type type)
{
	switch(type) {
		case ItemEffect::Type::ItemEffectGrabStar:
			return GrabStarEffect::create();
			
		case ItemEffect::Type::ItemEffectShield:
			return ShieldEffect::create();
		
		case ItemEffect::Type::ItemEffectInvulnerable:
			return InvulnerableEffect::create();
			
		case ItemEffect::Type::ItemEffectMissile:
			return MissileEffect::create();
			
		case ItemEffect::Type::ItemEffectTimeStop:
			return TimeStopEffect::create();
			
		case ItemEffect::Type::ItemEffectDoubleCoins:
			return DoubleCoinsEffect::create();
			
		case ItemEffect::Type::ItemEffectStarMagnet:
			return StarMagnetEffect::create();
			
		case ItemEffect::Type::ItemEffectSnowball:
			return SnowBallEffect::create();
			
        case ItemEffect::Type::ItemEffectSpeedUp:
            return SpeedUpEffect::create();
            
        case ItemEffect::Type::ItemEffectCashOut:
            return CashOutEffect::create();
        
        
        
            
		default:
			return nullptr;
	}
}
