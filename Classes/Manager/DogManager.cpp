

//
//  DogManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 4/10/2016.
//
//

#include "DogManager.h"
#include "DogData.h"
#include "GameManager.h"
#include "SuitManager.h"
#include "MasteryManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "JSONHelper.h"
#include "StringHelper.h"
#include "PlayerRecord.h"
#include "PlanetManager.h"
#include "PlanetData.h"
#include "RewardManager.h"

#define kKeyUnlockDog	"ghostleg.unlockDog"
#define kKeyNewDog		"ghostleg.newDog"
#define kKeyDogSuggest	"ghostleg.lastSuggest"				// the suggestion point

const int kUnlockablePlanetMax = 50;

static DogManager *sInstance = nullptr;

const int kInitialSuggestCount = 10;
const int kSuggestCountStep = 8;

DogManager* DogManager::instance(){
	if(sInstance != nullptr) {
		return sInstance;
	}else{
		sInstance = new DogManager();
		return sInstance;
	}
}



DogManager::DogManager()
: mDogDataMap()
, mSelectedDog(1)
, mLastSuggestPos(-1)
{
	
}

void DogManager::setup()
{
	
//	setupMock();
    loadData();
    loadUnlockData();
    loadNewDogList();
	loadSuggestRecord();
    
	// Setup the default dog
	int selectedDog = GameManager::instance()->getUserData()->getSelectedDog();
	selectDog(selectedDog, false);	// false so that no save 
}

void DogManager::loadData()			// Load Data from JSON
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/dog.json");
    
    //log("product json content:\n%s", content.c_str());
    rapidjson::Document dogJSON;
    dogJSON.Parse(content.c_str());
    if (dogJSON.HasParseError())
    {
        CCLOG("GetParseError %d\n", dogJSON.GetParseError());
        return;
    }
    
    for(int i=0; i<dogJSON.Size(); i++)
    {
        const rapidjson::Value& currentDog = dogJSON[i];
        DogData *data = generateSingleDogFromJSON(currentDog);
        addDogData(data);
    }

    log("%s",infoDogData().c_str());

}

DogData* DogManager::generateSingleDogFromJSON(const rapidjson::Value &dogJSON){
    
    DogData *data = DogData::create();
    std::map<int, int> masteryList;
    std::map<std::string, int> unlocks;
    masteryList.clear();
    unlocks.clear();
    
    data->setName(aurora::JSONHelper::getString(dogJSON,"name" ,""));
	data->setInfo(aurora::JSONHelper::getString(dogJSON,"info" ,"todo: dog info"));
	data->setUnlockInfo(aurora::JSONHelper::getString(dogJSON,"unlockInfo" ,"todo: unlock info"));
	
    data->setDogID(aurora::JSONHelper::getInt(dogJSON, "dogID", -1));
    data->setIcon(aurora::JSONHelper::getInt(dogJSON, "icon", -1));
    data->setAnimeID(aurora::JSONHelper::getInt(dogJSON, "animeID", -1));
    data->setSuit(aurora::JSONHelper::getInt(dogJSON, "suit", 0));
    data->setPrice(aurora::JSONHelper::getInt(dogJSON, "price", 0));
	
	MoneyType moneyType = (MoneyType) aurora::JSONHelper::getInt(dogJSON, "moneyType", MoneyTypeStar);
	data->setMoneyType(moneyType);
    
    aurora::JSONHelper::getJsonIntMap(dogJSON, "mastery", masteryList);
    data->setMasteryList(masteryList);
    aurora::JSONHelper::getJsonStrIntMap(dogJSON, "unlock", unlocks);
    data->setUnlockCondList(unlocks);
	
	
	// Rarity
	std::string rarityStr = aurora::JSONHelper::getString(dogJSON, "rarity");
	data->setRarity(DogData::parseRarity(rarityStr));
	
	//dog->setR
	std::string rankStr = aurora::JSONHelper::getString(dogJSON, "rank");
	data->setRank(DogData::parseRank(rankStr));
	
	
	// Ability List
	std::vector<std::string> abilityStrList;
	aurora::JSONHelper::getJsonStrArray(dogJSON, "abilityList", abilityStrList);
	data->parseAbilityList(abilityStrList);
	
	
	return data;
}

void DogManager::setupMock()
{
	std::map<int, int> masteryList;
	std::map<std::string, int> unlocks;
	DogData *data;
	
	int numDog = 6;
	
	int dogID = 1;
	
	for(int i=0; i<numDog; i++) {
		int suitID = (i % 6) + 1;
		int suitLevel = (i / 6) + 1;
		
		data = DogData::create();
		data->setName(StringUtils::format("Dog %d", dogID));
		data->setDogID(dogID);
		data->setIcon(suitID);
		data->setAnimeID(suitID-1);
		data->setSuit(suitID * 100 + suitLevel);
		
		masteryList.clear();
		data->setMasteryList(masteryList);
		
		unlocks.clear();
		data->setUnlockCondList(unlocks);
		
		addDogData(data);
		
		dogID++;
	}
	
	DogData *dog2 = getDogData(2);
	masteryList.clear();
	masteryList[1] = 2;
	masteryList[2] = 2;
	dog2->setMasteryList(masteryList);
	
//	
//	// First Dog
//	
//	
//	
//	
//	
//	// 2nd Dog
//	data = DogData::create();
//	data->setName("Iron");
//	data->setDogID(2);
//	data->setIcon(2);
//	data->setAnimeID(2);
//	data->setSuit(202);
//	
//	masteryList.clear();
//	masteryList[1] = 2;
//	masteryList[2] = 2;
//	data->setMasteryList(masteryList);
//	
//	unlocks.clear();
//	unlocks["bestDistance"] = 100;
//	unlocks["starTotal"] = 50;
//	
//	addDogData(data);
	//data->
}


Vector<DogData*> DogManager::getDogDataList()
{
    Map<int, DogData *>::iterator it;
    Vector<DogData*> result{};
    for(it=mDogDataMap.begin();it!=mDogDataMap.end();it++){
        result.pushBack(it->second);
    }
    return result;
}

void DogManager::addDogData(DogData *dogData)
{
	if(dogData == nullptr) {
		log("DogManager: dogData is null");
		return;
	}
	
	mDogDataMap.insert(dogData->getDogID(), dogData);
}

std::string DogManager::getDogName(int dogID)
{
	if(dogID == 0) {
		return "";
	}
	DogData *data = getDogData(dogID);
	
	return data == nullptr ? "" : data->getName();
}

std::string DogManager::infoDogData()
{
	std::string result = StringUtils::format("Dog Count: %ld\n", mDogDataMap.size());

	for(Map<int, DogData *>::iterator it=mDogDataMap.begin(); it != mDogDataMap.end(); it++) {
		int dogID = it->first;
		DogData *data = it->second;
		
		result += StringUtils::format("%d: %s\n", dogID, data->toString().c_str());
		result += "  Ability:\n" + data->infoAbilityList() + "\n";
	}
	
	return result;
}

DogData *DogManager::getDogData(int dogID)
{
	return mDogDataMap.at(dogID);
}


void DogManager::selectDog(int dogID, bool saveChange)
{
	
	DogData *dogData = getDogData(dogID);
	if(dogData == nullptr) {
		log("DogManager.selectDog: dogData is null. dogID=%d", dogID);
		return;
	}
	
	mSelectedDog = dogID;
	
	if(saveChange) {	// usually do this
		GameManager::instance()->getUserData()->setSelectedDog(mSelectedDog);
	}

	// Mark the selectDog in UserGameData
	
	
	// Define the suit using
	GameManager::instance()->getSuitManager()->setDogSuitAndLevel(dogData->getSuitID(), dogData->getSuitLevel());
}



int DogManager::getSelectedDogAnimeID()
{
	DogData *dogData = getDogData(mSelectedDog);
	if(dogData == nullptr) {
		log("DogManager.selectDog: dogData is null. dogID=%d", mSelectedDog);
		return 1;
	}
	
	int animeID = dogData->getAnimeID();
	if(animeID > 4) {
		animeID = 4;
	}
	
	return animeID;
}


int DogManager::getSelectedDogID()
{
	return mSelectedDog;
}

DogData *DogManager::getSelectedDogData()
{
	return getDogData(getSelectedDogID());
}


int DogManager::getTotalDogCount()
{
	return (int) mDogDataMap.size();
}

int DogManager::getTotalUnlockedDog()
{
    int count = 0;
    for(Map<int, DogData *>::iterator it=mDogDataMap.begin(); it != mDogDataMap.end(); it++) {
        int dogID = it->first;
        
        if(mDogUnlockMap[dogID] == UnlockStatus::StatusUnlocked){
            count++;
        }
    }
    return count;
}


#pragma mark - Dog Unlock Status
void DogManager::loadUnlockData()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("DogManager::loadUnlockData: userDefault not available");
        return;
    }
    
    mDogUnlockMap.clear();
    
    std::string content = userData->getStringForKey(kKeyUnlockDog, "");
    
    if(content == "") {
        mDogUnlockMap[1] = StatusUnlocked;	// default dog alway unlocked
        return;
    }
    
    std::stringstream ss(content);
    std::string to;
    
    while(std::getline(ss, to, '\n')){
        int dogID;
        int unlockStatus;
        
        sscanf(to.c_str(), "dogID=%d unlockStatus=%d", &dogID, &unlockStatus);
        
        mDogUnlockMap[dogID] = (UnlockStatus)unlockStatus;
    }

}


void DogManager::loadNewDogList()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("DogManager::loadNewDogList: userDefault not available");
        return;
    }
    
    mNewDogList.clear();
    
    std::string content = userData->getStringForKey(kKeyNewDog, "");
    
    if(content == "") {
        return;
    }
    
    std::stringstream ss(content);
    std::string to;
    
    while(std::getline(ss, to, '\n')){
        int dogID;
        
        sscanf(to.c_str(), "dogID=%d", &dogID);
        
        mNewDogList.push_back(dogID);
    }
}

void DogManager::saveNewDogList()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("DogManager::saveUnlockData: userDefault not available");
        return;
    }
    
    std::string newDogList = "";
    for (int dogID : mNewDogList) {
        int id = dogID;
        newDogList += "dogID=" + INT_TO_STR(id);
        newDogList += "\n";
    }
    
    userData->setStringForKey(kKeyNewDog, newDogList);
}

void DogManager::saveUnlockData()
{
    UserDefault *userData = UserDefault::getInstance();
    if(userData == nullptr) {
        log("DogManager::saveUnlockData: userDefault not available");
        return;
    }
    
    std::string unlockData = "";
    for (std::map<int, UnlockStatus>::iterator it=mDogUnlockMap.begin(); it!=mDogUnlockMap.end(); ++it) {
        int id = it->first;
        UnlockStatus unlocked = it->second;
        
        unlockData += "dogID=" + INT_TO_STR(id);
        unlockData += " unlockStatus=" + INT_TO_STR((int)unlocked);
        unlockData += "\n";
    }
    
    userData->setStringForKey(kKeyUnlockDog, unlockData);
}

void DogManager::resetAllDog()	// remove all except the dog1 (default dog)
{
    mDogUnlockMap.clear();
    mDogUnlockMap[1] = StatusUnlocked;
    saveUnlockData();
    
    mNewDogList.clear();
    saveNewDogList();
}

void DogManager::unlockAllDog()	// unlock all dog
{
    for(Map<int, DogData*>::iterator it = mDogDataMap.begin(); it!=mDogDataMap.end(); ++it){
        int dogID = it->first;
        mDogUnlockMap[dogID] = StatusUnlocked;
    }
    saveUnlockData();
}

bool DogManager::isUnlockable(int dogID)		// check to see the dog can be unlock or not
{
	int planetID = PlanetManager::instance()->getPlanetWithDog(dogID);
	
	return planetID <= kUnlockablePlanetMax;
}


bool DogManager::isLocked(int dogID)		// check to see a dog is unlocked or not
{
    if(mDogUnlockMap[dogID] == StatusLocked){
        return true;
    }else{
        return false;
    }
}

DogManager::UnlockStatus DogManager::getUnlockStatus(int dogID)
{
    return mDogUnlockMap[dogID];
}


void DogManager::unlock(int dogID)			// unlock the given dog
{
    mDogUnlockMap[dogID] = StatusUnlocked;
    saveUnlockData();
    mNewDogList.push_back(dogID);
    saveNewDogList();
}


void DogManager::setReadyToUnlock(int dogID)
{
    mDogUnlockMap[dogID] = StatusReadyToUnlock;
    saveUnlockData();
}

void DogManager::removeNewDog(int dogID)
{
    std::vector<int>::iterator it;
    
    it = find (mNewDogList.begin(), mNewDogList.end(), dogID);
    if (it != mNewDogList.end()){
        mNewDogList.erase(it);
    }
    
    saveNewDogList();
}

bool DogManager::isNewDog(int dogID)
{
    std::vector<int>::iterator it;
    
    it = find (mNewDogList.begin(), mNewDogList.end(), dogID);
    if (it != mNewDogList.end()){
        return true;
    }else{
        return false;
    }
}

bool DogManager::hasNewDog()
{
    return mNewDogList.size();
}


#pragma mark - Dog New Unlock Finder
std::vector<int> DogManager::getNewUnlockedDogs()
{
	std::vector<int> result;
    result.clear();
    
    PlayerRecord *record = GameManager::instance()->getPlayerRecord();
    int bestDistance = record->getRecordData(PlayerRecord::KeyBestDistance);
    int totalStar = record->getRecordData(PlayerRecord::KeyStarTotal);
   
    log("DogManager::getNewUnlockedDogs:playerRecord bestDistance:%d,totalStar:%d",bestDistance,totalStar);
    
    std::map<std::string, int> unlockCondMap;

    for(Map<int, DogData *>::iterator it=mDogDataMap.begin(); it != mDogDataMap.end(); it++) {
        int dogID = it->first;
        DogData *data = it->second;
        int valueNeed, valueNow;
        bool isRecordBroken = true;
		
		if(isUnlockable(dogID) == false) {
			continue;
		}
		
        
        if(isLocked(dogID)){
			//log("check: dogID=%d", )
            std::map<std::string,int> condList = data->getUnlockCondList();
            
            for(std::map<std::string, int>::iterator it=condList.begin(); it != condList.end(); it++) {
                std::string key = it->first;
                valueNeed = it->second;
                
                valueNow = GameManager::instance()->getPlayerRecord()->getRecordData(key);
                
                if(valueNow < valueNeed) {		// early exit if the condition not match
                    isRecordBroken = false;
                    break;
                }
            }
            
            if(isRecordBroken){
                unlock(dogID);
                //setReadyToUnlock(dogID);
                result.push_back(dogID);
            }
//            int dogBestDistance = unlockCondMap["bestDistance"];
//            int dogStarTotal = unlockCondMap["starTotal"];
//            
//            log("DogManager::getNewUnlockedDogs:dog%02d bestDistance:%d,totalStar:%d",dogID,dogBestDistance,dogStarTotal);
//            
//            if(totalStar >= dogStarTotal && bestDistance >= dogBestDistance){
//                log("dog%02d is ready to unlock!",dogID);
//                unlock(dogID);
//                //setReadyToUnlock(dogID);
//                result.push_back(dogID);
//            }
        }
    }
	return result;
}


std::string DogManager::getNextUnlockHint()
{
	int unlockDog;
	int unlockPlanet;
	
	bool anyUnlock = getNextUnlockDog(unlockDog, unlockPlanet);
	if(anyUnlock == false) {
		return "";
	}
	
	return getUnlockProgressMessage(unlockDog, unlockPlanet);
}

bool DogManager::getNextUnlockDog(int &unlockDog, int &unlockPlanet)
{
	// Clear
	unlockDog = 0;
	unlockPlanet = 0;
	
	std::vector<int> planetList = PlanetManager::instance()->getPlanetList(PlanetData::Normal);
	//for(int )
	
	//std::vector<int> PlanetManager::getLockedDogList(int planetID)
	for(int planetID : planetList) {
		
		std::vector<int> lockedDog = PlanetManager::instance()->getLockedDogList(planetID);
		
		if(lockedDog.size() == 0) {
			continue;
		}
		
		// Found something to unlock
		// sorting
		sortLockedDogList(lockedDog);
		
		unlockDog = lockedDog[0];
		if(lockedDog.size() == 1) {	// this is the last dog to unlock
			unlockPlanet = planetID;
		}
		
		return true;
	}
	
	return false;
}

void DogManager::sortLockedDogList(std::vector<int> &unlockedDogList)
{
	std::map<int, float> scoreMap;	// key=dogID, value=score
	
	for(int dogID : unlockedDogList) {
		int currentValue;
		int requiredValue;
		
		std::string key = getUnlockProgress(dogID, currentValue, requiredValue);
		
		int remain = requiredValue - currentValue;
		float score = (float) (remain * PlayerRecord::getKeyScoreWeight(key));
		
		log("debug: dogID=%d current=%d required=%d key=%s remain=%d score=%f",
			dogID, currentValue, requiredValue, key.c_str(), remain, score);
		
		
		scoreMap[dogID] = score;
	}
	
	
	// Sorting
	std::sort(unlockedDogList.begin(), unlockedDogList.end(), [&](int a, int b) {
		float scoreA = scoreMap[a];
		float scoreB = scoreMap[b];
		
		log("debug: scoreA=%f scoreB=%f", scoreA, scoreB);
		
		if(scoreA != scoreB){
			return scoreA < scoreB;
		}else{
			return a < b;
		}
	});
	
}




std::string DogManager::getUnlockProgress(int dogID, int &currentValue, int &requiredValue)
{
	
	DogData *data = getDogData(dogID);
	if(data == nullptr) {
		currentValue = 0;
		requiredValue = 100;
		//log("DogManager.getUnlockProgress: no data for dogID=%d", dogID);
		return "";
	}
	
	std::map<std::string, int> condList = data->getUnlockCondList();
	if(condList.size() == 0) {
		currentValue = 1;
		requiredValue = 1;
		//log("DogManager.getUnlockProgress: no unlock condition for dogID=%d", dogID);
		return "";
	}
    
	int valueNow;
	int valueNeed;
	std::string selectedKey = "";
	
	for(std::map<std::string, int>::iterator it=condList.begin(); it != condList.end(); it++) {
		std::string key = it->first;
		valueNeed = it->second;
		
        if(key == "dailyReward"){
            valueNow = RewardManager::instance()->getTotalCollectedDay();
        }else{
            valueNow = GameManager::instance()->getPlayerRecord()->getRecordData(key);
        }
		
		selectedKey = key;
		
		if(valueNow < valueNeed) {		// early exit if the condition not match
			break;
		}
	}
	
	currentValue = valueNow;
	requiredValue = valueNeed;
	
	return selectedKey;
}


#pragma mark - Dog Suggestion
void DogManager::resetSuggestRecord()
{
	updateSuggestRecord(-1);
}


void DogManager::updateSuggestRecord(int value)
{
	mLastSuggestPos = value;
	// log("updateRecord: lastPos=%d value=%d", mLastSuggestPos, value);
	
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("DogManager::updateSuggestRecord: userDefault not available");
		return;
	}
	
	userData->setIntegerForKey(kKeyDogSuggest, value);
}

void DogManager::loadSuggestRecord()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("DogManager::loadUnlockData: userDefault not available");
		return;
	}
	
	mLastSuggestPos = userData->getIntegerForKey(kKeyDogSuggest, -1);
}


bool DogManager::shouldShowSuggestion()
{
	int playCount = GameManager::instance()->getPlayerRecord()->getPlayCount();
	
	return shouldShowSuggestion(playCount);	
}

bool DogManager::shouldShowSuggestion(int playCount)
{
	int suggestDog = DogManager::instance()->getSuggestedDogID();
	if(suggestDog == 0) {	// no more dog to suggest
		return false;
	}
	
	bool isFirstSuggest = mLastSuggestPos <= 0;
	
	int threshold = isFirstSuggest ? kInitialSuggestCount : kSuggestCountStep;
	
	
	int lastPos = mLastSuggestPos <=0 ? 0 : mLastSuggestPos;
	
	
	
	int diff = playCount - lastPos;
	
	bool willSuggest = diff >= threshold;
	
	log("playCount=%d lastPos=%d willSuggest=%d", playCount, lastPos, willSuggest);
	
	if(willSuggest) {
		updateSuggestRecord(playCount);
	}
	
	return willSuggest;
}

std::vector<int> DogManager::getDogListOrderByTeam(DogSearchType searchType)
{
	std::vector<int> result;
	
	std::vector<int> planets = PlanetManager::instance()->getPlanetList(PlanetData::PlanetType::Normal);// not include the reward map
	
	for(int planetID : planets) {
		PlanetData *data = PlanetManager::instance()->getPlanetData(planetID);
		
		std::vector<int> &dogList = data->getDogList();
		for(int dogID : dogList) {
			bool isCountIn = isDogMatchSearchType(dogID, searchType);
			if(isCountIn == false) {
				continue;
			}
			
			result.push_back(dogID);
		}
	}
	
	return result;
}


bool DogManager::isDogMatchSearchType(int dogID, DogSearchType searchType)
{
	if(searchType == SearchTypeAll) {
		return true;
	}
	
	if(searchType == SearchTypeLocked) {
		return isLocked(dogID);
	}

	if(searchType == SearchTypeUnLocked)  {
		return isLocked(dogID) == false;
	}

	
	return true;
}

int DogManager::getSuggestedDogID()
{
	std::vector<int> dogList = getDogListOrderByTeam(SearchTypeLocked);
	
	if(dogList.size() == 0) {
		return 0;
	}
	
	return dogList[0];
}

void DogManager::parseUnlockCond(const std::string &unlockCondKey,
								 int &planet, int &dog, std::string &mainKey)
{
	// reset the output reference
	dog = 0;
	planet = 0;
	mainKey = "";
	
	std::vector<std::string> tokens = aurora::StringHelper::split(unlockCondKey, '_');
	
	if(tokens.size() > 0) {
		mainKey = tokens[0];
	}
	
	if(tokens.size() <= 1) {
		return;
	}
	std::string cond2 = tokens[1];
	
	
	
	// Check if any planet
	if (cond2.find("dog") != std::string::npos){
		int value;
		sscanf(cond2.c_str(), "dog%03d", &value);
		dog = value;
	}
	
	if (cond2.find("planet") != std::string::npos){
		int value;
		sscanf(cond2.c_str(), "planet%03d", &value);
		planet = value;
	}

	
	//
}

std::string DogManager::getUnlockInfo(const std::string &infoTemplate, int value,
									  int planetID, int dogID)
{
	std::string result = infoTemplate;

	aurora::StringHelper::replaceString(result, "#v", StringUtils::format("%d", value));
	
	if(planetID > 0) {
		std::string name = PlanetManager::instance()->getPlanetName(planetID);
		aurora::StringHelper::replaceString(result, "#planet", name);
	}
	
	if(dogID > 0) {
		std::string name = getDogName(dogID);
		aurora::StringHelper::replaceString(result, "#dog", name);
	}
	
	return result;
}

int DogManager::getBestDistancePlanetID(DogData *data)
{
    if(data == nullptr) {
        return 0;
    }
    
    
    // Find the first
    std::string condKey = "";
    int condValue = 0;
    
    std::map<std::string,int> condList = data->getUnlockCondList();
    
    for(std::map<std::string, int>::iterator it=condList.begin(); it != condList.end(); it++) {
        condKey = it->first;
        condValue = it->second;
    }
    
    
    if(condKey == "") {
        return 0;
    }
    
    // Parse the planet and dog
    int planet;
    int dog;
    std::string mainKey;
    
    parseUnlockCond(condKey, planet, dog, mainKey);
    
    return planet;
}

std::string DogManager::getUnlockMessage(DogData *data)
{
	if(data == nullptr) {
		return "";
	}

	
	// Find the first
	std::string condKey = "";
	int condValue = 0;
	
	std::map<std::string,int> condList = data->getUnlockCondList();
	
	for(std::map<std::string, int>::iterator it=condList.begin(); it != condList.end(); it++) {
		condKey = it->first;
		condValue = it->second;
	}
	
	
	if(condKey == "") {
		return "";
	}
	
	// Parse the planet and dog
	int planet;
	int dog;
	std::string mainKey;
	
	parseUnlockCond(condKey, planet, dog, mainKey);
	
	// Make the message

	
	std::string result = getUnlockInfo(data->getUnlockInfo(), condValue, planet, dog);
	
	return result;
}


std::string DogManager::getUnlockProgressMessage(int unlockDog, int unlockPlanet)
{
	if(unlockDog == 0) {
		return "";
	}
	int current;
	int required;

	std::string key = getUnlockProgress(unlockDog, current, required);
	
	int remain = required - current;
	
	//log("debug: dogID=%d key=%s progress=[required: %d current=%d remain=%d]",
	//				dogID, key.c_str(), required, current, remain);

	
	// Parse the planet and dog
	int planet;
	int dog;
	std::string mainKey;
	
	parseUnlockCond(key, planet, dog, mainKey);
	
	std::string dogName = getDogName(unlockDog);
	std::string reward = unlockPlanet == 0 ? dogName	// just the dog
		: dogName + " and " + PlanetManager::instance()->getPlanetName(unlockPlanet);
	
	std::string msgTemplate = PlayerRecord::getUnlockMessageTemplate(mainKey);
	
	
	// Replace the macro of template
	std::string result = msgTemplate;
	
	std::string planetStr = planet == 0 ? "" :
				" in " + PlanetManager::instance()->getPlanetName(planet);
	aurora::StringHelper::replaceString(result, "#planet", planetStr);
	
	
	//
	aurora::StringHelper::replaceString(result, "#item", reward);
	
	std::string remainStr = StringUtils::format("%d", remain);
	aurora::StringHelper::replaceString(result, "#remain", remainStr);
	
	std::string reqStr = StringUtils::format("%d", required);
	aurora::StringHelper::replaceString(result, "#req", reqStr);
	
	return result;
}
