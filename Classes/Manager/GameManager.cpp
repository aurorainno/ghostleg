//
//  GameManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 24/4/2016.
//
//

#include "GameManager.h"
#include "UserGameData.h"
#include "AppHelper.h"
#include "IAPManager.h"
#include "RateAppHelper.h"
#include "MasteryManager.h"
#include "GameCenterManager.h"
#include "EnemyFactory.h"
#include "SuitManager.h"
#include "ShareHelper.h"
#include "ShareImage.h"
#include "Analytics.h"
#include "DogManager.h"
#include "PlayerManager.h"
#include "PlayerRecord.h"
#include "PlayerGameResult.h"
#include "PlanetManager.h"
#include "AppHelper.h"
#include "RewardManager.h"
#include "AstrodogClient.h"
#include "ItemManager.h"
#include "BoosterData.h"
#include "LuckyDrawManager.h"
#include "StageManager.h"
#include "Price.h"
#include "LeaderboardRewardManager.h"
#include "NPCManager.h"

static GameManager *sInstance = NULL;

const int kInitialReviveCost = 1;
const int kReviveCostIncrement = 300;
const int kReviveCostMax = 1200;
const int kNumPlayBeforeRateNotice = 2;		// Number of play before showing the rate notice
const char *kRateNoticeFlag = "ghostLeg.rateNotice";
const char *kEventFlag = "ghostLeg.eventFlag";
const char *kEventPopUpFlag = "ghostLeg.eventPopUpFlag";

const bool kAlwaysToProduction = true;
//const bool kAlwaysToProduction = false;

GameManager *GameManager::instance()
{
	if(sInstance != NULL) {
		return sInstance;
	}
	
	sInstance = new GameManager();
	
	return sInstance;
}

GameManager::GameManager()
: mDebugMap(-1)
, mReviveCost(Price(MoneyTypeStar,300))
, mReviveCount(0)
, mPlayCount(0)
, mSelectedBooster(BoosterItemNone)
, mShouldShowSpeed(false)
{
	mUserData = new UserGameData();
	mMasteryManager = new MasteryManager();
	mIAPManager = new IAPManager();
    mGameCenterManager = new GameCenterManager();
	mSuitManager = new SuitManager();
	mPlayerRecord = new PlayerRecord();		//

	
}

GameManager::~GameManager()
{
	delete mUserData;
	delete mPlayerRecord;
	// TODO other created objects
}

bool GameManager::isDebugMode()
{
#if COCOS2D_DEBUG == 1
	return true;
#else
	return false;
#endif
}

void GameManager::toggleShowSpeed()
{
    mShouldShowSpeed = !mShouldShowSpeed;
}

bool GameManager::shouldShowSpeed()
{
    //show player speed value in game scene layer?
    return mShouldShowSpeed;
}

bool GameManager::isEventOn()
{
  //  return mIsEventOn;
	return false;
}

bool GameManager::isEventPopUpShown()
{
	return false;	// mIsEventPopUpShown;
}

void GameManager::setEventOnOff(bool isON)
{
    mIsEventOn = isON;
    UserDefault *userData = UserDefault::getInstance();
    userData->setBoolForKey(kEventFlag, isON);
}

void GameManager::setEventPopUpShown(bool isShown)
{
    mIsEventPopUpShown = isShown;
    UserDefault *userData = UserDefault::getInstance();
    userData->setBoolForKey(kEventPopUpFlag, isShown);
}

// UserGameData::instance()->loadData();
void GameManager::setup()
{
    Analytics::instance()->setup();
	
	// AstrodogClient
	if(kAlwaysToProduction || isDebugMode() == false) {
		log("##### USING PRODUCTION SERVER ###### ");
		AstrodogClient::instance()->setServer(AstrodogClient::AstrodogServerProduction);
	} else {
		AstrodogClient::instance()->setServer(AstrodogClient::AstrodogServerLocal);
	}
    AstrodogClient::instance()->setup();
    
	
    //check event on/off
    UserDefault *userData = UserDefault::getInstance();
    mIsEventOn = userData->getBoolForKey(kEventFlag, false);
    mIsEventPopUpShown = userData->getBoolForKey(kEventPopUpFlag, false);
    
	// Load User Data 
	mUserData->loadData();
	mPlayerRecord->load();
    
	// Mastery Data
	// mMasteryManager->loadMasteryData();			// Still need??
	// mMasteryManager->loadPlayerMastery();
	
	// IAP and GameCenter
	mIAPManager->setup();
    //mGameCenterManager->setup();
	
	// ItemManager
	ItemManager::instance()->setup();
	
	// Enemy Data
	EnemyFactory::instance()->loadData();
	
	// SuitData									// Still need??
	// mSuitManager->loadPlayerSuit();
	// mSuitManager->loadSuitData();
	// mSuitManager->loadPlayerSelectedSuit();
	
	// DogData
	// DogManager::instance()->setup();			// Still need??
	PlayerManager::instance()->setup();
	
	// update the global ability setting
	PlayerManager::instance()->updateGlobalGameplaySetting();
	
	// PlanetData
	PlanetManager::instance()->setup();
	
    //Dialy Reward
    RewardManager::instance()->setup();
    
    //Lucky Draw
    LuckyDrawManager::instance()->setup();
    
    //LeaderboardReward
    LeaderboardRewardManager::instance()->setup();
    
    //NPCMAnager
    NPCManager::instance()->setup();
	
		
	// Default Debug Setting		// note: cancel them after debug!!
	// setDebugMap(0);
}

UserGameData *GameManager::getUserData() const	// const to prevent user change the address
{
	return mUserData;
}

SuitManager *GameManager::getSuitManager() const
{
	return mSuitManager;
}

MasteryManager *GameManager::getMasteryManager() const
{
	return mMasteryManager;
}

IAPManager *GameManager::getIAPManager() const
{
	return mIAPManager;
}

GameCenterManager *GameManager::getGameCenterManager() const
{
    return mGameCenterManager;
}

void GameManager::resetDebugMap()
{
	mDebugMap = -1;
}


std::string GameManager::getVersionString()
{
	std::string info = "";
	
	info = AppHelper::getVersionString();
	
	info += StringUtils::format(" b%d", AppHelper::getBuildNumber());
	
    
    if(UserGameData::isPaidApp())
    {
        info += " (paid)";
    }
    
	if(isDebugMode()) {
		info += " (debug)";
	}
	
#if(ENTERPRISE == 1)
	info += " (beta)";
#endif

	
	if(getUserData()->isNoAd()) {
		info += " (noad)";
	}

	return info;
}


bool GameManager::shouldShowAd()
{
	return getUserData()->isNoAd() == false;
}


int GameManager::getDebugProperty(const std::string &name)
{
	return mDebugProperty[name];
}

void GameManager::setDebugProperty(const std::string &name, int value)
{
	mDebugProperty[name] = value;
}

void GameManager::share(int score, int stageID)
{
	ShareImage *shareImage = ShareImage::create();
	
	
	int myScore = score;
	shareImage->setScore(myScore);
    shareImage->setStage(stageID);
	
	std::string msg = "Join me in #MonstersExpress";
	
	shareImage->setCallback([msg](RenderTexture*, const std::string &saveFile){
		log("GameManager::share: file saved [%s]", saveFile.c_str());
		
		ShareHelper::share(msg, saveFile);
	});
	
	shareImage->saveToLocal();
	

	
}

bool GameManager::shouldShowTutorial()
{
	return getUserData()->isTutorialPlayed() ? false : true;
}

void GameManager::turnOffTutorial()
{
	getUserData()->markTutorialPlayed(true);
}


void GameManager::resetTutorial()
{
	getUserData()->markTutorialPlayed(false);
}



#pragma mark - PlayerRecord
PlayerRecord *GameManager::getPlayerRecord()
{
	return mPlayerRecord;
}

void GameManager::updatePlayerRecord(PlayerGameResult *gameResult)
{
	mPlayCount++;
	
#if(COCOS2D_DEBUG == 1)
	int multipler = getResultMultiplier();
	if(multipler > 1) {
		gameResult->multiply(multipler);
		log("debug: GameManager: result is multiplied (%d):\n%s",
			multipler, gameResult->toString().c_str());

	}
#endif
	
	//mPlayerRecord->up
	mPlayerRecord->updatePlayerRecordWithResult(gameResult);
}

bool GameManager::isStageClear(int stageID)
{
	return mPlayerRecord->getPlanetRecord(PlayerRecord::KeyHasClear, stageID) == 1;
}

void GameManager::clearPlayerRecord()
{
	mPlayerRecord->clear();
}

std::string GameManager::infoPlayerRecord()
{
	return mPlayerRecord->toString();
}


#pragma mark - Revive (GameContinue) Data
void GameManager::resetReviveData()
{
	mReviveCount = 0;
	resetReviveCost();
}

void GameManager::increaseRevive()
{
	mReviveCount++;
	increaseReviveCost();
}

int GameManager::getReviveCount()
{
	return mReviveCount;
}


Price GameManager::getReviveCost()
{
	return mReviveCost;
}

void GameManager::resetReviveCost()	// should do this when a new game start
{
	mReviveCost = Price(MoneyTypeDiamond,kInitialReviveCost);
}

void GameManager::increaseReviveCost()	// should do this when player click 'continue'
{
	if(mReviveCost.amount <= 1) {
		mReviveCost.amount = 2;
	} else if(mReviveCost.amount <= 2) {
		mReviveCost.amount = 4;
	} else if(mReviveCost.amount <= 4) {
		mReviveCost.amount = 8;
	}
	
//	if(mReviveCost.amount >= kReviveCostMax) {
//		return;	// No need to increase any more
//	}
//	mReviveCost.amount += kReviveCostIncrement;
	
}

#pragma mark - Rating Notice
void GameManager::showRateNotice(bool isForcedShow)
{
    int isAlreadyShown = UserDefault::getInstance()->getIntegerForKey(kRateNoticeFlag,0);
    
    if(!isAlreadyShown){
        RateAppHelper::getInstance()->init();
        RateAppHelper::getInstance()->showRatePrompt(isForcedShow);
        UserDefault::getInstance()->setIntegerForKey(kRateNoticeFlag, 1);
    }
}

void GameManager::resetRateNoticeFlag()
{
    UserDefault::getInstance()->setIntegerForKey(kRateNoticeFlag, 0);
}

bool GameManager::shouldHideBackground()
{
	std::string key = "noBackground";
	int value = GameManager::instance()->getDebugProperty(key);
	
	return value == 1;
}


bool GameManager::shouldGenerateEndPointStar()
{
	return false;
}


int GameManager::getResultMultiplier()
{
	return 1;
}

#pragma mark - Money (Star, Candy, ..) related
std::string GameManager::infoMoney()
{
	std::string result = "";
	
	result += StringUtils::format("diamond=%d", getMoneyValue(MoneyTypeDiamond));
	result += StringUtils::format(" star=%d", getMoneyValue(MoneyTypeStar));
	
	return result;
}


int GameManager::getMoneyValue(MoneyType moneyType)
{
	if(MoneyTypeCandy == moneyType) {
		return getUserData()->getTotalCandy();
	} else if(MoneyTypeDiamond == moneyType) {
		return getUserData()->getTotalDiamond();
	} else {	// Star
		return getUserData()->getTotalCoin();
	}
}

bool GameManager::checkMoneyValue(MoneyType moneyType, int reqValue)
{
	int owned = getMoneyValue(moneyType);
	
	log("debug: moneyType=%d req=%d own=%d", moneyType, reqValue, owned);
	
	return owned >= reqValue;
}

void GameManager::addMoney(MoneyType moneyType, int addCount)
{
	if(MoneyTypeCandy == moneyType) {
		getUserData()->addCandy(addCount);
	}else if(MoneyTypeDiamond == moneyType) {
		getUserData()->addDiamond(addCount);
	} else {	// Star
		getUserData()->addCoin(addCount);
	}
	
}


bool GameManager::useMoney(MoneyType moneyType, int useCount)
{
	int total = getMoneyValue(moneyType);
	
	if(MoneyTypeCandy == moneyType) {
		if(total < useCount) {
			return false;
		}
		
		getUserData()->addCandy(-useCount);
		
		return true;
	}else if(MoneyTypeDiamond == moneyType) {
		if(total < useCount) {
			return false;
		}
			
		getUserData()->addDiamond(-useCount);
			
		return true;
	} else {	// Star
		if(total < useCount) {
			return false;
		}
		
		getUserData()->addCoin(-useCount);
		
		return true;
	}
	
}

#pragma mark - booster
void GameManager::selectBooster(BoosterItemType type)
{
    mSelectedBooster = type;
}

BoosterItemType GameManager::getSelectedBooster()
{
    return mSelectedBooster;
}
