//
//  NPCMapGenerator.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#ifndef NPCMapGenerator_hpp
#define NPCMapGenerator_hpp

#include <stdio.h>
#include <queue>

#include "MapDataGenerator.h"


#include "cocos2d.h"
#include "NPCOrder.h"

USING_NS_CC;


class NPCMapLogic : public Ref
{
public:
	enum TestMode {
		NoTest,
		QuickMode,	// just show 1 game map
		NoGameMap,	// show no game map
	};
public:
	NPCMapLogic();
	virtual ~NPCMapLogic();
	
	virtual bool init();
	
	void setupWithNPC(int npc, int startOffset = -1);
	void setupWithNPCOrder(NPCOrder *npcOrder, int startOffset = -1);
	
	CC_SYNTHESIZE(bool, mQuickMode, QuickMode);		// if QuickMode=true, just one game map
	CC_SYNTHESIZE(Vec2, mDropPosition, DropPosition);
	CC_SYNTHESIZE(Vec2, mStopPosition, StopPosition);
	CC_SYNTHESIZE(TestMode, mTestMode, TestMode);
	
	CC_SYNTHESIZE(int, mLastMap, LastMap);
	CC_SYNTHESIZE(int, mCurrentMap, CurrentMap);
	CC_SYNTHESIZE(int, mComingMap, ComingMap);
	
	void reset();
	
	std::string infoMap();
	
	bool needNextMap();
	bool generateNextMap();					// true if the nextMap is npcMap
	int getNextMapID();						// return -1 if there is no map
	void generateGameMap(int mapID);		// generate single gameMap
	void generateNpcMap(int mapID);			// generate npc map
	
	
	
	void updateWorldData(float delta);		// sync world data to this logic
	
	void updateMapStartPosition(int mapOffset);	// the start offset of a new NPC mapset
	
	bool isAtNpcMap();						//

	int getTravelDistance();			// the distance from mapStart to now
	int getRemainDistance();			// remain distance to NPC map
	
	NPCOrder *getCurrentNPCOrder();
	
	std::string infoDebugData();
	
	int getStartOffset();
	
	
private:
	bool requireNpcMap();
	void updateMapOffset(int mapHeight);
	void setupMapArray(NPCOrder *npcOrder);
	
private:
	CC_SYNTHESIZE_RETAIN(NPCOrder *, mNpcOrder, NpcOrder);
	MapDataGenerator *mMapGenerator;
	
	std::queue<int> mMapQueue;
	int mNpcMapID;
	
	int mTotalDistance;			// Total distance required for this NPC
	int mTravelDistance;		// the distance travel from beginning of a new NPC Map set

	int mPlayerY;
	int mStartOffset;
	int mNextMapOffset;
	bool mReachNpcMap;
};


#endif /* NPCMapGenerator_hpp */
