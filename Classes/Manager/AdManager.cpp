//
//  AdManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 22/6/2016.
//
//

#include "AdManager.h"
#include "cocos2d.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "AdColonyHandler.h"
#include "AdmobHandler.h"
#include "GameSound.h"
#include "CommonMacro.h"
#include "DeviceHelper.h"
#include "Analytics.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "InMobiHandler.h"
#endif

static AdManager* sInstance = NULL;

const int kRetryVideoInterval = 4;		// 0: Always show up		1: Every 1 retry

namespace {
	std::string getAdNameOfVideo(AdManager::VideoAd videoAd) {
		switch (videoAd) {
			case AdManager::VideoAd::GainStar:			return "VideoReward";
			case AdManager::VideoAd::OneMoreChance:		return "VideoRevive";
			case AdManager::VideoAd::PlayAgain:			return "VideoInter";
    		default:									return "";
    	}
	}
	
	bool isRewardVideo(AdManager::VideoAd videoAd) {
		switch (videoAd) {
			case AdManager::VideoAd::GainStar:
			case AdManager::VideoAd::OneMoreChance:
				return true;
				
			case AdManager::VideoAd::PlayAgain:
				return false;
		}
	}
	
	void showAdmobAd(const std::string &name) {
#ifdef SDKBOX_ENABLED
		sdkbox::PluginAdMob::show(name);
#endif
	}
	
	void hideAdmobAd(const std::string &name) {
#ifdef SDKBOX_ENABLED
		sdkbox::PluginAdMob::hide(name);
#endif
	}
}


AdManager::AdManager()
: sdkbox::AdMobListener()
, mHandlerList()
, mCallback(nullptr)
, mLastBanner("")
, mReplayAdCounter(0)
, mVideoProvider(AdColony)
, mRewardedCallback(nullptr)
, mFinishedCallback(nullptr)
{
	mAdmobHandler = new AdmobHandler;
	mHandlerList.push_back(mAdmobHandler);
	mVideoProviderList.push_back(Admob);
	
	mAdColonyHandler = new AdColonyHandler;
	mHandlerList.push_back(mAdColonyHandler);
	mVideoProviderList.push_back(AdColony);
	
#if IS_IOS
	mInMobiHandler = new InMobiHandler;

	mVideoProviderList.push_back(InMobi);
	mHandlerList.push_back(mInMobiHandler);
#else
	mInMobiHandler = nullptr;
#endif
}

AdManager* AdManager::instance(){
    if(sInstance!=NULL){
        return sInstance;
    }else{
        sInstance = new AdManager();
        return sInstance;
    }
}


AdManager::~AdManager()
{
	CC_SAFE_DELETE(mAdColonyHandler);
	CC_SAFE_DELETE(mAdmobHandler);
	
	if(IS_IOS) {
		CC_SAFE_DELETE(mInMobiHandler);
	}
}

void AdManager::init() {
	if(isAdEnabled() == false) {
		return;
	}
	
#ifdef SDKBOX_ENABLED
	log("Setting UP the Ad Handler");
	
	
	
	// Add Callback
	AdHandler::VideoCallback rewardCallback = [&](bool rewarded) {
		onVideoRewarded(rewarded);
	};
	AdHandler::VideoCallback finishCallback = [&](bool finished) {
		onVideoFinished(finished);
	};
	
	for(AdHandler *handler : mHandlerList) {
		handler->setup();
		handler->setVideoFinishedCallback(finishCallback);
		handler->setVideoRewardedCallback(rewardCallback);
	}
	
#endif
}



bool AdManager::showAdBeforeReplay()
{
	if(isNoAd()) { 
		return false;
	}
	
	mReplayAdCounter++;		// increase first
	if(mReplayAdCounter < kRetryVideoInterval) {
		return false;
	}
	
	
	resetShowReplayAdCounter();
	
	return true;
}

void AdManager::resetShowReplayAdCounter()
{
	mReplayAdCounter = 0;
}


void AdManager::showBannerAd(const std::string &name)
{
	if(isNoAd()) {
		log("showBannerAd: no Ad");
		return;	// do nothing
	}
	
	if(mAdmobHandler) {
		mAdmobHandler->showBanner(name);
	}
	mLastBanner = name;
}

void AdManager::hideBannerAd(const std::string &name)
{
	if(mAdmobHandler) {
		mAdmobHandler->hideBanner(name);
	}
	
	mLastBanner = "";
}

void AdManager::hideBannerAd()
{
	if(mLastBanner != "") {
		hideBannerAd(mLastBanner);
	}
}


bool AdManager::isVideoReady(Provider provider, VideoAd ad)
{
	
	AdHandler *handler = getAdHandler(provider);
	if(handler == nullptr) {
		log("AdManager: handler for %d is null. ad=%d", provider, ad);
		return false;
	}
	
	return handler->isAdReady((int) ad);
}

AdManager::Provider AdManager::findDefaultVideoProvider(VideoAd ad)
{
	for(int i=0; i<mVideoProviderList.size(); i++) {
		Provider provider = mVideoProviderList[i];
		if(isVideoReady(provider, ad)) {
			return provider;
		}
	}
	
	return Admob;
}

// The Ad play when retry
bool AdManager::showInterstitualAd()
{
	return showVideoAd(VideoAd::PlayAgain, Provider::Admob);
}

// logic
//		Pick the available Ad provider for the given Ad
//		Pick get the name of the Ad
bool AdManager::showVideoAd(VideoAd video, Provider provider)
{
	Provider selectedProvider = provider == Auto ?
									findDefaultVideoProvider(video)
									: provider;
	
	AdHandler *handler = getAdHandler(selectedProvider);
	if(handler == nullptr) {
		log("showVideoAd: video=%d uknown provider=%d", video, selectedProvider);
		return false;
	}
	
	
	//
	std::string adName = handler->getAdName(video);
	if(adName == "") {
		log("showVideoAd: video=%d not register. provider=%d", video, selectedProvider);
		return false;
	}
	// log("Trying to play: ad=%s (%d) provider=%d", adName.c_str(), video, selectedProvider);
	return showVideoAd(adName, selectedProvider);
}

bool AdManager::showVideoAd(const std::string &name, Provider provider)
{
	
	AdHandler *handler = getAdHandler(provider);
	
	if(handler == nullptr) {
		log("showVideoAd: no handler. provider=%d", provider);
		return false;
	}
	
	log("ShowVideoAd: %s isReady=%d", name.c_str(), handler->isAdReady(name));
	
	if(handler->isAdReady(name) == false) {
		return false;
	}
	
	
	GameSound::pause();
	
	handler->showVideo(name);
	
	return true;
}

void AdManager::setVideoRewardedCallback(const AdHandler::VideoCallback &callback)
{
	mRewardedCallback = callback;
}

void AdManager::setVideoFinishedCallback(const AdHandler::VideoCallback &callback)
{
	mFinishedCallback = callback;
}


bool AdManager::canShowReplayAd()
{
	bool haveInternet = DeviceHelper::isConnectedToInternet();
	if(haveInternet == false) {
		return false;
	}
	
	if(isAdEnabled() == false) {	// Ad is disable, show no AD
		return true;
	}
	
	return GameManager::instance()->getUserData()->isNoAd();
}


void AdManager::setNoAd(bool flag)
{
	GameManager::instance()->getUserData()->setNoAd(flag);
	if(flag) {
		hideBannerAd();
	}
}

bool AdManager::isNoAd()
{
	if(isAdEnabled() == false) {	// Ad is disable, show no AD
		return true;
	}
	
	return GameManager::instance()->getUserData()->isNoAd();
}

bool AdManager::isAdEnabled()
{
#if (DISABLE_AD == 1)
	return false;
#else
	return true;
#endif
}


bool AdManager::isProviderAvailable(Provider provider)
{
	return getAdHandler(provider) != nullptr;
	
}

AdHandler *AdManager::getAdHandler(Provider provider)
{
	switch (provider) {
		case AdColony	:	return mAdColonyHandler;
		case Admob		:	return mAdmobHandler;
		case InMobi		:	return mInMobiHandler;
		default			:	return nullptr;
	}
}

void AdManager::onVideoRewarded(bool flag)
{
	if(mRewardedCallback) {
		mRewardedCallback(flag);
	}
}

void AdManager::onVideoFinished(bool flag)
{
	GameSound::resume();
	
	if(mFinishedCallback) {
		mFinishedCallback(flag);
	}
}

// Note: moved to AdmobHandler
//void AdManager::adViewDidReceiveAd(const std::string &name)
//{
//	printf("adViewDidReceiveAd is called. name:%s\n",name.c_str());
//}
//
//void AdManager::adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg){
//	printf("adViewDidFailToReceiveAdWithError is called. name:%s msg:%s\n",name.c_str(),msg.c_str());
//}
//
//void AdManager::adViewWillPresentScreen(const std::string &name){
//	printf("adViewWillPresentScreen is called. name:%s\n",name.c_str());
//}
//
//void AdManager::adViewDidDismissScreen(const std::string &name)
//{
//	if(mCallback) {
//		mCallback(0);
//	}
//}
//
//void AdManager::adViewWillDismissScreen(const std::string &name){
//	printf("adViewWillDismissScreen is called. name:%s\n",name.c_str());
//}
//
//void AdManager::adViewWillLeaveApplication(const std::string &name){
//	printf("adViewWillLeaveApplication is called. name:%s\n",name.c_str());
//}



#pragma mark - Auto play

bool AdManager::playVideoAd(const AdManager::Ad &toPlayAd,
							const AdHandler::VideoCallback &callback)
{
	AdManager::Provider provider;
	AdManager::VideoAd videoAd;
	findSelectedProviderAndAd(toPlayAd, provider, videoAd);
	
	if(provider == Provider::None) {
		return false;
	}
	
	setVideoFinishedCallback(callback);
	setVideoRewardedCallback(nullptr);
	
	showVideoAd(videoAd, provider);
	
	
	return true;
}

void AdManager::findSelectedProviderAndAd(const AdManager::Ad &toPlayAd,
										  AdManager::Provider &outProvider,
										  AdManager::VideoAd &outAd)
{
	std::vector<Provider> providerList = getProviderOrder(toPlayAd);
	
	std::vector<AdManager::VideoAd> videoList = getTargetVideoAdList(toPlayAd);
	//VideoAd videoAd = getTargetVideoAd(toPlayAd);
	
	
	Provider foundProvider = Provider::None;
	VideoAd foundVideo = VideoAd::OneMoreChance;
	
	bool foundResult = false;
	
	for(Provider provider : providerList) {
		for(VideoAd videoAd : videoList) {
			bool isOkay = isVideoReady(provider, videoAd);
			log("DEBUG: provider=%d video=%d okay=%d", provider, videoAd, isOkay);
			if(isOkay == false) {
				continue;
			}
			
			foundVideo = videoAd;
			foundProvider = provider;
			foundResult = true;
			break;
		}
		
		if(foundResult) {
			break;
		}
	}

	outProvider = foundProvider;
	outAd = foundVideo;
	
	
	//bool isVideoReady(Provider provider, VideoAd ad);
	
}


std::vector<AdManager::VideoAd> AdManager::getTargetVideoAdList(const AdManager::Ad &toPlayAd)
{
	std::vector<AdManager::VideoAd> videoList;
	videoList.push_back(AdManager::Video1);
	videoList.push_back(AdManager::Video2);
	
	
	return videoList;
}


AdManager::VideoAd AdManager::getTargetVideoAd(const AdManager::Ad &toPlayAd)
{
	if(toPlayAd == AdPlayAgain) {
		return AdManager::PlayAgain;
	} else {
		return AdManager::GainStar;
	}
	
}

std::vector<AdManager::Provider> AdManager::getProviderOrder(const AdManager::Ad &toPlayAd)
{
	// ken: we will try different order to test the performance of the eCPM of Ad Network
	//		Continue	: AdMob -> inMobi -> Adcolony
	//		Free Coin	: Adcolony -> inMobi -> Admob
	//		Other		: inMobi -> Adcolony ->Admob
	
	std::vector<AdManager::Provider> providerList;
	
	
	
	
	
	switch (toPlayAd) {
		case AdPlayAgain:
		case AdGameContinue:
		{
			providerList.push_back(Provider::Admob);
			providerList.push_back(Provider::InMobi);
			providerList.push_back(Provider::AdColony);
			break;
		}
			
		case AdFreeCoin:
		{
			providerList.push_back(Provider::AdColony);
			providerList.push_back(Provider::InMobi);
			providerList.push_back(Provider::Admob);
			break;
		}
			
		default: {
			providerList.push_back(Provider::InMobi);
			providerList.push_back(Provider::AdColony);
			providerList.push_back(Provider::Admob);
			break;
		}
	}
	
	return providerList;
}
