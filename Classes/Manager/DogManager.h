//
//  DogManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 4/10/2016.
//
//

#ifndef DogManager_hpp
#define DogManager_hpp

#include <stdio.h>

#include <stdio.h>
#include "cocos2d.h"
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

USING_NS_CC;

class DogData;

class DogManager
{
public:
	static DogManager *instance();
	
	
public:
    enum UnlockStatus{
        StatusLocked = 0,
        StatusReadyToUnlock = 1,  // ready to unlock but not yet unlocked by player
        StatusUnlocked = 2,
    };
	
	enum DogSearchType {
		SearchTypeAll,
		SearchTypeLocked,
		SearchTypeUnLocked
	};
    
	DogManager();
	
	void loadData();			// Load Data from JSON
	void setupMock();
	void setup();
	
	std::string infoDogData();
	std::string getDogName(int dogID);
	DogData *getDogData(int dogID);

	void selectDog(int dogID, bool saveChange=true);	// ken: just setup don't saveChange
	int getSelectedDogAnimeID();
	int getSelectedDogID();
	
	bool isUnlockable(int dogID);
	DogData *getSelectedDogData();

    DogData *generateSingleDogFromJSON(const rapidjson::Value &dogJSON);

    Vector<DogData*> getDogDataList();
	int getTotalDogCount();
    int getTotalUnlockedDog();
	std::string getUnlockProgress(int dogID, int &currentValue, int &requiredValue);
	bool getNextUnlockDog(int &unlockDog, int &unlockPlanet);
	void sortLockedDogList(std::vector<int> &unlockedDogList);
	std::string getUnlockProgressMessage(int dogID, int planetID);
	std::string getNextUnlockHint();
    int getBestDistancePlanetID(DogData* data);
	
protected:
	void addDogData(DogData *dogData);
	
protected:
	Map<int, DogData *> mDogDataMap;
	int mSelectedDog;
	
	
#pragma mark - Dog Unlock Status
public:
	void loadUnlockData();
	void saveUnlockData();
    
    void saveNewDogList();
    void loadNewDogList();
    
    void unlockAllDog();
	void resetAllDog();	// remove all except the dog1 (default dog)

	void unlock(int dogID);	// unlock the given dog
    void setReadyToUnlock(int dogID); //set the dog to "Ready to unlock" state but not yet unlock

    bool isLocked(int dogID);
    UnlockStatus getUnlockStatus(int dogID);
    
    void removeNewDog(int dogID);
    bool isNewDog(int dogID);
    bool hasNewDog();
	
#pragma mark - Search dog
public:
	std::vector<int> getDogListOrderByTeam(DogSearchType searchType);
private:
	bool isDogMatchSearchType(int dogID, DogSearchType searchType);
	
#pragma mark - Dog New Unlock Finder
public:
	std::vector<int> getNewUnlockedDogs();

	
#pragma mark - UnlockInfo
public:
	std::string getUnlockInfo(const std::string &infoTemplate, int value, int planetID=0, int dogID=0);
	std::string getUnlockMessage(DogData *data);
	
	
#pragma mark - Dog Suggestion
public:
	bool shouldShowSuggestion();
	bool shouldShowSuggestion(int playCountNow);
	int getSuggestedDogID();
	void loadSuggestRecord();
	void resetSuggestRecord();
	void updateSuggestRecord(int value);
	void parseUnlockCond(const std::string &unlockCondKey,
						 int &outPlanet, int &outDog, std::string &mainKey);
	
private:
	int mLastSuggestPos;		// value: play count , default: -1
	
private:
	std::map<int, UnlockStatus> mDogUnlockMap;		// key=dogID value=(unlocked: 2 Ready to unlock: 1 locked: 0)
    std::vector<int> mNewDogList;
};


#endif /* DogManager_hpp */
