//
//  GameManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 24/4/2016.
//
//

#ifndef GameManager_hpp
#define GameManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "Price.h"
#include <map>

class UserGameData;
class MasteryManager;
class IAPManager;
class GameCenterManager;
class SuitManager;
class PlayerRecord;
class PlayerGameResult;

class GameManager
{
public: // Type and Enum
	static GameManager *instance();

	
public:
	bool isDebugMode();
    void toggleShowSpeed();
    bool shouldShowSpeed();
	UserGameData *getUserData() const;
	MasteryManager *getMasteryManager() const;
	IAPManager *getIAPManager() const;
    GameCenterManager *getGameCenterManager() const;
	SuitManager *getSuitManager() const;
	
	void setup();
	
	bool shouldShowAd();
	bool shouldGenerateEndPointStar();		//	whether generate the end point star at route end or not
	
    void setEventOnOff(bool isON);
    bool isEventOn();
    
    void setEventPopUpShown(bool isShown);
    bool isEventPopUpShown();
	
	//
	int getResultMultiplier();
	
	bool isStageClear(int stageID);
	
	// Share
	void share(int distance, int stageID);
	
	// Debugging Stuff
	CC_SYNTHESIZE(int, mDebugMap, DebugMap);
    //CC_SYNTHESIZE(int, mRetryCounter, RetryCounter);
	void resetDebugMap();
	std::string getVersionString();
	
	int getDebugProperty(const std::string &name);
	void setDebugProperty(const std::string &name, int value);
	
	bool shouldShowTutorial();
	void turnOffTutorial();
	void resetTutorial();
	
	bool shouldHideBackground();

    
#pragma mark - Booster
    void selectBooster(BoosterItemType type);
    BoosterItemType getSelectedBooster();

#pragma mark - Money (Star, Candy, ..) related
public:
	int getMoneyValue(MoneyType moneyType);
	bool useMoney(MoneyType moneyType, int useCount);	// return false if not enough
	bool checkMoneyValue(MoneyType moneyType, int reqValue);
	void addMoney(MoneyType moneyType, int addCount);
	std::string infoMoney();
	
#pragma mark - Revive (GameContinue) Data
public:
	Price getReviveCost();
	
	void resetReviveCost();
	void increaseReviveCost();
	
	void resetReviveData();
	void increaseRevive();
	int getReviveCount();
	
private:
	Price mReviveCost;
	int mReviveCount;
	int mPlayCount;

#pragma mark - Rating Notice
public:
	void showRateNotice(bool isForcedShow = false);
    void resetRateNoticeFlag();
	
#pragma mark - PlayerRecord
public:
	PlayerRecord *getPlayerRecord();
	void updatePlayerRecord(PlayerGameResult *gameResult);
	void clearPlayerRecord();
	std::string infoPlayerRecord();
	
private:
	PlayerRecord *mPlayerRecord;

	
private:
	GameManager();
	~GameManager();
	
private: // internal value
	//Player
	UserGameData *mUserData;
	MasteryManager *mMasteryManager;
	IAPManager *mIAPManager;
    GameCenterManager *mGameCenterManager;
	SuitManager *mSuitManager;
    BoosterItemType mSelectedBooster;
	std::map<std::string, int> mDebugProperty;
    bool mShouldShowSpeed;
    bool mIsEventOn;
    bool mIsEventPopUpShown;
};

#endif /* GameManager_hpp */
