//
//  BasePlayerData.hpp
//  BounceDefense
//
//  Created by Ken Lee on 30/8/2016.
//
//

#ifndef BasePlayerData_hpp
#define BasePlayerData_hpp

#include <stdio.h>
#include <vector>
#include <string>

#include "cocos2d.h"

USING_NS_CC;

class BasePlayerData : public Ref
{
public:
	BasePlayerData();
	BasePlayerData(const std::string &key);
	virtual bool init() { return true; }
	
	bool save();
	bool load();
	
	virtual std::string toString();
	bool isReady();			// true if load successfully
	
public:	// abstract methods
	virtual std::string generateDataContent() = 0;					// Need implementation
	virtual void parseDataContent(const std::string &content) = 0;	// Need implementation
	virtual void setDefault() = 0;										// Need implementation

protected:	// Supporting method
	std::string getDataKey();
	
protected:
	std::string mDataKey;
	bool mIsReady;
};



#endif /* BasePlayerData_hpp */
