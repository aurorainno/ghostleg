//
//  TutorialManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 4/7/2016.
//
#ifndef TutorialManager_hpp
#define TutorialManager_hpp

#include "cocos2d.h"
#include "TutorialData.h"
#include "external/json/document.h"

USING_NS_CC;

class TutorialManager
{
public:
	static TutorialManager *instance();
	static Vec2 gridToScreenPos(const Vec2 &gridPos);
	
	void loadTutorialData();
	void setMockData();
	
	std::string infoTutorialData();

	TutorialData *getTutorialData(int step);
	
	bool isLastStep(int step);
	Vector<TutorialData *> &getTutorialDataList();
	
private:
	TutorialManager();
    TutorialData* generateSingleDataFromJSON(const rapidjson::Value& data, unsigned index);
	
	void addLastStep();
	
private:
	Vector<TutorialData *> mTutorialDataList;
	
};

#endif /* TutorialManager_hpp */
