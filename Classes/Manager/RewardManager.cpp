//
//  RewardManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 22/11/2016.
//
//

#include "RewardManager.h"
#include "TimeHelper.h"
#include "JSONHelper.h"

static RewardManager* sInstance = NULL;


RewardManager *RewardManager::instance()
{
	if(sInstance != nullptr){
		return sInstance;
	}else{
		sInstance = new RewardManager();
		return sInstance;
	}
}


RewardManager::RewardManager():
mCycleRange(7)
{
	
}

void RewardManager::setup()
{
//	setupMock();
    mDailyReward.setCycleRange(mCycleRange);
    mDailyRewardProfile.setCycleRange(mCycleRange);
	
	loadData();
	loadPlayerData();
}


void RewardManager::parseDailyRewardListRewardFromJSON(
								const rapidjson::Value &rewardJSON,
								DailyReward &dailyReward, bool isDefaultReward)
{
	for(rapidjson::Value::ConstMemberIterator iter = rewardJSON.MemberBegin();
					iter != rewardJSON.MemberEnd(); ++iter)
	{
		// Parse the day value
		int day;
		sscanf(iter->name.GetString(), "day_%03d", &day);
		
		
		// Parse the daily reward
		RewardData *data = generateSingleRewardFromJSON(iter->value);
		
		// Setting data
		if(isDefaultReward) {
			dailyReward.setDefaultReward(day, data);
		} else {
			dailyReward.addReward(day, data);
		}
	}
}

void RewardManager::loadData()
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/dailyReward.json");
    
    //log("product json content:\n%s", content.c_str());
    rapidjson::Document rewardJSON;
    rewardJSON.Parse(content.c_str());
    if (rewardJSON.HasParseError())
    {
        CCLOG("GetParseError %d\n", rewardJSON.GetParseError());
        return;
    }
	
	// Default Reward
	const rapidjson::Value &defaultRewardJSON = rewardJSON["defaultReward"];
	parseDailyRewardListRewardFromJSON(defaultRewardJSON, mDailyReward, true);
	
	
	const rapidjson::Value &dailyRewardJSON = rewardJSON["dailyReward"];
	parseDailyRewardListRewardFromJSON(dailyRewardJSON, mDailyReward, false);
	
//	
//    for(rapidjson::Value::ConstMemberIterator iter = rewardJSON.MemberBegin(); iter != rewardJSON.MemberEnd(); ++iter)
//    {
//        int day;
//        sscanf(iter->name.GetString(), "day_%03d", &day);
//        RewardData *data = generateSingleRewardFromJSON(iter->value);
//        mDailyReward.addReward(day,data);
//    }
	
}

void RewardManager::loadPlayerData()
{
	mDailyRewardProfile.load();
}

RewardData* RewardManager::generateSingleRewardFromJSON(const rapidjson::Value &rewardJSON)
{
    
        int objectID = aurora::JSONHelper::getInt(rewardJSON, "objectID", -1);
        std::string typeStr = aurora::JSONHelper::getString(rewardJSON, "type", "");
        RewardData::RewardType type = RewardData::getRewardTypeByName(typeStr);
        int amount = aurora::JSONHelper::getInt(rewardJSON, "count", 0);
	
        RewardData* data = RewardData::create(type, objectID, amount);
        
        return data;
}

void RewardManager::saveData()
{
    mDailyRewardProfile.save();
}

void RewardManager::setupMock()
{
	// Adding Star first
	RewardData *reward;
	
	mDailyReward.setCycleRange(mCycleRange);
    mDailyRewardProfile.setCycleRange(mCycleRange);
	
	for(int day=1; day <= 30; day++) {
		reward = RewardData::create(RewardData::RewardTypeStar, 0, day * 50);
		mDailyReward.addReward(day, reward);
	}

	// Special Dog at cycle 1
	reward = RewardData::create(RewardData::RewardTypeStar, 0, 1500);
	mDailyReward.addReward(30 + 10, reward);
	
	reward = RewardData::create(RewardData::RewardTypeStar, 0, 2000);
	mDailyReward.addReward(30 + 20, reward);
}


std::string RewardManager::toString()
{
	std::string result = "";
	
	result += "Daily Reward Data";
	result += mDailyReward.toString();
	
	result += "Daily Reward Profile";
    result += mDailyRewardProfile.toString();
	
	return result;
}



bool RewardManager::checkDailyRewardStatus(DailyRewardCheckStatus status,int givenDay)
{
    if(status == DailyRewardCheckStatus::DailyRewardStatusIncreaseCycle){
        return mDailyRewardProfile.isAllDayCollected();
    }else if(status == DailyRewardCheckStatus::DailyRewardStatusNoNeed){
        return mDailyRewardProfile.isCollectedToday(givenDay);
    }else if(status == DailyRewardStatusNeedReset){
        return mDailyRewardProfile.isCollectedYesterday(givenDay) == false;
    }
    
    return false;
//	if(mDailyRewardProfile.isCollectedToday(givenDay)) {
//		return RewardManager::DailyRewardStatusNoNeed;
//	}
//
//	if(mDailyRewardProfile.isAllDayCollected()) {
//		return RewardManager::DailyRewardStatusIncreaseCycle;
//	}
//
//    if(mDailyRewardProfile.isCollectedYesterday(givenDay) == false) {
//        return RewardManager::DailyRewardStatusNeedReset;
//    }
//	
//	return RewardManager::DailyRewardStatusNormal;
}

RewardData* RewardManager::collectReward(int day)
{
    if(day == -1){
        day = TimeHelper::getCurrentTimeStampInDays();
    }
    
    mDailyRewardProfile.markDayCollected(day);
    
    RewardData* data =  mDailyReward.getRewardAtDay(mDailyRewardProfile.getCurrentCycle(),
                                                    getTotalCollectedDayInTermsOfCycle());
    
    return data;
}

int RewardManager::getTotalCollectedDayInTermsOfCycle()
{
    return mDailyRewardProfile.getCurrentDayWithinACycle() - 1;
}

int RewardManager::getTotalCollectedDay()
{
    return 5*mDailyRewardProfile.getCurrentCycle()+getTotalCollectedDayInTermsOfCycle();
}

RewardData* RewardManager::getRewardByDay(int day)
{
    return mDailyReward.getRewardAtDay(mDailyRewardProfile.getCurrentCycle(), day);
}

void RewardManager::increaseCycle()
{
    mDailyRewardProfile.increaseCycle();
    mDailyRewardProfile.resetCollectedFlag();
}

void RewardManager::resetCollectedReward()
{
    mDailyRewardProfile.resetCollectedFlag();
}

void RewardManager::resetAll()
{
    mDailyRewardProfile.reset();
}
