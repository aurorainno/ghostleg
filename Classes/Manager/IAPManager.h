//
//  IAPManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 24/5/2016.
//
//

#ifndef IAPManager_hpp
#define IAPManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <string>
#include <iostream>

USING_NS_CC;

#include "GameProduct.h"
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include "PluginIAP/PluginIAP.h"

#include "IAPStatusCode.h"

class IAPManager : public sdkbox::IAPListener
{
public:		// callback definition
	typedef std::function<void(IAPStatusCode status, const std::string &msg)> UpdateProductCallback;
	typedef std::function<void(GameProduct *product, IAPStatusCode status, const std::string &msg)> PurchaseCallback;
    typedef std::function<void(IAPStatusCode status, const std::string &msg)> RestoreItemCallback;

	
public:
	IAPManager();
	void setup();
	void updateProductInfo();
    void restorePurchasedItems();
	
	
	//IAPStatusCode purchase
	IAPStatusCode buyProduct(GameProduct *product);
	
	IAPStatusCode purchase(const std::string & name);
	IAPStatusCode purchaseStarByDiamond(GameProduct *product);
	
	void loadProductList();
	GameProduct *getGameProductByName(const std::string &name);
	IAPStatusCode executionAction(const std::string &name);		// return: statusCode
	std::string getStatusCodeString(IAPStatusCode status);
	
	bool isAvailable();
	bool isReady();				// not ready before updateProductInfo
	bool isProductAvailable(GameProduct *product);

	Vector<GameProduct*> getProductArray();
	
	// TODO: combine with LoadPriceCallback
	typedef std::function<void()> ProductRequestSuccessCallback;
	void setProductRequestSuccessCallback(const ProductRequestSuccessCallback &callback);
	
	
	void resetNonConsumable(const std::string &name);		// For testing
	
	// Callback
	void setUpdateProductCallback(const UpdateProductCallback &callback);
	void setPurchaseCallback(const PurchaseCallback &callback);
    void setRestoreItemCallback(const RestoreItemCallback &callback);

	
	// String Info
	std::string infoGameProductList();
	
#pragma mark - Local Variable
	bool mIsReady;				// Set in init and updatePrice
	bool mIsSupported;			// Is it supported by current version
	UpdateProductCallback mUpdateProductCallback;
	PurchaseCallback mPurchaseCallback;
    RestoreItemCallback mRestoreItemCallback;

	Vector<GameProduct*> mProductArray;
	ProductRequestSuccessCallback mProductRequestSuccessCallback;

	
	
#pragma mark - IAP Listener
private:
	virtual void onInitialized(bool ok) override;
	virtual void onSuccess(sdkbox::Product const& p) override;
	virtual void onFailure(sdkbox::Product const& p, const std::string &msg) override;
	virtual void onCanceled(sdkbox::Product const& p) override;
	virtual void onRestored(sdkbox::Product const& p) override;
	virtual void onProductRequestSuccess(std::vector<sdkbox::Product> const &products) override;
	virtual void onProductRequestFailure(const std::string &msg) override;
	virtual void onRestoreComplete(bool ok, const std::string &msg);

#pragma mark - IAP Action
private:
	IAPStatusCode handleAddStar(const std::string arg);
	IAPStatusCode handleRemoveAd();
	IAPStatusCode handleAddCandy(const std::string arg);
    IAPStatusCode handleAddDiamond(const std::string arg);
	
#pragma mark - Analytics
	void logAnalytics(GameProduct *product);
	
#pragma mark - Is Owned Check
	bool isProductOwned(GameProduct *product);
	void updateOwnStatus(GameProduct *product, bool ownFlag);

#pragma mark - Callback (private part)
private:
	void callbackPurchase(GameProduct *product, IAPStatusCode status, const std::string &msg);
    void callbackRestore(IAPStatusCode status, const std::string &msg);
    GameProduct* generateSingleProductFromJSON(const rapidjson::Value &productJSON);
};


#endif /* IAPManager_hpp */
