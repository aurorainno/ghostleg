//
//  StageManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#ifndef StageManager_hpp
#define StageManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "StageData.h"
#include "NPCOrder.h"
#include "NPCOrderData.h"
#include "GameplaySetting.h"

USING_NS_CC;



class StageManager
{
public: // Type and Enum
	static StageManager *instance();

public:
	CC_SYNTHESIZE(int, mCurrentStage, CurrentStage);
	CC_SYNTHESIZE(int, mDebugNpcOrder, DebugNpcOrder);
	CC_SYNTHESIZE_RETAIN(GameplaySetting *, mGameplaySetting, GameplaySetting);
	
public:
	void loadAllStageData();
	void loadStageData(int stageID);
	void updateNpcOrderDataHash();
	void setup();
	void setupMockData();
	std::vector<int> getAllGameMap();
	std::vector<int> getAllNpcMap();
	void loadGameMaps();
	
	StageData *getCurrentStageData();
	StageData *getStageData(int stageID);
	NPCOrderData *getNpcOrderData(int npcOrderID);
	std::string infoStageData();
	
	void reset();
	void setNpcOrderDone(int npcOrderID);
	int getNpcOrderDoneCount(int npcOrderID);
	int getTotalOrderCount();
	int getOrderDoneCount();
	
	bool shouldRewardPuzzle();
	std::string infoPuzzleChanceTable();
	std::vector<int> rollRewardPuzzle(int numPuzzle);
	
	NPCOrder *getNextNPCOrder();
	
private:
	StageManager();
	~StageManager();

	void updatePuzzleChanceTable(StageData *stageData);

private:
	void addStageData(StageData *stageData);
	
private:
	Map<int, StageData *> mStageDataHash;
	Map<int, NPCOrderData *> mNpcOrderDataHash;			// key=orderID,
	std::map<int, int> mNpcOrderDoneCount;				// key=orderID, value=done count
	std::map<int, int> mPuzzleChanceTable;
	int mOrderDoneCount;
	
	
#pragma mark - Stage & NPC Order Selection
public:
	Vector<NPCOrder *> getNextNPCOrderList();
	NPCOrder *getNpcOrder(int npcOrderID);

private:
	std::vector<NPCOrderData *> getNextNPCOrderDataList();
};

#endif /* StageManager_hpp */
