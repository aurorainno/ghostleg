//
//  StageManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#include "StageManager.h"
#include "StageData.h"
#include "NPCOrder.h"
#include "LevelData.h"
#include "StringHelper.h"
#include "RandomHelper.h"
#include "DogData.h"
#include "PlayerAbility.h"
#include "PlayerManager.h"
#include "GameWorld.h"

static StageManager *sInstance = nullptr;

StageManager *StageManager::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new StageManager();
	
	return sInstance;
}

StageManager::StageManager()
: mStageDataHash()
, mNpcOrderDataHash()
, mCurrentStage(0)
, mDebugNpcOrder(0)
, mGameplaySetting(nullptr)
{
	
}

StageManager::~StageManager()
{
	
}


void StageManager::setup()
{
	//setupMockData();
	loadAllStageData();
	loadGameMaps();
	updateNpcOrderDataHash();
	
	//mGameplaySetting = GameWorld::instance()->getGameplaySetting();
}


// Deprecated
//void StageManager::updateGameplaySetting()
//{
//	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
//	if(profile == nullptr) {
//		log("updateGameplaySetting: profile is null");
//		return;
//	}
//	
//	mGameplaySetting->reset();
//	
//	profile->updateGameplaySetting(mGameplaySetting);
//	
//	
//	mGameplaySetting->setMaxSpeed(profile->getMaxSpeed());
//	mGameplaySetting->setTurnSpeed(profile->getTurnSpeed());
//	mGameplaySetting->setAcceleration(profile->getAcceleration());
//	mGameplaySetting->setStunReduction(profile->getStunReduction());
//}

#pragma mark - Design Data Loading
void StageManager::loadAllStageData()
{
	int numStage = 5;
	for(int stage=0; stage<=numStage; stage++) {
		loadStageData(stage);
	}
	loadStageData(999);		// Tutorial
	
}

void StageManager::loadStageData(int stageID)
{
	std::string filename = StringUtils::format("level/stage/stage_%03d.json", stageID);
	std::string content = FileUtils::getInstance()->getStringFromFile(filename);
	
	// log("CONTENT\n%s\n", content.c_str());
	
	StageData *stageData = StageData::create();
	stageData->parseJSONContent(content);
	
	log("**** INFO:\n%s\n", stageData->toString().c_str());

	addStageData(stageData);
}

void StageManager::addStageData(StageData *stageData)
{
	mStageDataHash.insert(stageData->getStageID(), stageData);
}

std::vector<int> StageManager::getAllGameMap()
{
	std::vector<int> result;
	
	std::vector<int> mapKeyVec = mStageDataHash.keys();
	for(int stageID : mapKeyVec)
	{
		StageData *data = getStageData(stageID);
		//log("StageData: %s", data->toString().c_str());
		std::vector<int> mapList = data->getGameMapList();
		//log("StageData: mapList=%s", VECTOR_TO_STR(mapList).c_str());
		for(int mapID : mapList) {
			result.push_back(mapID);
		}
	}
	
	return result;
}

std::vector<int> StageManager::getAllNpcMap()
{
	std::vector<int> result;
	
	std::vector<int> mapKeyVec = mStageDataHash.keys();
	for(int stageID : mapKeyVec)
	{
		StageData *data = getStageData(stageID);
		std::vector<int> mapList = data->getNpcMapList();
		for(int mapID : mapList) {
			result.push_back(mapID);
		}
	}
	
	return result;
}

void StageManager::loadGameMaps()
{
	MapLevelDataCache::instance()->loadGameMap(getAllGameMap());
	
	MapLevelDataCache::instance()->loadNpcMap(getAllNpcMap());
}


void StageManager::setupMockData()
{
	StageData *stageData = StageData::create();
	
	
	
	
	// Stage
	stageData->setStageID(1);
	addStageData(stageData);
	
	// NPC ORDER
	NPCOrderData *order;
	std::vector<int> mapList;
	
	// 1st NPC
	mapList.clear();
	mapList.push_back(816); mapList.push_back(817); mapList.push_back(818);
	
	order = NPCOrderData::create();
	order->setOrderID(1);
	order->setNpcID(1);
	order->setNpcMap(101);
	order->setTimeLimit(100);
	order->setReward(1000);
	order->setDifficulty(1);
    order->setName("ken");
	order->setMapList(mapList);
	stageData->addNpcOrderData(order);
	
	
	// 2nd NPC
	mapList.clear();
	mapList.push_back(818); mapList.push_back(819);
	
	order = NPCOrderData::create();
	order->setOrderID(2);
	order->setNpcID(2);
	order->setNpcMap(102);
	order->setReward(500);
	order->setTimeLimit(160);
	order->setDifficulty(2);
    order->setName("calvin");
	order->setMapList(mapList);
	stageData->addNpcOrderData(order);

	//
	updateNpcOrderDataHash();
}


NPCOrderData *StageManager::getNpcOrderData(int npcOrderID)
{
	return mNpcOrderDataHash.at(npcOrderID);
}

void StageManager::updateNpcOrderDataHash()
{
	mNpcOrderDataHash.clear();		// clear the hash
	
	std::vector<int> mapKeyVec = mStageDataHash.keys();
	for(int stageID : mapKeyVec)
	{
		StageData *data = getStageData(stageID);
		Vector<NPCOrderData *> orderList = data->getNpcOrderDataList();
		for(NPCOrderData *order : orderList) {
			mNpcOrderDataHash.insert(order->getOrderID(), order);
		}
	}
}

#pragma mark - Getting Stage Data and Information
StageData *StageManager::getCurrentStageData()
{
	return getStageData(getCurrentStage());
}

StageData *StageManager::getStageData(int stageID)
{
	if(mStageDataHash.find(stageID) == mStageDataHash.end()) {
		return nullptr;
	}
	
	return mStageDataHash.at(stageID);
}

std::string StageManager::infoStageData()
{
	std::string result = "";
	
	result = StringUtils::format("stageCount=%ld\n", mStageDataHash.size());
	
	std::vector<int> mapKeyVec = mStageDataHash.keys();
	for(int stageID : mapKeyVec)
	{
		StageData *data = getStageData(stageID);
		result += StringUtils::format("Stage %d\n============\n", stageID);
		
		result += data->toString();
		
		result += "\n";
	}
	
	
	return result;
}

#pragma mark - Stage & NPC Order Selection
std::vector<NPCOrderData *> StageManager::getNextNPCOrderDataList()
{
	
	std::vector<NPCOrderData*> resultList;
	
	
	if(mDebugNpcOrder > 0) {
		resultList.push_back(getNpcOrderData(mDebugNpcOrder));
		resultList.push_back(getNpcOrderData(mDebugNpcOrder));
		return resultList;
	}
	
	
	StageData *data = getStageData(getCurrentStage());
	
	
	
	// Exceptional case
	if(data == nullptr) {
		resultList.push_back(getNpcOrderData(101));
		resultList.push_back(getNpcOrderData(102));

		return resultList;
	}
	
	
	
	
	// Turn the hash map
	Vector<NPCOrderData *> npcOrderList = data->getNpcOrderDataList();
	Map<int, NPCOrderData *> npcOrderMap;
	
	int firstOrderID = 0;
	for(NPCOrderData *order : npcOrderList) {
		npcOrderMap.insert(order->getOrderID(), order);
		if(firstOrderID == 0) {
			firstOrderID = order->getOrderID();
		}
	}


	// Roll the NPC
	std::vector<int> rollNpcList = data->rollNpc(getOrderDoneCount());
	
	int firstNpc = rollNpcList.size() <= 0 ? firstOrderID : rollNpcList[0];			// 101 is the default
	int secondNpc = rollNpcList.size() <= 1 ? firstNpc : rollNpcList[1];

	
	// output to result
	resultList.push_back(npcOrderMap.at(firstNpc));
	resultList.push_back(npcOrderMap.at(secondNpc));
	
	return resultList;
}

Vector<NPCOrder *> StageManager::getNextNPCOrderList()
{
	GameplaySetting *setting = mGameplaySetting;
	
	std::vector<NPCOrderData *> orderDataList = getNextNPCOrderDataList();
	
	Vector<NPCOrder*> resultList;
	
	for(NPCOrderData *orderData : orderDataList) {
		NPCOrder *order = NPCOrder::create();
		
		order->setup(orderData, setting);
		
		
		resultList.pushBack(order);
	}
	
	return resultList;
}


void StageManager::reset()
{
	mNpcOrderDoneCount.clear();
	mOrderDoneCount = 0;
	
	// update the puzzle chance table
	updatePuzzleChanceTable(getStageData(getCurrentStage()));
}

void StageManager::setNpcOrderDone(int npcOrderID)
{
	int count = getNpcOrderDoneCount(npcOrderID);
	count++;
	mNpcOrderDoneCount[npcOrderID] = count;
	
	// Total Count
	mOrderDoneCount++;
}

int StageManager::getNpcOrderDoneCount(int npcOrderID)
{
	if(mNpcOrderDoneCount.find(npcOrderID) == mNpcOrderDoneCount.end()) {
		return 0;
	} else {
		return mNpcOrderDoneCount.at(npcOrderID);
	}
	
}


NPCOrder *StageManager::getNpcOrder(int npcOrderID)
{
	NPCOrderData *orderData = getNpcOrderData(npcOrderID);
	if(orderData == nullptr) {
		return nullptr;
	}
	
	NPCOrder *npcOrder = NPCOrder::create();
	npcOrder->setup(orderData, mGameplaySetting);
	
	return npcOrder;
}


int StageManager::getOrderDoneCount()
{
	return mOrderDoneCount;
}

int StageManager::getTotalOrderCount()
{
	StageData *data = getStageData(getCurrentStage());
	if(data == nullptr) {
		return 0;
	}
	
	return data->getTotalOrderCount();
}

NPCOrder *StageManager::getNextNPCOrder()
{
	
	if(mDebugNpcOrder > 0) {
		return getNpcOrder(mDebugNpcOrder);
	}
	
	
	StageData *data = getStageData(getCurrentStage());
	if(data == nullptr) {
		return nullptr;
	}
	
	int orderServed = getOrderDoneCount();
	int orderID = data->rollOneNpc(orderServed);
	if(orderID == 0) {
		return nullptr;
	}
							   
	NPCOrder *orderData = getNpcOrder(orderID);
	
	orderData->setOrderIndex(orderServed);
	orderData->setIsLastOrder(data->isLastOrder(orderServed));
							   
	return orderData;
}

bool StageManager::shouldRewardPuzzle()
{
	return mPuzzleChanceTable.size() > 0;
}


std::string StageManager::infoPuzzleChanceTable()
{
	return "PuzzleChance:\n" + aurora::StringHelper::mapToString(mPuzzleChanceTable);
}

void StageManager::updatePuzzleChanceTable(StageData *stageData)
{
	mPuzzleChanceTable.clear();
	if(stageData == nullptr) {
		return;
	}
	
	std::map<int, int> chanceData = stageData->getPuzzleChanceTable();
	
	for (std::map<int,int>::iterator it=chanceData.begin(); it!=chanceData.end(); ++it) {
		int characterID = it->first;
		int chance = it->second;
		
		if(PlayerManager::instance()->isCharacterNeedFragment(characterID) == false) {
			continue;
		}
		
		mPuzzleChanceTable[characterID] = chance;
	}
	
}


std::vector<int> StageManager::rollRewardPuzzle(int numPuzzle)
{
	return aurora::RandomHelper::findMultiRandomValue(mPuzzleChanceTable, numPuzzle, true);
}


