//
//  IAPStatusCode.hpp
//  GhostLeg
//
//  Created by Ken Lee on 25/5/2016.
//
//

#ifndef IAPStatusCode_hpp
#define IAPStatusCode_hpp

#include <stdio.h>

typedef enum
{
	IAP_SUCCESS					= 0,
	IAP_PURCHASE_CANCEL			= 1,
	IAP_WAIT_CALLBACK			= 2,
	
	IAP_ERROR_PURCHASE_FAIL		= -1,
	IAP_ERROR_PRICE_NOT_READY	= -2,
	IAP_ERROR_NOT_SUPPORT		= -3,
	IAP_UNKNOWN_ACTION			= -4,
	IAP_LOAD_PRICE_FAIL			= -5,
    IAP_NO_ITEM_TO_RESTORE	    = -6,
	IAP_NOT_ENOUGH_MONEY		= -7,		// if the given cost not enough
}IAPStatusCode;

#endif /* IAPStatusCode_hpp */
