//
//  PlayerManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#ifndef PlayerManager_hpp
#define PlayerManager_hpp

#include <stdio.h>


#include <stdio.h>
#include "cocos2d.h"
#include "external/json/document.h"
#include "PlayerCharProfile.h"
#include "PlayerCharDataSet.h"
#include "GameplaySetting.h"

class CharGameData;
class PlayerCharData;

USING_NS_CC;

class PlayerManager
{
public:
	enum CharFilterType
	{
		CharFilterAll,			// All Character
		CharFilterOwned,		// Owned character (Unlocked)
		CharFilterLocked,		// Locked Character
	};
	
	enum CharSortType
	{
		CharSortNone,
		CharSortDefault		// First: selected char, order by charID
	};
	
public:
	static PlayerManager *instance();

private:
	PlayerManager();
	~PlayerManager();
	
#pragma mark - Data Setup
public:
	void loadData();			
	void setupMockData();
	void setup();
	CharGameData* parseCharGameData(const rapidjson::Value &dogJSON);
	
	CharGameData *getCharGameData(int charID);
	
	int getTotalCharCount();
	bool isCharacterLocked(int charID);
	bool isCharacterNeedFragment(int charID);
	
	std::string getCharName(int charID);
	
	std::string infoAllCharData();
	std::string infoCharAbility(const std::vector<int> selectedChar = std::vector<int>());

private:
	void saveData(CharGameData *data);
	
private:
	Map<int, CharGameData *> mCharDataHash;

#pragma mark - Upgrade Related
public:
	PlayerCharProfile::UpgradeStatus upgradeChar(int charID);
	PlayerCharProfile::UpgradeStatus unlock(int charID);
	void forceUnlock(int charID);
	
	
#pragma mark - PlayerCharData

#pragma mark - PlayerCharProfile
public:
	PlayerCharProfile *getPlayerCharProfile(int charID);
	void updateCharProfile();
	std::string infoPlayerCharProfile();
	
	void addPlayerFragment(int charID, int fragCount);
	void addPlayerFragments(const std::vector<int> charList);
	
	
private:
	Map<int, PlayerCharProfile *> mPlayerProfileHash;
	


#pragma mark - PlayerCharData
public:
	void setupMockPlayerCharData();
	std::string infoPlayerCharData();
	PlayerCharData *getPlayerCharData(int charID);
	
	void savePlayerCharData();
	void loadPlayerCharData();
	void resetPlayerCharData();
	void unlockAllChar();
	void upgradeAllChar();
	
private:
	CC_SYNTHESIZE_RETAIN(PlayerCharDataSet *, mPlayerCharDataSet, PlayerCharDataSet);
	
#pragma mark - Selected Character
public:
	int getSelectedChar();
	void selectChar(int charID);
	PlayerCharProfile *getSelectedProfile();
	

#pragma mark - Character Listing
public:
	Vector<PlayerCharProfile *> getProfileList(
					const CharFilterType &filter, const CharSortType &sort);
	Vector<PlayerCharProfile *> getOwnedCharacters(const CharSortType &sort = CharSortDefault);
	Vector<PlayerCharProfile *> getLockedCharacters(const CharSortType &sort = CharSortDefault);
    //auto do unlock logic when calling this function
    std::vector<int> getNewUnlockedCharacters();
	
private:
	bool isFilterMatch(PlayerCharProfile *profile, const CharFilterType &filter);


#pragma mark - GameplaySetting
public:
	void updateGlobalGameplaySetting();
	GameplaySetting *getCharacterGameplaySetting(PlayerCharProfile *profile, bool includeGlobal=true);
	GameplaySetting *getCharacterGameplaySetting(int charID, bool includeGlobal=true);
	GameplaySetting *getCurrentGameplaySetting(bool includeGlobal=true);		// Selected character
	std::string infoGlobalPlaySetting();
	
private:
	CC_SYNTHESIZE_RETAIN(GameplaySetting *, mGlobalPlaySetting, GlobalGameplaySetting);

};

#endif /* PlayerManager_hpp */
