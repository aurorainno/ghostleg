//
//  Analytics.cpp
//  GhostLeg
//
//  Created by Ken Lee on 6/7/2016.
//
//

#include "Analytics.h"

// For the Dimension and Index Mapping: see GA -> Property (AstroRun) -> Custom Definition -> Custom Dimension
const int kDimensionIndexPaidStatus	= 1;
const int kDimensionIndexDebugVersion = 2;
const bool kIsEnabled =	true;		// this is for debugging when sdkbox not working

#define RETURN_CASE_NAME(__x)	case Analytics::__x			: return #__x;
#define RETURN_SCREEN_NAME(__x)	case Analytics::scn_##__x		: return #__x;

#include "GameManager.h"
#include "GameWorld.h"
#include "SuitManager.h"
#include "MasteryManager.h"
#include "DogData.h"
#include "DogManager.h"
#include "PlanetManager.h"
#include "PlayerManager.h"
#include "ItemManager.h"

#ifdef SDKBOX_ENABLED
#include "PluginGoogleAnalytics/PluginGoogleAnalytics.h"
#endif


#pragma mark - Enum to String mapping
namespace {
	Analytics::Category getConsumeCategory(MoneyType type) {
		switch (type) {
			case MoneyTypeDiamond	: return Analytics::consume_diamond;
			case MoneyTypePuzzle	: return Analytics::consume_puzzle;
			default					: return Analytics::consume_coin;
		}
	}
	
	Analytics::Category getCollectCategory(MoneyType type) {
		switch (type) {
			case MoneyTypeDiamond	: return Analytics::collect_diamond;
			case MoneyTypePuzzle	: return Analytics::collect_puzzle;
			default					: return Analytics::collect_coin;
		}
	}
	
	
	std::string getActionString(Analytics::Action action)
	{
		switch (action) {
			case Analytics::on_enter:           return "on_enter";
            case Analytics::play_again:         return "play_again";
            case Analytics::revive:             return "revive";
            case Analytics::back_main:          return "back_main";
            case Analytics::share:              return "share";
            case Analytics::start_game:         return "start_game";
            case Analytics::play_game:          return "play_game";
            case Analytics::from_appstore:		return "from_appstore";
            case Analytics::ad_reward:          return "ad_reward";
			case Analytics::bonus_star:			return "bonus_star";
			case Analytics::daily_reward:		return "daily_reward";
				
			case Analytics::click_fb:			return "click_fb";
			case Analytics::click_setting:		return "click_setting";
			case Analytics::change_planet:		return "change_planet";
			case Analytics::click_dogmenu:		return "click_dogmenu";
			case Analytics::click_gmtool:		return "click_gmtool";
			case Analytics::click_leaderboard:	return "click_leaderboard";
			case Analytics::show_rate_us:		return "show_rate_us";
			case Analytics::continue_game:		return "game_continue";

				// For Resource Collection

				RETURN_CASE_NAME(from_game)
				RETURN_CASE_NAME(from_diamond)
				RETURN_CASE_NAME(from_iap)
				RETURN_CASE_NAME(from_free_coin)
				RETURN_CASE_NAME(from_lucky_draw)
				RETURN_CASE_NAME(from_double_up)
				RETURN_CASE_NAME(from_daily_reward)
				RETURN_CASE_NAME(from_mail)
				RETURN_CASE_NAME(from_booster_shop)
//
//				// For Resource Consumption
				RETURN_CASE_NAME(act_unlock_char)
				RETURN_CASE_NAME(act_upgrade_char)
				RETURN_CASE_NAME(act_continue_game)
				RETURN_CASE_NAME(act_unlock_stage)
				RETURN_CASE_NAME(act_lucky_draw)
				RETURN_CASE_NAME(act_buy_coin)
				RETURN_CASE_NAME(act_buy_booster)
				RETURN_CASE_NAME(act_use_booster)
//
				
			default:
				return StringUtils::format("action_%d", action);
				
		}
	}
	
	std::string getCategoryString(Analytics::Category category)
	{
		switch (category) {
			case Analytics::game_stage:		 return "game_stage";
			case Analytics::player_hit:		 return "player_hit";
				
			case Analytics::in_game:		 return "in_game";
            case Analytics::end_game:		 return "end_game";
            case Analytics::game_map:		 return "game_map";
            case Analytics::main:            return "main";
			case Analytics::game_continue:	 return "game_continue";
			
			case Analytics::collect_coin:	   return "collect_coin";
			case Analytics::consume_coin:	   return "consume_coin";
			case Analytics::collect_diamond:   return "collect_diamond";
			case Analytics::consume_diamond:   return "consume_diamond";
			case Analytics::collect_puzzle:    return "collect_puzzle";
			case Analytics::consume_puzzle:    return "consume_puzzle";
				
			case Analytics::collect_booster:	return "collect_booster";
			case Analytics::use_booster:		return "use_booster";
			
            case Analytics::upgrade_suit:	 return "upgrade_suit";
            case Analytics::upgrade_mastery: return "upgrade_mastery";
            case Analytics::tutorial:		 return "tutorial";
            case Analytics::payment:		 return "payment";
            case Analytics::testing:		 return "testing";
			case Analytics::dog_menu:		 return "dog_menu";
			default:						return StringUtils::format("category_%d", category);
		}
        
        
        

	}
	
	std::string getScreenString(Analytics::Screen screen)
	{
		switch (screen) {
				
				RETURN_SCREEN_NAME(title_screen)
				RETURN_SCREEN_NAME(game_continue)
				RETURN_SCREEN_NAME(coin_shop)
				RETURN_SCREEN_NAME(booster_select)
				RETURN_SCREEN_NAME(booster_buy)
				RETURN_SCREEN_NAME(pause)
				RETURN_SCREEN_NAME(luckydraw1)
				RETURN_SCREEN_NAME(luckydraw2)
				RETURN_SCREEN_NAME(inbox)
				RETURN_SCREEN_NAME(ranking)
				RETURN_SCREEN_NAME(fb_invite)
				RETURN_SCREEN_NAME(ranking_reward)
				RETURN_SCREEN_NAME(character_main)
				RETURN_SCREEN_NAME(character_detail)
				RETURN_SCREEN_NAME(character_ability)
				RETURN_SCREEN_NAME(tutorial)
				
			case Analytics::app_launch:		return "app_launch";
			case Analytics::main_screen:    return "main_screen";
            case Analytics::game:			return "game";
            case Analytics::game_over:		return "game_over";
            case Analytics::revive_video:	return "revive_video";
            case Analytics::suit:           return "suit";
            case Analytics::mastery:        return "mastery";
            case Analytics::coin_shop:      return "coin_shop";
            case Analytics::appstore:       return "appstore";
            case Analytics::setting:        return "setting";
            case Analytics::gm:             return "gm";
			case Analytics::dog_select:     return "dog_select";
			case Analytics::dog_info:       return "dog_info";
			case Analytics::credit:         return "credit";
			default:						return StringUtils::format("screen_%d", screen);
		}
        

	}
	
	std::string getLevelString(int level) {
		return StringUtils::format("lv_%d", level);
	}
}


#pragma mark -

static Analytics *sInstance = nullptr;


Analytics *Analytics::instance(){
	if(sInstance == nullptr){
		sInstance = new Analytics();
	}
	
	return sInstance;
}


Analytics::Analytics()
: mDebug(false)
, mEnabled(kIsEnabled)
, mHitMapHash()
{
	
}

void Analytics::setup()
{
#ifdef SDKBOX_ENABLED
	sdkbox::PluginGoogleAnalytics::init();
	
	setupCustomDimension();
#endif
}


void Analytics::setupCustomDimension()
{
#ifdef SDKBOX_ENABLED
	
	std::string value = "";  ///  // TODO
	
	// Paid Status of the user
	// value =  getPaidStatusValue();
	sdkbox::PluginGoogleAnalytics::setDimension(kDimensionIndexPaidStatus, value);
	
	// Is this session a debug
	value = GameManager::instance()->isDebugMode() ? "true" : "false";
	sdkbox::PluginGoogleAnalytics::setDimension(kDimensionIndexDebugVersion, value);
	
#endif
}



void Analytics::logEvent(Category category, const std::string &action, const std::string &label, int value)
{
	std::string categoryName = getCategoryString(category);
	logGAEvent(categoryName, action, label, value);
}

void Analytics::logEvent(Category category, Action action, const std::string &label, int value)
{
	std::string categoryName = getCategoryString(category);
	std::string actionName = getActionString(action);
	std::string labelValue = getCategoryLabel(category, label);
	int finalValue = getCategoryValue(category, value);
    	
	logGAEvent(categoryName, actionName, labelValue, finalValue);

}

void Analytics::logScreen(Screen screen)
{
#ifdef SDKBOX_ENABLED
	if(kIsEnabled) {
		sdkbox::PluginGoogleAnalytics::logScreen(getScreenString(screen));
	}
	
#endif
}


void Analytics::logGAEvent(const std::string &category,
						   const std::string &action,
						   const std::string &label,
						   int value)
{
	if(mDebug) {
		log("debug:[logGA]: cat=%s action=%s label=%s value=%d", category.c_str(), action.c_str(), label.c_str(), value);
	}
	
#ifdef SDKBOX_ENABLED
	if(mEnabled) {
		sdkbox::PluginGoogleAnalytics::logEvent(category, action, label, value);
	}
#endif
}

std::string Analytics::getCategoryLabel(Category category, std::string originLabel)
{
	if(originLabel == "") {
		return getCurrentPlanetName();
	}

	switch(category) {
		case in_game:
		case end_game:
		case main:
		case game_map:
		case collect_coin:
			return getCurrentPlanetName();
        case tutorial:
            return originLabel;
        default:
			return (originLabel == "") ? getCurrentPlanetName() : originLabel;
	}
	
	
	return originLabel;
}

int Analytics::getCategoryValue(Category category, int originValue)
{
    
    switch(category) {
        case in_game:
            return originValue;
        case end_game:
            return originValue;
        case game_map:
            return originValue;
        case main:
            return originValue;
        case collect_coin:
            return originValue;
        case upgrade_suit:
            return originValue;
        case upgrade_mastery:
            return originValue;
        case tutorial:
            return originValue;
        default:
            return originValue;
    }
    
    
    return originValue;
}

void Analytics::logGameContinue(const std::string &action, int value)
{
	std::string label = getCurrentCharacterName();
	
	logGAEvent(getCategoryString(Category::game_continue), action, label, value);
	
	
}


void Analytics::logSuitUpgrade(int suitID, int upgradeLevel, int moneySpent)
{
	std::string action = GameManager::instance()->getSuitManager()->getSuitName(suitID);
	std::string label = getLevelString(upgradeLevel);
	
	logGAEvent(getCategoryString(Category::upgrade_suit), action, label, moneySpent);
}

void Analytics::logSuitUse(int suitID)
{
	std::string action = GameManager::instance()->getSuitManager()->getSuitName(suitID);
	
	int level = GameManager::instance()->getSuitManager()->getPlayerSuitLevel(suitID);
	std::string label = getLevelString(level);
	
	logGAEvent(getCategoryString(Category::use_suit), action, label, 0);
}

void Analytics::logMastery(int masteryID, int upgradeLevel, int moneySpent)
{
	std::string action = GameManager::instance()->getMasteryManager()->getMasteryName(masteryID);
	std::string label = getLevelString(upgradeLevel);
	
	logGAEvent(getCategoryString(upgrade_mastery), action, label, moneySpent);
}

void Analytics::logDieMap(int mapID, int travelDistance)
{
	//log("debug: logDieMap: mapID=%d", mapID);
	std::string action = StringUtils::format("map_%06d", mapID);
	
	std::string label = getCurrentPlanetName();
	
	logGAEvent(getCategoryString(Category::game_map), action, label, travelDistance);
}

void Analytics::logPayment(const std::string &productName, float price)
{
	logGAEvent(getCategoryString(Category::payment), productName, "hkd", (int) price);
}


std::string Analytics::getCurrentSuitName(bool includeLevel)
{
    //log("getCurrentSuitName: %s" ,GameManager::instance()->getSuitManager()->getSelectedSuitString(includeLevel).c_str());
    return GameManager::instance()->getSuitManager()->getSelectedSuitString(includeLevel);
    
}

int Analytics::getCurrentDistance()
{
	return GameWorld::instance()->getPlayerDistance();
}


std::string Analytics::getCurrentCharacterName()
{
	int charID = PlayerManager::instance()->getSelectedChar();
	
	return PlayerManager::instance()->getCharName(charID);
}


std::string Analytics::getCurrentPlanetName()
{
	int selected = PlanetManager::instance()->getSelectedPlanet();
	return PlanetManager::instance()->getPlanetName(selected);
}


void Analytics::logTutorial(const std::string &name, int step, float timestamp)
{
	Category category = Category::tutorial;
	
	// define the actionName
	std::string labelName;
	if(step == 0) {
		labelName = "start";
	} else if(step == -1) {
		labelName = "last_step";
	} else {
		labelName = StringUtils::format("step_%d", step);
	}
	
	logGAEvent(getCategoryString(category), name, labelName, (int) (timestamp * 100));
}

void Analytics::logCollect(Action action, int value, MoneyType type, const std::string &label)
{
	if(value <= 0) {
		return;
	}
	
	Category cat = getCollectCategory(type);
	
	logEvent(cat, action, label, value);
}

void Analytics::logConsume(Action action, const std::string &productName,
							int value, MoneyType type)
{
	if(value <= 0) {
		return;
	}
	
	std::string labelName;
	
	if(action == Action::continue_game) {
		labelName = getCurrentPlanetName();
	} else {
		labelName = productName;
	}
	
	//
	logEvent(getConsumeCategory(type), action, labelName, value);
}


void Analytics::logCollectBooster(Action action, const int &boosterType, int boosterCount)
{
	if(boosterCount <= 0) {
		return;
	}
	
	std::string name = getBoosterName(boosterType);
	logEvent(Category::collect_booster, action, name, boosterCount);
}

void Analytics::logUseBooster(const int &boosterType, int boosterCount)
{
	if(boosterCount <= 0) {
		return;
	}
	
	std::string name = getBoosterName(boosterType);
	
	logEvent(Category::use_booster, Action::act_use_booster, name, boosterCount);
}


void Analytics::setDebug(bool flag)
{
	mDebug = flag;
	
	mEnabled = flag == false;
}


void Analytics::logDogMenu(Action action, int dogID)
{
	std::string label = DogManager::instance()->getDogName(dogID);
	
	
	logEvent(Category::dog_menu, action, label, 0);
}


std::string Analytics::getBoosterName(int itemType)
{
	return ItemManager::instance()->getBoosterName((BoosterItemType) itemType);
}


void Analytics::logGameStage()
{
	std::string action = getCurrentPlanetName();
	std::string label = getCurrentCharacterName();
	
	logEvent(Category::game_stage, action, label, 0);
}


void Analytics::resetPlayerHitData()
{
	mHitMapHash.clear();
}

void Analytics::logPlayerHit(const int &mapID)
{
	if(mHitMapHash.find(mapID) != mHitMapHash.end()) {
		return;		// No need to log
	}
	
	std::string action = StringUtils::format("map_%06d", mapID);
	std::string label = getCurrentCharacterName();
	
	logEvent(Category::player_hit, action, label, 0);
}
