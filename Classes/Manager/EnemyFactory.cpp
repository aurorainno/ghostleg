//
//  EnemyFactory.cpp
//  GhostLeg


//
//  Created by Ken Lee on 24/3/2016.
//
//

#include "EnemyFactory.h"
#include "Enemy.h"
#include "EnemyData.h"

#include "MoveStraightEnemy.h"

#include "EnemyBehaviour.h"
#include "ObstacleBehaviour.h"
#include "NormalBehaviour.h"
#include "FreeMoveBehaviour.h"
#include "AutoHideBehaviour.h"
#include "DropStuffBehaviour.h"
#include "ReduceEnergyBehaviour.h"
#include "SlowPlayerBehaviour.h"
#include "ThiefBehaviour.h"
#include "SuddenMoveBehaviour.h"
#include "PatrolBehaviour.h"
#include "AttackBehaviour.h"
#include "JumpBehaviour.h"
#include "FireBulletBehaviour.h"
#include "BulletBehaviour.h"
#include "JSONHelper.h"
#include "BomberBehaviour.h"
#include "ChaserBehaviour.h"

#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

const int kTypeMoveStraight = 101;

static EnemyFactory *sInstance = nullptr;


namespace {
	int parseIDFromName(const std::string &name) {
		// std::string name = itr->name.GetString();
		int result;
		
		sscanf(name.c_str(), "enemy_%d", &result);
	
		return result;
	}
	
	
	bool getBool(const rapidjson::Value &jsonValue, const char *name, bool defaultValue)
	{
		if(jsonValue.HasMember(name) == false) {
			return defaultValue;
		}
		
		return jsonValue[name].GetBool();
	}
    
    float getFloat(const rapidjson::Value &jsonValue, const char *name, float defaultValue)
    {
        if(jsonValue.HasMember(name) == false) {
            return defaultValue;
        }
        
        return jsonValue[name].GetDouble();
    }
	
	std::string getString(const rapidjson::Value &jsonValue, const char *name,
						  const std::string &defaultValue = "")
	{
		if(jsonValue.HasMember(name) == false) {
			return defaultValue;
		}
		
		return jsonValue[name].GetString();
	}
	
	void getJsonIntArray(const rapidjson::Value &arrayValue, std::vector<int> &outArray)
	{
		if(arrayValue.IsArray() == false) {
			return;
		}
		
		for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
		{
			outArray.push_back(arrayValue[i].GetInt());
		}
	}
	
	void getJsonFloatArray(const rapidjson::Value &arrayValue, std::vector<float> &outArray)
	{
		if(arrayValue.IsArray() == false) {
			return;
		}
		
		for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
		{
			outArray.push_back((float) arrayValue[i].GetDouble());
		}
	}
	
	
	void getJsonStrMap(const rapidjson::Value &jsonValue, std::map<std::string, std::string> &outMap)
	{
		
		for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
			 itr != jsonValue.MemberEnd();
			 ++itr)
		{
			std::string key = itr->name.GetString();
			const rapidjson::Value &object = jsonValue[key.c_str()];
			std::string value = object.GetString();
			
			//log("getJsonStrMap: key=%s value=%s", key.c_str(), value.c_str());
		}
		
		//jsonValue.
		
//		for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
//		{
//			outArray.push_back((float) arrayValue[i].GetDouble());
//		}
	}
	
	HitboxType parseHitboxType(const std::string &name) {
		if("simple" == name) {
			return HitboxSimple;
		} else if("fix" == name) {
			return HitboxFixed;
		} else {
			return HitboxDynamic;
		}
	}
}

EnemyFactory *EnemyFactory::instance()
{
	if(!sInstance) {
		sInstance = new EnemyFactory();
	}
	return sInstance;
}

EnemyFactory::EnemyFactory()
: mObjectID(1)
, mEffectStatus(EffectStatusNone)
, mDefaultEnemyData(nullptr)
{
	setupDefaultEnemyData();
}

EnemyFactory::~EnemyFactory()
{
	CC_SAFE_RELEASE(mDefaultEnemyData);
}

Vector<EnemyData *> EnemyFactory::loadEnemyDataListFromJson(const std::string &jsonData)
{
	Vector<EnemyData *> result;
	
	std::string content = FileUtils::getInstance()->getStringFromFile(jsonData);
	
	rapidjson::Document jsonDoc;
	jsonDoc.Parse(content.c_str());
	
	if (jsonDoc.HasParseError())
	{
		CCLOG("loadEnemyDataListFromJson: json error. %d\n", jsonDoc.GetParseError());
		return result;
	}
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonDoc.MemberBegin();
		 itr != jsonDoc.MemberEnd();
		 ++itr)
	{
		std::string name = itr->name.GetString();
		
		int defaultID = parseIDFromName(name);
		
		const rapidjson::Value &object = jsonDoc[name.c_str()];
		
		int enemyID = defaultID;
		
		std::string defaultBehaviour = enemyID < 100 ? "normal" : "obstacle";
		
		std::string behaviour = aurora::JSONHelper::getString(object, "behaviour", defaultBehaviour); // object["behaviour"].GetString();
		
		int resID = aurora::JSONHelper::getInt(object, "res", enemyID);
		std::string activeCond = aurora::JSONHelper::getString(object, "active", "");
        
        bool isDestroyable = object.HasMember("destroyable") ? object["destroyable"].GetBool() : enemyID < 100;
		
		bool hideAtStart = aurora::JSONHelper::getBool(object, "hideAtStart", false);
		
		bool defaultTrapValue = enemyID < 100 ? false : true;
		bool isTrap = aurora::JSONHelper::getBool(object, "isTrap", defaultTrapValue);
		
		// Object Tag
		std::string defaultTag = enemyID < 100 ? "enemy" : "obstacle";
		std::string tagStr = aurora::JSONHelper::getString(object, "tag", defaultTag);
		ObjectTag tag = EnemyData::parseTagStr(tagStr);
		
		
		
		// Get the alert Type
		std::string alertTypeString = getString(object, "alertType", "");
        AlertType alertType;
        if(alertTypeString.compare("model") == 0){
            alertType = AlertType::AlertOnModel;
        } else if(alertTypeString.compare("screen") == 0){
			alertType = AlertType::AlertOnScreen;
			
		} else {
			alertType = AlertType::AlertNone;
        }
		
		// HitboxType
		std::string hitboxStr = aurora::JSONHelper::getString(object, "hitbox", "");
		HitboxType hitboxType = parseHitboxType(hitboxStr);
		
		
		//
		
		std::vector<float> speedXArray;
		std::vector<float> speedYArray;
		
		aurora::JSONHelper::getJsonFloatArray(object, "speedX", speedXArray);
		aurora::JSONHelper::getJsonFloatArray(object, "speedY", speedYArray);
		
		float accelerX = aurora::JSONHelper::getFloat(object, "accelX", 0);
		float accelerY = aurora::JSONHelper::getFloat(object, "accelY", 0);
		

		// Property
		std::map<std::string, std::string> propertyMap;
        std::map<std::string, std::vector<std::string>> sfxMap{};
        std::string key = "";
        
        if(object.HasMember("dropSetting")){
            key = "dropSetting";
        }else if(object.HasMember("patrolSetting")){
            key = "patrolSetting";
		}else if(object.HasMember("behaviourSetting")){
			key = "behaviourSetting";
        }else if(object.HasMember("attackSetting")){
            key = "attackSetting";
        }else if(object.HasMember("jumpSetting")){
            key = "jumpSetting";
        }else if(object.HasMember("fireBulletSetting")){
            key = "fireBulletSetting";
        }else if(object.HasMember("bomberSetting")){
            key = "bomberSetting";
        }else if(object.HasMember("bulletSetting")){
            key = "bulletSetting";
        }
        
		aurora::JSONHelper::getJsonStrMap(object, key, propertyMap);
        
        aurora::JSONHelper::getJsonStrStrListMap(object, "sfx", sfxMap);
        

		//getJsonStrMap(object[key.c_str()], propertyMap);
		
		
		// Move Behaviour
		bool isFreeMove = aurora::JSONHelper::getBool(object, "isFreeMove", false);
		
		
		//
		
		
		// Construct the EnemyData
		EnemyData *data = EnemyData::create();
		
		data->setType(enemyID);
		data->setRes(resID);
		data->setTrap(isTrap);
		data->setTag(tag);
		data->setBehaviour(EnemyBehaviour::getBehaviourType(behaviour));
		data->setHideAtStart(hideAtStart);
		data->setHitboxType(hitboxType);
		
		EnemyBehaviour::parseActiveCondition(data, activeCond);
		data->setSpeedXArray(speedXArray);
		data->setSpeedYArray(speedYArray);
        data->setAccelerX(accelerX);
        data->setAccelerY(accelerY);
        data->setAlertType(alertType);
		data->setMapProperty(key, propertyMap);
        data->setSfxMap(sfxMap);
        
        data->setDestroyable(isDestroyable);
		data->setFreeMove(isFreeMove);
		
		result.pushBack(data);
	}
	

	return result;
}


void EnemyFactory::loadData()
{
	loadDataFromJson();
}

void EnemyFactory::loadDataFromJson()
{
	mEnemyDataMap.clear();
	Vector<EnemyData *> dataList = loadEnemyDataListFromJson("json/enemy.json");
	
	for(int i=0; i<dataList.size(); i++) {
		EnemyData *data = dataList.at(i);
		
		//log("%s", data->toString().c_str());
		mEnemyDataMap.insert(data->getType(), data);
	}
}

void EnemyFactory::loadDataFromDat()
{
	std::string content = FileUtils::getInstance()->getStringFromFile("json/enemy.dat");
	
	// log("Content:\n%s\n", content.c_str());
	
	std::stringstream ss(content);	// turn input to stream
	std::string line;
	
	while(std::getline(ss, line, '\n')) {
		//	log("line [%s]", line.c_str());
		if(line.length() == 0) {
			continue;
		}
		if(line.at(0) == '#') {
			continue;
		}
		
		EnemyData *data = EnemyData::create();
		data->parse(line.c_str());
		
		mEnemyDataMap.insert(data->getType(), data);
	}
}

std::string EnemyFactory::infoEnemyData()
{
	std::string result = "";
	
	std::vector<int> mapKeyVec = mEnemyDataMap.keys();
	
	
	for(int key : mapKeyVec)
	{
		EnemyData *data  = getEnemyDataByType(key);
		result += data->toString();
		result += "\n";
	}
	
	return result;
}

EnemyData *EnemyFactory::getEnemyDataByType(int type)
{
	return mEnemyDataMap.at(type);
}

Enemy *EnemyFactory::createByType(int type)
{
	switch (type) {
		case kTypeMoveStraight:		return MoveStraightEnemy::create();
		default:					return Enemy::create();
	}
}

Enemy *EnemyFactory::createEnemy(int monsterID)
{
	// Object creation
	Enemy *enemy = Enemy::create();
	enemy->setObjectID(nextObjectID());
	
	// Attach a behaviour to the enemy
	EnemyBehaviour *behaviour = createBehaviourByMonsterID(monsterID);
	if(behaviour) {
		behaviour->setEnemy(enemy);
		enemy->setBehaviour(behaviour);
		//enemy->setResource(behaviour->getRes());
		
		// For debug:
		int resID = behaviour->getRes();
		// if(resID < 100) {	resID = 1;	}
		//enemy->setResource(resID);
		enemy->setResID(resID);
	} else {
		log("createEnemy: error: changing the id [%d] -> [%d]", monsterID, 1);
		//enemy->setResource(1);	// resID = monsterId
		enemy->setResID(1);
	}
	
	// enemy->setupAttribute();
	
	if(mEffectStatus != EffectStatusNone) {
		enemy->setEffectToAdd(mEffectStatus);
	}

	
	return enemy;
}

EffectStatus EnemyFactory::getCurrentEffectStatus()
{
    return mEffectStatus;
}

void EnemyFactory::setDefaultEffectStatus(EffectStatus status)// monsterID = resID
{
	mEffectStatus = status;
}

void EnemyFactory::resetDefaultEffectStatus()
{
	mEffectStatus = EffectStatusNone;
}

EnemyBehaviour *EnemyFactory::createBehaviourByMonsterID(int monsterID)// monsterID = resID
{
	EnemyData *data = getEnemyDataByType(monsterID);
	if(data == nullptr) {
		log("createBehaviourByMonsterID: missing data. id=%d", monsterID);
		return nullptr;
	}
	EnemyBehaviour *behaviour = createBehaviourByType(data->getBehaviour());
	
	if(behaviour) {
		behaviour->setData(data);
	}
	
	return behaviour;
}

EnemyBehaviour *EnemyFactory::createBehaviourByType(EnemyBehaviour::BehaviourType type)
{
	// TODO: Different Behaviour
	switch(type) {
		case EnemyBehaviour::Normal			: return NormalBehaviour::create();
		case EnemyBehaviour::Obstacle		: return ObstacleBehaviour::create();
		case EnemyBehaviour::FreeMove		: return FreeMoveBehaviour::create();
		case EnemyBehaviour::AutoHide		: return AutoHideBehaviour::create();
		case EnemyBehaviour::DropStuff		: return DropStuffBehaviour::create();
		case EnemyBehaviour::ReduceEnergy	: return ReduceEnergyBehaviour::create();
		case EnemyBehaviour::SlowPlayer		: return SlowPlayerBehaviour::create();
		case EnemyBehaviour::Thief			: return ThiefBehaviour::create();
        case EnemyBehaviour::Patrol         : return PatrolBehaviour::create();
        case EnemyBehaviour::Attack         : return AttackBehaviour::create();
		case EnemyBehaviour::SuddenMove		: return SuddenMoveBehaviour::create();
        case EnemyBehaviour::Jump           : return JumpBehaviour::create();
        case EnemyBehaviour::FireBullet     : return FireBulletBehaviour::create();
        case EnemyBehaviour::Bullet         : return BulletBehaviour::create();
        case EnemyBehaviour::Bomber         : return BomberBehaviour::create();
        case EnemyBehaviour::Chaser         : return ChaserBehaviour::create();
        default: {
			return NormalBehaviour::create();
		}
	}
}


void EnemyFactory::resetObjectID()
{
	mObjectID = 1;
}

int EnemyFactory::nextObjectID()
{
	int returnValue = mObjectID;
	
	mObjectID++;
	
	return returnValue;
}


void EnemyFactory::reset()
{
	resetObjectID();
	setDefaultEffectStatus(EffectStatusNone);
}


void EnemyFactory::setupDefaultEnemyData()
{
	mDefaultEnemyData = new EnemyData();
	
	//mDefaultEnemyData.bev
}

std::string EnemyFactory::infoDefaultEnemyData()
{
	return mDefaultEnemyData->toString();
}
