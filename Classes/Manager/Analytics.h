//
//  Analytics.h
//  GhostLeg
//
//	This is a Singleton class!
//
//  Created by Ken Lee on 6/7/2016.
//
//

#ifndef Analytics_hpp
#define Analytics_hpp

#include <stdio.h>
#include <map>

#include "cocos2d.h"
#include "CommonType.h"

USING_NS_CC;

#define LOG_SCREEN(__screen__)	Analytics::instance()->logScreen(__screen__);
#define LOG_EVENT(_cat_, _action_, _label_, _value_)	\
	Analytics::instance()->logEvent(_cat_, _action_, _label_, _value_);



class Analytics
{
public: // enum
	enum Category {
		in_game,
		end_game,
        game_map,
        main,
		
        collect_coin,
		consume_coin,
		
		collect_diamond,
		consume_diamond,
		
		collect_puzzle,
		consume_puzzle,
		
		collect_booster,
		use_booster,

		tutorial,
		payment,
		game_continue,

		game_stage,
		player_hit,
		
		// Old
		upgrade_suit,
		use_suit,
        upgrade_mastery,
		testing,
		dog_menu,
	};
	
	enum Action {
		on_enter,
        tap_1st_line,
        tap_2nd_line,
        collect_energy,
        collect_capsule,
        use_capsule,
        play_again,
        revive,
        back_main,
        share,
		
        suit_upgrade,
        suit_use,
        tutorial_start,
        tutorial_step1,
        tutorial_step2,
        tutorial_step3,
        tutorial_step4,
        tutorial_step5,
        tutorial_end,
		
		// Collect coin
		play_game,
		from_appstore,
		ad_reward,
		bonus_star,
		daily_reward,
		
		// Consume star
		unlock_dog,
		continue_game,
		

		// Main Screen Action
		start_game,
		click_coin_shop,
		click_mastery,
		click_suit,
		click_fb,
		click_setting,
		click_gmtool,
		change_planet,
		click_dogmenu,
		click_leaderboard,
		show_rate_us,
		
		// Dog Menu
		click_select_dog,
		click_unlock_dog,
		click_dog_detail,
		click_unlock_in_detail,
		click_select_in_detail,
		
		
		
		
		// ---------------
		// New Action
		// ---------------
		// For Booster
		
		
		
		// For Resource / Booster Collection
		
		from_game,
		from_diamond,
		from_iap,
		from_free_coin,
		from_lucky_draw,
		from_double_up,
		from_daily_reward,
		from_mail,
		from_booster_shop,
		
		// For Resource Consumption
		act_unlock_char,
		act_upgrade_char,
		act_continue_game,
		act_unlock_stage,
		act_lucky_draw,
		act_buy_coin,
		act_buy_booster,
		act_use_booster,
	};
	
	enum Screen {
		app_launch,
		main_screen,
		game,
		game_over,
		coin_shop,
		appstore,
		setting,
		gm,
		
		
        revive_video,
        suit,
        mastery,
		dog_select,
		dog_info,
		credit,
		
		
		scn_title_screen,
		scn_game_continue,
		scn_coin_shop,
		scn_booster_select,
		scn_booster_buy,
		scn_pause,
		scn_luckydraw1,
		scn_luckydraw2,
		scn_inbox,
		scn_ranking,
		scn_fb_invite,
		scn_ranking_reward,
		scn_character_main,
		scn_character_detail,
		scn_character_ability,
		scn_tutorial,

	};
	
public:
	static Analytics *instance();
	Analytics();
	void setup();
	
	CC_SYNTHESIZE(bool, mEnabled, Enabled);
	
	void logEvent(Category category, Action action, const std::string &label="", int value=0);
	void logEvent(Category category, const std::string &action,  const std::string &label="", int value=0);
	void logScreen(Screen screen);
	
	void logGameContinue(const std::string &action, int value);
	void logPayment(const std::string &productName, float price);
	void logTutorial(const std::string &name, int step, float timestamp);
	
	
	void logGameStage();
	//void logPlayerHit(int )
	
	void logCollect(Action action, int value, MoneyType type=MoneyTypeStar, const std::string &label="");
	void logConsume(Action action, const std::string &productName, int value, MoneyType type=MoneyTypeStar);
	
	void logCollectBooster(Action action, const int &itemType, int boosterCount);
	void logUseBooster(const int &itemType, int boosterCount);
	
	void logDieMap(int mapID, int travelDistance);
	
	// Deprecated
	
	void logDogMenu(Action action, int dogID);
	void logSuitUpgrade(int suitID, int upgradeLevel, int moneySpent);
	void logSuitUse(int suitID);
	void logMastery(int masteryID, int upgradeLevel, int moneySpent);

	
	
	// Helper Method for defining label and value
	std::string getCurrentSuitName(bool includeLevel);
	std::string getCurrentCharacterName();
	std::string getBoosterName(int itemType);
	std::string getCurrentPlanetName();
	int getCurrentDistance();
	
	void setDebug(bool flag);
	
	void resetPlayerHitData();
	void logPlayerHit(const int &mapID);
	
protected:
	void logGAEvent(const std::string &category, const std::string &action, const std::string &label, int value);
	
	
	void setupCustomDimension();
	std::string getCategoryLabel(Category category, std::string originLabel);
	int getCategoryValue(Category category, int originValue);


	bool mDebug;

private:
	std::map<int, bool> mHitMapHash;
};

#endif /* Analytics_hpp */

