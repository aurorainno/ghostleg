//
//  LuckyDrawManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#ifndef LuckyDrawManager_hpp
#define LuckyDrawManager_hpp

#include <stdio.h>

#include "cocos2d.h"

#include "PlayerBooster.h"
#include "CommonType.h"
#include "LuckyDrawReward.h"
#include "Price.h"

USING_NS_CC;

class LuckyDrawManager
{
public: // Type and Enum
	static LuckyDrawManager *instance();
	
private:
	LuckyDrawManager();
	~LuckyDrawManager();
	
#pragma mark - General
public:
	void setup();
	std::string infoRewardList();
	
	LuckyDrawReward *roll();
    LuckyDrawReward *generateReward();
    
	Price getDrawPrice();
	
	
	
private:
	void loadRewardData();
	void setupMockData();
	void setupChanceRange();
	void setupDefaultReward();
	void logReward(LuckyDrawReward *reward, bool isDoubleUpReward);
	bool hasEnoughMoney();
	
private:
	Vector<LuckyDrawReward *> mRewardList;
	CC_SYNTHESIZE_RETAIN(LuckyDrawReward *, mDefaultReward, DefaultReward);
	Price mPrice;
	
#pragma mark - Reward Collection
public:
	void collectReward(LuckyDrawReward *reward, bool isFree);
    void collectRewardForFree(LuckyDrawReward *reward, bool isCollectFromAd);
    
private:
	void collectFragmentReward(int fragmentID,int count);
	void collectStarReward(int starAmount);
	void collectBoosterReward(int boosterID, int count);
};

#endif /* LuckyDrawManager_hpp */
