//
//  NPCManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/3/2017.
//
//

#include "NPCManager.h"
#include "NPCOrder.h"
#include "NPCRating.h"
#include "GameWorld.h"
#include "GameplaySetting.h"
#include "StageManager.h"
#include "GameManager.h"
#include "PlayerManager.h"


static NPCManager *sInstance = nullptr;

const int kPerClientScore = 900;
const int kNoCrashBonus = 1000;

NPCManager *NPCManager::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new NPCManager();
	
	return sInstance;
}

NPCManager::NPCManager()
: mRatingList()
, mCrashCount(0)
{
	
}

NPCManager::~NPCManager()
{
	
}

void NPCManager::setup()
{
    loadRatingMessage();
}


std::string NPCManager::getRatingComment(int rating)
{
    std::vector<std::string> list = mRatingMessageHash[rating];
    float random = CCRANDOM_0_1();
    int index = (list.size() - 1) * random;
    return list.at(index);
}




#pragma mark - Npc Rating
int NPCManager::calculateNpcRating(NPCOrder *order, float timeUsed)
{
	// Note: the follow value should be based on Game Design
	if(timeUsed > 100) {	// spend more than 100s
		return 1;
	}
	
	if(timeUsed > 80) {	// 80 ~ 100
		return 2;
	}
	
	if(timeUsed > 60) {	// 60 ~ 80
		return 3;
	}
	
	if(timeUsed > 50) {	// 50 ~ 60
		return 4;
	}
	
	return 5;
}


// Reference documents
// https://docs.google.com/document/d/1q-MxpNAk1AeGy5D-K_rPAxGTaHLkpCC-0ZSUUxstQIw/edit#
void NPCManager::calculateScore(NPCRating *rating, NPCOrder *npcOrder)
{
	if(rating == nullptr) {
		return;
	}
	
	if(npcOrder == nullptr) {
		return;
	}
	
	// Score Calculation
	float baseScore = 500;
	
//	float time = rating->getTimeUsed();
//	if(time < 1.0) {		// Protection of divide by zero
//		time = 1.0;
//	}
//	float scoreWeight = 50.0 / time;
//	int score = ((int) ceilf(baseScore * scoreWeight);
	float remain = 180 - rating->getTimeUsed();
	if(remain <= 0)  { remain = 0; }
	int bonusScore = remain * 10;
	
	
	int score = (int) baseScore + bonusScore;
	
	// Update to rating
	rating->setScore(score);
}

int NPCManager::calculateTip(NPCRating *rating, NPCOrder *npcOrder)
{
	if(rating == nullptr) {
		return 0;
	}
	
	
	// Base Tip
	float ratio = npcOrder->getFinalBonusTipRatio();
	float tip = ratio * (rating->getRating() - 1) * rating->getBaseReward();	// ??? Base or Final
	int baseTip = (int) ceilf(tip);
	
	
	// Bonus Tip
	//		By ExtraReward for the rating
	int ratingValue = rating->getRating();
	int bonusTip = GameWorld::instance()->getGameplaySetting()->getExtraReward(ratingValue);
	
	//		By the BonusTip Percent
	int extraTip = (int)((float) baseTip * npcOrder->getBonusTipRatio());
	bonusTip += extraTip;
	
	
	rating->setTipValue(baseTip, bonusTip);
	
	return bonusTip;
}

// ken: deprecated
void NPCManager::rewardByNPCRating(NPCRating *rating)
{
	//if
	if(rating == nullptr) {
		return;
	}
	
	// Reward the money
	int totalReward = rating->getTotalReward();		// include reward & tips
	GameManager::instance()->addMoney(MoneyTypeStar, totalReward);
	
	// Reward the fragment
	if(rating->getFragChar() > 0 && rating->getFragCount()) {
		PlayerManager::instance()->addPlayerFragment(rating->getFragChar(), rating->getFragCount());
	}
	
	// Reward the Score
	//GameWorld::instance()->add
	//GameWorld::instance()->addScore(rating->getScore());
	
}

NPCRating *NPCManager::addNpcRating(NPCOrder *order, float timeUsed)
{
	if(order == nullptr) {
		log("NPCManger.addNpcRating: order is null");
		return nullptr;
	}
	
	int remainTime = 100 - (int) timeUsed;
	
	int ratingValue = calculateNpcRating(order, timeUsed);
	
	
	// Prepare the NPCRating object
	NPCRating *rating = NPCRating::create();
	
	rating->setNpcID(order->getNpcID());
	rating->setTimeUsed(timeUsed);
	rating->setRemainTime(remainTime);
	rating->setRating(ratingValue);
	rating->setFragChar(order->getFragChar());
	rating->setFragCount(order->getFragReward());
	

	calculateReward(rating, order);
	calculateScore(rating, order);
	
	log("Final Rating Data: %s", rating->toString().c_str());
	
	
	// Add to the list
	mRatingList.pushBack(rating);
	
	
	return rating;
	
}

void NPCManager::calculateReward(NPCRating *rating, NPCOrder *npcOrder)
{
	int baseReward = npcOrder->getBaseReward();
	
	// just baseReward in tutorial mode
	if(npcOrder->isTutorialNpc()) {
		rating->setRewardValue(baseReward, 0);
		return;
	}
	

	float rewardRatio = rating->getRating() * 0.2;
	
	int reward = (int) ceilf(rewardRatio * baseReward);
	
	//int bonusByTip =
	
	int bonus = (int)(npcOrder->getBonusTipRatio() * reward);
	bonus += npcOrder->getBonusReward();
	
	
	rating->setRewardValue(reward, bonus);
}

void NPCManager::resetNpcRating()
{
	mRatingList.clear();
	mCrashCount = 0;
}

std::string NPCManager::infoNpcRating()
{
	std::string info = StringUtils::format("npcRating count: %ld\n", mRatingList.size());
	
	for(NPCRating *rating : mRatingList) {
		info += rating->toString();
		info += "\n";
	}
	
	return info;
}

Vector<NPCRating *> NPCManager::getRatingList()
{
	Vector<NPCRating *> result;
	
	for(NPCRating *rating : mRatingList) {
		result.pushBack(rating);
	}
	
	return result;
}


int NPCManager::getTotalCollected()
{
	int total = 0;
	for(NPCRating *rating : mRatingList) {
		total += rating->getReward();
	}
	
	total += GameWorld::instance()->getPlayerCollectedCoins();
	
	return total;
}

int NPCManager::getTotalTip()
{
	int total = 0;
	for(NPCRating *rating : mRatingList) {
		total += rating->getTip();
	}
	
	return total;
}

int NPCManager::getClientCount()
{
	return (int) mRatingList.size();
}

int NPCManager::getClientScore()
{
	return kPerClientScore;
}

int NPCManager::getNoCrashScore()
{
	return getCrashCount() == 0 ? kNoCrashBonus : 0;
}

int NPCManager::getTotalScore()
{
	int total = 0;
	
	total += getTotalCollected();
	total += getNoCrashScore();
	total += getClientScore() *  getClientCount();
	total += getTotalTip();
	
	return total;
}

void NPCManager::loadRatingMessage()
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/rating_message.json");
    
    // log("rating_message json content:\n%s", content.c_str());
    rapidjson::Document jsonArray;
    jsonArray.Parse(content.c_str());
    if (jsonArray.HasParseError())
    {
        CCLOG("loadRatingMessage: GetParseError %d\n", jsonArray.GetParseError());
        return;
    }
    
    mRatingMessageHash.clear();
    
    for(rapidjson::Value::ConstMemberIterator iter = jsonArray.MemberBegin();
        iter != jsonArray.MemberEnd(); ++iter)
    {
        int index;
        std::string name = iter->name.GetString();
        sscanf(name.c_str(), "rating_%d", &index);
        
        const rapidjson::Value &commentArray = jsonArray[name.c_str()];
        std::vector<std::string> commentList{};
        
        for(rapidjson::SizeType i=0;i<commentArray.Size();i++){
            const rapidjson::Value &currentJsonComment = commentArray[i];
            std::string comment = currentJsonComment.GetString();
            commentList.push_back(comment);
        }
        
        mRatingMessageHash[index] = commentList;
    }
}
