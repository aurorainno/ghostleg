//
//  SuitManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/6/2016.
//
//

#ifndef SuitManager_hpp
#define SuitManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <string>
#include <iostream>

#include "Suit.h"


USING_NS_CC;

#include "external/json/document.h"

class ItemEffect;

class Player;


class SuitManager
{
public:
	SuitManager();
	
	void loadSuitData();
	void loadPlayerSuit();
	void loadPlayerSelectedSuit();
	
	void savePlayerSuit();
	
	
	Vector<SuitData *> getSuitArray();
	int getPlayerSuitLevel(int suitID);
	void setPlayerSuitLevel(int suitID, int level);
	int upgradeSuit(int suitID);					// success or not, return -3 if nullptr, return -2 if max lvl,
	// return -1 if not enough stars, return 0 if success.
	void resetAllSuit();
	
	
	void selectPlayerSuit(int suitID);
	int getSelectedSuit();
    std::string getSelectedSuitString(bool withLevel);
	std::string getSuitName(int suitID);
	void savePlayerSelectedSuit();
	
	SuitData *getSuitData(int suitID);
	
	std::string infoSuitArray();
	std::string infoPlayerSuitArray();
	int getSelectedSuitLevel();
	
	//
	int getCurrentSuitValue(int suitID);
	ItemEffect *getPassiveItemEffect();
	void getCapsuleItemEffectAndValue(ItemEffect::Type &outType, float &outValue);
	
	
	void setDogSuitAndLevel(int suitID, int level);
	
private:
	PlayerSuit *getPlayerSuit(int suitID);
	SuitData *generateSuitDataFromJSON(const rapidjson::Value &masteryJSON);
	
	void initPlaySuit();
	
private:
	Vector<SuitData *> mSuitArray;
	Vector<PlayerSuit *> mPlayerSuitArray;
	int mSelectedSuit;
	int mSelectedSuitLevel;		// New Logic
};
#endif /* SuitManager_hpp */
