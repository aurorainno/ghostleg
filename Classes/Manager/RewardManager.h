//
//  RewardManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 22/11/2016.
//
//

#ifndef RewardManager_hpp
#define RewardManager_hpp

#include <stdio.h>

#include "cocos2d.h"


#include "DailyReward.h"


#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

USING_NS_CC;


class RewardManager
{
public:
	static RewardManager *instance();
	
public:
	enum DailyRewardCheckStatus
	{
		DailyRewardStatusNoNeed,			// Already popup to the play
		DailyRewardStatusNormal,			// Nothing special, popup the reward
		DailyRewardStatusNeedReset,			// Need to reset, missed yesterday
		DailyRewardStatusIncreaseCycle,		// Increase the cycle before popup
	};
	
public:
	RewardManager();
	CC_SYNTHESIZE(int, mCycleRange, CycleRange);	// Number of day per cycle
    
	void setup();
	void loadData();			// Load Data from JSON and load rewardProfile from internal memory
	void loadPlayerData();
    RewardData* generateSingleRewardFromJSON(const rapidjson::Value &rewardJSON);
    void saveData();            // save rewardProfile to internal memory
	void setupMock();
	
	bool checkDailyRewardStatus(DailyRewardCheckStatus status,int givenDay = -1);
    RewardData* collectReward(int day = -1);
    RewardData* getRewardByDay(int day);
    void increaseCycle();
    void resetCollectedReward();
    void resetAll();
    
    int getTotalCollectedDayInTermsOfCycle();
    int getTotalCollectedDay();
	
	std::string toString();
	
	
private:
	void parseDailyRewardListRewardFromJSON(const rapidjson::Value &rewardJSON,
								DailyReward &dailyReward, bool isDefaultReward);
	
private:
	DailyReward mDailyReward;
	DailyReward mDefaultDailyReward;
	
	DailyRewardProfile mDailyRewardProfile;
};

#endif /* RewardManager_hpp */
