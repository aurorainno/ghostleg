//
//  NPCMapGenerator.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#include "NPCMapLogic.h"
#include "NPCOrder.h"
#include "GameWorld.h"
#include "GameMap.h"
#include "Player.h"
#include "Constant.h"
#include "StageManager.h"
#include "ViewHelper.h"
#include "StringHelper.h"
#include "GameManager.h"
#include "RandomHelper.h"

const int kMapSize = 6;
const int kNpcMapHeight = 600;	// 20 * 30 grid
const int kMapGenerateThreshold = 1000;
const NPCMapLogic::TestMode kDefaultMode = NPCMapLogic::NoTest;
//const NPCMapLogic::TestMode kDefaultMode = NPCMapLogic::QuickMode;

NPCMapLogic::NPCMapLogic()
: mNpcOrder(nullptr)
, mQuickMode(false)
, mDropPosition(0, 0)
, mStopPosition(0, 0)
, mTestMode(kDefaultMode)
, mComingMap(-1)
, mCurrentMap(-1)
{
	//
	mMapGenerator = new MapDataGenerator();
}

NPCMapLogic::~NPCMapLogic()
{
	CC_SAFE_RELEASE(mNpcOrder);
	
	delete mMapGenerator;	// free
}

bool NPCMapLogic::init()
{
	return true;
}


void NPCMapLogic::setupWithNPC(int npc, int startOffset)
{
	NPCOrder *npcOrder = StageManager::instance()->getNpcOrder(npc);
	setupWithNPCOrder(npcOrder, startOffset);
}

void NPCMapLogic::setupMapArray(NPCOrder *npcOrder)
{
    std::queue<int> empty{};
    std::swap( mMapQueue, empty );
	
	if(mTestMode == NoGameMap) {
		return;
	}
	
	int mapSize = (mTestMode == QuickMode)  ? 1 : 0;		// 0 mean all maps
	std::vector<int> randomMap = mNpcOrder->getRandomMaps(mapSize);
	for(int mapID : randomMap) {
		mMapQueue.push(mapID);
	}
	
	//log("****** DEBUG: mapCount=%ld", mMapQueue.size());
	// log("****** DEBUG: mapList=%s", VECTOR_TO_STR(randomMap).c_str());

	
//
//	std::vector<int> sourceMap = mNpcOrder->getMapList();
//	
//	int totalMapSize = (int) sourceMap.size();
//	int numShuffle = totalMapSize * 1.5;
//	
//	std::vector<int> randomMapList = aurora::RandomHelper::getRandomizeList(
//										sourceMap, mapSize, numShuffle);
//	
//	for(int mapID : randomMapList) {
//		mMapQueue.push(mapID);
//	}
//	
//
}


void NPCMapLogic::setupWithNPCOrder(NPCOrder *npcOrder, int startOffset)
{
	
	if(startOffset < 0) {
		startOffset = 300;
	}
	
	mPlayerY = (int) GameWorld::instance()->getPlayer()->getPositionY();
	startOffset += mPlayerY;
	
	// log("DEBUG: startOffset at %d", startOffset);
	
	setNpcOrder(npcOrder);
	
	if(mNpcOrder == nullptr) {
		log("NPCMapLogic:ERROR: mNpcOrder is null");
		return;
	}
	
	// Setup the MAP Generator
	mMapGenerator->init(GameWorld::instance()->getMap(),
						GameWorld::instance()->getPlayer(), 0);

	
	// Setup the map array
	setupMapArray(mNpcOrder);
	
	// NPC Order
	mNpcMapID = mNpcOrder->getNpcMap();
	
	// distance to NPC map 
	mTotalDistance = (int) mMapQueue.size() * kMapHeight;

	// Setting
	mNextMapOffset = startOffset;
	mStartOffset = startOffset;
	mReachNpcMap = false;
	mComingMap = -1;
	mCurrentMap = -1;
	mLastMap = -1;
	
	generateNextMap();
//	int mStartOffset;
//	int mCurrentOffset;
//	int mNextMapOffset;
//	bool mReachNpcMap;

	//
}

void NPCMapLogic::updateWorldData(float delta)		// sync world data to this logi
{
	mPlayerY = GameWorld::instance()->getPlayer()->getPositionY();
	
	// mPlayerY
	mTravelDistance = mPlayerY - mStartOffset;
	if(mTravelDistance <= 0) {
		mTravelDistance = 0;
	}

	bool nextMap = needNextMap();
	//log("needNextMap: %d", nextMap);
	if(nextMap) {
		generateNextMap();
	}
	
	// Update the Map element generation
	if(mMapGenerator->getIsGenerationDone() == false) {
		mMapGenerator->generateMapElements(GameWorld::instance());		//
	}
}

bool NPCMapLogic::needNextMap()
{
	int diffToNextMap = (mNextMapOffset - mPlayerY);
	
	return diffToNextMap < kMapGenerateThreshold;
}

bool NPCMapLogic::requireNpcMap()
{
	if(mReachNpcMap) {	// false if the player already reach NPC Map
		return false;		//
	}
	
	return mMapQueue.empty();
}

// true if the nextMap is npcMap
bool NPCMapLogic::generateNextMap()
{
	if(mReachNpcMap) {	// false if the player already reach NPC Map
		return false;		//
	}
	
	if(requireNpcMap()) {
		generateNpcMap(mNpcMapID);
		mReachNpcMap = true;
		
		mLastMap = mCurrentMap;
		mCurrentMap = mComingMap;
		mComingMap = mNpcMapID;
		
		return true;
		
	} else {
		int nextMap = getNextMapID();
		
		if(nextMap > 0) {
			mLastMap = mCurrentMap;
			mCurrentMap = mComingMap;
			mComingMap = nextMap;

			generateGameMap(nextMap);
		}
		
		return false;
	}
}

int NPCMapLogic::getNextMapID()
{
	if(mMapQueue.empty()) {
		return -1;
	}
	
	//
	int mapID = mMapQueue.front();
	mMapQueue.pop();
	
	// IF DEBUG MAP is on, use the giving map
	int debugMap = GameManager::instance()->getDebugMap();
	if(debugMap > 0) {
		mapID = debugMap;
	}
	
	
	//
	return mapID;
}



void NPCMapLogic::updateMapOffset(int mapHeight)
{
	mNextMapOffset += mapHeight;
}


void NPCMapLogic::generateGameMap(int mapID)		// generate single gameMap
{
	mMapGenerator->setRangeStart(mNextMapOffset);
	

	// Generate all map elements once !!  // safe but slow
	// mMapGenerator->generateByMapID(mapID);		// Created the objects need by map
	// mMapGenerator->addModelToWorld(GameWorld::instance());
	
	// Generate map elements using different update
	mMapGenerator->generateGameMapAsync(mapID, false);
	// mMapGenerator->generateMapElements(GameWorld::instance());		// 
	
	
	// update the nextMapOffset
	updateMapOffset(kMapHeight);
}

void NPCMapLogic::generateNpcMap(int mapID)			// generate npc map
{
	mMapGenerator->setRangeStart(mNextMapOffset);
    
    // define the custom npc anime id
    mMapGenerator->setCustomNpcAnime(mNpcOrder->getNpcID());
	
	// Core logic
	mMapGenerator->generateByNpcMapID(mapID);		// Created the objects need by map
	
	// mMapGenerator->addModelToWorld(GameWorld::instance());
	mMapGenerator->generateGameMapAsync(mapID, true);
	
	//mMapGenerator->getN
	
	//mDropPosition
	setDropPosition(mMapGenerator->getDropPosition());
	setStopPosition(mMapGenerator->getStopPosition());
	
	// Add Debug Mark on the Drop Position
	//static Node *createRedSpot(Node *parent, const Vec2 &pos);
//	Node *parent = (Node *) GameWorld::instance()->getModelLayer();
//	Node *node;
//	node = ViewHelper::createDebugSpot(parent, mDropPosition, Color4B::RED);
//	node->setGlobalZOrder(10000);
//	node = ViewHelper::createDebugSpot(parent, mStopPosition, Color4B::BLUE);
//	node->setGlobalZOrder(10000);
	
	//log("DEBUG: DropPostion: %s", POINT_TO_STR(mDropPosition).c_str());
	//log("DEBUG: StopPosition: %s", POINT_TO_STR(mStopPosition).c_str());
	
	
	
	
	//
	updateMapOffset(kNpcMapHeight);
}

void NPCMapLogic::updateMapStartPosition(int mapOffset)	// the start offset of a new NPC ;
{
	mStartOffset = mapOffset;
}

bool NPCMapLogic::isAtNpcMap()						//
{
	return getRemainDistance() <= 0;
}

int NPCMapLogic::getTravelDistance()			// the distance from mapStart to now
{
	return mTravelDistance;
}

int NPCMapLogic::getRemainDistance()			// remain distance to NPC map
{
	return mTotalDistance - mTravelDistance;
}

std::string NPCMapLogic::infoDebugData()
{
	std::string info = "";
	
	info += StringUtils::format("TotalDistance	 : %d\n", mTotalDistance);
	info += StringUtils::format("TravelDistance	 : %d\n", mTravelDistance);
	info += StringUtils::format("PlayerY		 : %d\n", mPlayerY);
	info += StringUtils::format("StartOffset	 : %d\n", mStartOffset);
	info += StringUtils::format("NextMapOffset	 : %d\n", mNextMapOffset);
	info += StringUtils::format("ReachNpcMap	 : %d\n", mReachNpcMap);
	info += StringUtils::format("RemainDistance	 : %d\n", getRemainDistance());
	
	
	return info;
}

//		needNextMap
//		generateNextMap
//		generateGameMap
//		updateMapPosition(offsetOfMap)		-> true
//		isAtFirstMap()
//		isAtLastMap()
//		isAtNpcMap
//		resetData()			// reset the internal data
//		getRemainDistance()


void NPCMapLogic::reset()
{
	
}

NPCOrder *NPCMapLogic::getCurrentNPCOrder()
{
	return mNpcOrder;
}

int NPCMapLogic::getStartOffset()
{
	return mStartOffset;
}

std::string NPCMapLogic::infoMap()
{
	//return StringUtils::format("map: current=%d next=%d", getCurrentMap(), getComingMap());
	return StringUtils::format("[%d]", getCurrentMap());
}
