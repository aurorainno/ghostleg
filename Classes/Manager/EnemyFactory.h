//
//  EnemyFactory.hpp
//  GhostLeg
//
//	A factory to produce enemy objects by different type
//
//  Created by Ken Lee on 24/3/2016.
//
//	
//

#ifndef EnemyFactory_hpp
#define EnemyFactory_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "EnemyBehaviour.h"
#include "CommonType.h"

USING_NS_CC;

class Enemy;
class EnemyData;

class EnemyFactory
{
public:
	static EnemyFactory *instance();
	static Enemy *createByType(int type);		// Old Stuff
	static ObjectTag parseTagStr(const std::string &tagStr);
	static std::string tagToString(const ObjectTag &tag);
public:
	EnemyFactory();
	~EnemyFactory();

	void loadData();
	
	std::string infoEnemyData();
	
	std::string infoDefaultEnemyData();
	
	EnemyData *getEnemyDataByType(int type);
	
	Enemy *createEnemy(int monsterID);
	EnemyBehaviour *createBehaviourByMonsterID(int monsterID);
	EnemyBehaviour *createBehaviourByType(EnemyBehaviour::BehaviourType type);
	void setDefaultEffectStatus(EffectStatus status);
	void resetDefaultEffectStatus();
    
    EffectStatus getCurrentEffectStatus();
	
	void reset();
	void resetObjectID();
	int nextObjectID();
	
	void setupDefaultEnemyData();
	
	Vector<EnemyData *> loadEnemyDataListFromJson(const std::string &jsonData);
	
protected:
	Map<int, EnemyData *> mEnemyDataMap;
	int mObjectID;
	EffectStatus mEffectStatus;
	
	void loadDataFromJson();
	void loadDataFromDat();
	
	EnemyData *mDefaultEnemyData;
	
	//EnemyBehaviour mDefaultBehaviour;
};

#endif /* EnemyFactory_hpp */
