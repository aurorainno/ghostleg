//
//  LevelDataLoader.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#ifndef LevelDataLoader_hpp
#define LevelDataLoader_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <vector>
#include <set>
#include "external/json/document.h"


class Enemy;
class MapLine;
class Item;
class Player;

USING_NS_CC;

#pragma mark - Class and Type using
class LevelLineData : public Ref
{
public:
	int id;
	int type;
	int xLine;		// which vertical line
	int y;
	int probability;		// base 100; value=50, it mean 50% chance occur
	
	void parse(const char *dataLine);
	
	std::string toString();
};

#pragma mark - LevelNpcData 
class LevelNpcData : public Ref
{
public:
	int npcID;
	int x;
	int y;
	int faceDir;
	
	void parse(const char *dataLine);
	
	std::string toString();
};



class LevelEnemyData : public Ref
{
public:
	int type;
	int x;
	int y;
	int resID;
	int speed;
	float scaleX;
	float scaleY;
	bool flipX;
	bool flipY;
	
	void parse(const char *dataLine);
	
	std::string toString();
};

class LevelItemData : public Ref
{
public:
	int type;
	int x;
	int y;

	void parse(const char *dataLine);
	
	std::string toString();
};

#pragma mark - Map Data Main objects

class MapLevelData : public Ref
{
public:
	MapLevelData();
	
	bool load(const char *filename);
	const int getMapID();
	bool isNpcMap();
	 
	const Vector<LevelLineData *>& getLineData();
	const Vector<LevelEnemyData *>& getEnemyData();
	const Vector<LevelItemData *>& getItemData();
	const Vector<LevelNpcData *>& getNpcData();
	
	std::vector<int> getEnemyIDList();
	std::vector<int> getNpcIDList();
	
	void getRandomLineData(Vector<LevelLineData *> &outLineArray);
	
	CC_SYNTHESIZE(Vec2, mDropPosition, DropPosition);
	CC_SYNTHESIZE(Vec2, mStopPosition, StopPosition);
	std::string toString();
	
private:
	Vector<LevelLineData *> mLineArray;
	Vector<LevelEnemyData *> mEnemyArray;
	Vector<LevelItemData *> mItemArray;
	Vector<LevelNpcData *> mNpcArray;
	int mMapID;
	bool mIsNpcMap;

	
private:
	int parseMainData(std::string &line, int currentStage);
	int parseLineData(std::string &line, int currentStage);
	int parseEnemyData(std::string &line, int currentStage);
	int parseItemData(std::string &line, int currentStage);
	int parseNpcData(std::string &line, int currentStage);
	int parseNpcSetting(std::string &line, int currentStage);
};


class MapLevelConfig : public Ref
{
public:
	int level;
	int mapStart;
	int mapEnd;
	std::string toString();
};


class MapSection : public Ref
{
public:
	CREATE_FUNC(MapSection)
	bool init()  { return true; }

	CC_SYNTHESIZE(int, mSectionID, SectionID);
	CC_SYNTHESIZE(int, mMapStart, MapStart);
	CC_SYNTHESIZE(int, mMapEnd, MapEnd);
	
	int getRandomMap();
	
	std::string toString();
};

class MapCycle : public Ref
{
public:
	CREATE_FUNC(MapCycle)
	
	bool init()  { return true; }
	
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(int, mIncreaseSpeed, IncreaseSpeed);
	CC_SYNTHESIZE(int, mInitialSpeed, InitialSpeed);
	CC_SYNTHESIZE(int, mMaxSpeed, MaxSpeed);
	
	MapCycle();
	
	int getTotalDistance();
	
	void setSection(const std::vector<int> &sections);
	
	int getSectionIDByIndex(int index);
	int getRandomMapByDistance(int distance);	// input is relative distance to the cycle start
	
	std::string toString();
	
private:
	int mTotalDistance;			// calculate when setSection
	std::vector<int> mSectionList;
};


class MapLevelDataCache
{
public:
#pragma mark - Static Method
	static MapLevelDataCache *instance();
	
	
#pragma mark - Public Method & Property
	bool loadConfig();
	void loadAllMap();
	
	MapLevelData *getMap(int mapID);
	MapLevelData *getNpcMap(int mapID);
	
	void loadGameMap(const std::vector<int> &mapList);
	void loadNpcMap(const std::vector<int> &npcList);
	
	
	const MapLevelConfig *getMapLevelConfig(int level) const;
	const MapLevelConfig *getNearestMapLevelConfig(int level) const;

	int getMaxMapLevel();
	bool hasLevel(int level);
	bool hasMap(int mapID);
	
	std::vector<int> getAvailableMaps() const;
	
	std::string infoConfig();
	
	// Load the Map
	MapLevelData *loadMap(const std::string &filename);
	
//public:
private:
	// mapID =
	Map<int, MapLevelData *> mMapCache;
	Map<int, MapLevelConfig *> mMapLevelConfig;
	Map<int, MapLevelData *> mNpcMapCache;
	int mMaxLevel;

private:
	MapLevelDataCache();
	bool loadMap(int mapID);
	bool loadNpcMap(int mapID);
	
};

//
//class LevelItemData
//{
//public:
//	int type;
//	int x;
//	int y;
//	
//	void parse(const char *dataLine);
//	
//	std::string toString();
//};






#pragma mark - MapCycleInfo
class MapCycleInfo : public Ref
{
public: // data
	CC_SYNTHESIZE(int, mCycle, Cycle);
	CC_SYNTHESIZE(int, mStartDistance, StartDistance);
	CC_SYNTHESIZE(int, mEndDistance, EndDistance);
	CC_SYNTHESIZE(bool, mHasSpeedUp, HasSpeedUp);
	CC_SYNTHESIZE(int, mSpeedUpValue, SpeedUpValue);
	CC_SYNTHESIZE(int, mSpeedMaxValue, SpeedMaxValue);

	
public:
	bool isInRange(int distance);
	int getMapID(int distance);
	
	std::string toString();
};

#pragma mark - Planet Level Data
class PlanetMapCycle : public Ref
{
public:
	CREATE_FUNC(PlanetMapCycle);
	PlanetMapCycle();
	virtual ~PlanetMapCycle();
	
	virtual bool init() { return true; }
	CC_SYNTHESIZE(int, mPlanetID, PlanetID);
	
	const Vector<MapCycle *> &getCycleList();
	void clearCycleList();
	void addCycle(MapCycle *cycle);
	
	
	std::string info();
	
protected:
	Vector<MapCycle *> mCycleList;
};

#pragma mark - MapLevelManager

class MapLevelManager
{
public:
	MapLevelManager();
	~MapLevelManager();
	static MapLevelManager *instance();
	
	CC_SYNTHESIZE(std::string, mVersion, Version);
	CC_SYNTHESIZE(int, mTutorialMap, TutorialMap);
	
	CC_SYNTHESIZE_READONLY(int, mInitialDistance, InitialDistance);	// the initial distance before any map generation
	CC_SYNTHESIZE(int, mGeneratedDistance, GeneratedDistance);		// Last generated distance
    CC_SYNTHESIZE(int, mOffsetSpeed, OffsetSpeed);
	
	bool loadConfig();
	
	std::string infoCycleData();
	std::string infoSectionData();
	std::string infoMap();
	
	
	void getMapList(std::vector<int> &outMapList);
	
	MapSection *getSectionByID(int sectionID);
	MapCycle *getMapCycle(int cycle);
	
	void setup(int initialDistance);	// different when using tutorial and normal game
	
	bool needNewMap(int distance);
	int getMapForDistance(int distance);
	
	
	MapCycleInfo *getCurrentCycleInfo();
	
	
	void updateMapCycle(int checkDistance);	// the distance to be generate new map
	int getCurrentCycle();
	void setNextMapCycle();
	bool needSpeedUp(float playerDistance);
	void updatePlayerSpeed(Player *player);
	void setInitialSpeed(Player *player);
	
	PlanetMapCycle *getPlanetCycle(int planetID);
	void setMapCycleForPlanet(int planetID);
	
	std::vector<int> getTestMapList();
	
private:
	MapSection *parseMapSection(const rapidjson::Value &object);
	MapCycle *parseMapCycle(const rapidjson::Value &object);
	PlanetMapCycle *parsePlanetMapCycle(int planetID, const rapidjson::Value &object);
	
	void parsePlanetDataMap(const rapidjson::Value &object);
	void updateCycleInfo(int cycle, int startDistance);

	void addPlantMapCycle(PlanetMapCycle *planetCycle);

	
	
private: // local data
	std::set<int> mTestMapList;
	cocos2d::Map<int, MapSection *> mSectionMap;
	cocos2d::Map<int, MapCycle *> mCycleMap;		// key
	cocos2d::Map<int, PlanetMapCycle *> mPlanetCycleMap;		// key: planetID, value PlanetMapCycle
	MapCycleInfo *mCycleInfo;
	
	int mSelectedPlanet;
};




#endif /* LevelDataLoader_hpp */
