//
//  IAPManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 24/5/2016.
//
// Reference:
//		http://docs.sdkbox.com/en/plugins/iap/v3-cpp/

#include "IAPManager.h"
#include "StringHelper.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "CommonMacro.h"
#include "AdManager.h"
#include "Analytics.h"
#include "JSONHelper.h"
#include "StringHelper.h"

#pragma mark - Local Method
namespace {
	
	MoneyType parseMoneyType(const std::string &moneyStr)
	{
		if("diamond" == moneyStr) {
			return MoneyTypeDiamond;
		}
		
		return MoneyTypeIAP;
	}
	void parseMoneyStr(const std::string &moneyStr, MoneyType &type, int &value)
	{
		std::vector<std::string> tokens = aurora::StringHelper::split(moneyStr, ':');
		
		std::string token1 = tokens.size() <= 0 ? "" : tokens[0];
		std::string token2 = tokens.size() <= 1 ? "0" : tokens[1];
		
		type = parseMoneyType(token1);
		value = aurora::StringHelper::parseInt(token2);
	}

	std::string productToStr(sdkbox::Product const &p)
	{
		std::string result = "";
		
		result = "name=" + p.name;
		result += " id=" + p.id;
		
		std::string typeStr = (p.type == sdkbox::IAP_Type::CONSUMABLE ? "Consumble" : "Non-Consume");
		
		result += " type=" + typeStr;
		result += " title=" + p.title;
		result += " description=" + p.description;
		result += " priceValue=" + FLOAT_TO_STR(p.priceValue);
		result += " price=" + p.price;
		result += " currencyCode=" + p.currencyCode;
		result += " receipt=[" + p.receipt + "]";
		result += " transactionID=[" + p.transactionID + "]";
		
		return result;
	}
	
	float getIncomeValue(const rapidjson::Value &productJSON) {
#if IS_ANDROID
		std::string name = "income.android";
#else
		std::string name = "income.iphone";
#endif
		
		return (float) productJSON[name.c_str()].GetDouble();
	}
}

IAPManager::IAPManager()
{
	mIsReady = false;
	
#if(ENTERPRISE == 1)
	mIsSupported = false;
#else 
	sdkbox::IAP::setListener(this);
	mIsSupported = true;
#endif
}

bool IAPManager::isAvailable()
{
	return mIsReady && mIsSupported;
}

bool IAPManager::isReady()
{
	return mIsReady;
}


void IAPManager::setup()
{
	// TODO: Load the local product list from JSON
	loadProductList();	
}

void IAPManager::updateProductInfo()
{
#if (ENTERPRISE != 1)
	sdkbox::IAP::refresh();
#else 
	if(mUpdateProductCallback) {
		IAPStatusCode status = IAP_ERROR_NOT_SUPPORT;
		std::string msg = getStatusCodeString(status);
		mUpdateProductCallback(status, msg);
	}
	// callback immediately
#endif
}


IAPStatusCode IAPManager::executionAction(const std::string &name)
{
	// split the token
	std::vector<std::string> tokens = aurora::StringHelper::split(name, ':');
	
	std::string action = tokens[0];
	std::string arg = tokens.size() <= 1 ? "" : tokens[1];
	
	if("addStar" == action) {
		return handleAddStar(arg);
	} else if("addCandy" == action) {
		return handleAddCandy(arg);
    } else if("addDiamond" == action) {
        return handleAddDiamond(arg);
    } else if("removeAd" == action) {
		return handleRemoveAd();
	} else {
		return IAP_UNKNOWN_ACTION;
	}
	
}

Vector<GameProduct*> IAPManager::getProductArray()
{
    return mProductArray;
}



GameProduct* IAPManager::generateSingleProductFromJSON(const rapidjson::Value &productJSON){
    
    GameProduct *data = GameProduct::create();
    
    data->setName(productJSON["key"].GetString());

	data->setDisplayName(productJSON["displayName"].GetString());
    data->setDisplayTag(productJSON["displayTag"].GetString());
	//data->setCostType(Mon)
	data->setAction(productJSON["action"].GetString());
	data->setIncome(getIncomeValue(productJSON));
    

	

	bool isEvent = aurora::JSONHelper::getBool(productJSON, "event", false);
	data->setIsEventItem(isEvent);

	
	// Parse the money
	std::string moneyStr = aurora::JSONHelper::getString(productJSON, "cost");
	MoneyType costType;
	int costValue;
	parseMoneyStr(moneyStr, costType, costValue);
	
	data->setCostType(costType);
	data->setCostValue(costValue);
	
	data->setGUIKey(aurora::JSONHelper::getString(productJSON, "guiKey"));
	
    return data;
}


void IAPManager::loadProductList()
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/product.json");
    
    //log("product json content:\n%s", content.c_str());
    rapidjson::Document productJSON;
    productJSON.Parse(content.c_str());
    if (productJSON.HasParseError())
    {
        CCLOG("GetParseError %d\n", productJSON.GetParseError());
        return;
    }
    
    mProductArray.clear();
    
    for(int i=0; i<productJSON.Size(); i++)
    {
        const rapidjson::Value& currentProduct = productJSON[i];
        GameProduct *data = generateSingleProductFromJSON(currentProduct);
        
#if(PAID_APP == 1)
        if(data->getName() == "noad")
        {
            GameManager::instance()->getUserData()->setNoAd(true);
			continue;
        }
#endif

        mProductArray.pushBack(data);
        
    }
}

                                                                           
void IAPManager::setProductRequestSuccessCallback(const ProductRequestSuccessCallback &callback){
    mProductRequestSuccessCallback = callback;
}

GameProduct *IAPManager::getGameProductByName(const std::string &name)
{
	for(GameProduct *product : mProductArray) {
		if(product->getName() == name) {
			return product;
		}
	}
	
	return nullptr;
}


void IAPManager::restorePurchasedItems()
{
    sdkbox::IAP::restore();
}


IAPStatusCode IAPManager::buyProduct(GameProduct *product)
{
	if(product == nullptr) {
		return IAP_ERROR_PRICE_NOT_READY;
	}
	
	IAPStatusCode status;
	
	if(product->getCostType() == MoneyTypeIAP) {
		status = purchase(product->getName());
		
		if(status == IAP_SUCCESS) {
			return IAP_WAIT_CALLBACK;
		} else {
			return status;
		}
	} else {
		return purchaseStarByDiamond(product);
	}
	
}


// Result:  IAP_SUCCESS or IAP_NOT_ENOUGH_MONEY
IAPStatusCode IAPManager::purchaseStarByDiamond(GameProduct *product)
{
    if(product->getCostType()!=MoneyTypeDiamond ){
        return IAP_ERROR_PURCHASE_FAIL;
    }
    
    int price = product->getCostValue();
    int ownedDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    if(ownedDiamond<price){
        return IAP_NOT_ENOUGH_MONEY;
    }else{
        GameManager::instance()->getUserData()->addDiamond(-price);
        return executionAction(product->getAction());
    }
    
//	return IAP_ERROR_NOT_SUPPORT;
}

// callback -> onSuccess / onFailure / onCanceled
IAPStatusCode IAPManager::purchase(const std::string & name)
{
#if (ENTERPRISE != 1)
	sdkbox::IAP::purchase(name);
	return IAP_SUCCESS;
#else 
	callbackPurchase(getGameProductByName(name), IAP_ERROR_NOT_SUPPORT, "");
	return IAP_ERROR_NOT_SUPPORT;
#endif
}

#pragma mark - IAP Listener
void IAPManager::onInitialized(bool ok)
{
	log("IAP: initialization. status=%d", ok);
}

void IAPManager::onSuccess(sdkbox::Product const& p)
{
	log("IAP: onSuccess: product=%s", productToStr(p).c_str());
	GameProduct *product = getGameProductByName(p.name);
	
	IAPStatusCode status = executionAction(product->getAction());
	
	if(status == IAP_SUCCESS) {
		updateOwnStatus(product, true);		// especially for removeAd
	}
	
	std::string msg = status == IAP_SUCCESS ? "success" : getStatusCodeString(status);
	
	callbackPurchase(product, status, msg);
	
	// Log Analytics
	logAnalytics(product);
}

void IAPManager::logAnalytics(GameProduct *product)
{
	if(product == nullptr) {
		log("logAnalytics: product is null");
		return;
	}
	
	// Logging the product and the related income that the product
	Analytics::instance()->logPayment(product->getName(), product->getIncome());
    
	
	
	// logging the star earn
	MoneyType moneyType;
	int amount;
	product->getMoneyDetail(moneyType, amount);	
}

void IAPManager::onFailure(sdkbox::Product const& p, const std::string &msg)
{
	log("IAP: onFailure: msg=%s product=%s ", msg.c_str(), productToStr(p).c_str());
	
	GameProduct *product = getGameProductByName(p.name);
	callbackPurchase(product, IAP_ERROR_PURCHASE_FAIL, msg);
}

void IAPManager::onCanceled(sdkbox::Product const& p)
{
	log("IAP: onCanceled: product=%s ", productToStr(p).c_str());

	GameProduct *product = getGameProductByName(p.name);
	callbackPurchase(product, IAP_PURCHASE_CANCEL, "cancelled");
}

void IAPManager::onRestored(sdkbox::Product const& p)
{
	log("IAP: onRestored: product=%s ", productToStr(p).c_str());
	
    GameProduct *product = getGameProductByName(p.name);
    if(product)
    {
        product->setIAPPrice(p.price);
        product->setIsConsumable(p.type == sdkbox::IAP_Type::CONSUMABLE);
        product->setProductID(p.id);
        product->setIsOwned(isProductOwned(product));
        product->setIsReady(true);
        if(product->getName() == "noad"){
			GameManager::instance()->getUserData()->setNoAd(true);
		}
        callbackRestore(IAP_SUCCESS, "success");
        
    }
    else
    {
        callbackRestore(IAP_NO_ITEM_TO_RESTORE, "No purchased record.");
    }
	
}

void IAPManager::onProductRequestSuccess(std::vector<sdkbox::Product> const &products)
{
	log("Product Request Success");
	for(int i=0; i<products.size(); i++) {
		sdkbox::Product p = products[i];
		
		log("product %d: %s", i, productToStr(p).c_str());
		
		GameProduct *product = getGameProductByName(p.name);
        
        if(product){
            product->setIAPPrice(p.price);
			product->setIsConsumable(p.type == sdkbox::IAP_Type::CONSUMABLE);
			product->setProductID(p.id);
			product->setIsOwned(isProductOwned(product));
			product->setIsReady(true);
        }
	}
	
	mIsReady = true;
	

	// Callback to listener
	if(mUpdateProductCallback) {
		mUpdateProductCallback(IAP_SUCCESS, "success");
	}

}

void IAPManager::onProductRequestFailure(const std::string &msg)
{
	log("Product Request Failure: msg=%s", msg.c_str());
	
    std::string err_msg = "Sorry, shop\nis not\navailable now.";
    
	if(mUpdateProductCallback) {
		mUpdateProductCallback(IAP_LOAD_PRICE_FAIL, err_msg);
	}
}

void IAPManager::onRestoreComplete(bool ok, const std::string &msg)
{
	log("Product Request Failure: ok=%d msg=%s", ok, msg.c_str());
	if(ok == false)
    {
        callbackRestore(IAP_LOAD_PRICE_FAIL, "failed.");
    }
    else
    {
        callbackRestore(IAP_NO_ITEM_TO_RESTORE, "No purchased record.");
    }
	
}


#pragma mark - IAP Action
IAPStatusCode IAPManager::handleAddCandy(const std::string arg)
{
	int value = aurora::StringHelper::parseInt(arg);
	
	GameManager::instance()->getUserData()->addCandy(value);
	
	return IAP_SUCCESS;
}

IAPStatusCode IAPManager::handleAddDiamond(const std::string arg)
{
    int value = aurora::StringHelper::parseInt(arg);
    
    GameManager::instance()->getUserData()->addDiamond(value);
    
    return IAP_SUCCESS;
}

IAPStatusCode IAPManager::handleAddStar(const std::string arg)
{
	int value = aurora::StringHelper::parseInt(arg);
	
	GameManager::instance()->getUserData()->addCoin(value);
	
	return IAP_SUCCESS;
}

IAPStatusCode IAPManager::handleRemoveAd()
{
	AdManager::instance()->setNoAd(true);
	
	return IAP_SUCCESS;
}


#pragma mark - Status Code
std::string IAPManager::getStatusCodeString(IAPStatusCode status)
{
	switch (status) {
		case IAP_SUCCESS				: return "success";
		case IAP_UNKNOWN_ACTION			: return "Fail to proceed the action.";
		case IAP_ERROR_NOT_SUPPORT		: return "IAP not supported in this edition.";
		case IAP_ERROR_PURCHASE_FAIL	: return "Fail to purchase";
		default:
			return StringUtils::format("statusCode %d", status);
	}
}

// Callback
#pragma mark - Callback
void IAPManager::setUpdateProductCallback(const UpdateProductCallback &callback)
{
	mUpdateProductCallback = callback;
}

void IAPManager::setPurchaseCallback(const PurchaseCallback &callback)
{
	mPurchaseCallback = callback;
}

void IAPManager::setRestoreItemCallback(const RestoreItemCallback &callback)
{
    mRestoreItemCallback = callback;
}


void IAPManager::callbackPurchase(GameProduct *product, IAPStatusCode status, const std::string &msg)
{
	if(! mPurchaseCallback) {
		return;
	}
	
	std::string finalMsg;
	if(msg == "") {
		finalMsg = getStatusCodeString(status);
	} else {
		finalMsg = msg;
	}
	
	mPurchaseCallback(product, status, finalMsg);
}

void IAPManager::callbackRestore(IAPStatusCode status, const std::string &msg)
{
    if(! mRestoreItemCallback) {
        return;
    }

    mRestoreItemCallback(status, msg);
}

#pragma mark - Info
std::string IAPManager::infoGameProductList()
{
	Vector<GameProduct*> products = getProductArray();
	
	std::string result = "Product Count: " + UNSIGNED_TO_STR(products.size()) + "\n";
	
	for(int i=0; i<products.size(); i++) {
		GameProduct *product = products.at(i);
		
		result += product->toString() + "\n";
	}
	
	
	return result;
}




#pragma mark - Is Owned Check
bool IAPManager::isProductOwned(GameProduct *product)
{
	if(product == nullptr) {
		return false;
	}
	
	std::string name = product->getName();
	
	if("noad" == name) {
		return GameManager::instance()->getUserData()->isNoAd() == true;
	}
	
	return false;
}

void IAPManager::updateOwnStatus(GameProduct *product, bool ownFlag)
{
	if(product == nullptr) {
		return;
	}
	if(product->getIsConsumable()) {
		return;	// we never own consumable
	}
	
	product->setIsOwned(ownFlag);
}

void IAPManager::resetNonConsumable(const std::string &name)
{
	GameProduct *product = getGameProductByName(name);
	if(product == nullptr) {
		return;
	}
	
	
	updateOwnStatus(product, false);
	
	if("noad" == name) {
		GameManager::instance()->getUserData()->setNoAd(false);
	}
}


bool IAPManager::isProductAvailable(GameProduct *product)
{
	if(product == nullptr) {
		return false;
	}
	
	bool eventOn = GameManager::instance()->isEventOn();
	if(product->getIsEventItem() && eventOn == false) {
		return false;
	}
	
	
	return true;
}
