//
//  GameCenterManager.cpp
//  GhostLeg
//
//  Created by Calvin on 27/5/2016.
//
//

#include <stdio.h>
#include "GameCenterManager.h"
#include "cocos2d.h"
#include "CommonMacro.h"

USING_NS_CC;

//#include "TDDHelper.h"		// ken: should be no need

const std::string kLBNameBestDistance = "Best Distance";

GameCenterManager::GameCenterManager()
: mHasSetup(false)
{
	
}

void GameCenterManager::setup()
{
	if(mHasSetup) {
		return;
	}

	
	
#if (ENTERPRISE != 1)
//#if (IS_IOS)
    sdkbox::PluginSdkboxPlay::init();
	//if(sdkbox::PluginSdkboxPlay::isConnected()) {
		if(sdkbox::PluginSdkboxPlay::isSignedIn() == false) {
			log("GameCenterManager.setup: try to signed in");
			sdkbox::PluginSdkboxPlay::signin();
		} else {
			log("GameCenterManager.setup: already signed in");
		}
	//}
	
	//sdkbox::PluginSdkboxPlay::set
//#endif
	mHasSetup = true;

#endif
}

bool GameCenterManager::hasSignedIn(){
    return isSignedIn();
}

void GameCenterManager::sendBestDistance(int score)
{
#ifndef ENTERPRISE
	if(hasSignedIn()) {
		log("GameCenterManager.sendBestDistance: signed in");
		submitScore(kLBNameBestDistance, score);
	}  else {
		log("GameCenterManager.sendBestDistance: not signed in yet");
	}
#endif
}

void GameCenterManager::showBestDistanceLeaderBoard()
{
#ifndef ENTERPRISE
	if(hasSignedIn()) {
		showLeaderboard(kLBNameBestDistance);
		log("GameCenterManager.showBestDistanceLeaderBoard: signed in");
	} else {
		log("GameCenterManager.showBestDistanceLeaderBoard: not signed in");
	}
#endif
}

void GameCenterManager::onConnectionStatusChanged(int status){
    log("GameCenterManager: connect status=%d",status);
}

void GameCenterManager::onScoreSubmitted (const std::string &leaderboardName ,
                               long score ,
                               bool maxScoreAllTime,
                               bool maxScoreWeek,
                               bool maxScoreToday)
{
	log("GameCenterManager: score submitted!");
}

void GameCenterManager::onIncrementalAchievementUnlocked(const std::string &achievementName)
{
	// TODO: Later
}

void GameCenterManager::onIncrementalAchievementStep(const std::string &achievementName, double step)
{
	// TODO: Later
}

void GameCenterManager::onAchievementUnlocked(const std::string &achievementName, bool newlyUnlocked)
{
	// TODO: Later
}



