//
//  ItemManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 19/1/2017.
//
//

#ifndef ItemManager_hpp
#define ItemManager_hpp

#include <stdio.h>
#include "cocos2d.h"

#include "PlayerBooster.h"
#include "CommonType.h"
#include "BoosterData.h"
#include "ItemEffect.h"

USING_NS_CC;


class ItemManager
{
public: // Type and Enum
	static ItemManager *instance();

private:
	ItemManager();
	~ItemManager();

#pragma mark - General
public:
	void setup();
	
	
#pragma mark - Player Booster
public:
	bool checkBoosterMoney(BoosterItemType type, int quantity);
	bool buyBooster(BoosterItemType type, int quantity);
	
	int getBoosterCount(BoosterItemType type);
	void addBoosterCount(BoosterItemType type, int count=1);
	void deduceBoosterCount(BoosterItemType type, int count=1);
	
	void savePlayerData();
	void loadPlayerData();

#pragma mark - General
public:
	std::string toString();


	
	
	
#pragma mark - BoosterData
public:
	std::string getBoosterName(BoosterItemType type);
	Price getBoosterPrice(BoosterItemType type);
	std::string getBoosterInfo(BoosterItemType type);
	float getBoosterCooldown(BoosterItemType type);
    bool getBoosterIsInstantActive(BoosterItemType type);
	
	void loadBoosterData();
	BoosterData *getBoosterData(BoosterItemType type);
	std::string infoBoosterDataSet();
	void setupMockData();
	
	void preloadCsb();
	
private:
	void addBoosterData(BoosterData *data);
	
private:
	PlayerBooster mPlayerBooster;
	
	Map<int, BoosterData *> mBoosterDataSet;		// key: int boosterItemType


#pragma mark - Booster & Power Setting
public:
	float getMissileSpeed();
	SpeedData getMissileSpeedData();
	SpeedData getCycloneSpeedData();
	
	void defineItemEffectProperty(ItemEffect *effect);
	float getFinalBoosterDuration(float originValue, int attribute);
	
	void setupBoosterGameSetting();
	std::string infoBoosterGameSetting();

private:
	void modifyPowerUpProperty(ItemEffect::Type type, ItemEffect *effect);
	void modifyBoosterProperty(ItemEffect::Type type, ItemEffect *effect);
	
private:
	float mMissileSpeedReduction;		// percent of slow
	float mCycloneSpeedReduction;		// percent of slow
};

#endif /* ItemManager_hpp */
