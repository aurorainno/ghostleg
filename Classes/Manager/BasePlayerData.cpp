//
//  BasePlayerData.cpp
//  BounceDefense
//
//  Created by Ken Lee on 30/8/2016.
//
//

#include "BasePlayerData.h"

BasePlayerData::BasePlayerData()
: mDataKey("")
, mIsReady(false)
{
	
}

BasePlayerData::BasePlayerData(const std::string &key)
: mDataKey(key)
, mIsReady(false)
{
	
}

bool BasePlayerData::save()
{
	std::string content = generateDataContent();		// this is JSON format data
	
	//
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("BasePlayerData.save: userDefault not available. dataKey=%s", mDataKey.c_str());
		return false;
	}
	
	userData->setStringForKey(mDataKey.c_str(), content);
    userData->flush();
	
	return true;
}

bool BasePlayerData::load()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("BasePlayerData.load: userDefault not available. dataKey=%s", mDataKey.c_str());
		return false;
	}
	
	std::string content = userData->getStringForKey(mDataKey.c_str(), "");
	
	if(content == "") {	// Player Data not yet created
		setDefault();
		save();
	} else {
		parseDataContent(content);
	}
	
	mIsReady = true;
	
	return true;
}

std::string BasePlayerData::toString()
{
	std::string result = "data=" + mDataKey;
	
	result += StringUtils::format(" ready=%d", mIsReady);
	
	return result;
}

bool BasePlayerData::isReady()
{
	return mIsReady;
}
