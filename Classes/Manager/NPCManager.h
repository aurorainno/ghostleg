//
//  NPCManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/3/2017.
//
//

#ifndef NPCManager_hpp
#define NPCManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <map>
#include <vector>
#include "CommonMacro.h"

USING_NS_CC;

class NPCRating;
class NPCOrder;

class NPCManager
{
public: // Type and Enum
	static NPCManager *instance();
	
public:
	CC_SYNTHESIZE(int, mCrashCount, CrashCount);
	void setup();				//	called by GameManager:setup
	
private:
	NPCManager();
	~NPCManager();
	


#pragma mark - Npc Rating
public:
	
	
	NPCRating *addNpcRating(NPCOrder *order, float timeUsed);
	void resetNpcRating();
	std::string infoNpcRating();
	Vector<NPCRating *> getRatingList();
	
	int getTotalCollected();		// reward coin + collect on the way
	int getTotalTip();			//
	int getClientCount();
	int getClientScore();
	int getNoCrashScore();
	int getTotalScore();
	
	void rewardByNPCRating(NPCRating *rating);
	
protected:
	void calculateReward(NPCRating *rating, NPCOrder *npcOrder);
	int calculateTip(NPCRating *rating, NPCOrder *npcOrder);
	void calculateScore(NPCRating *rating, NPCOrder *npcOrder);
	int calculateNpcRating(NPCOrder *order, float timeUsed);
	
private:
	Vector<NPCRating *> mRatingList;
	
#pragma mark - Npc Rating Message
public:
	void loadRatingMessage();				// load from Resources/json/rating_message.json"
											//   called by "setup"
	std::string getRatingComment(int rating);
	
	std::string infoRatingMessageHash();	// for debug
	

private:
	std::map<int, std::vector<std::string>> mRatingMessageHash;
};

#endif /* NPCManager_hpp */
