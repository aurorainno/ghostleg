//
//  JSONPlayerData.cpp
//  BounceDefense
//
//  Created by Ken Lee on 30/8/2016.
//
//

#include "JSONData.h"

using namespace rapidjson;


JSONData::JSONData()
{
	
}

bool JSONData::isJSONObject()
{
	return true;
}

std::string JSONData::toJSONContent()
{
	rapidjson::Document doc;
	
	//
	if(isJSONObject()) {
		doc.SetObject();
	} else {
		
	}
	
	// Fill the Document detail
	//defineJSON(doc);
	
	rapidjson::Document::AllocatorType& allocator = doc.GetAllocator();
	defineJSON(allocator, doc);
	
	
	// Output
	StringBuffer buffer;
	Writer<StringBuffer, Document::EncodingType, ASCII<> > writer(buffer);
	doc.Accept(writer);
	
	
	return std::string(buffer.GetString(), buffer.GetSize());
}

void JSONData::addDouble(rapidjson::Document::AllocatorType &allocator,
					  const std::string &name, double value, rapidjson::Value &outValue)
{
	rapidjson::Value v;
	rapidjson::Value n(name.c_str(), allocator);
	
	v.SetDouble(value);
	outValue.AddMember(n, v, allocator);
}


void JSONData::addInt(rapidjson::Document::AllocatorType &allocator,
				const std::string &name, int value, rapidjson::Value &outValue)
{
	rapidjson::Value v;
	rapidjson::Value n(name.c_str(), allocator);
	//rapidjson::GenericValue::StringRefType n = name;
	
	v.SetInt(value);
	outValue.AddMember(n, v, allocator);
}


void JSONData::addString(rapidjson::Document::AllocatorType &allocator,
					  const std::string &name, const std::string &str, rapidjson::Value &outValue)
{
	rapidjson::Value v(str.c_str(), allocator);
	rapidjson::Value n(name.c_str(), allocator);

	outValue.AddMember(n, v, allocator);
}


void JSONData::addObject(rapidjson::Document::AllocatorType &allocator,
						 const std::string &name, JSONData *object,
						 rapidjson::Value &outValue)
{
	if(object == nullptr) {
		return;
	}
	
	// Value
	rapidjson::Value v(rapidjson::kObjectType);
	
	object->defineJSON(allocator, v);

	
	// Name
	rapidjson::Value n(name.c_str(), allocator);
	
	outValue.AddMember(n, v, allocator);
}


void JSONData::addJsonValue(rapidjson::Document::AllocatorType &allocator,
					const std::string &name,
					rapidjson::Value &value,
					rapidjson::Value &outValue)
{
	// Name
	rapidjson::Value n(name.c_str(), allocator);
	
	rapidjson::Value v(value, allocator);
	
	outValue.AddMember(n, v, allocator);

}

void JSONData::addIntDataArray(rapidjson::Document::AllocatorType &allocator,
                               const std::string &name,
                               const std::vector<int> &intVector,
                               rapidjson::Value &outValue)
{
    rapidjson::Value arrayJson(rapidjson::kArrayType);
    for(int i=0; i<intVector.size(); i++) {
        int value = intVector.at(i);
        
        rapidjson::Value v;
        v.SetInt(value);
        
        arrayJson.PushBack(v, allocator);
    }
    
    // Save to parent
    addJsonValue(allocator, name, arrayJson, outValue);
}


void JSONData::addJSONDataArray(rapidjson::Document::AllocatorType &allocator,
							const std::string &name,
							const std::vector<JSONData *> &objectVector,
							rapidjson::Value &outValue)
{
	// Build up the array JSON
	rapidjson::Value arrayJson(rapidjson::kArrayType);
	for(int i=0; i<objectVector.size(); i++) {
		JSONData *item = objectVector.at(i);
		
		rapidjson::Value itemJson(rapidjson::kObjectType);
		item->defineJSON(allocator, itemJson);
		arrayJson.PushBack(itemJson, allocator);
	}
	
	// Save to parent
	addJsonValue(allocator, name, arrayJson, outValue);
}

bool JSONData::parseJSONContent(const std::string &jsonContent)
{
	rapidjson::Document jsonDoc;
	jsonDoc.Parse(jsonContent.c_str());
	
	if (jsonDoc.HasParseError()){
		log("JSONData.parseJSONContent: error-%d\n%s", jsonDoc.GetParseError(), jsonContent.c_str());
		return false;
	}
	
	return parseJSON(jsonDoc);
}


void JSONData::addDataMap(rapidjson::Document::AllocatorType &allocator,
				const std::string &name,
				const std::map<int, int> &valueMap,
				rapidjson::Value &outValue)
{
	rapidjson::Value mapJson(rapidjson::kObjectType);
	
	for (auto iter = valueMap.begin(); iter != valueMap.cend(); ++iter)
	{
		int key = iter->first;
		int value = iter->second;
		
		std::string keyStr = StringUtils::format("%d", key);
		rapidjson::Value mapK(keyStr.c_str(), allocator);
		rapidjson::Value mapV(rapidjson::kNumberType);
		mapV.SetInt(value);
		
		mapJson.AddMember(mapK, mapV, allocator);
	}
	
	rapidjson::Value n(name.c_str(), allocator);
	outValue.AddMember(n, mapJson, allocator);
}


void JSONData::addDataMap(rapidjson::Document::AllocatorType &allocator,
						  const std::string &name,
						  const std::map<std::string, int> &valueMap,
						  rapidjson::Value &outValue)
{
	rapidjson::Value mapJson(rapidjson::kObjectType);
	
	for (auto iter = valueMap.begin(); iter != valueMap.cend(); ++iter)
	{
		std::string key = iter->first;
		int value = iter->second;
		
		rapidjson::Value mapK(key.c_str(), allocator);
		rapidjson::Value mapV(rapidjson::kNumberType);
		mapV.SetInt(value);
		
		mapJson.AddMember(mapK, mapV, allocator);
	}
	
	rapidjson::Value n(name.c_str(), allocator);
	outValue.AddMember(n, mapJson, allocator);
}
//template <typename T>
//void JSONData::convertVector(Vector<T> &ccVector, std::vector<JSONData *> &stdVector)
