//
//  LuckyDrawManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#include "LuckyDrawManager.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "DogManager.h"
#include "ItemManager.h"
#include "PlayerManager.h"
#include "Analytics.h"

// JSON
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"


const int kRollBase = 10000;
const int kDefaultPrice = 5;	// 1 diamond

static LuckyDrawManager *sInstance = nullptr;

LuckyDrawManager *LuckyDrawManager::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new LuckyDrawManager();
	
	return sInstance;
}

LuckyDrawManager::LuckyDrawManager()
: mRewardList()
, mDefaultReward(nullptr)
, mPrice(MoneyTypeDiamond, kDefaultPrice)
{
	
}

LuckyDrawManager::~LuckyDrawManager()
{
	
}

#pragma mark - General
void LuckyDrawManager::setup()
{
	loadRewardData();
	//setupMockData();
	
	setupChanceRange();
	setupDefaultReward();
}

void LuckyDrawManager::setupDefaultReward()
{
	LuckyDrawReward *reward;
 
	reward = LuckyDrawReward::create();
	reward->setItemName("Star");
	reward->setItemID(0);
	reward->setProbability(0);
	reward->setQuantity(2000);
	reward->setRarityType(LuckyDrawReward::RarityNone);
	reward->setRewardType(LuckyDrawReward::RewardStar);
	
	setDefaultReward(reward);
}

Price LuckyDrawManager::getDrawPrice()
{
	return mPrice;
}

LuckyDrawReward *LuckyDrawManager::roll()
{
	int rollValue = RandomHelper::random_int(0, kRollBase);
	
	for(LuckyDrawReward *reward : mRewardList) {
		int end = reward->getRangeEnd();
		
		if(rollValue < end) {
			return reward;
		}
	}
	
	return getDefaultReward();
}

LuckyDrawReward *LuckyDrawManager::generateReward()
{
    int drawCount = 1;
    LuckyDrawReward *reward = nullptr;
    while(drawCount){
        reward = roll();
        drawCount--;
//        if(reward->getRewardType() == LuckyDrawReward::RewardFragment){
//            bool isUnlockable = DogManager::instance()->isUnlockable(reward->getItemID());
//            bool isOwned = !DogManager::instance()->isLocked(reward->getItemID());
//        
//            if(isOwned || !isUnlockable){
//                drawCount++;
//            }
//        }
    }
    return reward;
}

void LuckyDrawManager::setupMockData()
{
	LuckyDrawReward *reward;
 
	mRewardList.clear();
	reward = LuckyDrawReward::create();
	reward->setItemName("Missile");
    reward->setItemID(3);
	reward->setProbability(500);
	reward->setQuantity(5);
	reward->setRarityType(LuckyDrawReward::RarityNone);
	reward->setRewardType(LuckyDrawReward::RewardBooster);
	mRewardList.pushBack(reward);

	reward = LuckyDrawReward::create();
	reward->setItemName("Time stop");
	reward->setItemID(7);
	reward->setProbability(500);
	reward->setQuantity(3);
	reward->setRarityType(LuckyDrawReward::RarityNone);
	reward->setRewardType(LuckyDrawReward::RewardBooster);
	mRewardList.pushBack(reward);

	reward = LuckyDrawReward::create();
	reward->setItemName("Golden Retriever");
	reward->setItemID(21);
	reward->setProbability(1000);
	reward->setQuantity(1);
	reward->setRarityType(LuckyDrawReward::RarityRare);
    reward->setRewardType(LuckyDrawReward::RewardFragment);
	mRewardList.pushBack(reward);

	reward = LuckyDrawReward::create();
	reward->setItemName("Star");
	reward->setItemID(0);
	reward->setProbability(2000);
	reward->setQuantity(1000);
	reward->setRarityType(LuckyDrawReward::RarityNone);
	reward->setRewardType(LuckyDrawReward::RewardStar);
	mRewardList.pushBack(reward);
}

void LuckyDrawManager::setupChanceRange()
{
	int range = 0;
	for(LuckyDrawReward *reward : mRewardList) {
		reward->setRangeStart(range);
		reward->setRangeEnd(range + reward->getProbability());
		range += reward->getProbability();
	}
}

std::string LuckyDrawManager::infoRewardList()
{
	std::string result = "";
	
	for(LuckyDrawReward *reward : mRewardList) {
		result += reward->toString() + "\n";
	}
	
	return result;
}

void LuckyDrawManager::loadRewardData()
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/gachaReward.json");
    
    // log("gacha json content:\n%s", content.c_str());
    rapidjson::Document gachaArray;
    gachaArray.Parse(content.c_str());
    if (gachaArray.HasParseError())
    {
        CCLOG("LuckyDrawManager: GetParseError %d\n", gachaArray.GetParseError());
        return;
    }
    
    mRewardList.clear();
    
    for(rapidjson::SizeType i=0;i<gachaArray.Size();i++){
        const rapidjson::Value &currentReward = gachaArray[i];
        LuckyDrawReward *data = LuckyDrawReward::create();
        data->parseJSON(currentReward);
        mRewardList.pushBack(data);
    }

}


#pragma mark - Reward Collection
bool LuckyDrawManager::hasEnoughMoney()
{
	int playerPrice = GameManager::instance()->getMoneyValue(MoneyType::MoneyTypeDiamond);
	
	return (playerPrice >= kDefaultPrice);
}


void LuckyDrawManager::collectReward(LuckyDrawReward *reward, bool isFree)
{
	// Deduce player money
	if(isFree == false) {
		if(hasEnoughMoney() == false) {
			log("LuckyDrawManager: no money to collect reward");
			return;
		}
	}
	
	// Spent the diamond
	if(isFree == false) {
		// Deduce diamond and Log the consumption
		GameManager::instance()->getUserData()->addDiamond(-kDefaultPrice);
		Analytics::instance()->logConsume(Analytics::act_lucky_draw, "", kDefaultPrice, MoneyTypeDiamond);
	}
	
	
	
	// do the collection logic
    int rewardID = reward->getItemID();
    int count = isFree ? reward->getFreeQuantity() : reward->getQuantity();
    LuckyDrawReward::RewardType type = reward->getRewardType();
    
    switch (type) {
        case LuckyDrawReward::RewardFragment:
            collectFragmentReward(rewardID,count);
            break;
            
        case LuckyDrawReward::RewardStar:
            collectStarReward(count);
            break;
            
        case LuckyDrawReward::RewardBooster:
            collectBoosterReward(rewardID,count);
            break;
            
        default:
            break;
    }
	
	logReward(reward, false);
}

void LuckyDrawManager::collectRewardForFree(LuckyDrawReward *reward, bool isCollectFromAd)
{
    int rewardID = reward->getItemID();
    int count = isCollectFromAd ? reward->getFreeQuantity() : reward->getQuantity();
    LuckyDrawReward::RewardType type = reward->getRewardType();
    
    switch (type) {
        case LuckyDrawReward::RewardFragment:
            collectFragmentReward(rewardID,count);
            break;
            
        case LuckyDrawReward::RewardStar:
            collectStarReward(count);
            break;
            
        case LuckyDrawReward::RewardBooster:
            collectBoosterReward(rewardID,count);
            break;
            
        default:
            break;
    }
	
	logReward(reward, true);
}

void LuckyDrawManager::collectFragmentReward(int fragmentID, int count)
{
    PlayerManager::instance()->addPlayerFragment(fragmentID, count);
}

void LuckyDrawManager::collectStarReward(int starAmount)
{
    GameManager::instance()->getUserData()->addCoin(starAmount);
}
void LuckyDrawManager::collectBoosterReward(int boosterID, int count)
{
    ItemManager::instance()->addBoosterCount((BoosterItemType)boosterID,count);
}


void LuckyDrawManager::logReward(LuckyDrawReward *reward, bool isDoubleUpReward)
{
	if(reward == nullptr) {
		return;
	}

	//int rewardID = reward->getItemID();
	int count = reward->getQuantity();
	LuckyDrawReward::RewardType type = reward->getRewardType();
	MoneyType moneyType = MoneyTypeUnknown;
	bool isBoosterReward = false;
	
	switch (type) {
		case LuckyDrawReward::RewardFragment: {
			moneyType = MoneyTypePuzzle;
			break;
		}
			
		case LuckyDrawReward::RewardStar: {
			moneyType = MoneyTypeStar;
			break;
		}

		case LuckyDrawReward::RewardBooster: {
			isBoosterReward = true;
			break;
		}
		default: {
			break;
		}
	}

	Analytics::Action action = isDoubleUpReward ?
				Analytics::from_double_up : Analytics::from_lucky_draw;
	
	std::string label = "";
	if(type == LuckyDrawReward::RewardFragment) {
		label = PlayerManager::instance()->getCharName(reward->getItemID());
	}

	if(moneyType != MoneyTypeUnknown) {
		Analytics::instance()->logCollect(action, count, moneyType, label);
	}
	
	if(isBoosterReward) {
		Analytics::instance()->logCollectBooster(action, reward->getItemID(), count);
	}
	

}
