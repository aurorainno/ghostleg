//
//  PlayerManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#include "PlayerManager.h"
#include "CharGameData.h"
#include "PlayerCharData.h"
//#include "GameManager.h"
#include "PlayerRecord.h"
#include "PlayerCharProfile.h"

static PlayerManager *sInstance = nullptr;

namespace {
	// Sorting Method
	
	bool profileSortFunctionDefault(PlayerCharProfile *a, PlayerCharProfile *b)
	{
        // Sort by new unlocked character first
        bool aIsNew = a->isNewChar();
        bool bIsNew = b->isNewChar();
        
        if(aIsNew != bIsNew){
            return aIsNew > bIsNew;
        }
        
		// Sort by selected or not
		int selected = PlayerManager::instance()->getSelectedChar();
		
		bool aSelected = selected == a->getCharID();
		bool bSelected = selected == b->getCharID();
		
		if(aSelected != bSelected) {
			return aSelected ? true : false;
		}
		
		// Sort by charID asending order
		return a->getCharID() < b->getCharID();	// sorted sequence: a, b
	}
	
}


PlayerManager *PlayerManager::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new PlayerManager();
	
	return sInstance;
}

PlayerManager::PlayerManager()
: mCharDataHash()
, mPlayerProfileHash()
, mPlayerCharDataSet(nullptr)
, mGlobalPlaySetting(nullptr)
{
	
	// Setting the internal objects
	setPlayerCharDataSet(PlayerCharDataSet::create());
	setGlobalGameplaySetting(GameplaySetting::create());
}

PlayerManager::~PlayerManager()
{
	setPlayerCharDataSet(nullptr);
}


void PlayerManager::setup()
{
	loadPlayerCharData();		// Load player record
	
	loadData();					// Load Game data
	
	updateCharProfile();		// update the char profile (local data) / need Player & Game Data
	
	updateGlobalGameplaySetting();
}

void PlayerManager::setupMockData()
{
	//setupMockData();
	CharGameData *data;
	
	// 1st Char
	data = CharGameData::create();
	data->setCharID(1);
	data->setAnimeID(1);
	data->setIcon(1);
	data->setBaseMaxSpeed(200);
	data->setBaseTurnSpeed(100);
	data->setBaseAcceleration(150);
	data->setBaseStunReduction(0.3);
	data->setName("PurpleBat");
	saveData(data);
	
	// 2nd Char
	data = CharGameData::create();
	data->setCharID(2);
	data->setAnimeID(2);
	data->setIcon(2);
	data->setBaseMaxSpeed(220);
	data->setBaseTurnSpeed(80);
	data->setBaseAcceleration(100);
	data->setBaseStunReduction(0.5);
	data->setName("BlueBat");
	saveData(data);
	
}


void PlayerManager::loadData()			// Load Data from JSON
{
	std::string content = FileUtils::getInstance()->getStringFromFile("json/charGameData.json");
	
	//log("product json content:\n%s", content.c_str());
	rapidjson::Document sourceJSON;
	sourceJSON.Parse(content.c_str());
	if (sourceJSON.HasParseError())
	{
		CCLOG("PlayerManager.loadData: GetParseError %d\n", sourceJSON.GetParseError());
		return;
	}
	
	mCharDataHash.clear();
	
	for(int i=0; i<sourceJSON.Size(); i++)
	{
		const rapidjson::Value& currentDog = sourceJSON[i];
		CharGameData *data = parseCharGameData(currentDog);
		if(data->isEnable() == false) {
			continue;
		}
		saveData(data);
	}
	
	// log("%s",infoDogData().c_str());
	
}

CharGameData* PlayerManager::parseCharGameData(const rapidjson::Value &sourceJSON){
	
	CharGameData *data = CharGameData::create();
	
	data->parseJSON(sourceJSON);
	
	return data;
}


void PlayerManager::saveData(CharGameData *data)
{
	if(data == nullptr) {
		log("PlayerManager:saveData: data is null");
		return;
	}
	int key = data->getCharID();
	
	mCharDataHash.insert(key, data);
}


CharGameData *PlayerManager::getCharGameData(int charID)
{
	return mCharDataHash.at(charID);
}

std::string PlayerManager::infoCharAbility(const std::vector<int> selectedChar)
{
	std::vector<int> charList;
	if(selectedChar.size() == 0) {
		charList = mCharDataHash.keys();
	} else {
		charList = selectedChar;
	}
	
	
	std::string info = StringUtils::format("Character Count: %ld\n", charList.size());
	
	for(int charID : charList)
	{
		CharGameData *data = getCharGameData(charID);
		std::string name = data == nullptr ? "(Not Found)" : data->getName();
		info += StringUtils::format("%d. %s", charID, name.c_str());
		info += data->infoAbilityList("   ") + "\n";
	}
	
	return info;
}

std::string PlayerManager::infoAllCharData()
{
	std::string info = StringUtils::format("charCount: %ld\n", mCharDataHash.size());
	std::vector<int> mapKeyVec = mCharDataHash.keys();
	
	for(auto key : mapKeyVec)
	{
		CharGameData *data = mCharDataHash.at(key);
		info += data->toString() + "\n";
	}
	
	return info;
}


#pragma mark - PlayerGame


#pragma mark - PlayerCharProfile
std::string PlayerManager::infoPlayerCharProfile()
{
	std::string info = "";
	
	std::vector<int> keys = mPlayerProfileHash.keys();
	
	for(int charID : keys) {
		PlayerCharProfile *profile = mPlayerProfileHash.at(charID);
		
		info += profile->toString() + "\n";
	}
	
	return info;
}

PlayerCharProfile *PlayerManager::getPlayerCharProfile(int charID)
{
	return mPlayerProfileHash.at(charID);
}

void PlayerManager::updateCharProfile()
{
	std::vector<int> mapKeyVec = mCharDataHash.keys();
	
	for(int charID : mapKeyVec)
	{
		PlayerCharProfile *profile = PlayerCharProfile::create();
		profile->setup(charID);
		
		mPlayerProfileHash.insert(charID, profile);
	}
	
}

#pragma mark - Upgrade Related
void PlayerManager::forceUnlock(int charID)
{
	PlayerCharProfile *profile = getPlayerCharProfile(charID);
	if(profile == nullptr) {
		return;
	}

	profile->doUpgradeLogic();
	
	PlayerManager::instance()->updateGlobalGameplaySetting();
	//log("DEBUG: global Gamesetting=%s", PlayerManager::instance()->infoGlobalPlaySetting().c_str());
}

PlayerCharProfile::UpgradeStatus PlayerManager::unlock(int charID)
{
	PlayerCharProfile *profile = getPlayerCharProfile(charID);
	if(profile != nullptr && profile->isLocked() == false) {
		return PlayerCharProfile::UpgradeAlreadyUnlocked;
	}
	
	return upgradeChar(charID);
}

PlayerCharProfile::UpgradeStatus PlayerManager::upgradeChar(int charID)
{
	PlayerCharProfile *profile = getPlayerCharProfile(charID);
	if(profile == nullptr) {
		log("PlayerManager::upgrade. profile is null. charID=%d", charID);
		return PlayerCharProfile::UpgradeError;
	}
	
	PlayerCharProfile::UpgradeStatus result = profile->upgrade();
	
	if(PlayerCharProfile::UpgradeOk != result) {
		return result;
	}
	
	// Update the setting
	PlayerManager::instance()->updateGlobalGameplaySetting();
	
	// log("DEBUG: global Gamesetting=%s", PlayerManager::instance()->infoGlobalPlaySetting().c_str());
	
	
	return result;
}




#pragma mark - PlayerCharData
void PlayerManager::upgradeAllChar()
{
	std::vector<int> mapKeyVec = mCharDataHash.keys();
	
	for(int charID : mapKeyVec)
	{
		PlayerCharProfile *profile = getPlayerCharProfile(charID);
		profile->upgradeToMax();
	}
	
	PlayerManager::instance()->updateGlobalGameplaySetting();
}

void PlayerManager::unlockAllChar()
{
	std::vector<int> mapKeyVec = mCharDataHash.keys();
	
	for(int charID : mapKeyVec)           
	{
		PlayerCharProfile *profile = getPlayerCharProfile(charID);
		profile->doUpgradeLogic();
	}

	PlayerManager::instance()->updateGlobalGameplaySetting();
}

void PlayerManager::resetPlayerCharData()
{
	getPlayerCharDataSet()->reset();
	updateCharProfile();	// update the profile after change
	
	PlayerManager::instance()->updateGlobalGameplaySetting();
}

void PlayerManager::setupMockPlayerCharData()
{
	getPlayerCharDataSet()->setupMockData();
}

std::string PlayerManager::infoPlayerCharData()
{
	return getPlayerCharDataSet()->toString();
}


PlayerCharData *PlayerManager::getPlayerCharData(int charID)
{
	return getPlayerCharDataSet()->getPlayerCharData(charID);
}

void PlayerManager::savePlayerCharData()
{
	getPlayerCharDataSet()->save();
}

void PlayerManager::loadPlayerCharData()
{
	getPlayerCharDataSet()->load();
	
	// XXX: For Alpha Version
	//getPlayerCharDataSet()->unlockChar(2);
}


//bool PlayerManager::savePlayerCharData()
//{
//	std::string content = generateDataContent();		// this is JSON format data
//	
//	//
//	UserDefault *userData = UserDefault::getInstance();
//	if(userData == nullptr) {
//		log("BasePlayerData.save: userDefault not available. dataKey=%s", mDataKey.c_str());
//		return false;
//	}
//	
//	userData->setStringForKey(mDataKey.c_str(), content);
//	
//	return true;
//}
//
//bool BasePlayerData::loadPlayerCharData()
//{
//	UserDefault *userData = UserDefault::getInstance();
//	if(userData == nullptr) {
//		log("BasePlayerData.load: userDefault not available. dataKey=%s", mDataKey.c_str());
//		return false;
//	}
//	
//	std::string content = userData->getStringForKey(mDataKey.c_str(), "");
//	
//	if(content == "") {	// Player Data not yet created
//		setDefault();
//		save();
//	} else {
//		parseDataContent(content);
//	}
//	
//	mIsReady = true;
//	
//	return true;
//}


#pragma mark - Selected Character, Ch
int PlayerManager::getSelectedChar()
{
	return mPlayerCharDataSet->getSelectedChar();
}

PlayerCharProfile *PlayerManager::getSelectedProfile()
{
	int charID = getSelectedChar();
	
	return getPlayerCharProfile(charID);
}

void PlayerManager::selectChar(int charID)
{
	mPlayerCharDataSet->selectChar(charID);
    getPlayerCharProfile(charID)->setNewChar(false);
}


#pragma mark - Character Listing
Vector<PlayerCharProfile *> PlayerManager::getProfileList(
						const CharFilterType &filter, const CharSortType &sort)
{
	Vector<PlayerCharProfile *> result;
	
	std::vector<int> mapKeyVec = mCharDataHash.keys();
	
	for(int charID : mapKeyVec)
	{
		PlayerCharProfile *profile = getPlayerCharProfile(charID);
		if(isFilterMatch(profile, filter)){
			result.pushBack(profile);
		}
	}
	
	// Sorting
	if(sort == CharSortDefault) {
		std::sort(result.begin(), result.end(), profileSortFunctionDefault);
	}

	return result;
}


bool PlayerManager::isFilterMatch(PlayerCharProfile *profile, const CharFilterType &filter)
{
	if(profile == nullptr) {
		return false;
	}
	
	switch(filter) {
		case CharFilterOwned:		return profile->isLocked() == false;
		case CharFilterLocked:		return profile->isLocked();;

		default:
		case CharFilterAll:
			return true;
	}
	
	
}

std::vector<int> PlayerManager::getNewUnlockedCharacters()
{
    Vector<PlayerCharProfile *> list = getLockedCharacters();
    std::vector<int> result{};
    for(PlayerCharProfile* profile : list){
        PlayerCharProfile::UpgradeStatus status = unlock(profile->getCharID());
        if(status == PlayerCharProfile::UpgradeStatus::UpgradeOk){
            result.push_back(profile->getCharID());
        }
    }
    return result;
}

Vector<PlayerCharProfile *> PlayerManager::getOwnedCharacters(const CharSortType &sort)
{
	return getProfileList(CharFilterOwned, sort);
}

Vector<PlayerCharProfile *> PlayerManager::getLockedCharacters(const CharSortType &sort)
{
	return getProfileList(CharFilterLocked, sort);
}

int PlayerManager::getTotalCharCount()
{
	return (int) mPlayerProfileHash.size();
}


bool PlayerManager::isCharacterLocked(int charID)
{
	PlayerCharProfile *profile = getPlayerCharProfile(charID);
	
	return profile == nullptr ? true : profile->isLocked();
}

bool PlayerManager::isCharacterNeedFragment(int charID)
{
	PlayerCharProfile *profile = getPlayerCharProfile(charID);
	
	if(profile == nullptr) {
		return false;
	}
	
	return profile->isMaxLevel() == false;
}


void PlayerManager::addPlayerFragments(const std::vector<int> charList)
{
	bool anyChange = false;
	
	for(int charID : charList) {
		PlayerCharProfile *profile = getPlayerCharProfile(charID);
		if(profile == nullptr) {
			continue;
		}
		profile->alterFragmentCount(1);		// +1
	}
	
	savePlayerCharData();
}

void PlayerManager::addPlayerFragment(int charID, int fragCount)
{
	if(charID <= 0 || fragCount <= 0) {
		return;
	}

	PlayerCharProfile *profile = getPlayerCharProfile(charID);
	if(profile == nullptr) {
		return;
	}
	
	profile->alterFragmentCount(fragCount);
	
	savePlayerCharData();
	
	return;
}


#pragma mark - GameplaySetting

void PlayerManager::updateGlobalGameplaySetting()
{
	mGlobalPlaySetting->reset();
	
	Vector<PlayerCharProfile *> profiles = PlayerManager::getProfileList(CharFilterOwned, CharSortNone);
	for(PlayerCharProfile *profile : profiles) {
		profile->updateGlobalGameplaySetting(mGlobalPlaySetting);
	}
//	
//	
//	PlayerCharProfile *profile = PlayerManager::instance()->getSelectedProfile();
//	if(profile == nullptr) {
//		log("updateGameplaySetting: profile is null");
//		return;
//	}
//	
//	mGameplaySetting->reset();
//	
//	profile->updateGameplaySetting(mGameplaySetting);
//	
//	
//	mGameplaySetting->setMaxSpeed(profile->getMaxSpeed());
//	mGameplaySetting->setTurnSpeed(profile->getTurnSpeed());
//	mGameplaySetting->setAcceleration(profile->getAcceleration());
//	mGameplaySetting->setStunReduction(profile->getStunReduction());
	
}
GameplaySetting *PlayerManager::getCharacterGameplaySetting(int charID, bool includeGlobal)
{
	PlayerCharProfile *profile = PlayerManager::instance()->getPlayerCharProfile(charID);
	
	return getCharacterGameplaySetting(profile, includeGlobal);
}

GameplaySetting *PlayerManager::getCharacterGameplaySetting(PlayerCharProfile *profile, bool includeGlobal)
{
	if(profile == nullptr) {
		log("getCharacterGameplaySetting: profile is null");
		return nullptr;
	}

	GameplaySetting *setting = GameplaySetting::create();
	
	
	profile->updateGameplaySetting(setting);
	
	if(includeGlobal) {
		setting->add(mGlobalPlaySetting);
	}
	
	
	// Conclusion of player speed attribute
	setting->setMaxSpeed(profile->getCharGameData()->getBaseMaxSpeed());
	setting->setTurnSpeed(profile->getCharGameData()->getBaseTurnSpeed());
	setting->setAcceleration(profile->getCharGameData()->getBaseAcceleration());
	setting->setStunReduction(profile->getCharGameData()->getBaseStunReduction());

	
	return setting;
}

GameplaySetting *PlayerManager::getCurrentGameplaySetting(bool includeGlobal)
{
	return getCharacterGameplaySetting(getSelectedChar(), includeGlobal);
}


std::string PlayerManager::infoGlobalPlaySetting()
{
	std::string info = "";
	
	std::vector<int> keys = mPlayerProfileHash.keys();
	
	for(int charID : keys) {
		PlayerCharProfile *profile = mPlayerProfileHash.at(charID);
		
		info += profile->toString() + "\n";
	}
	
	return info;
}


std::string PlayerManager::getCharName(int charID)
{
	CharGameData *gameData = getCharGameData(charID);
	
	return gameData == nullptr ? "Courier" : gameData->getName();
}
