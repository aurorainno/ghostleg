//
//  ItemManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 19/1/2017.
//
//

#include "ItemManager.h"
#include "GameManager.h"
#include "GameWorld.h"
#include "Player.h"
#include "ItemEffect.h"
#include "Star.h"
#include "Coin.h"
#include "MagnetCapsule.h"
#include "SpeedUpCapsule.h"
#include "Cashout.h"

// JSON
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

#include "GameplaySetting.h"

static ItemManager *sInstance = nullptr;

const char *profileKey = "profile.booster";

const float kMaxMissileSpeed = 300;
const float kMissileAddOnSpeed = 50;

// Power Up
const float kDefaultSpeedUpDuration = 5.0f;
const float kDefaultCashOutDuration = 7.0f;
const float kDefaultMagnetDuration  = 6.0f;

// Booster
const float kDefaultShieldDuration  = 5.0f;
const float kDefaultTimeStopDuration  = 5.0f;
//const float kDefaultShieldDuration  = 5.0f;

// Booster Object setting
const float kMissileInitialSpeed = 350;
const float kMissileAcceleration = 300;
const float kMissileMaxSpeed = 500;

const float kCycloneInitialSpeed = 350;
const float kCycloneAcceleration = 300;
const float kCycloneMaxSpeed = 500;


ItemManager *ItemManager::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new ItemManager();
	
	return sInstance;
}

ItemManager::ItemManager()
: mBoosterDataSet()
, mMissileSpeedReduction(0)
, mCycloneSpeedReduction(0)
{
	
}

ItemManager::~ItemManager()
{
	
}

#pragma mark - General
void ItemManager::setup()
{
	loadBoosterData();
	loadPlayerData();
}


#pragma mark - Player Booster
// TODO: Change to StatusCode instead of true / false
bool ItemManager::buyBooster(BoosterItemType type, int quantity)
{
	BoosterData *data = getBoosterData(type);
	if(data == nullptr) {
		log("ItemManager::buyBooster: data is null");
		return false;
	}
	
	if(checkBoosterMoney(type, quantity) == false) {
		log("ItemManager: not enough money");
		return false;
	}
	
	
	// Add booster
	addBoosterCount(type, quantity);
	
	// Deduce money
	int total = data->getPrice().amount * quantity;
	MoneyType moneyType = data->getPrice().type;

	return GameManager::instance()->useMoney(moneyType, total);
}


bool ItemManager::checkBoosterMoney(BoosterItemType type, int quantity)
{
	BoosterData *data = getBoosterData(type);
	if(data == nullptr) {
		log("ItemManager::buyBooster: data is null");
		return false;
	}
	
	int total = data->getPrice().amount * quantity;
	MoneyType moneyType = data->getPrice().type;
	
	return GameManager::instance()->checkMoneyValue(moneyType, total);
}

void ItemManager::savePlayerData()
{
	std::string content = mPlayerBooster.toJSONContent();
	
	//
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("ItemManager::savePlayerData");
		return;
	}
	
	userData->setStringForKey(profileKey, content);
}

void ItemManager::loadPlayerData()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("ItemManager::savePlayerData");
		mPlayerBooster.setDefault();
		savePlayerData();
		return;
	}
	
	std::string content = userData->getStringForKey(profileKey);
	if(content == "") {
		mPlayerBooster.setDefault();
		savePlayerData();
		return;
	}
	
	mPlayerBooster.parseJSONContent(content);
}

std::string ItemManager::toString()
{
	
	std::string result = "";
	
	result = "PlayerBooster:\n";
	result += mPlayerBooster.toString() + "\n";
	
	
	return result;
}


int ItemManager::getBoosterCount(BoosterItemType type)
{
	return mPlayerBooster.getBoosterCount(type);
}

void ItemManager::addBoosterCount(BoosterItemType type, int count)
{
	mPlayerBooster.addBooster(type, count);
	
	savePlayerData();
}

void ItemManager::deduceBoosterCount(BoosterItemType type, int count)
{
	mPlayerBooster.deduceBooster(type, count);
	
	savePlayerData();
}

#pragma mark - BoosterData
float ItemManager::getBoosterCooldown(BoosterItemType type)
{
	BoosterData *data = getBoosterData(type);
	
	return data == nullptr ? 5 : data->getCooldown();
}

bool ItemManager::getBoosterIsInstantActive(BoosterItemType type)
{
    BoosterData *data = getBoosterData(type);
    return data->getIsInstantActive();
}

std::string ItemManager::getBoosterName(BoosterItemType type)
{
	BoosterData *data = getBoosterData(type);
	
	return data == nullptr ? "Unknown" : data->getName();
}

Price ItemManager::getBoosterPrice(BoosterItemType type)
{
	BoosterData *data = getBoosterData(type);
	
	return data == nullptr ? Price(MoneyTypeStar, 9999999) : data->getPrice();
}

std::string ItemManager::getBoosterInfo(BoosterItemType type)
{
	BoosterData *data = getBoosterData(type);
	
	return data == nullptr ? "Unknown" : data->getInfo();
}

BoosterData *ItemManager::getBoosterData(BoosterItemType type)
{
	if(mBoosterDataSet.find(type) == mBoosterDataSet.end()) {
		return nullptr;
	}
	
	return mBoosterDataSet.at(type);
}

void ItemManager::loadBoosterData()
{
	std::string content = FileUtils::getInstance()->getStringFromFile("json/booster.json");
	
	// log("suit json content:\n%s", content.c_str());
	rapidjson::Document jsonDoc;
	jsonDoc.Parse(content.c_str());
	if (jsonDoc.HasParseError())
	{
		CCLOG("ItemManager.loadBoosterData %d\n", jsonDoc.GetParseError());
		return;
	}
	
	// Clean up
	mBoosterDataSet.clear();
	
	// Loop and parse
	for (rapidjson::Value::ConstMemberIterator itr = jsonDoc.MemberBegin();
					itr != jsonDoc.MemberEnd();
					++itr) {
		
		std::string key = itr->name.GetString();
		const rapidjson::Value &object = jsonDoc[key.c_str()];
		
		int type = BoosterData::parseBoosterType(key);
		
		// log("loadBoosterData: key=%s value=%d", key.c_str(), type);
		BoosterData *data = BoosterData::create();
		data->parseJSON(object);
		data->setType((BoosterItemType) type);
		
		//
		mBoosterDataSet.insert(type, data);
	}
}

void ItemManager::setupMockData()
{
	BoosterData *boosterData;
	
	boosterData = BoosterData::create();
	boosterData->setType(BoosterItemDoubleCoin);
	boosterData->setInfo("Slow down the enemies");
	boosterData->setPrice(Price(MoneyTypeDiamond, 30));
	
	addBoosterData(boosterData);

	boosterData = BoosterData::create();
	boosterData->setType(BoosterItemMissile);
	boosterData->setInfo("Kill the enemies with missiles");
	boosterData->setPrice(Price(MoneyTypeStar, 3000));
	
	addBoosterData(boosterData);
}


void ItemManager::addBoosterData(BoosterData *data)
{
	mBoosterDataSet.insert(data->getType(), data);
}

std::string ItemManager::infoBoosterDataSet()
{
	std::string result = "";
	
	result += StringUtils::format("BoosterData Count: %ld\n", mBoosterDataSet.size());
	
	std::vector<int> mapKeyVec = mBoosterDataSet.keys();
	
	for(auto key : mapKeyVec)
	{
		BoosterData *data = mBoosterDataSet.at(key);
		
		result += data->toString() + "\n";
	}
	
	return result;
}


#pragma mark - Booster & Power Setting
float ItemManager::getMissileSpeed()
{
	//float playerSpeed = GameWorld::instance()->getPlayer()->getSpeed();
	float missileSpeed = 250;		//playerSpeed + kMissileAddOnSpeed;
	if(missileSpeed > kMaxMissileSpeed) {
		missileSpeed = kMaxMissileSpeed;
	}
	
	if(mMissileSpeedReduction > 0) {
		missileSpeed -= (missileSpeed * mMissileSpeedReduction);
	}
	
	return missileSpeed;
}

void ItemManager::setupBoosterGameSetting()
{
	GameplaySetting *setting = GameWorld::instance()->getGameplaySetting();
	
	mMissileSpeedReduction = setting->getAttributeValue(GameplaySetting::MissleSpeedReduction);
	mCycloneSpeedReduction = setting->getAttributeValue(GameplaySetting::CycloneSpeedReduction);
	
	// TODO: cycloneSpeedReduction
}

std::string ItemManager::infoBoosterGameSetting()
{
	std::string result = "";
	
	result += StringUtils::format("MissileSpeedReduction=%f\n", mMissileSpeedReduction);
	result += StringUtils::format("CycloneSpeedReduction=%f\n", mCycloneSpeedReduction);
	
	return result;
}

float ItemManager::getFinalBoosterDuration(float originValue, int attributeValue)
{
	GameplaySetting::Attribute attribute = (GameplaySetting::Attribute) attributeValue;
	
	
	float extra = GameWorld::instance()->getGameplaySetting()->getAttributeValue(attribute);
	// log("DEBUG: attribute=%d extra=%f", attribute, extra);
	if(extra == 0) {
		return originValue;
	}
	
	return originValue + extra;
}


//float
void ItemManager::modifyPowerUpProperty(ItemEffect::Type type, ItemEffect *effect)
{
	if(effect == nullptr) {
		return;
	}
	// log("DEBUG: type=%d effect=%s", type, effect->toString().c_str());
	if(ItemEffect::ItemEffectSpeedUp == type) {
		float duration = getFinalBoosterDuration(kDefaultSpeedUpDuration,
												 GameplaySetting::Attribute::ExtraSpeedUpDuration);
		effect->setMainProperty(duration);
		
		// Faster SpeedUp
		float bonusSpeed = GameWorld::instance()->getGameplaySetting()
							->getAttributeValue(GameplaySetting::Attribute::ExtraSpeedUpEffect);
		if(bonusSpeed > 0) {
			effect->setProperty("bonus", bonusSpeed);
		}
		
	} else if(ItemEffect::ItemEffectCashOut == type) {
		float duration = getFinalBoosterDuration(kDefaultCashOutDuration,
												 GameplaySetting::Attribute::ExtraCashOutDuration);
		effect->setMainProperty(duration);
		
	} else if(ItemEffect::ItemEffectStarMagnet == type) {
		float duration = getFinalBoosterDuration(kDefaultMagnetDuration,
												 GameplaySetting::Attribute::ExtraMagnetDuration);
		effect->setMainProperty(duration);
	}
}

void ItemManager::modifyBoosterProperty(ItemEffect::Type type, ItemEffect *effect)
{
	if(effect == nullptr) {
		return;
	}
	
	float originDuration = effect->getProperty("duration");
	
	if(ItemEffect::ItemEffectTimeStop == type) {
		float duration = getFinalBoosterDuration(originDuration,
												 GameplaySetting::Attribute::ExtraTimeStopDuration);
		effect->setProperty("duration", duration);
		
	} else if(ItemEffect::ItemEffectInvulnerable == type) {
		float duration = getFinalBoosterDuration(originDuration,
												 GameplaySetting::Attribute::ExtraShieldDuration);
		effect->setProperty("duration", duration);
	} else if(ItemEffect::ItemEffectDoubleCoins == type) {
		float duration = getFinalBoosterDuration(originDuration,
												 GameplaySetting::Attribute::ExtraDoubleCoinDuration);
		effect->setProperty("duration", duration);
	}
}

void ItemManager::defineItemEffectProperty(ItemEffect *effect)
{
	if(effect == nullptr) {
		return;
	}
	
	ItemEffect::Type type = effect->getType();
	
	if(effect->getIsPowerUp()) {
		modifyPowerUpProperty(type, effect);
	} else {
		modifyBoosterProperty(type, effect);
	}
	
}


SpeedData ItemManager::getMissileSpeedData()
{
//	const float kMissileInitialSpeed = 200;
//	const float kMissileAcceleration = 100;
//	const float kMissileMaxSpeed = 400;
//	
//	const float kCycloneInitialSpeed = 200;
//	const float kCycloneAcceleration = 100;
//	const float kCycloneMaxSpeed = 400;
//
	//
	
	SpeedData result;
	
	// calculate the initial speed
	float initialSpeed = kMissileInitialSpeed;
	
	if(mMissileSpeedReduction > 0) {
		initialSpeed -= (initialSpeed * mMissileSpeedReduction / 100);
	}
	

	
	// Setting the Acceleration and Max
	result.acceleration = kMissileAcceleration;
	result.maxSpeed = kMissileMaxSpeed;
	result.initialSpeed = initialSpeed;
	
	
	return result;
}

void ItemManager::preloadCsb()
{
	std::vector<std::string> preloadList;
    preloadList.push_back(Star::kSmallStarCsbFile);
    preloadList.push_back(Coin::kSmallCoinCsbFile);
	preloadList.push_back(MagnetCapsule::kCsbFile);
	preloadList.push_back(SpeedUpCapsule::kCsbFile);
	preloadList.push_back(Cashout::kCsbFile);
	
	
	for(std::string csbFile : preloadList) {
		CSLoader::createTimeline(csbFile);
		CSLoader::createNode(csbFile);
	}
}

SpeedData ItemManager::getCycloneSpeedData()
{
	SpeedData result;
	// calculate the initial speed
	//float initialSpeed = kCycloneInitialSpeed;
	Player *player = GameWorld::instance()->getPlayer();
	float playerSpeed = (player == nullptr) ? 200 : player->getMaxSpeed();
	float initialSpeed = playerSpeed + 100;
	
	if(mCycloneSpeedReduction > 0) {
		initialSpeed -= (initialSpeed * mCycloneSpeedReduction / 100);
	}
	
	
	// Setting the Acceleration and Max
	result.acceleration = kCycloneAcceleration;
	result.maxSpeed = initialSpeed + 200;
	result.initialSpeed = initialSpeed;
	
	
	return result;
}


//
//private:
//float mMissileSpeedReduction;		// percent of slow
//
