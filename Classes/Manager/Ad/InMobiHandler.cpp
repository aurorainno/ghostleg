//
//  InMobiHandler.cpp
//  GhostLeg
//
//  Created by Ken Lee on 6/6/2017.
//
//



#include "InMobiHandler.h"

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include "AdManager.h"

InMobiHandler::InMobiHandler()
: AdHandler()
, sdkbox::InMobiListener()
{
	//log("");
}

void InMobiHandler::setup()
{
	sdkbox::PluginInMobi::setLogLevel(sdkbox::PluginInMobi::SBIMSDKLogLevel::kIMSDKLogLevelDebug);
	// Setup
	sdkbox::PluginInMobi::init();
	sdkbox::PluginInMobi::setListener(this);
	
	
	addAd(AdManager::VideoAd::Video1, "video1");
	addAd(AdManager::VideoAd::Video2, "video1");
	//
	sdkbox::PluginInMobi::loadInterstitial("video1");
}

bool InMobiHandler::isAdReady(int key)
{
	return isAdReady("");
}

bool InMobiHandler::isAdReady(const std::string &name)
{
	bool result = sdkbox::PluginInMobi::isInterstitialReady();
	
	//log("DEBUG: isAdReady. name=%s result=%d", name.c_str(), result);
	
	return result;
}

void InMobiHandler::showVideo(const std::string &name)
{
	if (sdkbox::PluginInMobi::isInterstitialReady()) {
		sdkbox::PluginInMobi::showInterstitial();
	}
	
}


void InMobiHandler::showBanner(const std::string &name)
{
	log("InMobiHandler: banner not available");
}

void InMobiHandler::hideBanner(const std::string &name)
{
	
}

void InMobiHandler::setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback)
{
	mRewardedCallback = callback;
}

void InMobiHandler::setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback)
{
	mFinishedCallback = callback;
}

#pragma mark - Listener
//
void InMobiHandler::interstitialDidDismiss()
{
	if(mRewardedCallback) {
		mRewardedCallback(true);
	}
	
	if(mFinishedCallback) {
		mFinishedCallback(true);
	}
	
	
	log("DEBUG: InMobiHandler: video Ad dismissed");
	// for next time
	sdkbox::PluginInMobi::loadInterstitial();

}

#endif
