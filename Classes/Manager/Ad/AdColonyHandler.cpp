//
//  AdColonyHandler.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/7/2016.
//
//

#include "AdColonyHandler.h"
#include "AdManager.h"

namespace {
	std::string infoAdColony(const sdkbox::AdColonyAdInfo& info)
	{
		std::string result = "";
		
		result += "name=" + info.name;
		result += " zone=" + info.zoneID;
		
		return result;
	}
}

AdColonyHandler::AdColonyHandler()
{
	
}

void AdColonyHandler::setup()
{
	sdkbox::PluginAdColony::init();
	sdkbox::PluginAdColony::setListener(this);
	//sdkbox::PluginAdColony::`
	
	// Setup the Ads
	addAd(AdManager::VideoAd::Video1, "video1");
	addAd(AdManager::VideoAd::Video2, "video2");
}

void AdColonyHandler::showVideo(const std::string &name)
{
	log("Try to show Adcolony video: %s", name.c_str());
	sdkbox::PluginAdColony::show(name);
}


void AdColonyHandler::showBanner(const std::string &name)
{
	log("AdColonyHandler: banner not available");
}

void AdColonyHandler::hideBanner(const std::string &name)
{
	
}

void AdColonyHandler::setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback)
{
	mRewardedCallback = callback;
}

void AdColonyHandler::setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback)
{
	mFinishedCallback = callback;
}

#pragma mark - Listener
void AdColonyHandler::onAdColonyChange(const sdkbox::AdColonyAdInfo& info, bool available)
{
	log("onAdColonyChange: %s available=%d", infoAdColony(info).c_str(), available);
	setAdReady(info.name, available);
}

void AdColonyHandler::onAdColonyReward(const sdkbox::AdColonyAdInfo& info,
							const std::string& currencyName, int amount, bool success)
{
	
	log("onAdColonyReward: %s amount=%d success=%d", infoAdColony(info).c_str(), amount, success);
	if(mRewardedCallback) {
		mRewardedCallback(success);
	}
}

void AdColonyHandler::onAdColonyStarted(const sdkbox::AdColonyAdInfo& info)
{
	log("onAdColonyStarted: %s", infoAdColony(info).c_str());
}

void AdColonyHandler::onAdColonyFinished(const sdkbox::AdColonyAdInfo& info)
{
	// See
	//		http://discuss.cocos2d-x.org/t/strange-ttf-issue-after-adcolony-ad-plays-solved/24832/2
	//	ken: was error in Android
	cocos2d::Director::getInstance()->getScheduler()->performFunctionInCocosThread([=](){});

	log("onAdColonyFinished: %s", infoAdColony(info).c_str());
	if(mFinishedCallback) {
		mFinishedCallback(true);
	}
}

