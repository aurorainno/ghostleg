//
//  AdmobHandler.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/7/2016.
//
//

#ifndef AdmobHandler_hpp
#define AdmobHandler_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "PluginAdMob/PluginAdMob.h"
#include "AdHandler.h"

USING_NS_CC;

class AdmobHandler : public AdHandler, public sdkbox::AdMobListener
{
public:
	AdmobHandler();
	
	virtual void setup();
	virtual bool isAdReady(const std::string &name);
	virtual void showVideo(const std::string &name);
	virtual void showBanner(const std::string &name);
	virtual void hideBanner(const std::string &name);
	virtual void setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback);
	virtual void setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback);
	virtual void reward(const std::string &name, const std::string &currency, double amount);
public:
	virtual void adViewDidReceiveAd(const std::string &name);
	virtual void adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg);
	virtual void adViewWillPresentScreen(const std::string &name);
	virtual void adViewDidDismissScreen(const std::string &name);
	virtual void adViewWillDismissScreen(const std::string &name);
	virtual void adViewWillLeaveApplication(const std::string &name);
	
private:
	VideoCallback mRewardedCallback;
	VideoCallback mFinishedCallback;
	
};

#endif /* AdmobHandler_hpp */
