//
//  AdmobHandler.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/7/2016.
//
//

#include "AdmobHandler.h"
#include "AdManager.h"

#pragma mark - AdHandler implementation

AdmobHandler::AdmobHandler()
{
	
}

void AdmobHandler::setup()
{
	sdkbox::PluginAdMob::init();
	sdkbox::PluginAdMob::setListener(this);
	
	// Setup the Ads
	addAd(AdManager::VideoAd::Video1, "video1");
	addAd(AdManager::VideoAd::Video2, "video2");
//	addAd(AdManager::VideoAd::PlayAgain, "VideoInter");
//	addAd(AdManager::VideoAd::OneMoreChance, "VideoInter");
//	addAd(AdManager::VideoAd::GainStar, "VideoInter");
	
	
	//addAd(AdManager::VideoAd::OneMoreChance, "VideoReward");
	//addAd(AdManager::VideoAd::GainStar, "VideoReward");
	
//	addAd(AdManager::BannerAd::MainBannerAd, "Banner");
	
	
	// Cache the Ads
	std::vector<std::string> adList = getAdNameList();
//	adList.push_back("testBanner");
//	adList.push_back("testInter");
//	adList.push_back("testVideo");
	
	
	for(std::string adName : adList) {
		sdkbox::PluginAdMob::cache(adName);
	
		bool isOkay = sdkbox::PluginAdMob::isAvailable(adName);
		log("AdmobHandler: name=%s isOkay=%d", adName.c_str(), isOkay);
	}
	
}

void AdmobHandler::showVideo(const std::string &name)
{
	bool isOkay = sdkbox::PluginAdMob::isAvailable(name);
	if(isOkay == false) {
		log("AdmobHandler: name=[%s] not available", name.c_str());
	}
	
	sdkbox::PluginAdMob::show(name);
//    sdkbox::PluginAdMob::cache(name);

}


void AdmobHandler::showBanner(const std::string &name)
{
	sdkbox::PluginAdMob::show(name);
}

void AdmobHandler::hideBanner(const std::string &name)
{
	sdkbox::PluginAdMob::hide(name);
}

bool AdmobHandler::isAdReady(const std::string &name)
{
	return sdkbox::PluginAdMob::isAvailable(name);
}

void AdmobHandler::setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback)
{
	mRewardedCallback = callback;
}

void AdmobHandler::setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback)
{
	mFinishedCallback = callback;
}



#pragma mark - AdmobListener
void AdmobHandler::adViewDidReceiveAd(const std::string &name)
{
	log("AdmobHandler: adViewDidReceiveAd is called. name:[%s]",name.c_str());
	setAdReady(name, true);
}

void AdmobHandler::adViewDidFailToReceiveAdWithError(const std::string &name, const std::string &msg){
	log("AdmobHandler: adViewDidFailToReceiveAdWithError is called. name:%s msg:%s\n",name.c_str(),msg.c_str());
}

void AdmobHandler::adViewWillPresentScreen(const std::string &name){
	log("AdmobHandler: adViewWillPresentScreen is called. name:%s\n",name.c_str());
}

void AdmobHandler::adViewDidDismissScreen(const std::string &name)
{
	log("AdmobHandler: adViewDidDismissScreen: %s", name.c_str());
	if(mFinishedCallback){
		mFinishedCallback(true);
	}
	if(mRewardedCallback) {
		mRewardedCallback(true);
	}
}

void AdmobHandler::adViewWillDismissScreen(const std::string &name){
	log("AdmobHandler: adViewWillDismissScreen is called. name:%s\n",name.c_str());
}

void AdmobHandler::adViewWillLeaveApplication(const std::string &name){
	log("AdmobHandler: adViewWillLeaveApplication is called. name:%s\n",name.c_str());
}

void AdmobHandler::reward(const std::string &name,
						  const std::string &currency, double amount) {
	log("AdmobHandler: reward is called. name:%s\n",name.c_str());
}

