//
//  AdHandler.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/7/2016.
//
//

#include <stdio.h>
#include "AdHandler.h"

AdHandler::AdHandler()
: mAdReadyMap()
, mAdList()
{
	
}

bool AdHandler::isAdReady(int key)
{
	std::string adName = mAdList[key];
	if(adName == "") {
		return false;
	}
	
	return isAdReady(adName);
}

bool AdHandler::isAdReady(const std::string &name)
{
	return mAdReadyMap[name];
}

std::string AdHandler::getAdName(int key)
{
	return mAdList[key];
}

void AdHandler::setAdReady(const std::string &name, bool flag)
{
	log("setReady=%s flag=%d", name.c_str(), flag);
	mAdReadyMap[name] = flag;
}

void AdHandler::addAd(int key, const std::string &name)
{
	mAdList[key] = name;
}

std::string AdHandler::infoAdList()
{
	std::string result = "";
	
	std::map<int, std::string>::iterator it = mAdList.begin();
	for(; it != mAdList.end(); it++) {
		int key = it->first;
		std::string ad = it->second;
		
		result += StringUtils::format("%d: %s ready=%d", key, ad.c_str(), isAdReady(ad));
		result += "\n";
	}
	
	return result;
}

std::vector<std::string> AdHandler::getAdNameList()
{
	std::set<std::string> nameSet;
	std::vector<std::string> result;
	std::map<int, std::string>::iterator it = mAdList.begin();
	for(; it != mAdList.end(); it++) {
		std::string ad = it->second;
		
		
		
		result.push_back(ad);
	}
	
	return result;
}
