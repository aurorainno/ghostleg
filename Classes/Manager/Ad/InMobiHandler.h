//
//  InMobiHandler.hpp
//  GhostLeg
//
//  Created by Ken Lee on 6/6/2017.
//
//

#ifndef InMobiHandler_hpp
#define InMobiHandler_hpp

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#include <stdio.h>
#include "cocos2d.h"
#include "AdHandler.h"
#include "PluginInMobi/PluginInMobi.h"

USING_NS_CC;

class InMobiHandler : public AdHandler, public sdkbox::InMobiListener
{
public:
	InMobiHandler();
	
	virtual void setup();
	virtual void showVideo(const std::string &name);
	virtual void showBanner(const std::string &name);
	virtual void hideBanner(const std::string &name);
	virtual void setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback);
	virtual void setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback);
	
	virtual bool isAdReady(int key);
	virtual bool isAdReady(const std::string &name);
	
public: // listener
	virtual void interstitialDidDismiss();
	virtual void bannerRewardActionCompletedWithRewards(const std::map<std::string, std::string>& rewards) {}
	virtual void interstitialRewardActionCompletedWithRewards(const std::map<std::string, std::string>& rewards) {}
	
private:
	VideoCallback mRewardedCallback;
	VideoCallback mFinishedCallback;
	
};

#endif
#endif /* InMobiHandler_hpp */


