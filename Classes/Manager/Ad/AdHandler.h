//
//  AdHandler.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/7/2016.
//
//

#ifndef AdHandler_hpp
#define AdHandler_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <map>
#include <vector>
#include <string>

USING_NS_CC;

class AdHandler : public Ref
{
public:
	typedef std::function<void(bool)> VideoCallback;
	
public:
	AdHandler();
	
	virtual void setup() = 0;
	
	virtual void showVideo(const std::string &name) = 0;
	virtual void showBanner(const std::string &name) = 0;
	virtual void hideBanner(const std::string &name) = 0;
	virtual void setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback) = 0;
	virtual void setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback) = 0;

public: // non virtual
	virtual bool isAdReady(int key);
	virtual bool isAdReady(const std::string &name);
	
	std::string getAdName(int key);
	std::string infoAdList();
	std::vector<std::string> getAdNameList();
	
protected:
	void addAd(int key, const std::string &name);		// key is the enum of the Ad
	void setAdReady(const std::string &name, bool flag);
	
private:
	std::map<std::string, bool> mAdReadyMap;
	std::map<int, std::string> mAdList;		// int: integral key of the given add
};

#endif /* AdHandler_hpp */
