//
//  AdColonyHandler.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/7/2016.
//
//

#ifndef AdColonyHandler_hpp
#define AdColonyHandler_hpp

#include <stdio.h>


#include "cocos2d.h"
#include "PluginAdColony/PluginAdColony.h"
#include "AdHandler.h"

USING_NS_CC;

class AdColonyHandler : public AdHandler, public sdkbox::AdColonyListener
{
public:
	AdColonyHandler();
	
	virtual void setup();
	virtual void showVideo(const std::string &name);
	virtual void showBanner(const std::string &name);
	virtual void hideBanner(const std::string &name);
	virtual void setVideoRewardedCallback(const std::function<void(bool isSuccess)> &callback);
	virtual void setVideoFinishedCallback(const std::function<void(bool hasPlayed)> &callback);
	
private:
	void onAdColonyChange(const sdkbox::AdColonyAdInfo& info, bool available);
	void onAdColonyReward(const sdkbox::AdColonyAdInfo& info,
							const std::string& currencyName, int amount, bool success);
	void onAdColonyStarted(const sdkbox::AdColonyAdInfo& info);
	void onAdColonyFinished(const sdkbox::AdColonyAdInfo& info);

private:
	VideoCallback mRewardedCallback;
	VideoCallback mFinishedCallback;
	
};
#endif /* AdColonyHandler_hpp */
