//
//  SuitManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 13/6/2016.
//
//

#include "SuitManager.h"

#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include <iostream>
#include "Suit.h"
#include "Player.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "StringHelper.h"
#include "ItemEffect.h"
#include "ItemEffectFactory.h"
#include "Analytics.h"

#define kKeyPlayerSuit			"ghostleg.suitData"
#define kKeySelectPlayerSuit	"ghostleg.selectedSuit"

#define kDefaultSuitID			1


#

//
namespace {
	ItemEffect::Type getPassiveItemTypeEffect(int suitID) {
		return ItemEffect::Type::ItemEffectNone;
	}
}

//
SuitManager::SuitManager()
: mSelectedSuit(kDefaultSuitID)
, mSelectedSuitLevel(1)
{
	
}


void SuitManager::initPlaySuit()
{
	if(mPlayerSuitArray.size() == 0)
    {
		// Need to setup the
		PlayerSuit *suit = new PlayerSuit();
		suit->setSuitID(kDefaultSuitID);
		suit->setLevel(1);
		
		mPlayerSuitArray.pushBack(suit);
		suit->release();
	}
	
	setPlayerSuitLevel(kDefaultSuitID, 1);
    if(UserGameData::isPaidApp()){setPlayerSuitLevel(2,1);}//iron dog default level 1 for paid app

}



void SuitManager::loadPlayerSelectedSuit()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("SuitManager.loadPlayerSuit: userDefault not available");
		initPlaySuit();
		return;
	}
	
	mSelectedSuit = userData->getIntegerForKey(kKeySelectPlayerSuit, kDefaultSuitID);

}

void SuitManager::loadPlayerSuit()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("SuitManager.loadPlayerSuit: userDefault not available");
		initPlaySuit();
		return;
	}
	
	mPlayerSuitArray.clear();
	
	std::string content = userData->getStringForKey(kKeyPlayerSuit, "");
	
	if(content == "") {
		initPlaySuit();
		log("Debug:%s", this->infoPlayerSuitArray().c_str());
		return;
	}
	
	std::stringstream ss(content);
	std::string to;
	
	while(std::getline(ss, to, '\n')){
		PlayerSuit *suit = new PlayerSuit();
		
		suit->parse(to);
		
		if(suit->getSuitID() > 0) {
			mPlayerSuitArray.pushBack(suit);	// retain happen here!
		}
		
		suit->release();
	}
	
	// Special setting for default suit
	if(getPlayerSuitLevel(1) <= 0) {
		initPlaySuit();
	}
	
	log("Debug:%s", this->infoPlayerSuitArray().c_str());
	
}

void SuitManager::savePlayerSuit()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("SuitManager.savePlayerSuit: userDefault not available");
		return;
	}
	
	
	std::string playerData = "";
	for(int i=0; i<mPlayerSuitArray.size(); i++) {
		PlayerSuit *data = mPlayerSuitArray.at(i);
		playerData += data->toString();
		playerData += "\n";
	}
	
	userData->setStringForKey(kKeyPlayerSuit, playerData);
}

/***
 *
 *  JSONData *jsonData = loadJSONData("suit.json");
 *  for( .....)
 *   {
 *      SuitData *data = parseSuitFromJSON(....)		// autorelease
 *       mSuitArray.pushBack(data);
 *  }
 *
 */
SuitData *SuitManager::generateSuitDataFromJSON(const rapidjson::Value &suitJSON){
	
	SuitData *data = SuitData::create();
	
	data->setSuitID(suitJSON["suitID"].GetInt());
	data->setName(suitJSON["name"].GetString());
	data->setInfo(suitJSON["info"].GetString());
	data->setMaxLevel(suitJSON["maxLevel"].GetInt());
    data->setCharIcon(suitJSON["icon"].GetString());
    data->setItemIcon(suitJSON["valueIcon"].GetString());
    data->setDisplayFormat(suitJSON["displayFormat"].GetString());
	
	
	// Item Effect : Passive, Item, Value
	const char *effectName;
	ItemEffect::Type effectType;
	
	// Passive
	effectName = suitJSON["passive"].GetString();
	effectType = ItemEffect::getTypeByName(effectName);
	data->setPassiveSkill(effectType);
	
	effectName = suitJSON["item"].GetString();
	effectType = ItemEffect::getTypeByName(effectName);
	data->setItemEffect(effectType);
	
	for(int k=0;k<suitJSON["value"].Size();k++){
		data->addLevelValue(suitJSON["value"][k].GetDouble());
		//printf("%.1f, ",suitJSON["value"][k].GetDouble());
	}
	
	
	// Price
	for(int j=0;j<suitJSON["price"].Size();j++){
		data->addLevelPrice(suitJSON["price"][j].GetInt());
		//printf("%i, ",suitJSON["price"][j].GetInt());
	}

	
	
//
	return data;
}

void SuitManager::loadSuitData()
{
	
	std::string content = FileUtils::getInstance()->getStringFromFile("json/suit.json");
	
	log("suit json content:\n%s", content.c_str());
	rapidjson::Document suitArray;
	suitArray.Parse(content.c_str());
	if (suitArray.HasParseError())
	{
		CCLOG("GetParseError %d\n", suitArray.GetParseError());
		return;
	}
	
	mSuitArray.clear();
	
	for(rapidjson::SizeType i=0;i<suitArray.Size();i++){
		
		const rapidjson::Value &currentSuit = suitArray[i];
		SuitData *data = generateSuitDataFromJSON(currentSuit);
		mSuitArray.pushBack(data);
	}
	
}

Vector<SuitData *> SuitManager::getSuitArray()
{
	return mSuitArray;
}




PlayerSuit *SuitManager::getPlayerSuit(int suitID)
{
	for(int i=0; i<mPlayerSuitArray.size(); i++) {
		PlayerSuit *data = mPlayerSuitArray.at(i);
		if(data->getSuitID() == suitID) {
			return data;
		}
	}
	
	return nullptr;
}

void SuitManager::setPlayerSuitLevel(int suitID, int level)
{
	PlayerSuit *suit = getPlayerSuit(suitID);
	
	if(suit != nullptr) {
		suit->setLevel(level);
		return;
	}
	
	// Make a new one
	suit = new PlayerSuit();
	suit->setSuitID(suitID);
	suit->setLevel(level);
	
	mPlayerSuitArray.pushBack(suit);
	
	
	suit->release();
}


int SuitManager::getPlayerSuitLevel(int suitID)
{
	PlayerSuit *suit = getPlayerSuit(suitID);
	
	return suit == nullptr ? 0 : suit->getLevel();
}

int SuitManager::upgradeSuit(int suitID)
{
	
	SuitData *suitData = getSuitData(suitID);
    int requiredCoin = 0;
	
	if(suitData == nullptr) {
		return -3;
	}
	
	int currentLevel = getPlayerSuitLevel(suitID);
	if(currentLevel == suitData->getMaxLevel()) {
		return -2;
	}
	
    
    if(suitID!=2||currentLevel>0){
        int playerCoin = GameManager::instance()->getUserData()->getTotalCoin();
        requiredCoin = suitData->getUpgradePrice(currentLevel + 1);
        log("SuitManager::upgradeSuit: playerCoin=%d,requiredCoin=%d",playerCoin,requiredCoin);
	
        if(playerCoin < requiredCoin) {
            return -1;
        }
	
        // Save the consumed coin
        GameManager::instance()->getUserData()->addCoin(-requiredCoin);
    }
    
    	// Save the suit after level up
	setPlayerSuitLevel(suitID, currentLevel+1);
	savePlayerSuit();
    log("suit leveled up!, currentlevel:%d",getPlayerSuitLevel(suitID));
	
	
	//Analytics::instance()->logSuitUpgrade(suitID, currentLevel+1, requiredCoin);
    
	return 0;
}

SuitData *SuitManager::getSuitData(int suitID)
{
	for(int i=0; i<mSuitArray.size(); i++) {
		SuitData *data = mSuitArray.at(i);
		if(data->getSuitID() == suitID) {
			return data;
		}
	}
	
	return nullptr;
}




std::string SuitManager::infoSuitArray()
{
	std::string count = StringUtils::format("%zd", mSuitArray.size());
	std::string result = "Suit Count: " + count;
	
	result += "\n";
	for(int i=0; i<mSuitArray.size(); i++) {
		SuitData *data = mSuitArray.at(i);
		
		result += data->toString();
		result += "\n";
	}
	
	return result;
}


void SuitManager::resetAllSuit()
{
	for(int i=0; i<mPlayerSuitArray.size(); i++) {
		PlayerSuit *suit = mPlayerSuitArray.at(i);
		
		suit->setLevel(suit->getSuitID() == 1 ? 1 : 0);
	}
    selectPlayerSuit(1);
    savePlayerSelectedSuit();
    savePlayerSuit();
}

std::string SuitManager::infoPlayerSuitArray()
{
	int selected = getSelectedSuit();
	
	
	std::string result = "Player Suit. selected=" + INT_TO_STR(selected);

	std::string count = StringUtils::format("%zd", mPlayerSuitArray.size());
	result += " Count: " + count;
	
	result += "\n";
	for(int i=0; i<mPlayerSuitArray.size(); i++) {
		PlayerSuit *data = mPlayerSuitArray.at(i);
		
		result += data->toString();
		result += "\n";
	}
	
	return result;
}

// The current value of the player suit (base on current level)
int SuitManager::getCurrentSuitValue(int suitID)
{
	SuitData *suit = getSuitData(suitID);
	if(suit == nullptr) {
		return 0;
	}
	
	int currentLevel = getPlayerSuitLevel(suitID);
	
	return suit->getLevelValue(currentLevel);
}


											   
void SuitManager::savePlayerSelectedSuit()
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("SuitManager.savePlayerSelectedSuit: userDefault not available");
		return;
	}
	
	

	userData->setIntegerForKey(kKeySelectPlayerSuit, mSelectedSuit);
}

											   
void SuitManager::selectPlayerSuit(int suitID)
{
	mSelectedSuit = suitID;
	savePlayerSelectedSuit();

	// log GA
	//Analytics::instance()->logSuitUse(suitID);
}

int SuitManager::getSelectedSuit()
{
	return mSelectedSuit;
}

std::string SuitManager::getSuitName(int suitID)
{
	SuitData *suit = getSuitData(suitID);
	if(suit == nullptr)
	{
		return "";
	}
	
	
	return suit->getName();
}

std::string SuitManager::getSelectedSuitString(bool withLevel)
{
	std::string result = getSuitName(mSelectedSuit);
	
	if(withLevel) {
		result += "_lv" + INT_TO_STR(getPlayerSuitLevel(mSelectedSuit));
	}
	
    
    return result;
    
}


ItemEffect *SuitManager::getPassiveItemEffect()
{
	SuitData *suit = getSuitData(mSelectedSuit);
	if(suit == nullptr) {
		return nullptr;
	}
	
	ItemEffect::Type effectType = suit->getPassiveSkill();
	if(effectType == ItemEffect::Type::ItemEffectNone) {
		return nullptr;
	}
	
	// Create the ItemEffect
	ItemEffect *effect = ItemEffectFactory::createEffectByType(effectType);
	if(effect == nullptr) {
		return nullptr;
	}
	
	// Set the ItemEffect value
	int level = mSelectedSuitLevel; // getPlayerSuitLevel();
	float value = suit->getLevelValue(level);
	effect->setMainProperty(value);
	
	return effect;
	
}

void SuitManager::getCapsuleItemEffectAndValue(ItemEffect::Type &outType, float &outValue)
{
	// initial value
	outType = ItemEffect::Type::ItemEffectNone;
	outValue = 0;
	
	SuitData *suit = getSuitData(mSelectedSuit);
	if(suit == nullptr) {
		log("debug: suit is null. %d", mSelectedSuit);
		return;
	}
	
	ItemEffect::Type effectType = suit->getItemEffect();
	log("DEBUG: item used=%s capsule=%d", ItemEffect::getTypeName(effectType).c_str(),
									(int) ItemEffect::isCapsuleItemEffect(effectType));
	
	if(ItemEffect::isCapsuleItemEffect(effectType) == false) {
		log("debug: not capsule item. %d type=%d", mSelectedSuit, effectType);
		return;
	}
	
	int level =	mSelectedSuitLevel;		// getPlayerSuitLevel(suit->getSuitID());
	float value = suit->getLevelValue(level);
	
	// Set to the output
	outType = effectType;
	outValue = value;
}

void SuitManager::setDogSuitAndLevel(int suitID, int level)
{
	mSelectedSuit = suitID;
	mSelectedSuitLevel = level;
}

int SuitManager::getSelectedSuitLevel()
{
	return mSelectedSuitLevel;
}


//ItemEffectType::Type SuitManager::getCapsuleItemEffect()
//{
//	
//}
