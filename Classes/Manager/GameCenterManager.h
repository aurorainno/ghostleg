//
//  GameCenterManager.h
//  GhostLeg
//
//  Created by Calvin on 27/5/2016.
//
//

#ifndef GameCenterManager_h
#define GameCenterManager_h

#include "PluginSdkboxPlay/PluginSdkboxPlay.h"

class GameCenterManager : public sdkbox::PluginSdkboxPlay, sdkbox::SdkboxPlayListener {
	
public:
	GameCenterManager();
	void setup();
    void sendBestDistance(int score);
    void showBestDistanceLeaderBoard();
    bool hasSignedIn();
	
	//virtual void onScoreSubmitted( const std::string& leaderboard_name, long score, bool maxScoreAllTime, bool maxScoreWeek, bool maxScoreToday )=0;
private:
	virtual void onConnectionStatusChanged(int status) override;
	virtual void onScoreSubmitted(const std::string & leaderboardName ,
                           long score ,
                           bool maxScoreAllTime ,
                           bool maxScoreWeek ,
                           bool maxScoreToday) override;
	
    virtual void onIncrementalAchievementUnlocked ( const std::string & achievementName ) override;
    virtual void onIncrementalAchievementStep ( const std::string & achievementName ,double step ) override;
    virtual void onAchievementUnlocked ( const std::string & achievement_name ,
                                                bool newlyUnlocked ) override;

private:
	bool mHasSetup;
};


#endif /* GameCenterManager_h */
