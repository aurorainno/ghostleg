//
//  JSONPlayerData.hpp
//
//	JSON Format for PlayerData
//  BounceDefense
//
//  Created by Ken Lee on 30/8/2016.
//
//

#ifndef JSONPlayerData_hpp
#define JSONPlayerData_hpp

#include <stdio.h>

#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

#include "cocos2d.h"

USING_NS_CC;


class JSONData
{
public:			// helper method
	
	// Note need to define implement in header for template
	// reference: http://stackoverflow.com/questions/10632251/undefined-reference-to-template-function
	template <typename T>
	static void convertVector(const Vector<T> &ccVector, std::vector<JSONData *> &stdVector)
	{
		for(int i=0; i<ccVector.size(); i++) {
			T item = ccVector.at(i);
			stdVector.push_back((JSONData *) item);
		}
	}
	
public:
	JSONData();
	

	
	std::string toJSONContent();
	bool parseJSONContent(const std::string &jsonContent);

	
	
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue) {} ;	// Define the JSON Doc
	virtual bool parseJSON(const rapidjson::Value &jsonValue) { return true; }

protected:
	virtual bool isJSONObject();
	
	

	
	

protected:	// Helper Methods (For Writing)
	void addString(rapidjson::Document::AllocatorType &allocator,
						const std::string &name, const std::string &str,
						rapidjson::Value &outValue);
	void addDouble(rapidjson::Document::AllocatorType &allocator,
						const std::string &name, double value, rapidjson::Value &outValue);
	void addInt(rapidjson::Document::AllocatorType &allocator,
						const std::string &name, int value,
						rapidjson::Value &outValue);
	void addObject(rapidjson::Document::AllocatorType &allocator,
					const std::string &name, JSONData *object, 
					rapidjson::Value &outValue);

	void addJsonValue(rapidjson::Document::AllocatorType &allocator,
					  const std::string &name,
					  rapidjson::Value &value,
					  rapidjson::Value &outValue);

	// template <class T>
    
    void addIntDataArray(rapidjson::Document::AllocatorType &allocator,
                         const std::string &name,
                         const std::vector<int> &intVector,
                         rapidjson::Value &outValue);

    
	void addJSONDataArray(rapidjson::Document::AllocatorType &allocator,
						const std::string &name,
						const std::vector<JSONData *> &objectVector,
						rapidjson::Value &outValue);
	
	template<class T>
	void addObjectArray(rapidjson::Document::AllocatorType &allocator,
						  const std::string &name,
						  const Vector<T> &objectVector,
						  rapidjson::Value &outValue)
	{
		std::vector<JSONData *> jsonDataList;
		
		JSONData::convertVector(objectVector, jsonDataList);
		addJSONDataArray(allocator, name, jsonDataList, outValue);
	}
	
	
	
	void addDataMap(rapidjson::Document::AllocatorType &allocator,
						const std::string &name,
						const std::map<int, int> &valueMap,
						rapidjson::Value &outValue);
	
	void addDataMap(rapidjson::Document::AllocatorType &allocator,
					const std::string &name,
					const std::map<std::string, int> &valueMap,
					rapidjson::Value &outValue);

protected:
	
};


#endif /* JSONPlayerData_hpp */
