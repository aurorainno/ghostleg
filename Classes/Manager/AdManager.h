//
//  AdManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 22/6/2016.
//
//

#ifndef AdManager_hpp
#define AdManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "PluginAdMob/PluginAdMob.h"
#include "AdHandler.h"
#include <vector>

USING_NS_CC;



class AdManager : public sdkbox::AdMobListener
{
public:
	enum Provider {
		None		= 0,
		Auto		= 1,		// Automatically Selection
		Admob		= 2,
		AdColony	= 3,
		InMobi		= 4,
//		Adcolony,
//		Vungle,
	};
	
	enum AdType {
		Banner,
		Video,		// offical name as "interstitial"
	};
	
	enum VideoAd {
		OneMoreChance = 100,		// rename later
		GainStar,
		PlayAgain,
		Video1,
		Video2,
	};
	
	
	enum Ad {
		AdPlayAgain			= 200,			// Interstitial
		AdGameContinue		= 201,
		AdFreeCoin			= 202,
		AdFreeGacha			= 203,
		AdDoubleReward		= 204,			// double the quantity of Gacha
	};
	
	enum BannerAd {
		MainBannerAd = 10,
	};
	
	
	//typedef std::function<void(Ref *, GameWorldEvent, int arg1)> EventCallback;
	typedef std::function<void(int)> VideoAdCallback;
    static AdManager* instance();
    
public:
    AdManager();
	~AdManager();
	
	CC_SYNTHESIZE(Provider, mVideoProvider, VideoProvider);
	
	void init();		// Setup SDKBOX ad integration
	
	//
	
	
	
	//
	bool showAdBeforeReplay();
	void resetShowReplayAdCounter();
	
	
	
	void showBannerAd(const std::string &name);
	void hideBannerAd(const std::string &name);
	void hideBannerAd();		// Hide last banner Ad
	
	bool showInterstitualAd();
	bool showVideoAd(VideoAd video, Provider provider=Auto);
	bool showVideoAd(const std::string &name, Provider provider=Auto);
										// User
										// adManager->showVideoAd("OneMoreChance", [](bool isPlayed) {
										//							if(isPlayed) { handleOneMoreChance(); }
										//					} );
	bool isNoAd();
	void setNoAd(bool flag);
	
	Provider findDefaultVideoProvider(VideoAd ad);
	bool isVideoReady(Provider provider, VideoAd ad);
	bool isProviderAvailable(Provider provider);
	
	void setVideoRewardedCallback(const AdHandler::VideoCallback &callback);
	void setVideoFinishedCallback(const AdHandler::VideoCallback &callback);
	
	AdHandler *getAdHandler(Provider provider);
	
	// Globle setting for Advertisemen, currently NO AD For Paid Android Version
	bool isAdEnabled();
	bool canShowReplayAd();
	
private:
	
	
	void onVideoRewarded(bool flag);
	void onVideoFinished(bool flag);
private:
	
    VideoAdCallback mCallback;
	int mReplayAdCounter;
	std::string mLastBanner;	// name of last banner
	
	std::vector<AdHandler *> mHandlerList;
	AdHandler *mAdmobHandler;
	AdHandler *mAdColonyHandler;
	AdHandler *mInMobiHandler;
	
	AdHandler::VideoCallback mRewardedCallback;
	AdHandler::VideoCallback mFinishedCallback;

	std::vector<Provider> mVideoProviderList;
	
#pragma mark - Auto play
public:
	void findSelectedProviderAndAd(const Ad &toPlayAd, Provider &outProvider, VideoAd &outAd);
	bool playVideoAd(const AdManager::Ad &toPlayAd,
					const AdHandler::VideoCallback &callback);
private:
	std::vector<AdManager::VideoAd> getTargetVideoAdList(const AdManager::Ad &toPlayAd);
	VideoAd getTargetVideoAd(const Ad &toPlayAd);
	std::vector<Provider> getProviderOrder(const Ad &toPlayAd);

};

#endif /* AdManager_hpp */
