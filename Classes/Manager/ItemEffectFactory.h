//
//  ItemManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 13/6/2016.
//
//

#ifndef ItemEffectFactory_h
#define ItemEffectFactory_h

#include <stdio.h>
#include <vector>
#include <map>

#include "ItemEffect.h"

class ItemEffectFactory
{
public:
	static ItemEffect *createEffectByType(ItemEffect::Type type);
	static ItemEffect *createPowerUpItem(ItemEffect::Type type);
};


#endif /* ItemManager_hpp */
