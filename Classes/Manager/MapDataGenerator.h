//
//  MapDataGenerator.hpp
//  GhostLeg
//
//
//
//  Created by Ken Lee on 17/3/2016.
//
//

#ifndef MapDataGenerator_hpp
#define MapDataGenerator_hpp

#include <stdio.h>
#include <string>
#include "cocos2d.h"

USING_NS_CC;

class Player;
class GameMap;
class GameWorld;
class Enemy;
class NPC;
class MapLine;
class Item;
class MapLevelData;
class LevelItemData;
class LevelEnemyData;
class LevelNpcData;

class MapDataGenerator
{
public:
	MapDataGenerator();
	virtual ~MapDataGenerator();
	
	
	virtual void init(GameMap *map, Player *player, int rangeStart);

	CC_SYNTHESIZE(int, mLastCapsulePos, LastCapsulePos);
	CC_SYNTHESIZE(int, mInitialOffset, InitialOffset);
    CC_SYNTHESIZE(int, mCustomNpcAnime, CustomNpcAnime);
	
	void setCapsuleType(int effectType, int level, float value);
	
	void generate();
	
	

	// Getting of internal data
	int getRangeStart();
	int getRangeEnd();
	const Vector<MapLine *>& getHoriLine();
	const Vector<Enemy *>& getEnemy();
	const Vector<NPC *>& getNpcList();
	const Vector<Item *>& getItemList();

	int getMapLevelByDistance(int distance);
	
	
	// Getting string information
	std::string info();
	std::string infoLine();
	std::string infoEnemy();
	std::string infoNpc();
	std::string infoItem();

	// Static helper
	void generateMapLinesFromLevel(MapLevelData *mapData);
	void generateEnemiesFromLevel(MapLevelData *mapData);
	void generateItemsFromLevel(MapLevelData *mapData);	
	void generateNpcFromLevel(MapLevelData *mapData);
	void generateCapsules(MapLevelData *mapData);
	
	

	// internal logic (open for unit testing)
	void generateByLevel();
	void generateByMapID(int mapID, bool isNpcMap=false);		// Call by generateByLevel
	void generateByNpcMapID(int mapID);
	void generateByRandom();
	void generateByMapData(int mapID, MapLevelData *mapData);

	int getMapID();
	Vec2 getDropPosition();
	Vec2 getStopPosition();
	
	// 
	void addModelToWorld(GameWorld *world);
	void setRangeStart(int offset);
    
	// Testing
	void testAddEnemyForPosition();
	void testAddLineForPosition();
protected:
//	// Result
	
	int mMapID;
	int mRangeStart;
	int mRangeEnd;
	int mPlayerY;
	
	int mCapsuleEffect;		// ItemEffect::Type
	int mCapsuleLevel;		// Level of the skills
	float mCapsuleValue;	// The value of the effect
	
	// Result of line and enemy to be added
	Vector<MapLine *> mHoriLine;
	Vector<Enemy *> mEnemy;
	Vector<NPC *> mNpcList;
	Vector<Item *> mItemList;
	Vec2 mDropPosition;
	Vec2 mStopPosition;

	CC_PROPERTY(Player *, mPlayer, Player);
	CC_PROPERTY(GameMap *, mMap, GameMap);

protected:
	void resetData();
	void generateHoriLine();
	void generateEnemy();
	void generateItems();
	
	
//	Player *getPlayer();
//	
//	GameMap *MapDataGenerator::getGameMap()
//	{
//		return mMap;
//	}

	
	
	void generateLineByRandom();
	void generateEnemyByRandom();
	void generateItemByRandom();

    void generateCoins();
    void generatePotions();

	void addEnemyWithLevelData(LevelEnemyData *enemy, int startX);
	void addNpcWithLevelData(LevelNpcData *npcData);
	MapLine *getEnemyStandingLine(const Vec2 &position);
	
	void addEnemyForPosition(int pos);
	void addLineForPosition(int pos);
	int determineMapByDistance(int distance);
	
	void generateStarFromHoriLine();
	
#pragma mark - Async Map Generation
public:
	//void generateNpcMap();
	
#pragma mark - Async Map Generation
public://	generateByMapID(int mapID, bool isNpcMap)
	void generateGameMapAsync(int mapID, bool isNpcMap);		// just define the list of elements to be generate
	void generateGameMapByData(int mapID, MapLevelData *mapData);
	void generateMapElements(GameWorld *world);
	std::string infoMapElementQueue();

	CC_SYNTHESIZE(bool, mIsGenerationDone, IsGenerationDone);
	
protected:
	void createItemFromQueue(GameWorld *world, int count);
	void createEnemyFromQueue(GameWorld *world, int count);
	void createEnemy(GameWorld *world, LevelEnemyData *data);
	void createItem(GameWorld *world, LevelItemData *data);
	void createNpc(GameWorld *world, LevelNpcData *data);
	
private:
	std::vector<LevelItemData *> mItemQueue;
	std::vector<LevelEnemyData *> mEnemyQueue;
	LevelNpcData *mNpcData;
};

#endif /* MapDataGenerator_hpp */
