//
//  TutorialManager.cpp
//  GhostLeg
//
//  Created by Ken Lee on 4/7/2016.
//
//

#include "TutorialManager.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"
#include "JSONHelper.h"

static TutorialManager *sInstance = nullptr;

const bool kHasLastStep = false;

namespace {
	Rect getRectFromArray(const std::vector<int> &intArray)
	{
		if(intArray.size() == 0) {
			return Rect::ZERO;
		}
		
		float x = (float) intArray[0];
		float y = (float) intArray[1];
		float width = (float) intArray[2];
		float height = (float) intArray[3];
		
		Rect rect = Rect(x, y, width, height);
		
		return rect;
	}
	
	Vec2 getPositionFromArray(const std::vector<int> &intArray) {
		if(intArray.size() == 0) {
			return Vec2::ZERO;
		}
		
		float x = (float) intArray[0];
		float y = (float) intArray.size() <= 1 ? 0 : intArray[1];
		
		return Vec2(x, y);
	}
}

TutorialManager::TutorialManager()
{
	
}

TutorialManager *TutorialManager::instance(){
	if(sInstance == nullptr){
		sInstance = new TutorialManager();
	}
	
	return sInstance;
}


static Vec2 gridToScreenPos(const Vec2 &gridPos)
{
	int totalGridY = 24;
	
	float px = gridPos.x * 18 + 25;
	float py = gridPos.y * 25;
	
	return Vec2(px, py);
}

void TutorialManager::loadTutorialData()
{
    std::string content = FileUtils::getInstance()->getStringFromFile("json/tutorial.json");
    
    log("tutorial json content:\n%s", content.c_str());
    rapidjson::Document tutorialDataArray;
    tutorialDataArray.Parse(content.c_str());
    if (tutorialDataArray.HasParseError())
    {
        CCLOG("GetParseError %d\n", tutorialDataArray.GetParseError());
        return;
    }
    
    mTutorialDataList.clear();
    
    for(rapidjson::SizeType i=0;i<tutorialDataArray.Size();i++){
        const rapidjson::Value &currentData = tutorialDataArray[i];
		
		TutorialData *data = generateSingleDataFromJSON(currentData, i);
		
        mTutorialDataList.pushBack(data);
    }

	if(kHasLastStep) {
		addLastStep();
	}

}

TutorialData* TutorialManager::generateSingleDataFromJSON(
							const rapidjson::Value& data, unsigned index){
	
	TutorialData *tutorialData = TutorialData::create();
	
	tutorialData->setStep(index);
	
	// Start Condition
	int startCondValue = aurora::JSONHelper::getInt(data, "startCondArg", 0);
	tutorialData->setStartConditionArg(startCondValue);
	
	std::string startCond = aurora::JSONHelper::getString(data, "startCond", "Auto");
	tutorialData->setStartCondition(TutorialData::getStartConditionByName(startCond));
	
	
	// End Condition
	std::string endCond = aurora::JSONHelper::getString(data, "endCond", "tapAnywhere");
	tutorialData->setEndCondition(TutorialData::getEndConditionByName(endCond));
	
	
	//
	std::vector<int> intArray; Rect rect;
	
	// First Touch Area
	intArray.clear();
	aurora::JSONHelper::getJsonIntArray(data, "startTouchArea", intArray);
	rect = getRectFromArray(intArray);
	tutorialData->setHitArea(rect);
	
	// End Touch Areas
	intArray.clear();
	aurora::JSONHelper::getJsonIntArray(data, "endTouchArea", intArray);
	rect = getRectFromArray(intArray);
	tutorialData->setSlopeEndHitArea(rect);
	
	
	// Display

	// Display Position Type
	std::string posType = aurora::JSONHelper::getString(data, "positionType", "");
	tutorialData->setDisplayPosType(TutorialData::getPositionTypeByName(posType));
	
	// Display Pos
	intArray.clear();
	aurora::JSONHelper::getJsonIntArray(data, "displayPos", intArray);
	tutorialData->setDisplayPosition(getPositionFromArray(intArray));
	
	// Display Csb
	std::string displayCsb = aurora::JSONHelper::getString(data, "displayCsb", "");
	tutorialData->setDisplayCsb(displayCsb);

	// Is the Csb Loop Animation 
	bool flag = aurora::JSONHelper::getBool(data, "isLoopAnimation", true);
	tutorialData->setIsLoopAnimation(flag);

	// Command
	std::string cmd = aurora::JSONHelper::getString(data, "command", "");
	tutorialData->setCommand(cmd);
	
	
	// old
//    tutorialData->setHitArea(Rect(data["hitArea"][0].GetInt(), data["hitArea"][1].GetInt(),
//                                  data["hitArea"][2].GetInt(), data["hitArea"][3].GetInt()));
//    tutorialData->setSlopeEndHitArea(Rect(data["slopeEndHitArea"][0].GetInt(), data["slopeEndHitArea"][1].GetInt(),
//                                  data["slopeEndHitArea"][2].GetInt(), data["slopeEndHitArea"][3].GetInt()));
//    tutorialData->setDisplayPosType(TutorialData::getPositionTypeByName(data["positionType"].GetString()));
//    tutorialData->setDisplayPosition(Vec2(data["displayPos"][0].GetInt(),data["displayPos"][1].GetInt()));
//    tutorialData->setDisplayCsb(data["displayCsb"].GetString());
	
	
	
	// Dialog
	//
	std::string msg = aurora::JSONHelper::getString(data, "dialogMsg", "");
	bool showDialog = aurora::JSONHelper::getBool(data, "showDialog", msg != "");
	tutorialData->setShowNpcDialog(showDialog);
	tutorialData->setDialogMessage(msg);
	
	
	// mIsLoopAnimation
	
    return tutorialData;
}

void TutorialManager::addLastStep()
{
	int step = (int) mTutorialDataList.size();
	
	TutorialData *tutorialData = TutorialData::create();
	tutorialData->setStep(step);
    tutorialData->setStartCondition(TutorialData::StartCondition::AutoStart);
	tutorialData->setStartConditionArg(550);
	tutorialData->setDisplayType(TutorialData::DisplayType::DisplayTypeBanner);
	tutorialData->setDisplayCsb("tutorial_dialog5.csb");

	tutorialData->setEndCondition(TutorialData::EndCondition::DeltaTime);
	tutorialData->setEndConditionArg(2);	// one second

	mTutorialDataList.pushBack(tutorialData);
}

void TutorialManager::setMockData()
{
	mTutorialDataList.clear();
	
	
	TutorialData *tutorialData;
	
	int step = 1;
	
	// Step 1:
	tutorialData = TutorialData::create();
	tutorialData->setStep(step);
	tutorialData->setStartCondition(TutorialData::StartCondition::AutoStart);
	tutorialData->setStartConditionArg(0);
	tutorialData->setEndCondition(TutorialData::EndCondition::TapAnywhere);
	mTutorialDataList.pushBack(tutorialData);

	step++;
	
	// Step 2:
	tutorialData = TutorialData::create();
	tutorialData->setStep(step);
	tutorialData->setStartCondition(TutorialData::StartCondition::PlayerY);
	tutorialData->setStartConditionArg(100);
	tutorialData->setEndCondition(TutorialData::EndCondition::DropLine);
	tutorialData->setHitArea(Rect(30, 200, 80, 60));
	mTutorialDataList.pushBack(tutorialData);
	
	step++;
	
	// Step 3:
	addLastStep();
}

std::string TutorialManager::infoTutorialData()
{
	std::string result = "";
	
	result += StringUtils::format("stepCount=%d\n", (int)mTutorialDataList.size());
	
	for(int i=0; i<mTutorialDataList.size(); i++) {
		TutorialData *data = mTutorialDataList.at(i);
		
		result += data->toString();
		result += "\n";
	}
	
	
	return result;
}

TutorialData *TutorialManager::getTutorialData(int step)
{
	if(step < 0) {
		step = (int) mTutorialDataList.size() + step;
	}
	
	
	if(step >= mTutorialDataList.size()) {
		return nullptr;
	}
	
	
	
	return mTutorialDataList.at(step);
}

bool TutorialManager::isLastStep(int step)
{
	return step == mTutorialDataList.size()-1;
}


Vector<TutorialData *> &TutorialManager::getTutorialDataList()
{
	return mTutorialDataList;
}
