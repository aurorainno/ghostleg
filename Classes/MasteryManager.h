//
//  MasteryManager.hpp
//  GhostLeg
//
//  Created by Ken Lee on 16/5/2016.
//
//

#ifndef MasteryManager_hpp
#define MasteryManager_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <string>
#include <iostream>
#include "external/json/document.h"
#include "external/json/writer.h"
#include "external/json/stringbuffer.h"

USING_NS_CC;


class Player;
class MasteryData;
class PlayerMastery;

class MasteryManager
{
public:
	MasteryManager();
	
	void loadMasteryData();
	void loadPlayerMastery();
	void savePlayerMastery();
	
	Vector<MasteryData *> getMasteryArray();
	int getPlayerMasteryLevel(int masteryID);
	int getDogMasteryLevel(int masteryID);
	void setPlayerMasteryLevel(int masteryID, int level);
	int upgradeMastery(int masteryID);		// success or not, return -3 if nullptr, return -2 if max lvl,
                                            // return -1 if not enough stars, return 0 if success.
	void resetAllMastery();
	
	MasteryData *getMasteryData(int masteryID);
	std::string getMasteryName(int masteryID);
	
	std::string infoMasteryArray();
	std::string infoPlayerMasteryArray();

	float getCurrentMasteryValue(int masteryID);
	void setPlayerMasteryAttribute(Player *player);
	
	
private:
	PlayerMastery *getPlayerMastery(int masteryID);
    MasteryData *generateSingleMasteryFromJSON(const rapidjson::Value &masteryJSON);
	
private:
	Vector<MasteryData *> mMasteryArray;
	Vector<PlayerMastery *> mPlayerMasteryArray;
};

#endif /* MasteryManager_hpp */
