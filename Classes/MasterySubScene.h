//
//  MasterySubScene.h
//  GhostLeg
//
//  Created by Calvin on 17/5/2016.
//
//

#ifndef MasterySubScene_h
#define MasterySubScene_h

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>

#include "CommonType.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class MasterySubScene : public Layer{
public:
    CREATE_FUNC(MasterySubScene);
    virtual bool init();
    
private:
    void setupTouchListener();
    void setupUI(Node *mainPanel);
private:
    Text* mLevelText;
    Text* mTitleText;
    Text* mDescriptionText;
    Sprite* mIcon;
    Node* mNormalUpgradePanel,mMaxUpgradePanel,mUnlockUpgradePanel;
    
    
};
#endif /* MasterySubScene_h */
