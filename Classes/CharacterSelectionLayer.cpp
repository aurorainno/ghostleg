//
//  CharacterSelectionLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 13/2/2017.
//
//

#include "CharacterSelectionLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "FBProfilePic.h"
#include "StageParallaxLayer.h"
#include "ViewHelper.h"
#include "CommonMacro.h"
#include "GameModel.h"
#include "DogManager.h"
#include "DogData.h"
#include "CharacterItemView.h"
#include "LuckyDrawDialog.h"
#include "PlayerCharProfile.h"
#include "PlayerManager.h"
#include "CharacterInfoLayer.h"
#include "UnlockDogLayer.h"
#include "GameSound.h"
#include "AnimeNode.h"
#include "TutorialLayer.h"
#include "Analytics.h"
#include "ViewHelper.h"

const int kLockedItemViewHeight = 117;
const int kUnlockedItemViewHeight = 137;
const int kPanelMarginY = 15;
const int kPanelUpperMarginY = 70;

CharacterSelectionLayer::CharacterSelectionLayer():
mIsScene(0)
,mContentSizeHeight(0)
,mItemViewMap(0)
,mTutorialLayer(nullptr)
{}

Scene* CharacterSelectionLayer::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    CharacterSelectionLayer *layer = CharacterSelectionLayer::create();
    layer->mIsScene = true;
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}

bool CharacterSelectionLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    StageParallaxLayer *parallaxLayer = StageParallaxLayer::create();
    parallaxLayer->setBGSpeed(0);
    parallaxLayer->setStage(1);
    addChild(parallaxLayer);
    parallaxLayer->scheduleUpdate();
    
    mRootNode = CSLoader::createNode("CharacterSelectionLayer.csb");
    setupUI();
    addChild(mRootNode);
    
    return true;
}

void CharacterSelectionLayer::setupUI()
{
    FIX_UI_LAYOUT(mRootNode);
    
    mMainPanel = mRootNode->getChildByName("mainPanel");
    mCoverPanel = mRootNode->getChildByName("coverPanel");
    
    mStarText = (Text*) mRootNode->getChildByName("totalCoinText");
    mDiamondText = mRootNode->getChildByName("diamondPanel")->getChildByName<Text*>("totalDiamondText");
    
    mFbConnectBtn = (Button*) mRootNode->getChildByName("fbBtn");
    mFbName = (Text *) mRootNode->getChildByName("fbName");
    
    mProfilePicFrame = (Sprite *) mRootNode->getChildByName("profilePicFrame");
    mProfilePic = FBProfilePic::create();
    mProfilePic->defineSize(Size(47, 47));
    mProfilePic->setAnchorPoint(Vec2::ZERO);
    mProfilePic->setPosition(Vec2(2,2));
    mProfilePicFrame->addChild(mProfilePic);
    mProfilePic->setLocalZOrder(-1);
    
    mFbConnectBtn->addClickEventListener([&](Ref*){
        AstrodogClient::instance()->connectFB();
    });

    mScrollView = mMainPanel->getChildByName<ui::ScrollView*>("scrollView");
    mScrollView->setInnerContainerSize(Size::ZERO);
    
    mCancelBtn = mMainPanel->getChildByName<Button*>("backBtn");
    mCancelBtn->addClickEventListener([&](Ref*){
        if(mIsScene){
            Director::getInstance()->popScene();
        }else{
            this->removeFromParent();
        }
        if(mCloseCallback){
            mCloseCallback();
        }
    });
    
    mUnlockSprite = mMainPanel->getChildByName<Sprite*>("unlockSprite");
    mUnlockSprite->setVisible(false);
}

void CharacterSelectionLayer::onEnter()
{
    Layer::onEnter();

    updateDiamond();
    updateStars();
    setupFbProfile();
    
    resetView();
    showSelectCharCoachmark();
	
	LOG_SCREEN(Analytics::scn_character_main);
}

void CharacterSelectionLayer::update(float delta)
{
    if(mTutorialLayer){
        mTutorialLayer->update(delta);
    }
}

void CharacterSelectionLayer::resetView()
{
    mContentSizeHeight = 0;
    Node* lockedContainer = mScrollView->getChildByName("lockedPanel")->getChildByName("itemViewContainer");
    Node* unlockedContainer = mScrollView->getChildByName("unlockedPanel")->getChildByName("itemViewContainer");
    lockedContainer->removeAllChildren();
    unlockedContainer->removeAllChildren();
    
    Vector<PlayerCharProfile*> lockedList = PlayerManager::instance()->getLockedCharacters();
    Vector<PlayerCharProfile*> ownedList = PlayerManager::instance()->getOwnedCharacters();
    
    setScrollView(lockedList, ownedList);
}

void CharacterSelectionLayer::showSelectCharCoachmark()
{
    
    //	GameManager::instance()->getUserData()->setCoachmarkPlayed("selectChar", true);
    //	mCoachmarkShown = true;
    bool isPlayed = GameManager::instance()->getUserData()->isCoachmarkPlayed("selectChar");
    if(isPlayed){
        return;
    }
    
    // Start showing
    TutorialLayer *tutorialLayer = TutorialLayer::create();
    //	tutorialLayer->setColor(Color3B::BLACK);
    //	tutorialLayer->setOpacity(200);
    
    tutorialLayer->setContentSize(getContentSize());
    
    tutorialLayer->setLocalZOrder(10000);	// Very high
    tutorialLayer->setCloseCallback([=](Ref *sender){
        PlayerCharProfile* profile = PlayerManager::instance()->getPlayerCharProfile(1);
        mTutorialLayer = nullptr;
        CharacterInfoLayer* infoLayer = CharacterInfoLayer::create();
        infoLayer->setUpgradeBtnCallback([=](int charID){
            this->refreshItemView(charID);
            updateDiamond();
            updateStars();
        });
        infoLayer->setView(profile);
        addChild(infoLayer);
    });
    
    CharacterItemView* targetItemView = mItemViewMap.at(1);
    if(!targetItemView){
        //something gone wrong!
        return;
    }
    
    Vec2 btnSize = targetItemView->getCharTouchArea();
    Vec2 touchPos = targetItemView->getCharCenterPos();
    Vec2 csbPos = Vec2(touchPos.x + 20, touchPos.y + 8);
    tutorialLayer->setCoachmarkTutorial("selectChar",
                                        "tutorial/tutorial_point.csb",
                                        csbPos,touchPos,btnSize,-120);
    
    
    //
    tutorialLayer->reset();
    addChild(tutorialLayer);
    mTutorialLayer = tutorialLayer;
    
    mTutorialLayer->update(0);
}

void CharacterSelectionLayer::updateDiamond()
{
    int numOfDiamond = GameManager::instance()->getUserData()->getTotalDiamond();
    mDiamondText->setString(StringUtils::format("%d",numOfDiamond));
}

void CharacterSelectionLayer::updateStars()
{
    int numOfStars = GameManager::instance()->getUserData()->getTotalCoin();
    mStarText->setString(StringUtils::format("%d",numOfStars));
}

void CharacterSelectionLayer::setupFbProfile()
{
    if(AstrodogClient::instance()->isFBConnected()){
        AstrodogUser* user = AstrodogClient::instance()->getUser();
        mProfilePicFrame->setVisible(true);
        mFbName->setVisible(true);
        //mFbName->setString(user->getName());
		ViewHelper::setUnicodeText(mFbName, user->getName());
        float width = mFbName->getContentSize().width;
        if(width > 160){
            float scale = 0.5 * 160 / width;
            if(mFbName->getScale() > scale){
                mFbName->setScale(scale);
            }
            //            mFbName->setContentSize(Size(160,mFbName->getContentSize().height));
        }

        mProfilePic->setWithFBId(user->getFbID());
    }else{
 //       mFbConnectBtn->setVisible(true);
        mFbName->setVisible(false);
        mProfilePicFrame->setVisible(false);
    }
}

void CharacterSelectionLayer::setScrollView(Vector<PlayerCharProfile*> lockedList, Vector<PlayerCharProfile*> unlockedList)
{
    Text* name = (Text*) mScrollView->getChildByName("nameText");
    setPanel(lockedList, "lockedPanel");
    setPanel(unlockedList, "unlockedPanel");
    if(mContentSizeHeight + kPanelMarginY <= mScrollView->getContentSize().height){
        float offsetY = mScrollView->getContentSize().height - mContentSizeHeight - kPanelMarginY;
        mScrollView->setPositionY(mScrollView->getPositionY() + offsetY);
        
        mScrollView->setContentSize(Size(mScrollView->getContentSize().width, mContentSizeHeight + kPanelMarginY));
        mScrollView->setInnerContainerSize(Size(mScrollView->getInnerContainerSize().width, mContentSizeHeight + kPanelMarginY));
    }else{
        mScrollView->setInnerContainerSize(Size(mScrollView->getInnerContainerSize().width, mContentSizeHeight + kPanelMarginY + kPanelUpperMarginY));
    }
    name->setPositionY(mContentSizeHeight + kPanelMarginY + 30);
}

void CharacterSelectionLayer::refreshItemView(int charID)
{
    if(mItemViewMap.find(charID) == mItemViewMap.end()){
        return;
    }
    
    CharacterItemView* itemView = mItemViewMap.at(charID);
    PlayerCharProfile* profile = PlayerManager::instance()->getPlayerCharProfile(charID);
    itemView->setView(profile);
}

void CharacterSelectionLayer::setPanel(Vector<PlayerCharProfile*> charList,std::string panelName)
{
    Node* panel = mScrollView->getChildByName(panelName);
    float itemHeight ;
    if(panelName == "unlockedPanel"){
        panel->setPositionY(mContentSizeHeight);
        itemHeight = kUnlockedItemViewHeight;
    }else{
        itemHeight = kLockedItemViewHeight;
    }
    
    if(charList.empty()){
        panel->setContentSize(Size::ZERO);
        panel->setVisible(false);
        return;
    }
    
    const float upperMarginY = 30;
    const float lowerMarginY = panelName == "unlockedPanel" ? 15 : 45;
    const float marginY = panelName == "unlockedPanel" ? 20 : 25;
    const float innerMarginX = 7.0;
    const float outerMarginX = 12.0;
   
    
    float listSize = (int) charList.size();
    int numOfRow = ceil(listSize/3.0);
    float height = itemHeight * numOfRow + marginY * (numOfRow  - 1) + upperMarginY + lowerMarginY;
    panel->setContentSize(Size(panel->getContentSize().width,height));
    ui::Helper::doLayout(panel);
    
    float posX = outerMarginX , posY = height - upperMarginY - itemHeight;
    Node* container = panel->getChildByName("itemViewContainer");
    
    for(int i=0;i<charList.size();i++){
        PlayerCharProfile* profile = charList.at(i);
        CharacterItemView* itemView = CharacterItemView::create();
        itemView->setView(profile);
        itemView->setPosition(posX, posY);
        
        itemView->setUnlockCallback([=](int charID, Widget::TouchEventType type){
            switch (type) {
                case Widget::TouchEventType::BEGAN:
                    mOriScrollViewPositionY = mScrollView->getInnerContainerPosition().y;
                    break;
                    
                case Widget::TouchEventType::ENDED:{
                    float currentScrollViewPosY = mScrollView->getInnerContainerPosition().y;
                    if(std::abs(currentScrollViewPosY - mOriScrollViewPositionY) > 5){
                        return;
                    }
                   
                    Vec2 charPos = itemView->getCharIconWorldPos();
                    Sprite* charSprite = Sprite::create(StringUtils::format("playChar/player_icon_%03d.png",charID));
                    charSprite->setScale(0.5);
                    charSprite->setAnchorPoint(Vec2(0.5,0.5));
                    charSprite->setPosition(charPos);
                    mRootNode->addChild(charSprite);
                    
                    mCoverPanel->setVisible(true);
                    handleUnlockNewChar(charID,charSprite);

                    
                    break;
                }
                    
                default:
                    break;
            }
        });

        
        itemView->setTouchCallback([=](int charID, Widget::TouchEventType type){
            switch (type) {
                case Widget::TouchEventType::BEGAN:
                    mOriScrollViewPositionY = mScrollView->getInnerContainerPosition().y;
                    break;
                    
                case Widget::TouchEventType::ENDED:{
                    float currentScrollViewPosY = mScrollView->getInnerContainerPosition().y;
                    if(std::abs(currentScrollViewPosY - mOriScrollViewPositionY) > 5){
                        return;
                    }
                    CharacterInfoLayer* infoLayer = CharacterInfoLayer::create();
                    infoLayer->setOnExitCallback([=](int charID){
                        this->refreshItemView(charID);
                        updateDiamond();
                        updateStars();
                    });
                    infoLayer->setView(profile);
                    addChild(infoLayer);
                    break;
                }

                default:
                    break;
            }
        });
        
        itemView->setBtnCallback([=](int charID){
            int oldSelectedID = PlayerManager::instance()->getSelectedChar();
            PlayerManager::instance()->selectChar(charID);
            
            this->refreshItemView(oldSelectedID);
            this->refreshItemView(charID);
        });
        
        
        mItemViewMap.insert(profile->getCharID(), itemView);

       
        container->addChild(itemView);
        
        posX += 94 + innerMarginX;
        
        if((i+1)%3==0){
            posY -= itemHeight + marginY;
            posX = outerMarginX;
        }
    }
    
    mContentSizeHeight += panel->getContentSize().height;
    
}


void CharacterSelectionLayer::handleUnlockNewChar(int charID,  Sprite* charIcon)
{
    PlayerCharProfile::UpgradeStatus status = PlayerManager::instance()->unlock(charID);
    if(status != PlayerCharProfile::UpgradeOk){
        //someth gone wrong
        return;
    }
    
    Vec2 targetPos = Vec2(getContentSize().width/2,getContentSize().height/2);
    MoveTo* moveTo = MoveTo::create(0.3, targetPos);
    CallFunc* callFunc = CallFunc::create([=](){
        GameSound::playSound(GameSound::UnlockChar);
        AnimeNode* animeNode = AnimeNode::create();
        animeNode->setup("gui_character_unlock.csb");
        animeNode->setStartAnime("active",false,true);
        animeNode->setFrameEventCallback([=](EventFrame* frame){
            if(frame->getEvent() == "fade"){
                charIcon->removeFromParent();
                this->resetView();
                mCoverPanel->setVisible(false);
                mScrollView->jumpToTop();
                mUnlockSprite->setVisible(false);
            }
        });
        animeNode->setPosition(getContentSize().width/2,getContentSize().height/2);
        mRootNode->addChild(animeNode);
    });
    
    mUnlockSprite->setVisible(true);
    charIcon->runAction(Sequence::create(moveTo, callFunc, NULL));
    
/*
    std::queue<int> charQueue;
    charQueue.push(charID);

    UnlockDogLayer* layer = UnlockDogLayer::create();
    
    layer->setDogQueue(charQueue);
    
    layer->setCloseCallback([=] () {
        resetView();

        GameSound::stopSound(GameSound::Unlock);
    });
    
    addChild(layer);
    
    GameSound::playSound(GameSound::Unlock);
 */
    
}

void CharacterSelectionLayer::setCloseCallback(std::function<void()> callback)
{
    mCloseCallback = callback;
}


void CharacterSelectionLayer::onFBConnect(int status)
{
    if(status != STATUS_SUCCESS){
        return;
    }
    
    setupFbProfile();
}
