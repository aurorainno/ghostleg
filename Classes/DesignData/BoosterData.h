//
//  BoosterData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 19/1/2017.
//
//

#ifndef BoosterData_hpp
#define BoosterData_hpp

#include <stdio.h>


#include <stdio.h>
#include <string>
#include <map>
#include "cocos2d.h"

#include "CommonMacro.h"
#include "CommonType.h"
#include "Price.h"
#include "JSONData.h"
#include "ItemEffect.h"

USING_NS_CC;

class BoosterData : public JSONData, public Ref
{
public:		// static
	static int parseBoosterType(const std::string &keyName);
	
public:
	CREATE_FUNC(BoosterData);
	
public:			// properties
	CC_SYNTHESIZE(BoosterItemType, mType, Type);
	CC_SYNTHESIZE(std::string, mName, Name);				// template of the description
	CC_SYNTHESIZE(std::string, mInfo, Info);				// template of the description
	CC_SYNTHESIZE(Price, mPrice, Price);
	CC_SYNTHESIZE(GameProperties, mAttribute, Attribute);	// Attribute is the value of the Booster
	CC_SYNTHESIZE(float, mCooldown, Cooldown);
    CC_SYNTHESIZE(bool, mIsInstantActive, IsInstantActive);
	
public:
	BoosterData();
	
	virtual bool init();
	
	void setAttribute(const std::string &name, float value);
	
	std::string getDescription();
	
	std::string toString();
	
	void setItemEffectAttribute(ItemEffect *effect);

public: // JSON Data
	void defineJSON(rapidjson::Document::AllocatorType &allocator,
					rapidjson::Value &outValue);
	bool parseJSON(const rapidjson::Value &jsonValue);
	
	
};


#endif /* BoosterData_hpp */
