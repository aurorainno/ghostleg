//
//  NPCChanceTable.hpp
//  GhostLeg
//
//  Created by Ken Lee on 30/3/2017.
//
//

#ifndef NPCChanceTable_hpp
#define NPCChanceTable_hpp

#include <stdio.h>


#include "JSONData.h"

USING_NS_CC;

#pragma mark - NPCChance
class NPCChance
{
public:
	int npcID;
	int probablity;

public:
	NPCChance();
	NPCChance(int _npcID, int _prob);
	
public:
	std::string toString();
};


#pragma mark - NPCChanceTable

class NPCChanceTable : public JSONData, public Ref
{
public:
	CREATE_FUNC(NPCChanceTable);
	NPCChanceTable();
	virtual bool init() { return true; }
	
	std::vector<int> rollNpc(int rotationIndex);
	int rollOneNpc(int rotationIndex);
	int getRotationCount();		// number of rotation in the chance
	
	void setupMockData();
	
	std::string toString();
	
	

private:
	std::vector<std::vector<NPCChance>> mChanceTable;
	
#pragma mark - JSON
public: // JSON Data
	void defineJSON(rapidjson::Document::AllocatorType &allocator,
					rapidjson::Value &outValue);
	bool parseJSON(const rapidjson::Value &jsonValue);
	
};

#endif /* NPCChanceTable_hpp */
