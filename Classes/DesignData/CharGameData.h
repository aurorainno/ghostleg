//
//  CharGameData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#ifndef CharGameData_hpp
#define CharGameData_hpp

#include <stdio.h>


#include "cocos2d.h"
#include <vector>
#include <map>
#include <string>
#include "CommonType.h"
#include "PlayerAbility.h"
#include "JSONData.h"
#include "CharUpgradeCost.h"
#include "CommonMacro.h"

USING_NS_CC;

class CharGameData : public Ref, public JSONData
{
public:
	CREATE_FUNC(CharGameData);
	CharGameData();
	virtual bool init() { return true; }
	
#pragma mark - Basic Attribute
public: // data
	// General Information
	CC_SYNTHESIZE(int, mCharID, CharID);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mInfo, Info);
	CC_SYNTHESIZE(std::string, mUnlockInfo, UnlockInfo);
	CC_SYNTHESIZE(int, mBaseMaxSpeed, BaseMaxSpeed);				// 200 - 300
	CC_SYNTHESIZE(int, mBaseTurnSpeed, BaseTurnSpeed);				// 50 - 150
	CC_SYNTHESIZE(float, mBaseAcceleration, BaseAcceleration);		// 150 - 250
	CC_SYNTHESIZE(float, mBaseStunReduction, BaseStunReduction);		// 0.1 - 1.0  (1.5)
	SYNTHESIZE_BOOL(mEnable, Enable);
	
	// Visual Attribute
	CC_SYNTHESIZE(int, mIcon, Icon);				// For GUI, suppose same as charID
	CC_SYNTHESIZE(int, mAnimeID, AnimeID);			// Animation ID

	//CC_SYNTHESIZE(CharUpgradeCost, mUpgradeCost, UpgradeCost);
	
public:
	std::string toString();
	std::string infoAbilityList(const std::string &prefixStr = "  ");
	
private:
	std::vector<CharUpgradeCost> mUpgradeCostList;	// index=0 --> unlock cost

public:
	CharUpgradeCost getUpgradeCostAtLevel(int level);
	
#pragma mark - JSON Parse
public: // JSON
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value


	
#pragma mark - Ability List
public:
	Vector<PlayerAbility *> getAbilityList();
	void addAbility(PlayerAbility *ability);		// sequence of the add is important
	PlayerAbility *getAbilityAtLevel(int level);
	void parseAbilityList(const std::vector<std::string> &abilityList);
	void clearAbilityList();		// For testing and Debug
	
private:
	Vector<PlayerAbility *> mAbilityList;		// index of the list is the level (0 ~ 4) -> idx=0 = Lv1
};

#endif /* CharGameData_hpp */
