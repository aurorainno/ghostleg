//
//  PlanetData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#include "PlanetData.h"
#include "StringHelper.h"

PlanetData::PlanetData()
: mPlanetID(-1)
, mName("Star")
, mUnlockStatement("Get team to unlock")
, mDogList()
, mBannerImage("")
{
	
}

std::vector<int> &PlanetData::getDogList()
{
	return mDogList;
}

void PlanetData::setDogList(const std::vector<int> &list)
{
	mDogList.clear();
	for(int dogID : list) {
		mDogList.push_back(dogID);
	}
}

std::string PlanetData::toString()
{
	std::string result = "";
	
	result += StringUtils::format("planetID=%d", mPlanetID);
	result += " type=" + typeToString(mType);
	result += StringUtils::format(" name=%s", mName.c_str());
	result += StringUtils::format(" statement=%s", mUnlockStatement.c_str());
	result += StringUtils::format(" dog=%s", VECTOR_TO_STR(mDogList).c_str());
	
	return result;
}

std::string PlanetData::typeToString(PlanetType type)
{
	switch(type) {
		case PlanetData::Event			: return "event";
		case PlanetData::DailyReward	: return "dailyReward";
		case PlanetData::Normal			: return "normal";
		case PlanetData::Default		: return "default";
		default							: return "";
	}
}

PlanetData::PlanetType PlanetData::parseType(const std::string &typeStr)
{
	PlanetType typeList[4] = {Normal, Default, Event, DailyReward};
	for(int i=0; i<4; i++) {
		if(typeToString(typeList[i]) == typeStr) {
			return typeList[i];
		}
	}
	
	return PlanetData::Normal;
}


bool PlanetData::isTypeMatched(const PlanetType &checkType, const PlanetType &typeSet)
{
	return ((checkType & typeSet) != 0);
}
