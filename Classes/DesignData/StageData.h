//
//  StageData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#ifndef StageData_hpp
#define StageData_hpp

#include <stdio.h>


#include "cocos2d.h"

#include <stdio.h>
#include <vector>
#include <map>
#include <string>
#include "NPCOrderData.h"
#include "JSONData.h"
#include "NPCChanceTable.h"

USING_NS_CC;

class StageData : public JSONData, public Ref
{
public:
	CREATE_FUNC(StageData);
	StageData();
	virtual bool init() { return true; }
	
public:
	CC_SYNTHESIZE(int, mStageID, StageID);		// the stageID of this Stage Data
	CC_SYNTHESIZE(int, mStageBg, StageBg);
	CC_SYNTHESIZE(int, mClearStageScore, ClearStageScore);	// score add when stage clear
	
	void addNpcOrderData(NPCOrderData *order);
	void setNpcOrderDataList(const Vector<NPCOrderData *> &list);
	Vector<NPCOrderData *> &getNpcOrderDataList();
	std::vector<int> getGameMapList();
	std::vector<int> getNpcMapList();
	int getTotalOrderCount();
	
	
	std::string toString();
	
	std::string infoChanceTable();
	std::string infoPuzzleChanceTable();
	std::string infoChaserAttribute();
	
	int rollOneNpc(int orderServed);
	std::vector<int> rollNpc(int orderServed);
	
	const std::map<std::string, int> &getChaserAttribute();
	const std::map<int, int> &getPuzzleChanceTable();
	
	bool isLastOrder(int orderServed);

private:
	Vector<NPCOrderData *> mNpcOrderList;
	NPCChanceTable mChanceTable;
	std::map<int, int> mPuzzleChanceTable;		//
	std::map<std::string, int> mChaserAttribute;		//

#pragma mark - JSON
public: // JSON Data
	void defineJSON(rapidjson::Document::AllocatorType &allocator,
					rapidjson::Value &outValue);
	bool parseJSON(const rapidjson::Value &jsonValue);
	void parsePuzzleChanceTable(const rapidjson::Value &jsonValue);
	void parseChaserAttribute(const rapidjson::Value &jsonValue);
};

#endif /* StageData_hpp */
