//
//  DogData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 4/10/2016.
//
//

#include "DogData.h"
#include "PlayerAbilityFactory.h"

#pragma mark -
std::string DogData::getRankString(const DogRank &rank)
{
	switch (rank) {
		case DogData::RankClassic:	return "classic";
		case DogData::RankPremium:	return "premium";
	}
	
	return StringUtils::format("rank=%d", rank);
}

std::string DogData::getRarityString(const DogRarity &rarity)
{
	switch (rarity) {
		case DogData::RarityRare:		return "rare";
		case DogData::RaritySuperRare:	return "superRare";
	}
	
	return StringUtils::format("rarity=%d", rarity);
}


DogData::DogRank DogData::parseRank(const std::string &str)
{
	if(str == "premium") {
		return RankPremium;
	}
	
	return RankClassic;
}

DogData::DogRarity DogData::parseRarity(const std::string &str)
{
	if(str == "superRare") {
		return RaritySuperRare;
	}
	
	return RarityRare;
}

#pragma mark - Class Object

DogData::DogData()
: mDogID(0)
, mName("")
, mInfo("")
, mUnlockInfo("")
, mIcon(1)
, mAnimeID(0)
, mPrice(100000)
, mMoneyType(MoneyTypeStar)
, mSuit(0)
, mMasteryMap()
, mUnlockCondMap()
, mAbilityList()
{
	
}


int DogData::getSuitID()
{
	return (int) mSuit / 100;
}

int DogData::getSuitLevel()
{
	return (int) mSuit % 100;
}

//void DogData::setMasteryList(const std::vector<int> &data)	// value = masteryID * 10 + level
//{
//	mMasteryList.clear();
//	
//	for(int i=0; i<data.size(); i++) {
//		mMasteryList.push_back(data[i]);
//	}
//}
//
const std::map<int, int> DogData::getMasteryMap() const		// map<masteryID, masteryLevel>
{
	return mMasteryMap;
}

void DogData::setMasteryList(std::map<int, int> &data)	// value = masteryID * 10 + level
{
	log("debug: setMasteryList: dog=%d masteryMap is updated!", mDogID);
	mMasteryMap.clear();
	
	for (std::map<int, int>::iterator it=data.begin(); it!=data.end(); ++it) {
		int key = it->first;
		int value = it->second;
		
		mMasteryMap[key] = value;
	}
}


void DogData::parseMasteryValue(const int &value, int &masteryID, int &masteryLevel)
{
	masteryID = value / 100;
	masteryLevel = value % 100;
}


int DogData::getMasteryLevel(int masteryID)
{
	// note: this is important
	//		without this mMasterMap will become {1:1, 2:1, 3:1, ...}
	if(mMasteryMap.find(masteryID) == mMasteryMap.end()) {
		return 0;
	}
	
	int level = mMasteryMap[masteryID];
	
	return level <= 0 ? 1 : level;
}



// Unlock Condition
const std::map<std::string, int> &DogData::getUnlockCondList() const
{
	return mUnlockCondMap;
}

void DogData::setUnlockCondList(std::map<std::string, int> &data)
{
	mUnlockCondMap.clear();
	
	for (std::map<std::string, int>::iterator it=data.begin(); it!=data.end(); ++it) {
		std::string key = it->first;
		int value = it->second;

		mUnlockCondMap[key] = value;
	}
}

std::string DogData::toString()
{
	std::string info = "";
	
	
	info += StringUtils::format("dogID=%d", mDogID);
	info += StringUtils::format(" name=%s", mName.c_str());
	info += StringUtils::format(" icon=%d", mIcon);
	info += StringUtils::format(" animeID=%d", mAnimeID);
	info += StringUtils::format(" price=%d", mPrice);
	info += StringUtils::format(" moneyType=%d", mMoneyType);
	info += " rarity=" + getRarityString(getRarity());
	info += " rank=" + getRankString(getRank());
	
	info += StringUtils::format(" suit=%d/%d", getSuitID(), getSuitLevel());
	for(std::map<int, int>::iterator it=mMasteryMap.begin(); it != mMasteryMap.end(); it++) {
        int mastery = it->first;
        
		info += StringUtils::format(" mastery%d=%d", mastery, getMasteryLevel(mastery));
	}
	
	info += StringUtils::format(" info=%s", mInfo.c_str());
	info += StringUtils::format(" unlockInfo=%s", mUnlockInfo.c_str());
	
	info += "\n";
	
    for(std::map<std::string, int>::iterator it=mUnlockCondMap.begin(); it != mUnlockCondMap.end(); it++) {
        std::string name = it->first;
        int val = it->second;
        
        info += StringUtils::format(" %s: %d\n", name.c_str(), val);
    }
	
	return info;
}


#pragma mark - Ability List
Vector<PlayerAbility *> DogData::getAbilityList()
{
	Vector<PlayerAbility *> result;
	
	for(PlayerAbility *ability : mAbilityList) {
		result.pushBack(ability);
	}
	
	return result;
}
void DogData::addAbility(PlayerAbility *ability)
{
	mAbilityList.pushBack(ability);
}

PlayerAbility *DogData::getAbilityAtLevel(int level)
{
	int index = level - 1;
	if(index < 0 || index >= mAbilityList.size()) {
		return nullptr;
	}
	
	return mAbilityList.at(index);
}

std::string DogData::infoAbilityList(const std::string &prefixStr)
{
	std::string info = prefixStr;
	info += StringUtils::format("abilityCount: %ld\n", mAbilityList.size());
	
	for(PlayerAbility *ability : mAbilityList) {
		info += prefixStr + ability->toString() + "\n";
	}
	
	return info;
}


void DogData::parseAbilityList(const std::vector<std::string> &abilityList)
{
	mAbilityList.size();
	
	for(std::string abilityStr : abilityList) {
		
		PlayerAbility *ability = PlayerAbilityFactory::instance()->create(abilityStr);
		
		//log("DEBUG: abilityStr=%s ability=%s", abilityStr.c_str(), ability->toString().c_str());
		
		mAbilityList.pushBack(ability);
	}
	
}
