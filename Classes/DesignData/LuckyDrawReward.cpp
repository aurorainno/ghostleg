//
//  LuckyDrawReward.cpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#include "LuckyDrawReward.h"
#include "StringHelper.h"
#include "JSONHelper.h"
#include "ItemManager.h"		// to get the boosterName
#include "PlayerManager.h"		// to get the character name

LuckyDrawReward::LuckyDrawReward()
: mRangeStart(0)
, mRangeEnd(0)
{
	
}

std::string LuckyDrawReward::toString()
{
	std::string result = "";
	
//	CC_SYNTHESIZE(RewardType, mRewardType, RewardType);
//	CC_SYNTHESIZE(RarityType, mRarityType, RarityType);
//	CC_SYNTHESIZE(std::string, mItemName, ItemName);
//	CC_SYNTHESIZE(int, mItemID, ItemID);
//	CC_SYNTHESIZE(int, mQuantity, Quantity);
//	CC_SYNTHESIZE(int, mProbability, Probability);	/// note: base = 10000

	result += "type=" + rewardTypeToString(getRewardType());
	result += " itemName=" + getItemName();
	result += StringUtils::format(" quantity=%d", getQuantity());
	result += StringUtils::format(" probability=%d", getProbability());
	result += StringUtils::format(" itemID=%d", getItemID());
	result += " rarity=" + rarityTypeToString(getRarityType());
	result += StringUtils::format(" range=[%d %d]", getRangeStart(), getRangeEnd());
	
	return result;
}


std::string LuckyDrawReward::rewardTypeToString(RewardType type)
{
	switch(type) {
		case RewardNone			: return "none";
		case RewardStar			: return "star";
		case RewardFragment		: return "fragment";
		case RewardBooster		: return "booster";
		default					: return "";
	}
}

LuckyDrawReward::RewardType LuckyDrawReward::parseRewardType(const std::string &typeStr)
{
	RewardType typeList[4] = {RewardNone, RewardStar, RewardFragment, RewardBooster};
	for(int i=0; i<4; i++) {
		if(rewardTypeToString(typeList[i]) == typeStr) {
			return typeList[i];
		}
	}
	
	return LuckyDrawReward::RewardNone;
}


std::string LuckyDrawReward::rarityTypeToString(RarityType type)
{
	switch(type) {
		case RarityNone			: return "none";
		case RarityRare			: return "rare";
		case RaritySuperRare	: return "superRare";
		default					: return "";
	}
}

LuckyDrawReward::RarityType LuckyDrawReward::parseRarityType(const std::string &typeStr)
{
	RarityType typeList[3] = {RarityNone, RarityRare, RaritySuperRare};
	for(int i=0; i<3; i++) {
		if(rarityTypeToString(typeList[i]) == typeStr) {
			return typeList[i];
		}
	}
	
	return LuckyDrawReward::RarityNone;
}


#pragma mark - JSON Data
void LuckyDrawReward::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
	std::string typeStr = rewardTypeToString(getRewardType());
	std::string rarityStr = rarityTypeToString(getRarityType());
	
	addInt(allocator, "itemID", getItemID(), outValue);
	addInt(allocator, "quantity", getQuantity(), outValue);
	addInt(allocator, "probability", getProbability(), outValue);
	addString(allocator, "type", typeStr, outValue);
	addString(allocator, "rarity", rarityStr, outValue);
	addString(allocator, "itemName", getItemName(), outValue);
}

bool LuckyDrawReward::parseJSON(const rapidjson::Value &json)		// JSON DAta -> this object value
{
	std::string stringValue;
	int intValue;
	
	
	stringValue = aurora::JSONHelper::getString(json, "type", "star");
	RewardType rewardType = parseRewardType(stringValue);
	setRewardType(rewardType);
	
	stringValue = aurora::JSONHelper::getString(json, "rarity", "none");
	RarityType rarity = parseRarityType(stringValue);
	setRarityType(rarity);
	
	
	intValue = aurora::JSONHelper::getInt(json, "quantity", 1);
	setQuantity(intValue);
    
    intValue = aurora::JSONHelper::getInt(json, "freeQuantity", 1);
    setFreeQuantity(intValue);
	
	intValue = aurora::JSONHelper::getInt(json, "probability", 1);
	setProbability(intValue);
	
	intValue = aurora::JSONHelper::getInt(json, "itemID", 0);
	setItemID(intValue);
	
	
	if(rewardType == RewardBooster) {
		stringValue = ItemManager::instance()->getBoosterName((BoosterItemType) getItemID());
	} else if(rewardType == RewardFragment) {
		stringValue = PlayerManager::instance()->getCharName(getItemID());
	} else {
		stringValue = aurora::JSONHelper::getString(json, "itemName", "unknown");		
	}
	setItemName(stringValue);
	
	
	
	return true;
}
