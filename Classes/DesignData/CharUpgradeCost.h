//
//  CharUpgradeCost.hpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#ifndef CharUpgradeCost_hpp
#define CharUpgradeCost_hpp

#include <stdio.h>


#include <stdio.h>


#include "JSONData.h"

USING_NS_CC;

#pragma mark - NPCChance
class CharUpgradeCost : public JSONData, public Ref
{
public:
	int requireFragCount;
	int requireMoney;
	
public:
	CharUpgradeCost();
	CharUpgradeCost(int _count, int _money);
	
public:
	std::string toString();
	
#pragma mark - JSON
public: // JSON Data
	void defineJSON(rapidjson::Document::AllocatorType &allocator,
					rapidjson::Value &outValue);
	bool parseJSON(const rapidjson::Value &jsonValue);
	
};


#endif /* CharUpgradeCost_hpp */
