//
//  NPCChanceTable.cpp
//  GhostLeg
//
//  Created by Ken Lee on 30/3/2017.
//
//

#include "NPCChanceTable.h"
#include "StringHelper.h"
#include "RandomHelper.h"

#pragma mark - NPCChance
NPCChance::NPCChance(int _npcID, int _prob)
: npcID(_npcID)
, probablity(_prob)
{
	
}

NPCChance::NPCChance()
: npcID(0)
, probablity(0)
{
	
}


std::string NPCChance::toString()
{
	return StringUtils::format("%d=%d", npcID, probablity);
}

#pragma mark - NPCChanceTable

NPCChanceTable::NPCChanceTable()
: JSONData()
, Ref()
, mChanceTable()
{
	
}

void NPCChanceTable::setupMockData()
{
	mChanceTable.clear();
	
	std::vector<NPCChance> chanceTable;
	
	chanceTable.push_back(NPCChance(101, 40));
	chanceTable.push_back(NPCChance(102, 20));	// 60
	chanceTable.push_back(NPCChance(103, 15));	// 75
	chanceTable.push_back(NPCChance(104, 15));  // 90
	chanceTable.push_back(NPCChance(105,  5));	// 95
	chanceTable.push_back(NPCChance(106,  5));	// 100
	
	mChanceTable.push_back(chanceTable);
}

// Logic:
//		 data = map<npcID, range>
//

int NPCChanceTable::rollOneNpc(int rotationIndex)
{
	if(rotationIndex >= mChanceTable.size()) {
		return 0;
	}
	
	// Convert the chanceTable to map<int, int>
	std::vector<NPCChance> &table = mChanceTable[rotationIndex];
	
	std::map<int, int> chanceMap;
	for(NPCChance chance : table) {
		chanceMap[chance.npcID] = chance.probablity;
	}
	
	//
	return aurora::RandomHelper::findRandomValue(chanceMap);
}

std::vector<int> NPCChanceTable::rollNpc(int rotationIndex)
{
	// Convert the chanceTable to map<int, int>
	std::vector<NPCChance> &table = mChanceTable[rotationIndex];
	
	std::map<int, int> chanceMap;
	for(NPCChance chance : table) {
		chanceMap[chance.npcID] = chance.probablity;
	}
	
	//
	return aurora::RandomHelper::findMultiRandomValue(chanceMap, 2);
}

int NPCChanceTable::getRotationCount()
{
	return (int) mChanceTable.size();
}

std::string NPCChanceTable::toString()
{
	std::string result = StringUtils::format("rotationCount: %d\n", getRotationCount());
	
	int rotationIdx = 0;
	for(std::vector<NPCChance> table : mChanceTable) {
		
		result += StringUtils::format("Rotation %d:\n", rotationIdx);
		
		for(NPCChance chance : table) {
			result += "[" + chance.toString() + "] ";
		}
		
		result += "\n";
		
		
		rotationIdx++;
	}
	
	return result;
}

#pragma mark - JSON
void NPCChanceTable::defineJSON(rapidjson::Document::AllocatorType &allocator,
				rapidjson::Value &outValue)
{
	// No need, this is ready only
}

bool NPCChanceTable::parseJSON(const rapidjson::Value &jsonValue)
{
	
	if(jsonValue.IsArray() == false) {
		log("NPCChanceTable.parseJSON: json isn't an array!");
		return false;
	}
	
	// log("size=%d", jsonValue.Size());
	
	mChanceTable.clear();
	
	for (rapidjson::SizeType i = 0; i < jsonValue.Size(); i++)
	{
		
		// const rapidjson::Value oneRoundTable = jsonValue[i];
		
		// log("debug: round: %d", i);
		
		const rapidjson::Value &jsonElement = jsonValue[i];

		
		std::vector<NPCChance> npcTable;
		
		
		for (rapidjson::Value::ConstMemberIterator itr = jsonElement.MemberBegin();
						 itr != jsonElement.MemberEnd();
						 ++itr)
		{
			std::string name = itr->name.GetString();
			const rapidjson::Value &json = jsonElement[name.c_str()];
			int value = json.GetInt();
			 
			// log("name=%s value=%d", name.c_str(), value);
			
			int npcID = STR_TO_INT(name);
			int percentage = value;
			
			NPCChance chance(npcID, percentage);
			
			npcTable.push_back(chance);
		}
		
		mChanceTable.push_back(npcTable);
			
			//log("debug: element: %d str=%s", k, str.c_str());
		
		
//		std::string str(arrayValue[i].GetString());
//		outArray.push_back(str);
	}
//	
//	
//	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
//		 itr != jsonValue.MemberEnd();
//		 ++itr)
//	{
//		
//
//		std::string name = itr->name.GetString();
//		
//		int npcOrderID = aurora::StringHelper::parseInt(name);
//		
//		log("DEBUG:parseJSON: name=%s", name.c_str());
//		
//		const rapidjson::Value &npcJSON = npcListJSON[name.c_str()];
//		NPCOrderData *npcOrder = NPCOrderData::create();
//		npcOrder->setOrderID(npcOrderID);
//		npcOrder->parseJSON(npcJSON);
//		
//		addNpcOrderData(npcOrder);
//	}

}
