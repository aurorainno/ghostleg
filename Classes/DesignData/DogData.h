//
//  DogData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 4/10/2016.
//
//

#ifndef DogData_hpp
#define DogData_hpp

#include <stdio.h>
#include "cocos2d.h"
#include <vector>
#include <map>
#include <string>
#include "CommonType.h"
#include "PlayerAbility.h"

USING_NS_CC;

class DogData : public Ref
{
public:
	enum DogRank {
		RankPremium,
		RankClassic,
	};
	
	enum DogRarity {
		RarityRare,
		RaritySuperRare,
	};
	
public: // static
	static std::string getRankString(const DogRank &rank);
	static std::string getRarityString(const DogRarity &rarity);
	static DogRank parseRank(const std::string &str);
	static DogRarity parseRarity(const std::string &str);
	
public:
	CREATE_FUNC(DogData);
	DogData();
	virtual bool init() { return true; }
	
public: // data
	// General Information
	CC_SYNTHESIZE(int, mDogID, DogID);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mInfo, Info);
	CC_SYNTHESIZE(std::string, mUnlockInfo, UnlockInfo);
	CC_SYNTHESIZE(int, mBaseMaxSpeed, BaseMaxSpeed);
	CC_SYNTHESIZE(float, mBaseAcceleration, BaseAcceleration);
	CC_SYNTHESIZE(float, mBaseStunDuration, BasemStunDuration);
	
	// Visual Attribute
	CC_SYNTHESIZE(int, mIcon, Icon);
	CC_SYNTHESIZE(int, mAnimeID, AnimeID);
	
	// Pricing
	CC_SYNTHESIZE(int, mPrice, Price);		// using Star
	CC_SYNTHESIZE(MoneyType, mMoneyType, MoneyType);	// Type of the price

	// Attribute
	CC_SYNTHESIZE(DogRank, mRank, Rank);
	CC_SYNTHESIZE(DogRarity, mRarity, Rarity);
	
	// Ability (Suit & Mastery)
	CC_SYNTHESIZE(int, mSuit, Suit);		// value = suitID * 10 + level
	int getSuitID();
	int getSuitLevel();
	
	
	const std::map<int, int> getMasteryMap() const;		// map<masteryID, masteryLevel>
	
	void setMasteryList(std::map<int, int> &data);	// value = masteryID * 10 + level
	void parseMasteryValue(const int &value, int &masteryID, int &masteryLevel);
	int getMasteryLevel(int masteryID);
	
	// Unlock Condition
	const std::map<std::string, int> &getUnlockCondList() const;
	void setUnlockCondList(std::map<std::string, int> &data);
	
	
	
public:
	std::string toString();
	
private:
	std::map<int, int> mMasteryMap;
	std::map<std::string, int> mUnlockCondMap;

#pragma mark - Basic Stat
public:
	
	
#pragma mark - Ability List 
public:
	Vector<PlayerAbility *> getAbilityList();
	void addAbility(PlayerAbility *ability);		// sequence of the add is important
	PlayerAbility *getAbilityAtLevel(int level);
	void parseAbilityList(const std::vector<std::string> &abilityList);
	
	std::string infoAbilityList(const std::string &prefixStr="");
	
	

private:
	Vector<PlayerAbility *> mAbilityList;		// index of the list is the level (0 ~ 4) -> idx=0 = Lv1
};

#endif /* DogData_hpp */
 
