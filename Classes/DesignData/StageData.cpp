//
//  StageData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2017.
//
//

#include "StageData.h"
#include "JSONHelper.h"
#include "StringHelper.h"

StageData::StageData()
: mStageID(0)
, mStageBg(0)
, mNpcOrderList()
, mPuzzleChanceTable()
, mChaserAttribute()
, mClearStageScore(1000)
{
	
}

void StageData::setNpcOrderDataList(const Vector<NPCOrderData *> &list)
{
	mNpcOrderList.clear();
	for(NPCOrderData *order : list) {
		mNpcOrderList.pushBack(order);
	}
	
}

int StageData::getTotalOrderCount()
{
	return mChanceTable.getRotationCount();
}

void StageData::addNpcOrderData(NPCOrderData *order)
{
	mNpcOrderList.pushBack(order);
}

Vector<NPCOrderData *> &StageData::getNpcOrderDataList()
{
	return mNpcOrderList;
}

std::string StageData::infoPuzzleChanceTable()
{
	
	return "puzzleChance=" + aurora::StringHelper::mapToString(mPuzzleChanceTable);
	
}

std::string StageData::toString()
{
	std::string result = "";
	
	result = StringUtils::format("stageID=%d npcCount=%ld\n", getStageID(), mNpcOrderList.size());
	
	for(NPCOrderData *order : mNpcOrderList) {
		result += order->toString() + "\n";
		result += "   " + order->infoMap();
		result += "\n";
	}
	result += infoChanceTable();
	result += "\n" + infoPuzzleChanceTable();
	result += "\n" + infoChaserAttribute();
	
	
	return result;
}


#pragma mark - JSON
void StageData::defineJSON(rapidjson::Document::AllocatorType &allocator,
						  rapidjson::Value &outValue)
{
	// No need to save
}


void StageData::parsePuzzleChanceTable(const rapidjson::Value &jsonValue)
{
	
	mPuzzleChanceTable.clear();
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr)
	{
		std::string name = itr->name.GetString();
		
		int characterID = aurora::StringHelper::parseInt(name);
		int value = aurora::JSONHelper::getInt(jsonValue, name.c_str(), 0);
		
		
		if(value == 0) {
			continue;
		}
		
		mPuzzleChanceTable[characterID] = value;
	}

}

bool StageData::parseJSON(const rapidjson::Value &jsonValue)
{
	int stageID = aurora::JSONHelper::getInt(jsonValue, "stageID", 1);
	setStageID(stageID);

	int bg = aurora::JSONHelper::getInt(jsonValue, "stageBg", stageID);
	setStageBg(bg);
	
	mNpcOrderList.clear();
	// parsing the npc order
	const rapidjson::Value &npcListJSON = jsonValue["npcList"];

	for (rapidjson::Value::ConstMemberIterator itr = npcListJSON.MemberBegin();
		 itr != npcListJSON.MemberEnd();
		 ++itr)
	{
		std::string name = itr->name.GetString();
		
		int npcOrderID = aurora::StringHelper::parseInt(name);
		
		// log("DEBUG:parseJSON: name=%s", name.c_str());

		const rapidjson::Value &npcJSON = npcListJSON[name.c_str()];
		NPCOrderData *npcOrder = NPCOrderData::create();
		npcOrder->setOrderID(npcOrderID);
		npcOrder->parseJSON(npcJSON);
		
		addNpcOrderData(npcOrder);
	}
	
	// Parse ChanceTable
	if(jsonValue.HasMember("chanceTable")) {
		const rapidjson::Value &chanceTableJSON = jsonValue["chanceTable"];
		mChanceTable.parseJSON(chanceTableJSON);
	}
	
	// Parse the PuzzleChanceTable
	if(jsonValue.HasMember("chanceTable")) {
		const rapidjson::Value &chanceTableJSON = jsonValue["puzzleChanceTable"];
		parsePuzzleChanceTable(chanceTableJSON);
	}

	// Parse the PuzzleChanceTable
	if(jsonValue.HasMember("chaserAttribute")) {
		const rapidjson::Value &json = jsonValue["chaserAttribute"];
		parseChaserAttribute(json);
	}

	
	////
	
	return true;
}

std::vector<int> StageData::getGameMapList()
{
	std::vector<int> mapList;
	
	for(NPCOrderData *order : mNpcOrderList) {
		std::vector<int> gameMaps = order->getRequireMapList();
		//log("DEBUG: StageData.npcOrder: mapList=%s", VECTOR_TO_STR(mapList).c_str());
		for(int map : gameMaps) {
			mapList.push_back(map);
		}
	}
	return mapList;
}

std::vector<int> StageData::getNpcMapList()
{
	std::vector<int> mapList;

	for(NPCOrderData *order : mNpcOrderList) {
		mapList.push_back(order->getNpcMap());
	}
	return mapList;
}


std::string StageData::infoChanceTable()
{
	return mChanceTable.toString();
}

int StageData::rollOneNpc(int orderServed)
{
	return mChanceTable.rollOneNpc(orderServed);
	
}


std::vector<int> StageData::rollNpc(int orderServed)
{
	int rotationIndex = orderServed % mChanceTable.getRotationCount();
	
	
	return mChanceTable.rollNpc(rotationIndex);
	
}

bool StageData::isLastOrder(int orderServed)
{
	return (mChanceTable.getRotationCount() - orderServed) <= 1;
}


const std::map<int, int> &StageData::getPuzzleChanceTable()
{
	return mPuzzleChanceTable;
}


void StageData::parseChaserAttribute(const rapidjson::Value &jsonValue)
{
	mChaserAttribute.clear();
	
	for (rapidjson::Value::ConstMemberIterator itr = jsonValue.MemberBegin();
		 itr != jsonValue.MemberEnd();
		 ++itr)
	{
		std::string name = itr->name.GetString();
		int value = aurora::JSONHelper::getInt(jsonValue, name.c_str(), 0);

		mChaserAttribute[name] = value;
	}

}

const std::map<std::string, int> &StageData::getChaserAttribute()
{
	return mChaserAttribute;
}

std::string StageData::infoChaserAttribute()
{
	return "chaserAttribute=" + aurora::StringHelper::mapToString(mChaserAttribute);
}
