//
//  CharGameData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#include "CharGameData.h"
#include "PlayerAbilityFactory.h"
#include "JSONHelper.h"

CharGameData::CharGameData()
: mCharID(1)
, mName("")
, mInfo("")
, mUnlockInfo("")
, mBaseMaxSpeed(100)
, mBaseTurnSpeed(100)
, mBaseAcceleration(100)
, mBaseStunReduction(0.5)
, mIcon(1)
, mAnimeID(1)
, mAbilityList()
, mEnable(true)
, mUpgradeCostList()
{
	
}



//std::string CharGameData::infoAbilityList(const std::string &prefixStr)
//{
//	std::string info = "";
//	
//	for(PlayerAbility *ability : mAbilityList) {
//		info +=  prefixStr  + ability->toString() + "\n";
//	}
//	
//	return info;
//}

std::string CharGameData::toString()
{
	std::string info = "";
	
	
	info += StringUtils::format("charID=%d", mCharID);
	info += StringUtils::format(" name=%s", mName.c_str());
	info += StringUtils::format(" icon=%d", mIcon);
	info += StringUtils::format(" animeID=%d", mAnimeID);
	info += StringUtils::format(" maxSpeed=%d", mBaseMaxSpeed);
	info += StringUtils::format(" turnSpeed=%d", mBaseTurnSpeed);
	info += StringUtils::format(" acceleration=%f", mBaseAcceleration);
	info += StringUtils::format(" stunReduction=%f", mBaseStunReduction);
	info += "\n";
	info += "Upgrade Cost:\n";
	for(int i=0; i<mUpgradeCostList.size(); i++) {
		info += StringUtils::format("   level-%d: %s\n", (i+1), mUpgradeCostList[i].toString().c_str());
	}
	info += "Ability:\n";
	for(PlayerAbility *ability : mAbilityList) {
		info +=  "   " + ability->toString() + "\n";
	}
	
	return info;
}


#pragma mark - Ability List
void CharGameData::clearAbilityList()		// For testing and Debug
{
	mAbilityList.clear();
}

Vector<PlayerAbility *> CharGameData::getAbilityList()
{
	Vector<PlayerAbility *> result;
	
	for(PlayerAbility *ability : mAbilityList) {
		result.pushBack(ability);
	}
	
	return result;
}
void CharGameData::addAbility(PlayerAbility *ability)
{
	mAbilityList.pushBack(ability);
}

PlayerAbility *CharGameData::getAbilityAtLevel(int level)
{
	int index = level - 1;
	if(index < 0 || index >= mAbilityList.size()) {
		return nullptr;
	}
	
	return mAbilityList.at(index);
}

std::string CharGameData::infoAbilityList(const std::string &prefixStr)
{
	std::string info = prefixStr;
	info += StringUtils::format("abilityCount: %ld\n", mAbilityList.size());
	
	for(PlayerAbility *ability : mAbilityList) {
		info += prefixStr;
		info += ability->getDesc();
		info += "  | ";
		info += ability->toString();
		info += "\n";
	}
	
	return info;
}


void CharGameData::parseAbilityList(const std::vector<std::string> &abilityList)
{
	mAbilityList.size();
	
	for(std::string abilityStr : abilityList) {
		
		PlayerAbility *ability = PlayerAbilityFactory::instance()->create(abilityStr);
		
		//log("DEBUG: abilityStr=%s ability=%s", abilityStr.c_str(), ability->toString().c_str());
		
		mAbilityList.pushBack(ability);
	}
	
}

#pragma mark - JSON Parse
bool CharGameData::parseJSON(const rapidjson::Value &jsonValue)
{
//	// General Information
//	CC_SYNTHESIZE(int, mCharID, CharID);
//	CC_SYNTHESIZE(std::string, mName, Name);
//	CC_SYNTHESIZE(std::string, mInfo, Info);
//	CC_SYNTHESIZE(std::string, mUnlockInfo, UnlockInfo);
//	CC_SYNTHESIZE(int, mBaseMaxSpeed, BaseMaxSpeed);				// 200 - 300
//	CC_SYNTHESIZE(int, mBaseTurnSpeed, BaseTurnSpeed);				// 50 - 150
//	CC_SYNTHESIZE(float, mBaseAcceleration, BaseAcceleration);		// 150 - 250
//	CC_SYNTHESIZE(float, mBaseStunDuration, BaseStunDuration);		// 0.1 - 1.0  (1.5)
//	
//	// Visual Attribute
//	CC_SYNTHESIZE(int, mIcon, Icon);				// For GUI, suppose same as charID
//	CC_SYNTHESIZE(int, mAnimeID, AnimeID);			// Animation ID
//
	mCharID = aurora::JSONHelper::getInt(jsonValue, "charID", 0);
    mName = aurora::JSONHelper::getString(jsonValue, "name", "");
	mBaseMaxSpeed = aurora::JSONHelper::getInt(jsonValue, "maxSpeed", 10);
	mBaseTurnSpeed = aurora::JSONHelper::getInt(jsonValue, "turnSpeed", 10);
	mBaseAcceleration = aurora::JSONHelper::getFloat(jsonValue, "acceleration", 100.0f);
	mBaseStunReduction = aurora::JSONHelper::getFloat(jsonValue, "stunReduction", 0.0f);
    mUnlockInfo = aurora::JSONHelper::getString(jsonValue, "unlockInfo", "");
	mIcon = aurora::JSONHelper::getInt(jsonValue, "icon", mCharID);
	mAnimeID = aurora::JSONHelper::getInt(jsonValue, "animeID", mCharID);
	mEnable = aurora::JSONHelper::getBool(jsonValue, "enable", true);
	
	//mUpgradeCost.parseJSON(jsonValue["upgradeCost"]);
	
	// UpgradeCost
	mUpgradeCostList.clear();
	const rapidjson::Value &upgradeJSON = jsonValue["upgradeCost"];
	for(int i=0; i<upgradeJSON.Size(); i++) {
		CharUpgradeCost cost;
		cost.parseJSON(upgradeJSON[i]);
		
		mUpgradeCostList.push_back(cost);
	}
	
	// Ability
	std::vector<std::string> abilityStrList;
	aurora::JSONHelper::getJsonStrArray(jsonValue, "abilityList", abilityStrList);
	parseAbilityList(abilityStrList);
	
	
	return true;
}

CharUpgradeCost CharGameData::getUpgradeCostAtLevel(int level)
{
	if(level <= 0) {
		level = 1;
	}
	
	if(level > mUpgradeCostList.size()) {
		return CharUpgradeCost(0, 0);
	}
	
	int idx = level - 1;
	
	return mUpgradeCostList[idx];
}
