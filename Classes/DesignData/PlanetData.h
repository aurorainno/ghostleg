//
//  PlanetData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 11/10/2016.
//
//

#ifndef PlanetData_hpp
#define PlanetData_hpp

#include "cocos2d.h"

#include <stdio.h>
#include <vector>
#include <map>
#include <string>

USING_NS_CC;

class PlanetData : public Ref
{
public:
	// General Information
	enum PlanetType{			// Define the value using Bitwise
		Default			= 1 << 0,		// Earth
		Normal			= 1 << 1,		// Planet
		Event			= 1 << 2,		// Event, e.g Christmas
		DailyReward		= 1 << 3,		// DailyReward
	};

public:
	static std::string typeToString(PlanetType type);
	static PlanetType parseType(const std::string &typeStr);
	static bool isTypeMatched(const PlanetType &checkType, const PlanetType &typeSet);
	
public:
	CREATE_FUNC(PlanetData);
	PlanetData();
	virtual bool init() { return true; }
	
public: // data
	
	CC_SYNTHESIZE(int, mPlanetID, PlanetID);
    CC_SYNTHESIZE(int, mPrice, Price);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mUnlockStatement, mUnlockStatement);
    CC_SYNTHESIZE(std::string, mFirstBg, FirstBg);
	CC_SYNTHESIZE(std::string, mSecondBg, SecondBg);
	CC_SYNTHESIZE(std::string, mBannerImage, BannerImage);		// the Banner image in Dog Selection
    CC_SYNTHESIZE(PlanetType, mType, PlanetType);
    
	void setDogList(const std::vector<int> &list);
	std::vector<int> &getDogList();
	
public:
	std::string toString();
	
private:
	std::vector<int> mDogList;
};

#endif /* PlanetData_hpp */
