//
//  LuckyDrawReward.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/2/2017.
//
//

#ifndef LuckyDrawReward_hpp
#define LuckyDrawReward_hpp

#include <stdio.h>

#include "cocos2d.h"
#include "JSONData.h"

#include <stdio.h>
#include <vector>
#include <map>
#include <string>

USING_NS_CC;

class LuckyDrawReward : public Ref, public JSONData
{
public:
	// General Information
	enum RewardType {			// Define the value using Bitwise
		RewardNone = 0,
		RewardBooster = 1,
		RewardFragment = 2,
		RewardStar = 3,
	};
	
	enum RarityType {
		RarityNone,
		RarityRare,
		RaritySuperRare,
	};
	
public:
	static std::string rewardTypeToString(RewardType type);
	static RewardType parseRewardType(const std::string &typeStr);
	
	static std::string rarityTypeToString(RarityType type);
	static RarityType parseRarityType(const std::string &typeStr);
	
public:
	CREATE_FUNC(LuckyDrawReward);
	LuckyDrawReward();
	virtual bool init() { return true; }

public: // properties
	CC_SYNTHESIZE(RewardType, mRewardType, RewardType);
	CC_SYNTHESIZE(RarityType, mRarityType, RarityType);
	CC_SYNTHESIZE(std::string, mItemName, ItemName);
	CC_SYNTHESIZE(int, mItemID, ItemID);
	CC_SYNTHESIZE(int, mQuantity, Quantity);
    CC_SYNTHESIZE(int, mFreeQuantity, FreeQuantity);
	CC_SYNTHESIZE(int, mProbability, Probability);	/// note: base = 10000
	
public: // Run Time properties
	CC_SYNTHESIZE(int, mRangeStart, RangeStart);
	CC_SYNTHESIZE(int, mRangeEnd, RangeEnd);
	
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
public: // data
	
//	CC_SYNTHESIZE(int, mPlanetID, PlanetID);
//	CC_SYNTHESIZE(std::string, mName, Name);
//	CC_SYNTHESIZE(std::string, mUnlockStatement, mUnlockStatement);
//	CC_SYNTHESIZE(std::string, mFirstBg, FirstBg);
//	CC_SYNTHESIZE(std::string, mSecondBg, SecondBg);
//	CC_SYNTHESIZE(std::string, mBannerImage, BannerImage);		// the Banner image in Dog Selection
//	CC_SYNTHESIZE(PlanetType, mType, PlanetType);
//	
//	void setDogList(const std::vector<int> &list);
//	std::vector<int> &getDogList();
	
public:
	
	std::string toString();
};


#endif /* LuckyDrawReward_hpp */
