//
//  BoosterData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 19/1/2017.
//
//

#include "BoosterData.h"
#include "JSONHelper.h"
#include "StringHelper.h"
#include "CommonType.h"

int BoosterData::parseBoosterType(const std::string &keyName)
{
	int result;
	
	sscanf(keyName.c_str(), "booster_%02d", &result);
	
	return result;
}

BoosterData::BoosterData()
: mType(BoosterItemMissile)
, mInfo("xxx")
, mPrice()		// default= 0 Stars
, mAttribute()	// no attribute
, mCooldown(5)
{
	
}

bool BoosterData::init()
{
	return true;
}


std::string BoosterData::getDescription()
{
	return "Activate to fire N Missles";
}

void BoosterData::setAttribute(const std::string &name, float value)
{
	mAttribute[name] = value;
}

std::string BoosterData::toString()
{
	std::string result = "";
	
	result += StringUtils::format("type=%d", (int) getType());
	result += StringUtils::format(" desc=[%s]", getInfo().c_str());
	result += StringUtils::format(" price=[%s]", getPrice().toString().c_str());
	
	
	std::string attributeStr = aurora::StringHelper::mapToString(mAttribute);
	
	result += StringUtils::format(" attribute[%s]", attributeStr.c_str());
	
	return result;
}



#pragma mark - JSON Data
void BoosterData::defineJSON(rapidjson::Document::AllocatorType &allocator,
							   rapidjson::Value &outValue)
{
//	
//	for (std::map<int, int>::iterator it=mBoosterCount.begin(); it!=mBoosterCount.end(); ++it)
//	{
//		int key = it->first;
//		int value = it->second;
//		
//		std::string keyName = StringUtils::format("booster_%d", key);
//		
//		addInt(allocator, keyName, value, outValue);
//	}
}

bool BoosterData::parseJSON(const rapidjson::Value &jsonValue)
{
	
	// Price
	MoneyType moneyType = (MoneyType) aurora::JSONHelper::getInt(jsonValue, "priceType", MoneyTypeStar);
	int moneyAmount = aurora::JSONHelper::getInt(jsonValue, "priceAmount", 0);
	setPrice(Price(moneyType, moneyAmount));
	
	std::string name = aurora::JSONHelper::getString(jsonValue, "name");
	setName(name);
	
	
	std::string info = aurora::JSONHelper::getString(jsonValue, "info");
	setInfo(info);
	
	setCooldown(aurora::JSONHelper::getFloat(jsonValue, "cooldown", 5));
    
    setIsInstantActive(aurora::JSONHelper::getBool(jsonValue, "instantActive", false));
	
	// Attribute
	std::map<std::string, float> attributeMap;
	aurora::JSONHelper::getJsonStrFloatMap(jsonValue, "attribute", attributeMap);
	
	for (std::map<std::string, float>::iterator it=attributeMap.begin();
				it != attributeMap.end(); ++it)
	{
		std::string name = it->first;
		float value = it->second;
		
		setAttribute(name, value);
	}
	
	
	
	return true;
}


void BoosterData::setItemEffectAttribute(ItemEffect *effect)
{
	if(effect == nullptr) {
		return;
	}
	
	for (std::map<std::string, float>::iterator it = mAttribute.begin();
					it != mAttribute.end(); ++it)
	{
		std::string name = it->first;
		float value = it->second;
		
		effect->setProperty(name, value);
	}
}
