//
//  CharUpgradeCost.cpp
//  GhostLeg
//
//  Created by Ken Lee on 3/4/2017.
//
//

#include "CharUpgradeCost.h"
#include "JSONHelper.h"

CharUpgradeCost::CharUpgradeCost(int _count, int _money)
: requireFragCount(_count)
, requireMoney(_money)
{
	
}

CharUpgradeCost::CharUpgradeCost()
: requireFragCount(0)
, requireMoney(0)
{
	
}


std::string CharUpgradeCost::toString()
{
	return StringUtils::format("frag=%d money=%d", requireFragCount, requireMoney);
}



#pragma mark - JSON
void CharUpgradeCost::defineJSON(rapidjson::Document::AllocatorType &allocator,
								rapidjson::Value &outValue)
{
	// No need, this is ready only
}

bool CharUpgradeCost::parseJSON(const rapidjson::Value &jsonValue)
{
	requireFragCount = aurora::JSONHelper::getInt(jsonValue, "fragment", 99);
	requireMoney = aurora::JSONHelper::getInt(jsonValue, "money", 9999);

	return true;
}
