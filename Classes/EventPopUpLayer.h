//
//  EventPopUpLayer.hpp
//  GhostLeg
//
//  Created by Calvin on 15/12/2016.
//
//

#ifndef EventPopUpLayer_hpp
#define EventPopUpLayer_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class DogData;

class EventPopUpLayer : public Layer
{
public:
    
    CREATE_FUNC(EventPopUpLayer);
    
    virtual bool init() override;
    virtual void onEnter() override;
    
    void setupUI(Node* node);
    
    void setCallback(std::function<void(Node*)> callback);
    
private:
    Node* mMainPanel;
    std::function<void(Node*)> mCallback;
    Button* mOKBtn;
    
};

#endif /* EventPopUpLayer_hpp */
