//
//  BoosterLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 23/1/2017.
//
//

#include "BoosterLayer.h"
#include "CommonMacro.h"
#include "ItemManager.h"
#include "GameManager.h"
#include "RichTextLabel.h"
#include "BuyBoosterLayer.h"
#include "MainScene.h"
#include "PlanetManager.h"
#include "GameSound.h"
#include "Analytics.h"

#define kParticleEffectTag 9999
#define kCountFontSize 24

const Color4B kColorYellow = Color4B(255, 211, 49, 255);
const Color4B kColorGreen = Color4B(14, 255, 217, 255);

BoosterLayer::BoosterLayer()
{}

bool BoosterLayer::init()
{
    if(Layer::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("BoosterLayer.csb");
    setupUI(rootNode);
    addChild(rootNode);
    //
    //    setVisible(false);
    return true;
}

void BoosterLayer::setupUI(Node* rootNode)
{
    FIX_UI_LAYOUT(rootNode);
    
    mMainPanel = rootNode->getChildByName("mainPanel");
    mStartBtn = (Button*) mMainPanel->getChildByName("startButton");
    mCancelBtn = (Button*) mMainPanel->getChildByName("cancelBtn");
    mPlanetSprite = mMainPanel->getChildByName<Sprite*>("planetSprite");
    mDistrictLabel = mMainPanel->getChildByName<Text*>("districtLabel");
    mPlanetNameLabel = mMainPanel->getChildByName<Text*>("planetName");
    
    mStartBtn->addClickEventListener([&](Ref*){
        if(mStartCallback){
            this->mStartCallback();
        }
        this->removeFromParent();
    });
    
    mCancelBtn->addClickEventListener([&](Ref*){
        if(this->getParent()){
            this->removeFromParent();
        }
    });
    
    BoosterItemType list[5] = {
		
		BoosterItemMissile,BoosterItemSnowball,BoosterItemDoubleCoin,BoosterItemTimeStop,BoosterItemUnbreakable};
    for(int i=0;i<5;i++){
        int type = static_cast<int>(list[i]);
        int tag = 100+type;
        setItemCallback(tag);
    }
    
    Text *infoLabel;
    
    infoLabel = mMainPanel->getChildByName<Text*>("infoText");
    mInfoLabel = RichTextLabel::createFromText(infoLabel);

    mInfoLabel->setTextColor(1, kColorGreen);
    mInfoLabel->setTextColor(2, kColorYellow);
    
    mMainPanel->addChild(mInfoLabel);
    mInfoLabel->setVisible(false);

}

void BoosterLayer::onEnter()
{
    Layer::onEnter();
    GameManager::instance()->selectBooster(BoosterItemType::BoosterItemNone);
    if(mOnEnterCallback){
        mOnEnterCallback();
    }
    refreshAllItemView();
	
	
	LOG_SCREEN(Analytics::scn_booster_select);
}

void BoosterLayer::setupDistrictUI(int planetID)
{
    PlanetData *data = PlanetManager::instance()->getPlanetData(planetID);
    mDistrictLabel->setString(StringUtils::format("stage %d",planetID));
    mPlanetNameLabel->setString(data->getName());
    mPlanetSprite->setTexture(StringUtils::format("guiImage/ui_booster_stage%d.png", planetID));
}

void BoosterLayer::refreshAllItemView()
{
    mInfoLabel->setVisible(false);
    BoosterItemType list[5] = {
		
		
		BoosterItemMissile,BoosterItemSnowball,BoosterItemDoubleCoin,BoosterItemTimeStop,BoosterItemUnbreakable};
    for(int i=0;i<5;i++){
        int type = static_cast<int>(list[i]);
        int tag = 100+type;
        bool isSelected = GameManager::instance()->getSelectedBooster() == list[i];
        int count = ItemManager::instance()->getBoosterCount(list[i]);
        setItemView(tag, count,isSelected);
    }
}

void BoosterLayer::setItemView(int tag, int count, bool isSelected)
{
    Node* itemNode = mMainPanel->getChildByTag(tag);
    if(!itemNode){
        return;
    }
    
    Node* counterPanel = itemNode->getChildByName<Sprite*>("counterPanel");
    Text* countText = counterPanel->getChildByName<Text*>("countText");
    Sprite* selectedBg = itemNode->getChildByName<Sprite*>("selectedBG");
    
    
//    Node* emitter = itemNode->getChildByTag(kParticleEffectTag);
    
    if(count>99){
        countText->setString("99+");
    }else{
        countText->setString(StringUtils::format("%d",count));
    }
    
    if(count<=0){
        counterPanel->setVisible(false);
        selectedBg->setVisible(false);
    }else if(!isSelected){
        counterPanel->setVisible(true);
        selectedBg->setVisible(false);
    }else if(isSelected){
        counterPanel->setVisible(true);
        selectedBg->setVisible(true);
        
//        emitter->setVisible(true);
//        BoosterItemType type = static_cast<BoosterItemType>(tag-100);
//       mInfoLabel->setString(ItemManager::instance()->getBoosterInfo(type));
//        mInfoLabel->setVisible(true);
    }
    
}


void BoosterLayer::setItemCallback(int tag)
{
    Node* itemNode = mMainPanel->getChildByTag(tag);
    if(!itemNode){
        return;
    }
//    Node* itemSprite = itemNode->getChildByName("itemSprite");
    BoosterItemType thisType = static_cast<BoosterItemType>(tag - 100);
    
/*
    auto emitter = ParticleSystemQuad::create("particle_booster_active1.plist");
    emitter->setScale(0.5);
    emitter->setPosition(itemSprite->getPosition());
    emitter->setVisible(false);
    emitter->setTag(kParticleEffectTag);
    itemNode->addChild(emitter,-1);
*/
    
    Layout* touchPanel =  itemNode->getChildByName<Layout*>("touchArea");
    Button* plusBtn = itemNode->getChildByName<Button*>("plusBtn");
    
    touchPanel->addClickEventListener([=](Ref*){
        GameSound::playSound(GameSound::SelectBooster);
        int count = ItemManager::instance()->getBoosterCount(thisType);
        if(count<=0){
            BuyBoosterLayer *layer = BuyBoosterLayer::create();
            layer->setupView(thisType);
            layer->setPurchaseSuccessCallback([&](int amount){
                GameSound::playSound(GameSound::Purchase);
                ItemManager::instance()->addBoosterCount(thisType,amount);
                GameManager::instance()->selectBooster(thisType);
                this->refreshAllItemView();
                
                Node* parentNode = getParent();
                while (parentNode) {
                    MainSceneLayer* mainScene = dynamic_cast<MainSceneLayer*>(parentNode);
                    if(!mainScene){
                        parentNode = parentNode->getParent();
                    }else{
                        break;
                    }
                }
                
                if(parentNode){
                    MainSceneLayer* mainNode = (MainSceneLayer*) parentNode;
                    mainNode->updateTotalCoin();
                    mainNode->updateDiamond();
                }
                
            });
            addChild(layer);
            return;
        }
        
        int boosterType = tag - 100;
        GameManager::instance()->selectBooster((BoosterItemType)boosterType);
        refreshAllItemView();
    });
    
    plusBtn->addClickEventListener([&,thisType](Ref*){
        GameSound::playSound(GameSound::SelectBooster);
        BuyBoosterLayer *layer = BuyBoosterLayer::create();
        layer->setupView(thisType);
        layer->setPurchaseSuccessCallback([&](int amount){
            GameSound::playSound(GameSound::Purchase);
            ItemManager::instance()->addBoosterCount(thisType,amount);
            GameManager::instance()->selectBooster(thisType);
            this->refreshAllItemView();
            
            Node* parentNode = getParent();
            while (parentNode) {
                MainSceneLayer* mainScene = dynamic_cast<MainSceneLayer*>(parentNode);
                if(!mainScene){
                    parentNode = parentNode->getParent();
                }else{
                    break;
                }
            }
            
            if(parentNode){
                MainSceneLayer* mainNode = (MainSceneLayer*) parentNode;
                mainNode->updateTotalCoin();
                mainNode->updateDiamond();
            }
        });
        addChild(layer);
    });
}





void BoosterLayer::setOnEnterCallback(std::function<void ()> callback)
{
    mOnEnterCallback = callback;
}


void BoosterLayer::setStartCallback(std::function<void ()> callback)
{
    mStartCallback = callback;
}
