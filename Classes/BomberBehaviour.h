//
//  BomberBehaviour.hpp
//  GhostLeg
//
//  Created by Calvin on 8/3/2017.
//
//

#ifndef BomberBehaviour_hpp
#define BomberBehaviour_hpp

#include <stdio.h>
#include "EnemyBehaviour.h"
#include "cocos2d.h"
#include "CommonType.h"

class BomberBehaviour : public EnemyBehaviour
{
public:
    
    CREATE_FUNC(BomberBehaviour);
    
    BomberBehaviour();
    
    bool init() { return true; }
    
    std::string toString();
    
public:
    virtual void onCreate();
    virtual void onVisible();
    virtual void onActive();
    virtual void onUpdate(float delta);
    
    void fireBullet();
    
    void updateProperty();
    
private:
    float mCooldown;    
    float mCounter;
    float mAppearTime;
    float mDisplacementY;
    bool mStartLeaving;
    
};


#endif /* BomberBehaviour_hpp */
