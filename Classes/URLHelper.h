//
//  URLHelper.hpp
//  GhostLeg
//
//  Created by Calvin on 11/1/2017.
//
//

#ifndef URLHelper_hpp
#define URLHelper_hpp

#include <stdio.h>
#include <string>

extern std::string UriEncode(const std::string & sSrc);
extern std::string UriDecode(const std::string & sSrc);

class URLHelper{
    
public:
    /* Returns a url-encoded version of str */
    /* IMPORTANT: be sure to free() the returned string after use */
    static char *url_encode(const char *str);

    /* Returns a url-decoded version of str */
    /* IMPORTANT: be sure to free() the returned string after use */
    static char *url_decode(char *str);


	static std::string uriEncode(const std::string & sSrc);
};

#endif /* URLHelper_hpp */
