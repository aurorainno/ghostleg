//
//  SnowBall.cpp
//  GhostLeg
//
//  Created by Calvin on 7/12/2016.
//
//

#include "SnowBall.h"
#include "GameWorld.h"
#include "Enemy.h"
#include "EnemyData.h"
#include "ModelLayer.h"
#include "Star.h"
#include "Coin.h"
#include "GeometryHelper.h"
#include "EffectLayer.h"
#include "GameSound.h"
#include "CommonType.h"
#include "ItemManager.h"
#include "AttackBehaviour.h"

const int kBulletID = 80;

namespace {
    std::string getActionAnimeName(SnowBall::Action action) {
        switch (action) {
            case SnowBall::Action::Start:
                return "active";
            case SnowBall::Action::Loop:
                return "loop";
            case SnowBall::Action::Destroy:
                return "disable";
            default:
                return "";
        }
    }
    
    bool isLoopAnimation(SnowBall::Action action) {
        switch (action) {
            case SnowBall::Action::Start:
            case SnowBall::Action::Destroy:
                return false;
            case SnowBall::Action::Loop:
                return true;
            
            default:
                return false;
        }
    }
}

// Note: it is Cyclone Now

SnowBall::SnowBall()
: Item(Item::Type::ItemSnowBall)
, mVelocity(100)
, mAcceler(150)
, mMaxVelocity(200)
, mMoveLogic(nullptr)
, isDestoryed(false)
{
    
}

SnowBall::~SnowBall()
{
    log("Destruction of SnowBall");
}


bool SnowBall::init()
{
    if(Item::init() == false) {
        log("SnowBall: init failed");
        return false;
    }
	
	SpeedData speedData = ItemManager::instance()->getCycloneSpeedData();
	
	mVelocity = speedData.initialSpeed;
	mAcceler = speedData.acceleration;
	mMaxVelocity = speedData.maxSpeed;
	
	
	//
    
    GameSound::playSound(GameSound::CycloneActive,true);
    
    GameModel::setup("cyclone.csb");
    setAction(SnowBall::Action::Start);
    
    setAnimationEndCallFunc(SnowBall::Action::Start, [=](){
        this->setAction(SnowBall::Action::Loop);
    });



    
    
    
//	Player *player = GameWorld::instance()->getModelLayer()->getPlayer();
//    mVelocity = player->getMaxSpeed();
    setScale(0.6);
	
	// Define the move logic
	RouteMoveLogic *moveLogic = RouteMoveLogic::create();
	moveLogic->setGameMap(GameWorld::instance()->getMap());
	moveLogic->setDefaultDir(DirUp);		// Move Up after turn!
	moveLogic->setDir(DirUp);
	moveLogic->setSpeed(Vec2(mVelocity, mVelocity));
	setMoveLogic(moveLogic);
	
    return true;
}


void SnowBall::setupMoveLogic()
{
	Player *player = GameWorld::instance()->getPlayer();
	if(player == nullptr) {
		log("SnowBall.setup: player is null");
		return;
	}
	
	// Define the ball property based on player
	
	setPosition(player->getPosition());
    setPositionY(getPositionY()+60);
	mMoveLogic->setPosition(getPosition());
	mMoveLogic->setDir(player->getDir());
	mMoveLogic->setWalkingRoute(player->getHoriLine());
}


// Two case here
//		Player at vertical line		: dir = Up   currentLine=null
//		Player at slope line		: dir = Left / Right  currentLine=the walking line
void SnowBall::setupMoveLogic(Dir dir, MapLine *currentLine)
{
	mMoveLogic->setDir(dir);
	mMoveLogic->setWalkingRoute(currentLine);
	mMoveLogic->setPosition(getPosition());
}

bool SnowBall::use()
{
    return false;
}


void SnowBall::handleCollision()
{
    Vector<Enemy*> enemyList = GameWorld::instance()->getModelLayer()->getEnemyList();
    for(Enemy* enemy : enemyList){
        if(enemy->getEnemyData()->getType() == kBulletID){
            continue;
        }
        
        if(enemy->isObstacle()||!enemy->isCollidable()){
            continue;
        }
		
		// ken: this line is weird!!
        AttackBehaviour* attackBehaviour = dynamic_cast<AttackBehaviour*>(enemy->getBehaviour());
        if(attackBehaviour){
            if(!attackBehaviour->isColliable()){
                continue;
            }
        }
        
        if(!enemy->getBehaviour()->getData()->getDestroyable()){
  //          log("Destroyable:%d enemyName:%s",enemy->getBehaviour()->getData()->getDestroyable(),enemy->getName().c_str());
            continue;
        }
        
        Rect thisRect = this->getBoundingBox();
        Rect enemyRect = enemy->getBoundingBox();
        
        if(this->isCollideModel(enemy)){
            Coin *coin = Coin::create(Coin::SmallCoin);
            coin->setPosition(enemy->getPosition());
//            GameWorld::instance()->getModelLayer()->removeEnemy(enemy);
            GameWorld::instance()->getModelLayer()->addItem(coin);
            
            GameWorld::instance()->getEffectLayer()->playCsbAnime("cyclone_hit1.csb", "active", false, true, 1.0f, enemy->getPosition());
            enemy->beHit();
            Vec2 force = Vec2(0, mVelocity * 2);
            enemy->showKnockoutEffect(force);
        }
    }
}



void SnowBall::update(float delta)
{
    mDuration -= delta;


//    float posY = getPositionY();
//    
//    setPositionY(posY + mVelocity*delta);
	
    //handle enemy collision
    handleCollision();
    
//    GameWorld::instance()->getModelLayer()->moveAllStarsToPlayer();
    
    
	Vector<Collectable *> starList = GameWorld::instance()->getModelLayer()->getVisibleCollectableList();
    for(Collectable *star : starList) {
        if(star->isCollideModel(this)){
            //  log("star collide with snowball");
            GameSound::playStarSound();
            GameWorld::instance()->collectCoin(1);
            GameWorld::instance()->getModelLayer()->removeItem(star);
            continue;
        }
        
        if(fabsf(star->getPositionX() - getPositionX())>100||
           fabsf(star->getPositionY() - getPositionY())>100){
            continue;
        }
        
        if(isDestoryed && !star->getStartMoving())
        {
            continue;
        }
        
        Vec2 starPos = star->getPosition();
        
        Vec2 newPos = aurora::GeometryHelper::calculateNewTracePosition(starPos,
                                                                        getPosition(),
                                                                        star->getVelocity(), delta);
        
        star->setStartMoving(true);
        star->setPosition(newPos);
        
        star->setVelocity(star->getVelocity() + 10);
    }
    

    if(isDestoryed){
        return;
    }
    
    //update position
	
	updateVelocity(delta);
	
	
    // Calculate Next position
    Vec2 newPos = mMoveLogic->calculateNewPosition(delta);
    
    setPosition(newPos);
    
    mMoveLogic->updateStateData(newPos);
    
    // Removal Handling
    if(shouldRemove()) {
        /*
        Vector<Star *> starList = GameWorld::instance()->getModelLayer()->getStarList();
        Vector<Star *> movingStarList{};
        for(Star *star : starList) {
            if(!star->getStartMoving()){
                continue;
            }
            
            Vec2 targetPos = getPosition();
//            targetPos.y += 200;
            float angle = aurora::GeometryHelper::findAngleRadian(star->getPosition(), targetPos);
            Vec2 speed = aurora::GeometryHelper::resolveVec2(star->getVelocity(), angle);
            star->setSpeed(speed);
            star->setAccel(200);
            star->setMovingToFreeSpace(true);
            movingStarList.pushBack(star);
//            log("========\nstar%s start moving, speed=%.2f,%.2f\n========",star->getName().c_str(),speed.x,speed.y);
        }
        */
        isDestoryed = true;
        setAction(Destroy);
        
        GameSound::stopSound(GameSound::CycloneActive);
        GameSound::playSound(GameSound::CycloneHit);
        
        setAnimationEndCallFunc(SnowBall::Action::Destroy, [=](){
//            for(Star* star : movingStarList){
//                GameWorld::instance()->getModelLayer()->removeItem(star);
//                GameWorld::instance()->collectCoin(1);
//            }
            GameWorld::instance()->getModelLayer()->removeItem(this);
        });
    }
}

// Out of the screen
bool SnowBall::shouldRemove()
{
//    Rect myRect = getBoundingBox();
//    myRect.size = Size(10, 10);
    
 //   return GameWorld::instance()->isRectVisible(myRect) == false;
    return mDuration <= 0;
}

void SnowBall::setAction(SnowBall::Action action)
{
    if(mRootNode == nullptr) {
        log("Debug: setAction: mRootNode is null");
        return;	// nothing to do
    }
    
    std::string animeName = getActionAnimeName(action);
    if(animeName.length() == 0){
        log("Debug: setAction: animeName is null");
        return;		// something wrong
    }
    runAnimation(animeName, isLoopAnimation(action));
}

void SnowBall::setAnimationEndCallFunc(SnowBall::Action action, std::function<void()> func)
{
    std::string animeName = getActionAnimeName(action);
    
    if(getTimeLine()) {
        getTimeLine()->setAnimationEndCallFunc(animeName, func);
    }
}


void SnowBall::updateVelocity(float delta)
{
	if(mVelocity >= mMaxVelocity) {
		return;
	}
	
	float newValue = mVelocity + delta * mAcceler;
	if(newValue > mMaxVelocity) {
		newValue = mMaxVelocity;
	}
	
	mVelocity = newValue;
}


bool SnowBall::isWeapon()
{
	return true;
}
