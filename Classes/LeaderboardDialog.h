//
//  LeaderboardDialog.hpp
//  GhostLeg
//
//  Created by Calvin on 16/1/2017.
//
//

#ifndef LeaderboardDialog_hpp
#define LeaderboardDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "AstrodogData.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class LeaderboardItemView;

class LeaderboardDialog : public Layout
{
public:
    enum LeaderboardUIType{
        Endgame = 0,
        Mainscene
    };
    
    CREATE_FUNC(LeaderboardDialog);
    
    LeaderboardDialog();
    
    virtual bool init();
    void setupUI();
    
    void setLeaderboardGUI(AstrodogLeaderboardType type);
    
    void updateWithLeaderboard(AstrodogLeaderboard *board, int currentPlanet);
    void updateLBListView(Vector<AstrodogRecord *> recordList);
    void updateRankStatus(AstrodogRankStatus status);
    
    void setFriendBtnCallback(std::function<void()>);
    void setNationalBtnCallback(std::function<void()>);
    void setGlobalBtnCallback(std::function<void()>);
    void setFbBtnCallback(std::function<void()>);
    
    void setLeaderboardUIType(LeaderboardUIType type);
    void setupRankStatus(int newRank, LeaderboardItemView *itemView);
    
    void showLoading();
    ListView *getListView();
    
private:
    std::string getCsbName(LeaderboardUIType type);
    
private:
    Node *mRootNode;
    Button *mFriendBtn, *mNationalBtn, *mGlobalBtn;
    Button *mFBConnectBtn;
    Node *mFriendBorder, *mNationalBorder, *mGlobalBorder;
    Text *mLoadingText;
    Text *mEmptyFriendAlert;
    ListView *mListView;
    LeaderboardUIType mMyType;
    AstrodogLeaderboardType mAstrodogLBType;
    int mMyRecordIndex;
    int mCurrentPlanet;
    
    std::function<void()> mFriendCallback;
    std::function<void()> mNationalCallback;
    std::function<void()> mGlobalCallback;
    std::function<void()> mFbBtnCallback;
    
};



#endif /* LeaderboardDialog_hpp */
