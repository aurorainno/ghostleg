//
//  AstrogData.hpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#ifndef AstrogData_hpp
#define AstrogData_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "JSONData.h"

USING_NS_CC;



typedef enum {
	AstrodogLeaderboardGlobal = 1,
	AstrodogLeaderboardCountry = 2,
	AstrodogLeaderboardFriend = 3,
}AstrodogLeaderboardType;


typedef enum {
	AstrodogRankNone = 0,	// no rank change or for those isn't my rank
	AstrodogRankUp = 1,
	AstrodogRankDown = 2,
}AstrodogRankStatus;


const int STATUS_SUCCESS = 0;
const int STATUS_ERROR = -1;


#pragma mark - FBFriend
class FBFriend : public Ref, public JSONData
{
public:
    FBFriend();
    
    CREATE_FUNC(FBFriend)
    
    virtual bool init() { return true; }
    
    CC_SYNTHESIZE(std::string, mFbID, FbID);
    CC_SYNTHESIZE(std::string, mName, Name);
    CC_SYNTHESIZE(bool, mInstalled, Installed);
    
    std::string toString();
    
#pragma mark - JSON Data
public: // JSON
    virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
    virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
    
};


#pragma mark - AstrodogUser
class AstrodogUser : public Ref, public JSONData
{
public:
	AstrodogUser();
	
	CREATE_FUNC(AstrodogUser)
	
	virtual bool init() { return true; }
	
	CC_SYNTHESIZE(int, mUid, Uid);				// it is just for reference now
	CC_SYNTHESIZE(std::string, mPlayerID, PlayerID);
	CC_SYNTHESIZE(std::string, mFbID, FbID);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mCountry, Country);
	
	std::string toString();
    
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
};

#pragma mark - AstrodogRecord

class AstrodogRecord : public Ref, public JSONData
{
public:
	AstrodogRecord();
	
	CREATE_FUNC(AstrodogRecord)
	
	virtual bool init() { return true; }
	
	CC_SYNTHESIZE(int, mUid, Uid);
	CC_SYNTHESIZE(std::string, mPlayerID, PlayerID);
	CC_SYNTHESIZE(std::string, mName, Name);
	CC_SYNTHESIZE(std::string, mFbID, FbID);
	CC_SYNTHESIZE(int, mRank, Rank);
	CC_SYNTHESIZE(int, mScore, Score);
	CC_SYNTHESIZE(std::string, mCountry, Country);
	
	
	std::string toString();
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value

};


class AstrodogLeaderboard : public Ref, public JSONData
{
public:
	AstrodogLeaderboard();
	
	CREATE_FUNC(AstrodogLeaderboard)
	
	static std::string getLeaderboardKey(AstrodogLeaderboardType type, int planetID);
	
	virtual bool init() { return true; }
	
	CC_SYNTHESIZE(AstrodogLeaderboardType, mType, Type);
	CC_SYNTHESIZE(int, mPlanet, Planet);
	CC_SYNTHESIZE(std::string, mWeek, Week);
	CC_SYNTHESIZE(float, mRemainTime, RemainTime);	// remain time
	CC_SYNTHESIZE(long long, mEndTime, EndTime);	// remain time

	void addRecord(AstrodogRecord *record);
    Vector<AstrodogRecord *> getRecordList();
	
	Vector<AstrodogRecord *> recordList;
	
	std::string getKey();
	
	std::string toString();
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value

};



#pragma mark - Mail
class AstrodogAttachment : public JSONData
{
public:
	enum Type {
		TypeNone = 0,
		TypeDiamond = 1,
		TypeStar = 2,
        TypePuzzle = 3,
	};
public:
	AstrodogAttachment();
	Type type;
	int amount;
    int itemID;

	std::string toString();
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
};

class AstrodogMail : public Ref, public JSONData
{
public:
	enum Status {
		StatusUnread = 0,
		StatusReceived = 1,
	};
public:
	AstrodogMail();
	
	CREATE_FUNC(AstrodogMail);
	
	virtual bool init() { return true; }
	
	CC_SYNTHESIZE(int, mMailID, MailID);
    CC_SYNTHESIZE(int, mIconID, IconID);
	CC_SYNTHESIZE(std::string, mTitle, Title);
	CC_SYNTHESIZE(std::string, mMessage, Message);
	CC_SYNTHESIZE(AstrodogAttachment, mAttachment, Attachment);
	CC_SYNTHESIZE(Status, mStatus, Status);
	CC_SYNTHESIZE(long long, mCreateTime, CreateTime);
	
	
	std::string toString();
	
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
	
};



#pragma mark AstrodogMailbox
class AstrodogMailbox : public Ref, public JSONData
{
public:
	AstrodogMailbox();
	
	CREATE_FUNC(AstrodogMailbox)
	
	virtual bool init() { return true; }

	void setupSampleData();
	void addMail(AstrodogMail *mail);
	
	AstrodogMail *getMailByID(int mailID);
	void markMailReceived(int mailID);
	
	Vector<AstrodogMail *> getMailList();
	Vector<AstrodogMail *> getSortedMailList();
	
	std::string toString();
public:
	Vector<AstrodogMail *> mMailList;
	
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
};




#endif /* AstrogData_hpp */
