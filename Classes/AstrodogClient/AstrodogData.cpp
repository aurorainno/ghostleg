//
//  AstrogData.cpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#include "AstrodogData.h"
#include "StringHelper.h"
#include "JSONHelper.h"


#define kDefaultCountryCode "US"


#pragma mark - FBFriend
FBFriend::FBFriend()
: Ref()
, mName("")
, mFbID("")
, mInstalled(false)
{
    
}


std::string FBFriend::toString()
{
    std::string str = "";
    
    str += " name=" + mName;
    str += " fbID=" + mFbID;
    str += StringUtils::format(" installed=%d",(int)mInstalled);

    return str;
}

void FBFriend::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue) {
    
    addString(allocator, "name", getName(), outValue);
    addString(allocator, "fbid", getFbID(), outValue);
    addInt(allocator, "installed", (int) getInstalled(), outValue);
    //   addIntDataArray(allocator, "friendList", getFriendList(), outValue);
}

bool FBFriend::parseJSON(const rapidjson::Value &json)
{

	setName(aurora::JSONHelper::getString(json, "name", "Player"));
    setFbID(aurora::JSONHelper::getString(json, "fbid", ""));
	
    setInstalled((bool)aurora::JSONHelper::getInt(json, "installed", 0));
	
    //    aurora::JSONHelper::getJsonIntArray(json, "friendList", mFriendList);
    
    return true;
}



#pragma mark - AstrodogUser
AstrodogUser::AstrodogUser()
: Ref()
, mUid(0)
, mName("")
, mPlayerID("")
, mFbID("")
, mCountry("US")

{
	
}


std::string AstrodogUser::toString()
{
	std::string str = "";
	
	str += "uid=" + INT_TO_STR(mUid);
	str += " playerID=" + mPlayerID;
	str += " name=" + mName;
	str += " fbID=" + mFbID;
	str += " country=" + mCountry;
    
	return str;
}

void AstrodogUser::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue) {

	addInt(allocator, "uid", getUid(), outValue);
	addString(allocator, "playerID", getPlayerID(), outValue);
	addString(allocator, "name", getName(), outValue);
	addString(allocator, "fbid", getFbID(), outValue);
	addString(allocator, "country", getCountry(), outValue);
 //   addIntDataArray(allocator, "friendList", getFriendList(), outValue);
}

bool AstrodogUser::parseJSON(const rapidjson::Value &json)
{
	setUid(aurora::JSONHelper::getInt(json, "uid", 0));
	setPlayerID(aurora::JSONHelper::getString(json, "playerID", ""));
	setName(aurora::JSONHelper::getString(json, "name", "Player"));
	setFbID(aurora::JSONHelper::getString(json, "fbid", ""));
	setCountry(aurora::JSONHelper::getString(json, "country", kDefaultCountryCode));
//    aurora::JSONHelper::getJsonIntArray(json, "friendList", mFriendList);

	return true;
}


#pragma mark - AstrodogLeaderboard
std::string AstrodogLeaderboard::getLeaderboardKey(AstrodogLeaderboardType type, int planetID)
{
	return StringUtils::format("lb_t%d_p%d", (int)type, planetID);
}


AstrodogLeaderboard::AstrodogLeaderboard()
: Ref()
, recordList()
, mType(AstrodogLeaderboardGlobal)
, mPlanet(1)
{
	
}

void AstrodogLeaderboard::addRecord(AstrodogRecord *record)
{
	recordList.pushBack(record);
}

Vector<AstrodogRecord*> AstrodogLeaderboard::getRecordList()
{
    return recordList;
}

std::string AstrodogLeaderboard::getKey()
{
	return AstrodogLeaderboard::getLeaderboardKey(getType(), getPlanet());
}

void AstrodogLeaderboard::defineJSON(rapidjson::Document::AllocatorType &allocator,
										rapidjson::Value &outValue)
{
//	CC_SYNTHESIZE(AstrodogLeaderboardType, mType, Type);
//	CC_SYNTHESIZE(int, mPlanet, Planet);
//	CC_SYNTHESIZE(std::string, mWeek, Week);
//	CC_SYNTHESIZE(float, mRemainTime, RemainTime);	// remain time
//	

//	addInt(allocator, "uid", getUid(), outValue);
//	addString(allocator, "name", getName(), outValue);

	
	addInt(allocator, "type", getType(), outValue);
	addInt(allocator, "planet", getPlanet(), outValue);
	addDouble(allocator, "remainTime", getRemainTime(), outValue);
	addString(allocator, "week", getWeek(), outValue);
	
	std::vector<JSONData *> recordVector;
	for(AstrodogRecord *record : recordList) {
		recordVector.push_back(record);
	}

	addJSONDataArray(allocator, "recordList", recordVector, outValue);
}

bool AstrodogLeaderboard::parseJSON(const rapidjson::Value &json)		// JSON DAta -> this object value
{
	setType((AstrodogLeaderboardType) aurora::JSONHelper::getInt(json, "type", 0));
	setPlanet(aurora::JSONHelper::getInt(json, "planet", 1));		// 1 is the first planet
	setRemainTime(aurora::JSONHelper::getFloat(json, "remainTime", 0));
	setWeek(aurora::JSONHelper::getString(json, "week", ""));
	setEndTime(aurora::JSONHelper::getLongLong(json, "endTime", 0));
	
	recordList.clear();
	
	const rapidjson::Value &recordJson = json["recordList"];
	
	if(recordJson.IsArray() == false) {
		log("AstrodogLeaderboard:parseJSON: json is not array");
	} else {
		for (rapidjson::SizeType i = 0; i < recordJson.Size(); i++)
		{
			AstrodogRecord *record = AstrodogRecord::create();
			record->parseJSON(recordJson[i]);
	
			recordList.pushBack(record);
		}
	}
	

	
	

	return true;
}


std::string AstrodogLeaderboard::toString()
{
	std::string str = "";
	
	str += "type=" + INT_TO_STR(mType);
	str += " planet=" + INT_TO_STR(mPlanet);
	str += " week=" + mWeek;
	str += " endTime=" + StringUtils::format("%lld", mEndTime);
	str += "\n";
	
	str += StringUtils::format("recordCount: %ld\n", recordList.size());
	for(AstrodogRecord *record : recordList) {
		str += record->toString() + "\n";
	}

	
	return str;
}

#pragma mark - AstrodogRecord

AstrodogRecord::AstrodogRecord()
: Ref()
, mUid(0)
, mPlayerID("")
, mName("")
, mRank(0)
, mScore(0)
, mCountry("US")
{
	
}

void AstrodogRecord::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
	addInt(allocator, "uid", getUid(), outValue);
	addInt(allocator, "rank", getRank(), outValue);
	addInt(allocator, "score", getScore(), outValue);
	addString(allocator, "name", getName(), outValue);
	addString(allocator, "fbid", getFbID(), outValue);
	addString(allocator, "playerID", getPlayerID(), outValue);
	
	addString(allocator, "country", getCountry(), outValue);
}

bool AstrodogRecord::parseJSON(const rapidjson::Value &json)		// JSON DAta -> this object value
{
	setUid(aurora::JSONHelper::getInt(json, "uid", 0));
	setRank(aurora::JSONHelper::getInt(json, "rank", 9999));
	setScore(aurora::JSONHelper::getInt(json, "score", 0));
	setName(aurora::JSONHelper::getString(json, "name", "Player"));
	setPlayerID(aurora::JSONHelper::getString(json, "playerID", ""));
	setFbID(aurora::JSONHelper::getString(json, "fbid", ""));
	setCountry(aurora::JSONHelper::getString(json, "country", kDefaultCountryCode));

	
	return true;
}

std::string AstrodogRecord::toString()
{
	std::string str = "";
	
	str += StringUtils::format("uid=%d", mUid);
	str += StringUtils::format(" name=%s", mName.c_str());
	str += StringUtils::format(" fb=%s", mFbID.c_str());
	str += StringUtils::format(" playerID=%s", mPlayerID.c_str());
	str += StringUtils::format(" rank=%d", mRank);
	str += StringUtils::format(" score=%d", mScore);
	str += StringUtils::format(" country=%s", mCountry.c_str());
	
	
	return str;
}





#pragma mark - Mail
AstrodogAttachment::AstrodogAttachment()
: type(TypeNone)
, amount(0)
{
	
}

std::string AstrodogAttachment::toString()
{
	return StringUtils::format("[%d: %d]", (int) type, amount);
}

void AstrodogAttachment::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
    
}

bool AstrodogAttachment::parseJSON(const rapidjson::Value &jsonValue)
{
    type = (Type) aurora::JSONHelper::getInt(jsonValue, "type", 0);
    amount =  aurora::JSONHelper::getInt(jsonValue, "amount", 0);
    itemID = aurora::JSONHelper::getInt(jsonValue, "itemID", 1);
    
	return true;
}

AstrodogMail::AstrodogMail()
: mMailID(0)
, mTitle("")
, mMessage("")
, mAttachment()
, mStatus(StatusUnread)
, mCreateTime(0)
{
	
}

	
std::string AstrodogMail::toString()
{
	std::string result = "";
	
	result += StringUtils::format("id=%d", mMailID);
	result += " title=[" + mTitle + "]";
	result += " message=[" + mMessage + "]";
	result += " attachment=" + mAttachment.toString();
	result += StringUtils::format(" status=%d", mStatus);
	result += StringUtils::format(" createTime=%lld", mCreateTime);
	
	return result;
}

void AstrodogMail::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
	
}

bool AstrodogMail::parseJSON(const rapidjson::Value &jsonValue)		// JSON DAta -> this object value
{
    setMailID(aurora::JSONHelper::getInt(jsonValue, "mailID", 0));
    setIconID(aurora::JSONHelper::getInt(jsonValue, "icon", 0));
    setTitle(aurora::JSONHelper::getString(jsonValue, "title", "empty title"));
    setMessage(aurora::JSONHelper::getString(jsonValue, "message", "empty message"));
    const rapidjson::Value &value = jsonValue["attachment"];
    AstrodogAttachment attachment;
    attachment.parseJSON(value);
    setAttachment(attachment);
    setStatus((Status) aurora::JSONHelper::getInt(jsonValue, "status", 0));
    setCreateTime(aurora::JSONHelper::getLongLong(jsonValue, "createTime", 0));
    
	return true;
}



#pragma mark AstrodogMailbox
AstrodogMailbox::AstrodogMailbox()
: mMailList(0)
{
	
}
	
void AstrodogMailbox::setupSampleData()
{
	mMailList.clear();
	
	for(int i=0; i<10; i++) {
		int mailID = 1 + i;
		std::string title = StringUtils::format("title %d", mailID);
		std::string message = StringUtils::format("message %d", mailID);
		
		AstrodogMail *mail = AstrodogMail::create();
		mail->setMailID(mailID);
		mail->setTitle(title);
		mail->setMessage(message);
			
		
		
		// Attachment
		AstrodogAttachment attachment;
		if(i < 5) {
            mail->setCreateTime(1487123100000);
            mail->setStatus(AstrodogMail::StatusReceived);
			attachment.type = AstrodogAttachment::TypeDiamond;
            attachment.amount = i == 0 ? 10 : i;
		} else {
            mail->setCreateTime(1487303100000);
            mail->setStatus(AstrodogMail::StatusUnread);
			attachment.type = AstrodogAttachment::TypeStar;
			attachment.amount = i * 100;
		}
		mail->setAttachment(attachment);
		
		
		addMail(mail);
	}
	
	
}

void AstrodogMailbox::addMail(AstrodogMail *mail)
{
	mMailList.pushBack(mail);
}

Vector<AstrodogMail *> AstrodogMailbox::getMailList()
{
	return mMailList;
}

Vector<AstrodogMail *> AstrodogMailbox::getSortedMailList()
{
	Vector<AstrodogMail *> result = mMailList;
	
	// TODO:
	// Sort the unread to top, Sort the received on bottom
    struct {
        bool operator()(AstrodogMail *a, AstrodogMail *b)
        {
            AstrodogMail::Status statusA = a->getStatus();
            AstrodogMail::Status statusB = b->getStatus();
            return statusA < statusB;
        }
    } customSort;
    
    std::sort(result.begin(), result.end(), customSort);
	
	return result;
}
	
std::string AstrodogMailbox::toString()
{
	std::string result = "";
	
	result += StringUtils::format("Mail Count: %d", (int) mMailList.size());
	
	for(AstrodogMail *mail : mMailList) {
		result += mail->toString();
		result += "\n";
	}
	
	return result;
}

AstrodogMail *AstrodogMailbox::getMailByID(int mailID)
{
	for(AstrodogMail *mail : mMailList) {
		if(mail->getMailID() == mailID) {
			return mail;
		}
	}
	
	return nullptr;
}

void AstrodogMailbox::markMailReceived(int mailID)
{
	AstrodogMail *mail = getMailByID(mailID);
	if(mail) {
		mail->setStatus(AstrodogMail::StatusReceived);
	}
}

void AstrodogMailbox::defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue)
{
	
}

bool AstrodogMailbox::parseJSON(const rapidjson::Value &jsonValue)		// JSON DAta -> this object value
{
	return true;
}
