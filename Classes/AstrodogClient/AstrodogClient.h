//
//  AstrodogClient.hpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#ifndef AstrodogClient_hpp
#define AstrodogClient_hpp

#include <stdio.h>


#include <stdio.h>
#include "cocos2d.h"
#include "CommonType.h"
#include "AstrodogData.h"
#include "network/HttpRequest.h"
#include "network/HttpClient.h"
#include "PluginFacebook/PluginFacebook.h"
#include <map>
#include <string>

USING_NS_CC;

class AstrodogClientListener {
public:
	virtual void onSubmitScore(int status) {}
    
	virtual void onGetRanking(int status, AstrodogLeaderboardType type, int planet,
							  const std::map<std::string, int> &metaData) {}
    
    virtual void onFBConnect(int status) {}
	
	virtual void onMailReceived(int status) {}
	virtual void onMailboxLoaded(int status) {}
    
};

class AstrodogClient : public sdkbox::FacebookListener
{
public:
	enum AstrodogServer{
		AstrodogServerProduction = 1,
		AstrodogServerLocal = 2,
	};
public:
	static AstrodogClient *instance();
	
public:
	AstrodogClient();
	~AstrodogClient();
	
	CC_SYNTHESIZE_RETAIN(AstrodogUser *, mUser, User);

	void setServer(AstrodogServer server);
	
public:
	void setListener(AstrodogClientListener *listener);
	void removeListener();
	
	bool isMe(const std::string &playerID);
	
	void getRanking(AstrodogLeaderboardType type, int planet);
    
    void loadMailboxFromServer();
    void markMailReceivedToServer(int mailID);
	
	void submitScore(int planetID, int score);
	
	
	void updateUser(AstrodogUser *user);

	std::string getPlayerID();
	int getUserID();
	bool isRegistered();		// true is already register before
	
	std::string toString();
	
protected:
	std::string getRequestURL(const std::string &requestName);
	void sendRequest(const std::string &request, const network::ccHttpRequestCallback& callback);
	
private:
	AstrodogClientListener *mListener;
	int mUid;

private:
	CC_SYNTHESIZE_RETAIN(AstrodogLeaderboard *, mGlobalBoard, GlobalBoard);
	CC_SYNTHESIZE_RETAIN(AstrodogLeaderboard *, mFriendBoard, FriendBoard);
	CC_SYNTHESIZE_RETAIN(AstrodogLeaderboard *, mCountryBoard, CountryBoard);
    
#pragma mark - Core
public:
	void setup();
	void setupPlanetList(const std::vector<int> &planetList);
	
	std::string getUserInfo();
	void resetUser();			// just in case the record is crashed
    void resetUserFbData();
	std::string getCurrentServerURL();
	
private:
	std::string getServerURL(AstrodogServer server);
	
#pragma mark - Submit Score
	
	
#pragma mark - Facebook Connect
public:
	void connectFB();
	void disconnectFB();
	bool isFBConnected();
	void submitFBInfo(const std::string &fbName, const std::string &fbid);
    void fetchFriends();
	void fetchMyProfile();
	void onFetchMyProfile(const std::string& jsonData);
	
private:
	bool handleRegisteredPlayer(const std::string &fbName, const std::string &fbid);
#pragma mark - Facebook Listener
private:
	void onSharedSuccess(const std::string& message){}
	void onSharedFailed(const std::string& message){}
	void onSharedCancel(){}
	void onPermission(bool isLogin, const std::string& msg){}
	void onRequestInvitableFriends( const sdkbox::FBInvitableFriendsInfo& friends ){}
	void onInviteFriendsWithInviteIdsResult( bool result, const std::string& msg ){}
	void onInviteFriendsResult( bool result, const std::string& msg ){}
	void onAPI(const std::string& tag, const std::string& jsonData);
	void onLogin(bool isLogin, const std::string& msg);
	void onGetUserInfo( const sdkbox::FBGraphUser& userInfo);
    void onFetchFriends(bool ok, const std::string& msg);
	
#pragma mark - Leaderboard Getter and Setter
public:
	int getRankPosition(AstrodogLeaderboardType type, int planet);
	AstrodogLeaderboard *getLeaderboard(AstrodogLeaderboardType type, int planet);
	void setLeaderboard(AstrodogLeaderboard *leaderboard);
	std::string infoLeaderboard();
	void saveLeaderboard(AstrodogLeaderboard *leaderboard);
	void loadAllLeaderboard();
	void loadLeaderboard(AstrodogLeaderboardType type, int planet);

private:
	AstrodogLeaderboard *getLeaderboardByKey(const std::string &key);
	
#pragma mark - Local Data Save / Load Handling
private:
	void saveUser();
	void loadUser();
    
    void saveFriendList();
    void loadFriendList();
    
	void saveData(const std::string &key, const std::string &content);
	std::string loadData(const std::string &key);


	
	
#pragma mark - Mailbox
public:
	void loadSampleMailData();
	void receivedMail(int mailID);			// will call server
	void loadMailbox();						// Will call server
	AstrodogMailbox *getMailbox();
	bool isMailboxLoaded();
	
	void giveMailAttachment(int mailID);
	
private:
	AstrodogMailbox *mMailbox;
	bool mMailboxLoaded;
	
	
	
#pragma mark - Class Properties
private:
	Map<std::string, AstrodogLeaderboard *> mLeaderboardMap;
	AstrodogServer mServer;
	std::vector<int> mPlanetList;
    std::vector<FBFriend*> mFriendList;
	
#pragma mark - Mock Data
private:
	static AstrodogLeaderboard *getMockLeaderboard();
	
};

#endif /* AstrodogClient_hpp */
