//
//  AstrodogClient.cpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#include "AstrodogClient.h"
#include "StringHelper.h"

#include "ADResponseSubmitScore.h"
#include "ADResponseFBConnect.h"
#include "ADResponseGetRanking.h"
#include "ADResponseGetMail.h"

#include "DeviceHelper.h"
#include "URLHelper.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "JSONHelper.h"

#include "Analytics.h"		// ken: better move the Analytics logic outside

#include "PluginFacebook/PluginFacebook.h"
#include "PlayerManager.h"

#define CALL_LISTENER(_method_)	\
	if(mListener != nullptr) {	\
		mListener->_method_;	\
	}

// Adding Server consideration when add the key
#define USER_DEFAULT_KEY(_keyString_) \
	StringUtils::format("%s@%d", _keyString_.c_str(), mServer).c_str()


const std::string kKeyUser = "AstrodogClient.user";
const std::string kKeyFriendList = "AstrodogClient.friendList";
const std::string kKeyLeaderboard = "AstrodogClient.leaderboard";


namespace {	// Local Private function
	AstrodogRankStatus calculateRankStatus(int oldPos, int newPos) {
		if(oldPos == -1) {		// no rank change if this is the first score
			return AstrodogRankNone;
		}
		
		
		if(oldPos == newPos) {
			return AstrodogRankNone;
		} else if(oldPos > newPos) {
			return AstrodogRankUp;
		} else {
			return AstrodogRankDown;
		}
	}
	
}

static AstrodogClient *sInstance = nullptr;

AstrodogClient *AstrodogClient::instance()
{
	if(!sInstance) {
		sInstance = new AstrodogClient();
	}
	return sInstance;
}

AstrodogClient::AstrodogClient()
: mGlobalBoard(nullptr)
, mFriendBoard(nullptr)
, mCountryBoard(nullptr)
, mUser(nullptr)
, mServer(AstrodogServerProduction)
, mListener(nullptr)
, mLeaderboardMap()
, mMailbox(new AstrodogMailbox())
, mMailboxLoaded(false)
{
    sdkbox::PluginFacebook::setListener(this);
	setGlobalBoard(AstrodogLeaderboard::create());
	setFriendBoard(AstrodogLeaderboard::create());
	setCountryBoard(AstrodogLeaderboard::create());
    mUid = 27;  //hardcode for testing
}

AstrodogClient::~AstrodogClient()
{
}

void AstrodogClient::setupPlanetList(const std::vector<int> &planetList)
{
	mPlanetList.clear();
	
	for(int planet : planetList) {
		mPlanetList.push_back(planet);
	}
}

void AstrodogClient::setListener(AstrodogClientListener *listener)
{
	mListener = listener;
}

void AstrodogClient::removeListener()
{
	mListener = nullptr;
}

std::string AstrodogClient::getRequestURL(const std::string &requestName)
{
	std::string url = "http://" + getCurrentServerURL();
	url += "/";
	url += requestName;
	
	return url;
}


void AstrodogClient::sendRequest(const std::string &requestName, const network::ccHttpRequestCallback& callback)
{
	network::HttpRequest* request = new network::HttpRequest();
	
	
	request->setRequestType(network::HttpRequest::Type::GET);
	request->setResponseCallback(callback);
	
	request->setUrl(getRequestURL(requestName));
	
	network::HttpClient::getInstance()->send(request);
	request->release();

}

void AstrodogClient::loadMailboxFromServer()
{
    network::ccHttpRequestCallback callback =
    [=](network::HttpClient *client, network::HttpResponse *response){
        
        std::map<std::string, int> meta;
        
        if(response->isSucceed() == false) {
            CALL_LISTENER(onMailboxLoaded(STATUS_ERROR));
            return;
        }
        
        //			const char *responseContent = response->getResponseDataString();
        //			std::vector<char>* data = response->getResponseData();
        //			log("DEBUG: content:\n [%s] dataLen=%ld", responseContent, data->size());
        
        std::vector<char> *buffer = response->getResponseData();
        std::string content = "";
        for (unsigned int i = 0; i < buffer->size(); i++)
        {
            content += (*buffer)[i];
        }
       
        
        ADResponseGetMail *responseObj = ADResponseGetMail::create();
        responseObj->parseResponse(content);
        
        log("DEBUG: ADResponseGetMail Data:\n%s", responseObj->toString().c_str());
        if(responseObj->getStatus() != "success") {
            CALL_LISTENER(onMailboxLoaded(STATUS_ERROR));
            return;
        }
        
        
        
        // Setup the AstrodogMailbox
        AstrodogMailbox *mailbox = responseObj->getMailbox();
        mMailbox = mailbox;
        
        CALL_LISTENER(onMailboxLoaded(STATUS_SUCCESS));
    };
    
    // Construct Request
    std::string request = "getMailbox?";
	request += "&playerID=" + getPlayerID();
	
    
    sendRequest(request, callback);
}

void AstrodogClient::markMailReceivedToServer(int mailID)
{
    network::ccHttpRequestCallback callback =
    [=](network::HttpClient *client, network::HttpResponse *response){
        
        std::map<std::string, int> meta;
        
        if(response->isSucceed() == false) {
            CALL_LISTENER(onMailReceived(STATUS_ERROR));
            return;
        }
        
        //			const char *responseContent = response->getResponseDataString();
        //			std::vector<char>* data = response->getResponseData();
        //			log("DEBUG: content:\n [%s] dataLen=%ld", responseContent, data->size());
        
        std::vector<char> *buffer = response->getResponseData();
        std::string content = "";
        for (unsigned int i = 0; i < buffer->size(); i++)
        {
            content += (*buffer)[i];
        }
        // log("DEBUG: content:\n [%s]", content.c_str());

        
        this->giveMailAttachment(mailID);
    };

    
    std::string request = "receiveMail?";
	request += "&playerID=" + getPlayerID();
    request += "&mailID=" + INT_TO_STR(mailID);
    
    sendRequest(request, callback);
}


void AstrodogClient::getRanking(AstrodogLeaderboardType type, int planet)
{
	network::ccHttpRequestCallback callback =
		[=](network::HttpClient *client, network::HttpResponse *response){
			
			std::map<std::string, int> meta;
			
			if(response->isSucceed() == false) {
				CALL_LISTENER(onGetRanking(STATUS_ERROR, type, planet, meta));
				return;
			}
			
//			const char *responseContent = response->getResponseDataString();
//			std::vector<char>* data = response->getResponseData();
//			log("DEBUG: content:\n [%s] dataLen=%ld", responseContent, data->size());
			
			std::vector<char> *buffer = response->getResponseData();
			std::string content = "";
			for (unsigned int i = 0; i < buffer->size(); i++)
			{
				content += (*buffer)[i];
			}
			// log("DEBUG: content:\n [%s]", content.c_str());
			
			ADResponseGetRanking *responseObj = ADResponseGetRanking::create();
			responseObj->parseResponse(content);

			log("DEBUG: ADResponseGetRanking Data:\n%s", responseObj->toString().c_str());
			if(responseObj->getStatus() != "success") {
				CALL_LISTENER(onGetRanking(STATUS_ERROR, type, planet, meta));
				return;
			}
			

			
			// Setup the leaderboard
			AstrodogLeaderboard *board = responseObj->getLeaderboard();
			
			// Get the previous ranking before update to leaderboard
			int lastKnownPos = AstrodogClient::instance()->getRankPosition(type, planet);	// also give the week
			
			// Set local and Save to cache
			setLeaderboard(board);
			saveLeaderboard(board);
			
			// Rank UP or down information
			int newPos = AstrodogClient::instance()->getRankPosition(type, planet);
			
			AstrodogRankStatus rankStatus = calculateRankStatus(lastKnownPos, newPos);
			meta["rankStatus"] = rankStatus;
			meta["oldRank"] = lastKnownPos;
			meta["newRank"] = newPos;
			
			CALL_LISTENER(onGetRanking(STATUS_SUCCESS, type, planet, meta));
		};
	
	// Construct Request
	std::string request = "getRanking?";
	
	request += "planet=" + INT_TO_STR(planet);
	request += "&type=" + INT_TO_STR(type);
	request += "&playerID=" + getPlayerID();
	
	if(AstrodogLeaderboardFriend == type) {
		if(isFBConnected()){
			for(FBFriend* fd : mFriendList){
				request += "&fbList=" + fd->getFbID();
			}
		}
	} else if(AstrodogLeaderboardCountry == type) {
		request += "&country=" + DeviceHelper::getCountryCode();
	}
	
	sendRequest(request, callback);
}

void AstrodogClient::submitScore(int planetID, int score)
{
	// Define Callback
	network::ccHttpRequestCallback callback =
	[=](network::HttpClient *client, network::HttpResponse *response){
		if(response->isSucceed() == false) {
			CALL_LISTENER(onSubmitScore(STATUS_ERROR));
			return;
		}

		// Response Content
		std::vector<char> *buffer = response->getResponseData();
		std::string content = "";
		for (unsigned int i = 0; i < buffer->size(); i++)
		{
			content += (*buffer)[i];
		}

		// Parsing the response
		ADResponseSubmitScore *responseObj = ADResponseSubmitScore::create();
		responseObj->parseResponse(content);
		
		//
		if(responseObj->getStatus() != "success") {
			CALL_LISTENER(onSubmitScore(STATUS_ERROR));
			return;
		}
		
		// Register New user (related input: isNewUser=1)
		if(responseObj->getIsNewUser()) {
			updateUser(responseObj->getUser());
		}
		
		
		CALL_LISTENER(onSubmitScore(STATUS_SUCCESS));	// TODO
	};
	
	// Construct Request
	std::string request = "submitScore?";
	
	
	request += "&score=" + INT_TO_STR(score);
	request += "&planet=" + INT_TO_STR(planetID);
	
	if(isRegistered()) {
		request += "&isNewUser=0";
		request += "&playerID=" + getPlayerID();
	} else {
		request += "&isNewUser=1";
		request += "&country=" + DeviceHelper::getCountryCode();
	}
	log("DEBUG: request=%s", request.c_str());
	
	
	// Send it out 
	sendRequest(request, callback);
}


void AstrodogClient::updateUser(AstrodogUser *user)
{
	if(user == nullptr) {
		log("updateUser: user is null");
		return;
	}
	
    
	setUser(user);
	log("DEBUG: updateUser: %s", getUser()->toString().c_str());
	saveUser();
	// TODO: Save the UserProfile
}

std::string AstrodogClient::getPlayerID()
{
	return getUser() == nullptr ? "" : getUser()->getPlayerID();
}

int AstrodogClient::getUserID()
{
	return getUser() == nullptr ? 0 : getUser()->getUid();
}

bool AstrodogClient::isRegistered()
{
	return getUser() != nullptr;
}

//AstrodogLeaderboard *AstrodogClient::getLeaderboard(AstrodogLeaderboardType type)
//{
//	switch (type) {
//		case AstrodogLeaderboardCountry	: return mCountryBoard;
//		case AstrodogLeaderboardFriend	: return mFriendBoard;
//		default							: return mGlobalBoard;
//	}
//}
//

//void AstrodogClient::setLeaderboard(AstrodogLeaderboard *leaderboard)
//{
//	AstrodogLeaderboard *board = getLeaderboard(leaderboard->getType());
//	
//	board->setType(leaderboard->getType());
//	board->setWeek(leaderboard->getWeek());
//	board->setPlanet(leaderboard->getPlanet());
//	board->setRemainTime(leaderboard->getRemainTime());
//	
//	board->recordList.clear();
//	
//	for(AstrodogRecord *record : leaderboard->recordList) {
//		board->addRecord(record);
//	}
//}

bool AstrodogClient::isMe(const std::string &otherPlayerID)
{
	//log("DEBUG: getPlayer=%s other=%s", getPlayerID().c_str(), otherPlayerID.c_str());
	return getPlayerID() == otherPlayerID;
}

std::string AstrodogClient::toString()
{
	std::string resultStr = "";
	
	
	resultStr += "User: " + (mUser == nullptr ? "" : mUser->toString());
	resultStr += "\n";
	
	//AstrodogLeaderboard *leaderboard;
	
//	resultStr += "Global Board\n";
//
//	leaderboard = getLeaderboard(AstrodogLeaderboardGlobal);
//	if(leaderboard == nullptr) {
//		resultStr += "[Leadboard not found]";
//	} else {
//		resultStr += leaderboard->toString();
//	}
	
	
	return resultStr;
}


#pragma mark - Facebook Connect
bool AstrodogClient::isFBConnected()
{
	AstrodogUser *user = getUser();
	if(user == nullptr) {
		return false;
	}
	
	if(user->getFbID() == "") {
		return false;
	}
	
    return sdkbox::PluginFacebook::isLoggedIn();
//	std::string fbID = this->getUser()->getFbID();
//	
//	return fbID != "";
}

void AstrodogClient::disconnectFB()
{
	sdkbox::PluginFacebook::logout();
}

void AstrodogClient::connectFB()
{
	std::vector<std::string> permissions;
	permissions.push_back(sdkbox::FB_PERM_READ_PUBLIC_PROFILE);
	permissions.push_back(sdkbox::FB_PERM_READ_USER_FRIENDS);
	permissions.push_back(sdkbox::FB_PERM_READ_EMAIL);
	
	sdkbox::PluginFacebook::login(permissions);
	//    FacebookListener* listener = new FacebookListener();
	
}

void AstrodogClient::fetchMyProfile()
{
	sdkbox::FBAPIParam params;
	std::string url = "/me";
	sdkbox::PluginFacebook::api(url, "GET", params, url);
}

void AstrodogClient::onFetchMyProfile(const std::string& jsonData)
{
	rapidjson::Document doc;
	doc.Parse(jsonData.c_str());
	
	std::string myName = aurora::JSONHelper::getString(doc, "name", "");
	std::string myID = aurora::JSONHelper::getString(doc, "id", "");
	
	log("MyName=[%s] myID=[%s]", myName.c_str(), myID.c_str());
	

	
	submitFBInfo(myName, myID);
}

void AstrodogClient::onAPI(const std::string& tag, const std::string& jsonData)
{
	log("##FB onAPI: tag -> %s, json -> %s", tag.c_str(), jsonData.c_str());
	
	if("/me" == tag) {
		onFetchMyProfile(jsonData);
	}
	
}

void AstrodogClient::onLogin(bool isLogin, const std::string& msg)
{
    log("onLoggedIn: isLogin=%d, msg=\n[%s]\n", isLogin, msg.c_str());

	// note: See the onGetUSerInfo, it is trigger for the 1st time facebook login
	// bool isLoggedIn = sdkbox::PluginFacebook::isLoggedIn();
	
		//std::string fbID = sdkbox::PluginFacebook::getUserID();
	//std::string token = sdkbox::PluginFacebook::getAccessToken();  // TO BE USE
	//std::string name = sdkbox::PluginFacebook::get
	//sdkbox::PluginFacebook::

	log("Facebook SDK version: %s", sdkbox::PluginFacebook::getSDKVersion().c_str());
	
	//submitFBInfo(<#const std::string &fbName#>, <#const std::string &fbid#>)
	fetchMyProfile();
	
	// find friends 
    fetchFriends();
}

void AstrodogClient::onGetUserInfo( const sdkbox::FBGraphUser& userInfo )
{
	// ken: using fetchMyProfile() now
	
//    std::string fbid;
//    std::string name;
//    mFriendList.clear();
//    
//    log("Facebook id:'%s' name:'%s' ",
//        userInfo.getUserId().c_str(),
//        userInfo.getName().c_str()
//        //       userInfo.getFirstName().c_str(),
//        //       userInfo.getLastName().c_str(),
//        //        userInfo.getEmail().c_str(),
//        //        userInfo.isInstalled ? 1 : 0
//        );
//    fbid = userInfo.getUserId();
//    name = userInfo.getName();
//    
//    submitFBInfo(name,fbid);
	
}

void AstrodogClient::fetchFriends()
{
    sdkbox::PluginFacebook::fetchFriends();
}

void AstrodogClient::onFetchFriends(bool ok, const std::string& msg)
{
    mFriendList.clear();
    const std::vector<sdkbox::FBGraphUser>& friends = sdkbox::PluginFacebook::getFriends();
    for (int i = 0; i < friends.size(); i++)
    {
        const sdkbox::FBGraphUser& user = friends.at(i);
        log("##FB> -------------------------------");
        log("##FB>> %s", user.uid.data());
        log("##FB>> %s", user.firstName.data());
        log("##FB>> %s", user.lastName.data());
        log("##FB>> %s", user.name.data());
        log("##FB>> %s", user.isInstalled ? "app is installed" : "app is not installed");
        log("##FB");
        
        FBFriend* fbFriend = FBFriend::create();
        fbFriend->setName(user.name);
        fbFriend->setFbID(user.uid);
        fbFriend->retain();
        fbFriend->setInstalled(user.isInstalled);
        
        mFriendList.push_back(fbFriend);
    }
	
	
    saveFriendList();
}

bool AstrodogClient::handleRegisteredPlayer(const std::string &fbName, const std::string &fbid)
{
	if(isRegistered()) {
		return false;
	}
	
	AstrodogUser *user = getUser();
	if(user == nullptr) {
		return false;
	}
	
	if(fbid != user->getPlayerID()) {
		return false;
	}
	
	user->setFbID(fbid);
	user->setName(fbName);
	saveUser();
	CALL_LISTENER(onFBConnect(STATUS_SUCCESS));	// TODO
	
	return true;
}

void AstrodogClient::submitFBInfo(const std::string &fbName, const std::string &fbid)
{
	
	// when the user already regisrer
	if(handleRegisteredPlayer(fbName, fbid)) {
		return;
	}
	
	// Sending to Server
	network::ccHttpRequestCallback callback =
	[=](network::HttpClient *client, network::HttpResponse *response){
		if(response->isSucceed() == false) {
			log("connectFB:onGetUserInfo:err: %s",response->getErrorBuffer());
			CALL_LISTENER(onFBConnect(STATUS_ERROR));
			return;
		}
		
		// Response Content
		std::vector<char> *buffer = response->getResponseData();
		std::string content = "";
		for (unsigned int i = 0; i < buffer->size(); i++)
		{
			content += (*buffer)[i];
		}
		
		// Parsing the response
		ADResponseFBConnect *responseObj = ADResponseFBConnect::create();
		responseObj->parseResponse(content);
		
		//
		if(responseObj->getStatus() != "success") {
			CALL_LISTENER(onFBConnect(STATUS_ERROR));
			return;
		}
		
		if(responseObj->getUser() == nullptr) {
			CALL_LISTENER(onFBConnect(STATUS_ERROR));
			return;
		}
		
		// Update or Register the user
		if(responseObj->getUser() != nullptr) {
			updateUser(responseObj->getUser());
		}
        
		
		CALL_LISTENER(onFBConnect(STATUS_SUCCESS));	// TODO
	};
	
	// Construct Request
	std::string request = "connectFB?";
	
	//std::string testname = "這是測試";
	std::string encodedName = UriEncode(fbName);
	//std::string encodedName = UriEncode(testname);
//	char *encodedName = URLHelper::url_encode(fbName.c_str());
	
	
	
	request += "&fbid=" + fbid;
	request += "&name=" + encodedName;
	request += "&country=" + DeviceHelper::getCountryCode();
	
	if(isRegistered()) {
		request += "&isNewUser=0";
		request += "&playerID=" + getPlayerID();
	} else {
		request += "&isNewUser=1";
		
	}
	log("DEBUG: %s", request.c_str());
	
	sendRequest(request, callback);	
}

#pragma mark - Leaderboard
void AstrodogClient::setLeaderboard(AstrodogLeaderboard *leaderboard)
{
	if(leaderboard == nullptr) {
		log("setLeaderboard: leaderboard is null");
		return;
	}
	
	std::string key = leaderboard->getKey();
	
	mLeaderboardMap.insert(key, leaderboard);
}

AstrodogLeaderboard *AstrodogClient::getLeaderboard(AstrodogLeaderboardType type, int planet)
{
	std::string key = AstrodogLeaderboard::getLeaderboardKey(type, planet);
	
	return getLeaderboardByKey(key);
}

AstrodogLeaderboard *AstrodogClient::getLeaderboardByKey(const std::string &key)
{
	if(mLeaderboardMap.find(key) == mLeaderboardMap.end()) {
		return nullptr;	// no leaderboard found
	}
	
	return mLeaderboardMap.at(key);
}

std::string AstrodogClient::infoLeaderboard()
{
	std::string result = "";
	
	//
	result += StringUtils::format("Leaderboard count: %ld\n", mLeaderboardMap.size());
	
	
	//
	std::vector<std::string> mapKeyVec = mLeaderboardMap.keys();
	
	for(auto key : mapKeyVec)
	{
		AstrodogLeaderboard *leaderboard = mLeaderboardMap.at(key);
		
		result += leaderboard->toString() + "\n";
	}
	
	return result;
}


void AstrodogClient::saveLeaderboard(AstrodogLeaderboard *leaderboard)
{
	if(leaderboard == nullptr) {
		log("saveLeaderboard: leaderboard is null");
		return;
	}
	
	std::string key = leaderboard->getKey();
	std::string profileKey = kKeyLeaderboard + "." + key;
	
	std::string json = leaderboard->toJSONContent();
	
	saveData(profileKey, json);
}

int AstrodogClient::getRankPosition(AstrodogLeaderboardType type, int planet)
{
	AstrodogLeaderboard *leaderboard = getLeaderboard(type, planet);
	if(leaderboard == nullptr) {
		return -1;		// no rank yet
	}
	
	for(AstrodogRecord *record : leaderboard->recordList) {
		if(record->getUid() == getUserID()) {
			return record->getRank();
		}
	}
	
	return -1;
}

void AstrodogClient::loadAllLeaderboard()
{
	std::vector<AstrodogLeaderboardType> typeList;
	typeList.push_back(AstrodogLeaderboardGlobal);
	typeList.push_back(AstrodogLeaderboardCountry);
	typeList.push_back(AstrodogLeaderboardFriend);
	
	for(AstrodogLeaderboardType type : typeList) {
		for(int planet : mPlanetList) {
			loadLeaderboard(type, planet);
		}
	}
}

void AstrodogClient::loadLeaderboard(AstrodogLeaderboardType type, int planet)
{
	std::string key = AstrodogLeaderboard::getLeaderboardKey(type, planet);
	std::string profileKey = kKeyLeaderboard + "." + key;
	
	std::string json = loadData(profileKey);
	
	if(json == "") {
		// note: the given leaderboard haven't save yet
		return;
	}
	
	AstrodogLeaderboard *leaderboard = AstrodogLeaderboard::create();
	leaderboard->parseJSONContent(json);
	
	setLeaderboard(leaderboard);
}

#pragma mark - Local Data Save / Load Handling
void AstrodogClient::resetUser()
{
	setUser(nullptr);
	
	disconnectFB();
	
	saveData(kKeyUser, "");
}

void AstrodogClient::resetUserFbData()
{
    if(getUser()==nullptr){
        return;
    }
    
    AstrodogUser *user = getUser();
    user->setFbID("");
    user->setName("");
    
    disconnectFB();
    
    saveUser();
}

void AstrodogClient::saveFriendList()
{
    if(mFriendList.empty()){
        return;
    }
    
    std::string dataToSave = "";
    for(FBFriend* fd: mFriendList){
        std::string content = fd->toJSONContent() + "\n";
        dataToSave += content;
    }
    
    saveData(kKeyFriendList, dataToSave);
}

void AstrodogClient::loadFriendList()
{
    std::string json = loadData(kKeyFriendList);
    if(json == "") {
        mFriendList.clear();
        return;
    }
    
    std::stringstream in(json);
    std::string out;
    
    while(std::getline(in, out, '\n')){
        FBFriend* fbFriend = FBFriend::create();
        fbFriend->parseJSONContent(out);
        fbFriend->retain();
        log("%s",fbFriend->toString().c_str());
        mFriendList.push_back(fbFriend);
    }
}


void AstrodogClient::saveUser()
{
	if(getUser() == nullptr) {
		return;		// Nothing to save
	}
	
	std::string content = getUser()->toJSONContent();
	log("AstrodogClient: saveUser:\n%s", content.c_str());
	saveData(kKeyUser, content);
}

void AstrodogClient::loadUser()
{
	std::string json = loadData(kKeyUser);
	if(json == "") {
		setUser(nullptr);
		return;
	}
	
	log("AstrodogClient: loadUser:\n%s", json.c_str());
	
	AstrodogUser *user = AstrodogUser::create();
	user->parseJSONContent(json);
    //user->setUid(1095);   //hardcoded for testing
    
	if(user->getPlayerID() == "") {
		setUser(nullptr);		// user not exist yet
	} else {
		setUser(user);
	}
}

void AstrodogClient::saveData(const std::string &key, const std::string &content)
{
	
	//
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("AstrodogClient::saveData: userDefault not available. dataKey=%s", key.c_str());
		return;
	}
	
	userData->setStringForKey(USER_DEFAULT_KEY(key), content);
	userData->flush();
}

std::string AstrodogClient::loadData(const std::string &key)
{
	UserDefault *userData = UserDefault::getInstance();
	if(userData == nullptr) {
		log("AstrodogClient.loadData: userDefault not available. dataKey=%s", key.c_str());
		return "";
	}
	
	//log("UserKey=%s", USER_DEFAULT_KEY(key));
	
	
	return userData->getStringForKey(USER_DEFAULT_KEY(key), "");
}

#pragma mark - Core
void AstrodogClient::setup()
{
	
	//sdkbox::PluginFacebook::setAppId("1810253282635241");
	
	loadUser();
    loadFriendList();
	loadAllLeaderboard();
}

std::string AstrodogClient::getCurrentServerURL()
{
	return getServerURL(mServer);
}


// Return the server with port
//		e.g localhost:3000
//		e.g aurorainnoapps:3000


std::string AstrodogClient::getServerURL(AstrodogServer server)
{
	switch (server) {
		case AstrodogServerProduction	: return "aurorainnoapps.com:4591";
		default							: return "127.0.0.1:4591";
      //  default							: return "192.168.0.103:3000";
	}
}

void AstrodogClient::setServer(AstrodogServer server)
{
	mServer = server;
}

std::string AstrodogClient::getUserInfo()
{
	std::string result = "";
	
	result += "user=[";
	if(getUser() == nullptr) {
		result += "notRegister";
	} else {
		result += getUser()->toString();
	}
	result += "]";
	
	return result;
}



#pragma mark - Mailbox
void AstrodogClient::loadSampleMailData()
{
	mMailbox->setupSampleData();
}


void AstrodogClient::giveMailAttachment(int mailID)		// call when server said 'yes'
{
	AstrodogMail *mail = mMailbox->getMailByID(mailID);
	if(mail == nullptr) {
		log("AstrodogClient: fail to find mail");
		CALL_LISTENER(onMailReceived(STATUS_ERROR));
		return;
	}
	
	// Give the attachment
	MoneyType moneyType = MoneyTypeUnknown;
	
	AstrodogAttachment attachment = mail->getAttachment();
	
	if(AstrodogAttachment::TypeDiamond == attachment.type) {
		// Give Diamond
        GameManager::instance()->addMoney(MoneyTypeDiamond, attachment.amount);
		moneyType = MoneyTypeDiamond;
	} else if(AstrodogAttachment::TypeStar == attachment.type) {
		// Give Star
		GameManager::instance()->addMoney(MoneyTypeStar, attachment.amount);
		moneyType = MoneyTypeStar;
    } else if(AstrodogAttachment::TypePuzzle == attachment.type){
        PlayerManager::instance()->addPlayerFragment(attachment.itemID, attachment.amount);
		moneyType = MoneyTypePuzzle;
    }
	
	if(MoneyTypeUnknown != moneyType) {
		Analytics::instance()->logCollect(Analytics::from_mail, attachment.amount, moneyType);
	}
	
	// Mark as received
	mail->setStatus(AstrodogMail::StatusReceived);
	
	CALL_LISTENER(onMailReceived(STATUS_SUCCESS));
}

void AstrodogClient::receivedMail(int mailID)			// will call server
{
    markMailReceivedToServer(mailID);
	// TODO: giveMailAttachment
//	giveMailAttachment(mailID);
}

void AstrodogClient::loadMailbox()						// Will call server
{
	// TODO
//	loadSampleMailData();
//	
//	CALL_LISTENER(onMailboxLoaded(STATUS_SUCCESS));
    if(isRegistered()){
        loadMailboxFromServer();
    }else{
        log("user is not registered, assume empty mailbox!");
        if(!mMailbox){
            mMailbox = new AstrodogMailbox();
        }
        CALL_LISTENER(onMailboxLoaded(STATUS_SUCCESS));
    }
}

AstrodogMailbox *AstrodogClient::getMailbox()
{
	return mMailbox;
}

bool AstrodogClient::isMailboxLoaded()
{
	return mMailboxLoaded;
}


#pragma mark - Mock Data
AstrodogLeaderboard *AstrodogClient::getMockLeaderboard()
{
	AstrodogLeaderboard *board = AstrodogLeaderboard::create();
//	
//	CC_SYNTHESIZE(AstrodogLeaderboardType, mType, Type);
//	CC_SYNTHESIZE(int, mPlanet, Planet);
//	CC_SYNTHESIZE(std::string, mWeek, Week);
//	CC_SYNTHESIZE(float, mRemainTime, RemainTime);	// remain time
//	CC_SYNTHESIZE(long long, mEndTime, EndTime);	// remain time
//	
//	void addRecord(AstrodogRecord *record);
//	Vector<AstrodogRecord *> getRecordList();
//	
//	Vector<AstrodogRecord *> recordList;
//	
//	std::str
	
	
	return board;
	
}


