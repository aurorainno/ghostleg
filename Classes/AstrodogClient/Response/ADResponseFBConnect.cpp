//
//  ADResponseFBConnect.cpp
//  GhostLeg
//
//  Created by Calvin on 10/1/2017.
//
//

#include "ADResponseFBConnect.h"

#include "JSONHelper.h"
#include "AstrodogData.h"

ADResponseFBConnect::ADResponseFBConnect()
: ADResponse()
, mUser(nullptr)
, mIsNewUser(0)
{
    
}


std::string ADResponseFBConnect::toString()
{
    std::string result = ADResponse::toString();
	
	result += StringUtils::format(" newUser=%d", mIsNewUser);
	result += StringUtils::format(" user=[%s]",
						(mUser == nullptr ? "null" : mUser->toString().c_str()));
	
    return result;
}


bool ADResponseFBConnect::parseJSON(const rapidjson::Value &jsonValue)
{
    log("Start parsing json");
    
    // check to setStatus
    ADResponse::parseJSON(jsonValue);
    
    //	int uid = aurora::JSONHelper::getInt(jsonValue, "uid", 0);
	AstrodogUser *user = parseUser(jsonValue["user"]);
	setUser(user);
	
    mIsNewUser = aurora::JSONHelper::getBool(jsonValue, "isNewUser", false);
	
	return true;
}
