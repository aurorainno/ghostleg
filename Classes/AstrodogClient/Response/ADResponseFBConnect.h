//
//  ADResponseFBConnect.hpp
//  GhostLeg
//
//  Created by Calvin on 10/1/2017.
//
//

#ifndef ADResponseFBConnect_hpp
#define ADResponseFBConnect_hpp

#include <stdio.h>
#include "ADResponse.h"

class AstrodogUser;

class ADResponseFBConnect : public ADResponse
{
public:
    CREATE_FUNC(ADResponseFBConnect);
    
public:
    ADResponseFBConnect();

	CC_SYNTHESIZE(AstrodogUser *, mUser, User);
    CC_SYNTHESIZE(int, mUid, Uid);
    CC_SYNTHESIZE(std::string , mFbId, FbId);
    
    CC_SYNTHESIZE(bool, mIsNewUser, IsNewUser);
    CC_SYNTHESIZE(std::string, mName, Name);
    CC_SYNTHESIZE(std::string, mCountry, Country);
    
    virtual std::string toString();
    
    virtual bool parseJSON(const rapidjson::Value &jsonValue);	// parsing
};

#endif /* ADResponseFBConnect_hpp */
