//
//  ADResponse.cpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#include "ADResponse.h"
#include "JSONHelper.h"

#define kDefaultCountryCode "US"

ADResponse::ADResponse()
{
	
}


void ADResponse::parseResponse(const std::string &json)
{
	// log("RESPONSE JSON:\n%s\n", json.c_str());
	parseJSONContent(json);
}

AstrodogUser *ADResponse::parseUser(const rapidjson::Value &json)
{
	AstrodogUser *record = AstrodogUser::create();
	
	record->setUid(aurora::JSONHelper::getInt(json, "uid", 0));
	record->setName(aurora::JSONHelper::getString(json, "name", "Player"));
	record->setPlayerID(aurora::JSONHelper::getString(json, "playerID", ""));
	record->setFbID(aurora::JSONHelper::getString(json, "fbid", ""));
	record->setCountry(aurora::JSONHelper::getString(json, "country", kDefaultCountryCode));
	
	return record;
}

AstrodogRecord *ADResponse::parseRecord(const rapidjson::Value &json)
{
	AstrodogRecord *record = AstrodogRecord::create();

	record->setUid(aurora::JSONHelper::getInt(json, "uid", 0));
	record->setFbID(aurora::JSONHelper::getString(json, "fbid", ""));
	record->setPlayerID(aurora::JSONHelper::getString(json, "playerID", ""));
	record->setName(aurora::JSONHelper::getString(json, "name", "Player"));
	record->setRank(aurora::JSONHelper::getInt(json, "rank", 999));
	record->setScore(aurora::JSONHelper::getInt(json, "score", 777));
	record->setCountry(aurora::JSONHelper::getString(json, "country", kDefaultCountryCode));
	
	return record;
}

AstrodogMail *ADResponse::parseMail(const rapidjson::Value &json)
{
    AstrodogMail *mail = AstrodogMail::create();
    
    mail->parseJSON(json);
    
    return mail;
}

AstrodogLeaderboard *ADResponse::parseLeaderboard(const rapidjson::Value &json)
{
	AstrodogLeaderboard *leaderboard = AstrodogLeaderboard::create();
	
	AstrodogLeaderboardType type = (AstrodogLeaderboardType) aurora::JSONHelper::getInt(json, "type", 1);
	
	leaderboard->setType(type);
	
	std::string week = aurora::JSONHelper::getString(json, "week", "");
	leaderboard->setWeek(week);
	leaderboard->setPlanet(aurora::JSONHelper::getInt(json, "planetID", 1));
	leaderboard->setEndTime(aurora::JSONHelper::getLongLong(json, "endTime", 0));
	
	return leaderboard;
}


std::string ADResponse::toString()
{
	return "status=" + mStatus;
}

bool ADResponse::parseJSON(const rapidjson::Value &jsonValue)
{
	setStatus(aurora::JSONHelper::getString(jsonValue, "status", "fail"));
	
	return true;
}



