//
//  ADResponse.hpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#ifndef ADResponse_hpp
#define ADResponse_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "JSONData.h"
#include "AstrodogData.h"

USING_NS_CC;

class ADResponse : public Ref, public JSONData
{
public:
	ADResponse();
	
	virtual bool init() { return true; }
	
	CC_SYNTHESIZE(std::string, mStatus, Status);	// success or error message
	
	
	virtual void parseResponse(const std::string &json);
	virtual std::string toString();
	
	
	//
	AstrodogLeaderboard *parseLeaderboard(const rapidjson::Value &json);
	AstrodogRecord *parseRecord(const rapidjson::Value &json);
    AstrodogMail *parseMail(const rapidjson::Value &json);
	AstrodogUser *parseUser(const rapidjson::Value &json);
	
#pragma mark - JSON Data
public: // JSON
	virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue) {}	// No need
	virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
};

#endif /* ADResponse_hpp */
