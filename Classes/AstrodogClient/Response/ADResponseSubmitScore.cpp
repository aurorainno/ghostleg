//
//  ADResponseSubmitScore.cpp
//  GhostLeg
//
//  Created by Ken Lee on 10/1/2017.
//
//

#include "ADResponseSubmitScore.h"
#include "AstrodogData.h"
#include "JSONHelper.h"

ADResponseSubmitScore::ADResponseSubmitScore()
: ADResponse()
, mUser(nullptr)
{
	
}


std::string ADResponseSubmitScore::toString()
{
	std::string result = ADResponse::toString();
	
    result += StringUtils::format(" newUser=%d", mIsNewUser);
    result += StringUtils::format(" change=%d", mAnyChange);
    result += StringUtils::format(" oldScore=%d", mOldScore);
    result += StringUtils::format(" score=%d", mScore);
	result += " user=[" + (mUser == nullptr ? "" : mUser->toString()) + "]";
	
	return result;
}


bool ADResponseSubmitScore::parseJSON(const rapidjson::Value &jsonValue)
{
	log("Start parsing json");
	
	// check to setStatus
	ADResponse::parseJSON(jsonValue);
	
//	int uid = aurora::JSONHelper::getInt(jsonValue, "uid", 0);
	
    if(jsonValue.HasMember("user")){
		AstrodogUser *user = parseUser(jsonValue["user"]);
		setUser(user);
    }
    
    mIsNewUser = aurora::JSONHelper::getBool(jsonValue, "isNewUser", 0);
    mAnyChange = aurora::JSONHelper::getBool(jsonValue, "anyChange", false);
    mOldScore = aurora::JSONHelper::getInt(jsonValue, "oldScore", 0);
    mScore = aurora::JSONHelper::getInt(jsonValue, "submitScore", 0);
	
	return true;
}
