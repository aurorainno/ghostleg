//
//  ADResponseSubmitScore.hpp
//  GhostLeg
//
//  Created by Ken Lee on 10/1/2017.
//
//

#ifndef ADResponseSubmitScore_hpp
#define ADResponseSubmitScore_hpp

#include <stdio.h>
#include "ADResponse.h"

class AstrodogUser;

class ADResponseSubmitScore : public ADResponse
{
public:
	CREATE_FUNC(ADResponseSubmitScore);
	
public:
	ADResponseSubmitScore();
 
    CC_SYNTHESIZE(bool , mAnyChange, AnyChange);
    CC_SYNTHESIZE(int, mOldScore, OldScore);
    CC_SYNTHESIZE(int, mScore, Score);
    CC_SYNTHESIZE(bool, mIsNewUser, IsNewUser);
	
	CC_SYNTHESIZE_RETAIN(AstrodogUser *, mUser, User);
	
	virtual std::string toString();
	
	virtual bool parseJSON(const rapidjson::Value &jsonValue);	// parsing 
};


#endif /* ADResponseSubmitScore_hpp */
