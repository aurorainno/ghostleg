//
//  ADResponseGetRanking.cpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#include "ADResponseGetRanking.h"
#include "JSONHelper.h"

ADResponseGetRanking::ADResponseGetRanking()
: ADResponse()
, mLeaderboard(nullptr)
{
	
}



std::string ADResponseGetRanking::toString()
{
	std::string result = ADResponse::toString() + " ";
	
	result += getLeaderboard() == nullptr ? "not ready" : getLeaderboard()->toString() + "\n";
	return result;
}


bool ADResponseGetRanking::parseJSON(const rapidjson::Value &jsonValue)
{
	log("Start parsing json");
	
	ADResponse::parseJSON(jsonValue);	// Common response data
	
	
	// Parse the leaderboard data
	//const rapidjson::Value &leaderboardJSON =
	
	std::string name;
	AstrodogLeaderboard *leaderboard;
	
	name = "leaderboard";
	if(jsonValue.HasMember(name.c_str()) == false) {
		log("fail to find the member=%s", name.c_str());
		return false;
	}
	leaderboard = parseLeaderboard(jsonValue[name.c_str()]);
	setLeaderboard(leaderboard);
	
	log("ADGetRanking: leaderboard is null ? %d", leaderboard == nullptr);

	// Parse the list of record
	name = "records";
    
    if(jsonValue.HasMember(name.c_str()) == false) {
        log("fail to find the member=%s", name.c_str());
        return false;
    }
    
	leaderboard->recordList.clear();
	const rapidjson::Value &arrayValue = jsonValue[name.c_str()];
	
	if(arrayValue.IsArray()) {
		for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
		{
			const rapidjson::Value &value = arrayValue[i];
			AstrodogRecord *record = parseRecord(value);
			leaderboard->addRecord(record);
		}
	}
	
	return true;
}
