//
//  ADResponseGetRanking.hpp
//  GhostLeg
//
//  Created by Ken Lee on 9/1/2017.
//
//

#ifndef ADResponseGetRanking_hpp
#define ADResponseGetRanking_hpp

#include <stdio.h>
#include "AstrodogData.h"
#include "ADResponse.h"

class ADResponseGetRanking : public ADResponse
{
public:
	CREATE_FUNC(ADResponseGetRanking);
	
public:
	ADResponseGetRanking();
	
	
	CC_SYNTHESIZE_RETAIN(AstrodogLeaderboard *, mLeaderboard, Leaderboard);

	virtual std::string toString();

	virtual bool parseJSON(const rapidjson::Value &jsonValue);	
};

#endif /* ADResponseGetRanking_hpp */
