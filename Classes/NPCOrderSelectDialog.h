//
//  NPCOrderSelectDialog.hpp
//  GhostLeg
//
//  Created by Calvin on 14/3/2017.
//
//

#ifndef NPCOrderSelectDialog_hpp
#define NPCOrderSelectDialog_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class NPCOrderItemView;
class NPCOrder;

class NPCOrderSelectDialogListener {
public:
    virtual void onNPCOrderSelected(NPCOrder* order){}
    virtual void onRefreshNPC(){}
    virtual void onDialogClosed(){}
};


class NPCOrderSelectDialog : public Layer
{
public:
    
    CREATE_FUNC(NPCOrderSelectDialog);
    
    NPCOrderSelectDialog();
    virtual bool init() override;
    
    void onExit()override;
    void onEnter()override;
    
    void setupUI(Node* node);
    
    void setItemView(Vector<NPCOrder*> orderList);
    void setListener(NPCOrderSelectDialogListener *listenser);
    
    void setupAnimation(const std::string &csbName);
    void runAnimation(const std::string &name);
    
private:
    Button* mChangeBtn;
    Node *mFirstNPCPanel, *mSecondNPCPanel;
    NPCOrderSelectDialogListener *mListener;
    cocostudio::timeline::ActionTimeline *mTimeline;
    
};

#endif /* NPCOrderSelectDialog_hpp */
