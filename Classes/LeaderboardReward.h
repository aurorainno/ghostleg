//
//  LeaderboardReward.hpp
//  GhostLeg
//
//  Created by Calvin on 24/2/2017.
//
//

#ifndef LeaderboardReward_hpp
#define LeaderboardReward_hpp

#include "cocos2d.h"
#include "JSONData.h"
#include "CommonType.h"

#include <stdio.h>
#include <vector>
#include <map>
#include <string>

USING_NS_CC;

class LeaderboardReward : public Ref, public JSONData
{
public:
    CREATE_FUNC(LeaderboardReward);
    LeaderboardReward();
    virtual bool init() { return true; }
    
public: // properties
    CC_SYNTHESIZE(MoneyType, mRewardType, RewardType);
    CC_SYNTHESIZE(int, mAmount, Amount);
    
    
#pragma mark - JSON Data
public: // JSON
    virtual void defineJSON(rapidjson::Document::AllocatorType &allocator, rapidjson::Value &outValue);
    virtual bool parseJSON(const rapidjson::Value &jsonValue);		// JSON DAta -> this object value
    
public:
    
    std::string toString();
};

#endif /* LeaderboardReward_hpp */
