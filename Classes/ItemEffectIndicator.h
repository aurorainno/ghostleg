//
//  ItemEffectIndicator.hpp
//  GhostLeg
//
//  Created by Calvin on 27/1/2017.
//
//

#ifndef ItemEffectIndicator_hpp
#define ItemEffectIndicator_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"
#include "CommonType.h"
//#include "EffectLayer.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class CircularMaskNode;

//ItemEffectShield		= 2,			// note: cannot change the following order
//ItemEffectInvulnerable	= 5,
//ItemEffectTimeStop		= 7,
//ItemEffectDoubleCoins	= 6,
//ItemEffectGrabStar		= 4,		// OLD style grab every N second (passive)
//ItemEffectMissile		= 3,
//ItemEffectStarMagnet	= 8,
//ItemEffectSnowball		= 9,
//ItemEffectSpeedUp       = 10,
//ItemEffectCashOut       = 11,


class ItemEffectIndicator : public Layout
{
public:
	// Note: sync with the value of ItemEffect
    enum EffectType{
        None = 0,
        TimeStop = 7,				// ItemEffectTimeStop	  = 7
		Shield = 5,					// ItemEffectInvulnerable = 5
        Magnet = 8,					// ItemEffectStarMagnet	  = 8
        SpeedUp = 10,				// ItemEffectSpeedUp      = 10,
        CashOut = 11,				// ItemEffectCashOut      = 11,
    };
    
    CREATE_FUNC(ItemEffectIndicator);
    
    ItemEffectIndicator();
    
    virtual bool init() override;
    void setupUI(Node* rootNode);
    
    void config(EffectType type, float time);
    void activate();
    
    void update(float delta);
    
    EffectType getEffectType();
    
    bool isMatchWithEffect(int type);
    //bool isMatchWithEffect(EffectLayer::PowerUpType type);
    
    void setOnFinishCallback(std::function<void(ItemEffectIndicator*)> callback);

private:
    std::string getIconResByType(EffectType type);
    std::string getCoolDownResByType(EffectType type);

private:
    CircularMaskNode* mCoolDownMask;
    EffectType mMyType;
    Sprite* mIcon;
    std::function<void(ItemEffectIndicator *)> mOnFinishCallback;

    float mRemainTime;
    float mOriTime;

};

#endif /* ItemEffectIndicator_hpp */
