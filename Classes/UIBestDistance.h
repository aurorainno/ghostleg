//
//  UIBestDistance.hpp
//  GhostLeg
//
//  Created by Calvin on 2/12/2016.
//
//

#ifndef UIBestDistance_hpp
#define UIBestDistance_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;
using namespace cocos2d::ui;

class UIBestDistance : public Node
{
public:
    CREATE_FUNC(UIBestDistance);

    UIBestDistance();

    virtual bool init();
   
    void setupGUI(const std::string &csbName);
    void setupAnimation(const std::string &csbName);
    void runAnimation();
    
    void setupBestDistanceText(int planetID);
    
private:
    Text* mBestDistanceText;
    cocostudio::timeline::ActionTimeline* mTimeLine;
};


#endif /* UIBestDistance_hpp */
