//
//  ADResponseGetMail.cpp
//  GhostLeg
//
//  Created by Calvin on 20/2/2017.
//
//

#include "ADResponseGetMail.h"

ADResponseGetMail::ADResponseGetMail()
: ADResponse()
, mMailbox(nullptr)
{
    
}

std::string ADResponseGetMail::toString()
{
    std::string result = ADResponse::toString() + " ";
    result += mMailbox->toString();
    return result;
}


bool ADResponseGetMail::parseJSON(const rapidjson::Value &jsonValue)
{
    log("Start parsing json");
    
    ADResponse::parseJSON(jsonValue);	// Common response data
    
    
    // Parse the leaderboard data
    //const rapidjson::Value &leaderboardJSON =
    
    std::string name;
    AstrodogMailbox *mailbox = AstrodogMailbox::create();
    
    name = "mailList";
    
    if(jsonValue.HasMember(name.c_str()) == false) {
        log("fail to find the member=%s", name.c_str());
        return false;
    }
    
    const rapidjson::Value &arrayValue = jsonValue[name.c_str()];
    
    if(arrayValue.IsArray()) {
        for (rapidjson::SizeType i = 0; i < arrayValue.Size(); i++)
        {
            const rapidjson::Value &value = arrayValue[i];
            AstrodogMail *mail = ADResponse::parseMail(value);
            mailbox->addMail(mail);
        }
    }
    
    setMailbox(mailbox);
    
    return true;
}
