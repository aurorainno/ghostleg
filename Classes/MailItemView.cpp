//
//  MailItemView.cpp
//  GhostLeg
//
//  Created by Calvin on 17/2/2017.
//
//

#include "MailItemView.h"
#include "TimeHelper.h"
#include "RichTextLabel.h"

bool MailItemView::init()
{
    if(Layout::init() == false) {
        return false;
    }
    //
    
    Node* rootNode = CSLoader::createNode("MailItemView.csb");
    setupUI(rootNode);
    addChild(rootNode);
    
    return true;
}


void MailItemView::setupUI(cocos2d::Node *rootNode)
{
    setContentSize(rootNode->getContentSize());
    
 //   mBGImageOn = rootNode->getChildByName<ImageView*>("bg_On");
 //   mBGImageOff = rootNode->getChildByName<ImageView*>("bg_Off");
    mIcon = rootNode->getChildByName<Sprite*>("icon");
    mTitleText = rootNode->getChildByName<Text*>("title");
    
    Text* message = rootNode->getChildByName<Text*>("content");
    RichTextLabel* messageRichText = RichTextLabel::createFromText(message);
    
//    messageRichText->setTextColor(1, kColorYellow);
    
    mMessageText = messageRichText;
    
    rootNode->addChild(mMessageText);
    message->setVisible(false);
    
    Node *rewardPanel = rootNode->getChildByName("rewardPanel");
    mRewardCountText = rewardPanel->getChildByName<Text*>("amount");
    mDiamondSprite = rewardPanel->getChildByName<Sprite*>("diamondSprite");
    mStarSprite = rewardPanel->getChildByName<Sprite*>("starSprite");
    mCharSprite = rewardPanel->getChildByName<Sprite*>("charSprite");
    
    mLargeDiamond = mDiamondSprite;
    mLargeStar = mStarSprite;
    mReceiveBtn = rootNode->getChildByName<Button*>("receiveBtn");
    mDoneBtn = rootNode->getChildByName<Button*>("doneBtn");
    mTimeText = rootNode->getChildByName<Text*>("timeText");
    
    mReceiveBtn->addClickEventListener([&](Ref*){
        if(mOnReceiveCallback){
            mOnReceiveCallback(mMailID);
        }
    });

}

void MailItemView::setOnReceiveCallback(std::function<void (int)> callback)
{
    mOnReceiveCallback = callback;
}


void MailItemView::setView(AstrodogMail *data)
{
    mMailID = data->getMailID();
    
    AstrodogMail::Status status = data->getStatus();
    if(status == AstrodogMail::StatusUnread){
        mReceiveBtn->setVisible(true);
        mDoneBtn->setVisible(false);
    }else{
        mReceiveBtn->setVisible(false);
        mDoneBtn->setVisible(true);
    }
    mTitleText->setString(data->getTitle());
    mMessageText->setString(data->getMessage());
    
    AstrodogAttachment attachment = data->getAttachment();
    if(attachment.type == AstrodogAttachment::TypeDiamond){
        mDiamondSprite->setVisible(true);
        mStarSprite->setVisible(false);
        mCharSprite->setVisible(false);
    }else if(attachment.type == AstrodogAttachment::TypeStar){
        mDiamondSprite->setVisible(false);
        mStarSprite->setVisible(true);
        mCharSprite->setVisible(false);
    }else if(attachment.type == AstrodogAttachment::TypePuzzle){
        mDiamondSprite->setVisible(false);
        mStarSprite->setVisible(false);
        mCharSprite->setVisible(true);
    }
    
    mRewardCountText->setString(StringUtils::format("%d",attachment.amount));
    
    mIcon->setTexture(StringUtils::format("guiImage/ui_inbox_reward_%d.png",data->getIconID()));
    
    long long createTime = data->getCreateTime();
    long long currentTime = TimeHelper::getCurrentTimeStampInMillis();
    
    long long timeDiff = currentTime - createTime;
    if(timeDiff>0){
        map<string,int> result = TimeHelper::parseMillis(timeDiff);
        
        std::string timeStr;
        int days = result["days"];
        int hours = result["hours"];
        int minutes = result["minutes"];
        int seconds = result["seconds"];
        
        if(days>0){
            timeStr = StringUtils::format("%d days ago",days);
        }else if(hours>0){
            timeStr = StringUtils::format("%d hrs ago",hours);
        }else if(minutes>0){
            timeStr = StringUtils::format("%d min ago",minutes);
        }else {
            timeStr = StringUtils::format("%d sec ago",seconds);
        }
        
        mTimeText->setString(timeStr);
    }
    
}


