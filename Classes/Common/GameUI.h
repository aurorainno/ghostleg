//
//  GameUI.hpp
//  GhostLeg
//
//  Created by Ken Lee on 17/5/2016.
//
//

#ifndef GameUI_hpp
#define GameUI_hpp

#include <stdio.h>
#include "cocos2d.h"

USING_NS_CC;

class GameUI
{
public:
	static void addSpaceParallax(Node *parent);
	static void showButtonClickAction(Node *button);
	static void updateStarValue(Node *starValueNode);
};

#endif /* GameUI_hpp */
