//
//  Constant.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#include "Constant.h"


//
const int kZorderStar = 8;
const int kZorderItem = 7;
const int kZorderEnemy = 10;
const int kZorderObstacle = 5;
const int kZorderPlayer = 20;
const int kZorderGUI = 1000;		// ????


//
const int kMaxCharLevel = 5;

const int ModelType::line1 = 1;
const int ModelType::line2 = 2;
const int ModelType::line3 = 3;

const int ModelType::enemyFix = 100;
const int ModelType::enemySlow = 101;
const int ModelType::enemyFast = 102;

// TO BE REMOVED!!!!
const int ModelType::itemCoin = 201;
const int ModelType::itemPotion = 202;
const int ModelType::itemCapsule = 203;

const int kTutorialMapID = 10002;
const int kChaserEnemyID = 1000;


const int kEnemyScore = 10;

const int kMapHeight = 1000;			// 50 Grids * 20 px
const int kMaxReviveCount = 4;

const int kCameraPosition = 250;

const Color4B kColorYellow = Color4B(255, 211, 49, 255);
const Color4B kColorGreen = Color4B(14, 255, 217, 255);

const std::string kAndroidPackage = "com.aurorainno.MonstersEx";
const std::string kFacebookPageLink = "https://www.facebook.com/AuroraInno-Limited-139826236114345/";
const std::string kFeedbackLink = "mailto:info@aurorainno.com";
const std::string kTwitterLink = "https://twitter.com/aurorainno";
//

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
const std::string kVersionCheckUrl = "http://aurorainnoapps.com:9280/static-doc/monsterEx/google.v";
#else
const std::string kVersionCheckUrl = "http://aurorainnoapps.com:9280/static-doc/monsterEx/iphone.v";
#endif

