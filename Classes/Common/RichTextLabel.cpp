//
//  RichTextLabel.cpp
//  BounceDefense
//
//  Created by Calvin on 19/9/2016.
//
//

#include "RichTextLabel.h"
#include "cocos2d.h"
#include "RichTextParser.h"

RichTextLabel::RichTextLabel()
: mDefaultColor(Color4B::WHITE)
, mDefaultHiliColor(Color4B::RED)
, mFontSize(24)
, mFontName("arial.ttf")
{
	
}

bool RichTextLabel::init()
{
    if(!RichText::init()){
        return false;
    }
    
    ignoreContentAdaptWithSize(false);

	return true;
}

void RichTextLabel::setFont(const std::string &font, float fontSize){
    mFontName = font;
    mFontSize = fontSize;
}

RichTextLabel* RichTextLabel::add(const std::string &content){
	Color3B textColor = Color3B(mDefaultColor);
	
    RichElementText *re = RichElementText::create(1, textColor,
						mDefaultColor.a, content, mFontName, mFontSize);
    pushBackElement(re);
    return this;
}

RichTextLabel* RichTextLabel::add(const std::string &content, const cocos2d::Color4B &color){
	Color3B textColor = Color3B(color);
	
	
    RichElementText *re = RichElementText::create(2, textColor, color.a, content, mFontName, mFontSize);
    pushBackElement(re);
	
    return this;
}

RichTextLabel* RichTextLabel::addNewLine()
{
    RichElementNewLine* newline = RichElementNewLine::create(-1, cocos2d::Color3B::WHITE, 255);
    pushBackElement(newline);
    return this;
}

RichTextLabel* RichTextLabel::addVertSpacing(float spacing){
    Node *customSpacing = Node::create();
    customSpacing->setContentSize(cocos2d::Size(getContentSize().width, spacing));
    pushBackElement(RichElementCustomNode::create(-2, cocos2d::Color3B::WHITE, 255, customSpacing));
    addNewLine();
    return this;
}


void RichTextLabel::setTextColor(int idx, const Color4B &color)
{
	mTextColorMap[idx] = color;
}

void RichTextLabel::setString(const std::string &content)
{
	removeAllElement();		/// this is modified by us.
	
	
	RichTextParser parser;
	
	parser.parse(content);
	
	std::vector<RichTextToken> tokenList = parser.getTokenList();
	
	for(RichTextToken token : tokenList) {
		if(RichTextToken::TokenTypeTag == token.type) {
			Color4B color = extractColorFromTokenTag(token.tag);
			
			add(token.content, color);
		} else if(RichTextToken::TokenTypeNewLine == token.type) {
			addNewLine();
		} else {	// Normal Text
			add(token.content);
		}
		
		//log("%s", RichTextParser::getTokenString(token).c_str());
	}
	
}

Color4B RichTextLabel::getTextColorByIndex(int idx)
{
	if(mTextColorMap.find(idx) == mTextColorMap.end()){	// not found
		return mDefaultHiliColor;
	}
	
	return mTextColorMap[idx];
}

Color4B RichTextLabel::extractColorFromTokenTag(const std::string &tag)
{
	int colorIdx;
	sscanf(tag.c_str(), "c%d", &colorIdx);
	
	return getTextColorByIndex(colorIdx);
}


RichTextLabel *RichTextLabel::createFromText(Text *text)
{
	
	std::string fontName = text->getFontName();
	float fontSize = text->getFontSize();
	Color4B textColor = text->getTextColor();
	float scale = text->getScale();
	Size contentSize = text->getContentSize();
	Vec2 anchorPoint = text->getAnchorPoint();
	
	RichTextLabel *richText = RichTextLabel::create();

	richText->setFont(fontName, fontSize);
	richText->setDefaultColor(textColor);
	richText->setScale(scale);
	richText->setAnchorPoint(anchorPoint);
	richText->setContentSize(text->getContentSize());
	richText->setPosition(text->getPosition());
	richText->setString(text->getString());
	
	return richText;
}
