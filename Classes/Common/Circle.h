//
//  Circle.hpp
//  GhostLeg
//
//  Created by Ken Lee on 1/3/2017.
//
//

#ifndef Circle_hpp
#define Circle_hpp

#include <stdio.h>
#include "cocos2d.h"

using namespace cocos2d;

class Circle
{
public:
	/**Low left point of rect.*/
	Vec2 origin;
	/**Width and height of the rect.*/
	float radius;
	
public:
	/**
	 Constructor an empty Circle.
	 * @js NA
	 */
	Circle();
	/**
	 Constructor a rect.
	 * @js NA
	 */
	Circle(float x, float y, float radius);
	Circle(const Vec2& pos, const float radius);
	Circle(const Circle& other);
	
	/**
	 * @js NA
	 * @lua NA
	 */
	Circle& operator= (const Circle& other);
	void setCircle(float x, float y, float _radius);

	bool intersectsCircle(const Circle& other) const;
	
	
	std::string toString();
};
#endif /* Circle_hpp */
