//
//  GameUI.cpp
//  GhostLeg
//
//  Created by Ken Lee on 17/5/2016.
//
//

#include "GameUI.h"

#include "StageParallaxLayer.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "ui/CocosGUI.h"

void GameUI::addSpaceParallax(Node *parent)
{
	ParallaxLayer *parallaxLayer = StageParallaxLayer::create();
	parent->addChild(parallaxLayer);
	parallaxLayer->scheduleUpdate();
}


void GameUI::showButtonClickAction(Node *button)
{
	
	
	//Sequence *seq =
}


// Star Value Node is the Node using StarValueLayer
// the value node is named as "value"
void GameUI::updateStarValue(Node *starValueNode)
{
	if(starValueNode == nullptr) {
		return;
	}
	
	ui::Text *text = starValueNode->getChildByName<ui::Text *>("value");
	if(text == nullptr) {
		return;
	}
	
	int star = GameManager::instance()->getUserData()->getTotalCoin();
	std::string strValue = StringUtils::format("%d", star);
	text->setString(strValue);
}
