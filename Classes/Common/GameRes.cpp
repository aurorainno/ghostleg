//
//  GameRes.cpp
//  GhostLeg
//
//  Created by Ken Lee on 15/6/2016.
//
//

#include "GameRes.h"
#include "GameManager.h"
#include "SuitManager.h"
#include "PlayerManager.h"
#include "ItemEffect.h"

std::string GameRes::getParticleName(Particle particle)
{
	switch(particle) {
		//case MissileExplode			: return "particle_missileexplode.plist"; // OLD
		case MissileExplode			: return "particle_explode.plist";		// new
		case ShieldBreak			: return "particle_shieldbreak1.plist";
		case Magnet					: return "particle_magnet.plist";
		case Invulernable			: return "particle_invulnerable1.plist";
		case Shield					: return "particle_shield.plist";
        case Slow                   : return "particle_snowball_debuff1.plist";
        
		default						: return "";
	}
}

float GameRes::getParticleScale(Particle particle)
{
	switch(particle) {
		case ShieldBreak			: return 1.2f;
		case Shield					: return 1.4f;
		case Invulernable			: return 0.3f;
			
		default						: return 1.0f;
	}
}



bool GameRes::isLoopParticle(Particle particle)
{
	switch(particle) {
		case MissileExplode:
		case ShieldBreak:
			return false;
			
		default:
			return true;
	}
}


std::string GameRes::getSelectedCharacterCsb()
{
	//int animeID = GameManager::instance()->getSuitManager()->getSelectedSuit() - 1;
	
	int animeID = PlayerManager::instance()->getSelectedChar();
	
	return getCharacterCsbName(animeID);
}

std::string GameRes::getCharacterCsbName(int animeID)
{
	return cocos2d::StringUtils::format("player/player_%03d.csb", animeID);
	
}

std::string GameRes::getCapsuleButtonImageName(int itemEffect)
{
	ItemEffect::Type itemType = (ItemEffect::Type) itemEffect;
	
	switch(itemType) {
		case ItemEffect::Type::ItemEffectInvulnerable:
			return "guiImage/ui_booster_item3.png";
			
		case ItemEffect::Type::ItemEffectMissile:
			return "guiImage/ui_booster_item1.png";
			
		case ItemEffect::Type::ItemEffectTimeStop:
			return "guiImage/ui_booster_item2.png";
			
		case ItemEffect::Type::ItemEffectDoubleCoins:
			return "guiImage/ui_booster_item5.png";
            
        case ItemEffect::Type::ItemEffectSnowball:
            return "guiImage/ui_booster_item4.png";
			
		default:
			return StringUtils::format("guiImage/suit_skill_%02d.png", itemEffect);
	}
}


std::string GameRes::getMoneyIcon(MoneyType type)
{
	switch (type) {
		case MoneyTypeCandy		: return "guiImage/icon_candystick.png";
		default					: return "guiImage/star_normal.png";
	}
}


std::string GameRes::getCountryFlag(const std::string &countryCode)
{
	std::string path = "countryFlag/";
	std::string file = path + countryCode + ".png";
	if(FileUtils::getInstance()->isFileExist(file) == false) {
		file = path + "unknown.png";
	}

	return file;
}
