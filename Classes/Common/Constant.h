//
//  Constant.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#ifndef Constant_hpp
#define Constant_hpp

#include <stdio.h>
#include <string>
#include "cocos2d.h"

USING_NS_CC;

#pragma mark - Macro used with

#define ANDROID_CLASS(x)	kAndroidPackage + "." + #x

#pragma mark - Common
extern const int kMapHeight;
extern const int kChaserEnemyID;
const int kInitialMapDataOffsetY = 300;		// The map data start from this position Y

extern const int kTutorialMapID;
extern const int kMaxReviveCount;

extern const std::string kFacebookPageLink;
extern const std::string kTwitterLink;
extern const std::string kFeedbackLink;
extern const std::string kVersionCheckUrl;

extern const std::string kAndroidPackage;

extern const Color4B kColorYellow;
extern const Color4B kColorGreen;

extern const int kEnemyScore;
extern const int kCameraPosition;
extern const int kMaxCharLevel;



extern const int kZorderStar;
extern const int kZorderItem;
extern const int kZorderEnemy;
extern const int kZorderObstacle;
extern const int kZorderPlayer;
extern const int kZorderGUI;

#pragma mark - Model Type
struct ModelType
{
	static const int line1;
	static const int line2;
	static const int line3;

	static const int enemyFix;
	static const int enemySlow;
	static const int enemyFast;

	static const int itemCoin;		// Also known as Star
	static const int itemPotion;
	static const int itemCapsule;
};


#endif /* Constant_hpp */
