//
//  PopTextAction.hpp
//  GhostLeg
//
//  Created by Calvin on 25/10/2016.
//
//

#ifndef PopTextAction_hpp
#define PopTextAction_hpp

#include <stdio.h>
#include "cocos2d.h"
#include "ui/CocosGUI.h"
#include "GameSound.h"

using namespace cocos2d;
using namespace cocos2d::ui;

class PopTextAction : public ActionInterval
{

public:
    PopTextAction();
    static PopTextAction* create(Text* text, int originalValue, int finalValue, std::function<void()> callback = nullptr,
                                 float finishTime = 0.8, float targetScale = 1.7, std::string displayFormat = "%d",
                                 GameSound::Sound sound = GameSound::Star5);
    
    bool init(Text* text, int originalValue, int finalValue, float finishTime , float targetScale ,
              std::string displayFormat, std::function<void()> callback, GameSound::Sound sound);
    
    virtual void startWithTarget(Node *pTarget) override;
    virtual void step(float dt) override;
    virtual bool isDone()const override;
    
protected:
    int mOriginalValue;
    int mFinalValue;
    float mTargetScale;
    float mFinishTime;
    
    float mAccumTime;
    
    float mInitScale;
    float mValueToAddEachTime;
    
    float mAccumValue;
    int mDisplayValue;
    
    std::string mDisplayFormat;
    
    GameSound::Sound mSfx;
    
    Text *mText;
    
    bool mIsDone;
    
    std::function<void()> mCallback;
    
};

#endif /* PopTextAction_hpp */
