//
//  Circle.cpp
//  GhostLeg
//
//  Created by Ken Lee on 1/3/2017.
//
//

#include "Circle.h"



// implementation of Circle

Circle::Circle(void)
{
	setCircle(0.0f, 0.0f, 0.0f);
}

Circle::Circle(float x, float y, float radius)
{
	setCircle(x, y, radius);
}

Circle::Circle(const Vec2& pos, const float radius)
{
	setCircle(pos.x, pos.y, radius);
}

Circle::Circle(const Circle& other)
{
	setCircle(other.origin.x, other.origin.y, other.radius);
}

Circle& Circle::operator= (const Circle& other)
{
	setCircle(other.origin.x, other.origin.y, other.radius);
	return *this;
}

void Circle::setCircle(float x, float y, float _radius)
{
	origin.x = x;
	origin.y = y;
	radius = _radius;
}



bool Circle::intersectsCircle(const Circle& other) const
{
	float dx = std::abs(other.origin.x - origin.x);
	float dy = std::abs(other.origin.y - origin.y);
	
	float minDist = other.radius + radius;
	if(dx > minDist || dy > minDist) {
		return false;	// Quick calculation
	}
	
	float dist = other.origin.distance(origin);
	
	return dist < minDist;
//	Vec2 circleDistance(std::abs(center.x - origin.x - w),
//						std::abs(center.y - origin.y - h));
//	
//	if (circleDistance.x <= (w))
//	{
//		return true;
//	}
//	
//	if (circleDistance.y <= (h))
//	{
//		return true;
//	}
//	
//	float cornerDistanceSq = powf(circleDistance.x - w, 2) + powf(circleDistance.y - h, 2);
//	
//	return (cornerDistanceSq <= (powf(radius, 2)));
}


std::string Circle::toString()
{
	return StringUtils::format("origin=[%f,%f] radius=%f", origin.x, origin.y, radius);
}
