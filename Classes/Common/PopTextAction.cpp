//
//  PopTextAction.cpp
//  GhostLeg
//
//  Created by Calvin on 25/10/2016.
//
//

#include "PopTextAction.h"
#define kRollingRefreshRate 0.05

PopTextAction::PopTextAction(){}

PopTextAction* PopTextAction::create(cocos2d::ui::Text *text, int originalValue, int finalValue,
                                     std::function<void()> callback,
                                     float finishTime, float targetScale, std::string displayFormat, GameSound::Sound sound)
{
    PopTextAction *ret = new (std::nothrow) PopTextAction();
    if (ret && ret->init(text,originalValue,finalValue,finishTime,targetScale,displayFormat,callback,sound))
    {
        ret->autorelease();
        return ret;
    }
    CC_SAFE_DELETE(ret);
    return nullptr;
}

bool PopTextAction::init(cocos2d::ui::Text *text, int originalValue, int finalValue,
                         float finishTime, float targetScale,std::string displayFormat, std::function<void()> callback, GameSound::Sound sound)
{
    CCASSERT(finalValue >= originalValue, "finalValue cannot be smaller than originalValue!");
    CCASSERT(text!=nullptr,"text cannot be null!");
    
    mText = text;
    mOriginalValue = originalValue;
    mFinalValue = finalValue;
    mFinishTime = finishTime;
    mTargetScale = targetScale;
    mDisplayFormat = displayFormat;
    mIsDone = false;
    mSfx = sound;
    mCallback = callback;

    return true;
}


void PopTextAction::step(float dt)
{
    if(mIsDone){
        return;
    }
    
    mAccumTime -= dt;
    if(mAccumTime > 0){
        return;
    }
    
    mAccumTime = kRollingRefreshRate;
    
        mAccumValue += mValueToAddEachTime;
    
        if(mAccumValue > mFinalValue ){
            mAccumValue = mFinalValue ;
            if(mCallback){
                mCallback();
            }
            mIsDone = true;
        }
    
        if((int)mAccumValue != mDisplayValue){
            mDisplayValue = (int)mAccumValue;
            
            ScaleTo *scaleTo = ScaleTo::create(0.25, mInitScale);
            
            mText->stopAllActions();
            mText->setScale(mInitScale * mTargetScale);
            mText->runAction(Sequence::create(scaleTo,NULL));
            mText->setString(StringUtils::format(mDisplayFormat.c_str(),(int)mDisplayValue));
            
            
            GameSound::playSound(mSfx);
        }
}

void PopTextAction::startWithTarget(Node *pTarget)
{
    ActionInterval::startWithTarget( pTarget );
    
    mInitScale = mText->getScale();
    mValueToAddEachTime = (float)(mFinalValue - mOriginalValue) * kRollingRefreshRate / mFinishTime;
    mAccumTime = 0;
    mAccumValue = mOriginalValue;
    mDisplayValue = mAccumValue;
    
    if(mValueToAddEachTime <= 0){
        mText->setString(StringUtils::format("%d",0));
        if(mCallback){
            mCallback();
        }
        mIsDone = true;
    }
}

bool PopTextAction::isDone() const
{
    return mIsDone;
}








