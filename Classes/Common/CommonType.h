//
//
//	Constant and Type for the game
//  GhostLeg
//
//  Created by Ken Lee on 14/3/2016.
//
//

#ifndef CommonType_hpp
#define CommonType_hpp

#include <stdio.h>
#include <map>
#include <string>

const int kPlayAdCountDownInSec = 720;
const std::string kEnemySfxKeyPrefix = "enemySFX";
const std::string kEnemyAnimeSfxKeySuffix = "enemy%02d_%d_%s";
const std::string kEnemyActiveSfxKeySuffix = "sfxActive/resID=%d/objectID=%d";
const std::string kEnemyAppearSfxKeySuffix = "sfxAppear/resID=%d/objectID=%d";
const std::string kEnemyAttackSfxKeySuffix = "sfxAttack/resID=%d/objectID=%d";
const std::string kEnemyIdleSfxKeySuffix = "sfxIdle/resID=%d/objectID=%d";

typedef struct {
	float x1;
	float y1;
	float x2;
	float y2;
} LineSegment;


typedef struct {
	float initialSpeed;
	float acceleration;
	float maxSpeed;
} SpeedData;

#define SPEED_DATA_STR(_data_) \
StringUtils::format("init=%f acc=%f max=%f", \
_data_.initialSpeed, _data_.acceleration, _data_.maxSpeed)

typedef enum {
	ObjectUnknown		= 0,
	ObjectObstacle		= 1,		// Rock, ...
	ObjectEnemy			= 2,
	ObjectBullet		= 3,
	ObjectLaser			= 4,
	ObjectBomb			= 5,
} ObjectTag;

typedef enum {
	DirNone = 0,
	DirUp = 1,
	DirDown = 2,
	DirLeft = 3,
	DirRight = 4,
} Dir;

typedef enum
{
	ScoreTypeDelivery,
	ScoreTypeKill,
	ScoreTypeDistance,
	ScoreTypeCollect,
	ScoreTypeAllClear,
	ScoreTypeBonus,
	
	ScoreTypeTotal,			// this is used in GameOverDialog
}ScoreType;


typedef enum
{
	HitByNone,		//
	HitByEnemy,		// Got Stun
	HitByChaser,	// Got GameOver
} CollisionType;

typedef enum
{
	EventUpdateTime,
    EventUpdateBarEnergy,
	EventGameOver,
	EventUpdateCoin,
	EventTapHintVisibility,
} GameWorldEvent;

typedef enum
{
	MoneyTypeUnknown = 0,
	MoneyTypeStar = 1,
	MoneyTypeCandy = 2,
	MoneyTypeDiamond = 3,
	MoneyTypeIAP = 4,			// Real money, just used in GameProduct to differentiate buy things by diamond or real money
	MoneyTypePuzzle = 5,		// ken: this is added mainly for Analytics
} MoneyType;

typedef enum
{
	AlertNone = 0,
	AlertOnScreen,
	AlertOnModel,
}AlertType;


typedef enum
{
	HitboxSimple = 0,			// A hitbox relative to the model position
	HitboxFixed = 1,			// A hitbox fix after creation, no need calculate
	HitboxDynamic = 2,			// Dynamic at every frame, e.g laser, ...
} HitboxType;



typedef std::map<std::string, float> GameProperties;


typedef enum
{
	EffectStatusNone,			// No Effect
	EffectStatusSlow,			// Model being slowed
	EffectStatusShield,			// Model have a shield
	EffectStatusTimestop,		// Model
	EffectStatusInvulnerable,	// Model become super
    EffectStatusSpeedUp         // Model speed up and invulnerable
} EffectStatus;		// e.g Slow, Shield, ....

typedef enum
{
    BoosterItemNone         = 0,
	BoosterItemMissile		= 3,
	BoosterItemDoubleCoin	= 6,
	BoosterItemTimeStop		= 7,
	BoosterItemUnbreakable	= 5,
	BoosterItemSnowball		= 9,
}BoosterItemType;

typedef enum
{
    BlockTypeNone = 0,
    BlockTypeEnemy,
    BlockTypeBullet,
    BlockTypeLaser,
    BlockTypeBomb
}BlockType;

#endif /* CommonType_hpp */
