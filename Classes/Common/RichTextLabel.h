//
//  RichTextLabel.hpp
//  BounceDefense
//
//  Created by Calvin on 19/9/2016.
//
//

#ifndef RichTextLabel_hpp
#define RichTextLabel_hpp

#include <stdio.h>

#include "cocos2d.h"

#include "ui/CocosGUI.h"
#include "cocostudio/CocoStudio.h"

USING_NS_CC;

using namespace cocos2d::ui;

class RichTextLabel : public RichText
{
public: // static
    CREATE_FUNC(RichTextLabel);
	
	static RichTextLabel *createFromText(Text *textUI);		// create a new richText and the attribute copy from Text
	
public:	// color attribute
	CC_SYNTHESIZE(cocos2d::Color4B, mDefaultColor, DefaultColor);
	CC_SYNTHESIZE(cocos2d::Color4B, mDefaultHiliColor, DefaultHiliColor);
	CC_SYNTHESIZE(float, mFontSize, FontSize);
	CC_SYNTHESIZE(std::string, mFontName, FontName);

public:
	// constructor
	RichTextLabel();
	
    virtual bool init();
    
    void setFont(const std::string &font, float fontSize);
	
	void setTextColor(int idx, const Color4B &color);
	Color4B getTextColorByIndex(int idx);
	
    void setString(const std::string &content);
	
	
    RichTextLabel *add(const std::string &content);
    RichTextLabel *add(const std::string &content, const cocos2d::Color4B &color);
    RichTextLabel *addNewLine();
    RichTextLabel *addVertSpacing(float spacing);
	
private:
	Color4B extractColorFromTokenTag(const std::string &tag);
	
private:
	std::map<int, Color4B> mTextColorMap;	// later
	
};

#endif /* RichTextLabel_hpp */
