//
//  DebugInfo.cpp
//  GhostLeg
//
//  Created by Ken Lee on 26/3/2016.
//
//

#include "DebugInfo.h"
#include "GameWorld.h"
#include "Player.h"
#include "Constant.h"
#include "StringHelper.h"
#include "cocos2d.h"
#include "GameManager.h"

USING_NS_CC;

static DebugInfo *sInstance = NULL;

namespace  {
	int getMapZoneIndex(int distance) {
		return (distance - kInitialMapDataOffsetY) / kMapHeight;
	}
	
	std::string getPlayerPos() {
		Player *player = GameWorld::instance()->getPlayer();
		
		if(player == nullptr) {
			return "";
		}
		
		char temp[100];
		Vec2 pos = player->getPosition();
		sprintf(temp, "%0.2f,%0.2f", pos.x, pos.y);
		
		return std::string(temp);
	}
}


DebugInfo *DebugInfo::instance()
{
	if(sInstance != nullptr) {
		return sInstance;
	}
	
	sInstance = new DebugInfo();
	
	return sInstance;
}


#pragma mark - Main Class
DebugInfo::DebugInfo()
{
	
}

void DebugInfo::reset()
{
	mDistanceMapID.clear();
}

void DebugInfo::saveDistanceMapID(int startDistance, int mapID)
{
	int zoneID = getMapZoneIndex(startDistance);
	if(zoneID < 0) {
		return;
	}
	
	mDistanceMapID[zoneID] = mapID;
}

int DebugInfo::getMapIDAtDistance(int distance)
{
	int zoneID = getMapZoneIndex(distance);
	if(zoneID < 0) {
		return -1;
	}
	
	
	return mDistanceMapID[zoneID];
}


std::string DebugInfo::info()
{
	Player *player = GameWorld::instance()->getPlayer();
	
	return infoAtDistance(player->getPosition().y);
}

std::string DebugInfo::infoAtDistance(int distance)
{
	char str[100];
	std::string result = "";
	
	// MapID
	int mapID = GameWorld::instance()->getMapID();
	
	result = "map: ";
	if(mapID < 0) {
		result += "n/a";
	} else {
		sprintf(str, "%d", mapID);
		result += str;
	}
	result += "\n";
	
	result += "player: ";
	result += getPlayerPos();
	result += "\n";

	Player *player = GameWorld::instance()->getPlayer();
	int speedX = player == nullptr ? -1 : (int) player->getSpeedX();
	int speedY = player == nullptr ? -1 : (int) player->getSpeedY();
	
	result += "speed: ";
	result += StringUtils::format("vx=%d vy=%d", speedX, speedY);
	result += "\n";

	
	return result;
}


std::string DebugInfo::infoScreen()
{
	float scaleFactor = Director::getInstance()->getOpenGLView()->getContentScaleFactor();
	Size designSize = Director::getInstance()->getOpenGLView()->getDesignResolutionSize();
	Size frameSize = Director::getInstance()->getOpenGLView()->getFrameSize();
	float zoomFactor = Director::getInstance()->getOpenGLView()->getFrameZoomFactor();
	
	Rect scissorRect = Director::getInstance()->getOpenGLView()->getScissorRect();
	Rect viewPortRect = Director::getInstance()->getOpenGLView()->getViewPortRect();
	
	
	std::string info = "";
	
	info += "designSize: " + SIZE_TO_STR(designSize) + "\n";
	info += "frameSize: " + SIZE_TO_STR(frameSize) + "\n";
	info += "scissorRect: " + RECT_TO_STR(scissorRect) + "\n";
	info += "viewPortRect: " + RECT_TO_STR(viewPortRect) + "\n";
	info += "zoomFactor: " + FLOAT_TO_STR(zoomFactor) + "\n";
	info += "scaleFactor: " + FLOAT_TO_STR(scaleFactor) + "\n";
	
	return info;
}

void DebugInfo::showState(bool flag)
{
	if(GameManager::instance()->isDebugMode() == false) {
		return;		// Do nothing for release version
	}
	
	Director::getInstance()->setDisplayStats(flag);
}