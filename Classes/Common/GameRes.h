//
//  GameRes.hpp
//  GhostLeg
//
//  Created by Ken Lee on 15/6/2016.
//
//

#ifndef GameRes_hpp
#define GameRes_hpp

#include <stdio.h>
#include <string>
#include "CommonType.h"

class GameRes
{
public: // enum
	enum Particle {
		MissileExplode,
		ShieldBreak,
		Shield,
		Invulernable,
		Magnet,
        Slow,
        None,
	};
	
public:
	static std::string getParticleName(Particle particle);
	static bool isLoopParticle(Particle particle);
	static float getParticleScale(Particle particle);
	static std::string getSelectedCharacterCsb();
	static std::string getCharacterCsbName(int suitID);
	static std::string getCapsuleButtonImageName(int itemEffect);
	static std::string getMoneyIcon(MoneyType type);
	static std::string getCountryFlag(const std::string &countryCode);
};

#endif /* GameRes_hpp */
