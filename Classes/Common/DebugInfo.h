//
//  DebugInfo.hpp
//  GhostLeg
//
//  Created by Ken Lee on 26/3/2016.
//
//

#ifndef DebugInfo_hpp
#define DebugInfo_hpp

#include <stdio.h>
#include <map>

#pragma mark - Static Method
class DebugInfo
{
public:
	static DebugInfo *instance();
	DebugInfo();

	void reset();
	void saveDistanceMapID(int startDistance, int mapID);
	int getMapIDAtDistance(int distance);
	
	std::string info();
	std::string infoAtDistance(int distance);

	// Static Helper
	static std::string infoScreen();
	
	static void showState(bool flag);
	
private:
	std::map<int, int> mDistanceMapID;		// key is the zone ID
};




#pragma mark - Public Method & Property


#endif /* DebugInfo_hpp */
