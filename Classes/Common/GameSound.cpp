//
//  GameSound.cpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#include "GameSound.h"
//#include "audio/include/SimpleAudioEngine.h"
#include "GameManager.h"
#include "UserGameData.h"
#include "Enemy.h"
#include "cocos2d.h"
#include <time.h>
#include <AudioEngine.h>
#include "TimeMeter.h"


USING_NS_CC;

//using namespace CocosDenshion;
using namespace experimental;

//const float kStarMelodyDuration = 0.75;
const float kStarMelodyDuration = 0.2;
const float kStarMelodyFreqControl = 0.05;		// Not play too frequency control

const float kDefaultBGMVolume = 1.0;

std::map<std::string,int> GameSound::mSoundIDMap;
int GameSound::mMusicSoundID(-1);

namespace  {
	float getMusicVolume(GameSound::Music music)
	{
		if(GameSound::EventInGame == music) {
			return 1.0f;
		}
		
		return kDefaultBGMVolume;
	}
	bool isLoopingMusic(GameSound::Music music)
	{
		switch (music) {
			case GameSound::InGame:
            case GameSound::Title:
            case GameSound::EventInGame:
			case GameSound::Bgm01:
			case GameSound::Bgm02:
			case GameSound::Bgm03:
			case GameSound::Bgm04:
			case GameSound::Bgm05:
                return true;
			default:
				return false;
		}
	}
	
	
	GameSound::Sound getEnemyAppearSound(int resID)
	{
		// Rocks Enemy
//		if(resID >= 13 && resID <= 16) {
//			return GameSound::RockMonster;
//		}
		
		switch (resID) {
            case 310:
            case 311:
            case 312:
            case 313:
            case 314:
            case 315:
            case 316:
            
            return GameSound::FireTrapIdle;
//			case 8:
//				return GameSound::RockMonster;
//				
//			case 2:
//			case 5:
//				return GameSound::RobotWake;
//				
//			case 3:
//				return GameSound::RobotDrop;
//				
			default:
				return GameSound::None;
		}
	}
	
	GameSound::Sound getEnemyActiveSound(int enemyID)
	{
		
		switch (enemyID) {
			case 22:
				return GameSound::RobotFlyUp;
            
            case 15:
                return GameSound::FlyDown;
            
            case 80:
                return GameSound::GreenBulletActive;
				
			default:
				return GameSound::None;
		}
	}
    
    GameSound::Sound getEnemyAttackSound(int enemyID)
    {
        switch (enemyID) {
            
            default:
            return GameSound::None;
        }
    }
    
    GameSound::Sound getEnemyIdleSound(int enemyID)
    {
        switch (enemyID) {

            
            default:
            return GameSound::None;
        }
    }
    
    bool getEnemyActiveSoundShouldLoop(int enemyID)
    {
        
        switch (enemyID) {
            case 22:
            return true;
            
            default:
            return false;
        }
    }
    
    bool getEnemyIdleSoundShouldLoop(int enemyID)
    {
        switch (enemyID) {

            
            default:
            return false;
        }
    }
    
    bool getEnemyAppearSoundShouldLoop(int enemyID)
    {
        switch (enemyID) {
            case 310:
            case 311:
            case 312:
            case 313:
            case 314:
            case 315:
            case 316:
            return true;
            
            default:
            return false;
        }
    }
}

void GameSound::setup()
{
	//SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(0.5f);
	GameSound::mMusicSoundID = -1;
    mSoundIDMap.clear();
}

std::string GameSound::getSoundName(Sound sound)
{
	switch (sound) {
			// Star
		case Star1:			return "coin_1.mp3";
		case Star2:			return "coin_2.mp3";
		case Star3:			return "coin_3.mp3";
		case Star4:			return "coin_4.mp3";
		case Star5:			return "coin_5.mp3";
			
        case Bee:           return "robot_drop.mp3";		// XXX
			
		case RobotWake:     return "ghost_appear.mp3";
		case RobotHide:     return "invisible.mp3";
		case RobotDrop:     return "robot_drop.mp3";
        case Hit:           return "hit.mp3";
        case RockMonster:	return "monster_drop.mp3";
		case RobotFlyUp:	return "enemySFX/monster_flyout.mp3";
		
			
			
		case Button:		return "general_btn_01.mp3";	// XXX
		
		
        //enemy
        case BomberDrop:    return "enemySFX/bomb_active.mp3";
        case BomberHit:     return "enemySFX/bomb_hit.mp3";
        case FlyDown:       return "";
        case MovingTrap:    return "";
        
            
            
		//Power up
        case MagnetActive:  return "powerUp/magnet_loop.mp3";
        case MagnetOut:     return "powerUp/magnet_out.mp3";
			
        case CashOutActive: return "powerUp/cashout_active.mp3";
        case CashOutHit:    return "powerUp/cashout_hit.mp3";
		
        case SpeedUp:       return "powerUp/speedup_active.mp3";
            
		// Potion
		case DrawLine:		return "drawline_ok.mp3";
        case CancelDrawLine:return "drawline_error.mp3";
        
		//case Coin:          return "coin_1.mp3";
		case Coin:          return "coin_alt.mp3";
		case Potion:		return "restore_res.mp3";
		case CollectCapsule:return "collect_item.mp3";
			
		// Effect
		case MissileActive:	return "booster/missile_active.mp3";
        case MissileLoop :   return "booster/missile_loop.mp3";
		case MissileHit:     return "booster/missile_hit.mp3";
		case Explosion:		return "explosion.mp3";
		case Burning:		return "burning.mp3";
		case ShieldBreak:	return "shield_hit.mp3";
		case ShieldHit:		return "shield_break.mp3";
		case GrabStar:		return "star_grab.mp3";
		case TimeSlow:		return "time_slow.mp3";
        case TimeStopActive: return "booster/timestop_loop.mp3";
        case TimeStopOut:   return "booster/timestop_out.mp3";
        case DoubleCoins:   return "booster/double_coins_boosters.mp3";

        
        case CycloneActive: return "booster/cyclone_loop.mp3";
        case CycloneHit:    return "booster/cyclone_hit.mp3";
        case ShieldActive:  return "booster/shield_active.mp3";
        case ShieldLoop:    return "booster/shield_loop.mp3";
        case ShieldEnd:     return "booster/shield_out.mp3";
			
			
        // Candy
        case Candy:         return "candyStick.mp3";
			
		// For GUI
		case Button1:		return "general_btn_01.mp3";
		case Button2:		return "general_btn_02.mp3";
		case Button3:		return "general_btn_02.mp3";		// ken: no general_btn_03.mp3
		case Button4:		return "general_btn_04.mp3";
		case Button5:		return "general_btn_05.mp3";
		case Select:		return "select.mp3";
        
        //case Unlock:        return "music_unlock.mp3";
		case Unlock:        return "sfx/stage_unlock.mp3";
        case UnlockChar:    return "sfx/random_value.mp3";
        
        case Collect:       return "voice/bark.mp3";
        case CollectOnDaySeven: return "voice/mission_completed.mp3";
			
			
		// Voice
		case VoiceBark				: return "voice/bark.mp3";
		case VoiceReady				: return "voice/ready.mp3";
		case VoiceGo				: return "voice/go.mp3";
		case VoicePowerUp			: return "voice/power_up.mp3";
		case VoiceHighScore			: return "voice/new_highscore.mp3";
		case VoiceObjectiveDone		: return "voice/objective_done.mp3";
		case VoiceCool				: return "voice/cool.mp3";
		case VoiceLaugh				: return "voice/crazylaugh.mp3";
		case VoiceAmazing			: return "voice/amazing.mp3";
		case VoiceSeeYa				: return "voice/seeya.mp3";
        case VoiceDeath             : return "voice/character_death.mp3";
			
		// Other
		case CountDown				: return "temp/countdown.mp3";
            
        case PassCheckPoint         : return "guiSfx/pass_checkpoint.mp3";
        case ChangeStage            : return "guiSfx/change_stage.mp3";
        case SelectBooster          : return "guiSfx/item_equip.mp3";
        case Purchase               : return "guiSfx/purchase_complete.mp3";
        case CollectPuzzle          : return "guiSfx/puzzle_get.mp3";
        case PuzzleExplode          : return "guiSfx/puzzle_open.mp3";
        case UnlockStage            : return "guiSfx/stage_unlock.mp3";
        case UpgradeChar            : return "guiSfx/upgrade_character.mp3";
        case ResultScore            : return "guiSfx/resultscore.mp3";
        
        case FireTrapActive         : return "enemySFX/firetrap_active.mp3";
        case FireTrapIdle           : return "enemySFX/firetrap_idle.mp3";
        
        case GreenBulletActive      : return "enemySFX/greenbullet_active.mp3";
        case SawActive              : return "enemySFX/saw_active.mp3";
        
        case Brake                  : return "character_brake.mp3";
             
		default                     : return "";
	}
}


GameSound::Music GameSound::getRandomInGameMusic()
{
	int start = Music::Bgm01;
	int end = Music::Bgm03;
	
	return (Music) RandomHelper::random_int(start, end);
}

std::string GameSound::getMusicName(Music music)
{
    bool isEventOn = GameManager::instance()->isEventOn();
	
	switch (music) {
        case InGame:        return getMusicName(getRandomInGameMusic());
        case EventInGame:   return "christmas_ingame.mp3";
		case GameEnd:		return "end_game.mp3";
		case Title:			return "music/main.mp3";
		case Bgm01:         return "music/stage_01.mp3";
		case Bgm02:         return "music/stage_02.mp3";
		case Bgm03:         return "music/stage_03.mp3";
		case Bgm04:         return "music/stage_04.mp3";
		case Bgm05:         return "music/stage_05.mp3";
		default:			return "";
	}
}

void GameSound::playStageMusic(int stage)
{
	if(!GameManager::instance()->getUserData()->isBGMOn()){
		return;
	}
	
	GameSound::Music music;
	switch (stage) {
		case 1		: music = GameSound::Bgm01; break;
		case 2		: music = GameSound::Bgm02; break;
		case 3		: music = GameSound::Bgm03; break;
		case 4		: music = GameSound::Bgm04; break;
		case 5		: music = GameSound::Bgm05; break;
		default		: music = GameSound::Bgm01; break;
			break;
	}
	
	playMusic(music);

}

void GameSound::playMusic(GameSound::Music music)
{
    if(!GameManager::instance()->getUserData()->isBGMOn()){
        return;
    }
    
	std::string name = getMusicName(music);
	if(name.length() == 0) {
		return;
	}
	
	bool isLoop = isLoopingMusic(music);
	float volume = getMusicVolume(music);
	
	//GameSound::mMusicSoundID =
	if(mMusicSoundID >= 0) {
		AudioEngine::stop(mMusicSoundID);
	}
	mMusicSoundID = AudioEngine::play2d(name, isLoop, volume);
	// log("DEBUG: playMusic: soundID=%d", mMusicSoundID);
//	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
//	SimpleAudioEngine::getInstance()->playBackgroundMusic(name.c_str(), isLoop);
}

void GameSound::playSound(GameSound::Sound sound, bool isLoop, std::string key, float volume)
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }

    std::string soundName = getSoundName(sound);
    if(soundName.length() == 0) {
        return ;
    }
    
    if(key == ""){
        key = soundName;
    }
	
	//SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
    //mSoundIDMap[key] = SimpleAudioEngine::getInstance()->playEffect(songName.c_str(),isLoop);
	mSoundIDMap[key] = AudioEngine::play2d(soundName, isLoop, volume);
}

void GameSound::pauseSound(GameSound::Sound sound)
{
	if(!GameManager::instance()->getUserData()->isSEOn()){
		return ;
	}
	
	int soundID = getSoundID(sound);
	if(soundID < 0) {
		return;
	}
	
	//SimpleAudioEngine::getInstance()->pauseEffect(soundID);
	AudioEngine::pause(soundID);
}

void GameSound::resumeSound(GameSound::Sound sound)
{
	if(!GameManager::instance()->getUserData()->isSEOn()){
		return ;
	}
	
	int soundID = getSoundID(sound);
	if(soundID < 0) {
		return;
	}
	
	//SimpleAudioEngine::getInstance()->resumeEffect(soundID);
	AudioEngine::resume(soundID);
}

void GameSound::stopSound(GameSound::Sound sound)
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }
	
	int soundID = getSoundID(sound);
	if(soundID < 0) {
		return;
	}
	
    //SimpleAudioEngine::getInstance()->stopEffect(soundID);
	AudioEngine::stop(soundID);
	
	std::string name = getSoundName(sound);
    mSoundIDMap.erase(name);
}

void GameSound::stopAllEffect()
{
    //SimpleAudioEngine::getInstance()->stopAllEffects();
	AudioEngine::stopAll();
    mSoundIDMap.clear();
}

void GameSound::pauseSound(std::string key)
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }
    
    if(mSoundIDMap.find(key) == mSoundIDMap.end()){
        return;
    }
    
    int soundID = mSoundIDMap[key];
    
    
    //SimpleAudioEngine::getInstance()->pauseEffect(soundID);
	AudioEngine::pause(soundID);
}

void GameSound::resumeSound(std::string key)
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }
    
    if(mSoundIDMap.find(key) == mSoundIDMap.end()){
        return;
    }
    
    int soundID = mSoundIDMap[key];
    
    //SimpleAudioEngine::getInstance()->resumeEffect(soundID);
	AudioEngine::resume(soundID);
}

void GameSound::stopSound(const std::string &key)
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }
    
    
    if(mSoundIDMap.find(key) == mSoundIDMap.end()){
        return;
    }
    
    log("%s",key.c_str());
    
    int soundID = mSoundIDMap[key];
    
    // SimpleAudioEngine::getInstance()->stopEffect(soundID);
	AudioEngine::stop(soundID);
    
    mSoundIDMap.erase(key);
}

void GameSound::stopSound(const int &soundID)
{
	// SimpleAudioEngine::getInstance()->stopEffect(soundID);
	AudioEngine::stop(soundID);
}



int GameSound::getSoundID(GameSound::Sound sound)
{
    std::string name = getSoundName(sound);
    if(name.length() == 0) {
        return -1;
    }
    
    if(mSoundIDMap.find(name) == mSoundIDMap.end()){
        return -1;
    }
    
    int soundID = mSoundIDMap[name];
    
    return soundID;
}

void GameSound::playEnemyAppearSound(int resID,std::string key)
{
	 Sound sound = getEnemyAppearSound(resID);
	if(sound == None) {
		return;
	}
	
    bool shouldLoop = getEnemyAppearSoundShouldLoop(resID);
	
	TSTART("Sound.EnemyAppear");
	playSound(sound, shouldLoop, key);
	TSTOP("Sound.EnemyAppear");
}

void GameSound::playEnemyActiveSound(int enemyID,std::string key)
{
	Sound sound = getEnemyActiveSound(enemyID);
	if(sound == None) {
		return;
	}
    
    bool shouldLoop = getEnemyActiveSoundShouldLoop(enemyID);
	
	TSTART("Sound.EnemyActive");
	playSound(sound, shouldLoop, key);
	TSTOP("Sound.EnemyActive");
}

void GameSound::playEnemyAttackSound(int enemyID, std::string key)
{
    Sound sound = getEnemyAttackSound(enemyID);
    if(sound == None) {
        return;
    }
    
//    bool shouldLoop = getEnemyActiveSoundShouldLoop(enemyID);
    TSTART("Sound.EnemyAttack");
    playSound(sound, false, key);
	TSTOP("Sound.EnemyAttack");
}

void GameSound::playEnemyIdleSound(int enemyID, std::string key)
{
    Sound sound = getEnemyIdleSound(enemyID);
    if(sound == None) {
        return;
    }
    
    bool shouldLoop = getEnemyIdleSoundShouldLoop(enemyID);
	
	TSTART("Sound.EnemyIdle");
    playSound(sound, shouldLoop, key);
	TSTOP("Sound.EnemyIdle");
}


void GameSound::stopAllEnemySound()
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }
    
    std::vector<std::string> keyList{};
    for(std::map<std::string,int>::iterator it = mSoundIDMap.begin();it!=mSoundIDMap.end();it++){
        std::string key = it->first;
        if(key.find(kEnemySfxKeyPrefix)!=std::string::npos){
            int soundID = it->second;
			stopSound(soundID);
            keyList.push_back(key);
        }
    }
    
    for(std::string key : keyList){
        mSoundIDMap.erase(key);
    }
}

void GameSound::stopEnemySound(int enemyID, int objectID)
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return ;
    }
    
    std::vector<std::string> keyList{};
    for(std::map<std::string,int>::iterator it = mSoundIDMap.begin();it!=mSoundIDMap.end();it++){
        std::string key = it->first;
        std::string targetKey = kEnemySfxKeyPrefix + StringUtils::format("enemy%02d_%d",enemyID,objectID);
        std::string targetKey2 = kEnemySfxKeyPrefix + StringUtils::format(kEnemyActiveSfxKeySuffix.c_str(),enemyID,objectID);
        std::string targetKey3 = kEnemySfxKeyPrefix + StringUtils::format(kEnemyAttackSfxKeySuffix.c_str(),enemyID,objectID);
        std::string targetKey4 = kEnemySfxKeyPrefix + StringUtils::format(kEnemyIdleSfxKeySuffix.c_str(),enemyID,objectID);
        std::string targetKey5 = kEnemySfxKeyPrefix + StringUtils::format(kEnemyAppearSfxKeySuffix.c_str(),enemyID,objectID);
        
        if(key.find(targetKey)!=std::string::npos){
            int soundID = it->second;
			stopSound(soundID);
            keyList.push_back(key);
        }else if(key.find(targetKey2)!=std::string::npos){
            int soundID = it->second;
			stopSound(soundID);
            keyList.push_back(key);
        }else if(key.find(targetKey3)!=std::string::npos){
            int soundID = it->second;
			stopSound(soundID);
            keyList.push_back(key);
        }else if(key.find(targetKey4)!=std::string::npos){
            int soundID = it->second;
			stopSound(soundID);
            keyList.push_back(key);
        }else if(key.find(targetKey5)!=std::string::npos){
            int soundID = it->second;
			stopSound(soundID);
            keyList.push_back(key);
        }
    }

    for(std::string key : keyList){
        mSoundIDMap.erase(key);
    }
}


void GameSound::stop()
{
//	auto audio = SimpleAudioEngine::getInstance();
//	// pause background music.
//	
//	audio->stopBackgroundMusic();
//	
	
	//audio->stopAllEffects();
	AudioEngine::stopAll();
	
}

void GameSound::pause()
{
//	auto audio = SimpleAudioEngine::getInstance();
//	
//	// pause background music.
//	audio->pauseBackgroundMusic();
	
	// pause all sound effects.
	//audio->pauseAllEffects();
	AudioEngine::pauseAll();
}

void GameSound::resume()
{
//	auto audio = SimpleAudioEngine::getInstance();
//	
//	// resume background music.
//	audio->resumeBackgroundMusic();
	
	// resume all sound effects.
	//audio->resumeAllEffects();
	AudioEngine::resumeAll();
}


void GameSound::playStarSound()
{
    if(!GameManager::instance()->getUserData()->isSEOn()){
        return;
    }
	
    static float previousTime = 0;
	static float lastPlayTime = 0;
	static int idx = 0;
    
    clock_t t = clock();
    
    float currentTime = (float)t/CLOCKS_PER_SEC;
	
	if(lastPlayTime > 0 && kStarMelodyFreqControl > 0) {
		if((currentTime - lastPlayTime) < kStarMelodyFreqControl) {
			return;
		}
	}
	lastPlayTime = currentTime;
	
	
	float timeElapse = currentTime - previousTime;
	
    if(timeElapse  < kStarMelodyDuration){
        idx = idx >= 4 ? 4 : idx+1;
		//idx = (idx+1) % 3;
    }else{
        idx=0;
    }
	
	previousTime = currentTime;
	
	Sound sound = (Sound)(GameSound::Star1 + idx);	// if idx=2, Sound = Star3 (1+2)
	
	playSound(sound);
    
   // log("GameSound.cpp: currentTime = %.2f, idx = %d",currentTime,idx);

}

void GameSound::playSoundFile(const std::string &filename, bool isLoop, float volume, std::string key)
{
	if(!GameManager::instance()->getUserData()->isSEOn()){
		return;
	}
	
	TSTART("playSoundFile");
	int soundID = AudioEngine::play2d(filename.c_str(), isLoop, volume);
	TSTOP("playSoundFile");
	
    if(key!=""){
		mSoundIDMap[key] = soundID;
	} else {
    }
}

void GameSound::preloadGameSound()
{
	std::vector<Sound> soundList;
	

	
	soundList.push_back(Star1);
	soundList.push_back(Star2);
	soundList.push_back(Star3);
	soundList.push_back(Star4);
	soundList.push_back(Star5);
	
	soundList.push_back(VoiceGo);
	soundList.push_back(VoiceCool);
	
	
	soundList.push_back(CollectCapsule);
	
	soundList.push_back(DrawLine);
	soundList.push_back(CancelDrawLine);
	soundList.push_back(Brake);
    soundList.push_back(CollectPuzzle);
    soundList.push_back(SpeedUp);
    soundList.push_back(MagnetOut);
    soundList.push_back(MagnetActive);
    soundList.push_back(CashOutHit);
    soundList.push_back(CashOutActive);

    soundList.push_back(MissileActive);
    soundList.push_back(MissileLoop);
    soundList.push_back(MissileHit);
    soundList.push_back(TimeStopActive);
    soundList.push_back(TimeStopOut);
    soundList.push_back(DoubleCoins);
    soundList.push_back(CycloneActive);
    soundList.push_back(CycloneHit);
    soundList.push_back(ShieldLoop);
    soundList.push_back(ShieldEnd);

	for(GameSound::Sound sound : soundList) {
		std::string soundFile = getSoundName(sound);
		AudioEngine::preload(soundFile.c_str());
	}
	
}

void GameSound::playCoinSound()
{
	if(!GameManager::instance()->getUserData()->isSEOn()){
		return ;
	}
	
	AudioEngine::play2d("coin_3.mp3");
}
