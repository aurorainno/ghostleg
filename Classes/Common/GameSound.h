//
//  GameSound.hpp
//  GhostLeg
//
//  Created by Ken Lee on 23/3/2016.
//
//

#ifndef GameSound_hpp
#define GameSound_hpp

#include <stdio.h>
#include <string>
#include "cocos2d.h"

class GameSound 
{
public: // enum
	enum Music {
		InGame,
        EventInGame,
		GameEnd,
        Title,
		
		Bgm01, 
		Bgm02,
		Bgm03,
		Bgm04,
		Bgm05,
	};
	
	enum Sound {
		None, 
        Bee,
        DrawLine,
        CancelDrawLine,
        Coin,
        RobotWake,
		RobotHide,
		RobotDrop,
		RockMonster,
		RobotFlyUp,
    
		
		Hit,
        
		
        Button,
		Potion,
		CollectCapsule,
        
        //enemy
        BomberDrop,
        BomberHit,
        MovingTrap,
        
        //PowerUp
        MagnetActive,
        MagnetOut,
        CashOutActive,
        CashOutHit,
		
		// Effect
		MissileActive,
        MissileLoop,
        MissileHit,
		Explosion,
		Burning,
		ShieldHit,
		ShieldBreak,
		GrabStar,
		ShieldActive,
        ShieldLoop,
        ShieldEnd,
		TimeSlow,
		TimeStopActive,
        TimeStopOut,
        SpeedUp,
        DoubleCoins,
        
        CycloneActive,
        CycloneHit,
		
		// Game Effect
		CountDown,
		
		// Star
		Star1,
		Star2,
		Star3,
		Star4,
		Star5,
        
        //Candy
        Candy,
		
		// For UI
		Button1,
		Button2,
		Button3,
		Button4,
		Button5,
		Select,
        
        //For Unlock Dogs/Planets
        Unlock,
        UnlockChar,
        
        //For Daily Reward
        Collect,
        CollectOnDaySeven,
		
		// Voice
		VoiceReady,
		
		VoiceCool,
		VoiceLaugh,
		VoiceAmazing,
		
		VoiceBark,
		VoiceSeeYa,
		
		VoiceGo,
		VoiceCongratulation,
		VoicePowerUp,
		VoiceHighScore,
		VoiceObjectiveDone,
        VoiceDeath,
        
        PassCheckPoint,
        ChangeStage,
        SelectBooster,
        Purchase,
        CollectPuzzle,
        PuzzleExplode,
        UnlockStage,
        UpgradeChar,
        ResultScore,
        
        FlyDown,
        FireTrapActive,
        FireTrapIdle,
        GreenBulletActive,
        SawActive,
        
        Brake,
	};
public:
	static void playCoinSound();
	static std::string getSoundName(Sound sound);
	static std::string getMusicName(Music music);
	
	static void setup();
	
	static void stop();
	static void pause();
	static void resume();
    
    static void stopAllEffect();
	
	
	static void playMusic(Music music);
    static void playSound(Sound sound, bool isLoop = false, std::string key = "", float volume=1.0);
	static void playSoundFile(const std::string &filename, bool isLoop = false, float volume = 1.0, std::string key = "");
	
    
    static void stopSound(Sound sound);
	static void resumeSound(Sound sound);
	static void pauseSound(Sound sound);
    
    static void resumeSound(std::string key);
    static void pauseSound(std::string key);
    static void stopSound(const std::string &key);
	static void stopSound(const int &soundID);
	
	static int getSoundID(Sound sound);
	static void playStageMusic(int stage);
	
	static void playStarSound();
	
    static void playEnemyAppearSound(int resID, std::string key = "");
	static void playEnemyActiveSound(int resID, std::string key = "");
    static void playEnemyAttackSound(int enemyID, std::string key = "");
    static void playEnemyIdleSound(int enemyID, std::string key = "");
    
    static void stopAllEnemySound();
    static void stopEnemySound(int enemyID, int objectID);
	
	static Music getRandomInGameMusic();
	static void preloadGameSound();
    
private:
    static std::map<std::string,int> mSoundIDMap;
	static int mMusicSoundID;
};

#endif /* GameSound_hpp */
