//
//  MasteryScene.h
//  GhostLeg
//
//  Created by Calvin on 17/5/2016.
//
//

#ifndef MasteryScene_h
#define MasteryScene_h

#include <stdio.h>
#include "cocos2d.h"
#include "extensions/cocos-ext.h"
#include "ui/CocosGUI.h"
#include <time.h>
#include <MasteryItemView.h>
#include "CommonType.h"

USING_NS_CC_EXT;
USING_NS_CC;

using namespace cocos2d;
using namespace cocos2d::ui;

class ParallaxLayer;

class MasterySceneLayer : public Layer
{
public:
    static Scene* createScene();
    
    
//    virtual void onEnter();
//    virtual void onExit();
    virtual bool init();
    void refreshPage();
    void showNotEnoughCoins();
    // implement the "static create()" method manually
    CREATE_FUNC(MasterySceneLayer);
    virtual void onEnter();
    virtual void onExit();
    void update(float delta);

private:
    void setupUI();
    void updateCoins();
	void showAddCoinDialog();

private:
    Text* mStars;
    Node *mRootNode;
    ParallaxLayer* mParallaxLayer;
    Vector<MasteryItemView*> mMasteryItems;
};


#endif /* MasteryScene_h */
