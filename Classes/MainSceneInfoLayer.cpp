//
//  MainSceneInfoLayer.cpp
//  GhostLeg
//
//  Created by Calvin on 8/12/2016.
//
//

#include "MainSceneInfoLayer.h"
#include "PlanetManager.h"
#include "PlanetData.h"
#include "DogManager.h"
#include "DogData.h"
#include "RichTextLabel.h"
#include "CircularMaskNode.h"
#include "StringHelper.h"


MainSceneInfoLayer::MainSceneInfoLayer()
:mRootNode(nullptr)
{
    
}

bool MainSceneInfoLayer::init()
{
    if(!Layer::init()){
        return false;
    }
    
        
    return true;
}

std::string MainSceneInfoLayer::getCsbNameByType(MainSceneInfoLayer::InfoType type)
{
    switch (type) {
        case EventInfo:
           return "MainScreenEventInfoLayer.csb";
            
        case UnlockInfo:
           return "MainScreenUnlockInfoLayer.csb";
            
        case LockedInfo:
           return "MainScreenLockedInfoLayer.csb";
            
        default:
           return "";
    }
}

void MainSceneInfoLayer::setup(int planetID)
{
     bool isUnlocked = PlanetManager::instance()->isPlanetUnLocked(planetID);
     PlanetData* planetData = PlanetManager::instance()->getPlanetData(planetID);
     if(planetData->getPlanetType() == PlanetData::Event){
         mInfoType = EventInfo;
     }else if(isUnlocked){
         mInfoType = UnlockInfo;
     }else if(!isUnlocked){
         mInfoType = LockedInfo;
     }
    mPlanetID = planetID;
    setupUI(mInfoType);
    this->setVisible(true);
}


void MainSceneInfoLayer::setupUI(InfoType type)
{
    if(mRootNode){
        mRootNode->removeFromParent();
    }
    std::string csbName = getCsbNameByType(type);
    mRootNode = CSLoader::createNode(csbName);
    setContentSize(mRootNode->getContentSize());
    
    if(type == EventInfo){
        setupEventInfo(mRootNode);
    }else if(type == LockedInfo){
        setupLockedInfo(mRootNode);
    }else if(type == UnlockInfo){
        setupUnlockInfo(mRootNode);
    }
    
    addChild(mRootNode);
}

void MainSceneInfoLayer::setupEventInfo(cocos2d::Node *rootNode)
{
    Layout* mainPanel =(Layout*) rootNode->getChildByName("mainPanel");
    
    int callbackPlanetID = mPlanetID;
    mainPanel->addClickEventListener([&,callbackPlanetID](Ref*){
        this->mCallBack(callbackPlanetID);
    });

}

void MainSceneInfoLayer::setupLockedInfo(Node* rootNode)
{
    PlanetData* planetData = PlanetManager::instance()->getPlanetData(mPlanetID);
    int callbackPlanetID = mPlanetID;
    Layout* lockedInfoPanel =(Layout*) rootNode->getChildByName("lockedInfoPanel");
    
    lockedInfoPanel->addClickEventListener([&,callbackPlanetID](Ref*){
        this->mCallBack(callbackPlanetID);
    });
    
    std::string planetName = "";
    if(planetData){
        planetName = planetData->getName();
    }
    Text* planetText = lockedInfoPanel->getChildByName<Text*>("planetName");
    planetText->setString(planetName);
}

void MainSceneInfoLayer::setupUnlockInfo(cocos2d::Node *rootNode)
{
    int infoDogID, infoPlanetID, unlockValue, requiredValue;
    DogManager::instance()->getNextUnlockDog(infoDogID, infoPlanetID); //infoPlanetID is useless here
    DogManager::instance()->getUnlockProgress(infoDogID, unlockValue, requiredValue);
    Layout* unLockedInfoPanel =(Layout*) rootNode->getChildByName("unlockInfoPanel");
    
    DogData *dogData = DogManager::instance()->getDogData(infoDogID);
    if(dogData == nullptr) {
        unLockedInfoPanel->setVisible(false);
        return;
    }
    unLockedInfoPanel->setVisible(true);

    //setup callback when panel is clicked
    int callbackPlanetID = PlanetManager::instance()->getPlanetWithDog(infoDogID);
    unLockedInfoPanel->addClickEventListener([&,callbackPlanetID](Ref*){
        this->mCallBack(callbackPlanetID);
    });
    
    //create richText for info
    Text* infoText = unLockedInfoPanel->getChildByName<Text*>("infoText");
    RichTextLabel* unlockInfoText = RichTextLabel::createFromText(infoText);
    unLockedInfoPanel->addChild(unlockInfoText);
    
    //create circular mask for dog icon
    Sprite* dogIconSprite = unLockedInfoPanel->getChildByName<Sprite*>("dogIconSprite");
    CircularMaskNode *progressBar = CircularMaskNode::create();
    progressBar->setPosition(Vec2(dogIconSprite->getContentSize().width/2,dogIconSprite->getContentSize().height/2));
    dogIconSprite->addChild(progressBar);
    
    Sprite *sprite = Sprite::create("guiImage/dselect_countup.png");
    progressBar->addChild(sprite);
    
    //setup dog Icon
    int resID = dogData->getAnimeID();
    std::string iconSpriteName = StringUtils::format("dog/dselect_dog%d.png", resID);
    dogIconSprite->setTexture(iconSpriteName);
    
    //setup progress bar
    unlockValue = unlockValue > requiredValue ? requiredValue : unlockValue;
    float percent;
    if(unlockValue > requiredValue){
        percent = 100;
    }else if(requiredValue <= 0){
        percent = 0;
    }else{
        percent = (float) unlockValue / requiredValue *100;
    }
    progressBar->updateAngleByPercent(percent);
    
    //setup info text
    infoPlanetID = DogManager::instance()->getBestDistancePlanetID(dogData);
    PlanetData* planetData = PlanetManager::instance()->getPlanetData(infoPlanetID);
    
    std::string planetName = "";
    if(planetData){
        planetName = planetData->getName();
    }
    std::string dogName = dogData->getName();
    std::string progress = StringUtils::format("<c1>%d</c1>/%d",unlockValue,requiredValue);
    
    std::string infoStr = dogData->getUnlockInfo() + " to unlock <c2>#dog</c2>";
    aurora::StringHelper::replaceString(infoStr, "#planet", planetName);
    aurora::StringHelper::replaceString(infoStr, "#v", progress);
    aurora::StringHelper::replaceString(infoStr, "#dog", dogName);
    
    unlockInfoText->setTextColor(2, Color4B(255,211,49,255));
    unlockInfoText->setTextColor(1, Color4B(14,255,217,255));
    
    unlockInfoText->setString(infoStr);
}

void MainSceneInfoLayer::setCallback(std::function<void (int)> callback)
{
    mCallBack = callback;
}

