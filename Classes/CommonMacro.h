//
//  Header.h
//  GhostLeg
//
//  Created by Ken Lee on 21/3/2016.
//
//

#ifndef CommonMacro_H
#define CommonMacro_H


#define NS_AURORA_BEGIN                     namespace aurora {
#define NS_AURORA_END                       }

#define IS_PAIDAPP							(PAID == 1)
#define IS_ANDROID							(CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define IS_IOS								(CC_TARGET_PLATFORM == CC_PLATFORM_IOS)

#define CCVECTOR_INFO(__type__, __vectorVar__, __outStr__) \
__outStr__ += StringUtils::format("count: %ld\n", __vectorVar__.size()); \
for(int i=0; i<__vectorVar__.size(); i++) { \
	__type__ * obj = __vectorVar__.at(i); \
	__outStr__ += obj->toString(); \
	__outStr__ += "\n"; \
}



#define FIX_UI_LAYOUT(__uiPanel__) \
__uiPanel__->setContentSize(getContentSize()); \
cocos2d::ui::Helper::doLayout(__uiPanel__);


#define SYNTHESIZE_BOOL(varName, funName)\
protected: bool varName;\
public: virtual bool is##funName(void) const { return varName; }\
public: virtual void set##funName(bool var){ varName = var; }


#define CREATE_FUNC_ARG(__TYPE__,ArgType) \
static __TYPE__* create(ArgType arg) \
{ \
    __TYPE__ *pRet = new(std::nothrow) __TYPE__(arg); \
    if (pRet && pRet->init()) \
    { \
        pRet->autorelease(); \
        return pRet; \
    } \
    else \
    { \
        delete pRet; \
        pRet = nullptr; \
        return nullptr; \
    } \
}

#endif /* Header_h */

