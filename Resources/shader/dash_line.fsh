varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

void main() {
	
//	float y = gl_FragCoord.xy.y + v_texCoord.y; // + mod(CC_Time[3], 50.0) * 20.0;
//	float q = floor(y / 50.0);
//	float rv = y - q * 50.0;
//	
	
	float rv = mod(gl_FragCoord.xy.y, 50.0);
	float c = rv  / 50.0;
	
	float r, g, b, a;
	r = v_fragmentColor.x;
	g = v_fragmentColor.y;
	b = v_fragmentColor.z;
	a = 1.0;
	
	if(c < 0.5) {
		a = 0.0;
		r = 0.0;
		g = 0.0;
		b = 0.0;
	}

	
	gl_FragColor = vec4(r, g, b, a);
}