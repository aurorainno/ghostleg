

precision mediump float;
varying vec4 v_position;
varying vec4 v_aposition;
varying vec2 v_texCoord;
varying vec4 v_fragmentColor;

void main() {
	
	
	//float dist = abs(distance(v_sourcePoint.xy, v_position.xy));
	//gl_Position = CC_MVPMatrix * a_position;
	
	float dash_size = 10.0;
	float half_dash = dash_size / 2.0;
	
	float y = v_aposition.xy.y;
	
	float modDist = mod(y, dash_size);
	
	if (modDist > half_dash) {
		gl_FragColor = vec4(0,0,0,0);
	} else {
		float r = v_fragmentColor.x;
		float g = v_fragmentColor.y;
		float b = v_fragmentColor.z;
		float a = 1.0; // sin(CC_Time[3]);
		
		gl_FragColor = vec4(r, g, b, a);
	}
}
