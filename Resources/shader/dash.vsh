attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;

#ifdef GL_ES
varying lowp vec4 v_fragmentColor;
varying mediump vec2 v_texCoord;
varying mediump vec4 v_position;
varying mediump vec4 v_aposition;
#else
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
varying vec4 v_position;
varying vec4 v_aposition;
#endif

void main()
{
	gl_Position = CC_MVPMatrix * a_position;
	v_fragmentColor = a_color;
	v_texCoord = a_texCoord;
	v_position = gl_Position;
	v_aposition = a_position;
}