varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

// Reference: http://cocos2d-x.org/docs/programmers-guide/13/index.html

void main() {

	float r, g, b, a;
	r = v_fragmentColor.x;
	g = v_fragmentColor.y;
	b = v_fragmentColor.z;
	a = sin(CC_Time[3]);
	
	
	gl_FragColor = vec4(r, g, b, a);
}