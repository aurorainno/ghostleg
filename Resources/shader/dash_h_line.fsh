varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

void main() {
	
	
	float dash_size = 20.0;
	float half_dash = dash_size / 2.0;

	
	float x = gl_FragCoord.xy.x;
	float mod_x = mod(x, dash_size);
	
	float r, g, b, a;
	
	
	if(mod_x > half_dash) {
		a = 0.0;
		r = 0.0;
		g = 0.0;
		b = 0.0;
	} else {
		r = v_fragmentColor.x;
		g = v_fragmentColor.y;
		b = v_fragmentColor.z;
		a = 0.5; // sin(CC_Time[3]);
	}
	
	
	gl_FragColor = vec4(r, g, b, a);
}