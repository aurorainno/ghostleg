// Shader from http://www.iquilezles.org/apps/shadertoy/

#ifdef GL_ES
precision highp float;
#endif

uniform vec2 center;
uniform vec2 resolution;

void main(void)
{
	float y = gl_FragCoord.xy.y;
	float q = floor(y / 50.0);
	float rv = y - q * 50.0;
	float c = rv  / 30.0;

	float r, g, b, a;
	if(c > 0.5) {
		r = 0.0; g=1.0; b=1.0; a=1.0;  // longer part
	} else {
		r = 1.0; g=1.0; b=0.0; a=0.0; // shorter part
	}


	gl_FragColor = vec4(r, g, b , a);

}
