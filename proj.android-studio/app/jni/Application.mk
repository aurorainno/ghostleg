APP_STL := gnustl_static
#APP_STL

APP_CPPFLAGS := -frtti \
-DCC_ENABLE_CHIPMUNK_INTEGRATION=1 \
-std=c++11 \
-fsigned-char
APP_LDFLAGS := -latomic


ifeq ($(NDK_DEBUG),1)
# Debug Build
APP_CPPFLAGS += -DCOCOS2D_DEBUG=1
APP_CPPFLAGS += -DENABLE_TDD=1
APP_OPTIM := debug
else
# release Build
APP_CPPFLAGS += -DNDEBUG
APP_CPPFLAGS += -DPAID_APP=0
APP_CPPFLAGS += -DDISABLE_AD=0
APP_OPTIM := release
endif
APP_PLATFORM := android-15
