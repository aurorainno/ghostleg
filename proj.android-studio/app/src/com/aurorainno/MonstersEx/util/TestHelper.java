package com.aurorainno.MonstersEx.util;

import android.util.Log;

/**
 * Created by kenlee on 3/8/2016.
 */
public class TestHelper {
    public static void testLog() {      // will be called by cocos2d-x code in testing
        Log.d("TestHelper", "Testing !!!");
    }

    public static void testLogMessage(String msg) {      // will be called by cocos2d-x code in testing
        Log.d("TestHelper", "Msg recv. msg=" + msg);
    }

    public static String testGetString() {      // will be called by cocos2d-x code in testing
        Log.d("TestHelpers", "Get String !!!");

        return "Hello Cocos2d-x";
    }
}
