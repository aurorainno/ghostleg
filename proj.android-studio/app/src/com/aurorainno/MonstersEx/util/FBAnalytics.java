package com.aurorainno.MonstersEx.util;

import android.util.Log;

import com.facebook.appevents.AppEventsLogger;

import org.cocos2dx.lib.Cocos2dxActivity;

public class FBAnalytics {
	private static final String TAG = "FBAnalytics";

	public static void logEvent(String event)
	{
		Log.e(TAG, "logEvent: event=" + event);
		AppEventsLogger.newLogger(Cocos2dxActivity.getContext()).logEvent(event);
	}
}