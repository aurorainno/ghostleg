package com.aurorainno.MonstersEx.util;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import org.cocos2dx.cpp.AppActivity;

public class ShareHelper {



	private static final String TAG = "MonstesEx: ShareHelper";

	public static void shareScoreImage(String content)
	{
		shareImage(content,"monsters_ex_share.png", false);
	}

	public static void shareImage(String content, String imageFile, boolean isInternal) {

		String sharePath = DeviceHelper.getExternalPath() + "/" + imageFile;	// Other app only can get extern path

						// Environment.getDataDirectory().getAbsolutePath()
						// : DeviceHelper.getExternalPath();

		Log.d(TAG, "ShareImage: sharePath=" + sharePath);

		if(isInternal) {
			String src = AppActivity.getContext().getFilesDir().getAbsolutePath() + "/" + imageFile;
			FileHelper.deleteFile(sharePath);

			boolean isOkay = FileHelper.copyFile(src, sharePath);
			Log.d(TAG, "copy " + src + " to " + sharePath);
			if(isOkay == false) {
				Log.d(TAG, "Fail to copy image");
			} else {
				Log.d(TAG, "success");
			}

		}

		//String externPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		//Uri imageUri = Uri.parse("file://" + externPath + "/temp/test.jpg");

		Uri imageUri = Uri.parse("file://" + sharePath);
		// Uri imageUri = Uri.parse("file://" + path + "/" + imageFile);

		Log.d(TAG, "ShareImage: imageUri=" + imageUri);


		// This is working on Twitter, Whatsapp but not Facebook
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, content);
		sendIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
		
		sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
		sendIntent.setType("image/jpeg");


		
		//
		//ActivityHelper.startActivityFromMain(sendIntent);
		ActivityHelper.openChooser(sendIntent);
	}

}
