package com.aurorainno.MonstersEx.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.telephony.TelephonyManager;

import org.cocos2dx.cpp.AppActivity;

public class DeviceHelper {
	private static final String TAG = "MonstersEx.DeviceHelper";
	public static String pathName = "monstersEx";

	public static String getExternalPath() {
		String path = Environment.getExternalStorageDirectory().getAbsolutePath()
				      + "/" + pathName;

		Log.d(TAG, "externalPath=" + path);

		return path;
	}

	public static String getBuildNumber(){
		Context context = AppActivity.getContext();
		PackageInfo pInfo = null;
		try {
			pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		int buildNum = pInfo.versionCode;
		return String.valueOf(buildNum);
	}

	public static String getCountryCode(){
		Context context = AppActivity.getContext();
		TelephonyManager tm = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
		String countryCode = tm.getNetworkCountryIso();
		countryCode = countryCode.toUpperCase();
		Log.d(TAG, "countryCode=" + countryCode);
		return countryCode;
	}

	/**
	 * https://developer.android.com/training/monitoring-device-state/connectivity-monitoring.html
	 *
	 *
	 * @return
     */
	public static String getNetworkConnectStatus()
	{
		Context context = AppActivity.getContext();
		if(context == null) {
			Log.d(TAG, "isNetworkConnect: context is null");
			return "error";
		}

		ConnectivityManager cm =
				(ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if(cm == null) {
			Log.d(TAG, "isNetworkConnect: ConnectivityManager is null");
			return "error";
		}

		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		if(activeNetwork == null) {
			Log.d(TAG, "isNetworkConnect: NetworkInfo is null");
			return "error";
		}

		boolean isConnected = activeNetwork != null &&
									activeNetwork.isConnectedOrConnecting();

		return (isConnected) ? "connected" : "disconnect";

	}

}
