package com.aurorainno.MonstersEx.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;

public class FileHelper {
	public static void writeFile(String filename, String content) throws Exception 
	{
		File file = new File(filename);
		
		FileOutputStream fs = new FileOutputStream(file);
		PrintWriter writer = new PrintWriter(fs);
		  
		writer.append(content);
		writer.flush();
		
		fs.close();
		writer.close();
	}
	
	public static String readFile(String filename) throws Exception {
	    File file = new File(filename);

	    int byteread = 0;
	    
	    StringBuilder builder = new StringBuilder();
	    
	    InputStream inStream = new FileInputStream(file);
        byte[] buffer = new byte[1444];
        while ((byteread = inStream.read(buffer)) != -1) {
        	// Log.d("TDD", "byteread=" + byteread);
        	builder.append(new String(buffer, 0, byteread));
        }
        inStream.close();
        
        return builder.toString();
	}
	
	public static boolean copyFile(String from, String to) {
		try {
		    int bytesum = 0;
		    int byteread = 0;
		    File oldfile = new File(from);		
		    
		    if (oldfile.exists()) {
		        InputStream inStream = new FileInputStream(from);
		        FileOutputStream fs = new FileOutputStream(to);
		        byte[] buffer = new byte[1444];
		        while ((byteread = inStream.read(buffer)) != -1) {
		            bytesum += byteread;
		            fs.write(buffer, 0, byteread);
		        }
		        inStream.close();
		        fs.flush();
		        fs.close();
		    }
		    return true;
		} catch (Exception e) {
			e.printStackTrace();
		    return false;
		}
	}

	public static void deleteFile(String sharePath) {
		File file = new File(sharePath);
		if(! file.exists()) {
			return;
		}
		
		file.delete();
	}
}
