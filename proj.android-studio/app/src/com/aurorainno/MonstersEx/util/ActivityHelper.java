package com.aurorainno.MonstersEx.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.cocos2dx.cpp.AppActivity;

public class ActivityHelper {
	private static final String TAG = "ActivityHelper";

	public static boolean startActivityFromMain(Intent intent) {
		if (intent == null) {
			Log.e(TAG, "ActivityHelper: startActivityFromMain: intent is null");
			return false;
		}

		Context callerContext = AppActivity.getContext();
		if ((callerContext instanceof Activity) == false) {
			Log.e(TAG, "ActivityHelper: callerContext is not Activity");
			return false;
		}
		Activity callerActivity = (Activity) callerContext;

		callerActivity.startActivity(intent);

		return true;
	}


	public static boolean openChooser(Intent intent) {
		if (intent == null) {
			Log.e(TAG, "ActivityHelper: openChooser: intent is null");
			return false;
		}

		Context callerContext = AppActivity.getContext();
		if ((callerContext instanceof Activity) == false) {
			Log.e(TAG, "ActivityHelper: callerContext is not Activity");
			return false;
		}
		Activity callerActivity = (Activity) callerContext;


		if(intent.resolveActivity(callerActivity.getPackageManager()) == null) {
			return false;
		}

		callerActivity.startActivity(Intent.createChooser(intent, "Share"));

		return true;
	}
}
