#!/usr/bin/python

import os
import sys
import shutil           # for file copy
import subprocess
import re

modeArg = ""
if len(sys.argv) >= 2:
	modeArg = sys.argv[1]
else:
	modeArg = "debug"

print "Build Mode: %s" % modeArg

# Refresh the source list 
os.system("./app/list_src.sh")


# Define the mode
if (modeArg == "debug"):
	mode = "--mode debug"
else : 
	mode = "--mode release"

#print "DEBUG: mode=%s" % mode

# Build the command 
cmd = "cocos compile -p android --android-studio %s" % mode

print "CMD:%s" % cmd
status = os.system(cmd)
print "Status: %d" % status;

