#!/bin/sh

# Generate the key hash for facebook android integration
# https://developers.facebook.com/quickstarts/321143354748231/?platform=android
# keytool -exportcert -alias androiddebugkey -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64


keytool -exportcert -alias androiddebugkey \
    -keystore ~/.android/debug.keystore | openssl sha1 -binary | openssl base64
