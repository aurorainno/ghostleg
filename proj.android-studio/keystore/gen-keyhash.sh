#!/bin/sh

# Generate the key hash for facebook android integration
# https://developers.facebook.com/quickstarts/321143354748231/?platform=android

RELEASE_KEY_ALIAS=aurorainno
RELEASE_KEY_PATH=aurora-release-key.keystore

keytool -exportcert -alias $RELEASE_KEY_ALIAS \
	-keystore $RELEASE_KEY_PATH | openssl sha1 -binary | openssl base64
