#!/bin/sh

# Generate the key hash for facebook android integration
# keytool -list -v -keystore mystore.keystore

RELEASE_KEY_ALIAS=aurorainno
RELEASE_KEY_PATH=aurora-release-key.keystore

keytool -list -v -keystore $RELEASE_KEY_PATH
