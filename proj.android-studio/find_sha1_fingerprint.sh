#!/bin/sh

ALIAS=$1
KEYSTORE=$2

keytool -exportcert -alias $ALIAS -keystore $KEYSTORE -list -v
