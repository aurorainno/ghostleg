

# Json
import json


# Read the content of the given file
def pretty_print(jsonObj):
    print json.dumps(jsonObj, sort_keys=True, indent=4, separators=(',', ': '));
