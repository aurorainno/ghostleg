# Helper method for File


# Read the content of the given file
def read_file(filename):
  fo = open(filename, "r");
  str = fo.read();
  fo.close();
  return str;

def write_file(filename, content):
  fo = open(filename, "w");
  fo.write(content);
  fo.close();
