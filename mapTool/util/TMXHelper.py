

import pytmx
from pytmx import *




# Read the content of the given file
def find_tmx_layer_by_name(tileMap, name):
    for layer in tileMap.layers:
        if(name == layer.name):
            return layer;


    return None;
