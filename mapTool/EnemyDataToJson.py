import os;
import json;
from FileHelper import *;

# input: datafile
# output: array of key-value array

def modifyEnemyData(inData):
    result = {};

    result["id"] = int(inData["type"]);

    # Same Key value
    copyList = ["res", "behaviour", "active"];
    for item in copyList:
        result[item] = inData[item];

    # Monster ID
    result["speedX"] = [inData["speedX"]];  # change to array
    result["speedY"] = [inData["speedY"]];  # change to array

    return result;

def parseEnemyDataLine(line):
    data = {};

    tokens = line.split(" ");

    for token in tokens:
        nvpair = token.split("=");
        data[nvpair[0]] = nvpair[1];

    return data;

def readEnemyData(enmeyDataFile):
    result = {};
    content = read_file(enmeyDataFile);
    # print "%s" % content;

    lines = content.split("\n");
    for line in lines:
        if(len(line) == 0):
            continue;
        if(line.startswith("#")):
            continue;

        # print "%s" % line;
        data = parseEnemyDataLine(line);
        finalData = modifyEnemyData(data);

        key = "enemy_%02d" % finalData["id"];
        result[key] = finalData;

    return result;

def convertDataToJSON(enmeyData):
    jsonStr = json.dumps(enmeyData, sort_keys=True, indent=4);
    return jsonStr;

def convertEnemyData(input, output):
    result = readEnemyData(input);

    json = convertDataToJSON(result);
    
    write_file(output, json);
