<?xml version="1.0" encoding="UTF-8"?>
<tileset name="model" tilewidth="25" tileheight="25" tilecount="33" columns="0">
 <tile id="0">
  <image width="25" height="25" source="tile/01.png"/>
 </tile>
 <tile id="1">
  <image width="25" height="25" source="tile/02.png"/>
 </tile>
 <tile id="2">
  <image width="25" height="25" source="tile/03.png"/>
 </tile>
 <tile id="5">
  <image width="25" height="25" source="tile/04.png"/>
 </tile>
 <tile id="6">
  <image width="25" height="25" source="tile/05.png"/>
 </tile>
 <tile id="7">
  <image width="25" height="25" source="tile/06.png"/>
 </tile>
 <tile id="8">
  <image width="25" height="25" source="tile/07.png"/>
 </tile>
 <tile id="9">
  <image width="25" height="25" source="tile/08.png"/>
 </tile>
 <tile id="10">
  <image width="25" height="25" source="tile/09.png"/>
 </tile>
 <tile id="11">
  <image width="25" height="25" source="tile/10.png"/>
 </tile>
 <tile id="12">
  <image width="25" height="25" source="tile/11.png"/>
 </tile>
 <tile id="13">
  <image width="25" height="25" source="tile/12.png"/>
 </tile>
 <tile id="14">
  <image width="25" height="25" source="tile/item_01.png"/>
 </tile>
 <tile id="15">
  <image width="25" height="25" source="tile/item_02.png"/>
 </tile>
 <tile id="16">
  <image width="25" height="25" source="tile/item_03.png"/>
 </tile>
 <tile id="17">
  <image width="25" height="25" source="tile/13.png"/>
 </tile>
 <tile id="18">
  <image width="25" height="25" source="tile/14.png"/>
 </tile>
 <tile id="19">
  <image width="25" height="25" source="tile/15.png"/>
 </tile>
 <tile id="20">
  <image width="25" height="25" source="tile/16.png"/>
 </tile>
 <tile id="21">
  <image width="25" height="25" source="tile/17.png"/>
 </tile>
 <tile id="22">
  <image width="25" height="25" source="tile/18.png"/>
 </tile>
 <tile id="23">
  <image width="25" height="25" source="tile/20.png"/>
 </tile>
 <tile id="24">
  <image width="25" height="25" source="tile/19.png"/>
 </tile>
 <tile id="25">
  <image width="25" height="25" source="tile/item_11.png"/>
 </tile>
 <tile id="26">
  <image width="25" height="25" source="tile/21.png"/>
 </tile>
 <tile id="27">
  <image width="25" height="25" source="tile/22.png"/>
 </tile>
 <tile id="28">
  <image width="25" height="25" source="tile/23.png"/>
 </tile>
 <tile id="36">
  <image width="25" height="25" source="tile/24.png"/>
 </tile>
 <tile id="37">
  <image width="25" height="25" source="tile/25.png"/>
 </tile>
 <tile id="38">
  <image width="25" height="25" source="tile/26.png"/>
 </tile>
 <tile id="42">
  <image width="25" height="25" source="tile/item_21.png"/>
 </tile>
 <tile id="43">
  <image width="25" height="25" source="tile/item_22.png"/>
 </tile>
 <tile id="44">
  <image width="25" height="25" source="tile/item_23.png"/>
 </tile>
</tileset>
