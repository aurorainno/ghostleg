<?xml version="1.0" encoding="UTF-8"?>
<tileset name="item" tilewidth="25" tileheight="25" tilecount="2" columns="0">
 <tile id="0">
  <properties>
   <property name="type" type="int" value="200"/>
   <property name="resID" type="int" value="1"/>
  </properties>
  <image width="25" height="25" source="tile/coin.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="type" type="int" value="201"/>
   <property name="resID" type="int" value="2"/>
  </properties>
  <image width="25" height="25" source="tile/potion.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="type" type="int" value="202"/>
   <property name="resID" type="int" value="2"/>
  </properties>
  <image width="25" height="25" source="tile/capsule.png"/>
 </tile>
</tileset>
