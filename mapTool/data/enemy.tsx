<?xml version="1.0" encoding="UTF-8"?>
<tileset name="enemy" tilewidth="25" tileheight="25" tilecount="12" columns="0">
 <tile id="0">
  <image width="25" height="25" source="tile/01.png"/>
 </tile>
 <tile id="1">
  <image width="25" height="25" source="tile/02.png"/>
 </tile>
 <tile id="2">
  <image width="25" height="25" source="tile/03.png"/>
 </tile>
 <tile id="5">
  <image width="25" height="25" source="tile/04.png"/>
 </tile>
 <tile id="6">
  <image width="25" height="25" source="tile/05.png"/>
 </tile>

 <tile id="7">
 <image width="25" height="25" source="tile/06.png"/>
 </tile>

 <tile id="8">
  <image width="25" height="25" source="tile/07.png"/>
 </tile>

 <tile id="9">
  <image width="25" height="25" source="tile/08.png"/>
 </tile>

 <tile id="10">
  <image width="25" height="25" source="tile/09.png"/>
 </tile>

 <tile id="11">
  <image width="25" height="25" source="tile/10.png"/>
 </tile>

 <tile id="12">
  <image width="25" height="25" source="tile/11.png"/>
 </tile>

 <tile id="13">
  <image width="25" height="25" source="tile/12.png"/>
 </tile>

 <tile id="14">
  <image width="25" height="25" source="tile/item_01.png"/>
 </tile>
 <tile id="15">
  <image width="25" height="25" source="tile/item_02.png"/>
 </tile>
 <tile id="16">
  <image width="25" height="25" source="tile/item_03.png"/>
 </tile>
</tileset>
