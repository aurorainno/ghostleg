<?xml version="1.0" encoding="UTF-8"?>
<tileset name="lines" tilewidth="25" tileheight="25" tilecount="4" columns="0">
 <properties>
  <property name="type" value="0"/>
 </properties>
 <tile id="0">
  <properties>
   <property name="probability" value="100"/>
   <property name="type" value="1"/>
  </properties>
  <image width="25" height="25" source="tile/line1.png"/>
 </tile>
 <tile id="1">
  <image width="25" height="25" source="tile/vertLine.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="probability" value="50"/>
   <property name="type" value="2"/>
  </properties>
  <image width="25" height="25" source="tile/line2.png"/>
 </tile>
 <tile id="3">
  <properties>
  <property name="probability" value="50"/>
  <property name="type" value="3"/>
  </properties>
  <image width="25" height="25" source="tile/line3.png"/>
 </tile>
 <tile id="4">
  <properties>
  <property name="probability" value="50"/>
  <property name="type" value="4"/>
  </properties>
  <image width="25" height="25" source="tile/line4.png"/>
 </tile>
 <tile id="5">
  <properties>
  <property name="probability" value="50"/>
  <property name="type" value="5"/>
  </properties>
  <image width="25" height="25" source="tile/line5.png"/>
 </tile>
</tileset>
