#!/usr/bin/python
# Note: Run the test at root path

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')
sys.path.append('./logic/')

#from LevelDataCreator import *
from LevelData import *
from LevelDataCreator import *

def test_line_data():
    lineData = LineData(1, 2, 0, 200);
    print "%s" % lineData.toString();

def test_creator():
    tmxFile = "./sample/level0.tmx";
    levelCreator = LevelDataCreator(tmxFile);

    levelCreator.process();

    print "%s" % levelCreator.info();

def test_getProperty():
    tmxFile = "./sample/level0.tmx";
    levelCreator = LevelDataCreator(tmxFile);

    result = levelCreator.getTileProperty(2, "type", "");

    print "result=%s" % result;

    value = levelCreator.getTileIntProperty(2, "type", -1);

    print "value=%s" % value;

def test_content():
    tmxFile = "./data/map0.tmx";
    levelCreator = LevelDataCreator(tmxFile);

    levelCreator.process();
    print "%s" % levelCreator.getDataContent();


def test_mapName():
    tmxFile = "./data/map41.tmx";
    levelCreator = LevelDataCreator(tmxFile);

    mapName = levelCreator.parseMapName(tmxFile);
    print "%s" % mapName;

    #levelCreator.process();
    #print "%s" % levelCreator.getDataContent();


def test_save():
    tmxFile = "./data/map41.tmx";
    levelCreator = LevelDataCreator(tmxFile);
    levelCreator.setOutputPath("./test/output");

    levelCreator.process();

    levelCreator.save();

def test_getPx():
    tmxFile = "./data/map41.tmx";
    levelCreator = LevelDataCreator(tmxFile);

    startGx = 0;
    endGx = 15;

    for gx in range(startGx, endGx+1):
        px = levelCreator.getPx(gx);
        print "gx=%d px=%d" % (gx, px);


def test_parseItemID():
    tmxFile = "./data/map41.tmx";
    levelCreator = LevelDataCreator(tmxFile);

    itemArray = {"item_01.png", "item_02.png", "item_03.png"};

    for itemName in itemArray:
        itemID = levelCreator.parseItemID(itemName);
        print "%s -> %d" % (itemName, itemID);

def test_generator():
    tmxFile = "./data/map20000.tmx";
    levelCreator = LevelDataCreator(tmxFile);
    levelCreator.process();

    print "%s" % (levelCreator.infoItemData());

test_generator();
# test_save();
# test_mapName();
# test_getPx();
#test_parseItemID();
# test_content();
#test_getProperty();
#test_creator();
#test_line_data();
