#!/usr/bin/python
# Unit Test for Map Convertor

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')
sys.path.append('./logic/')

#from LevelDataCreator import *
from LevelData import *
from MapDataConvertor import *

def test_line_data():
    tmxFile = "./mapTmx/map1.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.parseLineData();

    print "%s" % mapConv.infoLineData();

def test_item_data():
    tmxFile = "./mapTmx/map2007.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.processItemLayer();

    print "%s" % mapConv.infoItemData();

def test_enemy_data():
    #  tmxFile = "./mapTmx/sample.tmx";
    # tmxFile = "./mapTmx/map10003.tmx";
    tmxFile = "./mapTmx/map20004.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.processEnemyLayer();

    print "%s" % mapConv.infoEnemyData();

def test_process():
    #tmxFile = "./mapTmx/sample.tmx";
    #tmxFile = "./mapTmx/map10002.tmx";
    tmxFile = "./mapTmx/map2.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.process();

    print "%s" % mapConv.info();

def test_save():
    #tmxFile = "./mapTmx/sample.tmx";
    tmxFile = "./mapTmx/map10002.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.outputPath = "./mapData/";
    mapConv.process();
    mapConv.save();

    print "%s" % mapConv.info();

def test_defineVertiLinePosition():
    tmxFile = "./mapTmx/sample.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.defineVertiLinePosition();

def test_getNearestLineX():
    test = [10, 20, 60, 100, 200, 220, 300, 350, 400, 450];

    tmxFile = "./mapTmx/sample.tmx";
    mapConv = MapDataConvertor(tmxFile);

    for testX in test:
        lineX = mapConv.getNearestLineX(testX);
        print "testX=%d lineX=%d" % (testX, lineX);
        # break;

def test_getAnchoredPosition():
    tmxFile = "./mapTmx/sample.tmx";
    mapConv = MapDataConvertor(tmxFile);

    testArray = [];
    testArray.append([41, 194, 100, 63, 0.7, 0, 0, 0]);
    testArray.append([156, 215, 100, 63, 0.7, 0, 1, 0]);

    for (refX, refY, width, height, anchorX, anchorY, flipX, flipY) in testArray:
        print "DEBUG: %d %d %d %d" % (refX, refY, width, height);

        leftBottomPos = [refX, refY];
        anchorPoint = [anchorX, anchorY];
        imageSize = [width, height];
        scale = [1.0, 1.0];

        (finalX, finalY) = mapConv.getAnchoredPosition(leftBottomPos, anchorPoint,
                            imageSize, scale, flipX, flipY);

        print "newPos=%f, %f" % (finalX, finalY);


def test_getAnchoredPosition_debug():
    tmxFile = "./mapTmx/sample.tmx";
    mapConv = MapDataConvertor(tmxFile);

    testArray = [];
    testArray.append([-40,630, 0.5,0.5,  320,306, 0.656,0.5032, 1, 0]);
    # testArray.append([156, 215, 100, 63, 0.7, 0, 1, 0]);

    for (refX, refY, anchorX, anchorY, width, height, scaleX, scaleY, flipX, flipY) in testArray:
        print "DEBUG: %d %d %d %d" % (refX, refY, width, height);

        leftBottomPos = [refX, refY];
        anchorPoint = [anchorX, anchorY];
        imageSize = [width, height];
        scale = [scaleX, scaleY];

        (finalX, finalY) = mapConv.getAnchoredPosition(1, leftBottomPos, anchorPoint,
                            imageSize, scale, flipX, flipY);

        print "newPos=%f, %f" % (finalX, finalY);


def test_npc_data():
    #  tmxFile = "./mapTmx/sample.tmx";
    # tmxFile = "./mapTmx/map10003.tmx";
    tmxFile = "./mapTmx/npc100.tmx";
    mapConv = MapDataConvertor(tmxFile);
    mapConv.processEnemyLayer();

    print "%s" % mapConv.infoEnemyData();
    print "%s" % mapConv.infoNpcData();
    print "=========\n%s\n", mapConv.getDataContent();

# test_getNearestLineX();
# test_defineVertiLinePosition();
# test_line_data();
test_item_data();
#test_enemy_data();
# test_process();
#test_save();
#test_getAnchoredPosition();
#test_getAnchoredPosition_debug();
# test_npc_data();
