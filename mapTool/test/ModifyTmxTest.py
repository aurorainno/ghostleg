#!/usr/bin/python
# Note: Run the test at root path

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')
sys.path.append('./logic/')

from ModifyTmx import *

def test_convert():
  print "Testing TMX Info";
  tmxFile = "./data/map2.tmx";

  tmxModifier = TMXModifier(tmxFile);
  tmxModifier.outputPath = "./output";

  tmxModifier.convertMapHeight(24, 50)

  tmxModifier.saveContent();

  print "CONTENT\n%s" % (tmxModifier.content);

def test_content():
  print "Testing TMX Info";
  tmxFile = "./data/map2.tmx";
  tmxModifier = TMXModifier(tmxFile);
  tmxModifier.outputPath = "./output";

  print "CONTENT\n%s" % (tmxModifier.content);

def test_info():
  print "Testing TMX Info";
  tmxFile = "./data/map2.tmx";
  tmxModifier = TMXModifier(tmxFile);
  tmxModifier.outputPath = "./output";

  print "INFO\n%s" % (tmxModifier.info());

def test_converFolder():
    srcPath = "./backup";
    dstPath = "./data";

    convertTmxFolderForHeight(srcPath, dstPath, 24, 50);

# test_info();
# test_content();
# test_convert();
test_converFolder();
