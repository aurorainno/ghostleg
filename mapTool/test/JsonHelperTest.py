#!/usr/bin/python
# Note: Run the test at root path

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')

# Json
import json

# Our library
from FileHelper import *
from JsonHelper import *


def test_json():
  content = read_file("./sample/level0.json")

  jsonObj = json.loads(content);

  print json.dumps(jsonObj, sort_keys=True, indent=4, separators=(',', ': '));


def test_json2():
  content = read_file("./sample/level0.json")

  jsonObj = json.loads(content);

  pretty_print(jsonObj);


# Start of testing
# test_json();
test_json2();
