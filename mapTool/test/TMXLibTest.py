#!/usr/bin/python
# Note: Run the test at root path

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')

from TMXHelper import *

import pytmx
from pytmx import *

def test_load_map():
    tileMap = pytmx.TiledMap("./sample/level0.tmx");

    for layer in tileMap.layers:
        print "layer=%s name=%s" % (layer, layer.name);
        print " prop=%s" % layer.properties;

    print "";


def test_layer():
    tileMap = pytmx.TiledMap("./sample/level0.tmx");

    for gid, props in tileMap.tile_properties.items():
        print "gid=%d props=%s" % (gid, props);

    layer = tileMap.layers[1]
    print "name=%s" % layer.name;
    for x, y, gid in layer:

        print "x=%d y=%d gid=%d" % (x, y, gid)


    print "";

def test_find_layer_by_name():

    tileMap = pytmx.TiledMap("./sample/level0.tmx");

    name = "Line Layer";
    layer = find_tmx_layer_by_name(tileMap, name);
    if(layer is None) :
        print "not found %s" % name;
    else:
        print "name=%s find=%s" % (layer.name, name);

    name = "xxx";
    layer = find_tmx_layer_by_name(tileMap, name);
    if(layer is None) :
        print "not found %s" % name;
    else:
        print "name=%s find=%s" % (layer.name, name);

def test_tileset():
    # tileMap = pytmx.TiledMap("./data/map0.tmx");

    print "";

    #
    # for layer in tileMap.layers:
    #     print "layer=%s name=%s" % (layer, layer.name);
    #     print " prop=%s" % layer.properties;
    #
    # print "";

# Testing

#test_find_layer_by_name();
test_layer();
#test_load_map();
