#!/usr/bin/python
# Note: Run the test at root path
# Testing For EnemyDataToJson.py

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')
sys.path.append('./')

from EnemyDataToJson import *

def test_readEnemyData():
  inputFile = "./data/enemy.dat"

  result = readEnemyData(inputFile);

  print "Result\n";
  for key in result:
      print "%s" % key;
      print "%s" % result[key];

def test_convertDataToJSON():
    inputFile = "./data/enemy.dat"

    result = readEnemyData(inputFile);
    json = convertDataToJSON(result);

    print "%s" % json;

def test_modifyEnemyData():
    inData = {'res': '3', 'behaviour': 'freeMove', 'speedY': ['-95'],
            'speedX': ['0'], 'active': 'topY:-50', 'id': 3};

    result = modifyEnemyData(inData);

    print "%s" % result;

def test_convertEnemyData():
    inputFile = "./data/enemy.dat"
    outputFile = "./data/enemy.json"

    convertEnemyData(inputFile, outputFile);


# Testing Call
test_convertEnemyData();
# test_readEnemyData();
# test_convertDataToJSON();
# test_modifyEnemyData();
