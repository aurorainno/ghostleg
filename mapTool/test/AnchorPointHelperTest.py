#!/usr/bin/python
# Note: Run the test at root path

import sys
# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./logic/')

from AnchorPointHelper import *


def test_find_anchor_point_from_svg():
    #filename = "anchor/test_anchor.svg";
    filename = "anchor/enemy_04.svg";

    print "filename: %s" % filename;
    (anchorX, anchorY) = find_anchor_point_from_svg(filename); 

    print "Anchor: %f, %f" % (anchorX, anchorY);

test_find_anchor_point_from_svg();
