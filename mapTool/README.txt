


# Dependency

* TMX Library:
  https://github.com/bitcraft/PyTMX
  - Download from github
  - Install the library cmd: "sudo python setup.py install"
  doc: http://pytmx.readthedocs.org/en/latest/



= Output Format =

filename: level_$level.dat

== Content Format ==
[Line]
lineId,xLine,y

[Enemy]
type,x,y,lineId

[Item]
type,x,y

== Content Example ==
[Line]
1,0,50
2,2,400

[Enemy]
1,30,75
2,0,125

[Item]
1,50,100
2,0,100


== Object Format in Cocos2d-x ==
struct {
    int id
    int xLine,
    int y,
} LevelDataLine;


struct {
    int type,
    int x,
    int y,
    int dockingLine;    // id of the line docking on
} LevelDataEnemy

struct {
    int type,
    int x,
    int y
} LevelDataItem;

class {
  Vector<LevelDataLine> lineData;
  Vector<LevelDataEnemy> enemyData;
  Vector<LevelDataItem> itemData;


}
