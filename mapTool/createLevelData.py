#!/usr/bin/python
# Note: Run the test at root path

import sys
import os
import shutil			# for file copy


# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')
sys.path.append('./logic/')

#from LevelDataCreator import *
from LevelData import *
from LevelDataCreator import *


def getTMXList(inputPath):

    result = [];

    for path, dirs, files in os.walk(inputPath):
        # path example: import/res/drawable
        for item in files:
            # basename example: drawable
            basename = os.path.basename(path);

            if(basename != ""):
                continue;


            if(item.startswith("map") == False):
                continue;

            if(item.endswith(".tmx") == False):
                continue;

            srcFile = os.path.join(path, item);

            #print "DEBUG: path: %s / %s" % (path, basename);
            #print "DEBUG: file: %s" % srcFile;
            result.append(srcFile);

    return result;



def batchCreateLevelData(inputPath, outputPath, mapFilter):
    print "inputPath: %s" % inputPath;
    print "outputPath: %s" % outputPath;

    tmxList = getTMXList(inputPath);

    for tmxFile in tmxList:
        filename = os.path.basename(tmxFile);
        #if filename == "map10000.tmx" :
        #    continue;

        if(mapFilter != ""):
            matchName = mapFilter + ".tmx";
            if matchName not in filename:
                continue;

        status = "";
        try:
            levelCreator = LevelDataCreator(tmxFile);
            levelCreator.setOutputPath(outputPath);
            levelCreator.process();
            levelCreator.save();
            status = "ok";
        except:
            # print "---> Fail to parse the file";
            status = "fail";

        print "process: %s => [%s]" % (filename, status);





# Main
inputPath = "./data/";
outputPath = "../Resources/level/";

mapFilter = "";
if len(sys.argv) >= 2:
  mapFilter = sys.argv[1];

print "mapFilter: [%s]" % mapFilter;

batchCreateLevelData(inputPath, outputPath, mapFilter);
