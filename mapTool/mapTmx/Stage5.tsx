<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Stage5" tilewidth="288" tileheight="200" tilecount="16" columns="0">
 <tile id="0">
  <image width="200" height="200" source="enemy/enemy_500.png"/>
 </tile>
 <tile id="1">
  <image width="100" height="100" source="enemy/enemy_501.png"/>
 </tile>
 <tile id="2">
  <image width="168" height="35" source="enemy/enemy_502.png"/>
 </tile>
 <tile id="3">
  <image width="54" height="35" source="enemy/enemy_503.png"/>
 </tile>
 <tile id="4">
  <image width="81" height="26" source="enemy/enemy_504.png"/>
 </tile>
 <tile id="5">
  <image width="59" height="39" source="enemy/enemy_505.png"/>
 </tile>
 <tile id="6">
  <image width="46" height="39" source="enemy/enemy_506.png"/>
 </tile>
 <tile id="7">
  <image width="47" height="46" source="enemy/enemy_507.png"/>
 </tile>
 <tile id="8">
  <image width="26" height="81" source="enemy/enemy_508.png"/>
 </tile>
 <tile id="9">
  <image width="35" height="54" source="enemy/enemy_509.png"/>
 </tile>
 <tile id="12">
  <image width="288" height="144" source="enemy/enemy_510.png"/>
 </tile>
 <tile id="13">
  <image width="206" height="192" source="enemy/enemy_511.png"/>
 </tile>
 <tile id="14">
  <image width="200" height="200" source="enemy/enemy_512.png"/>
 </tile>
 <tile id="15">
  <image width="200" height="200" source="enemy/enemy_513.png"/>
 </tile>
 <tile id="16">
  <image width="288" height="144" source="enemy/enemy_514.png"/>
 </tile>
 <tile id="17">
  <image width="206" height="192" source="enemy/enemy_515.png"/>
 </tile>
</tileset>
