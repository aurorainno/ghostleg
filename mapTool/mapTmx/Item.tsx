<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Item" tilewidth="60" tileheight="60" tilecount="11" columns="0">
 <tile id="7">
  <image width="20" height="20" source="tile/item_01.png"/>
 </tile>
 <tile id="8">
  <image width="20" height="20" source="tile/item_02.png"/>
 </tile>
 <tile id="9">
  <image width="20" height="20" source="tile/item_03.png"/>
 </tile>
 <tile id="10">
  <image width="20" height="20" source="tile/item_11.png"/>
 </tile>
 <tile id="11">
  <image width="20" height="20" source="tile/item_21.png"/>
 </tile>
 <tile id="12">
  <image width="20" height="20" source="tile/item_22.png"/>
 </tile>
 <tile id="13">
  <image width="20" height="20" source="tile/item_23.png"/>
 </tile>
 <tile id="14">
  <image width="40" height="40" source="tile/item_31.png"/>
 </tile>
 <tile id="15">
  <image width="20" height="20" source="tile/item_32.png"/>
 </tile>
 <tile id="20">
  <image width="60" height="60" source="tile/item_34.png"/>
 </tile>
 <tile id="21">
  <image width="40" height="40" source="tile/item_33.png"/>
 </tile>
</tileset>
