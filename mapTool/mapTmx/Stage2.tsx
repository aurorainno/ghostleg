<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Stage2" tilewidth="344" tileheight="433" tilecount="10" columns="0">
 <tile id="10">
  <image width="137" height="238" source="enemy/enemy_203.png"/>
 </tile>
 <tile id="11">
  <image width="276" height="150" source="enemy/enemy_204.png"/>
 </tile>
 <tile id="12">
  <image width="103" height="118" source="enemy/enemy_205.png"/>
 </tile>
 <tile id="13">
  <image width="140" height="222" source="enemy/enemy_206.png"/>
 </tile>
 <tile id="14">
  <image width="320" height="306" source="enemy/enemy_207.png"/>
 </tile>
 <tile id="15">
  <image width="326" height="433" source="enemy/enemy_208.png"/>
 </tile>
 <tile id="16">
  <image width="174" height="135" source="enemy/enemy_202.png"/>
 </tile>
 <tile id="17">
  <image width="344" height="312" source="enemy/enemy_201.png"/>
 </tile>
 <tile id="18">
  <image width="128" height="204" source="enemy/enemy_209.png"/>
 </tile>
 <tile id="19">
  <image width="270" height="283" source="enemy/enemy_200.png"/>
 </tile>
</tileset>
