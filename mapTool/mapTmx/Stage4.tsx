<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Stage4" tilewidth="268" tileheight="239" tilecount="8" columns="0">
 <tile id="0">
  <image width="108" height="70" source="enemy/enemy_400.png"/>
 </tile>
 <tile id="1">
  <image width="255" height="239" source="enemy/enemy_401.png"/>
 </tile>
 <tile id="2">
  <image width="268" height="202" source="enemy/enemy_402.png"/>
 </tile>
 <tile id="3">
  <image width="167" height="132" source="enemy/enemy_403.png"/>
 </tile>
 <tile id="4">
  <image width="82" height="78" source="enemy/enemy_404.png"/>
 </tile>
 <tile id="5">
  <image width="95" height="78" source="enemy/enemy_405.png"/>
 </tile>
 <tile id="6">
  <image width="170" height="103" source="enemy/enemy_406.png"/>
 </tile>
 <tile id="7">
  <image width="102" height="67" source="enemy/enemy_407.png"/>
 </tile>
</tileset>
