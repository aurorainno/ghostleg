<?xml version="1.0" encoding="UTF-8"?>
<tileset name="NPC" tilewidth="100" tileheight="138" tilecount="9" columns="0">
 <tile id="27">
  <image width="100" height="138" source="npc/drop_point.png"/>
 </tile>
 <tile id="28">
  <image width="40" height="55" source="npc/stop_point.png"/>
 </tile>
 <tile id="29">
  <image width="60" height="72" source="npc/npc_001.png"/>
 </tile>
 <tile id="30">
  <image width="60" height="72" source="npc/npc_002.png"/>
 </tile>
 <tile id="31">
  <image width="80" height="69" source="npc/npc_003.png"/>
 </tile>
 <tile id="32">
  <image width="60" height="83" source="npc/npc_004.png"/>
 </tile>
 <tile id="33">
  <image width="60" height="75" source="npc/npc_005.png"/>
 </tile>
 <tile id="34">
  <image width="60" height="67" source="npc/npc_006.png"/>
 </tile>
 <tile id="35">
  <image width="60" height="53" source="npc/npc_007.png"/>
 </tile>
</tileset>
