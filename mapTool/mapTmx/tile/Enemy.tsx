<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Enemy" tilewidth="100" tileheight="176" tilecount="4" columns="0">
 <tile id="3">
  <image width="80" height="73" source="../enemy/enemy_01.png"/>
 </tile>
 <tile id="7">
  <image width="100" height="95" source="../enemy/enemy_110.png"/>
 </tile>
 <tile id="8">
  <image width="100" height="176" source="../enemy/enemy_111.png"/>
 </tile>
 <tile id="9">
  <image width="100" height="170" source="../enemy/enemy_112.png"/>
 </tile>
</tileset>
