<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Stage3" tilewidth="326" tileheight="424" tilecount="17" columns="0">
 <tile id="0">
  <image width="216" height="275" source="enemy/enemy_300.png"/>
 </tile>
 <tile id="1">
  <image width="326" height="304" source="enemy/enemy_301.png"/>
 </tile>
 <tile id="2">
  <image width="174" height="134" source="enemy/enemy_302.png"/>
 </tile>
 <tile id="3">
  <image width="125" height="233" source="enemy/enemy_303.png"/>
 </tile>
 <tile id="4">
  <image width="115" height="218" source="enemy/enemy_304.png"/>
 </tile>
 <tile id="5">
  <image width="251" height="138" source="enemy/enemy_305.png"/>
 </tile>
 <tile id="6">
  <image width="91" height="111" source="enemy/enemy_306.png"/>
 </tile>
 <tile id="7">
  <image width="320" height="298" source="enemy/enemy_307.png"/>
 </tile>
 <tile id="8">
  <image width="319" height="424" source="enemy/enemy_308.png"/>
 </tile>
 <tile id="9">
  <image width="125" height="199" source="enemy/enemy_309.png"/>
 </tile>
 <tile id="10">
  <image width="48" height="49" source="enemy/enemy_310.png"/>
 </tile>
 <tile id="11">
  <image width="32" height="55" source="enemy/enemy_311.png"/>
 </tile>
 <tile id="12">
  <image width="72" height="32" source="enemy/enemy_312.png"/>
 </tile>
 <tile id="13">
  <image width="59" height="32" source="enemy/enemy_313.png"/>
 </tile>
 <tile id="14">
  <image width="46" height="42" source="enemy/enemy_314.png"/>
 </tile>
 <tile id="15">
  <image width="45" height="55" source="enemy/enemy_315.png"/>
 </tile>
 <tile id="16">
  <image width="46" height="42" source="enemy/enemy_316.png"/>
 </tile>
</tileset>
