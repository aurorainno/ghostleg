<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Lines" tilewidth="20" tileheight="20" tilecount="5" columns="0">
 <tile id="5">
  <image width="20" height="20" source="tile/line1.png"/>
 </tile>
 <tile id="6">
  <image width="20" height="20" source="tile/line2.png"/>
 </tile>
 <tile id="7">
  <image width="20" height="20" source="tile/line3.png"/>
 </tile>
 <tile id="8">
  <image width="20" height="20" source="tile/line4.png"/>
 </tile>
 <tile id="9">
  <image width="20" height="20" source="tile/line5.png"/>
 </tile>
</tileset>
