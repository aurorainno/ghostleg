#
# Convert TMX layer to level data for GhostLeg Game
#

import pytmx
import os
from pytmx import *


from TMXHelper import *
from LevelData import *
from FileHelper import *

# Constant Definition
TYPE_UNKNOWN = 0;
TYPE_ENEMY = 1;
TYPE_ITEM = 2;
TYPE_VLINE = 3;
TYPE_HLINE = 4;

LAYER_NAME_LINE = "Line Layer";
LAYER_NAME_MODEL = "Model Layer";

ENEMY_TYPE_START = 100;
ENEMY_TYPE_END = 199;

# Map Line Setting (Need to sync with C++ code)
# Use in Gx to Px convertion
NUM_VERTICAL_LINE = 4;
NUM_COLUMN = NUM_VERTICAL_LINE - 1;
COLUMN_WIDTH = 90;              # px
COLUMN_GRID_SIZE = 22.5;        # 90 / 5
NUM_GRID_PER_COLUMN = 5;        #
PX_SPACING = COLUMN_WIDTH / NUM_GRID_PER_COLUMN;
TOTAL_GRID_HEIGHT = 50;

# Total Grid = 4 line + 3 * 4 grid = 4 + 12 = 16
GRID_SIZE = 18;
MARGIN_X = 16;


# Class Definition

class LevelDataCreator:
    def __init__(self, _tmxFile):
        self.lineArray = [];
        self.enemyArray = [];
        self.itemArray = [];
        self.tmxFile = _tmxFile;
        self.outputPath = "";
        self.minGid = 1;
        self.maxGid = 1;
        self.lineSet = {}
        self.lineID = 1;

        self.mapName = self.parseMapName(self.tmxFile);

        self.tileMap = pytmx.TiledMap(self.tmxFile);
        # print "DEBUG: %s" % self.tileMap.tiles();

        # Old Logic
        #self.totalHeight = 600;
        #self.gridSize = 25; # TOLD
        self.gridSize = GRID_SIZE;
        self.totalHeight = TOTAL_GRID_HEIGHT * self.gridSize; # 432

        self.lineDataLayer = find_tmx_layer_by_name(self.tileMap, LAYER_NAME_LINE);
        self.modelDataLayer = find_tmx_layer_by_name(self.tileMap, LAYER_NAME_MODEL);

        self.setupTileProperties();

    def setOutputPath(self, path):
        self.outputPath = path;

    def parseMapName(self, tmxFile):
        # input: ../tmx/level0.tmx
        # output: level0

        tokens = tmxFile.split("/");
        filename = tokens[-1];      # last token

        nameTokens = filename.split(".");

        return nameTokens[0];

    def intValue(self, s, defaultValue):
        try:
            return int(s)
        except ValueError:
            return defaultValue;

    def setupTileProperties(self):
        self.tileProperties = {};

        maxGid = 0;
        for gid, props in self.tileMap.tile_properties.items():
            self.tileProperties[gid] = props;
            if(gid > maxGid):
                maxGid= gid;

        self.maxGid = maxGid;

            #tileSet = self.tileMap.get_tileset_from_gid(gid)
            #print "DEBUG: gid=%d props=[%s]" % (gid, props);
            #print "tileSet=[%s]" % tileSet;

    def getTypeFromGID(self, gid):
        source = self.getTileProperty(gid, "source", "");
        basename = os.path.basename(source);

        if basename.startswith("line"):
            return TYPE_HLINE;
        elif basename.startswith("vertLine"):
            return TYPE_VLINE;
        elif basename.startswith("item"):
            return TYPE_ITEM;
        else:
            return TYPE_ENEMY;

    def getTileProperty(self, gid, key, defaultValue):
        props = self.tileProperties[gid];

        if(props is None):
            return defaultValue;

        if(props.has_key(key) is False):
            return defaultValue;

        return props[key];


    def getTileIntProperty(self, gid, key, defaultValue):
        result = self.getTileProperty(gid, key, "");

        return self.intValue(result, defaultValue);

    def process(self):
        self.processTileLayer(self.lineDataLayer);
        self.processTileLayer(self.modelDataLayer);

        # self.parseLineData();
        # self.parseModelData();

    def createLineData(self, lineId, gx, gy, gid, probability):
        lineType = self.getTileIntProperty(gid, "type", 0);

        xPos = gx / 5;
        py = self.getPy(gy);

        lineData = LineData(lineId, lineType, xPos, py, probability);

        return lineData;




    def parseLineData(self):

        if(self.lineDataLayer is None):
            print "Line Data Layer is missing";
            return;

        lineID = 1;

        lineSet = {};

        for gx, gy, gid in self.lineDataLayer:
            if(gid == 0):
                continue;
            # print "gx=%d gy=%d gid=%d" % (gx, gy, gid);

            probability = self.getTileIntProperty(gid, "probability", 100);

            lineData = self.createLineData(lineID, gx, gy, gid, probability);
            key = lineData.getKey();
            isExist = lineSet.has_key(key);

            if(isExist):
                continue;

            self.lineArray.append(lineData);
            lineSet[key] = 1;

            lineID = lineID + 1;

        return;

    def isEnemy(self, gid):
        source = self.getTileProperty(gid, "source", "");
        print "gid=%d source: %s" % (gid, source);
        return False;
        #typeValue = self.getTileIntProperty(gid, "type", 0);
        # print "DEBUG: gid=%d type=%s" % (gid, typeValue);
        #return typeValue >= ENEMY_TYPE_START and typeValue < ENEMY_TYPE_END;    # NOTE!! hard coded here

    def getPy(self, gy):
        return self.totalHeight - gy * self.gridSize;

    def getPx(self, gx):
        # col = gx / NUM_GRID_PER_COLUMN;
        # offset = gx % NUM_GRID_PER_COLUMN;
        #
        # px = col * COLUMN_WIDTH;
        # if(offset > 0):
        #     px += int((offset - 0.5) * COLUMN_GRID_SIZE);
        px = gx * self.gridSize + MARGIN_X + self.gridSize/2;

        return px;


    def createEnemyData(self, gx, gy, gid):
        source = self.getTileProperty(gid, "source", "");
        enemyID = self.parseEnemyID(source);

        speed = 0;  # self.getTileIntProperty(gid, "speed", 0);
        resID = 0;  # self.getTileIntProperty(gid, "resID", 0);

        xPos = gx / 5;
        px = self.getPx(gx);
        py = self.getPy(gy);
        #print "DEBUG %d -> %d (%s)" % (gid, enemyID, source);
        data = EnemyData(enemyID, px, py, speed, resID);

        return data;

    def createItemData(self, gx, gy, gid):
        source = self.getTileProperty(gid, "source", "");
        itemID = self.parseItemID(source);

        # modelType = self.getTileIntProperty(gid, "type", 0);      # no use
        resID = 0;  # self.getTileIntProperty(gid, "resID", 0);   # no use

        px = self.getPx(gx);
        py = self.getPy(gy);
        data = ItemData(itemID, px, py, resID);

        return data;

    def processEnemyTile(self, gx, gy, gid):
        enemyData = self.createEnemyData(gx, gy, gid);
        self.enemyArray.append(enemyData);

    def processItemTile(self, gx, gy, gid):
        itemData = self.createItemData(gx, gy, gid);
        self.itemArray.append(itemData);

    def processLineTile(self, gx, gy, gid):
        probability = self.getTileIntProperty(gid, "probability", 100);

        lineData = self.createLineData(self.lineID, gx, gy, gid, probability);
        key = lineData.getKey();
        isExist = self.lineSet.has_key(key);

        if(isExist):
            return;

        self.lineArray.append(lineData);
        self.lineSet[key] = 1;

        self.lineID = self.lineID + 1;

    def processTileLayer(self, layerObject):
        if(self.modelDataLayer is None):
            print "processTileLayer: Data Layer is missing";
            return;

        for gx, gy, gid in layerObject:
            if(gid == 0):
                continue;

            gidType = self.getTypeFromGID(gid);
            if(TYPE_ITEM == gidType):
                self.processItemTile(gx, gy, gid);
            elif(TYPE_ENEMY == gidType):
                self.processEnemyTile(gx, gy, gid);
            elif(TYPE_HLINE == gidType):
                self.processLineTile(gx, gy, gid);


            #End of for

    # 02.png -> 2
    def parseEnemyID(self, source):
        basename = os.path.basename(source);
        valueStr = basename.replace(".png", "");
        return int(valueStr);


    # Reference Check : Constant.cpp / ModelType::itemXXXX
    # item_1.png -> 201
    # item_2.png -> 202     //
    # item_3.png -> 203     //
    # item_11.png -> 211   // Candy Stick
    def parseItemID(self, source):
        basename = os.path.basename(source);
        valueStr = basename.replace(".png", "");
        valueStr = valueStr.replace("item_", "");
        return int(valueStr) + 200 ; # item is start from 200, reference: Constant.cpp

    def parseModelData(self):
        if(self.modelDataLayer is None):
            print "Model Data Layer is missing";
            return;

        for gx, gy, gid in self.modelDataLayer:
            if(gid == 0):
                continue;
            isEnemy = self.isEnemy(gid);
            print "gx=%d gy=%d gid=%d isEnemy=%d" % (gx, gy, gid, isEnemy);

            if(isEnemy == 1):
                enemyData = self.createEnemyData(gx, gy, gid);
                self.enemyArray.append(enemyData);
            else:
                itemData = self.createItemData(gx, gy, gid);
                self.itemArray.append(itemData);


        return;


    def infoLineData(self):
        result = "Line count=%d\n" % len(self.lineArray);

        for lineData in self.lineArray:
            result += lineData.toString();
            result += "\n";

        return result;


    def infoEnemyData(self):
        result = "Enemy count=%d\n" % len(self.enemyArray);
        for data in self.enemyArray:
            result += data.toString();
            result += "\n";

        return result;

    def infoItemData(self):
        result = "Item count=%d\n" % len(self.itemArray);

        for data in self.itemArray:
            result += data.toString();
            result += "\n";

        return result;

    def info(self):
        result = "";

        result += "tmxFile: %s\n" % self.tmxFile;

        result += self.infoLineData();
        result += "\n";
        result += self.infoEnemyData();
        result += "\n";
        result += self.infoItemData();
        result += "\n";


        return result;

    def getDataContent(self):
        content = "";

        # Line Data
        content += "[Line]\n";
        for lineData in self.lineArray:
            content += lineData.toString();
            content += "\n";
        content += "\n";

        # Enemy Data
        content += "[Enemy]\n";
        for enemyData in self.enemyArray:
            content += enemyData.toString();
            content += "\n";
        content += "\n";

        # Item Data
        content += "[Item]\n";
        for itemData in self.itemArray:
            content += itemData.toString();
            content += "\n";
        content += "\n";

        return content;

    def save(self):
        if(self.outputPath == ""):
            print "ERROR: outputPath not defined";
            return;

        outputFile = "%s/%s.dat" % (self.outputPath, self.mapName);

        write_file(outputFile, self.getDataContent());
