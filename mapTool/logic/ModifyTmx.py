
import pytmx

import os;
import os.path;
from FileHelper import *;
import pytmx;
from pytmx import *;

# Class Definition

class TMXModifier:
    def __init__(self, _tmxFile):
        self.tmxFile = _tmxFile;
        self.tileMap = pytmx.TiledMap(self.tmxFile);
        self.outputPath = "";
        self.content = read_file(self.tmxFile);
        self.basename = os.path.basename(self.tmxFile)

    def info(self):
        result = "";

        result += "file: " + self.tmxFile + "\n";
        result += "outputPath: " + self.outputPath + "\n";
        result += "basename: " + self.basename + "\n";

        return result;

    def convertMapHeight(self, oldHeight, newHeight):
        fromStr = "height=\"%d\"" % oldHeight;
        toStr = "height=\"%d\"" % newHeight;

        self.content = self.content.replace(fromStr, toStr);

    def saveContent(self):
        print "SaveTmx";
        outFile = self.outputPath + "/" + self.basename;


        write_file(outFile, self.content);


def convertTmxFolderForHeight(fromPath, toPath, oldHeight, newHeight):
        # copy the file srcResPath to targetResPath
    for path, dirs, files in os.walk(fromPath):
        # path example: import/res/drawable
        #print "\npath=%s dirs=%s" % (path, dirs);

        # for item in dirs:
        #     # basename example: drawable
        #     #basename = os.path.basename(path);
        #     print "dir: %s" % item;

        # skip the subpath
        relativePath = path.replace(fromPath, "");
        if(relativePath != ""):
            continue;

        for item in files:
            # basename example: drawable
            #basename = os.path.basename(path);
            if(item.endswith(".tmx") == False):
                continue;

            srcFile = path + "/" + item;
            dstFile = toPath + "/" + item;
            print "convert: [%s] -> [%s]" % (srcFile, dstFile);

            # Start conversion
            tmxModifier = TMXModifier(srcFile);
            tmxModifier.outputPath = toPath;
            tmxModifier.convertMapHeight(oldHeight, newHeight);
            tmxModifier.saveContent();
