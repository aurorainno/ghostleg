#
# Convert TMX Data to Astrodog Map Data
#
# Main Entry Point 'process'

import pytmx
import os
from pytmx import *


from TMXHelper import *
from LevelData import *
from FileHelper import *
from ImageHelper import *
from AnchorPointHelper import *

# Constant Definition
TYPE_UNKNOWN = 0;
TYPE_ENEMY = 1;
TYPE_ITEM = 2;
TYPE_VLINE = 3;
TYPE_HLINE = 4;

LAYER_NAME_LINE = "Line Layer";
LAYER_NAME_ITEM = "Item Layer";
LAYER_NAME_MODEL = "Enemy Layer";

ENEMY_TYPE_START = 100;
ENEMY_TYPE_END = 199;

# Map Line Setting (Need to sync with C++ code)
# Use in Gx to Px convertion
MAP_HEIGHT = 50;            # 50 Grid
MAP_WIDTH = 15;             # 15 Grid
NPC_MAP_HEIGHT = 30;
GRID_SIZE = 20;
MARGIN_X = 10;
NUM_GRID_BETWEEN = 3;

NUM_VERTICAL_LINE = 4;
NUM_COLUMN = NUM_VERTICAL_LINE - 1;
COLUMN_WIDTH = 90;              # px
COLUMN_GRID_SIZE = 22.5;        # 90 / 5
NUM_GRID_PER_COLUMN = 5;        #
PX_SPACING = COLUMN_WIDTH / NUM_GRID_PER_COLUMN;
TOTAL_GRID_HEIGHT = 50;

# Total Grid = 4 line + 3 * 4 grid = 4 + 12 = 16


# Class Definition

class MapDataConvertor:
    def __init__(self, _tmxFile):
        self.lineArray = [];
        self.enemyArray = [];
        self.itemArray = [];
        self.npcArray = [];
        self.linePosArray = [];         ## Information about the X position of the Verti Line
        self.tmxFile = _tmxFile;
        self.outputPath = "";
        self.minGid = 1;
        self.maxGid = 1;
        self.lineSet = {}
        self.lineID = 1;

        self.dropPosition = (-1, -1);
        self.stopPostion = (-1, -1);


        self.mapName = self.parseMapName(self.tmxFile);
        self.basePath = os.path.dirname(os.path.realpath(self.tmxFile));
        self.isNpcMap = self.checkIsNpcMap(self.mapName);


        self.tileMap = pytmx.TiledMap(self.tmxFile);
        # print "DEBUG: %s" % self.tileMap.tiles();



        # Old Logic
        #self.totalHeight = 600;
        #self.gridSize = 25; # TOLD
        self.gridSize = GRID_SIZE;

        self.totalHeight = (NPC_MAP_HEIGHT if self.isNpcMap else MAP_HEIGHT) * self.gridSize; # 432

        self.lineDataLayer = find_tmx_layer_by_name(self.tileMap, LAYER_NAME_LINE);
        self.modelDataLayer = find_tmx_layer_by_name(self.tileMap, LAYER_NAME_MODEL);
        self.itemDataLayer = find_tmx_layer_by_name(self.tileMap, LAYER_NAME_ITEM);

        self.setupTileProperties();

        self.defineVertiLinePosition();

    def setOutputPath(self, path):
        self.outputPath = path;

    def shouldSnapToLine(self, monsterID):
        if(monsterID == 25 or monsterID == 26):
            return True;
        else:
            return False;

    def defineVertiLinePosition(self):
        self.linePosArray = [];

        # Logic Reference: Astrodog: GameMap::setupVertLines
        x = MARGIN_X + GRID_SIZE + GRID_SIZE/2;
        spacing = GRID_SIZE * (NUM_GRID_BETWEEN + 1);
        for i in range(0, NUM_VERTICAL_LINE):
            #print "i=%d x=%d" % (i, x);
            self.linePosArray.append(x);

            x += spacing;

    def getNearestLineX(self, testX):    # return -999 if found no match line

        bestX = -1;
        bestDiff = -1;

        for x in self.linePosArray:
            diff = abs(testX - x);
            # print "x=%d diff=%d" % (x, diff);

            if(bestX != -1 and diff >= bestDiff):
                continue;

            bestX = x;
            bestDiff = diff;

        return bestX;

    def checkIsNpcMap(self, mapName):
        return mapName.startswith("npc");

    def parseMapName(self, tmxFile):
        # input: ../tmx/level0.tmx
        # output: level0

        tokens = tmxFile.split("/");
        filename = tokens[-1];      # last token

        nameTokens = filename.split(".");

        return nameTokens[0];

    def intValue(self, s, defaultValue):
        try:
            return int(s)
        except ValueError:
            return defaultValue;

    def getTileIntProperty(self, gid, key, defaultValue):
        result = self.getTileProperty(gid, key, "");


    def setupTileProperties(self):
        self.tileProperties = {};

        maxGid = 0;
        for gid, props in self.tileMap.tile_properties.items():
            self.tileProperties[gid] = props;
            if(gid > maxGid):
                maxGid= gid;

        self.maxGid = maxGid;

        tileSet = self.tileMap.get_tileset_from_gid(gid)
        #print "DEBUG TILE: gid=%d props=[%s]" % (gid, props);
        #print "tileSet=[%s]" % tileSet;

    def getTypeFromGID(self, gid):
        source = self.getTileProperty(gid, "source", "");
        basename = os.path.basename(source);

        if basename.startswith("line"):
            return TYPE_HLINE;
        elif basename.startswith("vertLine"):
            return TYPE_VLINE;
        elif basename.startswith("item"):
            return TYPE_ITEM;
        else:
            return TYPE_ENEMY;

    def getTileProperty(self, gid, key, defaultValue):
        #print "DEBUG: gid=%d" % gid;
        props = self.tileProperties[gid];

        if(props is None):
            return defaultValue;

        if(props.has_key(key) is False):
            return defaultValue;

        return props[key];


    def getTileIntProperty(self, gid, key, defaultValue):
        result = self.getTileProperty(gid, key, "");

        return self.intValue(result, defaultValue);

    def process(self):
        self.parseLineData();
        self.processItemLayer();
        self.processEnemyLayer();

        # self.parseLineData();
        # self.parseModelData();


    ## Line Data Processing
    def parseLineType(self, source):
        basename = os.path.basename(source);
        valueStr = basename.replace(".png", "");
        valueStr = valueStr.replace("line", "");

        return int(valueStr);

    def parseLineData(self):

        if(self.lineDataLayer is None):
            print "Line Data Layer is missing";
            return;

        lineID = 1;

        lineSet = {};

        for gx, gy, gid in self.lineDataLayer:
            if(gid == 0):
                continue;

            #print "DEBUG: gx=%d gy=%d gid=%d" % (gx, gy, gid);

            probability = self.getTileIntProperty(gid, "probability", 100);

            lineData = self.createLineData(lineID, gx, gy, gid, probability);
            key = lineData.getKey();
            isExist = lineSet.has_key(key);

            if(isExist):
                continue;

            self.lineArray.append(lineData);
            lineSet[key] = 1;

            lineID = lineID + 1;

        return;

    def processLineTile(self, gx, gy, gid):
        probability = self.getTileIntProperty(gid, "probability", 100);

        lineData = self.createLineData(self.lineID, gx, gy, gid, probability);
        key = lineData.getKey();
        isExist = self.lineSet.has_key(key);

        if(isExist):
            return;

        self.lineArray.append(lineData);
        self.lineSet[key] = 1;

        self.lineID = self.lineID + 1;


    def createLineData(self, lineId, gx, gy, gid, probability):
        source = self.getTileProperty(gid, "source", "");

        lineType = self.parseLineType(source);

        xPos = (gx - 1) / 4;
        py = self.getPy(gy);

        lineData = LineData(lineId, lineType, xPos, py, probability);

        return lineData;



    ## Item Data Processing
    def processItemLayer(self):
        layerObject = self.itemDataLayer;
        print "DEBUG: processItemLayer";
        if(layerObject is None):
            print "processItemLayer: Data Layer is missing";
            return;

        for gx, gy, gid in layerObject:
            if(gid == 0):
                continue;
            self.processItemTile(gx, gy, gid);

    def processItemTile(self, gx, gy, gid):
        print "DEBUG: processItemTile";
        itemData = self.createItemData(gx, gy, gid);
        self.itemArray.append(itemData);



    def createItemDataXXXX(self, gx, gy, gid):
        print "DEBUG: createItemData";
        source = self.getTileProperty(gid, "source", "");
        itemID = self.parseItemID(source);
        resID = 0;      # ???

        px = self.getPx(gx);
        py = self.getPy(gy);

        print "debug: src=%s itemID=%d px=%d py=%d" % (source, itemID, px, py);

        data = ItemData(itemID, px, py, resID);

        return data;


    # Processing Enemy Layer
    def parseEnemyID(self, source):
        basename = os.path.basename(source);
        valueStr = basename.replace(".png", "");
        valueStr = valueStr.replace("enemy_", "");

        valueStr = valueStr.lstrip("0");

        return int(valueStr);

    def parseNpcID(self, source):
        basename = os.path.basename(source);
        valueStr = basename.replace(".png", "");
        valueStr = valueStr.replace("npc_", "");

        valueStr = valueStr.lstrip("0");

        return int(valueStr);

    def getAnchorForObject(self, imageName, monsterID):

        # if monsterID >= 100:
        #     return [0.0, 0.0];

        basename = os.path.basename(imageName);
        basename = basename.replace(".png", "");

        svgFile = self.basePath + "/../anchor/" + basename + ".svg";
        # print "DEBUG: %s" % svgFile;

        return find_anchor_point_from_svg(svgFile); # [0.5, 0.5];

    def getAnchoredPosition(self, leftBottomPos, anchorPoint, imageSize, scale, flipX, flipY):
        # if monsterID >= 100:        # id more than 100, they are obstacle, its anchor always stick with 0,0
        #     return leftBottomPos;
        print "getAnchoredPosition: DEBUG: pos=%s anchor=%s imageSize=%s scale=%s flipX=%d flipY=%d" % (leftBottomPos,
                            anchorPoint, imageSize, scale, flipX, flipY);

        offsetX = imageSize[0] * scale[0] * anchorPoint[0];
        offsetY = imageSize[1] * scale[1] *anchorPoint[1];
        scaledWidth = imageSize[0] * scale[0];
        scaleHeight = imageSize[1] * scale[1];

        if(flipX):
            offsetX = scaledWidth - offsetX;

        if(flipY):
            offsetY = scaleHeight - offsetY;

        finalX = leftBottomPos[0] + offsetX;
        finalY = leftBottomPos[1] - offsetY;

        return [finalX, finalY];

    def convertEditorPosToGamePos(self, editorX, editorY):
        finalX = editorX + MARGIN_X;

        totalH = self.totalHeight;
        finalY = totalH - editorY;

        return [finalX, finalY];

    def getObjectDetail(self, tileObject, anchorX, anchorY):
        source = tileObject.source;
        sourcePath = self.basePath + "/" + source;

        (objectW, objectH) = get_image_size(sourcePath);

        tunedX = tileObject.x;
        tunedY = tileObject.y + tileObject.height;  # this is special hack
        #tunedY -= tileObject.finalHeight / 2;        # the monster are anchor at center now

        scaleX = tileObject.finalWidth / objectW;
        scaleY = tileObject.finalHeight / objectH;

        flipX = tileObject.flipped_horizontally;
        flipY = tileObject.flipped_vertically;

        # adjustX, Y = position after anchor
        (adjustX, adjustY) = self.getAnchoredPosition([tunedX, tunedY],
                                [anchorX, anchorY], [objectW, objectH],
                                [scaleX, scaleY], flipX, flipY);
        (px, py) = self.convertEditorPosToGamePos(adjustX, adjustY);

        return (px, py, flipX, flipY, scaleX, scaleY);



    def processEnemyObject(self, tileObject):
        source = tileObject.source;

        enemyID = self.parseEnemyID(source);
        (anchorX, anchorY) = self.getAnchorForObject(source, enemyID);

        (px, py, flipX, flipY, scaleX, scaleY) = self.getObjectDetail(tileObject, anchorX, anchorY);

        # Walk on Route enemy need snap to line
        needSnap = self.shouldSnapToLine(enemyID);
        if(needSnap):
            px = self.getNearestLineX(px);
            # print "DEBUG: (SNAP) AFTER: enemyID=%d needSnap=%d x=%d" % (enemyID, needSnap, px);

        enemyData = EnemyData(enemyID, px, py, flipX, flipY);
        enemyData.scaleX = scaleX;
        enemyData.scaleY = scaleY;
        enemyData.refID = tileObject.id;

        self.enemyArray.append(enemyData);

    def processNpcObject(self, tileObject):
        source = os.path.basename(tileObject.source);

        (anchorX, anchorY) = (0.5, 0);

        (px, py, flipX, flipY, scaleX, scaleY) = self.getObjectDetail(tileObject, anchorX, anchorY);

        faceDir = flipX;    # same value:

        # def __init__(self, _npcID, _x, _y, _faceDir):

        if source.startswith("npc_"):
            # NPC
            npcID = self.parseNpcID(source);

            npcData = NPCData(npcID, px, py, faceDir);
            npcData.refID = tileObject.id;
            self.npcArray.append(npcData);

        elif source.startswith("drop_point"):
            self.dropPosition = (px, py);

        elif source.startswith("stop_point"):
            self.stopPostion = (px, py);






    def processEnemyLayer(self):
        layerObject = self.modelDataLayer;

        if(layerObject is None):
            print "processEnemyLayer: Data Layer is missing";
            return;

        for tileObject in self.tileMap.objects:
            # print "tileObject: [%s]" % tileObject;

            gid = tileObject.gid;
            if not tileObject.properties:
                continue;

            # tileObject.
            #print "tileObject: gid=%d properties: [%s]" % (gid, tileObject.properties);
            #print "flip: h=%d v=%d" % (tileObject.flipped_horizontally, tileObject.flipped_vertically);

            source = os.path.basename(tileObject.source);

            if source.startswith("enemy_"):
                print "ProcessEnemy: source=%s" % source;
                self.processEnemyObject(tileObject);
            else:
                print "ProcessNpc: source=%s" % source;
                self.processNpcObject(tileObject);

            #
            # objectW = tileObject.width;
            # objectH = tileObject.height;
            #
            # enemyID = self.parseEnemyID(source);
            #
            # (anchorX, anchorY) = self.getAnchorForObject(source, enemyID);
            #
            # sourcePath = self.basePath + "/" + source;
            # print "DEBUG: %s -> [%s]" % (self.basePath, sourcePath);
            # (objectW, objectH) = get_image_size(sourcePath);
            # #objectW = self.getTileIntProperty(gid, "width", 50);
            # #objectH = self.getTileIntProperty(gid, "height", 50);
            #
            #
            # tunedX = tileObject.x;
            # tunedY = tileObject.y + tileObject.height;  # this is special hack
            # #tunedY -= tileObject.finalHeight / 2;        # the monster are anchor at center now
            #
            # scaleX = tileObject.finalWidth / objectW;
            # scaleY = tileObject.finalHeight / objectH;
            #
            # px = MARGIN_X + tileObject.x + tileObject.finalWidth / 2;
            # py = self.totalHeight - tunedY;
            # flipX = tileObject.flipped_horizontally;
            # flipY = tileObject.flipped_vertically;
            #
            # print "DEBUG: source: %s" % source;
            # print "DEBUG: OrignXY: %d, %d (%d)" % (tileObject.x, tunedY, tileObject.y);
            # print "DEBUG: Anchor: %f, %f" % (anchorX, anchorY);
            # print "DEBUG: Flip: x=%d, y=%d" % (flipX, flipY);
            #
            #
            # # adjustX, Y = position after anchor
            # (adjustX, adjustY) = self.getAnchoredPosition([tunedX, tunedY],
            #                         [anchorX, anchorY], [objectW, objectH],
            #                         [scaleX, scaleY], flipX, flipY);
            # (px, py) = self.convertEditorPosToGamePos(adjustX, adjustY);
            #
            #
            #
            # # Extract from source data
            # needSnap = self.shouldSnapToLine(enemyID);
            # print "DEBUG: refID=%d" % (tileObject.id);
            # print "DEBUG: src=%s id=%s size=(%d, %d) pos=(%d, %d) h=%d x=%d y=%d h=%d" % (source,
            #                 enemyID, objectW, objectH, px, py,
            #                 self.totalHeight, tileObject.x, tunedY,
            #                 tileObject.height);
            # #print "DEBUG: (SNAP) enemyID=%d needSnap=%d x=%d" % (enemyID, needSnap, px);
            # print "DEBUG: size=%d,%d" % (tileObject.finalWidth, tileObject.finalHeight);
            # #print "DEBUG: point=%s" % (tileObject.properties);
            #
            # print "DEBUG: Position: id=%d leftBot=(%d,%d) afterAnchor=(%d,%d) afterConv=(%d,%d) size=(%d, %d) anchor=(%f,%f) scale=(%f,%f)" % (enemyID,
            #             tunedX, tunedY, adjustX, adjustY, px, py, objectW, objectH, anchorX, anchorY,
            #             scaleX, scaleY);
            #
            #
            #
            #
            # if(needSnap):
            #     px = self.getNearestLineX(px);
            #     # print "DEBUG: (SNAP) AFTER: enemyID=%d needSnap=%d x=%d" % (enemyID, needSnap, px);
            #
            # enemyData = EnemyData(enemyID, px, py, flipX, flipY);
            # enemyData.scaleX = scaleX;
            # enemyData.scaleY = scaleY;
            # enemyData.refID = tileObject.id;
            #
            # self.enemyArray.append(enemyData);




    def isEnemy(self, gid):
        source = self.getTileProperty(gid, "source", "");
        print "gid=%d source: %s" % (gid, source);
        return False;
        #typeValue = self.getTileIntProperty(gid, "type", 0);
        # print "DEBUG: gid=%d type=%s" % (gid, typeValue);
        #return typeValue >= ENEMY_TYPE_START and typeValue < ENEMY_TYPE_END;    # NOTE!! hard coded here

    def getPy(self, gy):
        return self.totalHeight - (gy+1) * self.gridSize;

    def getPx(self, gx):
        # col = gx / NUM_GRID_PER_COLUMN;
        # offset = gx % NUM_GRID_PER_COLUMN;
        #
        # px = col * COLUMN_WIDTH;
        # if(offset > 0):
        #     px += int((offset - 0.5) * COLUMN_GRID_SIZE);
        px = MARGIN_X + gx * self.gridSize + self.gridSize/2;

        return px;



    def createEnemyData(self, gx, gy, gid):
        source = self.getTileProperty(gid, "source", "");
        enemyID = self.parseEnemyID(source);

        speed = 0;  # self.getTileIntProperty(gid, "speed", 0);
        resID = 0;  # self.getTileIntProperty(gid, "resID", 0);

        xPos = gx / 5;
        px = self.getPx(gx);
        py = self.getPy(gy);
        #print "DEBUG %d -> %d (%s)" % (gid, enemyID, source);
        data = EnemyData(enemyID, px, py, speed, resID);

        return data;

    def getTilePy(self, gy, tileSize):
        return self.totalHeight - (gy+1) * self.gridSize + tileSize/2;

    def getTilePx(self, gx, tileSize):
        # col = gx / NUM_GRID_PER_COLUMN;
        # offset = gx % NUM_GRID_PER_COLUMN;
        #
        # px = col * COLUMN_WIDTH;
        # if(offset > 0):
        #     px += int((offset - 0.5) * COLUMN_GRID_SIZE);
        px = MARGIN_X + gx * self.gridSize + tileSize/2;

        return px;

    def createItemData(self, gx, gy, gid):
        source = self.getTileProperty(gid, "source", "");
        itemID = self.parseItemID(source);

        # modelType = self.getTileIntProperty(gid, "type", 0);      # no use
        resID = 0;  # self.getTileIntProperty(gid, "resID", 0);   # no use

        tileImagePath = self.basePath + "/" + source;
        # print "DEBUG: createItemData: path=%s" % tileImagePath;
        (tileW, tileH) = get_image_size(tileImagePath);



        px = self.getTilePx(gx, tileW);
        py = self.getTilePy(gy, tileH);

        #print "DEBUG: createItemData: source=%s itemID=%d tileW=%d tileH=%d g=(%d,%d) p=(%d,%d)" % (source, itemID, tileW, tileH, gx, gy, px, py);

        data = ItemData(itemID, px, py, resID);

        return data;

    def processEnemyTile(self, gx, gy, gid):
        enemyData = self.createEnemyData(gx, gy, gid);
        self.enemyArray.append(enemyData);




    def processTileLayer(self, layerObject):
        if(self.modelDataLayer is None):
            print "processTileLayer: Data Layer is missing";
            return;

        for gx, gy, gid in layerObject:
            if(gid == 0):
                continue;

            gidType = self.getTypeFromGID(gid);
            if(TYPE_ITEM == gidType):
                self.processItemTile(gx, gy, gid);
            elif(TYPE_ENEMY == gidType):
                self.processEnemyTile(gx, gy, gid);
            elif(TYPE_HLINE == gidType):
                self.processLineTile(gx, gy, gid);


            #End of for

    # 02.png -> 2
    # def parseEnemyID(self, source):
    #     basename = os.path.basename(source);
    #     valueStr = basename.replace(".png", "");
    #     return int(valueStr);


    # Reference Check : Constant.cpp / ModelType::itemXXXX
    # item_1.png -> 201
    # item_2.png -> 202     //
    # item_3.png -> 203     //
    # item_11.png -> 211   // Candy Stick
    def parseItemID(self, source):
        basename = os.path.basename(source);
        valueStr = basename.replace(".png", "");
        valueStr = valueStr.replace("item_", "");
        return int(valueStr) + 200 ; # item is start from 200, reference: Constant.cpp

    def parseModelData(self):
        if(self.modelDataLayer is None):
            print "Model Data Layer is missing";
            return;

        for gx, gy, gid in self.modelDataLayer:
            if(gid == 0):
                continue;
            isEnemy = self.isEnemy(gid);
            print "gx=%d gy=%d gid=%d isEnemy=%d" % (gx, gy, gid, isEnemy);

            if(isEnemy == 1):
                enemyData = self.createEnemyData(gx, gy, gid);
                self.enemyArray.append(enemyData);
            else:
                itemData = self.createItemData(gx, gy, gid);
                self.itemArray.append(itemData);


        return;


    def infoNpcData(self):
        result = "isNPCMap=%d npcCount=%d\n" % (self.isNpcMap, len(self.npcArray));
        result += "dropPosition=(%d, %d)\n" % self.dropPosition;
        result += "stopPostion=(%d, %d)\n" % self.stopPostion;

        for npcData in self.npcArray:
            result += npcData.toString();
            result += "\n";

        return result;


    def infoLineData(self):
        result = "Line count=%d\n" % len(self.lineArray);

        for lineData in self.lineArray:
            result += lineData.toString();
            result += "\n";

        return result;


    def infoEnemyData(self):
        result = "Enemy count=%d\n" % len(self.enemyArray);
        for data in self.enemyArray:
            result += data.toString();
            result += "\n";

        return result;

    def infoItemData(self):
        result = "Item count=%d\n" % len(self.itemArray);

        for data in self.itemArray:
            result += data.toString();
            result += "\n";

        return result;

    def info(self):
        result = "";

        result += "tmxFile: %s\n" % self.tmxFile;

        result += self.infoLineData();
        result += "\n";
        result += self.infoEnemyData();
        result += "\n";
        result += self.infoItemData();
        result += "\n";


        return result;

    def getDataContent(self):
        content = "";

        # Line Data
        content += "[Line]\n";
        for lineData in reversed(self.lineArray):
            content += lineData.toString();
            content += "\n";
        content += "\n";

        # Enemy Data
        content += "[Enemy]\n";
        for enemyData in self.enemyArray:
            content += enemyData.toString();
            content += "\n";
        content += "\n";

        # Item Data
        content += "[Item]\n";
        for itemData in reversed(self.itemArray):
            content += itemData.toString();
            content += "\n";
        content += "\n";

        if(self.isNpcMap == False):
            return content;

        # NPC Data
        content += "[Npc]\n";
        for npcData in self.npcArray:
            content += npcData.toString();
            content += "\n";
        content += "\n";

        # self.dropPosition = (-1, -1);
        # self.stopPostion = (-1, -1);

        # NPC Setting
        content += "[NpcSetting]\n";
        content += "isNpcMap=%d\n" % self.isNpcMap;
        content += "dropPosition=%d,%d\n" % self.dropPosition;
        content += "stopPostion=%d,%d\n" % self.stopPostion;
        content += "\n";

        return content;

    def save(self):
        if(self.outputPath == ""):
            print "ERROR: outputPath not defined";
            return;

        outputFile = "%s/%s.dat" % (self.outputPath, self.mapName);

        write_file(outputFile, self.getDataContent());
