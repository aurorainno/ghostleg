import sys
import os.path


# the mock-0.3.1 dir contains testcase.py, testutils.py & mock.py
sys.path.append('./util/')

import xml.etree.ElementTree as ET

# Read the content of the given file
def find_anchor_point_from_svg(filename):

    if os.path.exists(filename) == False:
        #print "ERROR: filename not found %s" % filename;
        return [0.5, 0.5];

    tree = ET.parse(filename)
    # doc = minidom.parse(filename);  # parseString also exists
    root = tree.getroot();


    imageWidth = 0;
    imageHeight = 0;
    imageX = 0;
    imageY = 0;
    anchorXPos = 0;
    anchorYPos = 0;

    for node in tree.iter():
        #print "DEBUG: [" , node.tag, "]: ", node.attrib
        #print "DEBUG: [%s]" % (node.attrib.get("cx"));


        if node.tag.endswith("image"):
            print "DEBUG: found image";
            imageWidth = int(node.attrib.get("width"));
            imageHeight = int(node.attrib.get("height"));
            imageX = int(node.attrib.get("x"));
            imageY = int(node.attrib.get("y"));
        elif node.tag.endswith("circle"):
            print "DEBUG: found anchorPoint";
            anchorXPos = int(node.attrib.get("cx"));
            anchorYPos = int(node.attrib.get("cy"));

    print "Data: size=%d,%d image=%d,%d anchor=%d,%d" % (imageWidth, imageHeight,
                    imageX, imageY, anchorXPos, anchorYPos);

    anchorOffsetX = anchorXPos - imageX;
    anchorOffsetY = imageHeight + imageY - anchorYPos;
    anchorX = 1.0 * anchorOffsetX / imageWidth;
    anchorY = 1.0 * anchorOffsetY / imageHeight;

    print "Anchor %f,%f" % (anchorX, anchorY);

    return [anchorX, anchorY];
