
import pytmx

import os
from FileHelper import *
import pytmx
from pytmx import *

def getIntArray(inputStr):
    result = [];

    strArray = inputStr.split(",");
    for str in strArray:
        result.append(int(str));

    return result;

def getRangeArray(fromArray, toArray):
    listSize = len(toArray);

    result = [];
    for i in range(0, listSize-1):
        #print "%d" % i
        fromValue = fromArray[i];
        toValue = toArray[i];
        endValue = fromArray[i+1];
        #print "%d %d %d" % (fromValue, toValue, endValue);
        data = {};
        data["from"] = fromValue;
        data["to"] = toValue;
        data["fromEnd"] = endValue;

        result.append(data);



    result.reverse();
    return result;

def batchMergeItemEnemy(inputDir, excludeSet):
    fileList = [];
    for path, dirs, files in os.walk(inputDir):
        #print("path: %s | %s  | %s\n" % (path, dirs, files))
        for file in files:
            if(file.endswith(".tmx") == False):
                continue;
            fullname = "%s%s" % (path, file);
            #print fullname;
            fileList.append(fullname);

    for file in fileList:
        print "Converting %s" % file;
        modifyMergeItemEnemy(file);


def modifyMergeItemEnemy(filename):
     firstGidMap = [
     ];
     gidMap = [
     ];
     basename = os.path.basename(filename);

     outputFile = "./output/%s" % basename;
     #print "outputFile: " + outputFile;
     #modifyTmxGid("./data/map0.tmx", "./temp/map0.tmx", "1,4,12,100", "1,10,50,100");
     content = modifyTmxContent(filename, firstGidMap, gidMap);

     # Extra Modification
     #<tileset firstgid="21" source="item.tsx"/>
     content = content.replace("<tileset firstgid=\"21\" source=\"item.tsx\"/>", "");
     content = content.replace("source=\"enemy.tsx\"", "source=\"model.tsx\"");

     write_file(outputFile, content);

#<tileset firstgid="1" source="lines.tsx"/>
# <tile gid="2"/>
##
##
##  mapping = array {
##      "5=10"
##      "6=12"
## }
##   keys =
def modifyTmxContent(filename, firstGidMap, gidMap):
    content = read_file(filename);

    ## Modify first  gid
    for data in firstGidMap:
        strArray = data.split(",");
        fromGid = strArray[0];
        toGid = strArray[1];

        print "%s %s->%s" % (data, fromGid, toGid);

        foundText = 'firstgid="%s"' % fromGid;
        replaceText = 'firstgid="%s"' % toGid;
        content = content.replace(foundText, replaceText);


    ## Modify first  gid
    for data in gidMap:
        strArray = data.split(",");
        fromGid = strArray[0];
        toGid = strArray[1];

        print "%s %s->%s" % (data, fromGid, toGid);
        foundText = 'gid="%s"' % fromGid;
        replaceText = 'gid="%s"' % toGid;
        content = content.replace(foundText, replaceText);

    # print "%s" % content;
    return content;

    def modifyTmxFile(filename, firstGidMap, gidMap):
        content = read_file(filename);

        ## Modify first  gid
        for data in firstGidMap:
            strArray = data.split(",");
            fromGid = strArray[0];
            toGid = strArray[1];

            print "%s %s->%s" % (data, fromGid, toGid);

            foundText = 'firstgid="%s"' % fromGid;
            replaceText = 'firstgid="%s"' % toGid;
            content = content.replace(foundText, replaceText);


        ## Modify first  gid
        for data in gidMap:
            strArray = data.split(",");
            fromGid = strArray[0];
            toGid = strArray[1];

            print "%s %s->%s" % (data, fromGid, toGid);
            foundText = 'gid="%s"' % fromGid;
            replaceText = 'gid="%s"' % toGid;
            content = content.replace(foundText, replaceText);

        write_file(outFile, content);


    # for data in rangeArray:
    #     #print "%s" % data;
    #
    #
    #     diff = data["to"] - data["from"];
    #     if(diff == 0):
    #         break;
    #
    #     firstGidOrigin = 'firstgid="%d"' % data["from"];
    #     firstGidNew = 'firstgid="%d"' % data["to"];
    #
    #     print "[%s] -> [%s]" % (firstGidOrigin, firstGidNew);
    #     rangeArray = range(data["fromEnd"]-1, data["from"]-1, -1);
    #
    #     content = content.replace(firstGidOrigin, firstGidNew);
    #
    #     print "DEBUG-Range: %s" % rangeArray;
    #     for gid in rangeArray:
    #         newGid = gid + diff;
    #
    #         gidOrigin = ' gid="%d"' % gid;
    #         gidNew = ' gid="%d"' % newGid;
    #         print "[%s] -> [%s]" % (gidOrigin, gidNew);
    #         content = content.replace(gidOrigin, gidNew);
    #     # for gid in
    #
    # #print "%s" % content;
    #
    # write_file(outFile, content);


def processGidChange(filename, outFile, rangeArray):
    content = read_file(filename);


    for data in rangeArray:
        #print "%s" % data;


        diff = data["to"] - data["from"];
        if(diff == 0):
            break;

        firstGidOrigin = 'firstgid="%d"' % data["from"];
        firstGidNew = 'firstgid="%d"' % data["to"];

        print "[%s] -> [%s]" % (firstGidOrigin, firstGidNew);
        rangeArray = range(data["fromEnd"]-1, data["from"]-1, -1);

        content = content.replace(firstGidOrigin, firstGidNew);

        print "DEBUG-Range: %s" % rangeArray;
        for gid in rangeArray:
            newGid = gid + diff;

            gidOrigin = ' gid="%d"' % gid;
            gidNew = ' gid="%d"' % newGid;
            print "[%s] -> [%s]" % (gidOrigin, gidNew);
            content = content.replace(gidOrigin, gidNew);
        # for gid in

    #print "%s" % content;

    write_file(outFile, content);

def batchModifyTmxGid(fromPath, toPath, mapArray, fromGid, toGid):
    for mapID in mapArray:
        sourceFile = "%s/map%d.tmx" % (fromPath, mapID);
        targetFile = "%s/map%d.tmx" % (toPath, mapID);

        print "convert [%s] -> [%s]" % (sourceFile, targetFile);
        modifyTmxGid(sourceFile, targetFile, fromGid, toGid);


def modifyTmxGid(filename, outFile, fromGid, toGid):
    fromGidArray = getIntArray(fromGid);
    toGidArray = getIntArray(toGid);
    print "%s" % (fromGidArray);
    print "%s" % (toGidArray);

    rangeArray = getRangeArray(fromGidArray, toGidArray);

    #for data in rangeArray:
    #    print "%s" % data;
    processGidChange(filename, outFile, rangeArray);
