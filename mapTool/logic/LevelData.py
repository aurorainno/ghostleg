#



class LineData:
    def __init__(self, _lineId, _lineType, _xLine, _y, _prob):
        self.lineId = _lineId;
        self.lineType = _lineType;
        self.xLine = _xLine;
        self.y = _y;
        self.probability = _prob;

    def getKey(self):   # to prevent same line add twice
        return "%d~%d" % (self.xLine, self.y);

    def toString(self):
        result = "id=%d type=%d xLine=%d y=%d prob=%d" % (self.lineId,
                    self.lineType, self.xLine, self.y, self.probability);
        return result;


class NPCData:
    def __init__(self, _npcID, _x, _y, _faceDir):       # faceDir=1 ()
        self.npcID = _npcID;
        self.x = _x;
        self.y = _y;
        self.faceDir = _faceDir;
        self.refID = 0;

    def toString(self):
        result = "npcID=%d x=%d y=%d faceDir=%d ref=%d" % (self.npcID,
                self.x, self.y, self.faceDir, self.refID);
        return result;

class EnemyData:
    def __init__(self, _enemyType, _x, _y, _flipX, _flipY):
        self.enemyType = _enemyType;
        self.x = _x;
        self.y = _y;
        self.flipX = _flipX;
        self.flipY = _flipY;
        self.scaleX = 1.0;
        self.scaleY = 1.0;
        self.refID = 0;

    def toString(self):
        result = "type=%d x=%d y=%d flipX=%d flipY=%d scaleX=%f scaleY=%f ref=%d" % (self.enemyType,
                self.x, self.y, self.flipX, self.flipY,
                self.scaleX, self.scaleY, self.refID);
        return result;

class ItemData:
    def __init__(self, _itemType, _x, _y, _resID):
        self.itemType = _itemType;
        self.x = _x;
        self.y = _y;
        self.resID = _resID;

    def toString(self):
        result = "type=%d x=%d y=%d res=%d" % (self.itemType, self.x, self.y, self.resID);
        return result;
