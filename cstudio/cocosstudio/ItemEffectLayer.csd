<GameFile>
  <PropertyGroup Name="ItemEffectLayer" Type="Layer" ID="e98992bb-029f-4807-bdd1-a91bb6c43441" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="169" ctype="GameLayerObjectData">
        <Size X="120.0000" Y="20.0000" />
        <Children>
          <AbstractNodeData Name="CounterPanel" ActionTag="-789372721" Tag="170" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="20.0000" />
            <Children>
              <AbstractNodeData Name="item1" ActionTag="1186982605" VisibleForFrame="False" Tag="171" IconVisible="False" LeftMargin="-24.0000" RightMargin="71.0000" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="12.5000" Y="10.0000" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1042" Y="0.5000" />
                <PreSize X="0.6083" Y="4.2000" />
                <FileData Type="Normal" Path="icon_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item2" ActionTag="1293207381" VisibleForFrame="False" Tag="172" IconVisible="False" LeftMargin="-3.5308" RightMargin="50.5308" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.9692" Y="10.0000" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2747" Y="0.5000" />
                <PreSize X="0.6083" Y="4.2000" />
                <FileData Type="Normal" Path="icon_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item3" ActionTag="768875721" VisibleForFrame="False" Tag="173" IconVisible="False" LeftMargin="16.9385" RightMargin="30.0615" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.4385" Y="10.0000" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4453" Y="0.5000" />
                <PreSize X="0.6083" Y="4.2000" />
                <FileData Type="Normal" Path="icon_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item4" ActionTag="567660497" VisibleForFrame="False" Tag="174" IconVisible="False" LeftMargin="37.4077" RightMargin="9.5923" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.9077" Y="10.0000" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6159" Y="0.5000" />
                <PreSize X="0.6083" Y="4.2000" />
                <FileData Type="Normal" Path="icon_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="item5" ActionTag="1621244712" VisibleForFrame="False" Tag="175" IconVisible="False" LeftMargin="57.8770" RightMargin="-10.8770" TopMargin="-32.0000" BottomMargin="-32.0000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="94.3770" Y="10.0000" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7865" Y="0.5000" />
                <PreSize X="0.6083" Y="4.2000" />
                <FileData Type="Normal" Path="icon_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="TimerPanel" ActionTag="702785625" Tag="176" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="20.0000" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="-374077002" Tag="183" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-21.1640" RightMargin="68.1640" TopMargin="-32.6754" BottomMargin="-31.3246" ctype="SpriteObjectData">
                <Size X="73.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="15.3360" Y="10.6754" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1278" Y="0.5338" />
                <PreSize X="0.6083" Y="4.2000" />
                <FileData Type="Normal" Path="icon_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="text" ActionTag="-1552757891" Tag="184" RotationSkewX="0.2124" RotationSkewY="0.2124" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="26.4000" RightMargin="-33.4000" TopMargin="-2.5000" BottomMargin="-2.5000" FontSize="20" LabelText="10.0000 sec" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="127.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="26.4000" Y="10.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2200" Y="0.5000" />
                <PreSize X="1.0583" Y="1.2500" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>