<GameFile>
  <PropertyGroup Name="StarValueLayer" Type="Layer" ID="9d334cda-1021-4c3c-b499-a7750f9b13ef" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="200.0000" Y="40.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="793615398" Tag="2" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="0.9999" RightMargin="165.0001" TopMargin="3.0000" BottomMargin="3.0000" ctype="SpriteObjectData">
            <Size X="42.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="17.9999" Y="20.0000" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0900" Y="0.5000" />
            <PreSize X="0.1700" Y="0.8500" />
            <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="value" ActionTag="635321" Tag="3" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="-20.0000" TopMargin="-3.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="36" LabelText="999999" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="250.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="95.0000" Y="18.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="78" />
            <PrePosition X="0.4750" Y="0.4500" />
            <PreSize X="1.2500" Y="1.2750" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>