<GameFile>
  <PropertyGroup Name="NPCOrderItemView" Type="Layer" ID="e4a3bcd8-53cf-42d5-a9d6-2d815b9e55b5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="52" ctype="GameLayerObjectData">
        <Size X="290.5000" Y="126.0000" />
        <Children>
          <AbstractNodeData Name="bgSprite" ActionTag="-1703063053" Tag="53" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-136.7500" RightMargin="-136.7500" TopMargin="-62.5000" BottomMargin="-62.5000" ctype="SpriteObjectData">
            <Size X="564.0000" Y="251.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="145.2500" Y="63.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.9415" Y="1.9921" />
            <FileData Type="Normal" Path="guiImage/ui_customer_selection_bg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="npcSprite" ActionTag="-273391035" Tag="54" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="-37.2640" RightMargin="170.7640" TopMargin="-43.0058" BottomMargin="12.0058" ctype="SpriteObjectData">
            <Size X="157.0000" Y="157.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="41.2360" Y="90.5058" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1419" Y="0.7183" />
            <PreSize X="0.5404" Y="1.2460" />
            <FileData Type="Normal" Path="guiImage/ingame_mission_npc_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ratingPanel" ActionTag="-576195618" VisibleForFrame="False" Tag="56" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="168.0000" RightMargin="22.5000" TopMargin="32.2500" BottomMargin="62.2500" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="100.0000" Y="31.5000" />
            <Children>
              <AbstractNodeData Name="Sprite_3" ActionTag="-1157840883" Tag="57" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-3.0000" RightMargin="67.0000" TopMargin="-0.7500" BottomMargin="-0.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="15.0000" Y="15.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1500" Y="0.5000" />
                <PreSize X="0.3600" Y="1.0476" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3_0" ActionTag="-358519547" Tag="58" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.0000" RightMargin="46.0000" TopMargin="-0.7500" BottomMargin="-0.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="36.0000" Y="15.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3600" Y="0.5000" />
                <PreSize X="0.3600" Y="1.0476" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3_1" ActionTag="1775462380" Tag="59" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="40.0000" RightMargin="24.0000" TopMargin="-0.7500" BottomMargin="-0.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.0000" Y="15.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5800" Y="0.5000" />
                <PreSize X="0.3600" Y="1.0476" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star01" ActionTag="-8515397" Tag="60" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-3.0000" RightMargin="67.0000" TopMargin="-0.7500" BottomMargin="-0.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="15.0000" Y="15.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1500" Y="0.5000" />
                <PreSize X="0.3600" Y="1.0476" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star02" ActionTag="1429281247" Tag="61" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.0000" RightMargin="46.0000" TopMargin="-0.7500" BottomMargin="-0.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="36.0000" Y="15.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3600" Y="0.5000" />
                <PreSize X="0.3600" Y="1.0476" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star03" ActionTag="1896022605" VisibleForFrame="False" Tag="62" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="40.0000" RightMargin="24.0000" TopMargin="-0.7500" BottomMargin="-0.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="58.0000" Y="15.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5800" Y="0.5000" />
                <PreSize X="0.3600" Y="1.0476" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="168.0000" Y="78.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5783" Y="0.6190" />
            <PreSize X="0.3442" Y="0.2500" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="gpsIcon" ActionTag="1312788439" Tag="63" IconVisible="False" LeftMargin="63.5913" RightMargin="190.9087" TopMargin="16.0000" BottomMargin="66.0000" ctype="SpriteObjectData">
            <Size X="36.0000" Y="44.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="81.5913" Y="88.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2809" Y="0.6984" />
            <PreSize X="0.1239" Y="0.3492" />
            <FileData Type="Normal" Path="guiImage/ui_customer_meter.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="distanceUnitText" ActionTag="-1674820281" Tag="1670" IconVisible="False" LeftMargin="123.0000" RightMargin="134.5000" TopMargin="20.5000" BottomMargin="64.5000" FontSize="24" LabelText="m" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="33.0000" Y="41.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="123.0000" Y="85.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4234" Y="0.6746" />
            <PreSize X="0.1136" Y="0.3254" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="6" G="98" B="118" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="distanceText" ActionTag="-1751087020" Tag="64" IconVisible="False" LeftMargin="93.0500" RightMargin="128.4500" TopMargin="10.0000" BottomMargin="58.0000" FontSize="36" LabelText="88" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="69.0000" Y="58.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="93.0500" Y="87.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3203" Y="0.6905" />
            <PreSize X="0.2375" Y="0.4603" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="6" G="98" B="118" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeSprite" ActionTag="-1350256903" Tag="65" IconVisible="False" LeftMargin="152.4114" RightMargin="95.0886" TopMargin="14.0000" BottomMargin="68.0000" ctype="SpriteObjectData">
            <Size X="43.0000" Y="44.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="173.9114" Y="90.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5987" Y="0.7143" />
            <PreSize X="0.1480" Y="0.3492" />
            <FileData Type="Normal" Path="guiImage/ui_customer_time.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeUnitText" ActionTag="417083256" Tag="1671" IconVisible="False" LeftMargin="205.0000" RightMargin="26.5000" TopMargin="20.5000" BottomMargin="64.5000" FontSize="24" LabelText="sec" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="59.0000" Y="41.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="205.0000" Y="85.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7057" Y="0.6746" />
            <PreSize X="0.2031" Y="0.3254" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="6" G="98" B="118" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeText" ActionTag="-9691513" Tag="66" IconVisible="False" LeftMargin="188.1458" RightMargin="67.3542" TopMargin="10.0000" BottomMargin="58.0000" FontSize="36" LabelText="4" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="35.0000" Y="58.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="188.1458" Y="87.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6477" Y="0.6905" />
            <PreSize X="0.1205" Y="0.4603" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="6" G="98" B="118" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bonusTimeText" ActionTag="-104828534" Tag="1672" IconVisible="False" LeftMargin="233.0000" RightMargin="15.5000" TopMargin="17.5000" BottomMargin="83.5000" FontSize="18" LabelText="+10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="42.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="233.0000" Y="96.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="223" B="4" />
            <PrePosition X="0.8021" Y="0.7619" />
            <PreSize X="0.1446" Y="0.1984" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="text" ActionTag="-540455246" Tag="68" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="45.8436" RightMargin="147.6564" TopMargin="67.8622" BottomMargin="30.1378" FontSize="20" LabelText="reward" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="97.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="45.8436" Y="44.1378" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="223" B="4" />
            <PrePosition X="0.1578" Y="0.3503" />
            <PreSize X="0.3339" Y="0.2222" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="rewardText" ActionTag="652915418" Tag="70" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="119.0299" RightMargin="150.4701" TopMargin="67.3582" BottomMargin="30.6418" FontSize="20" LabelText="8" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="21.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="119.0299" Y="44.6418" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="223" B="4" />
            <PrePosition X="0.4097" Y="0.3543" />
            <PreSize X="0.0723" Y="0.2222" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bonusRewardText" ActionTag="1203054679" Tag="1669" IconVisible="False" LeftMargin="130.0000" RightMargin="118.5000" TopMargin="62.8543" BottomMargin="38.1457" FontSize="18" LabelText="+10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="42.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="130.0000" Y="50.6457" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="223" B="4" />
            <PrePosition X="0.4475" Y="0.4020" />
            <PreSize X="0.1446" Y="0.1984" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="moneySprite" ActionTag="356667508" Tag="1668" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="87.9252" RightMargin="158.5748" TopMargin="61.1566" BottomMargin="24.8434" ctype="SpriteObjectData">
            <Size X="44.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="109.9252" Y="44.8434" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3784" Y="0.3559" />
            <PreSize X="0.1515" Y="0.3175" />
            <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="charFragPanel" ActionTag="-299350978" Tag="2209" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="90.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="290.5000" Y="36.0000" />
            <Children>
              <AbstractNodeData Name="charSmallIcon" ActionTag="813452263" Tag="2182" IconVisible="False" LeftMargin="68.1838" RightMargin="141.3162" TopMargin="-29.7414" BottomMargin="-17.2586" ctype="SpriteObjectData">
                <Size X="81.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.6838" Y="24.2414" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3741" Y="0.6734" />
                <PreSize X="0.2788" Y="2.3056" />
                <FileData Type="Normal" Path="guiImage/ui_character_lvlbg_profile.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_2" ActionTag="-1156787169" Tag="2185" IconVisible="False" LeftMargin="151.0000" RightMargin="-62.5000" BottomMargin="14.0000" Scale9Enable="True" LeftEage="66" RightEage="66" TopEage="7" BottomEage="7" Scale9OriginX="66" Scale9OriginY="7" Scale9Width="70" Scale9Height="8" ctype="ImageViewObjectData">
                <Size X="202.0000" Y="22.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="151.0000" Y="25.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5198" Y="0.6944" />
                <PreSize X="0.6954" Y="0.6111" />
                <FileData Type="Normal" Path="guiImage/ui_customer_shardbg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardFragText" ActionTag="-740226685" Tag="2208" IconVisible="False" LeftMargin="120.0000" RightMargin="128.5000" TopMargin="-1.7931" BottomMargin="9.7931" FontSize="20" LabelText="x 2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="42.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="120.0000" Y="23.7931" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4131" Y="0.6609" />
                <PreSize X="0.1446" Y="0.7778" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="progressBar" ActionTag="-1057526381" Tag="2207" IconVisible="False" LeftMargin="109.0000" RightMargin="-4.5000" TopMargin="-1.5001" BottomMargin="12.5001" ctype="LoadingBarObjectData">
                <Size X="186.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="202.0000" Y="25.0001" />
                <Scale ScaleX="0.5600" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6954" Y="0.6944" />
                <PreSize X="0.6403" Y="0.6944" />
                <ImageFileData Type="Normal" Path="guiImage/ui_character_shard_bar.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="collectedFragText" ActionTag="-233857496" Tag="2180" IconVisible="False" LeftMargin="182.0252" RightMargin="67.4748" TopMargin="3.0699" BottomMargin="15.9301" FontSize="12" LabelText="8/10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="41.0000" Y="17.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="202.5252" Y="24.4301" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6972" Y="0.6786" />
                <PreSize X="0.1411" Y="0.4722" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.2857" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="touchPanel" ActionTag="1550883458" Tag="71" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="290.5000" Y="126.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>