<GameFile>
  <PropertyGroup Name="NotEnoughCoinsLayer" Type="Layer" ID="5ad1f954-3fc9-4aa7-b4dd-8bf86cf9aa00" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="NotEnoughCoinsLayer" Tag="115" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="cover" ActionTag="-1522464889" Tag="116" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="-0.3408" BottomMargin="0.3408" TouchEnable="True" ClipAble="False" BackColorAlpha="216" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.3408" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5006" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="backBtn" ActionTag="1731047876" Tag="140" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-48.7999" RightMargin="168.7999" TopMargin="-20.0676" BottomMargin="472.0676" TouchEnable="True" FontSize="36" ButtonText="back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="116.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.2001" Y="530.0676" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1600" Y="0.9332" />
            <PreSize X="0.6250" Y="0.2042" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="49" G="216" B="212" />
            <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_11" ActionTag="-1192466878" Tag="141" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="46.4680" RightMargin="150.5320" TopMargin="163.5800" BottomMargin="379.4200" FontSize="20" LabelText="require more" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="123.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="107.9680" Y="391.9200" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3374" Y="0.6900" />
            <PreSize X="0.3844" Y="0.0440" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_2" ActionTag="321181348" Tag="142" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="112.4830" RightMargin="99.5170" TopMargin="122.0800" BottomMargin="337.9200" ctype="SpriteObjectData">
            <Size X="108.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="166.4830" Y="391.9200" />
            <Scale ScaleX="0.1100" ScaleY="0.1100" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5203" Y="0.6900" />
            <PreSize X="0.3375" Y="0.1901" />
            <FileData Type="Normal" Path="star_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_11_0" ActionTag="1146036982" Tag="143" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="165.6600" RightMargin="53.3400" TopMargin="163.5800" BottomMargin="379.4200" FontSize="20" LabelText="to update" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="101.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="216.1600" Y="391.9200" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6755" Y="0.6900" />
            <PreSize X="0.3156" Y="0.0440" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="movieBtn" ActionTag="1791728651" Tag="144" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-322.0000" RightMargin="-322.0000" TopMargin="129.6460" BottomMargin="238.3612" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="934" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="964.0000" Y="199.9928" />
            <Children>
              <AbstractNodeData Name="Text_11_1" ActionTag="1377633389" Tag="146" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="305.2200" RightMargin="389.7800" TopMargin="137.4928" BottomMargin="37.5000" FontSize="20" LabelText="watch a video to gain 100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="269.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="439.7200" Y="50.0000" />
                <Scale ScaleX="2.5000" ScaleY="2.5000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.4561" Y="0.2500" />
                <PreSize X="0.2790" Y="0.1250" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3" ActionTag="77401605" Tag="147" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="768.6776" RightMargin="87.3224" TopMargin="91.9928" ctype="SpriteObjectData">
                <Size X="108.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="822.6776" Y="54.0000" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8534" Y="0.2700" />
                <PreSize X="0.1120" Y="0.5400" />
                <FileData Type="Normal" Path="masteries_star.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="338.3576" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5957" />
            <PreSize X="3.0125" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="PlistSubImage" Path="btn_onemore.png" Plist="gui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="shopBtn" ActionTag="-342313202" Tag="145" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-322.0000" RightMargin="-322.0000" TopMargin="191.5012" BottomMargin="176.5060" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="934" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="964.0000" Y="199.9928" />
            <Children>
              <AbstractNodeData Name="Text_11_1_0" ActionTag="-532808276" Tag="148" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="457.5000" RightMargin="457.5000" TopMargin="137.4928" BottomMargin="37.5000" FontSize="20" LabelText="shop" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="49.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="482.0000" Y="50.0000" />
                <Scale ScaleX="2.5000" ScaleY="2.5000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.5000" Y="0.2500" />
                <PreSize X="0.0508" Y="0.1250" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="276.5024" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4868" />
            <PreSize X="3.0125" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn_shop.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>