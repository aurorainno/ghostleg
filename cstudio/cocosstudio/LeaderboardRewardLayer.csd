<GameFile>
  <PropertyGroup Name="LeaderboardRewardLayer" Type="Layer" ID="43ba8f6a-85cc-4a20-8cb7-61f9761d5c3d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1412" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgPanel" ActionTag="-136096206" Tag="1413" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="2077040032" Tag="1449" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-154.5000" RightMargin="-154.5000" TopMargin="37.0000" BottomMargin="-515.0000" Scale9Enable="True" LeftEage="198" RightEage="198" TopEage="297" BottomEage="297" Scale9OriginX="198" Scale9OriginY="297" Scale9Width="167" Scale9Height="136" ctype="ImageViewObjectData">
            <Size X="629.0000" Y="1046.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="531.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9349" />
            <PreSize X="1.9656" Y="1.8415" />
            <FileData Type="Normal" Path="guiImage/ui_character_statsinfo.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="backBtn" ActionTag="743585272" Tag="1450" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-15.6442" RightMargin="199.6442" TopMargin="29.0210" BottomMargin="453.9790" TouchEnable="True" FontSize="36" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="136.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="52.3558" Y="496.4790" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1636" Y="0.8741" />
            <PreSize X="0.4250" Y="0.1496" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="-60775317" Tag="1451" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="86.0000" RightMargin="86.0000" TopMargin="50.5159" BottomMargin="471.4841" FontSize="26" LabelText="rewards" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="148.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="494.4841" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8706" />
            <PreSize X="0.4625" Y="0.0810" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="1" G="44" B="98" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward01" ActionTag="1739352771" Tag="1452" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="100.0000" BottomMargin="429.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-287197419" Tag="1455" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_on.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="819936095" Tag="1458" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="13.0000" RightMargin="228.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="1st" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="53.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="13.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0442" Y="0.5000" />
                <PreSize X="0.1800" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="-1817289262" Tag="1459" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="994618223" Tag="1460" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="2138013937" Tag="1461" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="1178245884" Tag="1462" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="468.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8239" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward02" ActionTag="-700757608" Tag="1484" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="143.0000" BottomMargin="386.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1862327244" Tag="1485" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_on.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="669970119" Tag="1486" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="13.0000" RightMargin="220.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="2nd" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="61.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="13.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0442" Y="0.5000" />
                <PreSize X="0.2072" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="-442418728" Tag="1487" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="-1560092787" Tag="1488" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="-1797559874" Tag="1489" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="1942103528" Tag="1490" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="425.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7482" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward03" ActionTag="907370778" Tag="1491" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="187.0000" BottomMargin="342.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-1927945788" Tag="1492" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_on.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="-1267803821" Tag="1493" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="13.0000" RightMargin="223.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="3rd" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="58.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="13.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0442" Y="0.5000" />
                <PreSize X="0.1970" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="69013137" Tag="1494" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_3.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="433690400" Tag="1495" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="-1399732356" Tag="1496" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="1423232737" Tag="1497" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="381.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6708" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward04" ActionTag="5052921" Tag="1505" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="231.0000" BottomMargin="298.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-415765186" Tag="1506" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="-2006782587" Tag="1507" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.5000" RightMargin="256.9000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="4" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="22.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="26.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0900" Y="0.5000" />
                <PreSize X="0.0747" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="1747707517" Tag="1508" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="1068941461" Tag="1509" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="-363826020" Tag="1510" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="2038450192" Tag="1511" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="337.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5933" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward05" ActionTag="-88890982" Tag="1512" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="275.0000" BottomMargin="254.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-474841607" Tag="1513" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="-1507449502" Tag="1514" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.5000" RightMargin="256.9000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="5" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="22.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="26.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0900" Y="0.5000" />
                <PreSize X="0.0747" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="651749806" Tag="1515" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="-1225745064" Tag="1516" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="-265323201" Tag="1517" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="1429299861" Tag="1518" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="293.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5158" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward06" ActionTag="72506844" Tag="1498" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="319.0000" BottomMargin="210.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1143219712" Tag="1499" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="1187182610" Tag="1500" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-4.5000" RightMargin="236.9000" TopMargin="4.5000" BottomMargin="4.5000" FontSize="22" LabelText="6-10" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="26.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0900" Y="0.5000" />
                <PreSize X="0.2106" Y="0.7692" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="547499998" Tag="1501" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="892355569" Tag="1502" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="1672855303" Tag="1503" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="2140668497" Tag="1504" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="249.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4384" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward07" ActionTag="75588918" Tag="1540" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="363.0000" BottomMargin="166.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1881265595" Tag="1541" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="-1140990929" Tag="1542" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-7.5000" RightMargin="227.9000" TopMargin="4.5000" BottomMargin="4.5000" FontSize="22" LabelText="11-50" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="74.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="29.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1002" Y="0.5000" />
                <PreSize X="0.2514" Y="0.7692" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="-555748028" Tag="1543" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="24.0000" RightMargin="192.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2140" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="515093955" Tag="1544" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="2020735075" Tag="1545" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="-299279386" Tag="1546" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="205.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3609" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward08" ActionTag="1383571825" Tag="1533" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="407.0000" BottomMargin="122.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-202256673" Tag="1534" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="1125916438" Tag="1535" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-13.0000" RightMargin="214.4000" TopMargin="4.5000" BottomMargin="4.5000" FontSize="22" LabelText="51-100" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="93.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="33.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1138" Y="0.5000" />
                <PreSize X="0.3159" Y="0.7692" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="-1205929820" Tag="1536" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="34.0000" RightMargin="182.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="73.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2480" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="-1707486172" Tag="1537" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="942544755" Tag="1538" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="-1547895139" Tag="1539" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="161.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2835" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward09" ActionTag="-73398329" Tag="1519" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="451.0000" BottomMargin="78.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-93788524" Tag="1520" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="-2099220704" Tag="1521" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-19.0000" RightMargin="200.4000" TopMargin="4.5000" BottomMargin="4.5000" FontSize="22" LabelText="101-200" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="113.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="37.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1274" Y="0.5000" />
                <PreSize X="0.3838" Y="0.7692" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="844771873" Tag="1522" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="43.0000" RightMargin="173.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="82.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2785" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="-1541348221" Tag="1523" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="-1840840075" Tag="1524" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="-1259914742" Tag="1525" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="117.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2060" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="reward10" ActionTag="622454044" Tag="1526" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="12.8000" RightMargin="12.8000" TopMargin="495.0000" BottomMargin="34.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="294.4000" Y="39.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1903956657" Tag="1527" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-294.4000" TopMargin="-39.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="197" Scale9Height="51" ctype="ImageViewObjectData">
                <Size X="588.8000" Y="78.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_bg_off.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankText" ActionTag="854622595" Tag="1528" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-19.5000" RightMargin="195.9000" TopMargin="4.5000" BottomMargin="4.5000" FontSize="22" LabelText="201-500" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="118.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="39.5000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1342" Y="0.5000" />
                <PreSize X="0.4008" Y="0.7692" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="-1528295231" Tag="1529" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="47.0000" RightMargin="169.4000" TopMargin="-19.5000" BottomMargin="-19.5000" ctype="SpriteObjectData">
                <Size X="78.0000" Y="78.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="86.0000" Y="19.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2921" Y="0.5000" />
                <PreSize X="0.2649" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_inbox_reward_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" Visible="False" ActionTag="-1217290110" Tag="1530" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="204.0000" RightMargin="48.4000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1427" Y="1.0769" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="-1186152859" Tag="1531" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="207.0000" RightMargin="51.4000" TopMargin="1.5000" BottomMargin="1.5000" ctype="SpriteObjectData">
                <Size X="36.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="225.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7643" Y="0.5000" />
                <PreSize X="0.1223" Y="0.9231" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amountText" ActionTag="1018974008" Tag="1532" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="236.0000" RightMargin="16.4000" TopMargin="3.0000" BottomMargin="3.0000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="42.0000" Y="33.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="278.0000" Y="19.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9443" Y="0.5000" />
                <PreSize X="0.1427" Y="0.8462" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="73.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1285" />
            <PreSize X="0.9200" Y="0.0687" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>