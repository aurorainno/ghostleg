<GameFile>
  <PropertyGroup Name="masteries-subView" Type="Layer" ID="60b9142c-11a7-4a52-9c89-28b48789548c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="156" ctype="GameLayerObjectData">
        <Size X="125.0000" Y="178.0000" />
        <Children>
          <AbstractNodeData Name="Image_1" ActionTag="930735956" Tag="159" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-67.5000" RightMargin="-67.5000" TopMargin="-96.5828" BottomMargin="-95.6572" Scale9Enable="True" LeftEage="18" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="18" Scale9OriginY="18" Scale9Width="64" Scale9Height="64" ctype="ImageViewObjectData">
            <Size X="260.0000" Y="370.2400" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="89.4628" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5026" />
            <PreSize X="2.0800" Y="2.0800" />
            <FileData Type="Normal" Path="ui_gameover_pop.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="iconPanel" ActionTag="820105851" Tag="158" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-2.5000" RightMargin="-2.5000" TopMargin="29.6548" BottomMargin="102.0652" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="130.0000" Y="46.2800" />
            <Children>
              <AbstractNodeData Name="icon" ActionTag="369599927" Tag="196" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="-64.8600" BottomMargin="-64.8600" ctype="SpriteObjectData">
                <Size X="176.0000" Y="176.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="65.0000" Y="23.1400" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.3538" Y="3.8029" />
                <FileData Type="Normal" Path="masteries_01.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="125.2052" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7034" />
            <PreSize X="1.0400" Y="0.2600" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="descriptionText" ActionTag="-526447459" Tag="161" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-118.5000" RightMargin="-118.5000" TopMargin="42.3076" BottomMargin="33.6924" FontSize="40" LabelText="Increases maximum &#xA;resource storage" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="362.0000" Y="102.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="84.6924" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4758" />
            <PreSize X="2.8960" Y="0.5730" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="titleText" ActionTag="-1954429829" Tag="165" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-69.5000" RightMargin="-69.5000" TopMargin="-8.0000" BottomMargin="130.0000" FontSize="44" LabelText="max storage" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="264.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="158.0000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8876" />
            <PreSize X="2.1120" Y="0.3146" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="upgradeValue_first" Visible="False" ActionTag="-1633961109" Tag="166" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-37.5000" RightMargin="-37.5000" TopMargin="98.0000" BottomMargin="30.0000" Scale9Enable="True" LeftEage="18" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="18" Scale9OriginY="18" Scale9Width="19" Scale9Height="19" ctype="ImageViewObjectData">
            <Size X="200.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="valueText" ActionTag="-83042907" Tag="171" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="-0.5000" BottomMargin="-0.5000" FontSize="40" LabelText="+0.5" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="84.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="25.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4200" Y="1.0200" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="55.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3090" />
            <PreSize X="1.6000" Y="0.2809" />
            <FileData Type="Normal" Path="ui_score_pop.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="upgradeValue_second" ActionTag="-565104488" Tag="170" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-37.5000" RightMargin="-37.5000" TopMargin="98.0000" BottomMargin="30.0000" Scale9Enable="True" LeftEage="18" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="18" Scale9OriginY="18" Scale9Width="19" Scale9Height="19" ctype="ImageViewObjectData">
            <Size X="200.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="valueTextCurrent" ActionTag="-1043941228" Tag="179" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="90.0000" TopMargin="-0.5000" BottomMargin="-0.5000" FontSize="40" LabelText="+50%" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="60.0000" Y="25.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3000" Y="0.5000" />
                <PreSize X="0.5000" Y="1.0200" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="valueTextNext" ActionTag="1550714913" Tag="180" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.5000" RightMargin="10.5000" TopMargin="-0.5000" BottomMargin="-0.5000" FontSize="40" LabelText="+80%" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="99.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="140.0000" Y="25.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.7000" Y="0.5000" />
                <PreSize X="0.4950" Y="1.0200" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_6" ActionTag="-1775633009" Tag="183" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="88.0000" RightMargin="88.0000" TopMargin="7.0000" BottomMargin="7.0000" LeftEage="7" RightEage="7" TopEage="11" BottomEage="11" Scale9OriginX="7" Scale9OriginY="11" Scale9Width="10" Scale9Height="14" ctype="ImageViewObjectData">
                <Size X="24.0000" Y="36.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="25.0000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.1200" Y="0.7200" />
                <FileData Type="Normal" Path="masteries_next.png" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="55.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3090" />
            <PreSize X="1.6000" Y="0.2809" />
            <FileData Type="Normal" Path="ui_score_pop.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="levelText" ActionTag="-1983894306" Tag="173" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="5.5000" RightMargin="5.5000" TopMargin="-15.0000" BottomMargin="147.0000" FontSize="36" LabelText="level 1" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="114.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="170.0000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.5000" Y="0.9551" />
            <PreSize X="0.9120" Y="0.2584" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="normalPricePanel" ActionTag="460027859" Tag="182" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="17.5000" RightMargin="17.5000" TopMargin="143.0000" BottomMargin="5.0000" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="90.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="priceBtn" ActionTag="887494813" Tag="174" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-155.0000" RightMargin="-155.0000" TopMargin="-55.0000" BottomMargin="-55.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="400.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.0000" Y="15.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="4.4444" Y="4.6667" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_5" ActionTag="-1954105297" Tag="175" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-44.0000" RightMargin="26.0000" TopMargin="-39.0000" BottomMargin="-39.0000" LeftEage="35" RightEage="35" TopEage="35" BottomEage="35" Scale9OriginX="35" Scale9OriginY="35" Scale9Width="38" Scale9Height="38" ctype="ImageViewObjectData">
                <Size X="108.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="10.0000" Y="15.0000" />
                <Scale ScaleX="0.1500" ScaleY="0.1500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1111" Y="0.5000" />
                <PreSize X="1.2000" Y="3.6000" />
                <FileData Type="Normal" Path="star_icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceText" ActionTag="-1443742549" Tag="177" IconVisible="False" LeftMargin="-16.0000" RightMargin="-36.0000" TopMargin="-17.0000" BottomMargin="-9.0000" FontSize="44" LabelText="50000" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="142.0000" Y="56.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="19.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6111" Y="0.6333" />
                <PreSize X="1.5778" Y="1.8667" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceLevelText" ActionTag="503053178" Tag="178" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-93.0000" RightMargin="-113.0000" BottomMargin="-13.0000" FontSize="34" LabelText="upgrade to lvl 99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="296.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="8.5000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6111" Y="0.2833" />
                <PreSize X="3.2889" Y="1.4333" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="20.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1124" />
            <PreSize X="0.7200" Y="0.1685" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="maxLevelPricePanel" Visible="False" ActionTag="-600158905" Alpha="114" Tag="185" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="17.5000" RightMargin="17.5000" TopMargin="143.0000" BottomMargin="5.0000" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="90.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="priceBtn" ActionTag="906095405" Tag="186" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-155.0000" RightMargin="-155.0000" TopMargin="-55.0000" BottomMargin="-55.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="400.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.0000" Y="15.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="4.4444" Y="4.6667" />
                <TextColor A="255" R="65" G="65" B="70" />
                <PressedFileData Type="Normal" Path="bg_green_border.png" Plist="" />
                <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_5" ActionTag="-39322875" Tag="187" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-44.0000" RightMargin="26.0000" TopMargin="-39.0000" BottomMargin="-39.0000" LeftEage="35" RightEage="35" TopEage="35" BottomEage="35" Scale9OriginX="35" Scale9OriginY="35" Scale9Width="38" Scale9Height="38" ctype="ImageViewObjectData">
                <Size X="108.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="10.0000" Y="15.0000" />
                <Scale ScaleX="0.1500" ScaleY="0.1500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1111" Y="0.5000" />
                <PreSize X="1.2000" Y="3.6000" />
                <FileData Type="Normal" Path="star_icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceText" ActionTag="-638430073" Tag="188" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-44.5000" RightMargin="-64.5000" TopMargin="-13.0000" BottomMargin="-13.0000" FontSize="44" LabelText="max level" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="199.0000" Y="56.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="15.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6111" Y="0.5000" />
                <PreSize X="2.2111" Y="1.8667" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="20.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1124" />
            <PreSize X="0.7200" Y="0.1685" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockPricePanel" Visible="False" ActionTag="1225039670" Tag="190" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="17.5000" RightMargin="17.5000" TopMargin="143.0000" BottomMargin="5.0000" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="90.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="priceBtn" ActionTag="-1587427295" Tag="191" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-155.0000" RightMargin="-155.0000" TopMargin="-55.0000" BottomMargin="-55.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="400.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="45.0000" Y="15.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="4.4444" Y="4.6667" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_5" ActionTag="-974851639" Tag="192" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-44.0000" RightMargin="26.0000" TopMargin="-39.0000" BottomMargin="-39.0000" LeftEage="35" RightEage="35" TopEage="35" BottomEage="35" Scale9OriginX="35" Scale9OriginY="35" Scale9Width="38" Scale9Height="38" ctype="ImageViewObjectData">
                <Size X="108.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="10.0000" Y="15.0000" />
                <Scale ScaleX="0.1500" ScaleY="0.1500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1111" Y="0.5000" />
                <PreSize X="1.2000" Y="3.6000" />
                <FileData Type="Normal" Path="star_icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceText" ActionTag="-562133317" Tag="193" IconVisible="False" LeftMargin="12.0000" RightMargin="-8.0000" TopMargin="-17.0000" BottomMargin="-9.0000" FontSize="44" LabelText="500" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="86.0000" Y="56.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="19.0000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6111" Y="0.6333" />
                <PreSize X="0.9556" Y="1.8667" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceLevelText" ActionTag="1798977504" Tag="194" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="1.5000" RightMargin="-18.5000" BottomMargin="-13.0000" FontSize="34" LabelText="unlock" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="107.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="55.0000" Y="8.5000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6111" Y="0.2833" />
                <PreSize X="1.1889" Y="1.4333" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="62.5000" Y="20.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1124" />
            <PreSize X="0.7200" Y="0.1685" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>