<GameFile>
  <PropertyGroup Name="DeliveryCompleteNoticeView" Type="Layer" ID="e0cdf086-fe3f-4630-8787-2ea95e95cb31" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="497" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="104.0000" />
        <Children>
          <AbstractNodeData Name="bgSprite" ActionTag="656409053" Tag="498" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-160.0000" RightMargin="-160.0000" BottomMargin="-112.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="216.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="104.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="1.0000" />
            <PreSize X="2.0000" Y="2.0769" />
            <FileData Type="Normal" Path="guiImage/ingame_message.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="npcSprite" ActionTag="870052950" Tag="499" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-44.5000" RightMargin="207.5000" TopMargin="-33.0000" BottomMargin="-20.0000" ctype="SpriteObjectData">
            <Size X="157.0000" Y="157.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="34.0000" Y="58.5000" />
            <Scale ScaleX="0.3200" ScaleY="0.3200" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1063" Y="0.5625" />
            <PreSize X="0.4906" Y="1.5096" />
            <FileData Type="Normal" Path="guiImage/ingame_mission_npc_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_10" ActionTag="1498577270" Tag="540" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-4.5000" RightMargin="-4.5000" TopMargin="2.0000" BottomMargin="52.0000" FontSize="30" LabelText="delivery arrived!" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="329.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="77.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7404" />
            <PreSize X="1.0281" Y="0.4808" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="23" G="114" B="151" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="rewardPanel" ActionTag="-2065686166" Tag="545" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="104.0000" />
            <Children>
              <AbstractNodeData Name="moneySprite" ActionTag="-1427976188" Tag="1780" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="74.0000" RightMargin="202.0000" TopMargin="27.4747" BottomMargin="36.5253" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="96.0000" Y="56.5253" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3000" Y="0.5435" />
                <PreSize X="0.1375" Y="0.3846" />
                <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="mainRewardText" ActionTag="66735065" Tag="546" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="107.0000" RightMargin="-87.0000" TopMargin="32.5000" BottomMargin="38.5000" IsCustomSize="True" FontSize="24" LabelText="100 received!" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="300.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="107.0000" Y="55.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3344" Y="0.5288" />
                <PreSize X="0.9375" Y="0.3173" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="28" B="75" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="popText" ActionTag="-948930440" Tag="1782" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="107.0000" RightMargin="189.0000" TopMargin="32.5000" BottomMargin="38.5000" FontSize="24" LabelText="0" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="24.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="107.0000" Y="55.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="216" B="0" />
                <PrePosition X="0.3344" Y="0.5288" />
                <PreSize X="0.0750" Y="0.3173" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="28" B="75" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tipsPanel" ActionTag="-2132543568" Tag="551" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="104.0000" />
            <Children>
              <AbstractNodeData Name="moneySprite" ActionTag="-1023708981" Tag="1781" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="74.0000" RightMargin="202.0000" TopMargin="49.4421" BottomMargin="14.5579" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="96.0000" Y="34.5579" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3000" Y="0.3323" />
                <PreSize X="0.1375" Y="0.3846" />
                <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="emoji" ActionTag="-986908065" Tag="549" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="41.0000" RightMargin="195.0000" TopMargin="27.0000" BottomMargin="-7.0000" ctype="SpriteObjectData">
                <Size X="84.0000" Y="84.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="83.0000" Y="35.0000" />
                <Scale ScaleX="0.1800" ScaleY="0.1800" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2594" Y="0.3365" />
                <PreSize X="0.2625" Y="0.8077" />
                <FileData Type="Normal" Path="guiImage/imgame_emoji_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="bonusRewardText" ActionTag="-1792470261" Tag="550" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="107.0000" RightMargin="-87.0000" TopMargin="54.5000" BottomMargin="16.5000" IsCustomSize="True" FontSize="24" LabelText="15 bonus!" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="300.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="107.0000" Y="33.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3344" Y="0.3173" />
                <PreSize X="0.9375" Y="0.3173" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="28" B="75" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="popText" ActionTag="226995682" Tag="1783" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="107.0000" RightMargin="189.0000" TopMargin="54.5000" BottomMargin="16.5000" FontSize="24" LabelText="0" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="24.0000" Y="33.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="107.0000" Y="33.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="216" B="0" />
                <PrePosition X="0.3344" Y="0.3173" />
                <PreSize X="0.0750" Y="0.3173" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="28" B="75" />
                <ShadowColor A="255" R="255" G="216" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>