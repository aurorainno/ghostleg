<GameFile>
  <PropertyGroup Name="tapAnime" Type="Node" ID="9f36d2cc-ad8c-42af-b233-2e7914b3789f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="90" Speed="1.0000" ActivedAnimationName="tap">
        <Timeline ActionTag="-2030474419" Property="Position">
          <PointFrame FrameIndex="0" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="40" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="50" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="70" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="80" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="90" X="13.5835" Y="-10.3874">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2030474419" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="50" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="70" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="80" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="90" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2030474419" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="50" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="70" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="80" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="90" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1643004588" Property="Position">
          <PointFrame FrameIndex="0" X="-6.3888" Y="83.8986">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="-6.3888" Y="83.8986">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="40" X="-2.3885" Y="63.8987">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="50" X="-6.3888" Y="83.8986">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="60" X="-2.3885" Y="63.8987">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="70" X="-6.3888" Y="83.8986">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="80" X="-2.3885" Y="63.8987">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="90" X="-6.3888" Y="83.8986">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-1643004588" Property="Scale">
          <ScaleFrame FrameIndex="0" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="50" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="70" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="80" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="90" X="1.0000" Y="1.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1643004588" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="50" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="60" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="70" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="80" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="90" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="tap" StartIndex="0" EndIndex="150">
          <RenderColor A="255" R="128" G="128" B="0" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Node" Tag="115" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="btn_finger1_1" ActionTag="-2030474419" Tag="116" IconVisible="False" LeftMargin="-66.4165" RightMargin="-93.5835" TopMargin="-99.1126" BottomMargin="-119.8874" ctype="SpriteObjectData">
            <Size X="160.0000" Y="219.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="13.5835" Y="-10.3874" />
            <Scale ScaleX="0.7750" ScaleY="0.7750" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="btn_finger1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_finger_circle1_2" ActionTag="-1643004588" Tag="117" IconVisible="False" LeftMargin="-56.7885" RightMargin="-51.2115" TopMargin="-112.3987" BottomMargin="19.3987" ctype="SpriteObjectData">
            <Size X="108.0000" Y="93.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-2.7885" Y="65.8987" />
            <Scale ScaleX="0.7750" ScaleY="0.7750" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="btn_finger_circle1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>