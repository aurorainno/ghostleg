<GameFile>
  <PropertyGroup Name="MainScreenEventInfoLayer" Type="Layer" ID="c4335d0c-378f-4bec-803f-9c4d7bfbc6fd" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="449" ctype="GameLayerObjectData">
        <Size X="271.0000" Y="65.0000" />
        <Children>
          <AbstractNodeData Name="mainPanel" ActionTag="715246695" Tag="450" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="21.6800" RightMargin="21.6800" TopMargin="7.5000" BottomMargin="7.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="227.6400" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_2" ActionTag="1764775907" Tag="451" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.1800" RightMargin="-160.1800" TopMargin="-39.0000" BottomMargin="-41.0000" Scale9Enable="True" LeftEage="180" RightEage="180" TopEage="41" BottomEage="41" Scale9OriginX="180" Scale9OriginY="41" Scale9Width="188" Scale9Height="48" ctype="ImageViewObjectData">
                <Size X="548.0000" Y="130.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.8200" Y="24.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4800" />
                <PreSize X="2.4073" Y="2.6000" />
                <FileData Type="Normal" Path="guiImage/ui_xmas_menu_mission.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogIcon" ActionTag="-123472885" Tag="452" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-65.0000" RightMargin="148.6400" TopMargin="-48.0000" BottomMargin="-46.0000" ctype="SpriteObjectData">
                <Size X="144.0000" Y="144.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="7.0000" Y="26.0000" />
                <Scale ScaleX="0.3500" ScaleY="0.3500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0308" Y="0.5200" />
                <PreSize X="0.6326" Y="2.8800" />
                <FileData Type="Normal" Path="guiImage/dselect_dog26_unlock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="text" ActionTag="-985034948" Tag="453" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="35.0000" RightMargin="-207.3600" TopMargin="-4.5000" BottomMargin="-9.5000" IsCustomSize="True" FontSize="22" LabelText="Get the                 in limited time&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="400.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="35.0000" Y="22.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1538" Y="0.4500" />
                <PreSize X="1.7572" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogName" ActionTag="1643614683" Tag="455" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="17.6400" TopMargin="2.5000" BottomMargin="15.5000" IsCustomSize="True" FontSize="22" LabelText="X'Mas Dogs" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="200.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="110.0000" Y="31.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="59" G="255" B="215" />
                <PrePosition X="0.4832" Y="0.6300" />
                <PreSize X="0.8786" Y="0.6400" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_1" ActionTag="-779099067" Tag="456" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="35.0000" RightMargin="-207.3600" TopMargin="12.5000" BottomMargin="-26.5000" IsCustomSize="True" FontSize="22" LabelText="Tap here to see more info&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="400.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="35.0000" Y="5.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="211" B="49" />
                <PrePosition X="0.1538" Y="0.1100" />
                <PreSize X="1.7572" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="135.5000" Y="32.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8400" Y="0.7692" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>