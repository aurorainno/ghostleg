<GameFile>
  <PropertyGroup Name="BuyBoosterLayer" Type="Layer" ID="e4464ee8-7ef8-468f-a4ec-dbe28beead58" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="128" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgPanel" ActionTag="-148592846" Tag="129" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="46.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="522.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.9190" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-985971473" Tag="130" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="79.5200" BottomMargin="147.6800" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="304.0000" Y="340.8000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1096631248" Tag="132" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-148.0000" RightMargin="-148.0000" TopMargin="12.9000" BottomMargin="-447.1000" Scale9Enable="True" LeftEage="198" RightEage="198" TopEage="297" BottomEage="297" Scale9OriginX="198" Scale9OriginY="297" Scale9Width="204" Scale9Height="181" ctype="ImageViewObjectData">
                <Size X="600.0000" Y="775.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="152.0000" Y="327.9000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9621" />
                <PreSize X="1.9737" Y="2.2741" />
                <FileData Type="Normal" Path="guiImage/ui_booster_info_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="cancelBtn" ActionTag="-1377944687" Tag="931" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-26.0000" RightMargin="194.0000" TopMargin="0.1759" BottomMargin="255.6241" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="136.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="42.0000" Y="298.1241" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1382" Y="0.8748" />
                <PreSize X="0.4474" Y="0.2494" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemSprite" ActionTag="722308278" Tag="216" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="73.5000" RightMargin="73.5000" TopMargin="39.8000" BottomMargin="161.0000" ctype="SpriteObjectData">
                <Size X="157.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="231.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6778" />
                <PreSize X="0.5164" Y="0.4108" />
                <FileData Type="Normal" Path="guiImage/ui_booster_item1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="counterPanel" ActionTag="-252681865" Tag="218" IconVisible="False" LeftMargin="89.0000" RightMargin="165.0000" TopMargin="96.8000" BottomMargin="194.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="50.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="countBG" ActionTag="638010496" Tag="219" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-11.5000" BottomMargin="-11.5000" ctype="SpriteObjectData">
                    <Size X="73.0000" Y="73.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="25.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.4600" Y="1.4600" />
                    <FileData Type="Normal" Path="guiImage/ui_booster_badge_number.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="-1493466071" Tag="220" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="13.5000" RightMargin="15.5000" TopMargin="9.5000" BottomMargin="7.5000" FontSize="24" LabelText="3" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="21.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="24.0000" Y="24.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4800" Y="0.4800" />
                    <PreSize X="0.4200" Y="0.6600" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="89.0000" Y="194.0000" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2928" Y="0.5692" />
                <PreSize X="0.1645" Y="0.1467" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillName" ActionTag="1442604551" Tag="221" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="73.0000" RightMargin="73.0000" TopMargin="143.3000" BottomMargin="164.5000" FontSize="24" LabelText="snow ball" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="158.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="181.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5311" />
                <PreSize X="0.5197" Y="0.0968" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="83" B="187" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillInfo" ActionTag="2097161192" Tag="222" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="25.2249" RightMargin="-233.2249" TopMargin="163.8000" BottomMargin="77.0000" IsCustomSize="True" FontSize="20" LabelText="Any contact from your Astrodogs with wolves will destroy them for" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="512.0000" Y="100.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="25.2249" Y="127.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0830" Y="0.3727" />
                <PreSize X="1.6842" Y="0.2934" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="26" G="83" B="187" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="rightButton" ActionTag="-725374935" Tag="236" IconVisible="False" LeftMargin="193.4289" RightMargin="37.5711" TopMargin="249.3000" BottomMargin="18.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="73.0000" Y="73.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="229.9289" Y="55.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7563" Y="0.1614" />
                <PreSize X="0.2401" Y="0.2142" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_add.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leftButton" ActionTag="-1747167271" VisibleForFrame="False" Tag="237" IconVisible="False" LeftMargin="41.5000" RightMargin="189.5000" TopMargin="249.3000" BottomMargin="18.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="73.0000" Y="73.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="78.0000" Y="55.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2566" Y="0.1614" />
                <PreSize X="0.2401" Y="0.2142" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_minus.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="countText" ActionTag="322777595" Tag="240" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="121.5000" RightMargin="121.5000" TopMargin="261.8000" BottomMargin="29.0000" FontSize="36" LabelText="99" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="61.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="54.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1585" />
                <PreSize X="0.2007" Y="0.1467" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="purchaseBtn" ActionTag="-1703503594" Tag="241" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-24.5000" RightMargin="-24.5000" TopMargin="304.6283" BottomMargin="-57.8283" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="1700636555" Tag="242" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="124.0000" RightMargin="124.0000" TopMargin="28.5000" BottomMargin="28.5000" FontSize="24" LabelText="60000" HorizontalAlignmentType="HT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="105.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.2975" Y="0.3936" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="20" G="119" B="122" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="starSprite" ActionTag="-1753668037" Tag="300" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="69.7800" RightMargin="239.2200" TopMargin="23.2400" BottomMargin="30.7600" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="91.7800" Y="50.7600" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2600" Y="0.5400" />
                    <PreSize X="0.1246" Y="0.4255" />
                    <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="diamondSprite" ActionTag="-1296535781" Tag="376" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="59.1805" RightMargin="241.8195" TopMargin="20.2230" BottomMargin="28.7770" ctype="SpriteObjectData">
                    <Size X="52.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="85.1805" Y="51.2770" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2413" Y="0.5455" />
                    <PreSize X="0.1473" Y="0.4787" />
                    <FileData Type="Normal" Path="guiImage/ui_general_diamond.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="-10.8283" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.0318" />
                <PreSize X="1.1612" Y="0.2758" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="318.0800" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5600" />
            <PreSize X="0.9500" Y="0.6000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="topPanel" ActionTag="1079068889" Tag="575" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="468.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="ui_main_topbar_1" ActionTag="-1525377398" Tag="576" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                <Size X="640.0000" Y="92.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="100.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="2.0000" Y="0.9200" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="283895838" Tag="579" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="55.0575" ctype="SpriteObjectData">
                <Size X="166.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="185.1505" Y="73.5575" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5786" Y="0.7356" />
                <PreSize X="0.5188" Y="0.4000" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalCoinText" ActionTag="-1944687778" Tag="580" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="7.6674" BottomMargin="52.3326" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="100.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="214.3221" Y="72.3326" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6698" Y="0.7233" />
                <PreSize X="0.3125" Y="0.4000" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondPanel" ActionTag="392747102" Tag="584" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="61.0100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="120.0000" Y="25.0000" />
                <Children>
                  <AbstractNodeData Name="sprite" ActionTag="-440203487" Tag="585" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                    <Size X="162.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                    <Position X="60.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.3500" Y="1.6000" />
                    <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="totalDiamondText" ActionTag="1696654170" Tag="586" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="120.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7500" Y="0.5000" />
                    <PreSize X="1.0000" Y="1.6000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="1" G="44" B="98" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="335.0000" Y="61.0100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0469" Y="0.6101" />
                <PreSize X="0.3750" Y="0.2500" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbBtn" ActionTag="41162781" VisibleForFrame="False" Tag="587" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-35.0650" RightMargin="163.0650" TopMargin="-2.0000" BottomMargin="45.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="192.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="60.9350" Y="73.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1904" Y="0.7350" />
                <PreSize X="0.6000" Y="0.5700" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_fb.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="profilePicFrame" ActionTag="-1764821909" Tag="588" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.9350" RightMargin="252.0650" BottomMargin="48.0000" ctype="SpriteObjectData">
                <Size X="52.0000" Y="52.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="15.9350" Y="74.0000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0498" Y="0.7400" />
                <PreSize X="0.1625" Y="0.5200" />
                <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbName" ActionTag="-188408333" Tag="589" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="56.0000" RightMargin="106.0000" TopMargin="11.0000" BottomMargin="55.0000" FontSize="20" LabelText="ggg ggggg" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="158.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="56.0000" Y="72.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1750" Y="0.7200" />
                <PreSize X="0.4938" Y="0.3400" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="182" G="88" B="14" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="0.1761" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>