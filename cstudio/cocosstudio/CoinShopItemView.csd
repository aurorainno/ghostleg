<GameFile>
  <PropertyGroup Name="CoinShopItemView" Type="Layer" ID="2933a878-284a-4c5b-8343-27c22fd95673" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="67" ctype="GameLayerObjectData">
        <Size X="94.0000" Y="133.0000" />
        <Children>
          <AbstractNodeData Name="normalBG" ActionTag="-1667934109" Tag="1630" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-46.0000" RightMargin="-46.0000" TopMargin="-47.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
            <Size X="186.0000" Y="186.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="87.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6541" />
            <PreSize X="1.9787" Y="1.3985" />
            <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="alertText" ActionTag="-1159650348" VisibleForFrame="False" Tag="125" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-13.0000" RightMargin="-13.0000" TopMargin="74.5000" BottomMargin="33.5000" FontSize="18" LabelText="25% Extra" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="120.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="46.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="210" B="0" />
            <PrePosition X="0.5000" Y="0.3459" />
            <PreSize X="1.2766" Y="0.1880" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="info" ActionTag="-703583544" VisibleForFrame="False" Tag="29" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-18.0400" RightMargin="12.0400" TopMargin="41.5000" BottomMargin="41.5000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="100.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="1702343207" Tag="129" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-32.9900" RightMargin="-127.0100" TopMargin="-15.2500" BottomMargin="5.2500" IsCustomSize="True" FontSize="32" LabelText="Small" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="260.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="97.0100" Y="35.2500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9701" Y="0.7050" />
                <PreSize X="2.6000" Y="1.2000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="content" ActionTag="-1864370733" Tag="130" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="40.4600" RightMargin="-2.4600" TopMargin="24.3100" BottomMargin="-4.3100" FontSize="25" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="62.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="71.4600" Y="10.6900" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7146" Y="0.2138" />
                <PreSize X="0.6200" Y="0.6000" />
                <FontResource Type="Normal" Path="AvenirNextLTPro-Regular.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="uselessText_0" ActionTag="338299189" Tag="131" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="31.7900" RightMargin="24.2100" TopMargin="24.3100" BottomMargin="-4.3100" FontSize="25" LabelText="Get" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="44.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="31.7900" Y="10.6900" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3179" Y="0.2138" />
                <PreSize X="0.4400" Y="0.6000" />
                <FontResource Type="Normal" Path="AvenirNextLTPro-Regular.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="-273951728" Tag="132" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="89.1400" RightMargin="-33.1400" TopMargin="14.3769" BottomMargin="-4.3769" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.4625" />
                <Position X="89.1400" Y="14.1231" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8914" Y="0.2825" />
                <PreSize X="0.4400" Y="0.8000" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="31.9600" Y="66.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3400" Y="0.5000" />
            <PreSize X="1.0638" Y="0.3759" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="infoSpecial" ActionTag="-61233471" VisibleForFrame="False" Tag="137" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-18.0400" RightMargin="12.0400" TopMargin="41.5000" BottomMargin="41.5000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="100.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="label_0" ActionTag="-1886601048" Tag="138" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-32.9900" RightMargin="-127.0100" TopMargin="-15.2500" BottomMargin="5.2500" IsCustomSize="True" FontSize="32" LabelText="Ad free" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="260.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="97.0100" Y="35.2500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9701" Y="0.7050" />
                <PreSize X="2.6000" Y="1.2000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_0" ActionTag="1442791323" Tag="140" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="31.7900" RightMargin="-200.7900" TopMargin="24.3100" BottomMargin="-4.3100" FontSize="25" LabelText="Remove all ads in game" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="269.0000" Y="30.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="31.7900" Y="10.6900" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3179" Y="0.2138" />
                <PreSize X="2.6900" Y="0.6000" />
                <FontResource Type="Normal" Path="AvenirNextLTPro-Regular.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="31.9600" Y="66.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3400" Y="0.5000" />
            <PreSize X="1.0638" Y="0.3759" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="starStage" ActionTag="2132436498" VisibleForFrame="False" Tag="275" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-38.5000" RightMargin="-38.5000" TopMargin="7.0000" BottomMargin="28.0000" ctype="SpriteObjectData">
            <Size X="171.0000" Y="98.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="77.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5789" />
            <PreSize X="1.8191" Y="0.7368" />
            <FileData Type="Normal" Path="guiImage/ui_shop_stage_2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondStage" ActionTag="-1565136687" VisibleForFrame="False" Tag="276" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-38.5000" RightMargin="-38.5000" TopMargin="7.0000" BottomMargin="28.0000" ctype="SpriteObjectData">
            <Size X="171.0000" Y="98.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="77.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5789" />
            <PreSize X="1.8191" Y="0.7368" />
            <FileData Type="Normal" Path="guiImage/ui_shop_stage_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="-464076133" Tag="36" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-46.0000" RightMargin="-46.0000" TopMargin="-47.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
            <Size X="186.0000" Y="186.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="87.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6541" />
            <PreSize X="1.9787" Y="1.3985" />
            <FileData Type="Normal" Path="guiImage/ui_shop_item_01.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="purchaseBtn" ActionTag="1275082917" Tag="35" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-84.5000" RightMargin="-84.5000" TopMargin="5.0000" TouchEnable="True" FontSize="37" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="233" Scale9Height="106" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="263.0000" Y="128.0000" />
            <Children>
              <AbstractNodeData Name="IAPPrice" ActionTag="-1284425940" Tag="900" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="72.5000" RightMargin="72.5000" TopMargin="44.0000" BottomMargin="36.0000" FontSize="32" LabelText="$4.99" HorizontalAlignmentType="HT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="118.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="131.5000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4688" />
                <PreSize X="0.4487" Y="0.3750" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="18" G="151" B="72" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondPrice" Visible="False" ActionTag="-1059700739" Tag="901" IconVisible="False" LeftMargin="122.0000" RightMargin="83.0000" TopMargin="44.0000" BottomMargin="36.0000" FontSize="32" LabelText="50" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="58.0000" Y="48.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="122.0000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4639" Y="0.4688" />
                <PreSize X="0.2205" Y="0.3750" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="18" G="151" B="72" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamond" Visible="False" ActionTag="479876985" Tag="902" IconVisible="False" LeftMargin="60.0000" RightMargin="151.0000" TopMargin="40.5000" BottomMargin="42.5000" ctype="SpriteObjectData">
                <Size X="52.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="86.0000" Y="65.0000" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3270" Y="0.5078" />
                <PreSize X="0.1977" Y="0.3516" />
                <FileData Type="Normal" Path="guiImage/ui_general_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="47.0000" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="2.7979" Y="0.9624" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_settings_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="watchAdBtn" ActionTag="-1382071350" VisibleForFrame="False" Tag="796" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-84.5000" RightMargin="-84.5000" TopMargin="5.0000" TouchEnable="True" FontSize="37" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="233" Scale9Height="106" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="263.0000" Y="128.0000" />
            <Children>
              <AbstractNodeData Name="diamondPrice" ActionTag="-97383791" Tag="798" IconVisible="False" LeftMargin="86.0000" RightMargin="34.0000" TopMargin="49.5000" BottomMargin="41.5000" FontSize="24" LabelText="watch ad" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="143.0000" Y="37.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="86.0000" Y="60.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3270" Y="0.4688" />
                <PreSize X="0.5437" Y="0.2891" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="18" G="151" B="72" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamond" ActionTag="1156244086" Tag="799" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="27.0000" RightMargin="184.0000" TopMargin="45.3400" BottomMargin="37.6600" ctype="SpriteObjectData">
                <Size X="52.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="53.0000" Y="60.1600" />
                <Scale ScaleX="1.2000" ScaleY="1.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2015" Y="0.4700" />
                <PreSize X="0.1977" Y="0.3516" />
                <FileData Type="Normal" Path="guiImage/ui_general_video.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="47.0000" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="2.7979" Y="0.9624" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_settings_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="blueActionInfo" ActionTag="1188980496" VisibleForFrame="False" Tag="75" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-22.9170" RightMargin="-3.0830" TopMargin="43.7211" BottomMargin="39.2789" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="blueActionLabel" ActionTag="980147570" Tag="76" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-39.7360" RightMargin="-100.2640" TopMargin="-17.5650" BottomMargin="7.5650" IsCustomSize="True" FontSize="32" LabelText="Small" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="260.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.2640" Y="37.5650" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7522" Y="0.7513" />
                <PreSize X="2.1667" Y="1.2000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="content" ActionTag="817558701" Tag="78" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="53.0040" RightMargin="0.9960" TopMargin="20.0000" BottomMargin="-4.0000" FontSize="28" LabelText="1000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="66.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="53.0040" Y="13.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4417" Y="0.2600" />
                <PreSize X="0.5500" Y="0.6800" />
                <FontResource Type="Normal" Path="AvenirNextLTPro-Regular.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="uselessText" ActionTag="273511044" Tag="127" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="25.2000" RightMargin="44.8000" TopMargin="20.0000" BottomMargin="-4.0000" FontSize="28" LabelText="Get" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="50.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="25.2000" Y="13.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2100" Y="0.2600" />
                <PreSize X="0.4167" Y="0.6800" />
                <FontResource Type="Normal" Path="AvenirNextLTPro-Regular.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="-1065485415" Tag="128" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="90.6900" RightMargin="-14.6900" TopMargin="12.0650" BottomMargin="-2.0650" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleY="0.4625" />
                <Position X="90.6900" Y="16.4350" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7558" Y="0.3287" />
                <PreSize X="0.3667" Y="0.8000" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="37.0830" Y="64.2789" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3945" Y="0.4833" />
            <PreSize X="1.2766" Y="0.3759" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondAmount" ActionTag="-273658552" Tag="277" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="8.5000" RightMargin="8.5000" TopMargin="63.5000" BottomMargin="36.5000" FontSize="24" LabelText="1000" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="77.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="53.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3985" />
            <PreSize X="0.8191" Y="0.2481" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="73" G="64" B="208" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="starAmount" ActionTag="495401720" Tag="1084" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="8.5000" RightMargin="8.5000" TopMargin="63.5000" BottomMargin="36.5000" FontSize="24" LabelText="1000" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="77.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="53.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3985" />
            <PreSize X="0.8191" Y="0.2481" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="182" G="117" B="18" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="-93459266" Tag="1231" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="46.6616" RightMargin="47.3384" TopMargin="41.7620" BottomMargin="91.2380" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="46.6616" Y="91.2380" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4964" Y="0.6860" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle_shopeffect1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>