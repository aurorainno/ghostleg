<GameFile>
  <PropertyGroup Name="MailItemView" Type="Layer" ID="b0db490c-eae5-405e-a201-5bf26d2e7833" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="7" ctype="GameLayerObjectData">
        <Size X="290.0000" Y="65.0000" />
        <Children>
          <AbstractNodeData Name="bg" ActionTag="-1659920795" Tag="3" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" RightMargin="-290.0000" TopMargin="-65.0000" Scale9Enable="True" LeftEage="189" RightEage="189" TopEage="47" BottomEage="47" Scale9OriginX="189" Scale9OriginY="47" Scale9Width="223" Scale9Height="77" ctype="ImageViewObjectData">
            <Size X="580.0000" Y="130.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="2.0000" Y="2.0000" />
            <FileData Type="Normal" Path="guiImage/ui_inbox_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="icon" ActionTag="-416360421" Tag="4" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-9.0000" RightMargin="221.0000" TopMargin="-6.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
            <Size X="78.0000" Y="78.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="30.0000" Y="32.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1034" Y="0.5000" />
            <PreSize X="0.2690" Y="1.2000" />
            <FileData Type="Normal" Path="guiImage/ui_inbox_reward_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="title" ActionTag="-1236589696" Tag="5" IconVisible="False" LeftMargin="54.0000" RightMargin="-7.0000" TopMargin="3.3700" BottomMargin="33.6300" FontSize="20" LabelText="Rank #300 Reward" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="243.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="54.0000" Y="47.6300" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1862" Y="0.7328" />
            <PreSize X="0.8379" Y="0.4308" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="content" ActionTag="-16776030" Tag="6" IconVisible="False" LeftMargin="54.0000" RightMargin="-75.0000" TopMargin="21.6500" BottomMargin="13.3500" IsCustomSize="True" FontSize="14" LabelText="Week 40 - Training Camp" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="311.0000" Y="30.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="54.0000" Y="28.3500" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1862" Y="0.4362" />
            <PreSize X="1.0724" Y="0.4615" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="rewardPanel" ActionTag="-1160002662" Tag="1027" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="41.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="290.0000" Y="24.0000" />
            <Children>
              <AbstractNodeData Name="largeStarSprite" ActionTag="1779675031" VisibleForFrame="False" Tag="88" IconVisible="False" LeftMargin="41.0000" RightMargin="205.0000" TopMargin="-8.0000" BottomMargin="-8.0000" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="12.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2172" Y="0.5000" />
                <PreSize X="0.1517" Y="1.6667" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="largeDiamondSprite" ActionTag="-1476159947" VisibleForFrame="False" Tag="89" IconVisible="False" LeftMargin="37.0000" RightMargin="201.0000" TopMargin="-10.5000" BottomMargin="-10.5000" ctype="SpriteObjectData">
                <Size X="52.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="12.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2172" Y="0.5000" />
                <PreSize X="0.1793" Y="1.8750" />
                <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="charSprite" ActionTag="307875449" Tag="1028" IconVisible="False" LeftMargin="22.3052" RightMargin="186.6948" TopMargin="-29.5037" BottomMargin="-29.4963" ctype="SpriteObjectData">
                <Size X="81.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="62.8052" Y="12.0037" />
                <Scale ScaleX="0.1500" ScaleY="0.1500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2166" Y="0.5002" />
                <PreSize X="0.2793" Y="3.4583" />
                <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="1945555692" Tag="7" IconVisible="False" LeftMargin="37.0000" RightMargin="201.0000" TopMargin="-10.5000" BottomMargin="-10.5000" ctype="SpriteObjectData">
                <Size X="52.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="12.0000" />
                <Scale ScaleX="0.3000" ScaleY="0.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2172" Y="0.5000" />
                <PreSize X="0.1793" Y="1.8750" />
                <FileData Type="Normal" Path="guiImage/ui_general_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="940096678" Tag="8" IconVisible="False" LeftMargin="41.0000" RightMargin="205.0000" TopMargin="-8.0000" BottomMargin="-8.0000" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="12.0000" />
                <Scale ScaleX="0.3000" ScaleY="0.3000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2172" Y="0.5000" />
                <PreSize X="0.1517" Y="1.6667" />
                <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="amount" ActionTag="-356127998" Tag="9" IconVisible="False" LeftMargin="72.8889" RightMargin="182.1111" TopMargin="3.3722" BottomMargin="1.6278" FontSize="14" LabelText="100" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="35.0000" Y="19.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="72.8889" Y="11.1278" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="223" B="4" />
                <PrePosition X="0.2513" Y="0.4637" />
                <PreSize X="0.1207" Y="0.7917" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.3692" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="receiveBtn" ActionTag="-1559115606" Tag="51" RotationSkewY="0.0021" IconVisible="False" LeftMargin="138.9080" RightMargin="-52.9080" TopMargin="-17.7500" BottomMargin="-11.2500" TouchEnable="True" FontSize="18" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="204.0000" Y="94.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="-1075172002" Tag="52" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="59.0000" RightMargin="59.0000" TopMargin="35.3200" BottomMargin="29.6800" FontSize="18" LabelText="receive" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="86.0000" Y="29.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="102.0000" Y="44.1800" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4700" />
                <PreSize X="0.4216" Y="0.3085" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="18" G="151" B="72" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="240.9080" Y="35.7500" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8307" Y="0.5500" />
            <PreSize X="0.7034" Y="1.4462" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeText" ActionTag="-1860035138" Tag="53" IconVisible="False" LeftMargin="192.0800" RightMargin="-3.0800" TopMargin="44.5000" BottomMargin="1.5000" FontSize="14" LabelText="7 days ago" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="101.0000" Y="19.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="242.5800" Y="11.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8365" Y="0.1692" />
            <PreSize X="0.3483" Y="0.2923" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="doneBtn" Visible="False" ActionTag="-1526987365" Tag="55" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="151.0000" RightMargin="-41.0000" TopMargin="-8.2500" BottomMargin="-1.7500" TouchEnable="True" FontSize="18" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="75.0000" />
            <Children>
              <AbstractNodeData Name="title" ActionTag="756992711" Tag="56" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="21.5000" BottomMargin="21.5000" FontSize="20" LabelText="done" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="65.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="90.0000" Y="37.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="195" G="195" B="195" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.3611" Y="0.4267" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="60" G="60" B="60" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="241.0000" Y="35.7500" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8310" Y="0.5500" />
            <PreSize X="0.6207" Y="1.1538" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Normal" Path="guiImage/ui_inbox_btn_off.png" Plist="" />
            <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_off.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>