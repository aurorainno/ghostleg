<GameFile>
  <PropertyGroup Name="DogSuggestionLayer" Type="Layer" ID="d4bbb260-f30e-4c42-ae31-e3868d6c1bd4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="166" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-1637218900" VisibleForFrame="False" Tag="10" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" ComboBoxIndex="2" ColorAngle="245.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="255" />
            <FirstColor A="255" R="30" G="144" B="255" />
            <EndColor A="255" R="255" G="165" B="0" />
            <ColorVector ScaleX="-0.4226" ScaleY="-0.9063" />
          </AbstractNodeData>
          <AbstractNodeData Name="backdrop" ActionTag="-1693403227" VisibleForFrame="False" Alpha="76" Tag="72" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" ComboBoxIndex="1" ColorAngle="273.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="0" G="0" B="255" />
            <EndColor A="255" R="30" G="144" B="255" />
            <ColorVector ScaleX="0.0523" ScaleY="-0.9986" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="368078574" Tag="167" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="165.0224" BottomMargin="162.9776" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" LeftEage="211" RightEage="211" TopEage="374" BottomEage="374" Scale9OriginX="-211" Scale9OriginY="-374" Scale9Width="422" Scale9Height="748" ctype="PanelObjectData">
            <Size X="320.0000" Y="240.0000" />
            <Children>
              <AbstractNodeData Name="PanelBg" ActionTag="347939141" Tag="11" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="127" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="240.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogDetailNode" ActionTag="-1044783307" Tag="43" IconVisible="True" TopMargin="79.6600" BottomMargin="80.3400" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="320.0000" Y="80.0000" />
                <AnchorPoint />
                <Position Y="80.3400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.3347" />
                <PreSize X="1.0000" Y="0.3333" />
                <FileData Type="Normal" Path="DogSelectItemView.csd" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockButton" ActionTag="-472467567" Tag="40" IconVisible="False" LeftMargin="-4.8503" RightMargin="-13.1497" TopMargin="153.9330" BottomMargin="-2.9330" TouchEnable="True" FontSize="30" ButtonText="1000     to unlock" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="308" Scale9Height="67" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="338.0000" Y="89.0000" />
                <Children>
                  <AbstractNodeData Name="unlockTitle" ActionTag="422508680" Tag="42" IconVisible="False" LeftMargin="123.1036" RightMargin="180.8964" TopMargin="25.0659" BottomMargin="29.9341" ctype="SpriteObjectData">
                    <Size X="34.0000" Y="34.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="140.1036" Y="46.9341" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4145" Y="0.5273" />
                    <PreSize X="0.1006" Y="0.3820" />
                    <FileData Type="Normal" Path="star_icon.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="164.1497" Y="41.5670" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5130" Y="0.1732" />
                <PreSize X="1.0562" Y="0.3708" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/btn_paystar.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="info" ActionTag="-1584091326" Tag="173" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-140.4040" RightMargin="-140.5960" TopMargin="15.1440" BottomMargin="176.8560" FontSize="35" LabelText="Try this NEW dog and get more fun!" HorizontalAlignmentType="HT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="601.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0960" Y="200.8560" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5003" Y="0.8369" />
                <PreSize X="1.8781" Y="0.2000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="240" G="173" B="50" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeButton" ActionTag="-538656786" Tag="175" IconVisible="False" LeftMargin="249.0477" RightMargin="-17.0477" TopMargin="-46.2446" BottomMargin="192.2446" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="88.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="293.0477" Y="239.2446" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9158" Y="0.9969" />
                <PreSize X="0.2750" Y="0.3917" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="guiImage/btn_cancel.png" Plist="" />
                <PressedFileData Type="Normal" Path="guiImage/btn_cancel.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/btn_cancel.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="282.9776" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4982" />
            <PreSize X="1.0000" Y="0.4225" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="starValueNode" ActionTag="645476542" Tag="39" IconVisible="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" RightMargin="120.0000" BottomMargin="528.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="200.0000" Y="40.0000" />
            <AnchorPoint />
            <Position Y="528.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9296" />
            <PreSize X="0.6250" Y="0.0704" />
            <FileData Type="Normal" Path="StarValueLayer.csd" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>