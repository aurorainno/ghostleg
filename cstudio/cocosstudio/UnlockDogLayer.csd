<GameFile>
  <PropertyGroup Name="UnlockDogLayer" Type="Layer" ID="b32239f5-486e-48f8-b60d-4b9ab41bc7a7" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="166" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="ui_main_bg1_1" ActionTag="1528730316" Tag="129" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-284.0000" BottomMargin="-284.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="181" G="181" B="181" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.0000" />
            <FileData Type="Normal" Path="guiImage/ui_main_bg1.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="368078574" Tag="167" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="71.0000" BottomMargin="71.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" LeftEage="211" RightEage="211" TopEage="374" BottomEage="374" Scale9OriginX="-211" Scale9OriginY="-374" Scale9Width="422" Scale9Height="748" ctype="PanelObjectData">
            <Size X="320.0000" Y="426.0000" />
            <Children>
              <AbstractNodeData Name="bgPanel" ActionTag="-696740999" Alpha="0" Tag="85" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="426.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlock_aura_1" ActionTag="-1947309531" Alpha="148" Tag="184" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="63.0000" BottomMargin="63.0000" ctype="SpriteObjectData">
                <Size X="300.0000" Y="300.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="213.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.9375" Y="0.7042" />
                <FileData Type="Normal" Path="unlock_aura.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlock_aura_2" ActionTag="1056055977" Alpha="148" Tag="185" RotationSkewX="25.0000" RotationSkewY="25.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="63.0000" BottomMargin="63.0000" ctype="SpriteObjectData">
                <Size X="300.0000" Y="300.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="213.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.9375" Y="0.7042" />
                <FileData Type="Normal" Path="unlock_aura.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Particle_1" ActionTag="-218103932" Tag="186" IconVisible="True" LeftMargin="159.9145" RightMargin="160.0855" TopMargin="222.6524" BottomMargin="203.3476" ctype="ParticleObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="159.9145" Y="203.3476" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4997" Y="0.4773" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="particle/particle_stars.plist" Plist="" />
                <BlendFunc Src="770" Dst="1" />
              </AbstractNodeData>
              <AbstractNodeData Name="fireworkNode" ActionTag="-802119505" Tag="154" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="213.0000" BottomMargin="213.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="160.0000" Y="213.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="ui_result_unlock_1" ActionTag="-1179164119" Tag="175" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-50.3553" RightMargin="13.3553" TopMargin="-31.6013" BottomMargin="302.6013" ctype="SpriteObjectData">
                <Size X="357.0000" Y="155.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="128.1447" Y="380.1013" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4005" Y="0.8923" />
                <PreSize X="1.1156" Y="0.3638" />
                <FileData Type="Normal" Path="guiImage/ui_result_unlock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="charName" ActionTag="-1595985650" Tag="169" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-29.5000" RightMargin="-29.5000" TopMargin="270.4645" BottomMargin="70.5355" FontSize="54" LabelText="BLUE SPIRIT" HorizontalAlignmentType="HT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="379.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="113.0355" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2653" />
                <PreSize X="1.1844" Y="0.1995" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="134" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="charSprite" ActionTag="-1405689246" Tag="171" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="90.5056" RightMargin="85.4944" TopMargin="136.3488" BottomMargin="145.6512" ctype="SpriteObjectData">
                <Size X="144.0000" Y="144.0000" />
                <AnchorPoint ScaleX="0.4826" ScaleY="0.4677" />
                <Position X="160.0000" Y="213.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4500" Y="0.3380" />
                <FileData Type="Normal" Path="guiImage/dselect_dog1_unlock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="okBtn" ActionTag="885540701" Tag="131" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="8.5000" RightMargin="8.5000" TopMargin="354.6295" BottomMargin="-27.6295" TouchEnable="True" FontSize="30" ButtonText="OK" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="273" Scale9Height="77" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="303.0000" Y="99.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="21.8705" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0513" />
                <PreSize X="0.9469" Y="0.2324" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/ui_main_start_locked.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="0.7500" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>