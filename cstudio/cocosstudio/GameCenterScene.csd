<GameFile>
  <PropertyGroup Name="GameCenterScene" Type="Layer" ID="8b1776d1-3c87-4af3-8b36-4a798c700a75" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="39" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="425010622" Tag="40" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="166" ComboBoxIndex="1" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="SignInBtn" ActionTag="672945491" Tag="42" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="120.0000" RightMargin="120.0000" TopMargin="264.0000" BottomMargin="264.0000" TouchEnable="True" FontSize="14" ButtonText="Sign in" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="80.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.2500" Y="0.0704" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="backBtn" ActionTag="-10294989" Tag="50" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-48.8000" RightMargin="168.8000" TopMargin="-25.5117" BottomMargin="477.5117" TouchEnable="True" FontSize="36" ButtonText="back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="116.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.2000" Y="535.5117" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1600" Y="0.9428" />
            <PreSize X="0.6250" Y="0.2042" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="49" G="216" B="212" />
            <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>