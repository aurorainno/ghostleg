<GameFile>
  <PropertyGroup Name="DailyRewardDialog" Type="Layer" ID="58ba45e5-dd2b-4b3e-a477-a130e0c00788" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="56" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgPanel" ActionTag="-2055544845" Tag="65" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="1266803662" Tag="58" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-138.0000" RightMargin="-138.0000" TopMargin="14.5000" BottomMargin="14.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" LeftEage="196" RightEage="196" TopEage="177" BottomEage="177" Scale9OriginX="-196" Scale9OriginY="-177" Scale9Width="392" Scale9Height="354" ctype="PanelObjectData">
            <Size X="596.0000" Y="539.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1821219725" Tag="1398" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="-90.0000" BottomMargin="-101.0000" LeftEage="198" RightEage="198" TopEage="300" BottomEage="297" Scale9OriginX="198" Scale9OriginY="300" Scale9Width="167" Scale9Height="133" ctype="ImageViewObjectData">
                <Size X="563.0000" Y="730.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="298.0000" Y="264.0000" />
                <Scale ScaleX="1.0000" ScaleY="0.8200" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4898" />
                <PreSize X="0.9446" Y="1.3544" />
                <FileData Type="Normal" Path="guiImage/ui_character_statsinfo.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmButton" ActionTag="1833377824" VisibleForFrame="False" Tag="117" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="121.5000" RightMargin="121.5000" TopMargin="445.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="content" ActionTag="-1632880527" Tag="1406" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="52.5000" RightMargin="52.5000" TopMargin="34.5000" BottomMargin="34.5000" FontSize="18" LabelText="come back tommorow" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="248.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.7025" Y="0.2660" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="298.0000" Y="47.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0872" />
                <PreSize X="0.5923" Y="0.1744" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="collectButton" ActionTag="2126541172" Tag="59" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="121.5000" RightMargin="121.5000" TopMargin="445.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="content" ActionTag="1302502826" Tag="1407" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="118.5000" RightMargin="118.5000" TopMargin="30.5000" BottomMargin="30.5000" FontSize="24" LabelText="collect" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="116.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.3286" Y="0.3511" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="298.0000" Y="47.0000" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0872" />
                <PreSize X="0.5923" Y="0.1744" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="infoText" ActionTag="291392347" Tag="60" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="151.0000" RightMargin="151.0000" TopMargin="44.5000" BottomMargin="453.5000" FontSize="24" LabelText="daily login reward" HorizontalAlignmentType="HT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="294.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="298.0000" Y="474.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8794" />
                <PreSize X="0.4933" Y="0.0761" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="23" G="114" B="151" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="day1Panel" ActionTag="1458705059" Tag="61" IconVisible="False" LeftMargin="76.9799" RightMargin="419.0201" TopMargin="141.0000" BottomMargin="298.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="264763249" Tag="942" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="-362479191" Tag="885" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="-1029051916" Tag="884" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-19.5978" RightMargin="58.5978" TopMargin="-19.9168" BottomMargin="61.9168" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="1298286186" Tag="882" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="1" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="28.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.4590" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.9022" Y="90.9168" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1090" Y="0.9092" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="1211099401" Tag="69" IconVisible="False" LeftMargin="28.0000" RightMargin="28.0000" TopMargin="25.0000" BottomMargin="35.0000" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="55.0000" />
                    <Scale ScaleX="1.4000" ScaleY="1.4000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="0.4400" Y="0.4000" />
                    <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="1443870752" Tag="70" IconVisible="False" LeftMargin="-25.0000" RightMargin="-25.0000" TopMargin="61.0000" BottomMargin="-9.0000" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="15.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1500" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="-420882234" Tag="79" IconVisible="False" LeftMargin="46.3843" RightMargin="-18.3843" TopMargin="50.0643" BottomMargin="-22.0643" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="82.3843" Y="13.9357" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8238" Y="0.1394" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="76.9799" Y="298.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1292" Y="0.5529" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="day2Panel" ActionTag="1554279287" Tag="81" IconVisible="False" LeftMargin="192.9254" RightMargin="303.0746" TopMargin="141.0000" BottomMargin="298.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="1794226839" Tag="943" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.6100" BottomMargin="-42.3900" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.6100" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5061" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="1737629418" Tag="944" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="-383267905" Tag="945" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-20.2060" RightMargin="59.2060" TopMargin="-20.5237" BottomMargin="62.5237" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="-528556914" Tag="946" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="2" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="45.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.7377" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.2940" Y="91.5237" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1029" Y="0.9152" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="634676846" Tag="947" IconVisible="False" LeftMargin="8.8916" RightMargin="10.1084" TopMargin="2.8920" BottomMargin="14.1080" ctype="SpriteObjectData">
                    <Size X="81.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.3916" Y="55.6080" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4939" Y="0.5561" />
                    <PreSize X="0.8100" Y="0.8300" />
                    <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="-1060890925" Tag="948" IconVisible="False" LeftMargin="-25.6085" RightMargin="-24.3915" TopMargin="60.3924" BottomMargin="-8.3924" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.3915" Y="15.6076" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4939" Y="0.1561" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="1912389872" Tag="949" IconVisible="False" LeftMargin="45.7762" RightMargin="-17.7762" TopMargin="49.4563" BottomMargin="-21.4563" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="81.7762" Y="14.5437" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8178" Y="0.1454" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="192.9254" Y="298.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3237" Y="0.5529" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="day3Panel" ActionTag="-410539809" Tag="85" IconVisible="False" LeftMargin="308.8725" RightMargin="187.1275" TopMargin="141.0000" BottomMargin="298.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="-1311762878" Tag="950" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.6100" RightMargin="-42.3900" TopMargin="-43.4800" BottomMargin="-42.5200" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.3900" Y="50.4800" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4939" Y="0.5048" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="388857147" Tag="951" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.6100" RightMargin="-42.3900" TopMargin="-42.8700" BottomMargin="-43.1300" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.3900" Y="49.8700" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4939" Y="0.4987" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="422469209" Tag="952" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-20.8141" RightMargin="59.8141" TopMargin="-20.3952" BottomMargin="62.3952" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="1666178407" Tag="953" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.5000" RightMargin="10.5000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="3" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="40.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6557" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="9.6859" Y="91.3952" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0969" Y="0.9140" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="-1489378782" Tag="954" IconVisible="False" LeftMargin="8.2827" RightMargin="10.7173" TopMargin="3.0202" BottomMargin="13.9798" ctype="SpriteObjectData">
                    <Size X="81.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="48.7827" Y="55.4798" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4878" Y="0.5548" />
                    <PreSize X="0.8100" Y="0.8300" />
                    <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="1255271961" Tag="955" IconVisible="False" LeftMargin="-26.2170" RightMargin="-23.7830" TopMargin="60.5207" BottomMargin="-8.5207" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="48.7830" Y="15.4793" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4878" Y="0.1548" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="2044522184" Tag="956" IconVisible="False" LeftMargin="45.1681" RightMargin="-17.1681" TopMargin="49.5847" BottomMargin="-21.5847" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="81.1681" Y="14.4153" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8117" Y="0.1442" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="308.8725" Y="298.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5182" Y="0.5529" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="day4Panel" ActionTag="-1645972070" Tag="89" IconVisible="False" LeftMargin="424.8191" RightMargin="71.1809" TopMargin="141.0000" BottomMargin="298.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="-708650382" Tag="957" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.7400" RightMargin="-43.2600" TopMargin="-44.0900" BottomMargin="-41.9100" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.2600" Y="51.0900" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5026" Y="0.5109" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="-15916477" Tag="958" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.7400" RightMargin="-43.2600" TopMargin="-43.4800" BottomMargin="-42.5200" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.2600" Y="50.4800" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5026" Y="0.5048" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="-1049454829" Tag="959" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-19.9508" RightMargin="58.9508" TopMargin="-21.0026" BottomMargin="63.0026" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="1081580536" Tag="960" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.5000" RightMargin="9.5000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="4" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6885" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.5492" Y="92.0026" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1055" Y="0.9200" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="1810475183" Tag="961" IconVisible="False" LeftMargin="9.1464" RightMargin="9.8536" TopMargin="2.4124" BottomMargin="14.5876" ctype="SpriteObjectData">
                    <Size X="81.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.6464" Y="56.0876" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4965" Y="0.5609" />
                    <PreSize X="0.8100" Y="0.8300" />
                    <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="1988149169" Tag="962" IconVisible="False" LeftMargin="-25.3535" RightMargin="-24.6465" TopMargin="59.9135" BottomMargin="-7.9135" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.6465" Y="16.0865" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4965" Y="0.1609" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="1712725580" Tag="963" IconVisible="False" LeftMargin="46.0318" RightMargin="-18.0318" TopMargin="48.9771" BottomMargin="-20.9771" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="82.0318" Y="15.0229" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8203" Y="0.1502" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="424.8191" Y="298.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7128" Y="0.5529" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="day5Panel" ActionTag="-1851708920" Tag="93" IconVisible="False" LeftMargin="132.7998" RightMargin="363.2002" TopMargin="297.0000" BottomMargin="142.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="1974979673" Tag="964" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.6200" RightMargin="-43.3800" TopMargin="-42.4900" BottomMargin="-43.5100" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.3800" Y="49.4900" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5038" Y="0.4949" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="-1035746891" Tag="965" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.6200" RightMargin="-43.3800" TopMargin="-41.8800" BottomMargin="-44.1200" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.3800" Y="48.8800" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5038" Y="0.4888" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="-358426606" Tag="966" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-19.8240" RightMargin="58.8240" TopMargin="-19.4016" BottomMargin="61.4016" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="-1716568669" Tag="967" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.5000" RightMargin="9.5000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="5" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.6885" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.6760" Y="90.4016" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1068" Y="0.9040" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="-570662182" Tag="968" IconVisible="False" LeftMargin="9.2733" RightMargin="9.7267" TopMargin="4.0130" BottomMargin="12.9870" ctype="SpriteObjectData">
                    <Size X="81.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.7733" Y="54.4870" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4977" Y="0.5449" />
                    <PreSize X="0.8100" Y="0.8300" />
                    <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="-1692470897" Tag="969" IconVisible="False" LeftMargin="-25.6528" RightMargin="-24.3472" TopMargin="64.8913" BottomMargin="-12.8913" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5057" ScaleY="0.6407" />
                    <Position X="50.2002" Y="17.8627" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5020" Y="0.1786" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="1559349338" Tag="970" IconVisible="False" LeftMargin="46.1594" RightMargin="-18.1594" TopMargin="50.5778" BottomMargin="-22.5778" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="82.1594" Y="13.4222" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8216" Y="0.1342" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="132.7998" Y="142.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2228" Y="0.2635" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="day6Panel" ActionTag="-691088164" Tag="154" IconVisible="False" LeftMargin="251.5698" RightMargin="244.4302" TopMargin="297.0000" BottomMargin="142.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="114891994" Tag="971" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.2200" RightMargin="-42.7800" TopMargin="-41.6200" BottomMargin="-44.3800" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.7800" Y="48.6200" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4978" Y="0.4862" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="2028980469" Tag="972" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.2200" RightMargin="-42.7800" TopMargin="-41.0100" BottomMargin="-44.9900" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.7800" Y="48.0100" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4978" Y="0.4801" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="1089336128" Tag="973" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-20.4316" RightMargin="59.4316" TopMargin="-18.5379" BottomMargin="60.5379" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="1503304013" Tag="974" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.0000" RightMargin="9.0000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="6" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="43.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.7049" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.0684" Y="89.5379" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1007" Y="0.8954" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="-1495283441" Tag="975" IconVisible="False" LeftMargin="8.6656" RightMargin="10.3344" TopMargin="4.8767" BottomMargin="12.1233" ctype="SpriteObjectData">
                    <Size X="81.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.1656" Y="53.6233" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4917" Y="0.5362" />
                    <PreSize X="0.8100" Y="0.8300" />
                    <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="1771443570" Tag="976" IconVisible="False" LeftMargin="-25.8336" RightMargin="-24.1664" TopMargin="62.3779" BottomMargin="-10.3779" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="49.1664" Y="13.6221" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4917" Y="0.1362" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="-235152472" Tag="977" IconVisible="False" LeftMargin="45.5516" RightMargin="-17.5516" TopMargin="51.4413" BottomMargin="-23.4413" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="81.5516" Y="12.5587" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8155" Y="0.1256" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="251.5698" Y="142.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4221" Y="0.2635" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="day7Panel" ActionTag="-954898370" Tag="158" IconVisible="False" LeftMargin="368.9998" RightMargin="127.0002" TopMargin="297.0000" BottomMargin="142.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="-1724273781" Tag="985" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.3600" RightMargin="-43.6400" TopMargin="-41.4900" BottomMargin="-44.5100" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.6400" Y="48.4900" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5064" Y="0.4849" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedBG" ActionTag="-1111159313" Tag="986" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.3600" RightMargin="-43.6400" TopMargin="-40.8900" BottomMargin="-45.1100" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.6400" Y="47.8900" />
                    <Scale ScaleX="0.5500" ScaleY="0.5500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5064" Y="0.4789" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="daySprite" ActionTag="2056808281" Tag="987" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-19.5675" RightMargin="58.5675" TopMargin="-18.4100" BottomMargin="60.4100" ctype="SpriteObjectData">
                    <Size X="61.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="dayText" ActionTag="989019426" Tag="988" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.5000" RightMargin="12.5000" TopMargin="-5.5000" BottomMargin="-5.5000" FontSize="50" LabelText="7" ShadowOffsetX="5.0000" ShadowOffsetY="-5.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="36.0000" Y="69.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="30.5000" Y="29.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="0.5902" Y="1.1897" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="26" G="26" B="26" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="10.9325" Y="89.4100" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1093" Y="0.8941" />
                    <PreSize X="0.6100" Y="0.5800" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="rewardSprite" ActionTag="2033870031" Tag="989" IconVisible="False" LeftMargin="9.5299" RightMargin="9.4701" TopMargin="5.0047" BottomMargin="11.9953" ctype="SpriteObjectData">
                    <Size X="81.0000" Y="83.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0299" Y="53.4953" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5003" Y="0.5350" />
                    <PreSize X="0.8100" Y="0.8300" />
                    <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="countText" ActionTag="1866592694" Tag="990" IconVisible="False" LeftMargin="-24.9695" RightMargin="-25.0305" TopMargin="62.5058" BottomMargin="-10.5058" IsCustomSize="True" FontSize="30" LabelText="2000" HorizontalAlignmentType="HT_Center" ShadowOffsetX="3.0000" ShadowOffsetY="-3.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="150.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0305" Y="13.4942" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5003" Y="0.1349" />
                    <PreSize X="1.5000" Y="0.4800" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="tickSprite" ActionTag="-2007528998" Tag="991" IconVisible="False" LeftMargin="46.4157" RightMargin="-18.4157" TopMargin="51.5696" BottomMargin="-23.5696" ctype="SpriteObjectData">
                    <Size X="72.0000" Y="72.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="82.4157" Y="12.4304" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8242" Y="0.1243" />
                    <PreSize X="0.7200" Y="0.7200" />
                    <FileData Type="Normal" Path="guiImage/ui_dailylogin_tick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="368.9998" Y="142.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6191" Y="0.2635" />
                <PreSize X="0.1678" Y="0.1855" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.8625" Y="0.9489" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>