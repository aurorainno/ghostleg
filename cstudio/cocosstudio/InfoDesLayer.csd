<GameFile>
  <PropertyGroup Name="InfoDesLayer" Type="Layer" ID="8adadcb0-a03a-419a-8a81-3bff0e74df7c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1110" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-661166137" Tag="1111" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="-386034818" Tag="1112" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-121.5000" RightMargin="-121.5000" TopMargin="-81.0000" BottomMargin="-81.0000" ctype="SpriteObjectData">
            <Size X="563.0000" Y="730.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.7594" Y="1.2852" />
            <FileData Type="Normal" Path="guiImage/ui_character_statsinfo.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-2109743842" Tag="1113" IconVisible="False" LeftMargin="48.0000" RightMargin="112.0000" TopMargin="142.0125" BottomMargin="392.9875" FontSize="24" LabelText="max speed" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="160.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="48.0000" Y="409.4875" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="221" B="61" />
            <PrePosition X="0.1500" Y="0.7209" />
            <PreSize X="0.5000" Y="0.0581" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="maxSpeedText" ActionTag="-1122638192" Tag="1114" IconVisible="False" LeftMargin="48.0000" RightMargin="-126.0000" TopMargin="160.0000" BottomMargin="358.0000" FontSize="18" LabelText="This is how fast your monster can&#xA;move along the lines" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="398.0000" Y="50.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="48.0000" Y="383.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1500" Y="0.6743" />
            <PreSize X="1.2437" Y="0.0880" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0" ActionTag="-1823032440" Tag="1115" IconVisible="False" LeftMargin="47.3020" RightMargin="71.6980" TopMargin="213.2261" BottomMargin="321.7739" FontSize="24" LabelText="acceleration" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="201.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="47.3020" Y="338.2739" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="0" G="255" B="210" />
            <PrePosition X="0.1478" Y="0.5956" />
            <PreSize X="0.6281" Y="0.0581" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="acceText" ActionTag="-2096161396" Tag="1116" IconVisible="False" LeftMargin="47.3020" RightMargin="-176.3020" TopMargin="231.2136" BottomMargin="286.7864" FontSize="18" LabelText="That is the time needed by your &#xA;monster to reach their maximum speed" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="449.0000" Y="50.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="47.3020" Y="311.7864" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1478" Y="0.5489" />
            <PreSize X="1.4031" Y="0.0880" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0_0" ActionTag="2019374970" Tag="1117" IconVisible="False" LeftMargin="46.6043" RightMargin="98.3957" TopMargin="287.4115" BottomMargin="247.5885" FontSize="24" LabelText="slide speed" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="175.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="46.6043" Y="264.0885" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="170" G="244" B="22" />
            <PrePosition X="0.1456" Y="0.4649" />
            <PreSize X="0.5469" Y="0.0581" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="slideText" ActionTag="1201189789" Tag="1118" IconVisible="False" LeftMargin="46.6043" RightMargin="-134.6043" TopMargin="305.3992" BottomMargin="212.6008" FontSize="18" LabelText="Here is the speed of your monsters&#xA;within the line you draw" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="408.0000" Y="50.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="46.6043" Y="237.6008" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1456" Y="0.4183" />
            <PreSize X="1.2750" Y="0.0880" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_0_0_0" ActionTag="-1463368278" Tag="1119" IconVisible="False" LeftMargin="46.5009" RightMargin="131.4991" TopMargin="362.7857" BottomMargin="172.2143" FontSize="24" LabelText="recovery" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="142.0000" Y="33.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="46.5009" Y="188.7143" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="104" B="232" />
            <PrePosition X="0.1453" Y="0.3322" />
            <PreSize X="0.4437" Y="0.0581" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="recoveryText" ActionTag="2107919232" Tag="1120" IconVisible="False" LeftMargin="48.5009" RightMargin="-89.5009" TopMargin="387.2738" BottomMargin="155.7262" FontSize="18" LabelText="Restore time after a knock out" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="361.0000" Y="25.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="48.5009" Y="168.2262" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1516" Y="0.2962" />
            <PreSize X="1.1281" Y="0.0440" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancelBtn" ActionTag="320112119" Tag="1121" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="254.4287" RightMargin="0.5713" TopMargin="72.2353" BottomMargin="432.7647" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="35" Scale9Height="41" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="65.0000" Y="63.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="286.9287" Y="464.2647" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8967" Y="0.8174" />
            <PreSize X="0.2031" Y="0.1109" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_cancel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>