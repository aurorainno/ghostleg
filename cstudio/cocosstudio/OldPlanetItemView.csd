<GameFile>
  <PropertyGroup Name="PlanetItemView_0" Type="Layer" ID="6faf6008-73bc-41f0-8ef8-b1b243e23eca" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="119" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="mainPanel" ActionTag="-1561300882" Tag="22" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="159.0000" BottomMargin="159.0000" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="250.0000" />
            <Children>
              <AbstractNodeData Name="planetNode" ActionTag="547278234" Tag="255" IconVisible="True" PositionPercentYEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="12.5000" BottomMargin="237.5000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="referenceNode" ActionTag="1154758733" Tag="156" IconVisible="True" LeftMargin="-160.0000" RightMargin="160.0000" TopMargin="160.0000" BottomMargin="-160.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="-160.0000" Y="-160.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="160.0000" Y="237.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9500" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetSprite" ActionTag="477476439" VisibleForFrame="False" Tag="193" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="137.0000" RightMargin="137.0000" TopMargin="102.0000" BottomMargin="102.0000" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="125.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.1437" Y="0.1840" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockPanel" ActionTag="-1427740604" VisibleForFrame="False" Tag="50" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="140.0000" BottomMargin="50.0000" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Enable="True" LeftEage="87" RightEage="87" TopEage="43" BottomEage="43" Scale9OriginX="87" Scale9OriginY="43" Scale9Width="92" Scale9Height="46" ctype="PanelObjectData">
                <Size X="200.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="infoText" ActionTag="1181676775" Tag="196" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-100.0000" RightMargin="-100.0000" TopMargin="-30.0000" BottomMargin="-30.0000" IsCustomSize="True" FontSize="26" LabelText="Collect 315 more star in a game to unlock Border Terrier and Jupiter" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="400.0000" Y="120.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="30.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="2.0000" Y="2.0000" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="160.0000" Y="110.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4400" />
                <PreSize X="0.6250" Y="0.2400" />
                <FileData Type="Normal" Path="guiImage/planet_board.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="lockPanel" Visible="False" ActionTag="-1868945725" VisibleForFrame="False" Tag="51" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="40.0000" RightMargin="40.0000" TopMargin="140.0000" BottomMargin="50.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="122" Scale9OriginY="37" Scale9Width="126" Scale9Height="40" ctype="PanelObjectData">
                <Size X="240.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="infoText" ActionTag="-2003471628" Tag="52" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-105.0000" RightMargin="-105.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="25" LabelText="Complete the SATURN team to unlock&#xA;0/5 members" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="450.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="120.0000" Y="30.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8750" Y="1.0667" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="planet_locked_7" ActionTag="1769066244" VisibleForFrame="False" Tag="54" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="64.0000" RightMargin="64.0000" TopMargin="-14.0000" BottomMargin="46.0000" ctype="SpriteObjectData">
                    <Size X="112.0000" Y="28.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_5" ActionTag="-1520162186" Tag="55" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.7516" RightMargin="-12.2484" TopMargin="-8.0004" BottomMargin="-9.9996" FontSize="36" LabelText="Locked" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="125.0000" Y="46.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="61.7484" Y="13.0004" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="165" B="0" />
                        <PrePosition X="0.5513" Y="0.4643" />
                        <PreSize X="1.1161" Y="1.6429" />
                        <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="120.0000" Y="60.0000" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.0000" />
                    <PreSize X="0.4667" Y="0.4667" />
                    <FileData Type="Normal" Path="guiImage/planet_locked.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="160.0000" Y="110.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4400" />
                <PreSize X="0.7500" Y="0.2400" />
                <FileData Type="Normal" Path="guiImage/planet_board_locked.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="nameSprite" ActionTag="1502789494" VisibleForFrame="False" Tag="195" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="38.0000" RightMargin="38.0000" TopMargin="-38.0000" BottomMargin="112.0000" ctype="SpriteObjectData">
                <Size X="244.0000" Y="176.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="200.0000" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8000" />
                <PreSize X="0.7625" Y="0.7040" />
                <FileData Type="Normal" Path="guiImage/planet_name.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="0.4401" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="touchPanel" ActionTag="728863731" Tag="13" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="120.0000" RightMargin="120.0000" TopMargin="140.4000" BottomMargin="367.6000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="80.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="397.6000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7000" />
            <PreSize X="0.2500" Y="0.1056" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>