<GameFile>
  <PropertyGroup Name="LeaderboardItemView" Type="Layer" ID="66b16600-6ac1-4a00-a22b-e75f74ac8e65" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="109" ctype="GameLayerObjectData">
        <Size X="295.0000" Y="40.0000" />
        <Children>
          <AbstractNodeData Name="bgNormal" ActionTag="596568708" Tag="110" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-146.4970" RightMargin="-146.4970" TopMargin="-22.0000" BottomMargin="-22.0000" Scale9Enable="True" LeftEage="194" RightEage="194" TopEage="27" BottomEage="27" Scale9OriginX="194" Scale9OriginY="27" Scale9Width="200" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="587.9940" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="147.5000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.9932" Y="2.1000" />
            <FileData Type="Normal" Path="guiImage/ui_main_rankingtab_p1.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="bgPlayer" Visible="False" ActionTag="-936266253" Tag="183" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-146.4970" RightMargin="-146.4970" TopMargin="-22.0000" BottomMargin="-22.0000" Scale9Enable="True" LeftEage="194" RightEage="194" TopEage="27" BottomEage="27" Scale9OriginX="194" Scale9OriginY="27" Scale9Width="200" Scale9Height="30" ctype="ImageViewObjectData">
            <Size X="587.9940" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="147.5000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.9932" Y="2.1000" />
            <FileData Type="Normal" Path="guiImage/ui_main_rankingtab_p2.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="rankSprite" ActionTag="-978240622" Tag="111" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="-4.0000" RightMargin="251.0000" TopMargin="-4.0000" BottomMargin="-4.0000" ctype="SpriteObjectData">
            <Size X="48.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="20.0000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0678" Y="0.5000" />
            <PreSize X="0.1627" Y="1.2000" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_badge.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="rankText" ActionTag="1137248335" Tag="112" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="11.0000" RightMargin="266.0000" TopMargin="8.7000" BottomMargin="6.3000" FontSize="18" LabelText="6" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="18.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="20.0000" Y="18.8000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="60" G="60" B="60" />
            <PrePosition X="0.0678" Y="0.4700" />
            <PreSize X="0.0610" Y="0.6250" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="profilePicFrame" ActionTag="-1874322345" Tag="113" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="42.0000" RightMargin="201.0000" TopMargin="-6.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
            <Size X="52.0000" Y="52.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="42.0000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1424" Y="0.5000" />
            <PreSize X="0.1763" Y="1.3000" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="countrySprite" ActionTag="-1018245988" Tag="114" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="43.0000" RightMargin="202.0000" TopMargin="5.5065" BottomMargin="-15.5065" ctype="SpriteObjectData">
            <Size X="50.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="68.0000" Y="9.4935" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2305" Y="0.2373" />
            <PreSize X="0.1695" Y="1.2500" />
            <FileData Type="Normal" Path="guiImage/AD.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="nameText" ActionTag="-939484270" Tag="115" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="79.0000" RightMargin="-14.0000" TopMargin="-10.0000" BottomMargin="-10.0000" IsCustomSize="True" FontSize="22" LabelText="Ken aaa bbb ccc ddd eee fff" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="230.0000" Y="60.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="79.0000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2678" Y="0.5000" />
            <PreSize X="0.7797" Y="1.5000" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_4" ActionTag="1808536260" Tag="116" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="158.0000" RightMargin="-17.0000" ctype="SpriteObjectData">
            <Size X="154.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="235.0000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7966" Y="0.5000" />
            <PreSize X="0.5220" Y="1.0000" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_scorelabel.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="scoreText" ActionTag="487076684" Tag="117" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="196.0000" RightMargin="27.0000" TopMargin="5.8000" BottomMargin="4.2000" FontSize="22" LabelText="1200" HorizontalAlignmentType="HT_Right" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="72.0000" Y="30.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="268.0000" Y="19.2000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="19" G="64" B="146" />
            <PrePosition X="0.9085" Y="0.4800" />
            <PreSize X="0.2441" Y="0.7500" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="rankUpSprite" ActionTag="1776175821" Tag="32" IconVisible="False" HorizontalEdge="RightEdge" LeftMargin="193.4121" RightMargin="71.5879" TopMargin="4.5883" BottomMargin="5.4117" ctype="SpriteObjectData">
            <Size X="30.0000" Y="30.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="208.4121" Y="20.4117" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7065" Y="0.5103" />
            <PreSize X="0.1017" Y="0.7500" />
            <FileData Type="Normal" Path="guiImage/ui_result_ranking_up.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>