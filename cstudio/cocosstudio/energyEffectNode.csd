<GameFile>
  <PropertyGroup Name="energyEffectNode" Type="Node" ID="d5abc22d-3c6f-4c18-b485-82d79efd822e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Node" Tag="102" ctype="GameNodeObjectData">
        <Size X="0.0000" Y="0.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1452691043" Tag="103" IconVisible="False" LeftMargin="-23.5000" RightMargin="2.5000" TopMargin="-9.0000" BottomMargin="-9.0000" ctype="SpriteObjectData">
            <Size X="21.0000" Y="18.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="-13.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="guiImage/ui_energy_spend.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="energyText" ActionTag="1775421898" Tag="104" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.2500" RightMargin="-44.7500" TopMargin="-22.5000" BottomMargin="-22.5000" FontSize="40" LabelText="10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="45.0000" Y="45.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="-0.2500" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>