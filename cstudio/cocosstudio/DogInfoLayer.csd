<GameFile>
  <PropertyGroup Name="DogInfoLayer" Type="Layer" ID="b79e71e7-581b-47c5-93b1-730443d75ab8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="296" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="totalCoinText" ActionTag="-958446639" Tag="297" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="35.2974" RightMargin="184.7026" TopMargin="5.4521" BottomMargin="522.5479" IsCustomSize="True" FontSize="35" LabelText="555" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="35.2974" Y="542.5479" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition X="0.1103" Y="0.9552" />
            <PreSize X="0.3125" Y="0.0704" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="moneyIcon" ActionTag="1213777745" Tag="298" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-1.6964" RightMargin="279.6964" TopMargin="1.8750" BottomMargin="524.1250" ctype="SpriteObjectData">
            <Size X="42.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="22.0000" Y="543.5500" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0688" Y="0.9570" />
            <PreSize X="0.1312" Y="0.0739" />
            <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="activePanel" ActionTag="719012103" Tag="299" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" LeftMargin="-0.8320" RightMargin="0.8320" TopMargin="42.5001" BottomMargin="288.4999" TouchEnable="True" ClipAble="False" BackColorAlpha="63" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="237.0000" />
            <Children>
              <AbstractNodeData Name="skillInfo" ActionTag="-413165694" Tag="303" IconVisible="False" LeftMargin="74.8260" RightMargin="-209.8260" TopMargin="163.5000" BottomMargin="-6.5000" IsCustomSize="True" FontSize="26" LabelText="Can dd&#xA;llll" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="455.0000" Y="80.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="74.8260" Y="33.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2338" Y="0.1414" />
                <PreSize X="1.4219" Y="0.3376" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillBG" ActionTag="314333354" Tag="220" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-46.7600" BottomMargin="15.7600" ctype="SpriteObjectData">
                <Size X="640.0000" Y="268.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="149.7600" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6319" />
                <PreSize X="2.0000" Y="1.1308" />
                <FileData Type="Normal" Path="guiImage/ability_shield.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_4" ActionTag="793123074" Tag="221" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="115.5000" BottomMargin="20.5000" ctype="SpriteObjectData">
                <Size X="640.0000" Y="101.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="71.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2996" />
                <PreSize X="2.0000" Y="0.4262" />
                <FileData Type="Normal" Path="guiImage/ability_banner.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillName" ActionTag="-1532600081" Tag="219" IconVisible="False" LeftMargin="76.6700" RightMargin="144.3300" TopMargin="148.5000" BottomMargin="53.5000" ctype="SpriteObjectData">
                <Size X="99.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="76.6700" Y="71.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2396" Y="0.2996" />
                <PreSize X="0.3094" Y="0.1477" />
                <FileData Type="Normal" Path="guiImage/ability_name_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillIcon" ActionTag="-698819359" Tag="300" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-34.2320" RightMargin="204.2320" TopMargin="91.1397" BottomMargin="-4.1397" ctype="SpriteObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="40.7680" Y="70.8603" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1274" Y="0.2990" />
                <PreSize X="0.4688" Y="0.6329" />
                <FileData Type="Normal" Path="guiImage/suit_skill_05.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="159.1680" Y="406.9999" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4974" Y="0.7165" />
            <PreSize X="1.0000" Y="0.4173" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancelBtn" ActionTag="805449712" Tag="304" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="236.1758" RightMargin="-4.1758" TopMargin="-4.0600" BottomMargin="478.0600" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="88.0000" Y="94.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="280.1758" Y="525.0600" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8755" Y="0.9244" />
            <PreSize X="0.2750" Y="0.1655" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/btn_cancel.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="passivePanel" ActionTag="-80244177" Tag="318" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="289.2600" BottomMargin="38.7400" TouchEnable="True" ClipAble="False" BackColorAlpha="63" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="240.0000" />
            <Children>
              <AbstractNodeData Name="skillInfo" ActionTag="1358841634" Tag="222" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="73.1840" RightMargin="-208.1840" TopMargin="123.3411" BottomMargin="36.6589" IsCustomSize="True" FontSize="26" LabelText="Get 200% more energy per second" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="455.0000" Y="80.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="73.1840" Y="76.6589" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2287" Y="0.3194" />
                <PreSize X="1.4219" Y="0.3333" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillBG" ActionTag="-633728222" Tag="223" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-26.0300" BottomMargin="82.0300" ctype="SpriteObjectData">
                <Size X="640.0000" Y="184.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="174.0300" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7251" />
                <PreSize X="2.0000" Y="0.7667" />
                <FileData Type="Normal" Path="guiImage/passive_storage.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_4_0" ActionTag="-1124540323" Tag="224" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="73.3400" BottomMargin="65.6600" ctype="SpriteObjectData">
                <Size X="640.0000" Y="101.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="116.1600" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4840" />
                <PreSize X="2.0000" Y="0.4208" />
                <FileData Type="Normal" Path="guiImage/passive_banner.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillName" ActionTag="-1571295803" Tag="225" IconVisible="False" LeftMargin="75.9900" RightMargin="145.0100" TopMargin="106.4200" BottomMargin="98.5800" ctype="SpriteObjectData">
                <Size X="99.0000" Y="35.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="75.9900" Y="116.0800" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2375" Y="0.4837" />
                <PreSize X="0.3094" Y="0.1458" />
                <FileData Type="Normal" Path="guiImage/ability_name_shield.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="skillIcon" ActionTag="1388255251" Tag="226" IconVisible="False" LeftMargin="3.3900" RightMargin="241.6100" TopMargin="86.5600" BottomMargin="78.4400" ctype="SpriteObjectData">
                <Size X="75.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="40.8900" Y="115.9400" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1278" Y="0.4831" />
                <PreSize X="0.2344" Y="0.3125" />
                <FileData Type="Normal" Path="guiImage/passive_icon_doublestar.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="selectBtn" ActionTag="939838247" VisibleForFrame="False" Tag="170" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="97.0000" RightMargin="97.0000" TopMargin="175.5000" BottomMargin="-10.5000" TouchEnable="True" FontSize="30" ButtonText="Select" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1125" />
                <PreSize X="0.3938" Y="0.3125" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/btn_dog_select_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="InUseBtn" ActionTag="-1734748939" VisibleForFrame="False" Tag="169" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="97.0000" RightMargin="97.0000" TopMargin="175.5000" BottomMargin="-10.5000" TouchEnable="True" FontSize="30" ButtonText="In-Use" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1125" />
                <PreSize X="0.3938" Y="0.3125" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <DisabledFileData Type="Normal" Path="guiImage/btn_dog_inuse.png" Plist="" />
                <PressedFileData Type="Normal" Path="guiImage/btn_dog_inuse.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/btn_dog_inuse.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockBtn" ActionTag="-1039590789" Tag="355" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-15.0000" RightMargin="-15.0000" TopMargin="175.5000" BottomMargin="-10.5000" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="350.0000" Y="75.0000" />
                <Children>
                  <AbstractNodeData Name="buttonMoneyIcon" ActionTag="1078596690" Tag="356" IconVisible="False" LeftMargin="122.6391" RightMargin="185.3609" TopMargin="3.8649" BottomMargin="29.1351" ctype="SpriteObjectData">
                    <Size X="42.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="143.6391" Y="50.1351" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4104" Y="0.6685" />
                    <PreSize X="0.1200" Y="0.5600" />
                    <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="cost" ActionTag="-324937136" Tag="357" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="140.8500" RightMargin="112.1500" TopMargin="-3.9194" BottomMargin="15.9194" FontSize="50" LabelText="400" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="97.0000" Y="63.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="189.3500" Y="47.4194" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5410" Y="0.6323" />
                    <PreSize X="0.2771" Y="0.8400" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_3" ActionTag="-783402495" Tag="358" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="95.0000" RightMargin="95.0000" TopMargin="28.9292" BottomMargin="2.0708" FontSize="35" LabelText="to unlock" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="160.0000" Y="44.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="175.0000" Y="24.0708" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3209" />
                    <PreSize X="0.4571" Y="0.5867" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1125" />
                <PreSize X="1.0938" Y="0.3125" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/btn_dog_finalpay.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="278.7400" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4907" />
            <PreSize X="1.0000" Y="0.4225" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>