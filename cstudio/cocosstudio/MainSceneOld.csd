<GameFile>
  <PropertyGroup Name="MainSceneOld" Type="Scene" ID="a2ee0952-26b5-49ae-8bf9-4f1d6279b798" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" ctype="GameNodeObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="titlePanel" ActionTag="-376892105" Tag="151" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="80.0000" BottomMargin="388.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_1" ActionTag="997645682" Tag="44" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-218.0000" RightMargin="-218.0000" TopMargin="-28.9738" BottomMargin="-9.0262" ctype="SpriteObjectData">
                <Size X="756.0000" Y="138.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="59.9738" />
                <Scale ScaleX="0.3500" ScaleY="0.3500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5997" />
                <PreSize X="2.3625" Y="1.3800" />
                <FileData Type="Normal" Path="ui_title.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_1" ActionTag="307029707" Tag="296" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="72.0000" ClipAble="False" BackColorAlpha="86" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="260.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" />
                <PreSize X="0.8125" Y="0.2800" />
                <SingleColor A="255" R="49" G="216" B="212" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bestScoreText" ActionTag="-1558669386" Tag="397" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="15.0000" RightMargin="15.0000" TopMargin="68.5000" BottomMargin="-3.5000" FontSize="28" LabelText="best distance 1000m" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="290.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="14.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.5000" Y="0.1400" />
                <PreSize X="0.9063" Y="0.3500" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position Y="388.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.6831" />
            <PreSize X="1.0000" Y="0.1761" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="centerPanel" ActionTag="-430333801" Tag="41" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="184.7952" BottomMargin="183.2048" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_14" ActionTag="-231180667" Tag="138" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="11.0000" RightMargin="5.0000" TopMargin="95.9200" BottomMargin="63.0800" FontSize="32" LabelText="tap to start" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="184.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.0000" Y="83.5800" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.5150" Y="0.4179" />
                <PreSize X="0.9200" Y="0.2050" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Image_3" ActionTag="-1257717177" Tag="153" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="19.0000" RightMargin="21.0000" TopMargin="-44.0000" BottomMargin="-4.0000" LeftEage="18" RightEage="18" TopEage="18" BottomEage="18" Scale9OriginX="18" Scale9OriginY="18" Scale9Width="124" Scale9Height="212" ctype="ImageViewObjectData">
                <Size X="160.0000" Y="248.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="99.0000" Y="120.0000" />
                <Scale ScaleX="0.1875" ScaleY="0.1875" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4950" Y="0.6000" />
                <PreSize X="0.8000" Y="1.2400" />
                <FileData Type="PlistSubImage" Path="btn_tap_to_start.png" Plist="gui.plist" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="283.2048" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.4986" />
            <PreSize X="0.6250" Y="0.3521" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="slopeModeButton" ActionTag="-638908203" Tag="22" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="10.4471" RightMargin="269.5529" TopMargin="9.5023" BottomMargin="518.4977" TouchEnable="True" FontSize="25" ButtonText="/" Scale9Enable="True" LeftEage="20" RightEage="20" TopEage="20" BottomEage="20" Scale9OriginX="20" Scale9OriginY="20" Scale9Width="60" Scale9Height="60" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="40.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5200" ScaleY="0.4764" />
            <Position X="31.2471" Y="537.5537" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0976" Y="0.9464" />
            <PreSize X="0.1250" Y="0.0704" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Normal" Path="roundRect.png" Plist="" />
            <PressedFileData Type="Normal" Path="roundRect.png" Plist="" />
            <NormalFileData Type="Normal" Path="roundRect.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="versionText" ActionTag="1199703351" Tag="27" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="538.0000" BottomMargin="10.0000" IsCustomSize="True" FontSize="20" LabelText="1.0.0 (debug)" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Bottom" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="300.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="160.0000" Y="10.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.0176" />
            <PreSize X="0.9375" Y="0.0352" />
            <FontResource Type="Normal" Path="Arial Rounded Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="-1921410590" Tag="38" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="168.1259" RightMargin="43.8741" TopMargin="-29.5800" BottomMargin="489.5800" ctype="SpriteObjectData">
            <Size X="108.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="229.0595" Y="539.5300" />
            <Scale ScaleX="0.1500" ScaleY="0.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7158" Y="0.9499" />
            <PreSize X="0.3375" Y="0.1901" />
            <FileData Type="Normal" Path="star_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="799579910" Tag="39" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="211.2000" RightMargin="8.8000" TopMargin="11.5700" BottomMargin="522.4300" IsCustomSize="True" FontSize="30" LabelText="99999&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;&#xA;" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="261.2000" Y="539.4300" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.8163" Y="0.9497" />
            <PreSize X="0.3125" Y="0.0599" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="addStarBtn" ActionTag="-1652005995" Tag="141" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="255.8697" RightMargin="-23.8697" TopMargin="-15.3728" BottomMargin="495.3728" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="66" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="88.0000" Y="88.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="299.8697" Y="539.3728" />
            <Scale ScaleX="0.2800" ScaleY="0.2800" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9371" Y="0.9496" />
            <PreSize X="0.2750" Y="0.1549" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn_addStar.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="suitsBtn" ActionTag="1400037367" Tag="143" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-53.9655" RightMargin="173.9655" TopMargin="343.1536" BottomMargin="24.8464" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_17" ActionTag="-103059519" Tag="144" IconVisible="False" LeftMargin="56.1632" RightMargin="62.8368" TopMargin="130.1890" BottomMargin="20.8110" FontSize="39" LabelText="suits" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="81.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="96.6632" Y="45.3110" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.4833" Y="0.2266" />
                <PreSize X="0.4050" Y="0.2450" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.0345" Y="124.8464" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1439" Y="0.2198" />
            <PreSize X="0.6250" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="masteriesBtn" ActionTag="303953749" Tag="145" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-54.0000" RightMargin="174.0000" TopMargin="413.5288" BottomMargin="-45.5288" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_17" ActionTag="1108809309" Tag="146" IconVisible="False" LeftMargin="11.6633" RightMargin="14.3367" TopMargin="130.1890" BottomMargin="20.8110" FontSize="39" LabelText="masteries" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="174.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="98.6633" Y="45.3110" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.4933" Y="0.2266" />
                <PreSize X="0.8700" Y="0.2450" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="46.0000" Y="54.4712" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1437" Y="0.0959" />
            <PreSize X="0.6250" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn_mastery.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="leaderBoardBtn" ActionTag="-1275323952" Tag="147" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="173.8029" RightMargin="-53.8029" TopMargin="342.3584" BottomMargin="25.6416" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_17" ActionTag="1841969219" Tag="148" IconVisible="False" LeftMargin="25.1638" RightMargin="31.8362" TopMargin="90.5857" BottomMargin="11.4143" FontSize="39" LabelText="leader-&#xA;board" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="143.0000" Y="98.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="96.6638" Y="60.4143" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.4833" Y="0.3021" />
                <PreSize X="0.7150" Y="0.4900" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="273.8029" Y="125.6416" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8556" Y="0.2212" />
            <PreSize X="0.6250" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn_leaderboard.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="optionBtn" ActionTag="1362030121" Tag="149" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="174.0157" RightMargin="-54.0157" TopMargin="413.4720" BottomMargin="-45.4720" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="178" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_17" ActionTag="838836381" Tag="150" IconVisible="False" LeftMargin="26.6632" RightMargin="33.3368" TopMargin="130.1890" BottomMargin="20.8110" FontSize="39" LabelText="options" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="140.0000" Y="49.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="96.6632" Y="45.3110" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.4833" Y="0.2266" />
                <PreSize X="0.7000" Y="0.2450" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="274.0157" Y="54.5280" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8563" Y="0.0960" />
            <PreSize X="0.6250" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="btn_option.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="playerNode" ActionTag="-398924780" Tag="989" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="369.2000" BottomMargin="198.8000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="160.0000" Y="198.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3500" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="gmBtn" ActionTag="-311868648" Tag="218" IconVisible="False" LeftMargin="51.9996" RightMargin="48.0004" TopMargin="459.0000" BottomMargin="-11.0000" TouchEnable="True" FontSize="35" ButtonText="GM Tool" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="220.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="161.9996" Y="49.0000" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5062" Y="0.0863" />
            <PreSize X="0.6875" Y="0.2113" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="173" G="216" B="230" />
            <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>