<GameFile>
  <PropertyGroup Name="GameOverDialog" Type="Layer" ID="b4bd761c-546d-4093-9006-8e1890005e7c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="17" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="panelDialog" ActionTag="857335574" Tag="18" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="21.2993" RightMargin="18.7007" TopMargin="157.5471" BottomMargin="110.4529" TouchEnable="True" StretchHeightEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Enable="True" LeftEage="30" RightEage="30" TopEage="30" BottomEage="30" Scale9OriginX="30" Scale9OriginY="30" Scale9Width="40" Scale9Height="40" ctype="PanelObjectData">
            <Size X="280.0000" Y="300.0000" />
            <Children>
              <AbstractNodeData Name="scoreText" ActionTag="1826609204" Tag="23" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="110.0000" RightMargin="110.0000" TopMargin="71.0000" BottomMargin="171.0000" FontSize="50" LabelText="50" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="140.0000" Y="200.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="216" B="0" />
                <PrePosition X="0.5000" Y="0.6667" />
                <PreSize X="0.2143" Y="0.1933" />
                <FontResource Type="Normal" Path="Arial Rounded Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="scoreTextCaption" ActionTag="1479195395" Tag="22" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="35.2725" RightMargin="38.7275" TopMargin="29.4212" BottomMargin="241.5788" FontSize="25" LabelText="Distance Travelled" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="TextObjectData">
                <Size X="206.0000" Y="29.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="241.2725" Y="256.0788" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="204" G="255" B="0" />
                <PrePosition X="0.8617" Y="0.8536" />
                <PreSize X="0.7357" Y="0.0967" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="buttonOK" ActionTag="2036692582" Tag="22" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="42.5981" RightMargin="37.4019" TopMargin="150.9134" BottomMargin="99.0866" TouchEnable="True" FontSize="24" ButtonText="Try Again" Scale9Enable="True" LeftEage="30" RightEage="30" TopEage="30" BottomEage="30" Scale9OriginX="30" Scale9OriginY="-2" Scale9Width="41" Scale9Height="32" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="200.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.5981" Y="124.0866" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5093" Y="0.4136" />
                <PreSize X="0.7143" Y="0.1667" />
                <TextColor A="255" R="255" G="165" B="0" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="buttonCancel" ActionTag="1988325996" Tag="24" IconVisible="False" HorizontalEdge="BothEdge" LeftMargin="43.8973" RightMargin="36.1027" TopMargin="207.0595" BottomMargin="42.9405" TouchEnable="True" FontSize="24" ButtonText="Bye bye!" Scale9Enable="True" LeftEage="30" RightEage="30" TopEage="30" BottomEage="30" Scale9OriginX="30" Scale9OriginY="-2" Scale9Width="34" Scale9Height="32" OutlineSize="0" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="ButtonObjectData">
                <Size X="200.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="143.8973" Y="67.9405" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5139" Y="0.2265" />
                <PreSize X="0.7143" Y="0.1667" />
                <TextColor A="255" R="255" G="192" B="203" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="161.2993" Y="260.4529" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5041" Y="0.4585" />
            <PreSize X="0.8750" Y="0.5282" />
            <FileData Type="Normal" Path="roundRect.png" Plist="" />
            <SingleColor A="255" R="206" G="255" B="169" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="textGameOver" ActionTag="297498239" Tag="20" RotationSkewX="5.0002" IconVisible="False" LeftMargin="16.9230" RightMargin="16.0770" TopMargin="91.5804" BottomMargin="416.4196" FontSize="52" LabelText="Game Over" OutlineEnabled="True" ShadowOffsetX="0.0000" ShadowOffsetY="0.0000" ctype="TextObjectData">
            <Size X="288.0000" Y="62.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.4230" Y="446.4196" />
            <Scale ScaleX="0.8388" ScaleY="1.0000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition X="0.5013" Y="0.7860" />
            <PreSize X="0.8969" Y="0.1056" />
            <FontResource Type="Normal" Path="Arial Rounded Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="255" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>