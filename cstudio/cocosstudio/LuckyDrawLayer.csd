<GameFile>
  <PropertyGroup Name="LuckyDrawLayer" Type="Layer" ID="85adf63b-3a36-4e19-9f93-5baf57455db1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="831" Speed="1.0000" ActivedAnimationName="gacha_inactive">
        <Timeline ActionTag="-235256430" Property="Position">
          <PointFrame FrameIndex="0" X="160.0293" Y="380.0183">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="160.0293" Y="380.0183">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="170" X="150.0684" Y="389.9780">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="370" X="160.0293" Y="380.0183">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="400" X="160.0293" Y="380.0183">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-235256430" Property="Scale">
          <ScaleFrame FrameIndex="0" X="-0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="-0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="170" X="-0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="370" X="-0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="-0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-235256430" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="170" X="4.2319" Y="4.2248">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="370" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-235256430" Property="AnchorPoint">
          <ScaleFrame FrameIndex="0" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="170" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="370" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2018913682" Property="Position">
          <PointFrame FrameIndex="0" X="160.1384" Y="378.4617">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="30" X="160.1384" Y="378.4617">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="170" X="171.0032" Y="389.3260">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="370" X="160.1384" Y="378.4617">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="400" X="160.1384" Y="378.4617">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-2018913682" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="170" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="370" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2018913682" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="170" X="-3.6464" Y="-3.6429">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="370" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-2018913682" Property="AnchorPoint">
          <ScaleFrame FrameIndex="0" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="30" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="170" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="370" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="659385943" Property="Position">
          <PointFrame FrameIndex="0" X="156.5710" Y="194.1927">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="400" X="156.5710" Y="194.1927">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-673382430" Property="Position">
          <PointFrame FrameIndex="0" X="32.6922" Y="500.3582">
            <EasingData Type="0" />
          </PointFrame>
          <PointFrame FrameIndex="400" X="9.4110" Y="556.2262">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="-673382430" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="0.3000" Y="0.3000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-673382430" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="770" Dst="1" />
          <BlendFuncFrame FrameIndex="400" Tween="False" Src="770" Dst="1" />
          <BlendFuncFrame FrameIndex="740" Tween="False" Src="770" Dst="1" />
          <BlendFuncFrame FrameIndex="831" Tween="False" Src="770" Dst="1" />
        </Timeline>
        <Timeline ActionTag="-673382430" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="True" />
          <BoolFrame FrameIndex="400" Tween="False" Value="True" />
          <BoolFrame FrameIndex="790" Tween="False" Value="False" />
        </Timeline>
        <Timeline ActionTag="-673382430" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="-29.2140" Y="-29.2116">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="400" X="-121.4155" Y="-121.4310">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="658838730" Property="Position">
          <PointFrame FrameIndex="0" X="285.6204" Y="501.1237">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
        <Timeline ActionTag="658838730" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.7500" Y="0.7500">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="658838730" Property="BlendFunc">
          <BlendFuncFrame FrameIndex="0" Tween="False" Src="770" Dst="1" />
          <BlendFuncFrame FrameIndex="740" Tween="False" Src="770" Dst="1" />
          <BlendFuncFrame FrameIndex="831" Tween="False" Src="770" Dst="1" />
        </Timeline>
        <Timeline ActionTag="658838730" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="True" />
          <BoolFrame FrameIndex="790" Tween="False" Value="False" />
        </Timeline>
        <Timeline ActionTag="658838730" Property="RotationSkew">
          <ScaleFrame FrameIndex="0" X="29.1715" Y="29.1683">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="1761900690" Property="Position">
          <PointFrame FrameIndex="0" X="1.5541" Y="0.0000">
            <EasingData Type="0" />
          </PointFrame>
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="gacha_inactive" StartIndex="0" EndIndex="400">
          <RenderColor A="255" R="46" G="139" B="87" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Layer" Tag="22" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="blockPanel" ActionTag="-861068503" Tag="570" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="bgSprite" ActionTag="605791210" Tag="1843" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-284.0000" BottomMargin="-284.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.0000" />
            <FileData Type="Normal" Path="guiImage/gui_gacha_bg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="gui_gacha_light_12_0_0" ActionTag="-235256430" Tag="96" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-160.9707" RightMargin="-161.0293" TopMargin="-190.5183" BottomMargin="1.5183" ctype="SpriteObjectData">
            <Size X="642.0000" Y="757.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0293" Y="380.0183" />
            <Scale ScaleX="-0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5001" Y="0.6690" />
            <PreSize X="2.0062" Y="1.3327" />
            <FileData Type="Normal" Path="guiImage/gui_gacha_light.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="gui_gacha_light_12_0" ActionTag="-2018913682" Tag="95" IconVisible="False" LeftMargin="-160.8616" RightMargin="-161.1384" TopMargin="-188.9617" BottomMargin="-0.0383" ctype="SpriteObjectData">
            <Size X="642.0000" Y="757.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.1384" Y="378.4617" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5004" Y="0.6663" />
            <PreSize X="2.0062" Y="1.3327" />
            <FileData Type="Normal" Path="guiImage/gui_gacha_light.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_21" ActionTag="659385943" Tag="129" IconVisible="True" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="156.5710" RightMargin="163.4290" TopMargin="373.8073" BottomMargin="194.1927" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="156.5710" Y="194.1927" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4893" Y="0.3419" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle_gacha_bg2.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_11" ActionTag="396766458" Tag="93" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="-0.0270" RightMargin="-319.9730" TopMargin="345.0000" BottomMargin="162.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="61.0000" />
            <AnchorPoint />
            <Position X="-0.0270" Y="162.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0001" Y="0.2852" />
            <PreSize X="2.0000" Y="0.1074" />
            <FileData Type="Normal" Path="guiImage/gui_gacha_track-2.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0" ActionTag="-673382430" Tag="126" RotationSkewX="-29.2140" RotationSkewY="-29.2116" IconVisible="True" LeftMargin="32.6922" RightMargin="287.3078" TopMargin="67.6418" BottomMargin="500.3582" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="32.6922" Y="500.3582" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1022" Y="0.8809" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle_gacha_spotlight_dust1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1_0_0" ActionTag="658838730" Tag="127" RotationSkewX="29.1715" RotationSkewY="29.1683" IconVisible="True" LeftMargin="285.6204" RightMargin="34.3796" TopMargin="66.8763" BottomMargin="501.1237" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="285.6204" Y="501.1237" />
            <Scale ScaleX="0.7500" ScaleY="0.7500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8926" Y="0.8823" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle_gacha_spotlight_dust1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="trackNode" ActionTag="-490728047" Tag="1993" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="395.0000" BottomMargin="173.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="160.0000" Y="173.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3046" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="box04" ActionTag="455248888" Tag="1543" IconVisible="True" LeftMargin="480.0000" RightMargin="-160.0000" TopMargin="348.0000" BottomMargin="220.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="480.0000" Y="220.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.5000" Y="0.3873" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="box03" ActionTag="-172935414" Tag="1544" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="320.0000" TopMargin="348.0000" BottomMargin="220.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="320.0000" Y="220.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0000" Y="0.3873" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="box02" ActionTag="1899534126" Tag="1545" IconVisible="True" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="348.0000" BottomMargin="220.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="160.0000" Y="220.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3873" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="box01" ActionTag="1804813125" Tag="1546" IconVisible="True" RightMargin="320.0000" TopMargin="348.0000" BottomMargin="220.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position Y="220.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.3873" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="animePanel" ActionTag="1761900690" Tag="1552" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="1.5541" RightMargin="-1.5541" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position X="1.5541" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0049" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="rewardPanel" ActionTag="1643629222" VisibleForFrame="False" Tag="1547" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="raritySprite" ActionTag="621694021" Tag="1548" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-83.0000" RightMargin="-83.0000" TopMargin="66.9200" BottomMargin="419.0800" ctype="SpriteObjectData">
                <Size X="486.0000" Y="82.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="460.0800" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8100" />
                <PreSize X="1.5187" Y="0.1444" />
                <FileData Type="Normal" Path="guiImage/gacha_title1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardSprite" ActionTag="-968004580" Tag="1549" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="81.5000" RightMargin="81.5000" TopMargin="196.9600" BottomMargin="231.0400" ctype="SpriteObjectData">
                <Size X="157.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="301.0400" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5300" />
                <PreSize X="0.4906" Y="0.2465" />
                <FileData Type="Normal" Path="guiImage/ui_booster_item1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-1338685554" Tag="1550" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="13.0000" RightMargin="13.0000" TopMargin="389.2800" BottomMargin="150.7200" FontSize="20" LabelText="YOU RECEIVE A TREASURE" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="294.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="164.7200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2900" />
                <PreSize X="0.9187" Y="0.0493" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardText" ActionTag="440629828" Tag="1551" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="43.5000" RightMargin="43.5000" TopMargin="402.6800" BottomMargin="107.3200" FontSize="42" LabelText="pika dog" OutlineSize="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="233.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="136.3200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="228" B="0" />
                <PrePosition X="0.5000" Y="0.2400" />
                <PreSize X="0.7281" Y="0.1021" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="165" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="doubleUpBtn" ActionTag="1622696840" Tag="414" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-16.5000" RightMargin="-16.5000" TopMargin="444.0000" BottomMargin="30.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="videoSprite" ActionTag="-392749292" Tag="415" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.5329" RightMargin="235.4671" TopMargin="25.3648" BottomMargin="23.6352" ctype="SpriteObjectData">
                    <Size X="52.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="91.5329" Y="46.1352" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2593" Y="0.4908" />
                    <PreSize X="0.1473" Y="0.4787" />
                    <FileData Type="Normal" Path="guiImage/ui_general_video.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="text" ActionTag="-1395538391" Tag="416" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="132.1500" RightMargin="96.8500" TopMargin="32.5000" BottomMargin="32.5000" FontSize="18" LabelText="double up!" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="124.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="194.1500" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5500" Y="0.5000" />
                    <PreSize X="0.3513" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="20" G="119" B="122" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="77.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1356" />
                <PreSize X="1.1031" Y="0.1655" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="guiImage/ui_general_btn_off.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_10" ActionTag="-39543689" Tag="417" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="58.0000" RightMargin="58.0000" TopMargin="528.0000" BottomMargin="12.0000" FontSize="20" LabelText="tap to continue" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="204.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="26.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0458" />
                <PreSize X="0.6375" Y="0.0493" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="touchPanel" ActionTag="141020798" Tag="1541" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="-1.0000" BottomMargin="1.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="tapMe" ActionTag="-1558575017" Tag="2353" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="244.2400" BottomMargin="323.7600" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="160.0000" Y="323.7600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5700" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position Y="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.0018" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>