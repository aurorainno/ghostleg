<GameFile>
  <PropertyGroup Name="MasteriesScene" Type="Scene" ID="c8d91a17-5087-4436-83bf-c3d65b423321" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="181" ctype="GameNodeObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-759671295" Tag="199" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="197.7579" RightMargin="14.2421" TopMargin="-32.7198" BottomMargin="492.7198" ctype="SpriteObjectData">
            <Size X="108.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5115" ScaleY="0.4100" />
            <Position X="252.9999" Y="536.9998" />
            <Scale ScaleX="0.1500" ScaleY="0.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7906" Y="0.9454" />
            <PreSize X="0.3375" Y="0.1901" />
            <FileData Type="Normal" Path="star_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="373265499" Tag="190" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="238.4160" RightMargin="-18.4160" TopMargin="14.5004" BottomMargin="520.4996" IsCustomSize="True" FontSize="30" LabelText="99999" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="288.4160" Y="536.9996" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.9013" Y="0.9454" />
            <PreSize X="0.3125" Y="0.0581" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="-2117484636" Tag="201" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="69.5000" RightMargin="69.5000" TopMargin="5.1800" BottomMargin="511.8200" FontSize="40" LabelText="masteries" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="181.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="537.3200" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.5000" Y="0.9460" />
            <PreSize X="0.5656" Y="0.0898" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="backBtn" ActionTag="-312706440" Tag="202" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-48.8000" RightMargin="168.8000" TopMargin="-25.5117" BottomMargin="477.5117" TouchEnable="True" FontSize="36" ButtonText="back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="116.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.2000" Y="535.5117" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1600" Y="0.9428" />
            <PreSize X="0.6250" Y="0.2042" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="49" G="216" B="212" />
            <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="testPanel" ActionTag="1924266574" Tag="203" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="BothEdge" TopMargin="55.2096" BottomMargin="-0.2096" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="513.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="512.7904" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9028" />
            <PreSize X="1.0000" Y="0.9032" />
            <SingleColor A="255" R="255" G="255" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>