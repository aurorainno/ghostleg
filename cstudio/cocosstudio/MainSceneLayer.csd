<GameFile>
  <PropertyGroup Name="MainSceneLayer" Type="Layer" ID="4e2ea483-2ae4-43bb-a606-1fffa6b6003b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="160" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="ui_main_bg1_3" ActionTag="-2023388489" Tag="294" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-284.0000" BottomMargin="-284.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.0000" />
            <FileData Type="Normal" Path="guiImage/ui_main_bg1.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="BGMain1_1cloud_6" ActionTag="1919867872" VisibleForFrame="False" Tag="73" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="446.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="122.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="160.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="2.0000" Y="0.2148" />
            <FileData Type="Normal" Path="BGMain1_1cloud.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="screenParticle" ActionTag="-946950849" VisibleForFrame="False" Tag="55" RotationSkewX="26.0000" RotationSkewY="26.0000" IconVisible="True" PositionPercentYEnabled="True" LeftMargin="323.1592" RightMargin="-3.1592" TopMargin="161.7664" BottomMargin="406.2336" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="323.1592" Y="406.2336" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0099" Y="0.7152" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/star2.plist" Plist="" />
            <BlendFunc Src="1" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="leaderboardView" ActionTag="214178151" Tag="1673" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="basePanel" ActionTag="-128262077" Tag="219" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="stageSelectPanel" ActionTag="-2141327413" Tag="125" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="95.0000" BottomMargin="313.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="193" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="160.0000" />
                <AnchorPoint />
                <Position Y="313.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.5511" />
                <PreSize X="1.0000" Y="0.2817" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="leftButton" ActionTag="291143969" Tag="56" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-18.0000" RightMargin="248.0000" TopMargin="112.9000" BottomMargin="340.1000" TouchEnable="True" FlipX="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="60" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="90.0000" Y="115.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="27.0000" Y="397.6000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0844" Y="0.7000" />
                <PreSize X="0.2813" Y="0.2025" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_main_arrow.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="rightButton" ActionTag="-563783796" Tag="58" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="248.0000" RightMargin="-18.0000" TopMargin="112.9000" BottomMargin="340.1000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="60" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="90.0000" Y="115.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="293.0000" Y="397.6000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9156" Y="0.7000" />
                <PreSize X="0.2813" Y="0.2025" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_main_arrow.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="playerNode" ActionTag="181585006" Tag="162" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.9680" RightMargin="272.0320" TopMargin="460.1936" BottomMargin="107.8064" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="47.9680" Y="107.8064" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1499" Y="0.1898" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="purchaseButton" ActionTag="1662555329" Tag="683" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-1.5000" RightMargin="-1.5000" TopMargin="411.0000" BottomMargin="43.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="293" Scale9Height="92" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="323.0000" Y="114.0000" />
                <Children>
                  <AbstractNodeData Name="starSprite" ActionTag="1102751306" Tag="698" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="78.0000" RightMargin="201.0000" TopMargin="51.8200" BottomMargin="22.1800" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="42.1800" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3096" Y="0.3700" />
                    <PreSize X="0.1362" Y="0.3509" />
                    <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="price" ActionTag="584959256" Tag="699" RotationSkewX="2.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="98.5000" RightMargin="98.5000" TopMargin="49.0000" BottomMargin="15.0000" FontSize="36" LabelText="8000" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="126.0000" Y="50.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="161.5000" Y="40.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3509" />
                    <PreSize X="0.3901" Y="0.4386" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="100.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1761" />
                <PreSize X="1.0094" Y="0.2007" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_main_locked_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="stageNotClearHint" ActionTag="-702328575" VisibleForFrame="False" Tag="1326" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="404.5000" BottomMargin="128.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="35.0000" />
                <Children>
                  <AbstractNodeData Name="Sprite_4" ActionTag="-2040244776" Tag="1327" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-61.5000" RightMargin="-61.5000" TopMargin="-39.5000" BottomMargin="-39.5000" ctype="SpriteObjectData">
                    <Size X="323.0000" Y="114.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="17.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.6150" Y="3.2571" />
                    <FileData Type="Normal" Path="guiImage/ui_main_locked_popup.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="content" ActionTag="-152452763" Tag="1328" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-20.0000" RightMargin="-20.0000" TopMargin="7.1000" BottomMargin="2.9000" FontSize="18" LabelText="unlock stage 3 first" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="240.0000" Y="25.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="15.4000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.4400" />
                    <PreSize X="1.2000" Y="0.7143" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="146.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2570" />
                <PreSize X="0.6250" Y="0.0616" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="playButton" Visible="False" ActionTag="1238420880" Tag="188" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-3.5000" RightMargin="-3.5000" TopMargin="413.0000" BottomMargin="45.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="297" Scale9Height="88" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="327.0000" Y="110.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="100.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1761" />
                <PreSize X="1.0219" Y="0.1937" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_main_start_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="referenceFileNode" ActionTag="1397606102" VisibleForFrame="False" Tag="23" IconVisible="True" RightMargin="160.0000" TopMargin="408.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="160.0000" Y="160.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.5000" Y="0.2817" />
            <FileData Type="Normal" Path="PlanetItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="gmButton" ActionTag="1254869564" Alpha="132" Tag="161" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="166.1534" RightMargin="-66.1534" TopMargin="407.8825" BottomMargin="40.1175" TouchEnable="True" FontSize="35" ButtonText="GM Tool" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="220.0000" Y="120.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="276.1534" Y="100.1175" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="0" B="0" />
            <PrePosition X="0.8630" Y="0.1763" />
            <PreSize X="0.6875" Y="0.2113" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="173" G="216" B="230" />
            <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="logoSprite" ActionTag="-981216352" VisibleForFrame="False" Tag="180" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-159.3920" RightMargin="-160.6080" TopMargin="10.6400" BottomMargin="317.3600" ctype="SpriteObjectData">
            <Size X="640.0000" Y="240.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.6080" Y="437.3600" />
            <Scale ScaleX="0.4500" ScaleY="0.4500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5019" Y="0.7700" />
            <PreSize X="2.0000" Y="0.4225" />
            <FileData Type="Normal" Path="ui_title.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="centerPanel" ActionTag="-406536086" Tag="187" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="183.9752" BottomMargin="184.0248" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="200.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="384.0248" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.6761" />
            <PreSize X="1.0000" Y="0.3521" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="planetInfo" ActionTag="-2090890462" VisibleForFrame="False" Tag="298" RotationSkewX="10.0000" IconVisible="False" LeftMargin="61.5003" RightMargin="167.4997" TopMargin="50.5000" BottomMargin="482.5000" FontSize="18" LabelText="stage 1" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="91.0000" Y="35.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="61.5003" Y="500.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1922" Y="0.8803" />
            <PreSize X="0.2844" Y="0.0616" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="156" G="0" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="planetName" ActionTag="2075888929" VisibleForFrame="False" Tag="299" RotationSkewX="10.0003" IconVisible="False" LeftMargin="60.4661" RightMargin="121.5339" TopMargin="68.5000" BottomMargin="456.5000" IsCustomSize="True" FontSize="24" LabelText="Planet A" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="138.0000" Y="43.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="60.4661" Y="478.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1890" Y="0.8415" />
            <PreSize X="0.4313" Y="0.0757" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="156" G="0" B="255" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="leaderboardTimePanel" ActionTag="-1678666326" Tag="203" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="38.0000" BottomMargin="485.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="153" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="45.0000" />
            <Children>
              <AbstractNodeData Name="ui_main_alarm_1" ActionTag="1268448369" Tag="204" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="0.5000" RightMargin="294.5000" TopMargin="11.5000" BottomMargin="2.5000" ctype="SpriteObjectData">
                <Size X="25.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="13.0000" Y="18.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0406" Y="0.4000" />
                <PreSize X="0.0781" Y="0.6889" />
                <FileData Type="Normal" Path="guiImage/ui_main_alarm.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="leaderboardTimeCaption" ActionTag="1243502682" Tag="206" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="25.0000" RightMargin="33.0000" TopMargin="13.9000" BottomMargin="3.1000" FontSize="20" LabelText="weekly tournament: " VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="262.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="25.0000" Y="17.1000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0781" Y="0.3800" />
                <PreSize X="0.8188" Y="0.6222" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leaderboardTimeText" ActionTag="1811870000" Tag="205" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="RightEdge" LeftMargin="115.0000" RightMargin="52.0000" TopMargin="13.9000" BottomMargin="3.1000" FontSize="20" LabelText="6days 07:10" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="153.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="268.0000" Y="17.1000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.8375" Y="0.3800" />
                <PreSize X="0.4781" Y="0.6222" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="255" B="255" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="530.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.9331" />
            <PreSize X="1.0000" Y="0.0792" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="topPanel" ActionTag="1002040230" Tag="185" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="468.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="ui_main_topbar_1" ActionTag="1428492800" Tag="285" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="8.0000" ctype="SpriteObjectData">
                <Size X="640.0000" Y="92.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="100.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="2.0000" Y="0.9200" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="oldFbButton" ActionTag="113646389" VisibleForFrame="False" Tag="183" IconVisible="False" LeftMargin="175.3192" RightMargin="72.6808" TopMargin="38.4520" BottomMargin="-10.4520" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="42" Scale9Height="50" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="72.0000" Y="72.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="211.3192" Y="25.5480" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6604" Y="0.2555" />
                <PreSize X="0.2250" Y="0.7200" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/btn_fb.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dailyRewardButton" ActionTag="283564949" VisibleForFrame="False" Tag="76" IconVisible="False" LeftMargin="197.0000" RightMargin="51.0000" TopMargin="62.4499" BottomMargin="-34.4499" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="42" Scale9Height="50" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="72.0000" Y="72.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="233.0000" Y="1.5501" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7281" Y="0.0155" />
                <PreSize X="0.2250" Y="0.7200" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/btn_dailylogin_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="1290985614" Tag="173" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="55.0575" ctype="SpriteObjectData">
                <Size X="166.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="185.1505" Y="73.5575" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5786" Y="0.7356" />
                <PreSize X="0.5188" Y="0.4000" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalCoinText" ActionTag="-1077608377" Tag="172" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="7.6674" BottomMargin="52.3326" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="100.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="214.3221" Y="72.3326" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6698" Y="0.7233" />
                <PreSize X="0.3125" Y="0.4000" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="candyPanel" ActionTag="-322286037" VisibleForFrame="False" Tag="539" IconVisible="False" VerticalEdge="TopEdge" RightMargin="120.0000" TopMargin="45.0000" BottomMargin="30.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="25.0000" />
                <Children>
                  <AbstractNodeData Name="candySprite" ActionTag="-1468452806" Tag="538" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.3814" RightMargin="163.6186" TopMargin="-19.7500" BottomMargin="-15.2500" ctype="SpriteObjectData">
                    <Size X="33.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                    <Position X="22.0000" Y="12.5000" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1100" Y="0.5000" />
                    <PreSize X="0.1650" Y="2.4000" />
                    <FileData Type="Normal" Path="guiImage/item_candystick.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="totalCandyText" ActionTag="1638855902" Tag="540" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="35.3000" RightMargin="64.7000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="35" LabelText="12" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="40.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="35.3000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1765" Y="0.5000" />
                    <PreSize X="0.5000" Y="1.6000" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="30.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.3000" />
                <PreSize X="0.6250" Y="0.2500" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondPanel" ActionTag="2014143710" Tag="97" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="61.0100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="120.0000" Y="25.0000" />
                <Children>
                  <AbstractNodeData Name="sprite" ActionTag="64945300" Tag="98" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                    <Size X="162.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                    <Position X="60.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.3500" Y="1.6000" />
                    <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="totalDiamondText" ActionTag="-1042283017" Tag="99" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="120.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7500" Y="0.5000" />
                    <PreSize X="1.0000" Y="1.6000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="1" G="44" B="98" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="335.0000" Y="61.0100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0469" Y="0.6101" />
                <PreSize X="0.3750" Y="0.2500" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbBtn" ActionTag="2035957376" Tag="254" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-35.0650" RightMargin="163.0650" TopMargin="-2.0000" BottomMargin="45.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="192.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="60.9350" Y="73.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1904" Y="0.7350" />
                <PreSize X="0.6000" Y="0.5700" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_fb.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="profilePicFrame" ActionTag="-960322983" Tag="365" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.9350" RightMargin="252.0650" BottomMargin="48.0000" ctype="SpriteObjectData">
                <Size X="52.0000" Y="52.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="15.9350" Y="74.0000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0498" Y="0.7400" />
                <PreSize X="0.1625" Y="0.5200" />
                <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbName" ActionTag="-177626357" Tag="366" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="56.0000" RightMargin="106.0000" TopMargin="11.0000" BottomMargin="55.0000" FontSize="20" LabelText="ggg ggggg" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="158.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="56.0000" Y="72.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1750" Y="0.7200" />
                <PreSize X="0.4938" Y="0.3400" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="182" G="88" B="14" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="giftButton" ActionTag="-101979484" Tag="508" IconVisible="False" LeftMargin="260.0000" RightMargin="-14.0000" TopMargin="26.0700" BottomMargin="-1.0700" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="44" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="74.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="297.0000" Y="36.4300" />
                <Scale ScaleX="0.4700" ScaleY="0.4700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9281" Y="0.3643" />
                <PreSize X="0.2313" Y="0.7500" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_main_giftbtn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="0.1761" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockInfoPanel" ActionTag="-1849942757" VisibleForFrame="False" Tag="430" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="25.6000" RightMargin="25.6000" TopMargin="411.0000" BottomMargin="107.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="268.8000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_2" ActionTag="1595917095" Tag="435" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-138.6000" RightMargin="-138.6000" TopMargin="-36.5000" BottomMargin="-38.5000" Scale9Enable="True" LeftEage="180" RightEage="180" TopEage="41" BottomEage="41" Scale9OriginX="180" Scale9OriginY="41" Scale9Width="186" Scale9Height="45" ctype="ImageViewObjectData">
                <Size X="546.0000" Y="125.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="134.4000" Y="24.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4800" />
                <PreSize X="2.0313" Y="2.5000" />
                <FileData Type="Normal" Path="guiImage/ui_menu_mission.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogIconSprite" ActionTag="1380393108" Tag="434" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-16.5000" RightMargin="188.3000" TopMargin="-24.5000" BottomMargin="-22.5000" ctype="SpriteObjectData">
                <Size X="97.0000" Y="97.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="26.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1190" Y="0.5200" />
                <PreSize X="0.3609" Y="1.9400" />
                <FileData Type="Normal" Path="dog/dselect_dog2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="infoText" ActionTag="69064708" Tag="437" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="-194.2000" TopMargin="-7.0000" BottomMargin="-7.0000" IsCustomSize="True" FontSize="25" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="400.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="63.0000" Y="25.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2344" Y="0.5000" />
                <PreSize X="1.4881" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="132.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2324" />
            <PreSize X="0.8400" Y="0.0880" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="versionText" ActionTag="571294491" Tag="174" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="9.7120" RightMargin="10.2880" TopMargin="545.0000" BottomMargin="3.0000" IsCustomSize="True" FontSize="20" LabelText="1.0.0 (debug)" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Bottom" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="300.0000" Y="20.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="159.7120" Y="3.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="127" G="127" B="127" />
            <PrePosition X="0.4991" Y="0.0053" />
            <PreSize X="0.9375" Y="0.0352" />
            <FontResource Type="Normal" Path="Arial Rounded Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="lockedInfoPanel" ActionTag="509644131" VisibleForFrame="False" Tag="63" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="25.6000" RightMargin="25.6000" TopMargin="411.0000" BottomMargin="107.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="268.8000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_2" ActionTag="541452557" Tag="64" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-2.0966" RightMargin="-275.1034" TopMargin="-67.7500" BottomMargin="-7.2500" Scale9Enable="True" LeftEage="180" RightEage="180" TopEage="41" BottomEage="41" Scale9OriginX="180" Scale9OriginY="41" Scale9Width="186" Scale9Height="45" ctype="ImageViewObjectData">
                <Size X="546.0000" Y="125.0000" />
                <AnchorPoint />
                <Position X="-2.0966" Y="-7.2500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.0078" Y="-0.1450" />
                <PreSize X="2.0313" Y="2.5000" />
                <FileData Type="Normal" Path="guiImage/ui_menu_mission.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sprite" ActionTag="-339223981" Tag="65" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-24.0000" RightMargin="180.8000" TopMargin="-39.5000" BottomMargin="-37.5000" ctype="SpriteObjectData">
                <Size X="112.0000" Y="127.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="26.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1190" Y="0.5200" />
                <PreSize X="0.4167" Y="2.5400" />
                <FileData Type="Normal" Path="guiImage/ui_menu_lightbulb.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="text" ActionTag="-860785447" Tag="66" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="62.9999" RightMargin="-44.1999" TopMargin="-9.0000" BottomMargin="-5.0000" IsCustomSize="True" FontSize="25" LabelText="Want to unlock&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="250.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="62.9999" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2344" Y="0.5400" />
                <PreSize X="0.9301" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_0" ActionTag="-1899241955" Tag="67" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="202.8899" RightMargin="-310.0899" TopMargin="-9.0000" BottomMargin="-5.0000" IsCustomSize="True" FontSize="25" LabelText="?" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="376.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="202.8899" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7548" Y="0.5400" />
                <PreSize X="1.3988" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetName" ActionTag="1165500579" Tag="69" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="128.9992" RightMargin="39.8008" TopMargin="-1.0000" BottomMargin="19.0000" IsCustomSize="True" FontSize="25" LabelText="Jupiter" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="178.9992" Y="35.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="59" G="255" B="215" />
                <PrePosition X="0.6659" Y="0.7000" />
                <PreSize X="0.3720" Y="0.6400" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_1" ActionTag="-247086587" Tag="217" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="-194.2000" TopMargin="12.5000" BottomMargin="-26.5000" IsCustomSize="True" FontSize="25" LabelText="Check the        menu below!&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="400.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="63.0000" Y="5.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="211" B="49" />
                <PrePosition X="0.2344" Y="0.1100" />
                <PreSize X="1.4881" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ui_menu_emoji_dog_1" ActionTag="-58485706" Tag="133" IconVisible="False" LeftMargin="129.2271" RightMargin="109.5729" TopMargin="21.2785" BottomMargin="-1.2785" ctype="SpriteObjectData">
                <Size X="30.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.2271" Y="13.7215" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5366" Y="0.2744" />
                <PreSize X="0.1116" Y="0.6000" />
                <FileData Type="Normal" Path="guiImage/ui_menu_emoji_dog.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="25.6000" Y="107.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0800" Y="0.1884" />
            <PreSize X="0.8400" Y="0.0880" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="snowParticle" ActionTag="50058398" VisibleForFrame="False" Tag="697" IconVisible="True" LeftMargin="165.4998" RightMargin="154.5002" TopMargin="179.0007" BottomMargin="388.9993" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="165.4998" Y="388.9993" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5172" Y="0.6849" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_snowflake.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="eventBtn" ActionTag="-537985570" Tag="275" IconVisible="False" LeftMargin="15.9833" RightMargin="204.0167" TopMargin="235.6371" BottomMargin="307.3629" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="6" BottomEage="6" Scale9OriginX="-15" Scale9OriginY="-6" Scale9Width="30" Scale9Height="12" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="100.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="65.9833" Y="319.8629" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2062" Y="0.5631" />
            <PreSize X="0.3125" Y="0.0440" />
            <TextColor A="255" R="65" G="65" B="70" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="tabView" ActionTag="-961813776" Tag="1646" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tabBar" ActionTag="-479766783" Tag="163" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="508.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="60.0000" />
            <Children>
              <AbstractNodeData Name="btn_bg" ActionTag="-1868835654" Tag="467" IconVisible="False" RightMargin="-320.0000" TopMargin="-7.0000" LeftEage="211" RightEage="211" TopEage="29" BottomEage="29" Scale9OriginX="211" Scale9OriginY="29" Scale9Width="218" Scale9Height="30" ctype="ImageViewObjectData">
                <Size X="640.0000" Y="67.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="1.1167" />
                <FileData Type="Normal" Path="guiImage/ui_maintab_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="optionButton" ActionTag="-694702192" Tag="184" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="224.0000" RightMargin="-32.0000" TopMargin="-24.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="98" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="128.0000" Y="115.0000" />
                <Children>
                  <AbstractNodeData Name="CaptionText" ActionTag="-768660755" Tag="286" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-23.5000" RightMargin="-23.5000" TopMargin="69.0000" BottomMargin="4.0000" FontSize="30" LabelText="Settings" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="175.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="64.0000" Y="25.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2174" />
                    <PreSize X="1.3672" Y="0.3652" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="288.0000" Y="26.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9000" Y="0.4417" />
                <PreSize X="0.4000" Y="1.9167" />
                <TextColor A="255" R="65" G="65" B="70" />
                <PressedFileData Type="Normal" Path="guiImage/ui_maintab_others_on.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_maintab_others_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leaderboardButton" ActionTag="2069875249" Tag="186" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="32.1400" RightMargin="159.8600" TopMargin="-24.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="98" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="128.0000" Y="115.0000" />
                <Children>
                  <AbstractNodeData Name="CaptionText" ActionTag="1554559455" Tag="289" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-60.4733" RightMargin="-56.5267" TopMargin="65.7952" BottomMargin="7.2048" FontSize="30" LabelText="Leaderboard" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="245.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5161" ScaleY="0.3474" />
                    <Position X="65.9712" Y="21.7956" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5154" Y="0.1895" />
                    <PreSize X="1.9141" Y="0.3652" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="96.1400" Y="26.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3004" Y="0.4417" />
                <PreSize X="0.4000" Y="1.9167" />
                <TextColor A="255" R="65" G="65" B="70" />
                <PressedFileData Type="Normal" Path="guiImage/ui_maintab_ranking_on.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_maintab_ranking_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogButton" ActionTag="469410074" Tag="189" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="-32.0000" RightMargin="224.0000" TopMargin="-24.0149" BottomMargin="-30.9851" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="98" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="128.0000" Y="115.0000" />
                <Children>
                  <AbstractNodeData Name="CaptionText" ActionTag="1132240893" Tag="290" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-21.0000" RightMargin="-21.0000" TopMargin="69.0000" BottomMargin="4.0000" FontSize="30" LabelText="Monster" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="170.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="64.0000" Y="25.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2174" />
                    <PreSize X="1.3281" Y="0.3652" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="26.5149" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1000" Y="0.4419" />
                <PreSize X="0.4000" Y="1.9167" />
                <TextColor A="255" R="65" G="65" B="70" />
                <PressedFileData Type="Normal" Path="guiImage/ui_maintab_character_on.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_maintab_character_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="newLabel" ActionTag="-2024065022" Tag="86" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="23.4966" RightMargin="242.5034" TopMargin="-12.9364" BottomMargin="32.9364" ctype="SpriteObjectData">
                <Size X="54.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.4966" Y="52.9364" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1578" Y="0.8823" />
                <PreSize X="0.1688" Y="0.6667" />
                <FileData Type="Normal" Path="guiImage/btn_ui_new.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="gachaButton" ActionTag="-663635480" Tag="468" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="96.0000" RightMargin="96.0000" TopMargin="-24.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="98" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="128.0000" Y="115.0000" />
                <Children>
                  <AbstractNodeData Name="CaptionText" ActionTag="1565958393" Tag="291" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-45.5000" RightMargin="-45.5000" TopMargin="69.0000" BottomMargin="4.0000" FontSize="30" LabelText="lucky draw" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="219.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="64.0000" Y="25.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2174" />
                    <PreSize X="1.7109" Y="0.3652" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="newLabel" ActionTag="1107098619" Tag="2368" IconVisible="True" LeftMargin="114.0002" RightMargin="13.9998" TopMargin="7.0005" BottomMargin="107.9995" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="114.0002" Y="107.9995" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8906" Y="0.9391" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="26.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4417" />
                <PreSize X="0.4000" Y="1.9167" />
                <TextColor A="255" R="65" G="65" B="70" />
                <PressedFileData Type="Normal" Path="guiImage/ui_maintab_gacha_on.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_maintab_gacha_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="addStarButton" ActionTag="1654071302" Tag="171" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="160.1300" RightMargin="31.8700" TopMargin="-24.0000" BottomMargin="-31.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="98" Scale9Height="93" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="128.0000" Y="115.0000" />
                <Children>
                  <AbstractNodeData Name="CaptionText" ActionTag="1091308112" Tag="292" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="16.5000" RightMargin="16.5000" TopMargin="69.0000" BottomMargin="4.0000" FontSize="30" LabelText="SHOP" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="95.0000" Y="42.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="64.0000" Y="25.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2174" />
                    <PreSize X="0.7422" Y="0.3652" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="224.1300" Y="26.5000" />
                <Scale ScaleX="0.5050" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7004" Y="0.4417" />
                <PreSize X="0.4000" Y="1.9167" />
                <TextColor A="255" R="65" G="65" B="70" />
                <PressedFileData Type="Normal" Path="guiImage/ui_maintab_shop_on.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_maintab_shop_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.1056" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>