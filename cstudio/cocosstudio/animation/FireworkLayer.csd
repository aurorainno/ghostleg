<GameFile>
  <PropertyGroup Name="FireworkLayer" Type="Layer" ID="16c71a08-58c9-4e52-aaca-cd09a202a209" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="145" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="550.0000" />
        <Children>
          <AbstractNodeData Name="firework1" ActionTag="1027385025" Tag="146" IconVisible="True" LeftMargin="56.2639" RightMargin="263.7361" TopMargin="63.3465" BottomMargin="486.6535" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="56.2639" Y="486.6535" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1758" Y="0.8848" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework2" ActionTag="2121766207" Tag="147" IconVisible="True" LeftMargin="106.6093" RightMargin="213.3907" TopMargin="164.0326" BottomMargin="385.9674" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="106.6093" Y="385.9674" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3332" Y="0.7018" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework3" ActionTag="1054167621" Tag="148" IconVisible="True" LeftMargin="208.2811" RightMargin="111.7189" TopMargin="187.7238" BottomMargin="362.2762" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="208.2811" Y="362.2762" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6509" Y="0.6587" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework4" ActionTag="1978277381" Tag="149" IconVisible="True" LeftMargin="251.7159" RightMargin="68.2841" TopMargin="88.0242" BottomMargin="461.9758" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="251.7159" Y="461.9758" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7866" Y="0.8400" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework5" ActionTag="-2103390746" Tag="150" IconVisible="True" LeftMargin="263.5616" RightMargin="56.4384" TopMargin="263.7334" BottomMargin="286.2666" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="263.5616" Y="286.2666" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8236" Y="0.5205" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework6" ActionTag="1730433241" Tag="151" IconVisible="True" LeftMargin="55.2773" RightMargin="264.7227" TopMargin="286.4374" BottomMargin="263.5626" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="55.2773" Y="263.5626" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1727" Y="0.4792" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework7" ActionTag="-1899297964" Tag="215" IconVisible="True" LeftMargin="109.6980" RightMargin="210.3020" TopMargin="443.5194" BottomMargin="106.4806" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="109.6980" Y="106.4806" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3428" Y="0.1936" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="firework8" ActionTag="1528954170" Tag="216" IconVisible="True" LeftMargin="249.7411" RightMargin="70.2589" TopMargin="427.5968" BottomMargin="122.4032" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="249.7411" Y="122.4032" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7804" Y="0.2226" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_firework1.plist" Plist="" />
            <BlendFunc Src="770" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-517697592" VisibleForFrame="False" Tag="151" IconVisible="False" LeftMargin="94.4010" RightMargin="92.5990" TopMargin="8.4146" BottomMargin="518.5854" IsCustomSize="True" FontSize="20" LabelText="Firework Layer asfasdfsadf" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="133.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5445" ScaleY="0.6088" />
            <Position X="166.8235" Y="532.5867" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5213" Y="0.9683" />
            <PreSize X="0.4156" Y="0.0418" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>