<GameFile>
  <PropertyGroup Name="GameOverDialog" Type="Layer" ID="5f151f22-5b1b-48fd-965f-aa5c5c023905" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="18" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="panelDialog" ActionTag="-1211135922" Tag="94" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentHeightEnable="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-69793243" Tag="3882" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-284.0000" BottomMargin="-284.0000" ctype="SpriteObjectData">
                <Size X="640.0000" Y="1136.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="284.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="2.0000" Y="2.0000" />
                <FileData Type="Normal" Path="guiImage/ui_result_bg.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="upperPanel" ActionTag="2081301606" Tag="88" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="10.0000" BottomMargin="398.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="160.0000" />
                <Children>
                  <AbstractNodeData Name="bgImage" ActionTag="198284157" Tag="89" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-134.7520" RightMargin="-137.2480" TopMargin="-104.7000" BottomMargin="-146.3000" LeftEage="108" RightEage="108" TopEage="97" BottomEage="97" Scale9OriginX="108" Scale9OriginY="97" Scale9Width="378" Scale9Height="240" ctype="ImageViewObjectData">
                    <Size X="592.0000" Y="411.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="161.2480" Y="59.2000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5039" Y="0.3700" />
                    <PreSize X="1.8500" Y="2.5687" />
                    <FileData Type="Normal" Path="guiImage/ui_result_scoreboard.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="title" ActionTag="1750410617" Tag="90" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="124.1000" RightMargin="104.9000" TopMargin="15.5000" BottomMargin="111.5000" FontSize="24" LabelText="score" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="91.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="169.6000" Y="128.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5300" Y="0.8000" />
                    <PreSize X="0.2844" Y="0.2062" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="deliveryScorePanel" ActionTag="1350538512" Tag="3877" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="49.0000" BottomMargin="81.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="moneyEarned" ActionTag="-1891414125" Tag="92" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="47.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="success delivery" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="218.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.6812" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="2122393204" Tag="95" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.0000" RightMargin="41.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="24.0000" Y="34.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.0750" Y="1.1333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="81.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.5063" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="clearScorePanel" ActionTag="1041913729" Tag="3878" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="70.0000" BottomMargin="60.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="tipEarned" ActionTag="-1758305206" Tag="91" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="108.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="stage clear" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="157.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.4906" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="-1475624999" Tag="623" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.0000" RightMargin="41.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="24.0000" Y="34.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.0750" Y="1.1333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="60.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.3750" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectScorePanel" ActionTag="-1062177074" VisibleForFrame="False" Tag="47" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="78.0000" BottomMargin="52.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="caption" ActionTag="-2007690832" Tag="48" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="80.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="Coin Collected" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="185.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.5781" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="1550470394" Tag="49" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="208.0000" RightMargin="41.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="20" LabelText="2222" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="71.0000" Y="34.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.2219" Y="1.1333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="52.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.3250" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="distanceScorePanel" ActionTag="873429373" Tag="50" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="91.0000" BottomMargin="39.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="caption" ActionTag="1037300977" Tag="51" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="61.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="Distance Travel" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="204.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.6375" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="1944688688" Tag="52" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.0000" RightMargin="41.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="24.0000" Y="34.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.0750" Y="1.1333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="39.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.2438" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="killScorePanel" ActionTag="1953244613" Tag="3879" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="112.0000" BottomMargin="18.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="clentServed" ActionTag="-44298072" Tag="622" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="67.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="swipe out alien" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="198.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.6187" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="2007787017" Tag="625" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.0000" RightMargin="41.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="24.0000" Y="34.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.0750" Y="1.1333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="18.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.1125" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bonusScorePanel" ActionTag="-1602652942" Tag="654" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="132.0000" BottomMargin="-2.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="bounsScore" ActionTag="-1993598823" Tag="655" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="103.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="bonus score" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="162.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.5063" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="557729450" Tag="656" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="255.0000" RightMargin="41.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="24.0000" Y="34.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.0750" Y="1.1333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="-2.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="-0.0125" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="totalScorePanel" ActionTag="1591301163" VisibleForFrame="False" Tag="3880" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="166.0000" BottomMargin="-36.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="totalScore" ActionTag="470477690" Tag="3881" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="81.0000" TopMargin="-1.5000" BottomMargin="-1.5000" FontSize="24" LabelText="total score" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="184.0000" Y="33.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="55.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.5000" />
                        <PreSize X="0.5750" Y="1.1000" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="scoreText" ActionTag="2087439356" Tag="97" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="241.0000" RightMargin="41.0000" TopMargin="-13.0000" BottomMargin="-13.0000" FontSize="36" LabelText="0" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="38.0000" Y="56.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.1187" Y="1.8667" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="-36.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="-0.2250" />
                    <PreSize X="1.0000" Y="0.1875" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="478.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8415" />
                <PreSize X="1.0000" Y="0.2817" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="rewardPanel" ActionTag="-1539049815" Tag="249" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="568.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="-1030099051" Tag="63" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="91.0000" RightMargin="91.0000" TopMargin="221.5000" BottomMargin="305.5000" FontSize="24" LabelText="rewards" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="138.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="326.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5739" />
                    <PreSize X="0.4313" Y="0.0722" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="23" G="114" B="151" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="collectedCoinPanel" ActionTag="1496749017" Tag="2316" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-3.6559" RightMargin="3.6559" TopMargin="258.0000" BottomMargin="280.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="30.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite_1" ActionTag="1058789487" Tag="1214" IconVisible="False" LeftMargin="29.0000" RightMargin="247.0000" TopMargin="-6.0000" BottomMargin="-4.0000" ctype="SpriteObjectData">
                        <Size X="44.0000" Y="40.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="51.0000" Y="16.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1594" Y="0.5333" />
                        <PreSize X="0.1375" Y="1.3333" />
                        <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="collectedCoin" ActionTag="626269071" Tag="2312" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="62.0000" RightMargin="132.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="collected" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="126.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="62.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1937" Y="0.5000" />
                        <PreSize X="0.3938" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="coinText" ActionTag="-212476636" Tag="2318" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="227.0000" RightMargin="41.0000" TopMargin="1.0000" BottomMargin="1.0000" FontSize="20" LabelText="222" HorizontalAlignmentType="HT_Right" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="52.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                        <Position X="279.0000" Y="15.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8719" Y="0.5000" />
                        <PreSize X="0.1625" Y="0.9333" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="225" G="88" B="0" />
                        <ShadowColor A="255" R="0" G="0" B="0" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-3.6559" Y="280.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0114" Y="0.4930" />
                    <PreSize X="1.0000" Y="0.0528" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="puzzlePanel" ActionTag="1046266596" Tag="65" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="285.0000" BottomMargin="135.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="148.0000" />
                    <Children>
                      <AbstractNodeData Name="puzzle1" ActionTag="701864289" Tag="66" IconVisible="True" HorizontalEdge="LeftEdge" LeftMargin="55.0000" RightMargin="265.0000" TopMargin="33.0000" BottomMargin="115.0000" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="55.0000" Y="115.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1719" Y="0.7770" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle2" ActionTag="-93202086" Tag="69" IconVisible="True" HorizontalEdge="LeftEdge" LeftMargin="125.0000" RightMargin="195.0000" TopMargin="33.0000" BottomMargin="115.0000" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="125.0000" Y="115.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3906" Y="0.7770" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle3" ActionTag="1205139754" Tag="70" IconVisible="True" HorizontalEdge="RightEdge" LeftMargin="195.0000" RightMargin="125.0000" TopMargin="33.0000" BottomMargin="115.0000" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="195.0000" Y="115.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6094" Y="0.7770" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle4" ActionTag="720813630" Tag="67" IconVisible="True" HorizontalEdge="RightEdge" LeftMargin="265.0000" RightMargin="55.0000" TopMargin="33.0000" BottomMargin="115.0000" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="265.0000" Y="115.0000" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8281" Y="0.7770" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle5" ActionTag="-84607143" Tag="71" IconVisible="True" HorizontalEdge="LeftEdge" LeftMargin="54.9776" RightMargin="265.0224" TopMargin="106.8300" BottomMargin="41.1700" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="54.9776" Y="41.1700" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1718" Y="0.2782" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle6" ActionTag="998455894" Tag="72" IconVisible="True" HorizontalEdge="LeftEdge" LeftMargin="124.9776" RightMargin="195.0224" TopMargin="106.8300" BottomMargin="41.1700" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="124.9776" Y="41.1700" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3906" Y="0.2782" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle7" ActionTag="1731782331" Tag="73" IconVisible="True" HorizontalEdge="RightEdge" LeftMargin="194.9779" RightMargin="125.0221" TopMargin="106.8300" BottomMargin="41.1700" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="194.9779" Y="41.1700" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6093" Y="0.2782" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="puzzle8" ActionTag="-1131046223" Tag="74" IconVisible="True" HorizontalEdge="RightEdge" LeftMargin="264.9777" RightMargin="55.0223" TopMargin="106.8300" BottomMargin="41.1700" ctype="SingleNodeObjectData">
                        <Size X="0.0000" Y="0.0000" />
                        <AnchorPoint />
                        <Position X="264.9777" Y="41.1700" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8281" Y="0.2782" />
                        <PreSize X="0.0000" Y="0.0000" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="135.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="0.2377" />
                    <PreSize X="1.0000" Y="0.2606" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="npcRatingPanel" ActionTag="1355224079" Tag="656" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="16.0000" RightMargin="16.0000" TopMargin="438.0000" BottomMargin="80.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="288.0000" Y="50.0000" />
                <AnchorPoint />
                <Position X="16.0000" Y="80.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0500" Y="0.1408" />
                <PreSize X="0.9000" Y="0.0880" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="lowerPanel" ActionTag="589723072" VisibleForFrame="False" Tag="82" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-0.5440" RightMargin="0.5440" TopMargin="385.0000" BottomMargin="83.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="retryButton" ActionTag="270720421" Tag="88" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="148.4200" RightMargin="-39.4200" TopMargin="89.0000" BottomMargin="-91.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="181" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="211.0000" Y="102.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_7_0" ActionTag="1734584788" Tag="1045" IconVisible="False" LeftMargin="88.5000" RightMargin="27.5000" TopMargin="33.6343" BottomMargin="27.3657" FontSize="24" LabelText="retry" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="95.0000" Y="41.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="136.0000" Y="47.8657" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6445" Y="0.4693" />
                        <PreSize X="0.4502" Y="0.4020" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="226" G="106" B="12" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="253.9200" Y="-40.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7935" Y="-0.4000" />
                    <PreSize X="0.6594" Y="1.0200" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_result_retrybtn.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="exitButton" ActionTag="655637118" Tag="90" IconVisible="False" LeftMargin="29.5000" RightMargin="79.5000" TopMargin="89.0000" BottomMargin="-91.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="181" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="211.0000" Y="102.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_7" ActionTag="-1834660071" Tag="1044" IconVisible="False" LeftMargin="92.5000" RightMargin="33.5000" TopMargin="31.2569" BottomMargin="29.7431" FontSize="24" LabelText="home" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="85.0000" Y="41.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="135.0000" Y="50.2431" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6398" Y="0.4926" />
                        <PreSize X="0.4028" Y="0.4020" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="23" G="114" B="151" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="135.0000" Y="-40.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4219" Y="-0.4000" />
                    <PreSize X="0.6594" Y="1.0200" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_result_homebtn.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="shareButton" ActionTag="-1875896146" Tag="92" IconVisible="False" LeftMargin="-5.0000" RightMargin="225.0000" TopMargin="89.0000" BottomMargin="-91.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="70" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="100.0000" Y="102.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="45.0000" Y="-40.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1406" Y="-0.4000" />
                    <PreSize X="0.3125" Y="1.0200" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_result_sharebtn.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="159.4560" Y="183.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4983" Y="0.3222" />
                <PreSize X="1.0000" Y="0.1761" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="playerModelNode" ActionTag="1674547793" VisibleForFrame="False" Tag="46" IconVisible="True" VerticalEdge="TopEdge" LeftMargin="65.0000" RightMargin="255.0000" TopMargin="98.0000" BottomMargin="470.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="65.0000" Y="470.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2031" Y="0.8275" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="leaderboardPanel" ActionTag="1346148011" VisibleForFrame="False" Tag="98" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="BottomEdge" LeftMargin="0.0011" RightMargin="-0.0011" TopMargin="271.8400" BottomMargin="86.0000" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="210.1600" />
                <AnchorPoint />
                <Position X="0.0011" Y="86.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0000" Y="0.1514" />
                <PreSize X="1.0000" Y="0.3700" />
                <SingleColor A="255" R="54" G="104" B="197" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="newRecordParticle" ActionTag="27378682" Tag="110" IconVisible="True" VerticalEdge="TopEdge" LeftMargin="159.0000" RightMargin="161.0000" TopMargin="-20.0000" BottomMargin="588.0000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="159.0000" Y="588.0000" />
            <Scale ScaleX="0.7000" ScaleY="0.7000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4969" Y="1.0352" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_result_1.plist" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>