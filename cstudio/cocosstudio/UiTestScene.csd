<GameFile>
  <PropertyGroup Name="UiTestScene" Type="Scene" ID="c8864a38-77f4-41a7-b50d-e5a61840a2b7" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="25" Speed="1.0000">
        <Timeline ActionTag="1184183906" Property="FrameEvent">
          <EventFrame FrameIndex="25" Tween="False" Value="fireParticle1" />
        </Timeline>
      </Animation>
      <ObjectData Name="Scene" Tag="24" ctype="GameNodeObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="topPanel" ActionTag="-444242091" Tag="27" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="368.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="200.0000" />
            <AnchorPoint />
            <Position Y="368.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="94" G="92" B="123" />
            <PrePosition Y="0.6479" />
            <PreSize X="1.0000" Y="0.3521" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="testButton_0" ActionTag="-808128112" Tag="58" IconVisible="False" LeftMargin="119.9999" RightMargin="0.0001" TopMargin="166.0001" BottomMargin="201.9999" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="219.9999" Y="301.9999" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6875" Y="0.5317" />
            <PreSize X="0.6250" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="topLeftText" ActionTag="-729001911" Tag="25" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" RightMargin="198.0000" BottomMargin="545.0000" FontSize="20" LabelText="Dock Top Left" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="122.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="61.0000" Y="556.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1906" Y="0.9798" />
            <PreSize X="0.3812" Y="0.0405" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="topRightText" ActionTag="-1119198786" Tag="26" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="184.0000" BottomMargin="545.0000" FontSize="20" LabelText="Dock Top Right" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="136.0000" Y="23.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="252.0000" Y="556.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.7875" Y="0.9798" />
            <PreSize X="0.4250" Y="0.0405" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="testButton" ActionTag="1586747721" Tag="6" IconVisible="False" LeftMargin="121.9988" RightMargin="-1.9988" TopMargin="295.0002" BottomMargin="72.9998" TouchEnable="True" FontSize="14" ButtonText="Button" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="221.9988" Y="172.9998" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6937" Y="0.3046" />
            <PreSize X="0.6250" Y="0.3521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_3" ActionTag="137439120" Tag="124" IconVisible="False" LeftMargin="515.0169" RightMargin="-395.0168" TopMargin="37.4232" BottomMargin="330.5768" ctype="SpriteObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.3149" ScaleY="0.4721" />
            <Position X="577.9969" Y="424.9968" />
            <Scale ScaleX="0.5168" ScaleY="0.4371" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.8062" Y="0.7482" />
            <PreSize X="0.6250" Y="0.3521" />
            <FileData Type="Normal" Path="circle_mask.png" Plist="" />
            <BlendFunc Src="1" Dst="772" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_5" ActionTag="722412705" Tag="126" IconVisible="False" LeftMargin="308.0001" RightMargin="-188.0001" TopMargin="86.9988" BottomMargin="281.0012" ctype="SpriteObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="408.0001" Y="381.0012" />
            <Scale ScaleX="0.6100" ScaleY="0.5900" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.2750" Y="0.6708" />
            <PreSize X="0.6250" Y="0.3521" />
            <FileData Type="Normal" Path="circle_mask2.png" Plist="" />
            <BlendFunc Src="774" Dst="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="Panel_1" ActionTag="-444953479" Alpha="242" Tag="55" IconVisible="False" LeftMargin="-351.0000" RightMargin="471.0000" TopMargin="479.0000" BottomMargin="-111.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="242" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_5_0_0" ActionTag="-1140868770" Tag="128" IconVisible="False" LeftMargin="-50.0000" RightMargin="50.0000" TopMargin="50.0000" BottomMargin="-50.0000" ctype="SpriteObjectData">
                <Size X="200.0000" Y="200.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="50.0000" Y="50.0000" />
                <Scale ScaleX="0.3000" ScaleY="0.1512" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2500" Y="0.2500" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="circle_mask2.png" Plist="" />
                <BlendFunc Src="775" Dst="768" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-351.0000" Y="-111.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="229" G="229" B="229" />
            <PrePosition X="-1.0969" Y="-0.1954" />
            <PreSize X="0.6250" Y="0.3521" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_5_0" ActionTag="1585714741" Tag="127" IconVisible="False" LeftMargin="293.3610" RightMargin="-173.3610" TopMargin="377.1428" BottomMargin="-9.1428" ctype="SpriteObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_4" ActionTag="1120291878" Alpha="52" Tag="125" IconVisible="False" LeftMargin="270.4401" RightMargin="-270.4401" TopMargin="72.7928" BottomMargin="-72.7928" ctype="SpriteObjectData">
                <Size X="200.0000" Y="200.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="370.4401" Y="27.2072" />
                <Scale ScaleX="1.1100" ScaleY="1.6800" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.8522" Y="0.1360" />
                <PreSize X="1.0000" Y="1.0000" />
                <FileData Type="Normal" Path="transparent.png" Plist="" />
                <BlendFunc Src="774" Dst="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="393.3610" Y="90.8572" />
            <Scale ScaleX="0.4500" ScaleY="0.3612" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.2293" Y="0.1600" />
            <PreSize X="0.6250" Y="0.3521" />
            <FileData Type="Normal" Path="circle_mask2.png" Plist="" />
            <BlendFunc Src="774" Dst="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_1" ActionTag="868786100" Tag="86" IconVisible="True" LeftMargin="147.9978" RightMargin="172.0022" TopMargin="150.5023" BottomMargin="417.4977" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="147.9978" Y="417.4977" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4625" Y="0.7350" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/star.plist" Plist="" />
            <BlendFunc Src="1" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="star_1" ActionTag="-1844820761" Tag="49" RotationSkewX="125.9900" RotationSkewY="168.0042" IconVisible="False" LeftMargin="93.9985" RightMargin="192.0015" TopMargin="323.0002" BottomMargin="210.9998" ctype="SpriteObjectData">
            <Size X="34.0000" Y="34.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="110.9985" Y="227.9998" />
            <Scale ScaleX="-2.4322" ScaleY="0.8894" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3469" Y="0.4014" />
            <PreSize X="0.1063" Y="0.0599" />
            <FileData Type="Normal" Path="star_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="testListView" ActionTag="-1119427010" Tag="127" IconVisible="False" LeftMargin="23.6911" RightMargin="96.3089" TopMargin="132.0771" BottomMargin="235.9229" TouchEnable="True" ClipAble="False" BackColorAlpha="159" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="200.0000" Y="200.0000" />
            <AnchorPoint />
            <Position X="23.6911" Y="235.9229" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0740" Y="0.4154" />
            <PreSize X="0.6250" Y="0.3521" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="titleFileNode" ActionTag="681554933" Tag="121" IconVisible="True" LeftMargin="143.0000" RightMargin="177.0000" TopMargin="472.0000" BottomMargin="96.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="143.0000" Y="96.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4469" Y="0.1690" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="gui/AnimateTitle.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Particle_2" ActionTag="1184183906" Tag="220" RotationSkewX="231.2661" RotationSkewY="204.2705" IconVisible="True" LeftMargin="63.4751" RightMargin="256.5249" TopMargin="298.0834" BottomMargin="269.9166" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="63.4751" Y="269.9166" />
            <Scale ScaleX="2.9725" ScaleY="1.9361" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1984" Y="0.4752" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Default" Path="Default/defaultParticle.plist" Plist="" />
            <BlendFunc Src="775" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="BitmapFontLabel_1" ActionTag="1203525234" Tag="20" IconVisible="False" LeftMargin="-96.5000" RightMargin="181.5000" TopMargin="359.9096" BottomMargin="144.0904" LabelText="1234567" ctype="TextBMFontObjectData">
            <Size X="235.0000" Y="64.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="21.0000" Y="176.0904" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0656" Y="0.3100" />
            <PreSize X="0.7344" Y="0.1127" />
            <LabelBMFontFile_CNB Type="Normal" Path="scoreFont.fnt" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>