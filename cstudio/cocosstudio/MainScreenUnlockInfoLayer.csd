<GameFile>
  <PropertyGroup Name="MainScreenUnlockInfoLayer" Type="Layer" ID="e3fe50dd-6332-498c-8477-2742da6729ae" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="331" ctype="GameLayerObjectData">
        <Size X="271.0000" Y="65.0000" />
        <Children>
          <AbstractNodeData Name="unlockInfoPanel" ActionTag="1660983059" Tag="366" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="21.6800" RightMargin="21.6800" TopMargin="7.5000" BottomMargin="7.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="227.6400" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_2" ActionTag="-967997668" Tag="367" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-159.1800" RightMargin="-159.1800" TopMargin="-36.5000" BottomMargin="-38.5000" Scale9Enable="True" LeftEage="180" RightEage="180" TopEage="41" BottomEage="41" Scale9OriginX="180" Scale9OriginY="41" Scale9Width="186" Scale9Height="45" ctype="ImageViewObjectData">
                <Size X="546.0000" Y="125.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.8200" Y="24.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4800" />
                <PreSize X="2.3985" Y="2.5000" />
                <FileData Type="Normal" Path="guiImage/ui_menu_mission.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogIconSprite" ActionTag="-1868921599" Tag="368" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-31.5000" RightMargin="162.1400" TopMargin="-24.5000" BottomMargin="-22.5000" ctype="SpriteObjectData">
                <Size X="97.0000" Y="97.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="17.0000" Y="26.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0747" Y="0.5200" />
                <PreSize X="0.4261" Y="1.9400" />
                <FileData Type="Normal" Path="dog/dselect_dog2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="infoText" ActionTag="-546530821" Tag="369" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="55.0000" RightMargin="-203.3600" TopMargin="-7.0000" BottomMargin="-7.0000" IsCustomSize="True" FontSize="25" LabelText="" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="376.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="55.0000" Y="25.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2416" Y="0.5000" />
                <PreSize X="1.6517" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="135.5000" Y="32.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8400" Y="0.7692" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>