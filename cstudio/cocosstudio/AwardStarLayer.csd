<GameFile>
  <PropertyGroup Name="AwardStarLayer" Type="Layer" ID="eaca0225-a41d-4f6b-8f53-8b9809616634" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="31" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="500.0000" />
        <Children>
          <AbstractNodeData Name="Panel_4" ActionTag="-1331868843" Tag="72" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="179" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="500.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="centerPanel" ActionTag="831106101" Tag="32" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="16.0000" RightMargin="16.0000" TopMargin="150.0000" BottomMargin="150.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="288.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="coinText" ActionTag="475042729" Tag="58" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="9.0064" RightMargin="28.9936" TopMargin="22.1000" BottomMargin="79.9000" IsCustomSize="True" FontSize="70" LabelText="600" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Bottom" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="250.0000" Y="98.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="134.0064" Y="128.9000" />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.4653" Y="0.6445" />
                <PreSize X="0.8681" Y="0.4900" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="77" G="77" B="77" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite_0" ActionTag="290295574" Tag="57" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="158.0002" RightMargin="95.9998" TopMargin="52.8150" BottomMargin="113.1850" ctype="SpriteObjectData">
                <Size X="34.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.4625" />
                <Position X="158.0002" Y="128.9100" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5486" Y="0.6446" />
                <PreSize X="0.1181" Y="0.1700" />
                <FileData Type="Normal" Path="star_icon.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-547290512" Tag="59" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="66.0000" RightMargin="66.0000" TopMargin="78.0000" BottomMargin="84.0000" FontSize="30" LabelText="Rewarded!" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="156.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="103.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="0" />
                <PrePosition X="0.5000" Y="0.5150" />
                <PreSize X="0.5417" Y="0.1900" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="-521109880" Tag="71" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-26.0000" RightMargin="-26.0000" TopMargin="124.0000" BottomMargin="-14.0000" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="310" Scale9Height="68" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="340.0000" Y="90.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="31.0000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1550" />
                <PreSize X="1.1806" Y="0.4500" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/btn_back_to_menu.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="250.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9000" Y="0.4000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="1450481967" Tag="55" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="17.9996" RightMargin="268.0004" TopMargin="8.8119" BottomMargin="457.1881" ctype="SpriteObjectData">
            <Size X="34.0000" Y="34.0000" />
            <AnchorPoint ScaleY="0.4625" />
            <Position X="17.9996" Y="472.9131" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0562" Y="0.9458" />
            <PreSize X="0.1063" Y="0.0680" />
            <FileData Type="Normal" Path="star_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coinText" ActionTag="-2106452273" Tag="56" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="42.9837" RightMargin="27.0163" TopMargin="-22.0000" BottomMargin="424.0000" IsCustomSize="True" FontSize="70" LabelText="878" VerticalAlignmentType="VT_Bottom" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="250.0000" Y="98.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="42.9837" Y="473.0000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.1343" Y="0.9460" />
            <PreSize X="0.7813" Y="0.1960" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="77" G="77" B="77" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>