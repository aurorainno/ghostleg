<GameFile>
  <PropertyGroup Name="CoinShopScene" Type="Layer" ID="02fbad9e-7c71-4480-91b4-09d8038393f5" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="1592602650" Tag="1604" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="39.2500" BottomMargin="-506.2500" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1035.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="528.7500" />
            <Scale ScaleX="0.5000" ScaleY="0.5500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9309" />
            <PreSize X="2.0000" Y="1.8222" />
            <FileData Type="Normal" Path="guiImage/ui_bg1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="restoreBtn" ActionTag="1827675640" Tag="82" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="302.0000" RightMargin="-282.0000" TopMargin="559.5000" BottomMargin="-66.5000" TouchEnable="True" FontSize="30" ButtonText="restore purchase" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="300.0000" Y="75.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="452.0000" Y="-29.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.4125" Y="-0.0511" />
            <PreSize X="0.9375" Y="0.1320" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/btn_dog_finalpay.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="subPanel" ActionTag="1237457084" VisibleForFrame="False" Tag="58" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="75.0000" BottomMargin="23.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="320.0000" Y="470.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="493.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8680" />
            <PreSize X="1.0000" Y="0.8275" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="320" Height="606" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="-627774849" VisibleForFrame="False" Tag="64" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="214.0000" RightMargin="-44.0000" TopMargin="6.0405" BottomMargin="501.9595" IsCustomSize="True" FontSize="30" LabelText="5" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="289.0000" Y="531.9595" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9031" Y="0.9365" />
            <PreSize X="0.4688" Y="0.1056" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="-2123886294" Tag="12" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="113.7930" RightMargin="124.2070" TopMargin="54.4170" BottomMargin="470.5830" FontSize="24" LabelText="Shop" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="81.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5635" ScaleY="0.4045" />
            <Position X="160.0000" Y="487.9765" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8591" />
            <PreSize X="0.2562" Y="0.0757" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="23" G="114" B="151" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="-351459290" VisibleForFrame="False" Tag="69" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="245.9787" RightMargin="30.0213" TopMargin="11.8924" BottomMargin="516.1076" ctype="SpriteObjectData">
            <Size X="44.0000" Y="40.0000" />
            <AnchorPoint ScaleY="0.4625" />
            <Position X="245.9787" Y="534.6076" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7687" Y="0.9412" />
            <PreSize X="0.1375" Y="0.0704" />
            <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="iconCandyStick" ActionTag="-1066367166" VisibleForFrame="False" Tag="79" IconVisible="False" LeftMargin="241.6405" RightMargin="45.3595" TopMargin="27.5719" BottomMargin="480.4281" ctype="SpriteObjectData">
            <Size X="33.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="258.1405" Y="510.4281" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8067" Y="0.8986" />
            <PreSize X="0.1031" Y="0.1056" />
            <FileData Type="Normal" Path="guiImage/item_candystick.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCandyText" ActionTag="-1401499859" VisibleForFrame="False" Tag="80" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="205.7900" RightMargin="-35.7900" TopMargin="27.0231" BottomMargin="480.9769" IsCustomSize="True" FontSize="30" LabelText="5" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.6010" ScaleY="0.3354" />
            <Position X="295.9400" Y="501.1009" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9248" Y="0.8822" />
            <PreSize X="0.4688" Y="0.1056" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="iconDiamond" ActionTag="1969571277" VisibleForFrame="False" Tag="42" IconVisible="False" LeftMargin="232.6367" RightMargin="35.3633" TopMargin="35.6100" BottomMargin="487.3900" ctype="SpriteObjectData">
            <Size X="52.0000" Y="45.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="258.6367" Y="509.8900" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8082" Y="0.8977" />
            <PreSize X="0.1625" Y="0.0792" />
            <FileData Type="Normal" Path="guiImage/diamond.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalDiamondText" ActionTag="-111292411" VisibleForFrame="False" Tag="43" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="214.0000" RightMargin="-44.0000" TopMargin="28.7800" BottomMargin="479.2200" IsCustomSize="True" FontSize="30" LabelText="5" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="150.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="289.0000" Y="509.2200" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9031" Y="0.8965" />
            <PreSize X="0.4688" Y="0.1056" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="adFreeHint" ActionTag="85905971" VisibleForFrame="False" Tag="93" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-79.5000" RightMargin="-79.5000" TopMargin="105.5000" BottomMargin="437.5000" FontSize="18" LabelText="ad free while purchase the $4.99 diamond" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="479.0000" Y="25.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="450.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.7923" />
            <PreSize X="1.4969" Y="0.0440" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="topPanel" ActionTag="981065355" Tag="339" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" BottomMargin="511.2000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="56.8000" />
            <Children>
              <AbstractNodeData Name="ui_main_topbar_1" ActionTag="-557921869" Tag="1603" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="-35.2000" ctype="SpriteObjectData">
                <Size X="640.0000" Y="92.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="56.8000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="2.0000" Y="1.6197" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbBtn" ActionTag="-410843078" VisibleForFrame="False" Tag="330" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-35.0650" RightMargin="163.0650" TopMargin="-2.0000" BottomMargin="1.8000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="192.0000" Y="57.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="60.9350" Y="30.3000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1904" Y="0.5335" />
                <PreSize X="0.6000" Y="1.0035" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_fb.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="profilePicFrame" ActionTag="1317129291" Tag="332" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="15.9350" RightMargin="252.0650" TopMargin="-0.2800" BottomMargin="5.0800" ctype="SpriteObjectData">
                <Size X="52.0000" Y="52.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="15.9350" Y="31.0800" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0498" Y="0.5472" />
                <PreSize X="0.1625" Y="0.9155" />
                <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbName" ActionTag="-1039263471" Tag="333" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="56.0000" RightMargin="75.0000" TopMargin="11.0000" BottomMargin="11.8000" FontSize="20" LabelText="CCcccccccccccc" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="189.0000" Y="34.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="56.0000" Y="28.8000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1750" Y="0.5070" />
                <PreSize X="0.5906" Y="0.5986" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="182" G="88" B="14" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="1790935509" Tag="335" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="11.8575" ctype="SpriteObjectData">
                <Size X="166.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="185.1505" Y="30.3575" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5786" Y="0.5345" />
                <PreSize X="0.5188" Y="0.7042" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalCoinText" ActionTag="-1108530704" Tag="334" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="7.6674" BottomMargin="9.1326" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="100.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="214.3221" Y="29.1326" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6698" Y="0.5129" />
                <PreSize X="0.3125" Y="0.7042" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondPanel" ActionTag="-1785493611" Tag="336" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="17.8100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="120.0000" Y="25.0000" />
                <Children>
                  <AbstractNodeData Name="sprite" ActionTag="2073116676" Tag="337" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                    <Size X="162.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                    <Position X="60.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.3500" Y="1.6000" />
                    <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="totalDiamondText" ActionTag="789561180" Tag="338" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="120.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="90.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7500" Y="0.5000" />
                    <PreSize X="1.0000" Y="1.6000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="0" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="1.0000" />
                <Position X="335.0000" Y="17.8100" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0469" Y="0.3136" />
                <PreSize X="0.3750" Y="0.4401" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="0.1000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondBar" ActionTag="1879372752" Tag="1605" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-0.0014" RightMargin="165.0014" TopMargin="107.9147" BottomMargin="430.0853" ctype="SpriteObjectData">
            <Size X="155.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="Text_22" ActionTag="-1570559431" Tag="1606" IconVisible="False" LeftMargin="30.0000" RightMargin="25.0000" TopMargin="5.0000" BottomMargin="3.0000" FontSize="16" LabelText="diamonds" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="100.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.0000" Y="14.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5161" Y="0.4667" />
                <PreSize X="0.6452" Y="0.7333" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="-0.0014" Y="460.0853" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="0.8100" />
            <PreSize X="0.4844" Y="0.0528" />
            <FileData Type="Normal" Path="guiImage/ui_character_header.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="CoinsBar" ActionTag="-1096472349" Tag="1607" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" RightMargin="165.0000" TopMargin="273.0000" BottomMargin="265.0000" ctype="SpriteObjectData">
            <Size X="155.0000" Y="30.0000" />
            <Children>
              <AbstractNodeData Name="Text_22" ActionTag="-1649720721" Tag="1608" IconVisible="False" LeftMargin="50.5000" RightMargin="45.5000" TopMargin="5.0000" BottomMargin="3.0000" FontSize="16" LabelText="coins" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="59.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.0000" Y="14.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5161" Y="0.4667" />
                <PreSize X="0.3806" Y="0.7333" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="295.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5194" />
            <PreSize X="0.4844" Y="0.0528" />
            <FileData Type="Normal" Path="guiImage/ui_character_header.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond1_node" ActionTag="-1717915973" Tag="397" IconVisible="True" LeftMargin="11.0000" RightMargin="309.0000" TopMargin="268.0000" BottomMargin="300.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="11.0000" Y="300.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0344" Y="0.5282" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond2_node" ActionTag="-742555457" Tag="1078" IconVisible="True" LeftMargin="113.0000" RightMargin="207.0000" TopMargin="268.0000" BottomMargin="300.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="113.0000" Y="300.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3531" Y="0.5282" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamond3_node" ActionTag="-1977581101" Tag="1079" IconVisible="True" LeftMargin="214.0000" RightMargin="106.0000" TopMargin="268.0000" BottomMargin="300.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="214.0000" Y="300.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6687" Y="0.5282" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="freeStar_node" ActionTag="-1567653254" Tag="800" IconVisible="True" LeftMargin="11.0000" RightMargin="309.0000" TopMargin="428.0000" BottomMargin="140.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="11.0000" Y="140.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0344" Y="0.2465" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="star1_node" ActionTag="2117880236" Tag="1081" IconVisible="True" LeftMargin="113.0000" RightMargin="207.0000" TopMargin="428.0000" BottomMargin="140.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="113.0000" Y="140.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3531" Y="0.2465" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="star2_node" ActionTag="-1846863051" Tag="1082" IconVisible="True" LeftMargin="214.0000" RightMargin="106.0000" TopMargin="428.0000" BottomMargin="140.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="214.0000" Y="140.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6687" Y="0.2465" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="star3_node" ActionTag="-1774761346" Tag="1080" IconVisible="True" LeftMargin="11.0000" RightMargin="309.0000" TopMargin="568.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="11.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0344" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode1" ActionTag="693743934" VisibleForFrame="False" Tag="398" IconVisible="True" LeftMargin="-15.0000" RightMargin="241.0000" TopMargin="155.0000" BottomMargin="280.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="94.0000" Y="133.0000" />
            <AnchorPoint />
            <Position X="-15.0000" Y="280.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0469" Y="0.4930" />
            <PreSize X="0.2937" Y="0.2342" />
            <FileData Type="Normal" Path="CoinShopItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode2" ActionTag="1368530314" VisibleForFrame="False" Tag="423" IconVisible="True" LeftMargin="85.0000" RightMargin="141.0000" TopMargin="155.0000" BottomMargin="280.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="94.0000" Y="133.0000" />
            <AnchorPoint />
            <Position X="85.0000" Y="280.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2656" Y="0.4930" />
            <PreSize X="0.2937" Y="0.2342" />
            <FileData Type="Normal" Path="CoinShopItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode3" ActionTag="-635508825" VisibleForFrame="False" Tag="448" IconVisible="True" LeftMargin="185.0000" RightMargin="41.0000" TopMargin="155.0000" BottomMargin="280.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="94.0000" Y="133.0000" />
            <AnchorPoint />
            <Position X="185.0000" Y="280.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5781" Y="0.4930" />
            <PreSize X="0.2937" Y="0.2342" />
            <FileData Type="Normal" Path="CoinShopItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode4" ActionTag="927446902" VisibleForFrame="False" Tag="498" IconVisible="True" LeftMargin="-15.0000" RightMargin="241.0000" TopMargin="325.0000" BottomMargin="110.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="94.0000" Y="133.0000" />
            <AnchorPoint />
            <Position X="-15.0000" Y="110.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0469" Y="0.1937" />
            <PreSize X="0.2937" Y="0.2342" />
            <FileData Type="Normal" Path="CoinShopItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode5" ActionTag="-642235119" VisibleForFrame="False" Tag="523" IconVisible="True" LeftMargin="85.0000" RightMargin="141.0000" TopMargin="325.0000" BottomMargin="110.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="94.0000" Y="133.0000" />
            <AnchorPoint />
            <Position X="85.0000" Y="110.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2656" Y="0.1937" />
            <PreSize X="0.2937" Y="0.2342" />
            <FileData Type="Normal" Path="CoinShopItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="testNode6" ActionTag="1486185278" VisibleForFrame="False" Tag="548" IconVisible="True" LeftMargin="185.0000" RightMargin="41.0000" TopMargin="325.0000" BottomMargin="110.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
            <Size X="94.0000" Y="133.0000" />
            <AnchorPoint />
            <Position X="185.0000" Y="110.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5781" Y="0.1937" />
            <PreSize X="0.2937" Y="0.2342" />
            <FileData Type="Normal" Path="CoinShopItemView.csd" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="err_msg" Visible="False" ActionTag="-916459246" Tag="47" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="48.0000" RightMargin="48.0000" TopMargin="222.5000" BottomMargin="222.5000" FontSize="32" LabelText="Sorry,shop &#xA;is not&#xA;available now" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="224.0000" Y="123.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7000" Y="0.2165" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="backBtn" ActionTag="-1023108875" Tag="92" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-26.3360" RightMargin="210.3360" TopMargin="34.0803" BottomMargin="448.9197" TouchEnable="True" FontSize="36" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="136.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="41.6640" Y="491.4197" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1302" Y="0.8652" />
            <PreSize X="0.4250" Y="0.1496" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="AlertLayer" ActionTag="738498262" Tag="41" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="320.0000" RightMargin="-320.0000" TopMargin="1.0224" BottomMargin="-1.0224" TouchEnable="True" ClipAble="False" BackColorAlpha="191" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-2142243792" Tag="1420" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-121.5000" RightMargin="-121.5000" TopMargin="-87.4529" BottomMargin="-74.5471" LeftEage="198" RightEage="198" TopEage="297" BottomEage="297" Scale9OriginX="198" Scale9OriginY="297" Scale9Width="167" Scale9Height="136" ctype="ImageViewObjectData">
                <Size X="563.0000" Y="730.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="290.4529" />
                <Scale ScaleX="0.5400" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5114" />
                <PreSize X="1.7594" Y="1.2852" />
                <FileData Type="Normal" Path="guiImage/ui_character_statsinfo.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="title" ActionTag="-154673686" Tag="42" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="19.0000" RightMargin="19.0000" TopMargin="228.4200" BottomMargin="296.5800" FontSize="24" LabelText="Purchase Success" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="282.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="318.0800" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5600" />
                <PreSize X="0.8813" Y="0.0757" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="23" G="114" B="151" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="info" ActionTag="-546846438" VisibleForFrame="False" Tag="44" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="18.5000" RightMargin="18.5000" TopMargin="217.1400" BottomMargin="239.8600" FontSize="32" LabelText="Shop not available&#xA;Shop not available&#xA;Shop not available" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="283.0000" Y="111.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="295.3600" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5200" />
                <PreSize X="0.8844" Y="0.1954" />
                <FontResource Type="Normal" Path="CaviarDreams.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="-537898826" Tag="45" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-16.5000" RightMargin="-16.5000" TopMargin="265.4000" BottomMargin="208.6000" TouchEnable="True" FontSize="50" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="Text" ActionTag="-329196821" Tag="1419" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="81.5000" RightMargin="81.5000" TopMargin="36.2600" BottomMargin="28.7400" FontSize="18" LabelText="go back to shop" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="190.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="43.2400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.4600" />
                    <PreSize X="0.5382" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="255.6000" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4500" />
                <PreSize X="1.1031" Y="0.1655" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="60" G="60" B="96" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="buyStarPanel" ActionTag="-768641396" VisibleForFrame="False" Tag="53" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="237.6400" BottomMargin="260.3600" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="70.0000" />
                <Children>
                  <AbstractNodeData Name="amount" ActionTag="-635998754" Tag="54" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="9.0000" RightMargin="109.0000" TopMargin="16.5000" BottomMargin="16.5000" FontSize="32" LabelText="3000" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="82.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="35.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2500" Y="0.5000" />
                    <PreSize X="0.4100" Y="0.5286" />
                    <FontResource Type="Normal" Path="CaviarDreams.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="iconSprite" ActionTag="-500332997" Tag="55" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="76.7600" RightMargin="79.2400" TopMargin="11.6925" BottomMargin="18.3075" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="40.0000" />
                    <AnchorPoint ScaleY="0.4625" />
                    <Position X="76.7600" Y="36.8075" />
                    <Scale ScaleX="0.6000" ScaleY="0.6000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3838" Y="0.5258" />
                    <PreSize X="0.2200" Y="0.5714" />
                    <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="text" ActionTag="2024909281" Tag="56" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="73.6400" RightMargin="-7.6400" TopMargin="15.8000" BottomMargin="17.2000" FontSize="32" LabelText="is added" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="134.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="140.6400" Y="35.7000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7032" Y="0.5100" />
                    <PreSize X="0.6700" Y="0.5286" />
                    <FontResource Type="Normal" Path="CaviarDreams.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="295.3600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5200" />
                <PreSize X="0.6250" Y="0.1232" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="320.0000" Y="282.9776" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0000" Y="0.4982" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ProgressLayer" Visible="False" ActionTag="-1548351082" Tag="37" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="186" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="message" ActionTag="-1951146980" Tag="38" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="34.0000" RightMargin="34.0000" TopMargin="248.0000" BottomMargin="248.0000" FontSize="26" LabelText="Loading &#xA;Product data..." HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="252.0000" Y="72.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="284.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.7875" Y="0.1268" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>