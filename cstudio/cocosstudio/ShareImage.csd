<GameFile>
  <PropertyGroup Name="ShareImage" Type="Layer" ID="a2cc63f6-f22d-42de-9821-45e44c813782" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="59" ctype="GameLayerObjectData">
        <Size X="600.0000" Y="600.0000" />
        <Children>
          <AbstractNodeData Name="Sprite_1" ActionTag="-351161237" Tag="2031" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-300.0000" RightMargin="-300.0000" TopMargin="-300.0000" BottomMargin="-300.0000" ctype="SpriteObjectData">
            <Size X="1200.0000" Y="1200.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="300.0000" Y="300.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.0000" />
            <FileData Type="Normal" Path="guiImage/ui_share_01.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="scoreText" ActionTag="2120749804" Tag="70" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="96.5000" RightMargin="96.5000" TopMargin="421.0000" BottomMargin="41.0000" FontSize="100" LabelText="23290" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="407.0000" Y="138.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="300.0000" Y="110.0000" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1833" />
            <PreSize X="0.6783" Y="0.2300" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="stageText" ActionTag="101306464" Tag="2032" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="154.0000" RightMargin="154.0000" TopMargin="327.5000" BottomMargin="167.5000" FontSize="60" LabelText="stage 1" OutlineSize="11" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="292.0000" Y="105.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="300.0000" Y="220.0000" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.5000" Y="0.3667" />
            <PreSize X="0.4867" Y="0.1750" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>