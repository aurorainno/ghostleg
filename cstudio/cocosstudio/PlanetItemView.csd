<GameFile>
  <PropertyGroup Name="PlanetItemView" Type="Layer" ID="7f090e46-9afc-4c3d-b0a5-e1635855ece8" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="119" ctype="GameLayerObjectData">
        <Size X="160.0000" Y="160.0000" />
        <Children>
          <AbstractNodeData Name="mainPanel" ActionTag="-1561300882" Tag="22" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="160.0000" Y="160.0000" />
            <Children>
              <AbstractNodeData Name="planetNode" ActionTag="547278234" Tag="255" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="80.0000" BottomMargin="80.0000" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="referenceNode" ActionTag="1154758733" Tag="156" IconVisible="True" LeftMargin="-160.0000" RightMargin="160.0000" TopMargin="160.0000" BottomMargin="-160.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="0.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="-160.0000" Y="-160.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="80.0000" Y="80.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="namePanel" ActionTag="-1813654717" Alpha="200" Tag="106" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="100.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="160.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="stageText" ActionTag="1550911699" VisibleForFrame="False" Tag="107" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-45.0000" RightMargin="-45.0000" TopMargin="-2.0000" BottomMargin="22.0000" IsCustomSize="True" FontSize="22" LabelText="STAGE 1" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="250.0000" Y="40.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="80.0000" Y="42.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.7000" />
                    <PreSize X="1.5625" Y="0.6667" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="8" G="114" B="79" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="stageNameText" ActionTag="751562830" Tag="108" RotationSkewX="15.0000" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-103.9999" RightMargin="-104.0001" TopMargin="6.0000" BottomMargin="-6.0000" IsCustomSize="True" FontSize="30" LabelText="WWWWWWWWWW" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="368.0000" Y="60.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="80.0001" Y="24.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.4000" />
                    <PreSize X="2.3000" Y="1.0000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="8" G="114" B="79" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="0.3750" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetSprite" ActionTag="477476439" VisibleForFrame="False" Tag="193" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.0000" RightMargin="57.0000" TopMargin="57.0000" BottomMargin="57.0000" ctype="SpriteObjectData">
                <Size X="46.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.0000" Y="80.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.2875" Y="0.2875" />
                <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockPanel" ActionTag="-1427740604" VisibleForFrame="False" Tag="50" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-20.0000" RightMargin="-20.0000" TopMargin="140.0000" BottomMargin="-40.0000" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Enable="True" LeftEage="87" RightEage="87" TopEage="43" BottomEage="43" Scale9OriginX="87" Scale9OriginY="43" Scale9Width="92" Scale9Height="46" ctype="PanelObjectData">
                <Size X="200.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="infoText" ActionTag="1181676775" Tag="196" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-105.0000" RightMargin="-95.0000" TopMargin="-1.9980" BottomMargin="-58.0020" IsCustomSize="True" FontSize="26" LabelText="Collect 315 more star in a game to unlock Border Terrier and Jupiter" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="400.0000" Y="120.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="95.0000" Y="1.9980" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4750" Y="0.0333" />
                    <PreSize X="2.0000" Y="2.0000" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="80.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1250" />
                <PreSize X="1.2500" Y="0.3750" />
                <FileData Type="Normal" Path="guiImage/planet_board.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="lockPanel" Visible="False" ActionTag="-1868945725" VisibleForFrame="False" Tag="51" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-40.0000" RightMargin="-40.0000" TopMargin="140.0000" BottomMargin="-40.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="122" Scale9OriginY="37" Scale9Width="126" Scale9Height="40" ctype="PanelObjectData">
                <Size X="240.0000" Y="60.0000" />
                <Children>
                  <AbstractNodeData Name="infoText" ActionTag="-2003471628" Tag="52" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-105.0000" RightMargin="-105.0000" TopMargin="-2.0000" BottomMargin="-2.0000" FontSize="25" LabelText="Complete the SATURN team to unlock&#xA;0/5 members" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="450.0000" Y="64.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="120.0000" Y="30.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8750" Y="1.0667" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="planet_locked_7" ActionTag="1769066244" VisibleForFrame="False" Tag="54" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="64.0000" RightMargin="64.0000" TopMargin="-14.0000" BottomMargin="46.0000" ctype="SpriteObjectData">
                    <Size X="112.0000" Y="28.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_5" ActionTag="-1520162186" Tag="55" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-0.7516" RightMargin="-12.2484" TopMargin="-8.0004" BottomMargin="-9.9996" FontSize="36" LabelText="Locked" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="125.0000" Y="46.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="61.7484" Y="13.0004" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="165" B="0" />
                        <PrePosition X="0.5513" Y="0.4643" />
                        <PreSize X="1.1161" Y="1.6429" />
                        <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="120.0000" Y="60.0000" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.0000" />
                    <PreSize X="0.4667" Y="0.4667" />
                    <FileData Type="Normal" Path="guiImage/planet_locked.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="80.0000" Y="20.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1250" />
                <PreSize X="1.5000" Y="0.3750" />
                <FileData Type="Normal" Path="guiImage/planet_board_locked.png" Plist="" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="nameSprite" ActionTag="1502789494" VisibleForFrame="False" Tag="195" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-42.0000" RightMargin="-42.0000" TopMargin="-56.0000" BottomMargin="40.0000" ctype="SpriteObjectData">
                <Size X="244.0000" Y="176.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="80.0000" Y="128.0000" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8000" />
                <PreSize X="1.5250" Y="1.1000" />
                <FileData Type="Normal" Path="guiImage/planet_name.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="80.0000" Y="80.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="touchPanel" ActionTag="728863731" VisibleForFrame="False" Tag="13" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="41.0080" RightMargin="38.9920" TopMargin="10.0000" BottomMargin="90.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="80.0000" Y="60.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="81.0080" Y="120.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5063" Y="0.7500" />
            <PreSize X="0.5000" Y="0.3750" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>