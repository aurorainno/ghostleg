<GameFile>
  <PropertyGroup Name="ItemEffectIndicator" Type="Layer" ID="20f5d4ac-97ca-42b9-80b0-514c45a1bdb9" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="169" ctype="GameLayerObjectData">
        <Size X="40.0000" Y="40.0000" />
        <Children>
          <AbstractNodeData Name="icon" ActionTag="-374077002" Tag="183" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-18.0000" RightMargin="-18.0000" TopMargin="-18.0000" BottomMargin="-18.0000" ctype="SpriteObjectData">
            <Size X="76.0000" Y="76.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="20.0000" Y="20.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.9000" Y="1.9000" />
            <FileData Type="Normal" Path="guiImage/ingame_booster_4.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="coolDownNode" ActionTag="-1632442504" Tag="3287" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" RightMargin="40.0000" TopMargin="40.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>