<GameFile>
  <PropertyGroup Name="LeaderboardLayer" Type="Layer" ID="688a1b3c-6a16-4428-a1a2-8d8c38dd52ae" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="112" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgSprite" ActionTag="-1149672600" Tag="893" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="39.2500" BottomMargin="-506.2500" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1035.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="528.7500" />
            <Scale ScaleX="0.5000" ScaleY="0.5500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9309" />
            <PreSize X="2.0000" Y="1.8222" />
            <FileData Type="Normal" Path="guiImage/ui_bg1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="counterPanel" ActionTag="705434656" Tag="703" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" LeftMargin="-0.0010" RightMargin="0.0010" TopMargin="38.0000" BottomMargin="501.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="29.0000" />
            <Children>
              <AbstractNodeData Name="bgPanel" ActionTag="-2112369455" Tag="895" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="155" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="29.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="26" G="26" B="26" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_10" ActionTag="-43052244" Tag="896" IconVisible="False" HorizontalEdge="LeftEdge" LeftMargin="0.5000" RightMargin="294.5000" TopMargin="3.8102" BottomMargin="-5.8102" ctype="SpriteObjectData">
                <Size X="25.0000" Y="31.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="13.0000" Y="9.6898" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0406" Y="0.3341" />
                <PreSize X="0.0781" Y="1.0690" />
                <FileData Type="Normal" Path="guiImage/ui_main_alarm.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="rankingText" ActionTag="1201671767" Tag="705" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="25.0000" RightMargin="61.0000" TopMargin="8.3800" BottomMargin="-4.3800" FontSize="18" LabelText="Weekly Tournament:" OutlineSize="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="234.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="25.0000" Y="8.1200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0781" Y="0.2800" />
                <PreSize X="0.7312" Y="0.8621" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="124" G="83" B="4" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_22_0" ActionTag="-1760231510" Tag="707" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="243.0000" RightMargin="20.0000" TopMargin="8.3800" BottomMargin="-4.3800" FontSize="18" LabelText="LEFT!" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="57.0000" Y="25.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="300.0000" Y="8.1200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9375" Y="0.2800" />
                <PreSize X="0.1781" Y="0.8621" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="124" G="83" B="4" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="firstValue" ActionTag="-1400361060" VisibleForFrame="False" Tag="710" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="219.0000" RightMargin="85.0000" TopMargin="6.8800" BottomMargin="-5.8800" FontSize="20" LabelText="7" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="16.0000" Y="28.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="235.0000" Y="8.1200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7344" Y="0.2800" />
                <PreSize X="0.0500" Y="0.9655" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="124" G="83" B="4" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="firstUnit" ActionTag="-346898434" VisibleForFrame="False" Tag="709" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="240.0000" RightMargin="21.0000" TopMargin="8.3800" BottomMargin="-4.3800" FontSize="18" LabelText="days" OutlineSize="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="59.0000" Y="25.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="240.0000" Y="8.1200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.2800" />
                <PreSize X="0.1844" Y="0.8621" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="124" G="83" B="4" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="secondValue" ActionTag="-843183021" VisibleForFrame="False" Tag="1254" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="201.0000" RightMargin="52.0000" TopMargin="8.3800" BottomMargin="-4.3800" FontSize="18" LabelText="23:24" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="67.0000" Y="25.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="268.0000" Y="8.1200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8375" Y="0.2800" />
                <PreSize X="0.2094" Y="0.8621" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="124" G="83" B="4" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="-0.0010" Y="530.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0000" Y="0.9331" />
            <PreSize X="1.0000" Y="0.0511" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ui_main_topbar_1" ActionTag="1098406681" Tag="892" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="476.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="92.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="2.0000" Y="0.1620" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_2" ActionTag="-37484421" Tag="894" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="59.0000" RightMargin="59.0000" TopMargin="73.5000" BottomMargin="453.5000" FontSize="24" LabelText="leaderboard" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="202.0000" Y="41.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="474.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8345" />
            <PreSize X="0.6313" Y="0.0722" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="23" G="114" B="151" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="leaderboardPanel" ActionTag="1383400711" Tag="113" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="BottomEdge" TopMargin="147.6800" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="420.3200" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-43105653" Tag="3250" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" RightMargin="-320.0000" TopMargin="-479.6800" ctype="SpriteObjectData">
                <Size X="640.0000" Y="900.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="0.5000" ScaleY="0.4600" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="2.0000" Y="2.1412" />
                <FileData Type="Normal" Path="guiImage/ui_ranking_bg.png" Plist="" />
                <BlendFunc Src="770" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="friendUpperBorder" ActionTag="-8995223" Tag="117" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-6.3000" BottomMargin="416.6200" ctype="SpriteObjectData">
                <Size X="640.0000" Y="10.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="421.6200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0031" />
                <PreSize X="2.0000" Y="0.0238" />
                <FileData Type="Normal" Path="guiImage/ui_result_ranking_2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="nationalUpperBorder" ActionTag="-664972223" Tag="118" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-6.3399" BottomMargin="416.6599" ctype="SpriteObjectData">
                <Size X="640.0000" Y="10.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="421.6599" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0032" />
                <PreSize X="2.0000" Y="0.0238" />
                <FileData Type="Normal" Path="guiImage/ui_result_ranking_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="globalUpperBorder" ActionTag="-963995795" VisibleForFrame="False" Tag="119" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-6.3000" BottomMargin="416.6200" ctype="SpriteObjectData">
                <Size X="640.0000" Y="10.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="421.6200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.0031" />
                <PreSize X="2.0000" Y="0.0238" />
                <FileData Type="Normal" Path="guiImage/ui_result_ranking_3.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="friendBtn" ActionTag="1901660431" Tag="1505" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="24.3040" RightMargin="115.6960" TopMargin="-43.0243" BottomMargin="411.3443" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="30" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="114.3040" Y="437.3443" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3572" Y="1.0405" />
                <PreSize X="0.5625" Y="0.1237" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_result_friends_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nationalBtn" ActionTag="-1659297582" Tag="1506" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="126.6950" RightMargin="13.3050" TopMargin="-43.0243" BottomMargin="411.3443" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="30" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="216.6950" Y="437.3443" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6772" Y="1.0405" />
                <PreSize X="0.5625" Y="0.1237" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_result_national_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="globalBtn" ActionTag="-2017755292" VisibleForFrame="False" Tag="1507" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="179.4080" RightMargin="-39.4080" TopMargin="-73.6000" BottomMargin="441.9200" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="30" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="269.4080" Y="467.9200" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8419" Y="1.1132" />
                <PreSize X="0.5625" Y="0.1237" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_result_global_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="leaderboardListView" ActionTag="-1105673855" Tag="123" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="BottomEdge" TopMargin="126.0960" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" DirectionType="Vertical" HorizontalType="Align_Right" ctype="ListViewObjectData">
                <Size X="320.0000" Y="294.2240" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="160.0000" Y="294.2240" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7000" />
                <PreSize X="1.0000" Y="0.7000" />
                <SingleColor A="255" R="150" G="150" B="255" />
                <FirstColor A="255" R="150" G="150" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="loadingText" ActionTag="-218445956" Tag="353" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="79.0000" RightMargin="79.0000" TopMargin="242.8016" BottomMargin="133.5184" FontSize="26" LabelText="Loading..." OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="162.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="155.5184" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3700" />
                <PreSize X="0.5063" Y="0.1047" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planet_00Btn" ActionTag="2057391301" Tag="123" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-36.0015" RightMargin="230.0015" TopMargin="23.7981" BottomMargin="274.5219" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="26.9985" Y="335.5219" />
                <Scale ScaleX="0.4100" ScaleY="0.4100" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0844" Y="0.7983" />
                <PreSize X="0.3938" Y="0.2903" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_ladderboard_planet1_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planet_01Btn" ActionTag="2050427632" Tag="1200" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="16.9984" RightMargin="177.0016" TopMargin="23.7981" BottomMargin="274.5219" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="79.9984" Y="335.5219" />
                <Scale ScaleX="0.4100" ScaleY="0.4100" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2500" Y="0.7983" />
                <PreSize X="0.3938" Y="0.2903" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_ladderboard_planet1_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planet_02Btn" ActionTag="-1874448035" Tag="1201" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="69.9982" RightMargin="124.0018" TopMargin="23.7981" BottomMargin="274.5219" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="132.9982" Y="335.5219" />
                <Scale ScaleX="0.4100" ScaleY="0.4100" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4156" Y="0.7983" />
                <PreSize X="0.3938" Y="0.2903" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_ladderboard_planet1_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planet_03Btn" ActionTag="1211347420" Tag="1202" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="122.9981" RightMargin="71.0019" TopMargin="23.7981" BottomMargin="274.5219" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="185.9981" Y="335.5219" />
                <Scale ScaleX="0.4100" ScaleY="0.4100" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5812" Y="0.7983" />
                <PreSize X="0.3938" Y="0.2903" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_ladderboard_planet1_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planet_04Btn" ActionTag="1723931606" Tag="1203" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="175.9980" RightMargin="18.0020" TopMargin="23.7981" BottomMargin="274.5219" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="238.9980" Y="335.5219" />
                <Scale ScaleX="0.4100" ScaleY="0.4100" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7469" Y="0.7983" />
                <PreSize X="0.3938" Y="0.2903" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_ladderboard_planet1_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planet_05Btn" ActionTag="-1894595519" Tag="1204" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="228.9978" RightMargin="-34.9978" TopMargin="23.7981" BottomMargin="274.5219" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="100" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="126.0000" Y="122.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="291.9978" Y="335.5219" />
                <Scale ScaleX="0.4100" ScaleY="0.4100" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9125" Y="0.7983" />
                <PreSize X="0.3938" Y="0.2903" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_ladderboard_planet1_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bannerPanel" ActionTag="1211476242" Tag="1197" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="0.4000" BottomMargin="362.9200" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="57.0000" />
                <Children>
                  <AbstractNodeData Name="bannerSprite" ActionTag="1601177253" Tag="711" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-0.3161" BottomMargin="-58.6839" ctype="SpriteObjectData">
                    <Size X="640.0000" Y="116.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                    <Position X="160.0000" Y="57.3161" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="1.0055" />
                    <PreSize X="2.0000" Y="2.0351" />
                    <FileData Type="Normal" Path="guiImage/ui_leaderboard_rewardbanner_fb.png" Plist="" />
                    <BlendFunc Src="770" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bannerBtn" ActionTag="-1285256132" Tag="802" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="163.0000" RightMargin="-47.0000" TopMargin="-18.5000" BottomMargin="-18.5000" TouchEnable="True" FontSize="18" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="204.0000" Y="94.0000" />
                    <Children>
                      <AbstractNodeData Name="title" ActionTag="1571271441" Tag="803" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="57.5000" RightMargin="57.5000" TopMargin="36.3200" BottomMargin="30.6800" FontSize="15" LabelText="REWARDS" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="89.0000" Y="27.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="102.0000" Y="44.1800" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.4700" />
                        <PreSize X="0.4363" Y="0.2872" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="7" G="111" B="49" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="265.0000" Y="28.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8281" Y="0.5000" />
                    <PreSize X="0.6375" Y="1.6491" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bannerDescription" ActionTag="2108419576" Tag="1198" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="59.0000" RightMargin="-39.0000" TopMargin="-9.7900" BottomMargin="-13.2100" IsCustomSize="True" FontSize="18" LabelText="CHALLENGE LOCAL PLAYERS&#xA;AND GET REWARDS&#xA;123123123" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="300.0000" Y="80.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="59.0000" Y="26.7900" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="59" G="59" B="59" />
                    <PrePosition X="0.1844" Y="0.4700" />
                    <PreSize X="0.9375" Y="1.4035" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="diamondSprite" ActionTag="-1294811115" VisibleForFrame="False" Tag="1199" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="87.5600" RightMargin="180.4400" TopMargin="13.4157" BottomMargin="-1.4157" ctype="SpriteObjectData">
                    <Size X="52.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="113.5600" Y="21.0843" />
                    <Scale ScaleX="0.3500" ScaleY="0.3500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3549" Y="0.3699" />
                    <PreSize X="0.1625" Y="0.7895" />
                    <FileData Type="Normal" Path="guiImage/ui_general_diamond.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bannerFbBtn" Visible="False" ActionTag="-2144480318" Tag="1255" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="171.6900" RightMargin="-41.6900" TopMargin="-0.5000" BottomMargin="-0.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="190.0000" Y="58.0000" />
                    <Children>
                      <AbstractNodeData Name="Text_1" ActionTag="440074178" Tag="1256" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="19.0000" TopMargin="14.7400" BottomMargin="11.2600" FontSize="20" LabelText="CONNECT" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="108.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="117.0000" Y="27.2600" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6158" Y="0.4700" />
                        <PreSize X="0.5684" Y="0.5517" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="44" B="98" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="266.6900" Y="28.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8334" Y="0.5000" />
                    <PreSize X="0.5938" Y="1.0175" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_main_fbconnect.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="419.9200" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.9990" />
                <PreSize X="1.0000" Y="0.1356" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.7400" />
            <SingleColor A="255" R="54" G="104" B="197" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="cancelBtn" ActionTag="1912673594" Tag="1509" IconVisible="False" LeftMargin="-28.9969" RightMargin="212.9969" TopMargin="49.5000" BottomMargin="433.5000" TouchEnable="True" FontSize="18" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="136.0000" Y="85.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="39.0031" Y="476.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1219" Y="0.8380" />
            <PreSize X="0.4250" Y="0.1496" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondPanel" ActionTag="664025216" Tag="629" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="529.0100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="25.0000" />
            <Children>
              <AbstractNodeData Name="sprite" ActionTag="1847764059" Tag="630" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="162.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="60.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.3500" Y="1.6000" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalDiamondText" ActionTag="-1307533213" Tag="631" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="120.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="90.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.5000" />
                <PreSize X="1.0000" Y="1.6000" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" />
            <Position X="335.0000" Y="529.0100" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0469" Y="0.9314" />
            <PreSize X="0.3750" Y="0.0440" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="-401676686" Tag="658" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="523.0575" ctype="SpriteObjectData">
            <Size X="166.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="185.1505" Y="541.5575" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5786" Y="0.9534" />
            <PreSize X="0.5188" Y="0.0704" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="284350540" Tag="659" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="7.6674" BottomMargin="520.3326" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="214.3221" Y="540.3326" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6698" Y="0.9513" />
            <PreSize X="0.3125" Y="0.0704" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="1" G="44" B="98" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbName" ActionTag="139414096" Tag="673" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="56.0000" RightMargin="231.0000" TopMargin="11.0000" BottomMargin="523.0000" FontSize="20" LabelText="CC" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="33.0000" Y="34.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="56.0000" Y="540.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1750" Y="0.9507" />
            <PreSize X="0.1031" Y="0.0599" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="182" G="88" B="14" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="profilePicFrame" ActionTag="42910623" Tag="674" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="15.9350" RightMargin="252.0650" TopMargin="-0.2800" BottomMargin="516.2800" ctype="SpriteObjectData">
            <Size X="52.0000" Y="52.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="15.9350" Y="542.2800" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0498" Y="0.9547" />
            <PreSize X="0.1625" Y="0.0915" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbBtn" ActionTag="-1618167196" VisibleForFrame="False" Tag="675" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-35.0650" RightMargin="163.0650" TopMargin="-2.0000" BottomMargin="513.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="192.0000" Y="57.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="267273374" Tag="676" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="21.0000" TopMargin="14.2100" BottomMargin="10.7900" FontSize="20" LabelText="CONNECT" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="117.0000" Y="26.7900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6094" Y="0.4700" />
                <PreSize X="0.5625" Y="0.5614" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.9350" Y="541.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1904" Y="0.9533" />
            <PreSize X="0.6000" Y="0.1004" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_fb.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>