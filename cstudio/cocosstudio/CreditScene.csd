<GameFile>
  <PropertyGroup Name="CreditScene" Type="Scene" ID="b7e7e713-2a1e-498d-a878-914546b2584b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <AnimationList>
        <AnimationInfo Name="showSecret" StartIndex="0" EndIndex="35">
          <RenderColor A="255" R="244" G="164" B="96" />
        </AnimationInfo>
        <AnimationInfo Name="hideSecret" StartIndex="45" EndIndex="80">
          <RenderColor A="255" R="0" G="255" B="255" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Scene" Tag="42" ctype="GameNodeObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgSprite" ActionTag="-722591898" Tag="3100" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="39.2500" BottomMargin="-506.2500" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1035.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
            <Position X="160.0000" Y="528.7500" />
            <Scale ScaleX="0.5000" ScaleY="0.5500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9309" />
            <PreSize X="2.0000" Y="1.8222" />
            <FileData Type="Normal" Path="guiImage/ui_bg1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="ui_main_topbar_1" ActionTag="-921813927" Tag="3099" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="476.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="92.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="2.0000" Y="0.1620" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbBtn" ActionTag="-1454863916" VisibleForFrame="False" Tag="3101" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-34.0650" RightMargin="164.0650" TopMargin="-2.5000" BottomMargin="512.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="160" Scale9Height="36" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="190.0000" Y="58.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="-1673517640" Tag="3102" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="19.0000" TopMargin="14.7400" BottomMargin="11.2600" FontSize="20" LabelText="CONNECT" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="108.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="117.0000" Y="27.2600" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6158" Y="0.4700" />
                <PreSize X="0.5684" Y="0.5517" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.9350" Y="541.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1904" Y="0.9533" />
            <PreSize X="0.5938" Y="0.1021" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/ui_main_fbconnect.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="profilePicFrame" ActionTag="230435639" Tag="3103" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="15.9350" RightMargin="252.0650" TopMargin="-0.2800" BottomMargin="516.2800" ctype="SpriteObjectData">
            <Size X="52.0000" Y="52.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="15.9350" Y="542.2800" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0498" Y="0.9547" />
            <PreSize X="0.1625" Y="0.0915" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbName" ActionTag="200841885" Tag="3104" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="56.0000" RightMargin="64.0000" TopMargin="8.0000" BottomMargin="520.0000" IsCustomSize="True" FontSize="20" LabelText="CC" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="200.0000" Y="40.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="56.0000" Y="540.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1750" Y="0.9507" />
            <PreSize X="0.6250" Y="0.0704" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="182" G="88" B="14" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="-192764430" Tag="3106" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="523.0575" ctype="SpriteObjectData">
            <Size X="166.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="185.1505" Y="541.5575" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5786" Y="0.9534" />
            <PreSize X="0.5188" Y="0.0704" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="265978064" Tag="3105" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="6.6700" BottomMargin="521.3300" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="214.3221" Y="541.3300" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6698" Y="0.9530" />
            <PreSize X="0.3125" Y="0.0704" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondPanel" ActionTag="478537487" Tag="3107" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="529.0100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="25.0000" />
            <Children>
              <AbstractNodeData Name="sprite" ActionTag="1996679614" Tag="3108" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="162.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="60.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.3500" Y="1.6000" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalDiamondText" ActionTag="261933353" Tag="3109" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="120.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="90.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.5000" />
                <PreSize X="1.0000" Y="1.6000" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" />
            <Position X="335.0000" Y="529.0100" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0469" Y="0.9314" />
            <PreSize X="0.3750" Y="0.0440" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-525841022" Tag="43" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-0.2979" RightMargin="0.2979" TopMargin="-0.2980" BottomMargin="0.2979" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="backBtn" ActionTag="-296026785" Tag="855" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-18.8800" RightMargin="202.8800" TopMargin="37.5000" BottomMargin="445.5000" TouchEnable="True" FontSize="36" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="136.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.1200" Y="488.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1535" Y="0.8592" />
                <PreSize X="0.4250" Y="0.1496" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="textTitle" ActionTag="-1553590465" Tag="59" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="99.0000" RightMargin="99.0000" TopMargin="59.0000" BottomMargin="466.0000" FontSize="24" LabelText="credits" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="122.0000" Y="43.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="487.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8583" />
                <PreSize X="0.3812" Y="0.0757" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="23" G="114" B="151" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="versionBtn" ActionTag="-615821276" Tag="71" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="474.0000" BottomMargin="14.0000" TouchEnable="True" FontSize="25" ButtonText="version 1.0.14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="5" BottomEage="5" Scale9OriginX="15" Scale9OriginY="5" Scale9Width="129" Scale9Height="19" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="300.0000" Y="80.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" Y="14.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0246" />
                <PreSize X="0.9375" Y="0.1408" />
                <FontResource Type="Normal" Path="CaviarDreams.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="designPanel" ActionTag="-1062138456" Tag="55" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="95.0000" BottomMargin="443.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="-122" Scale9OriginY="-37" Scale9Width="244" Scale9Height="74" ctype="PanelObjectData">
                <Size X="320.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="heading" ActionTag="-10018402" Tag="61" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="97.0000" RightMargin="97.0000" TopMargin="-3.9430" BottomMargin="-13.0570" FontSize="28" LabelText="design" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="126.0000" Y="47.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="10.4430" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3481" />
                    <PreSize X="0.3938" Y="1.5667" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="1" G="44" B="98" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="nameDesign2" ActionTag="1051628282" Tag="67" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="82.5000" RightMargin="82.5000" TopMargin="28.9999" BottomMargin="-31.9999" FontSize="24" LabelText="Jack tang" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="155.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-15.4999" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.5167" />
                    <PreSize X="0.4844" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="nameDesign1" ActionTag="1694435038" Tag="65" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="65.5000" RightMargin="65.5000" TopMargin="48.9997" BottomMargin="-51.9997" FontSize="24" LabelText="Allen tsang" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="189.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-35.4997" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-1.1833" />
                    <PreSize X="0.5906" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="nameDesign3" ActionTag="-1451515328" Tag="3110" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="114.0000" RightMargin="114.0000" TopMargin="68.9995" BottomMargin="-71.9995" FontSize="24" LabelText="Kei Ko" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="92.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-55.4995" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-1.8500" />
                    <PreSize X="0.2875" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" Y="443.0000" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7799" />
                <PreSize X="1.0000" Y="0.0528" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="codingPanel" ActionTag="620581991" Tag="56" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="184.0000" BottomMargin="354.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="-122" Scale9OriginY="-37" Scale9Width="244" Scale9Height="74" ctype="PanelObjectData">
                <Size X="320.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="heading" ActionTag="-1494734190" Tag="57" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="33.5000" RightMargin="33.5000" TopMargin="-1.3900" BottomMargin="-15.6100" FontSize="28" LabelText="Programming" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="253.0000" Y="47.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="7.8900" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2630" />
                    <PreSize X="0.7906" Y="1.5667" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="nameCoder1" ActionTag="-2047541818" Tag="68" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="86.5000" RightMargin="86.5000" TopMargin="49.7783" BottomMargin="-52.7783" FontSize="24" LabelText="Calvin Kei" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="147.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-36.2783" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-1.2093" />
                    <PreSize X="0.4594" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="nameCoder3" ActionTag="-484837334" Tag="70" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="104.3600" RightMargin="105.6400" TopMargin="30.8889" BottomMargin="-33.8889" FontSize="24" LabelText="Ken Lee" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="110.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="159.3600" Y="-17.3889" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4980" Y="-0.5796" />
                    <PreSize X="0.3438" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" Y="354.0000" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6232" />
                <PreSize X="1.0000" Y="0.0528" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="mktPanel" ActionTag="-445124820" Tag="62" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="284.0000" BottomMargin="254.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="-122" Scale9OriginY="-37" Scale9Width="244" Scale9Height="74" ctype="PanelObjectData">
                <Size X="320.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="heading" ActionTag="908038140" Tag="63" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="63.6200" RightMargin="61.3800" TopMargin="-24.7210" BottomMargin="7.7210" FontSize="28" LabelText="Marketing" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="195.0000" Y="47.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="161.1200" Y="31.2210" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5035" Y="1.0407" />
                    <PreSize X="0.6094" Y="1.5667" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="name1" ActionTag="1561818386" Tag="64" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="46.5000" RightMargin="46.5000" TopMargin="5.4444" BottomMargin="-8.4444" FontSize="24" LabelText="David Phalaris" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="227.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="8.0556" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.2685" />
                    <PreSize X="0.7094" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" Y="254.0000" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4472" />
                <PreSize X="1.0000" Y="0.0528" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="qaPanel" ActionTag="-1105059618" VisibleForFrame="False" Tag="58" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="224.8100" BottomMargin="313.1900" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="-122" Scale9OriginY="-37" Scale9Width="244" Scale9Height="74" ctype="PanelObjectData">
                <Size X="320.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="heading" ActionTag="71161172" Tag="59" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="37.5000" RightMargin="37.5000" TopMargin="7.0000" BottomMargin="-5.0000" FontSize="20" LabelText="Quality Assurance" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="245.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="9.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.3000" />
                    <PreSize X="0.7656" Y="0.9333" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="name1" ActionTag="1606951210" Tag="22" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="80.5000" RightMargin="80.5000" TopMargin="30.0000" BottomMargin="-44.0000" FontSize="35" LabelText="Gary Lau" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="159.0000" Y="44.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-22.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.7333" />
                    <PreSize X="0.4969" Y="1.4667" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="313.1900" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.5514" />
                <PreSize X="1.0000" Y="0.0528" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="musicPanel" ActionTag="2021735303" Tag="65" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="319.8100" BottomMargin="218.1900" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="122" RightEage="122" TopEage="37" BottomEage="37" Scale9OriginX="-122" Scale9OriginY="-37" Scale9Width="244" Scale9Height="74" ctype="PanelObjectData">
                <Size X="320.0000" Y="30.0000" />
                <Children>
                  <AbstractNodeData Name="heading_0" ActionTag="-1041394551" Tag="73" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="105.0000" RightMargin="105.0000" TopMargin="1.9430" BottomMargin="-18.9430" FontSize="28" LabelText="Music" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="110.0000" Y="47.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="4.5570" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.1519" />
                    <PreSize X="0.3438" Y="1.5667" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="128" G="0" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="name1" ActionTag="-2032291553" Tag="67" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-98.0000" RightMargin="-98.0000" TopMargin="29.8891" BottomMargin="-32.8891" FontSize="24" LabelText="Kevin MacLeod  (incompetech.com)" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="516.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-16.3891" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-0.5463" />
                    <PreSize X="1.6125" Y="1.1000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="license" ActionTag="1129464726" Tag="74" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="24.5000" RightMargin="24.5000" TopMargin="51.5600" BottomMargin="-49.5600" FontSize="20" LabelText=" Creative Common 3.0" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="271.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="-35.5600" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-1.1853" />
                    <PreSize X="0.8469" Y="0.9333" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" Y="218.1900" />
                <Scale ScaleX="0.9000" ScaleY="0.9000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3841" />
                <PreSize X="1.0000" Y="0.0528" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="fbButton" ActionTag="-813402755" VisibleForFrame="False" Tag="71" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-11.5080" RightMargin="-21.4920" TopMargin="382.0000" BottomMargin="92.0000" TouchEnable="True" FontSize="25" ButtonText="  " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="Sprite_3" ActionTag="732276344" Tag="856" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="54.0000" RightMargin="171.0000" TopMargin="-19.8200" BottomMargin="-14.1800" ctype="SpriteObjectData">
                    <Size X="128.0000" Y="128.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="118.0000" Y="49.8200" />
                    <Scale ScaleX="0.3000" ScaleY="0.3000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3343" Y="0.5300" />
                    <PreSize X="0.3626" Y="1.3617" />
                    <FileData Type="Normal" Path="guiImage/ui_shop_fb.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_2_0" ActionTag="-1119176257" Tag="858" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="162.3800" RightMargin="106.6200" TopMargin="32.5000" BottomMargin="32.5000" FontSize="18" LabelText="like us" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="84.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="162.3800" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4600" Y="0.5000" />
                    <PreSize X="0.2380" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="164.9920" Y="139.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5156" Y="0.2447" />
                <PreSize X="1.1031" Y="0.1655" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="twitterButton" ActionTag="1228088065" VisibleForFrame="False" Tag="31" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-11.5080" RightMargin="-21.4920" TopMargin="443.0000" BottomMargin="31.0000" TouchEnable="True" FontSize="25" ButtonText="     " Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="btn_fb_5" ActionTag="8098159" Tag="32" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" RightMargin="153.0000" TopMargin="-56.7600" BottomMargin="-49.2400" ctype="SpriteObjectData">
                    <Size X="200.0000" Y="200.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="100.0000" Y="50.7600" />
                    <Scale ScaleX="0.2000" ScaleY="0.2000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2833" Y="0.5400" />
                    <PreSize X="0.5666" Y="2.1277" />
                    <FileData Type="Normal" Path="guiImage/twitter.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_2" ActionTag="2101573091" Tag="857" RotationSkewX="0.2465" RotationSkewY="0.2464" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="137.6700" RightMargin="96.3300" TopMargin="32.5000" BottomMargin="32.5000" FontSize="18" LabelText="follow us" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="119.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="137.6700" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3900" Y="0.5000" />
                    <PreSize X="0.3371" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="164.9920" Y="78.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5156" Y="0.1373" />
                <PreSize X="1.1031" Y="0.1655" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-0.2979" Y="0.2979" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0009" Y="0.0005" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="secretPanel" ActionTag="-1422431159" Tag="86" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="326.4000" RightMargin="-326.4000" TouchEnable="True" ClipAble="False" BackColorAlpha="222" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="1043369582" Tag="16" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="64.0000" RightMargin="64.0000" TopMargin="122.9437" BottomMargin="395.0563" FontSize="20" LabelText="Tell me the secret &#xA;to unlock GM mode" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="192.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="420.0563" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7395" />
                <PreSize X="0.6000" Y="0.0880" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="secretTextField" ActionTag="1905508124" Tag="17" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="35.0000" RightMargin="35.0000" TopMargin="189.7326" BottomMargin="332.2674" TouchEnable="True" FontSize="30" IsCustomSize="True" LabelText="" PlaceHolderText="Secret here" MaxLengthText="10" PasswordEnable="True" ctype="TextFieldObjectData">
                <Size X="250.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="355.2674" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6255" />
                <PreSize X="0.7813" Y="0.0810" />
                <FontResource Type="Normal" Path="CaviarDreams.ttf" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="confirmBtn" ActionTag="-2003561520" Tag="18" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="35.0000" RightMargin="35.0000" TopMargin="237.2394" BottomMargin="230.7606" TouchEnable="True" FontSize="40" ButtonText="Confirm" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="250.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="280.7606" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4943" />
                <PreSize X="0.7813" Y="0.1761" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="0" />
                <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="secretBackBtn" ActionTag="712833358" Tag="20" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-40.9920" RightMargin="160.9920" TopMargin="-20.3345" BottomMargin="472.3345" TouchEnable="True" FontSize="36" ButtonText="close" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="200.0000" Y="116.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.0080" Y="530.3345" />
                <Scale ScaleX="0.2700" ScaleY="0.2700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1844" Y="0.9337" />
                <PreSize X="0.6250" Y="0.2042" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="49" G="216" B="212" />
                <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="326.4000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0200" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>