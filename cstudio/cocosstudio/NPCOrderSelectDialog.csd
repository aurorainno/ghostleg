<GameFile>
  <PropertyGroup Name="NPCOrderSelectDialog" Type="Layer" ID="9971cc43-412b-40cc-a82c-855b3eb84628" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="20" Speed="1.0000" ActivedAnimationName="popUp">
        <Timeline ActionTag="-599154009" Property="Scale">
          <ScaleFrame FrameIndex="0" X="0.1000" Y="0.1000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="3" X="0.1500" Y="0.5700">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="0.5500" Y="0.5500">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-599154009" Property="RotationSkew">
          <ScaleFrame FrameIndex="3" X="0.0000" Y="0.0000">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1396587375" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="True" />
          <BoolFrame FrameIndex="20" Tween="False" Value="True" />
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="popUp" StartIndex="0" EndIndex="20">
          <RenderColor A="255" R="255" G="250" B="205" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Layer" Tag="72" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="boss" ActionTag="-1582887713" VisibleForFrame="False" Tag="74" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="-32.7431" RightMargin="139.7431" TopMargin="282.8046" BottomMargin="4.1954" ctype="SpriteObjectData">
            <Size X="213.0000" Y="281.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="73.7569" Y="4.1954" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2305" Y="0.0074" />
            <PreSize X="0.6656" Y="0.4947" />
            <FileData Type="Normal" Path="guiImage/ingame_mission_boss.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="contentPanel" ActionTag="-1396587375" Tag="206" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="38.0000" BottomMargin="-38.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="dialogSprite" ActionTag="-599154009" Tag="73" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-121.5000" RightMargin="-121.5000" TopMargin="-286.0000" BottomMargin="124.0000" ctype="SpriteObjectData">
                <Size X="563.0000" Y="730.0000" />
                <AnchorPoint ScaleX="0.5000" />
                <Position X="160.0000" Y="124.0000" />
                <Scale ScaleX="0.5500" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2183" />
                <PreSize X="1.7594" Y="1.2852" />
                <FileData Type="Normal" Path="guiImage/ui_character_statsinfo.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="firstNPCPanel" ActionTag="-1938513390" Tag="111" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-0.0009" RightMargin="0.0009" TopMargin="95.0000" BottomMargin="347.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="126.0000" />
                <Children>
                  <AbstractNodeData Name="testNode_notDisplay01" ActionTag="1226316291" VisibleForFrame="False" Tag="138" IconVisible="True" PositionPercentXEnabled="True" LeftMargin="15.8400" RightMargin="13.6600" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="290.5000" Y="126.0000" />
                    <AnchorPoint />
                    <Position X="15.8400" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0495" />
                    <PreSize X="0.9078" Y="1.0000" />
                    <FileData Type="Normal" Path="NPCOrderItemView.csd" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="-0.0009" Y="347.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0000" Y="0.6109" />
                <PreSize X="1.0000" Y="0.2218" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="secondNPCPanel" ActionTag="-1956032580" Tag="116" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="231.0000" BottomMargin="211.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="126.0000" />
                <Children>
                  <AbstractNodeData Name="testNode_notDisplay02" ActionTag="-664210597" VisibleForFrame="False" Tag="117" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="14.3680" RightMargin="15.1320" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                    <Size X="290.5000" Y="126.0000" />
                    <AnchorPoint />
                    <Position X="14.3680" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0449" />
                    <PreSize X="0.9078" Y="1.0000" />
                    <FileData Type="Normal" Path="NPCOrderItemView.csd" Plist="" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="211.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.3715" />
                <PreSize X="1.0000" Y="0.2218" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_10" ActionTag="-1209347208" Tag="115" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-3.5000" RightMargin="-3.5000" TopMargin="52.5000" BottomMargin="474.5000" FontSize="24" LabelText="pick your next order" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="327.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="495.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.8715" />
                <PreSize X="1.0219" Y="0.0722" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="23" G="114" B="151" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="changeBtn" ActionTag="-2039366524" Tag="205" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-16.5000" RightMargin="-16.5000" TopMargin="348.0000" BottomMargin="126.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="text" ActionTag="2022951774" Tag="1748" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="107.0000" RightMargin="72.0000" TopMargin="34.9794" BottomMargin="30.0206" FontSize="18" LabelText="change orders" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="174.0000" Y="29.0000" />
                    <AnchorPoint ScaleY="0.3586" />
                    <Position X="107.0000" Y="40.4200" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3031" Y="0.4300" />
                    <PreSize X="0.4929" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="20" G="119" B="122" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="videoSprite" ActionTag="-2079647113" Tag="1749" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="54.0000" RightMargin="247.0000" TopMargin="25.3648" BottomMargin="23.6352" ctype="SpriteObjectData">
                    <Size X="52.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="80.0000" Y="46.1352" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2266" Y="0.4908" />
                    <PreSize X="0.1473" Y="0.4787" />
                    <FileData Type="Normal" Path="guiImage/ui_general_video.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="173.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3046" />
                <PreSize X="1.1031" Y="0.1655" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position Y="-38.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="-0.0669" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>