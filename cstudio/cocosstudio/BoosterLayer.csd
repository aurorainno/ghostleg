<GameFile>
  <PropertyGroup Name="BoosterLayer" Type="Layer" ID="e824bf30-d501-4632-ac75-e5aae91a11c3" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1258" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="coverPanel" ActionTag="-1718575910" Tag="794" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-1035320799" Tag="1259" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="16.0000" RightMargin="16.0000" TopMargin="71.0000" BottomMargin="71.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="288.0000" Y="426.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="-1630497751" Tag="1260" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-156.0000" RightMargin="-156.0000" TopMargin="-219.5000" BottomMargin="-219.5000" ctype="SpriteObjectData">
                <Size X="600.0000" Y="865.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="213.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="2.0833" Y="2.0305" />
                <FileData Type="Normal" Path="guiImage/ui_booster_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="303577354" Tag="508" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-16.5000" RightMargin="-16.5000" TopMargin="57.0000" BottomMargin="265.0000" ctype="SpriteObjectData">
                <Size X="321.0000" Y="104.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="317.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7441" />
                <PreSize X="1.1146" Y="0.2441" />
                <FileData Type="Normal" Path="guiImage/ui_booster_header.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="-61902762" Tag="509" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="47.3800" RightMargin="41.6200" TopMargin="98.0000" BottomMargin="300.0000" FontSize="20" LabelText="select booster" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="199.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="146.8800" Y="314.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5100" Y="0.7371" />
                <PreSize X="0.6910" Y="0.0657" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cancelBtn" ActionTag="1209540037" Tag="1266" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="230.0612" RightMargin="-7.0612" TopMargin="-33.5500" BottomMargin="396.5500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="35" Scale9Height="41" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="65.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="262.5612" Y="428.0500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9117" Y="1.0048" />
                <PreSize X="0.2257" Y="0.1479" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_cancel.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemPanel03" ActionTag="-1293704288" Tag="105" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="191.1744" RightMargin="-3.1744" TopMargin="146.0000" BottomMargin="180.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="1981911653" Tag="881" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selectedBG" ActionTag="-631477435" VisibleForFrame="False" Tag="882" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="itemSprite" ActionTag="1236927175" Tag="883" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-19.5000" RightMargin="-37.5000" TopMargin="-20.0000" BottomMargin="-20.0000" ctype="SpriteObjectData">
                    <Size X="157.0000" Y="140.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="59.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5900" Y="0.5000" />
                    <PreSize X="1.5700" Y="1.4000" />
                    <FileData Type="Normal" Path="guiImage/ui_booster_item3.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="counterPanel" ActionTag="-1547073078" Tag="884" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="-5.0000" RightMargin="55.0000" TopMargin="55.0000" BottomMargin="-5.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="50.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="countBG" ActionTag="432590367" Tag="885" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-11.5000" BottomMargin="-11.5000" ctype="SpriteObjectData">
                        <Size X="73.0000" Y="73.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="25.0000" Y="25.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4600" Y="1.4600" />
                        <FileData Type="Normal" Path="guiImage/ui_booster_badge_number.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="countText" ActionTag="613630244" Tag="886" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.0000" RightMargin="5.0000" TopMargin="9.5000" BottomMargin="7.5000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="33.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="24.0000" Y="24.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4800" Y="0.4800" />
                        <PreSize X="0.8400" Y="0.6600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-5.0000" Y="-5.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0500" Y="-0.0500" />
                    <PreSize X="0.5000" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="touchArea" ActionTag="-1571376620" Tag="887" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0000" BottomMargin="5.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.9000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="plusBtn" ActionTag="-1834707494" Tag="888" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="50.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="50.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="73.0000" Y="73.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="86.5000" Y="86.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8650" Y="0.8650" />
                    <PreSize X="0.7300" Y="0.7300" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_add.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="191.1744" Y="180.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6638" Y="0.4225" />
                <PreSize X="0.3472" Y="0.2347" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemPanel02" ActionTag="2064101946" Tag="107" IconVisible="False" LeftMargin="95.5000" RightMargin="92.5000" TopMargin="146.0000" BottomMargin="180.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="885558005" Tag="872" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selectedBG" ActionTag="696472447" VisibleForFrame="False" Tag="873" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="itemSprite" ActionTag="698180506" Tag="874" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.5000" RightMargin="-33.5000" TopMargin="-20.0000" BottomMargin="-20.0000" ctype="SpriteObjectData">
                    <Size X="157.0000" Y="140.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="55.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5500" Y="0.5000" />
                    <PreSize X="1.5700" Y="1.4000" />
                    <FileData Type="Normal" Path="guiImage/ui_booster_item2.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="counterPanel" ActionTag="-1476154926" Tag="875" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="-5.0000" RightMargin="55.0000" TopMargin="55.0000" BottomMargin="-5.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="50.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="countBG" ActionTag="1457130839" Tag="876" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-11.5000" BottomMargin="-11.5000" ctype="SpriteObjectData">
                        <Size X="73.0000" Y="73.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="25.0000" Y="25.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4600" Y="1.4600" />
                        <FileData Type="Normal" Path="guiImage/ui_booster_badge_number.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="countText" ActionTag="990066948" Tag="877" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.0000" RightMargin="5.0000" TopMargin="9.5000" BottomMargin="7.5000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="33.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="24.0000" Y="24.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4800" Y="0.4800" />
                        <PreSize X="0.8400" Y="0.6600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-5.0000" Y="-5.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0500" Y="-0.0500" />
                    <PreSize X="0.5000" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="touchArea" ActionTag="484266382" Tag="878" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0000" BottomMargin="5.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.9000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="plusBtn" ActionTag="-927351283" Tag="879" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="50.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="50.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="73.0000" Y="73.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="86.5000" Y="86.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8650" Y="0.8650" />
                    <PreSize X="0.7300" Y="0.7300" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_add.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="95.5000" Y="180.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3316" Y="0.4225" />
                <PreSize X="0.3472" Y="0.2347" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemPanel01" ActionTag="-1252213666" Tag="103" IconVisible="False" LeftMargin="-1.0000" RightMargin="189.0000" TopMargin="146.0000" BottomMargin="180.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="978980014" Tag="510" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selectedBG" ActionTag="164051628" VisibleForFrame="False" Tag="511" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="itemSprite" ActionTag="1401287298" Tag="1269" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-28.5000" RightMargin="-28.5000" TopMargin="-20.0000" BottomMargin="-20.0000" ctype="SpriteObjectData">
                    <Size X="157.0000" Y="140.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.5700" Y="1.4000" />
                    <FileData Type="Normal" Path="guiImage/ui_booster_item1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="counterPanel" ActionTag="-207639049" Tag="1276" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="-5.0000" RightMargin="55.0000" TopMargin="55.0000" BottomMargin="-5.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="50.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="countBG" ActionTag="-1193000368" Tag="1274" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-11.5000" BottomMargin="-11.5000" ctype="SpriteObjectData">
                        <Size X="73.0000" Y="73.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="25.0000" Y="25.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4600" Y="1.4600" />
                        <FileData Type="Normal" Path="guiImage/ui_booster_badge_number.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="countText" ActionTag="2054239498" Tag="1275" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.0000" RightMargin="5.0000" TopMargin="9.5000" BottomMargin="7.5000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="33.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="24.0000" Y="24.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4800" Y="0.4800" />
                        <PreSize X="0.8400" Y="0.6600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-5.0000" Y="-5.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0500" Y="-0.0500" />
                    <PreSize X="0.5000" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="touchArea" ActionTag="1677484423" Tag="1314" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0000" BottomMargin="5.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.9000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="plusBtn" ActionTag="-34287653" Tag="1271" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="51.0000" RightMargin="-24.0000" TopMargin="-24.0000" BottomMargin="51.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="73.0000" Y="73.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="87.5000" Y="87.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8750" Y="0.8750" />
                    <PreSize X="0.7300" Y="0.7300" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_add.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="-1.0000" Y="180.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="-0.0035" Y="0.4225" />
                <PreSize X="0.3472" Y="0.2347" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemPanel04" ActionTag="1220699838" Tag="109" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="41.9616" RightMargin="146.0384" TopMargin="257.0000" BottomMargin="69.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="156565227" Tag="890" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selectedBG" ActionTag="-215480648" Tag="891" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="itemSprite" ActionTag="-1670414476" Tag="892" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-24.5000" RightMargin="-32.5000" TopMargin="-17.0000" BottomMargin="-23.0000" ctype="SpriteObjectData">
                    <Size X="157.0000" Y="140.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="54.0000" Y="47.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5400" Y="0.4700" />
                    <PreSize X="1.5700" Y="1.4000" />
                    <FileData Type="Normal" Path="guiImage/ui_booster_item4.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="counterPanel" ActionTag="418313150" Tag="893" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="-5.0000" RightMargin="55.0000" TopMargin="55.0000" BottomMargin="-5.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="50.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="countBG" ActionTag="201676028" Tag="894" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-11.5000" BottomMargin="-11.5000" ctype="SpriteObjectData">
                        <Size X="73.0000" Y="73.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="25.0000" Y="25.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4600" Y="1.4600" />
                        <FileData Type="Normal" Path="guiImage/ui_booster_badge_number.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="countText" ActionTag="1149023026" Tag="895" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.0000" RightMargin="5.0000" TopMargin="9.5000" BottomMargin="7.5000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="33.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="24.0000" Y="24.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4800" Y="0.4800" />
                        <PreSize X="0.8400" Y="0.6600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-5.0000" Y="-5.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0500" Y="-0.0500" />
                    <PreSize X="0.5000" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="touchArea" ActionTag="977404176" Tag="896" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0000" BottomMargin="5.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.9000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="plusBtn" ActionTag="1276868951" Tag="897" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="50.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="50.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="73.0000" Y="73.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="86.5000" Y="86.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8650" Y="0.8650" />
                    <PreSize X="0.7300" Y="0.7300" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_add.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="41.9616" Y="69.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1457" Y="0.1620" />
                <PreSize X="0.3472" Y="0.2347" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="itemPanel05" ActionTag="1558765770" Tag="106" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="143.8272" RightMargin="44.1728" TopMargin="257.0000" BottomMargin="69.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="100.0000" Y="100.0000" />
                <Children>
                  <AbstractNodeData Name="normalBG" ActionTag="-1291579498" Tag="899" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="selectedBG" ActionTag="-1869886496" VisibleForFrame="False" Tag="900" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-43.0000" RightMargin="-43.0000" TopMargin="-43.0000" BottomMargin="-43.0000" ctype="SpriteObjectData">
                    <Size X="186.0000" Y="186.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.8600" Y="1.8600" />
                    <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="itemSprite" ActionTag="-130597092" Tag="901" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-23.5000" RightMargin="-33.5000" TopMargin="-19.0000" BottomMargin="-21.0000" ctype="SpriteObjectData">
                    <Size X="157.0000" Y="140.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="55.0000" Y="49.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5500" Y="0.4900" />
                    <PreSize X="1.5700" Y="1.4000" />
                    <FileData Type="Normal" Path="guiImage/ui_booster_item5.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="counterPanel" ActionTag="1129752549" Tag="902" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="-5.0000" RightMargin="55.0000" TopMargin="55.0000" BottomMargin="-5.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="50.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="countBG" ActionTag="-2089858625" Tag="903" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-11.5000" RightMargin="-11.5000" TopMargin="-11.5000" BottomMargin="-11.5000" ctype="SpriteObjectData">
                        <Size X="73.0000" Y="73.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="25.0000" Y="25.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4600" Y="1.4600" />
                        <FileData Type="Normal" Path="guiImage/ui_booster_badge_number.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="countText" ActionTag="-1064072864" Tag="904" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.0000" RightMargin="5.0000" TopMargin="9.5000" BottomMargin="7.5000" FontSize="24" LabelText="99" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="42.0000" Y="33.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="24.0000" Y="24.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4800" Y="0.4800" />
                        <PreSize X="0.8400" Y="0.6600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="-5.0000" Y="-5.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0500" Y="-0.0500" />
                    <PreSize X="0.5000" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="touchArea" ActionTag="-1235322173" Tag="905" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="5.0000" RightMargin="5.0000" TopMargin="5.0000" BottomMargin="5.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="90.0000" Y="90.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="50.0000" Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.9000" Y="0.9000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="plusBtn" ActionTag="-713248756" Tag="906" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="50.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="50.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="43" Scale9Height="51" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                    <Size X="73.0000" Y="73.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="86.5000" Y="86.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8650" Y="0.8650" />
                    <PreSize X="0.7300" Y="0.7300" />
                    <TextColor A="255" R="65" G="65" B="70" />
                    <NormalFileData Type="Normal" Path="guiImage/ui_booster_badge_add.png" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="143.8272" Y="69.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4994" Y="0.1620" />
                <PreSize X="0.3472" Y="0.2347" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="infoText" ActionTag="-836491533" VisibleForFrame="False" Tag="1313" IconVisible="False" VerticalEdge="BottomEdge" LeftMargin="-96.0000" RightMargin="-96.0000" TopMargin="328.0000" BottomMargin="48.0000" IsCustomSize="True" FontSize="18" LabelText="Text Label&#xA;ffdddddd" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="480.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="73.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1714" />
                <PreSize X="1.6667" Y="0.1174" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="startButton" ActionTag="-826863345" Tag="1319" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-19.5000" RightMargin="-19.5000" TopMargin="340.0000" BottomMargin="-24.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="297" Scale9Height="88" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="327.0000" Y="110.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="31.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0728" />
                <PreSize X="1.1354" Y="0.2582" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_booster_start_btn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetSprite" ActionTag="-857198722" Tag="907" IconVisible="False" LeftMargin="-33.5000" RightMargin="152.5000" TopMargin="-38.0000" BottomMargin="278.0000" ctype="SpriteObjectData">
                <Size X="169.0000" Y="186.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="51.0000" Y="371.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1771" Y="0.8709" />
                <PreSize X="0.5868" Y="0.4366" />
                <FileData Type="Normal" Path="guiImage/ui_booster_stage1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetName" ActionTag="1321733111" Tag="909" RotationSkewX="12.0002" IconVisible="False" LeftMargin="100.0005" RightMargin="-2.0005" TopMargin="28.5176" BottomMargin="361.4824" FontSize="26" LabelText="rocky road" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="190.0000" Y="36.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="100.0005" Y="379.4824" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3472" Y="0.8908" />
                <PreSize X="0.6597" Y="0.0845" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="districtLabel" ActionTag="-2104632135" VisibleForFrame="False" Tag="908" RotationSkewX="12.0000" IconVisible="False" LeftMargin="100.0000" RightMargin="65.0000" TopMargin="16.0000" BottomMargin="382.0000" FontSize="20" LabelText="district 1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="123.0000" Y="28.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="100.0000" Y="396.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3472" Y="0.9296" />
                <PreSize X="0.4271" Y="0.0657" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.9000" Y="0.7500" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>