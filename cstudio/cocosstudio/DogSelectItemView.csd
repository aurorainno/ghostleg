<GameFile>
  <PropertyGroup Name="DogSelectItemView" Type="Layer" ID="20faaf0e-2cc5-44a3-968c-3aeb829d043d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="21" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="80.0000" />
        <Children>
          <AbstractNodeData Name="basePanel" ActionTag="1051290053" Tag="62" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="80.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockedBg" ActionTag="-830533331" Tag="25" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-46.5000" BottomMargin="-46.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="173.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="40.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.4700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.1625" />
            <FileData Type="Normal" Path="guiImage/dselect_bg_unlocked.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="normalBg" Visible="False" ActionTag="-940963296" Tag="27" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-46.5000" BottomMargin="-46.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="173.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="40.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.4700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.1625" />
            <FileData Type="Normal" Path="guiImage/dselect_bg_deselect.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="dogSprite" Visible="False" ActionTag="1077342689" Tag="26" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="18.0000" RightMargin="256.0000" TopMargin="17.0000" BottomMargin="17.0000" ctype="SpriteObjectData">
            <Size X="46.0000" Y="46.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="41.0000" Y="40.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1281" Y="0.5000" />
            <PreSize X="0.1437" Y="0.5750" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="dogIconPanel" ActionTag="2063862978" Tag="59" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="8.5000" RightMargin="234.5000" TopMargin="1.5000" BottomMargin="1.5000" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="77.0000" Y="77.0000" />
            <Children>
              <AbstractNodeData Name="dogIconSprite" ActionTag="-1846475227" Tag="60" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-16.5000" RightMargin="-3.5000" TopMargin="-17.7000" BottomMargin="-2.3000" ctype="SpriteObjectData">
                <Size X="97.0000" Y="97.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="46.2000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4156" Y="0.6000" />
                <PreSize X="1.2597" Y="1.2597" />
                <FileData Type="Normal" Path="dog/dselect_dog2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="progressText" ActionTag="-377501126" Tag="66" IconVisible="False" LeftMargin="-43.0000" RightMargin="-30.0000" TopMargin="52.0000" BottomMargin="-5.0000" IsCustomSize="True" FontSize="23" LabelText="12 / 33" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="150.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="10.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4156" Y="0.1299" />
                <PreSize X="1.9481" Y="0.3896" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="47.0000" Y="40.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1469" Y="0.5000" />
            <PreSize X="0.2406" Y="0.9625" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="dogNameText" ActionTag="-2083724726" Tag="63" IconVisible="False" LeftMargin="86.6700" RightMargin="-45.6700" TopMargin="-5.5500" BottomMargin="44.5500" FontSize="32" LabelText="English Sheepdog" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="279.0000" Y="41.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="86.6700" Y="65.0500" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2708" Y="0.8131" />
            <PreSize X="0.8719" Y="0.5125" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="infoButton" ActionTag="-1536032408" Tag="64" IconVisible="False" LeftMargin="214.1300" RightMargin="57.8700" TopMargin="-9.5500" BottomMargin="41.5500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="18" Scale9Height="26" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="48.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="238.1300" Y="65.5500" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7442" Y="0.8194" />
            <PreSize X="0.1500" Y="0.6000" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/ui_info.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="lockedInfoPanel" ActionTag="2125380415" Tag="43" IconVisible="False" LeftMargin="87.0000" RightMargin="53.0000" TopMargin="27.0000" BottomMargin="3.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="180.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="dogInfoText" ActionTag="-1134550361" Tag="65" IconVisible="False" PositionPercentYEnabled="True" RightMargin="-133.0000" TopMargin="2.5000" BottomMargin="-62.5000" IsCustomSize="True" FontSize="22" LabelText="Travel 250 metre to unlock ss" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="313.0000" Y="110.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="47.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.9500" />
                <PreSize X="1.7389" Y="2.2000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="87.0000" Y="53.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2719" Y="0.6625" />
            <PreSize X="0.5625" Y="0.6250" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockedInfoPanel" Visible="False" ActionTag="263879891" Tag="44" IconVisible="False" LeftMargin="87.0000" RightMargin="33.0000" TopMargin="27.0000" BottomMargin="3.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="suitPanel" ActionTag="-2110686808" Tag="116" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="0.5000" BottomMargin="24.5000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="25.0000" />
                <Children>
                  <AbstractNodeData Name="suitIcon" ActionTag="-1027384566" Tag="52" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-3.5000" RightMargin="170.5000" TopMargin="-4.0000" BottomMargin="-4.0000" ctype="SpriteObjectData">
                    <Size X="33.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="13.0000" Y="12.5000" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0650" Y="0.5000" />
                    <PreSize X="0.1650" Y="1.3200" />
                    <FileData Type="Normal" Path="guiImage/ui_dog_ability01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="suitLevelIcon" ActionTag="-1720682274" Tag="54" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="7.6500" RightMargin="168.3500" TopMargin="7.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
                    <Size X="24.0000" Y="24.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="19.6500" Y="5.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0983" Y="0.2200" />
                    <PreSize X="0.1200" Y="0.9600" />
                    <FileData Type="Normal" Path="guiImage/ui_dog_abilitylevel01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="suitName" ActionTag="-1678670906" Tag="55" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="35.0000" RightMargin="59.0000" TopMargin="-3.5000" BottomMargin="-3.5000" FontSize="25" LabelText="Unknown" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="106.0000" Y="32.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="35.0000" Y="12.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1750" Y="0.5000" />
                    <PreSize X="0.5300" Y="1.2800" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="49.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.9900" />
                <PreSize X="1.0000" Y="0.5000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="masteryPanel" ActionTag="-861602517" Tag="117" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="24.5000" BottomMargin="0.5000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="200.0000" Y="25.0000" />
                <Children>
                  <AbstractNodeData Name="masteryIcon" ActionTag="1097514501" Tag="56" IconVisible="False" LeftMargin="-3.5000" RightMargin="170.5000" TopMargin="-0.5000" BottomMargin="-7.5000" ctype="SpriteObjectData">
                    <Size X="33.0000" Y="33.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="13.0000" Y="9.0000" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0650" Y="0.3600" />
                    <PreSize X="0.1650" Y="1.3200" />
                    <FileData Type="Normal" Path="guiImage/ui_dog_ability01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="masteryLevelIcon" ActionTag="-2091009754" Tag="57" IconVisible="False" LeftMargin="7.6500" RightMargin="168.3500" TopMargin="11.3200" BottomMargin="-10.3200" ctype="SpriteObjectData">
                    <Size X="24.0000" Y="24.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="19.6500" Y="1.6800" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0983" Y="0.0672" />
                    <PreSize X="0.1200" Y="0.9600" />
                    <FileData Type="Normal" Path="guiImage/ui_dog_abilitylevel01.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="masteryName" ActionTag="396854727" Tag="58" IconVisible="False" LeftMargin="35.0000" RightMargin="59.0000" TopMargin="1.2000" BottomMargin="-8.2000" FontSize="25" LabelText="Unknown" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="106.0000" Y="32.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="35.0000" Y="7.8000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1750" Y="0.3120" />
                    <PreSize X="0.5300" Y="1.2800" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="25.5000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.5100" />
                <PreSize X="1.0000" Y="0.5000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position X="87.0000" Y="53.0000" />
            <Scale ScaleX="0.8000" ScaleY="0.8000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2719" Y="0.6625" />
            <PreSize X="0.6250" Y="0.6250" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="selectBtn" ActionTag="-181511100" Tag="46" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="216.0000" RightMargin="-22.0000" TopMargin="7.3000" BottomMargin="-2.3000" TouchEnable="True" FontSize="30" ButtonText="Select" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="126.0000" Y="75.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="279.0000" Y="35.2000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8719" Y="0.4400" />
            <PreSize X="0.3938" Y="0.9375" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/btn_dog_select_btn.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="InUseBtn" ActionTag="-1527660219" Tag="47" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="216.0000" RightMargin="-22.0000" TopMargin="7.3000" BottomMargin="-2.3000" TouchEnable="True" FontSize="30" ButtonText="In-Use" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="126.0000" Y="75.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="279.0000" Y="35.2000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8719" Y="0.4400" />
            <PreSize X="0.3938" Y="0.9375" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <DisabledFileData Type="Normal" Path="guiImage/btn_dog_inuse.png" Plist="" />
            <PressedFileData Type="Normal" Path="guiImage/btn_dog_inuse.png" Plist="" />
            <NormalFileData Type="Normal" Path="guiImage/btn_dog_inuse.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockBtn" Visible="False" ActionTag="-1158721945" Tag="48" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="216.0000" RightMargin="-22.0000" TopMargin="7.3000" BottomMargin="-2.3000" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="150" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="126.0000" Y="75.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_1" ActionTag="258841419" Tag="49" IconVisible="False" LeftMargin="10.2700" RightMargin="73.7300" TopMargin="3.9500" BottomMargin="29.0500" ctype="SpriteObjectData">
                <Size X="42.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="31.2700" Y="50.0500" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2482" Y="0.6673" />
                <PreSize X="0.3333" Y="0.5600" />
                <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="cost" ActionTag="-1800529772" Tag="50" IconVisible="False" LeftMargin="28.9700" RightMargin="1.0300" TopMargin="-4.1300" BottomMargin="16.1300" FontSize="50" LabelText="300" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="76.9700" Y="47.6300" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6109" Y="0.6351" />
                <PreSize X="0.7619" Y="0.8400" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="1116015646" Tag="51" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-17.0000" RightMargin="-17.0000" TopMargin="31.2097" BottomMargin="-0.2097" FontSize="35" LabelText="to unlock" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="160.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="21.7903" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2905" />
                <PreSize X="1.2698" Y="0.5867" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="279.0000" Y="35.2000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8719" Y="0.4400" />
            <PreSize X="0.3938" Y="0.9375" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/btn_dog_finalpay.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="newLabel" ActionTag="-612150345" VisibleForFrame="False" Tag="45" IconVisible="False" LeftMargin="276.2856" RightMargin="-10.2856" TopMargin="-2.6853" BottomMargin="42.6853" ctype="SpriteObjectData">
            <Size X="54.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="303.2856" Y="62.6853" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9478" Y="0.7836" />
            <PreSize X="0.1688" Y="0.5000" />
            <FileData Type="Normal" Path="guiImage/btn_ui_new.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="eventUnlockBtn" ActionTag="-347698451" Tag="142" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="216.0000" RightMargin="-22.0000" TopMargin="7.3000" BottomMargin="-2.3000" TouchEnable="True" FontSize="30" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="96" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="126.0000" Y="75.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_1" ActionTag="913118039" Tag="143" IconVisible="False" LeftMargin="18.7700" RightMargin="74.2300" TopMargin="-4.0500" BottomMargin="19.0500" ctype="SpriteObjectData">
                <Size X="33.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="35.2700" Y="49.0500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2799" Y="0.6540" />
                <PreSize X="0.2619" Y="0.8000" />
                <FileData Type="Normal" Path="guiImage/item_candystick.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="cost" ActionTag="-256601202" Tag="144" IconVisible="False" LeftMargin="28.9700" RightMargin="1.0300" TopMargin="-4.1300" BottomMargin="16.1300" FontSize="50" LabelText="300" HorizontalAlignmentType="HT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="96.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="76.9700" Y="47.6300" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6109" Y="0.6351" />
                <PreSize X="0.7619" Y="0.8400" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_3" ActionTag="-832279023" Tag="145" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-17.0000" RightMargin="-17.0000" TopMargin="31.2097" BottomMargin="-0.2097" FontSize="35" LabelText="to unlock" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="160.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="63.0000" Y="21.7903" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2905" />
                <PreSize X="1.2698" Y="0.5867" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="279.0000" Y="35.2000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8719" Y="0.4400" />
            <PreSize X="0.3938" Y="0.9375" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="255" G="255" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/btn_xmas_buy.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>