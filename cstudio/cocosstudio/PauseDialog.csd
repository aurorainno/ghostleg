<GameFile>
  <PropertyGroup Name="PauseDialog" Type="Layer" ID="7343e80f-a157-48fd-a29d-e96c26c31e5b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="42" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="420.0000" />
        <Children>
          <AbstractNodeData Name="bgPanel" ActionTag="-551206895" Tag="48" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="1.0000" RightMargin="-1.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="151" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="420.0000" />
            <AnchorPoint />
            <Position X="1.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0031" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mapInfoText" ActionTag="-182739942" Tag="11" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="13.0000" RightMargin="57.0000" TopMargin="10.0000" BottomMargin="392.0000" IsCustomSize="True" FontSize="10" LabelText="MapInfo" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="250.0000" Y="18.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="138.0000" Y="401.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4313" Y="0.9548" />
            <PreSize X="0.7813" Y="0.0429" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="bg" ActionTag="1475055740" Tag="1247" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-125.0000" RightMargin="-125.0000" TopMargin="36.0000" BottomMargin="36.0000" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="15" BottomEage="15" Scale9OriginX="15" Scale9OriginY="15" Scale9Width="540" Scale9Height="318" ctype="ImageViewObjectData">
            <Size X="570.0000" Y="348.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="210.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.7813" Y="0.8286" />
            <FileData Type="Normal" Path="guiImage/ui_pause_bg.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="centerPanel" ActionTag="-1752577296" Tag="44" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="110.0000" BottomMargin="110.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="200.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="731818819" Tag="1246" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="47.5000" RightMargin="47.5000" TopMargin="21.5000" BottomMargin="139.5000" FontSize="28" LabelText="pause" OutlineSize="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="105.0000" Y="39.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="159.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7950" />
                <PreSize X="0.5250" Y="0.1950" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="resumeButton" ActionTag="-1483866266" Tag="1248" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-76.1200" RightMargin="-76.8800" TopMargin="40.0000" BottomMargin="66.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="count" ActionTag="975798983" Tag="1250" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="132.5000" RightMargin="132.5000" TopMargin="32.5000" BottomMargin="32.5000" FontSize="18" LabelText="resume" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="88.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.2493" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="20" G="119" B="122" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.3800" Y="113.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5019" Y="0.5650" />
                <PreSize X="1.7650" Y="0.4700" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="homeButton" ActionTag="-1997459376" Tag="1252" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-76.5000" RightMargin="-76.5000" TopMargin="102.0000" BottomMargin="4.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="count" ActionTag="990448052" Tag="1253" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="118.5000" RightMargin="118.5000" TopMargin="32.5000" BottomMargin="32.5000" FontSize="18" LabelText="Exit Game" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="116.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.3286" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="79" G="83" B="128" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="100.0000" Y="51.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2550" />
                <PreSize X="1.7650" Y="0.4700" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_alt.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="210.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.6250" Y="0.4762" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="topPanel" ActionTag="1054688457" Tag="196" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="370.0000" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="50.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="420.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="0.1190" />
            <SingleColor A="255" R="21" G="21" B="80" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>