<GameFile>
  <PropertyGroup Name="CharacterItemView" Type="Layer" ID="9aba50f1-bf41-4e9e-b4d8-07957f14554a" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="1776" ctype="GameLayerObjectData">
        <Size X="94.0000" Y="117.0000" />
        <Children>
          <AbstractNodeData Name="lockedPanel" ActionTag="-1152455976" Tag="449" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="94.0000" Y="117.0000" />
            <Children>
              <AbstractNodeData Name="normalBG" ActionTag="1615355181" Tag="1779" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="0.5000" RightMargin="-92.5000" TopMargin="0.5000" BottomMargin="-69.5000" ctype="SpriteObjectData">
                <Size X="186.0000" Y="186.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="0.5000" Y="116.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0053" Y="0.9957" />
                <PreSize X="1.9787" Y="1.5897" />
                <FileData Type="Normal" Path="guiImage/ui_character_holder_off.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="nameText" ActionTag="-2129923393" Tag="10" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-35.5000" RightMargin="-35.5000" TopMargin="71.0000" BottomMargin="24.0000" FontSize="16" LabelText="character name" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="165.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="35.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2991" />
                <PreSize X="1.7553" Y="0.1880" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="charFrame" ActionTag="-728438337" Tag="441" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" BottomMargin="23.0000" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="94.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="charSprite" ActionTag="-998233237" Tag="442" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-103.0000" RightMargin="-103.0000" TopMargin="-107.7000" BottomMargin="-98.3000" ctype="SpriteObjectData">
                    <Size X="300.0000" Y="300.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="47.0000" Y="51.7000" />
                    <Scale ScaleX="0.2300" ScaleY="0.2300" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="3.1915" Y="3.1915" />
                    <FileData Type="Normal" Path="guiImage/player_icon_sample.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="117.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="1.0000" Y="0.8034" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="-7329366" Tag="11" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-46.0000" RightMargin="-46.0000" TopMargin="98.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
                <Size X="186.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="6.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0513" />
                <PreSize X="1.9787" Y="0.2137" />
                <FileData Type="Normal" Path="guiImage/ui_character_shard_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="progressBar" ActionTag="1248162292" Tag="12" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-47.0000" RightMargin="-47.0000" TopMargin="97.0000" BottomMargin="-8.0000" ProgressInfo="52" ctype="LoadingBarObjectData">
                <Size X="188.0000" Y="28.0000" />
                <Children>
                  <AbstractNodeData Name="animeNode" ActionTag="1548291364" Tag="3574" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="94.0000" RightMargin="94.0000" TopMargin="14.0000" BottomMargin="14.0000" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="94.0000" Y="14.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="6.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0513" />
                <PreSize X="2.0000" Y="0.2393" />
                <ImageFileData Type="Normal" Path="guiImage/ui_character_shard_bar.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="progressText" ActionTag="1381686537" Tag="13" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="18.0000" RightMargin="18.0000" TopMargin="98.0000" BottomMargin="-7.0000" FontSize="16" LabelText="8/10" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="58.0000" Y="26.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="6.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0513" />
                <PreSize X="0.6170" Y="0.2222" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="192" G="108" B="42" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="touchPanel" ActionTag="315226315" Tag="185" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" BottomMargin="23.0000" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="94.0000" Y="94.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="117.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="1.0000" Y="0.8034" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockedPanel" ActionTag="1990125729" Tag="14" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="94.0000" Y="117.0000" />
            <Children>
              <AbstractNodeData Name="normalBG" ActionTag="70188595" Tag="15" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-46.0000" RightMargin="-46.0000" TopMargin="-46.0000" BottomMargin="-23.0000" ctype="SpriteObjectData">
                <Size X="186.0000" Y="186.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="70.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5983" />
                <PreSize X="1.9787" Y="1.5897" />
                <FileData Type="Normal" Path="guiImage/ui_character_holder_on.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="nameText" ActionTag="1804599092" Tag="16" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-35.5000" RightMargin="-35.5000" TopMargin="71.0000" BottomMargin="24.0000" FontSize="16" LabelText="character name" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="165.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="35.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2991" />
                <PreSize X="1.7553" Y="0.1880" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="charFrame" ActionTag="-325861084" Tag="443" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" BottomMargin="23.0000" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="94.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="charAnime" ActionTag="-1910279870" Tag="3722" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="47.0000" RightMargin="47.0000" TopMargin="42.3000" BottomMargin="51.7000" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="47.0000" Y="51.7000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="charSprite" ActionTag="1423794267" Tag="444" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-103.0000" RightMargin="-103.0000" TopMargin="-107.7000" BottomMargin="-98.3000" ctype="SpriteObjectData">
                    <Size X="300.0000" Y="300.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="47.0000" Y="51.7000" />
                    <Scale ScaleX="0.2300" ScaleY="0.2300" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="3.1915" Y="3.1915" />
                    <FileData Type="Normal" Path="guiImage/player_icon_sample.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="117.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="1.0000" Y="0.8034" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_4" ActionTag="86096386" Tag="20" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-23.2777" RightMargin="56.2777" TopMargin="-21.3890" BottomMargin="80.3890" ctype="SpriteObjectData">
                <Size X="61.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="7.2223" Y="109.3890" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0768" Y="0.9349" />
                <PreSize X="0.6489" Y="0.4957" />
                <FileData Type="Normal" Path="guiImage/ui_character_lvl1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_5" ActionTag="-1812189996" Tag="21" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-4.5000" RightMargin="81.5000" TopMargin="1.0000" BottomMargin="102.0000" FontSize="10" LabelText="lv" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="17.0000" Y="14.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="4.0000" Y="109.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0426" Y="0.9316" />
                <PreSize X="0.1809" Y="0.1197" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="levelText" ActionTag="-397506094" Tag="22" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="5.0000" RightMargin="75.0000" TopMargin="-4.0000" BottomMargin="99.0000" FontSize="16" LabelText="7" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="14.0000" Y="22.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="12.0000" Y="110.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1277" Y="0.9402" />
                <PreSize X="0.1489" Y="0.1880" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
              <AbstractNodeData Name="selectBtn" ActionTag="-663754820" Tag="23" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-54.0000" RightMargin="-54.0000" TopMargin="72.5000" BottomMargin="-54.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="172" Scale9Height="77" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="202.0000" Y="99.0000" />
                <Children>
                  <AbstractNodeData Name="animeNode" ActionTag="1980180708" Tag="3474" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="101.0000" RightMargin="101.0000" TopMargin="44.5500" BottomMargin="54.4500" ctype="SingleNodeObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="101.0000" Y="54.4500" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5500" />
                    <PreSize X="0.0000" Y="0.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="content" ActionTag="-409182265" Tag="24" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="58.5000" RightMargin="58.5000" TopMargin="35.5000" BottomMargin="35.5000" FontSize="20" LabelText="select" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="85.0000" Y="28.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="101.0000" Y="49.5000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4208" Y="0.2828" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="47.0000" Y="-5.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.0427" />
                <PreSize X="2.1489" Y="0.8462" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="guiImage/ui_character_select_on.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_character_select_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="newLabel" ActionTag="-1609829756" Tag="615" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="37.5000" RightMargin="-16.5000" TopMargin="-18.5000" BottomMargin="84.5000" ctype="SpriteObjectData">
                <Size X="73.0000" Y="51.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="74.0000" Y="110.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7872" Y="0.9402" />
                <PreSize X="0.7766" Y="0.4359" />
                <FileData Type="Normal" Path="guiImage/ui_character_newlabel.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="touchPanel" ActionTag="-854599723" Tag="132" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" BottomMargin="23.0000" ClipAble="False" BackColorAlpha="204" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="94.0000" Y="94.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="117.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="1.0000" Y="0.8034" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>