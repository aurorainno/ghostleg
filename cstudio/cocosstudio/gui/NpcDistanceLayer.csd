<GameFile>
  <PropertyGroup Name="NpcDistanceLayer" Type="Layer" ID="952194be-285a-4dd2-aae4-8acad4a96f3b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="210" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="100.0000" />
        <Children>
          <AbstractNodeData Name="fullBarImage" ActionTag="1888396941" Tag="56" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="-230.0000" TopMargin="45.5000" BottomMargin="45.5000" LeftEage="50" RightEage="50" TopEage="2" BottomEage="2" Scale9OriginX="50" Scale9OriginY="2" Scale9Width="54" Scale9Height="5" ctype="ImageViewObjectData">
            <Size X="520.0000" Y="9.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="30.0000" Y="50.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.5000" />
            <PreSize X="1.6250" Y="0.0900" />
            <FileData Type="Normal" Path="guiImage/ingame_waypoint_id_bar_off.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="grayLine" ActionTag="1639685011" VisibleForFrame="False" Tag="214" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="30.0000" TopMargin="49.0000" BottomMargin="49.0000" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="260.0000" Y="2.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="30.0000" Y="50.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.5000" />
            <PreSize X="0.8125" Y="0.0200" />
            <SingleColor A="255" R="127" G="127" B="127" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="progressBarImage" ActionTag="1392105719" Tag="57" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="-10.0000" TopMargin="45.5000" BottomMargin="45.5000" LeftEage="50" RightEage="50" TopEage="2" BottomEage="2" Scale9OriginX="50" Scale9OriginY="2" Scale9Width="54" Scale9Height="5" ctype="ImageViewObjectData">
            <Size X="300.0000" Y="9.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="30.0000" Y="50.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.5000" />
            <PreSize X="0.9375" Y="0.0900" />
            <FileData Type="Normal" Path="guiImage/ingame_waypoint_id_bar_on.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="redLine" ActionTag="1430794468" VisibleForFrame="False" Tag="213" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="30.0000" RightMargin="190.0000" TopMargin="49.0000" BottomMargin="49.0000" ClipAble="False" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="100.0000" Y="2.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="30.0000" Y="50.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.5000" />
            <PreSize X="0.3125" Y="0.0200" />
            <SingleColor A="255" R="255" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="firstDot" ActionTag="1029482383" Tag="211" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="11.5000" RightMargin="271.5000" TopMargin="31.5000" BottomMargin="31.5000" ctype="SpriteObjectData">
            <Size X="37.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="30.0000" Y="50.0000" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0938" Y="0.5000" />
            <PreSize X="0.1156" Y="0.3700" />
            <FileData Type="Normal" Path="guiImage/ingame_waypoint_id_on.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="lastDot" ActionTag="-881789927" Tag="212" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="271.5000" RightMargin="11.5000" TopMargin="31.5000" BottomMargin="31.5000" ctype="SpriteObjectData">
            <Size X="37.0000" Y="37.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="290.0000" Y="50.0000" />
            <Scale ScaleX="0.3000" ScaleY="0.3000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.9063" Y="0.5000" />
            <PreSize X="0.1156" Y="0.3700" />
            <FileData Type="Normal" Path="guiImage/ingame_waypoint_id_off.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="npcNode" ActionTag="649529096" Tag="59" IconVisible="True" PositionPercentYEnabled="True" LeftMargin="100.0000" RightMargin="220.0000" TopMargin="50.0000" BottomMargin="50.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <Children>
              <AbstractNodeData Name="npcIcon" ActionTag="1698075705" Tag="6" IconVisible="False" LeftMargin="-78.5000" RightMargin="-78.5000" TopMargin="-78.5000" BottomMargin="-78.5000" ctype="SpriteObjectData">
                <Size X="157.0000" Y="157.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="0.2500" ScaleY="0.2500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="guiImage/npc_icon_sample.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="frame" ActionTag="-89209790" Tag="58" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-50.5000" RightMargin="-50.5000" TopMargin="-50.5000" BottomMargin="-50.5000" ctype="SpriteObjectData">
                <Size X="101.0000" Y="101.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="guiImage/ingame_waypoint_frame.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="100.0000" Y="50.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3125" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="textPanel" ActionTag="-1878804448" Tag="62" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="1.0000" RightMargin="-1.0000" TopMargin="12.0000" BottomMargin="38.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_1" ActionTag="218963439" Tag="3178" IconVisible="False" LeftMargin="-19.5000" RightMargin="236.5000" TopMargin="5.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
                <Size X="103.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="32.0000" Y="30.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1000" Y="0.6000" />
                <PreSize X="0.1437" Y="0.9200" />
                <FileData Type="Normal" Path="guiImage/ingame_text_start.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_2" ActionTag="-388928602" Tag="3179" IconVisible="False" LeftMargin="239.2741" RightMargin="-14.2741" TopMargin="5.0000" BottomMargin="15.0000" ctype="SpriteObjectData">
                <Size X="95.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="286.7741" Y="30.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8962" Y="0.6000" />
                <PreSize X="0.1437" Y="0.9200" />
                <FileData Type="Normal" Path="guiImage/ingame_text_goal.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="1.0000" Y="38.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0031" Y="0.3800" />
            <PreSize X="1.0000" Y="0.5000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>