<GameFile>
  <PropertyGroup Name="InGameCheckPointLayer" Type="Layer" ID="18f4f8fd-7479-4ec5-b4dd-7d93b313c534" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="72" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="140.0000" />
        <Children>
          <AbstractNodeData Name="ingame_scoreoboard_1" ActionTag="1224997725" Tag="73" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-93.5000" RightMargin="-93.5000" TopMargin="-62.5000" BottomMargin="-62.5000" ctype="SpriteObjectData">
            <Size X="507.0000" Y="265.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="70.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.5844" Y="1.8929" />
            <FileData Type="Normal" Path="guiImage/ingame_scoreoboard.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="-1710152959" Tag="74" IconVisible="False" LeftMargin="64.5000" RightMargin="170.5000" TopMargin="52.5000" BottomMargin="51.5000" FontSize="26" LabelText="Time: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="85.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="107.0000" Y="69.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3344" Y="0.4964" />
            <PreSize X="0.2656" Y="0.2571" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeText" ActionTag="-1308436574" Tag="75" IconVisible="False" LeftMargin="88.9999" RightMargin="81.0001" TopMargin="40.2500" BottomMargin="44.7500" IsCustomSize="True" FontSize="40" LabelText="12.23&#xA;" HorizontalAlignmentType="HT_Right" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="150.0000" Y="55.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="163.9999" Y="72.2500" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5125" Y="0.5161" />
            <PreSize X="0.4688" Y="0.3929" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_1" ActionTag="-363566646" Tag="76" IconVisible="False" LeftMargin="189.0000" RightMargin="73.0000" TopMargin="52.5000" BottomMargin="51.5000" FontSize="26" LabelText="sec" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="58.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="218.0000" Y="69.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6812" Y="0.4964" />
            <PreSize X="0.1813" Y="0.2571" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1_2" ActionTag="944813460" Tag="77" IconVisible="False" LeftMargin="42.0203" RightMargin="194.9797" TopMargin="91.8334" BottomMargin="20.1666" FontSize="20" LabelText="Score: " ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="83.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="83.5203" Y="34.1666" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2610" Y="0.2440" />
            <PreSize X="0.2594" Y="0.2000" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="scoreText" ActionTag="2009722866" Tag="78" IconVisible="False" LeftMargin="79.5203" RightMargin="120.4797" TopMargin="91.8334" BottomMargin="20.1666" IsCustomSize="True" FontSize="20" LabelText="+1000&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="120.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="139.5203" Y="34.1666" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4360" Y="0.2440" />
            <PreSize X="0.3750" Y="0.2000" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="moneyText" ActionTag="-992400472" Tag="79" IconVisible="False" LeftMargin="174.5202" RightMargin="25.4799" TopMargin="91.8334" BottomMargin="20.1666" IsCustomSize="True" FontSize="20" LabelText="+1000&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="120.0000" Y="28.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="234.5202" Y="34.1666" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7329" Y="0.2440" />
            <PreSize X="0.3750" Y="0.2000" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="ui_general_coins_2" ActionTag="-239897157" Tag="80" IconVisible="False" LeftMargin="171.4715" RightMargin="104.5285" TopMargin="85.8334" BottomMargin="14.1666" ctype="SpriteObjectData">
            <Size X="44.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="193.4715" Y="34.1666" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6046" Y="0.2440" />
            <PreSize X="0.1375" Y="0.2857" />
            <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>