<GameFile>
  <PropertyGroup Name="NotEnoughMoneyDialog" Type="Layer" ID="16ad75c7-9d8d-4b9d-9042-68c6796f176f" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="417" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgPanel" ActionTag="1321950804" Tag="418" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="204" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-1411081369" Tag="440" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="16.0000" RightMargin="16.0000" TopMargin="152.2000" BottomMargin="265.8000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="288.0000" Y="150.0000" />
            <Children>
              <AbstractNodeData Name="bg" ActionTag="1070524087" Tag="441" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-141.0000" RightMargin="-141.0000" TopMargin="-150.0000" BottomMargin="-150.0000" Scale9Enable="True" LeftEage="198" RightEage="198" TopEage="48" BottomEage="200" Scale9OriginX="198" Scale9OriginY="48" Scale9Width="174" Scale9Height="100" ctype="ImageViewObjectData">
                <Size X="570.0000" Y="450.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="75.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.9792" Y="3.0000" />
                <FileData Type="Normal" Path="guiImage/ui_pause_bg.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="cancelBtnX" ActionTag="-1283583242" VisibleForFrame="False" Tag="442" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="95.2854" RightMargin="104.7146" TopMargin="97.6822" BottomMargin="-41.6822" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="58" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="88.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="139.2854" Y="5.3178" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4836" Y="0.0355" />
                <PreSize X="0.3056" Y="0.6267" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/btn_cancel.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="title" ActionTag="-1291708584" Tag="443" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="87.5000" RightMargin="87.5000" TopMargin="-22.0685" BottomMargin="120.0685" FontSize="30" LabelText="Oops!" VerticalAlignmentType="VT_Center" OutlineSize="5" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="113.0000" Y="52.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="146.0685" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9738" />
                <PreSize X="0.3924" Y="0.3467" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="content" ActionTag="-1896315867" Tag="444" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="49.1156" RightMargin="87.8844" TopMargin="29.2600" BottomMargin="92.7400" FontSize="20" LabelText="not enough" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="151.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="124.6156" Y="106.7400" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4327" Y="0.7116" />
                <PreSize X="0.5243" Y="0.1867" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="content_0" ActionTag="995199549" Tag="495" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="176.3129" RightMargin="51.6871" TopMargin="29.2600" BottomMargin="92.7400" IsCustomSize="True" FontSize="20" LabelText="!" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="206.3129" Y="106.7400" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7164" Y="0.7116" />
                <PreSize X="0.2083" Y="0.1867" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="diamondSprite" ActionTag="259369685" Tag="445" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="152.3129" RightMargin="83.6871" TopMargin="17.5200" BottomMargin="87.4800" ctype="SpriteObjectData">
                <Size X="52.0000" Y="45.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="178.3129" Y="109.9800" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6191" Y="0.7332" />
                <PreSize X="0.1806" Y="0.3000" />
                <FileData Type="Normal" Path="guiImage/ui_general_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="starSprite" ActionTag="-196799449" Tag="446" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="154.6155" RightMargin="89.3845" TopMargin="21.2650" BottomMargin="88.7350" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="176.6155" Y="108.7350" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6132" Y="0.7249" />
                <PreSize X="0.1528" Y="0.2667" />
                <FileData Type="Normal" Path="guiImage/ui_general_coins.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="puzzleSprite" ActionTag="-352592612" Tag="114" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="156.0000" RightMargin="88.0000" TopMargin="20.6799" BottomMargin="89.3201" ctype="SpriteObjectData">
                <Size X="44.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="178.0000" Y="109.3201" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6181" Y="0.7288" />
                <PreSize X="0.1528" Y="0.2667" />
                <FileData Type="Normal" Path="guiImage/ui_general_fragment.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="shopBtn" ActionTag="-617612655" Tag="447" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-32.5000" RightMargin="-32.5000" TopMargin="49.2626" BottomMargin="6.7374" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="-1080229460" Tag="448" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="103.5000" RightMargin="103.5000" TopMargin="31.0000" BottomMargin="31.0000" FontSize="20" LabelText="go to shop" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="146.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4136" Y="0.3404" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="53.7374" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3582" />
                <PreSize X="1.2257" Y="0.6267" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="cancelBtn" ActionTag="959638241" Tag="496" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-32.5000" RightMargin="-32.5000" TopMargin="102.8990" BottomMargin="-46.8990" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="-401482883" Tag="497" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="95.5000" RightMargin="95.5000" TopMargin="31.0000" BottomMargin="31.0000" FontSize="20" LabelText="maybe later" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="162.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.4589" Y="0.3404" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="77" G="77" B="77" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="0.1010" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0007" />
                <PreSize X="1.2257" Y="0.6267" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_off.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="drawBtn" ActionTag="-1179883848" Tag="115" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-32.5000" RightMargin="-32.5000" TopMargin="49.2600" BottomMargin="6.7400" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="-90753371" Tag="116" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="76.0000" RightMargin="76.0000" TopMargin="31.0000" BottomMargin="31.0000" FontSize="20" LabelText="try lucky draw" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="201.0000" Y="32.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="176.5000" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.5694" Y="0.3404" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="144.0000" Y="53.7400" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3583" />
                <PreSize X="1.2257" Y="0.6267" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="340.8000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6000" />
            <PreSize X="0.9000" Y="0.2641" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>