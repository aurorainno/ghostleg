<GameFile>
  <PropertyGroup Name="TutorialNpcDialog" Type="Layer" ID="619176ec-03c9-4bf9-82f8-cdcee0239abe" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="55" Speed="1.0000" ActivedAnimationName="closeDown">
        <Timeline ActionTag="-1520727766" Property="Scale">
          <ScaleFrame FrameIndex="5" X="0.0100" Y="0.0100">
            <EasingData Type="26" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="20" X="0.5000" Y="0.5000">
            <EasingData Type="0" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="40" X="1.0000" Y="1.0000">
            <EasingData Type="25" />
          </ScaleFrame>
          <ScaleFrame FrameIndex="55" X="0.0100" Y="0.0100">
            <EasingData Type="0" />
          </ScaleFrame>
        </Timeline>
        <Timeline ActionTag="-1520727766" Property="VisibleForFrame">
          <BoolFrame FrameIndex="0" Tween="False" Value="False" />
          <BoolFrame FrameIndex="5" Tween="False" Value="True" />
          <BoolFrame FrameIndex="20" Tween="False" Value="True" />
          <BoolFrame FrameIndex="40" Tween="False" Value="True" />
          <BoolFrame FrameIndex="55" Tween="False" Value="True" />
        </Timeline>
      </Animation>
      <AnimationList>
        <AnimationInfo Name="default" StartIndex="0" EndIndex="0">
          <RenderColor A="255" R="255" G="99" B="71" />
        </AnimationInfo>
        <AnimationInfo Name="showUp" StartIndex="5" EndIndex="20">
          <RenderColor A="255" R="173" G="255" B="47" />
        </AnimationInfo>
        <AnimationInfo Name="closeDown" StartIndex="40" EndIndex="55">
          <RenderColor A="255" R="245" G="255" B="250" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Layer" Tag="77" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="320.0000" />
        <Children>
          <AbstractNodeData Name="npc" ActionTag="1071683709" Tag="78" IconVisible="False" LeftMargin="-184.5008" RightMargin="89.5008" TopMargin="54.4999" BottomMargin="-133.4999" ctype="SpriteObjectData">
            <Size X="415.0000" Y="399.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="22.9992" Y="66.0001" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0719" Y="0.2063" />
            <PreSize X="1.2969" Y="1.2469" />
            <FileData Type="Normal" Path="gui/tutorial/image/tutorial_boss.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="dialogSprite" ActionTag="-1520727766" Tag="79" IconVisible="False" LeftMargin="-32.3804" RightMargin="-61.6196" TopMargin="0.7100" BottomMargin="-25.7100" ctype="SpriteObjectData">
            <Size X="414.0000" Y="345.0000" />
            <Children>
              <AbstractNodeData Name="dialogMsg" ActionTag="1124089554" Tag="80" IconVisible="False" LeftMargin="35.0000" RightMargin="39.0000" TopMargin="61.9998" BottomMargin="143.0002" IsCustomSize="True" FontSize="28" LabelText="Collect Puzzle to unlock and upgrade couriers" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="340.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="205.0000" Y="213.0002" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="0" G="0" B="0" />
                <PrePosition X="0.4952" Y="0.6174" />
                <PreSize X="0.8213" Y="0.4058" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.7000" />
            <Position X="257.4196" Y="-25.7100" />
            <Scale ScaleX="0.5342" ScaleY="0.5342" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8044" Y="-0.0803" />
            <PreSize X="1.2938" Y="1.0781" />
            <FileData Type="Normal" Path="gui/tutorial/image/tutorial_dialog.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>