<GameFile>
  <PropertyGroup Name="LuckyDrawDialog" Type="Layer" ID="923cee55-0c75-45ba-9717-5867ca0d3cc4" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="474" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="coverPanel" ActionTag="-995601678" Tag="1278" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-1806053058" Tag="476" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="TopEdge" LeftMargin="8.0000" RightMargin="8.0000" TopMargin="77.6000" BottomMargin="149.6000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="304.0000" Y="340.8000" />
            <Children>
              <AbstractNodeData Name="Sprite_1" ActionTag="-962650660" Tag="1222" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-168.0000" RightMargin="-168.0000" TopMargin="-37.9992" BottomMargin="-656.2008" ctype="SpriteObjectData">
                <Size X="640.0000" Y="1035.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                <Position X="152.0000" Y="378.7992" />
                <Scale ScaleX="0.5000" ScaleY="0.5500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="1.1115" />
                <PreSize X="2.1053" Y="3.0370" />
                <FileData Type="Normal" Path="guiImage/ui_bg1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="backBtn" ActionTag="-190800401" Tag="1223" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-26.1329" RightMargin="194.1329" TopMargin="-34.7000" BottomMargin="290.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="136.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="41.8671" Y="333.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1377" Y="0.9771" />
                <PreSize X="0.4474" Y="0.2494" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="bgFrame" ActionTag="520071853" Tag="1225" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="2.0000" RightMargin="2.0000" TopMargin="45.8000" BottomMargin="85.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="300.0000" Y="210.0000" />
                <Children>
                  <AbstractNodeData Name="bgSprite01" ActionTag="559912844" Tag="479" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-145.0000" RightMargin="-145.0000" TopMargin="-110.0000" BottomMargin="-110.0000" ctype="SpriteObjectData">
                    <Size X="590.0000" Y="430.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="150.0000" Y="105.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.9667" Y="2.0476" />
                    <FileData Type="Normal" Path="guiImage/poster1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bgSprite02" ActionTag="-582577092" VisibleForFrame="False" Tag="1224" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-145.0000" RightMargin="-145.0000" TopMargin="-110.0000" BottomMargin="-110.0000" ctype="SpriteObjectData">
                    <Size X="590.0000" Y="430.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="150.0000" Y="105.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.9667" Y="2.0476" />
                    <FileData Type="Normal" Path="guiImage/poster2.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="bgSprite03" ActionTag="-792631552" VisibleForFrame="False" Tag="1227" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-145.0000" RightMargin="-145.0000" TopMargin="-110.0000" BottomMargin="-110.0000" ctype="SpriteObjectData">
                    <Size X="590.0000" Y="430.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="150.0000" Y="105.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.9667" Y="2.0476" />
                    <FileData Type="Normal" Path="guiImage/poster3.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="190.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5575" />
                <PreSize X="0.9868" Y="0.6162" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="purchaseBtn" ActionTag="1645084925" Tag="491" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-24.5000" RightMargin="-24.5000" TopMargin="302.8000" BottomMargin="-56.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="title" ActionTag="-1491361872" Tag="492" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="157.5900" RightMargin="136.4100" TopMargin="28.5000" BottomMargin="28.5000" FontSize="24" LabelText="100" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="59.0000" Y="37.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="187.0900" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5300" Y="0.5000" />
                    <PreSize X="0.1671" Y="0.3936" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="diamondSprite" ActionTag="35150587" Tag="494" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="101.0800" RightMargin="199.9200" TopMargin="21.5860" BottomMargin="27.4140" ctype="SpriteObjectData">
                    <Size X="52.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="127.0800" Y="49.9140" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.3600" Y="0.5310" />
                    <PreSize X="0.1473" Y="0.4787" />
                    <FileData Type="Normal" Path="guiImage/ui_general_diamond.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="-9.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.0264" />
                <PreSize X="1.1612" Y="0.2758" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="useAdButton" ActionTag="375128158" Tag="246" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-24.5000" RightMargin="-24.5000" TopMargin="379.8000" BottomMargin="-133.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <Children>
                  <AbstractNodeData Name="videoSprite" ActionTag="279483322" Tag="247" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="65.5329" RightMargin="235.4671" TopMargin="25.3648" BottomMargin="23.6352" ctype="SpriteObjectData">
                    <Size X="52.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="91.5329" Y="46.1352" />
                    <Scale ScaleX="0.8000" ScaleY="0.8000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.2593" Y="0.4908" />
                    <PreSize X="0.1473" Y="0.4787" />
                    <FileData Type="Normal" Path="guiImage/ui_general_video.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="text" ActionTag="-325272987" Tag="248" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="139.6500" RightMargin="104.3500" TopMargin="32.5000" BottomMargin="32.5000" FontSize="18" LabelText="watch ad" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="109.0000" Y="29.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="194.1500" Y="47.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5500" Y="0.5000" />
                    <PreSize X="0.3088" Y="0.3085" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="18" G="151" B="72" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="-86.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="-0.2523" />
                <PreSize X="1.1612" Y="0.2758" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Normal" Path="guiImage/ui_general_btn_off.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_22" ActionTag="99959918" Tag="570" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="62.0000" RightMargin="62.0000" TopMargin="-13.2000" BottomMargin="313.0000" FontSize="24" LabelText="lucky draw" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="180.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="333.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9786" />
                <PreSize X="0.5921" Y="0.1203" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="23" G="114" B="151" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_22_0" ActionTag="-1727336952" Tag="571" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-94.0000" RightMargin="-94.0000" TopMargin="273.8000" BottomMargin="39.0000" FontSize="20" LabelText="Try your luck to get the big rewards!" VerticalAlignmentType="VT_Center" OutlineSize="5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="492.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="152.0000" Y="53.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.1555" />
                <PreSize X="1.6184" Y="0.0822" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="320.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5634" />
            <PreSize X="0.9500" Y="0.6000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ui_main_topbar_1" ActionTag="1727805366" Tag="1221" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="476.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="92.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="2.0000" Y="0.1620" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbBtn" ActionTag="425535124" VisibleForFrame="False" Tag="1213" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-35.0650" RightMargin="163.0650" TopMargin="-2.0000" BottomMargin="513.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="192.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.9350" Y="541.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1904" Y="0.9533" />
            <PreSize X="0.6000" Y="0.1004" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_fb.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondPanel" ActionTag="61569345" Tag="1214" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="529.0100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="25.0000" />
            <Children>
              <AbstractNodeData Name="sprite" ActionTag="-1947277784" Tag="1215" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="162.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="60.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.3500" Y="1.6000" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalDiamondText" ActionTag="389545700" Tag="1216" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="120.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="90.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.5000" />
                <PreSize X="1.0000" Y="1.6000" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" />
            <Position X="335.0000" Y="529.0100" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0469" Y="0.9314" />
            <PreSize X="0.3750" Y="0.0440" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="-390166388" Tag="1218" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="523.0575" ctype="SpriteObjectData">
            <Size X="166.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="185.1505" Y="541.5575" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5786" Y="0.9534" />
            <PreSize X="0.5188" Y="0.0704" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="-2026514883" Tag="1217" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="7.6674" BottomMargin="520.3326" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="214.3221" Y="540.3326" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6698" Y="0.9513" />
            <PreSize X="0.3125" Y="0.0704" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="0" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbName" ActionTag="1180044693" Tag="1219" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="56.0000" RightMargin="231.0000" TopMargin="11.0000" BottomMargin="523.0000" FontSize="20" LabelText="CC" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="33.0000" Y="34.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="56.0000" Y="540.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1750" Y="0.9507" />
            <PreSize X="0.1031" Y="0.0599" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="182" G="88" B="14" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="profilePicFrame" ActionTag="-681941616" Tag="1220" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="15.9350" RightMargin="252.0650" TopMargin="-0.2800" BottomMargin="516.2800" ctype="SpriteObjectData">
            <Size X="52.0000" Y="52.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="15.9350" Y="542.2800" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0498" Y="0.9547" />
            <PreSize X="0.1625" Y="0.0915" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>