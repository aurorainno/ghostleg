<GameFile>
  <PropertyGroup Name="MainScreenLockedInfoLayer" Type="Layer" ID="9eb3bcc5-22c2-402b-8b50-aece027d516b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="88" ctype="GameLayerObjectData">
        <Size X="271.0000" Y="65.0000" />
        <Children>
          <AbstractNodeData Name="lockedInfoPanel" ActionTag="604469826" Tag="219" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="21.6800" RightMargin="21.6800" TopMargin="7.5000" BottomMargin="7.5000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="227.6400" Y="50.0000" />
            <Children>
              <AbstractNodeData Name="Image_2" ActionTag="1422740854" Tag="220" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-159.1800" RightMargin="-159.1800" TopMargin="-36.5000" BottomMargin="-38.5000" Scale9Enable="True" LeftEage="180" RightEage="180" TopEage="41" BottomEage="41" Scale9OriginX="180" Scale9OriginY="41" Scale9Width="186" Scale9Height="45" ctype="ImageViewObjectData">
                <Size X="546.0000" Y="125.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="113.8200" Y="24.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4800" />
                <PreSize X="2.3985" Y="2.5000" />
                <FileData Type="Normal" Path="guiImage/ui_menu_mission.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="sprite" ActionTag="2089473023" Tag="221" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-45.0000" RightMargin="160.6400" TopMargin="-39.5000" BottomMargin="-37.5000" ctype="SpriteObjectData">
                <Size X="112.0000" Y="127.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="11.0000" Y="26.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0483" Y="0.5200" />
                <PreSize X="0.4920" Y="2.5400" />
                <FileData Type="Normal" Path="guiImage/ui_menu_lightbulb.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="text" ActionTag="741233106" Tag="222" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="41.0000" RightMargin="-63.3600" TopMargin="-9.0000" BottomMargin="-5.0000" IsCustomSize="True" FontSize="25" LabelText="Want to unlock&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="250.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="41.0000" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1801" Y="0.5400" />
                <PreSize X="1.0982" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_0" ActionTag="334485409" Tag="223" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="178.8900" RightMargin="-327.2500" TopMargin="-9.0000" BottomMargin="-5.0000" IsCustomSize="True" FontSize="25" LabelText="?" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="376.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="178.8900" Y="27.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7858" Y="0.5400" />
                <PreSize X="1.6517" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetName" ActionTag="-1551266848" Tag="224" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="105.0000" RightMargin="22.6400" TopMargin="-1.0000" BottomMargin="19.0000" IsCustomSize="True" FontSize="25" LabelText="Mars" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="100.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="155.0000" Y="35.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="59" G="255" B="215" />
                <PrePosition X="0.6809" Y="0.7000" />
                <PreSize X="0.4393" Y="0.6400" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="text_1" ActionTag="41409541" Tag="225" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="41.0000" RightMargin="-213.3600" TopMargin="12.5000" BottomMargin="-26.5000" IsCustomSize="True" FontSize="25" LabelText="Check the        menu below!&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="400.0000" Y="64.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="41.0000" Y="5.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="211" B="49" />
                <PrePosition X="0.1801" Y="0.1100" />
                <PreSize X="1.7572" Y="1.2800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ui_menu_emoji_dog_1" ActionTag="-1988442729" Tag="226" IconVisible="False" LeftMargin="106.2300" RightMargin="91.4100" TopMargin="21.2785" BottomMargin="-1.2785" ctype="SpriteObjectData">
                <Size X="30.0000" Y="30.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="121.2300" Y="13.7215" />
                <Scale ScaleX="0.7000" ScaleY="0.7000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5326" Y="0.2744" />
                <PreSize X="0.1318" Y="0.6000" />
                <FileData Type="Normal" Path="guiImage/ui_menu_emoji_dog.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="135.5000" Y="32.5000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.8400" Y="0.7692" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>