<GameFile>
  <PropertyGroup Name="SuitScene" Type="Layer" ID="8951ee08-98b1-4510-81d9-3f2f3132069e" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="92" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="ScrollView" ActionTag="720039083" Tag="22" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" RightMargin="-64.0000" TopMargin="63.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" IsBounceEnabled="True" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
            <Size X="384.0000" Y="505.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="505.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8891" />
            <PreSize X="1.2000" Y="0.8891" />
            <SingleColor A="255" R="255" G="150" B="100" />
            <FirstColor A="255" R="255" G="150" B="100" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
            <InnerNodeSize Width="383" Height="1650" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_3" ActionTag="1432337376" Tag="96" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="117.5000" RightMargin="117.5000" TopMargin="5.1800" BottomMargin="511.8200" FontSize="40" LabelText="suits" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="85.0000" Y="51.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="537.3200" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.5000" Y="0.9460" />
            <PreSize X="0.2656" Y="0.0898" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="backBtn" ActionTag="740598134" Tag="97" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-48.8000" RightMargin="168.8000" TopMargin="-25.5117" BottomMargin="477.5117" TouchEnable="True" FontSize="36" ButtonText="back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="170" Scale9Height="94" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="200.0000" Y="116.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="51.2000" Y="535.5117" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1600" Y="0.9428" />
            <PreSize X="0.6250" Y="0.2042" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <TextColor A="255" R="49" G="216" B="212" />
            <NormalFileData Type="Normal" Path="btn_back.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="-203057743" Tag="72" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="234.4160" RightMargin="-14.4160" TopMargin="14.5004" BottomMargin="520.4996" IsCustomSize="True" FontSize="30" LabelText="99999" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="33.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="284.4160" Y="536.9996" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="49" G="216" B="212" />
            <PrePosition X="0.8888" Y="0.9454" />
            <PreSize X="0.3125" Y="0.0581" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_1" ActionTag="-1941313632" Tag="73" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="193.7578" RightMargin="18.2422" TopMargin="-32.7198" BottomMargin="492.7198" ctype="SpriteObjectData">
            <Size X="108.0000" Y="108.0000" />
            <AnchorPoint ScaleX="0.5115" ScaleY="0.4100" />
            <Position X="248.9998" Y="536.9998" />
            <Scale ScaleX="0.1500" ScaleY="0.1500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.7781" Y="0.9454" />
            <PreSize X="0.3375" Y="0.1901" />
            <FileData Type="Normal" Path="star_icon.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>