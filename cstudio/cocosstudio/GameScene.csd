<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="970d97e1-c928-4245-880e-64e6006ff433" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <AnimationList>
        <AnimationInfo Name="popStarLabel" StartIndex="0" EndIndex="20">
          <RenderColor A="255" R="0" G="255" B="255" />
        </AnimationInfo>
      </AnimationList>
      <ObjectData Name="Scene" Tag="44" ctype="GameNodeObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgSprite" Visible="False" ActionTag="-1898882656" VisibleForFrame="False" Tag="45" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-284.0000" BottomMargin="-284.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="1136.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.6050" ScaleY="0.5500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="2.0000" />
            <FileData Type="Normal" Path="background.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="worldParent" ActionTag="-1946232806" Tag="15" IconVisible="True" RightMargin="320.0000" TopMargin="568.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="uiPanel" ActionTag="651250614" Tag="11" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-0.9600" RightMargin="0.9600" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <Children>
              <AbstractNodeData Name="debugText" ActionTag="-655308109" Tag="21" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="6.7094" RightMargin="113.2906" TopMargin="118.0471" BottomMargin="349.9529" IsCustomSize="True" FontSize="12" LabelText="Debug Info&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="200.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="106.7094" Y="399.9529" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.3335" Y="0.7041" />
                <PreSize X="0.6250" Y="0.1761" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="speedText" ActionTag="-1710053215" VisibleForFrame="False" Tag="119" IconVisible="False" LeftMargin="21.0000" RightMargin="175.0000" TopMargin="57.5000" BottomMargin="481.5000" FontSize="25" LabelText="speed: 120" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="124.0000" Y="29.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="21.0000" Y="496.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0656" Y="0.8732" />
                <PreSize X="0.3875" Y="0.0511" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="topPanel" ActionTag="524140277" Tag="62" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="518.0000" ClipAble="False" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="npcPanel" ActionTag="2012040326" Tag="748" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" RightMargin="190.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="130.0000" Y="50.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite_5" ActionTag="623497420" Tag="2530" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" RightMargin="-94.0000" TopMargin="-4.0000" BottomMargin="19.0000" ctype="SpriteObjectData">
                        <Size X="224.0000" Y="35.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position Y="36.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition Y="0.7300" />
                        <PreSize X="1.7231" Y="0.7000" />
                        <FileData Type="Normal" Path="guiImage/ui_ingame_namelabel.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="npcIcon" ActionTag="128510016" Tag="479" IconVisible="False" LeftMargin="-52.5000" RightMargin="25.5000" TopMargin="-33.5000" BottomMargin="-73.5000" ctype="SpriteObjectData">
                        <Size X="157.0000" Y="157.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="26.0000" Y="5.0000" />
                        <Scale ScaleX="0.2500" ScaleY="0.2500" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2000" Y="0.1000" />
                        <PreSize X="1.2077" Y="3.1400" />
                        <FileData Type="Normal" Path="guiImage/ingame_mission_npc_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="npcNameText" ActionTag="1095751384" Tag="469" IconVisible="False" LeftMargin="9.1068" RightMargin="-26.1068" TopMargin="2.0000" BottomMargin="22.0000" FontSize="16" LabelText="1st customer" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="147.0000" Y="26.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="9.1068" Y="35.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="247" G="231" B="80" />
                        <PrePosition X="0.0701" Y="0.7000" />
                        <PreSize X="1.1308" Y="0.5200" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="28" B="75" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="distanceText" ActionTag="1270718663" Tag="480" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="50.7000" RightMargin="24.3000" TopMargin="15.0000" BottomMargin="-13.0000" FontSize="29" LabelText="22" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="55.0000" Y="48.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="50.7000" Y="11.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3900" Y="0.2200" />
                        <PreSize X="0.4231" Y="0.9600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="28" B="75" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="distanceUnitText" ActionTag="-2112025490" Tag="481" IconVisible="False" LeftMargin="57.5000" RightMargin="43.5000" TopMargin="15.0000" BottomMargin="-1.0000" IsCustomSize="True" FontSize="16" LabelText="m" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="29.0000" Y="36.0000" />
                        <AnchorPoint ScaleX="0.5000" />
                        <Position X="72.0000" Y="-1.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5538" Y="-0.0200" />
                        <PreSize X="0.2231" Y="0.7200" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="28" B="75" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="away" ActionTag="1565778368" Tag="2504" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="65.0520" RightMargin="7.9480" TopMargin="41.0000" BottomMargin="-21.0000" FontSize="16" LabelText="away" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="57.0000" Y="30.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="65.0520" Y="-6.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5004" Y="-0.1200" />
                        <PreSize X="0.4385" Y="0.6000" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="28" B="75" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="timeUsedText" ActionTag="649666106" Tag="64" IconVisible="False" LeftMargin="-28.0000" RightMargin="8.0000" TopMargin="62.0000" BottomMargin="-42.0000" IsCustomSize="True" FontSize="20" LabelText="00.23&quot;" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="150.0000" Y="30.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="47.0000" Y="-27.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3615" Y="-0.5400" />
                        <PreSize X="1.1538" Y="0.6000" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="0" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.4063" Y="1.0000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="timePanel" ActionTag="-1167992580" VisibleForFrame="False" Tag="749" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="110.0000" RightMargin="110.0000" TopMargin="77.9999" BottomMargin="-127.9999" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="100.0000" Y="100.0000" />
                    <Children>
                      <AbstractNodeData Name="timeText" ActionTag="-1637104873" Alpha="127" Tag="46" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-70.0000" RightMargin="-70.0000" TopMargin="-15.5000" BottomMargin="-15.5000" IsCustomSize="True" FontSize="60" LabelText="75" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="8.0000" ShadowOffsetY="-8.0000" ctype="TextObjectData">
                        <Size X="240.0000" Y="131.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="50.0000" Y="50.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="165" B="0" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="2.4000" Y="1.3100" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="28" B="75" />
                        <ShadowColor A="255" R="77" G="77" B="77" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="distanceBar" ActionTag="1219766140" VisibleForFrame="False" Tag="87" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="5.3300" RightMargin="-5.3300" TopMargin="-1.5500" BottomMargin="1.5500" ctype="SpriteObjectData">
                        <Size X="100.0000" Y="100.0000" />
                        <AnchorPoint ScaleX="0.4467" ScaleY="0.4845" />
                        <Position X="50.0000" Y="50.0000" />
                        <Scale ScaleX="0.6000" ScaleY="0.6000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="guiImage/ui_meters_0.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="unitText" ActionTag="-47549967" VisibleForFrame="False" Tag="59" IconVisible="False" LeftMargin="59.1015" RightMargin="11.8985" TopMargin="26.9800" BottomMargin="37.0200" IsCustomSize="True" FontSize="16" LabelText="s" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="29.0000" Y="36.0000" />
                        <AnchorPoint ScaleX="0.5000" />
                        <Position X="73.6015" Y="37.0200" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7360" Y="0.3702" />
                        <PreSize X="0.2900" Y="0.3600" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="1" G="28" B="75" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="160.0000" Y="-127.9999" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="-2.5600" />
                    <PreSize X="0.3125" Y="2.0000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="blockPanel" ActionTag="1706335980" VisibleForFrame="False" Tag="2012" IconVisible="False" LeftMargin="6.1325" RightMargin="288.8675" TopMargin="85.8231" BottomMargin="-60.8231" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="25.0000" Y="25.0000" />
                    <Children>
                      <AbstractNodeData Name="iconSprite" ActionTag="782428822" Tag="2013" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-5.5000" RightMargin="-5.5000" TopMargin="-9.5000" BottomMargin="-9.5000" ctype="SpriteObjectData">
                        <Size X="36.0000" Y="44.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="12.5000" Y="12.5000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.5000" />
                        <PreSize X="1.4400" Y="1.7600" />
                        <FileData Type="Normal" Path="guiImage/ingame_shield1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="countText" ActionTag="2089345450" Tag="2014" IconVisible="False" LeftMargin="8.7216" RightMargin="-1.7216" TopMargin="5.8295" BottomMargin="-8.8295" FontSize="20" LabelText="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                        <Size X="18.0000" Y="28.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="17.7216" Y="5.1705" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7089" Y="0.2068" />
                        <PreSize X="0.7200" Y="1.1200" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="6.1325" Y="-60.8231" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0192" Y="-1.2165" />
                    <PreSize X="0.0781" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="scorelabel" ActionTag="-2132403238" Tag="2506" IconVisible="False" LeftMargin="214.5000" RightMargin="38.5000" TopMargin="6.7624" BottomMargin="13.2376" IsCustomSize="True" FontSize="16" LabelText="score" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="67.0000" Y="30.0000" />
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="248.0000" Y="13.2376" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7750" Y="0.2648" />
                    <PreSize X="0.2094" Y="0.6000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="1" G="28" B="75" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="scoreText" ActionTag="-2029207276" Tag="36" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="110.0000" RightMargin="10.0000" TopMargin="7.0000" BottomMargin="2.0000" IsCustomSize="True" FontSize="20" LabelText="0" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="200.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="310.0000" Y="22.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9688" Y="0.4500" />
                    <PreSize X="0.6250" Y="0.8200" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="1" G="28" B="75" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="coinText" ActionTag="-2021420152" Tag="9" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="210.0000" RightMargin="10.0000" TopMargin="32.9140" BottomMargin="-23.9140" IsCustomSize="True" FontSize="20" LabelText="1" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="310.0000" Y="-3.4140" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9688" Y="-0.0683" />
                    <PreSize X="0.3125" Y="0.8200" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="1" G="28" B="75" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="puzzleText" ActionTag="1021103956" Tag="131" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="210.0000" RightMargin="10.0000" TopMargin="56.6777" BottomMargin="-47.6777" IsCustomSize="True" FontSize="20" LabelText="1" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="100.0000" Y="41.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="310.0000" Y="-27.1777" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9688" Y="-0.5436" />
                    <PreSize X="0.3125" Y="0.8200" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="1" G="28" B="75" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="moneySprite" ActionTag="277321278" Tag="2526" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="233.7347" RightMargin="42.2653" TopMargin="30.9140" BottomMargin="-25.9140" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="255.7347" Y="-3.4140" />
                    <Scale ScaleX="0.4000" ScaleY="0.4000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.7992" Y="-0.0683" />
                    <PreSize X="0.1375" Y="0.9000" />
                    <FileData Type="Normal" Path="guiImage/ui_ingame_coincounter.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="puzzleSprite" ActionTag="-1896933111" Tag="130" IconVisible="False" LeftMargin="234.9591" RightMargin="41.0409" TopMargin="54.6777" BottomMargin="-49.6777" ctype="SpriteObjectData">
                    <Size X="44.0000" Y="45.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="256.9591" Y="-27.1777" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.8030" Y="-0.5436" />
                    <PreSize X="0.1375" Y="0.9000" />
                    <FileData Type="Normal" Path="guiImage/ui_ingame_puzzle.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="testSprite" ActionTag="1335198590" VisibleForFrame="False" Tag="88" IconVisible="False" LeftMargin="120.0000" RightMargin="100.0000" TopMargin="-11.0000" BottomMargin="-39.0000" ctype="SpriteObjectData">
                    <Size X="100.0000" Y="100.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="170.0000" Y="11.0000" />
                    <Scale ScaleX="0.6000" ScaleY="0.6000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5313" Y="0.2200" />
                    <PreSize X="0.3125" Y="2.0000" />
                    <FileData Type="Normal" Path="guiImage/ui_meters_1.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="candyPanel" Visible="False" ActionTag="-1044171130" Tag="596" IconVisible="False" VerticalEdge="TopEdge" RightMargin="120.0000" TopMargin="45.0000" BottomMargin="-20.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="200.0000" Y="25.0000" />
                    <Children>
                      <AbstractNodeData Name="candySprite" ActionTag="1373253115" Tag="597" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="3.3814" RightMargin="163.6186" TopMargin="-19.7500" BottomMargin="-15.2500" ctype="SpriteObjectData">
                        <Size X="33.0000" Y="60.0000" />
                        <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                        <Position X="22.0000" Y="12.5000" />
                        <Scale ScaleX="0.3000" ScaleY="0.3000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1100" Y="0.5000" />
                        <PreSize X="0.1650" Y="2.4000" />
                        <FileData Type="Normal" Path="guiImage/item_candystick.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="totalCandyText" ActionTag="-1892556995" Tag="598" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="33.3000" RightMargin="13.7000" TopMargin="-25.5000" BottomMargin="-25.5000" IsCustomSize="True" FontSize="60" LabelText="9999" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="153.0000" Y="76.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="33.3000" Y="12.5000" />
                        <Scale ScaleX="0.2500" ScaleY="0.2500" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1665" Y="0.5000" />
                        <PreSize X="0.7650" Y="3.0400" />
                        <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position Y="-20.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="-0.4000" />
                    <PreSize X="0.6250" Y="0.5000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="testDialougeSprite" ActionTag="1001700721" VisibleForFrame="False" Tag="496" IconVisible="False" LeftMargin="33.0000" RightMargin="118.0000" TopMargin="52.0000" BottomMargin="-150.0000" ctype="SpriteObjectData">
                    <Size X="169.0000" Y="148.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="33.0000" Y="-2.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1031" Y="-0.0400" />
                    <PreSize X="0.5281" Y="2.9600" />
                    <FileData Type="Normal" Path="guiImage/ingame_dialog_06.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="customerPanel" ActionTag="-751519688" Tag="747" IconVisible="False" RightMargin="190.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="130.0000" Y="50.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position Y="50.0000" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition Y="1.0000" />
                    <PreSize X="0.4063" Y="1.0000" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="1.0000" />
                <Position Y="568.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="1.0000" />
                <PreSize X="1.0000" Y="0.0880" />
                <SingleColor A="255" R="21" G="21" B="80" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="energyPanel" ActionTag="1825371450" VisibleForFrame="False" Tag="173" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="17.8240" RightMargin="82.1760" TopMargin="541.0001" BottomMargin="8.9999" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Enable="True" LeftEage="187" RightEage="187" TopEage="15" BottomEage="15" Scale9OriginX="-187" Scale9OriginY="-15" Scale9Width="374" Scale9Height="30" ctype="PanelObjectData">
                <Size X="220.0000" Y="18.0000" />
                <Children>
                  <AbstractNodeData Name="energyBar" ActionTag="581546404" Tag="32" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="10.5781" RightMargin="-246.9681" TopMargin="-9.4444" BottomMargin="2.4444" ProgressInfo="100" ctype="LoadingBarObjectData">
                    <Size X="456.3900" Y="25.0000" />
                    <AnchorPoint />
                    <Position X="10.5781" Y="2.4444" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="229" G="229" B="229" />
                    <PrePosition X="0.0481" Y="0.1358" />
                    <PreSize X="2.0745" Y="1.3889" />
                    <ImageFileData Type="Normal" Path="guiImage/energy_bar_energy.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Image_1" ActionTag="-636101660" Tag="150" IconVisible="False" LeftMargin="-0.0332" RightMargin="-259.9668" TopMargin="-14.9905" BottomMargin="-9.0095" Scale9Enable="True" LeftEage="171" RightEage="171" TopEage="13" BottomEage="13" Scale9OriginX="171" Scale9OriginY="13" Scale9Width="177" Scale9Height="16" ctype="ImageViewObjectData">
                    <Size X="480.0000" Y="42.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="-0.0332" Y="11.9905" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="-0.0002" Y="0.6661" />
                    <PreSize X="2.1818" Y="2.3333" />
                    <FileData Type="Normal" Path="guiImage/energy_bar_border.png" Plist="" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="energyText" ActionTag="-1586180187" Tag="174" IconVisible="False" HorizontalEdge="RightEdge" LeftMargin="-141.0001" RightMargin="1.0001" TopMargin="-29.9700" BottomMargin="-18.0300" IsCustomSize="True" FontSize="48" LabelText="20/100" HorizontalAlignmentType="HT_Right" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="360.0000" Y="66.0000" />
                    <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                    <Position X="218.9999" Y="14.9700" />
                    <Scale ScaleX="0.2500" ScaleY="0.2500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9955" Y="0.8317" />
                    <PreSize X="1.6364" Y="3.6667" />
                    <FontResource Type="Normal" Path="Avenir Next LT Pro Demi.ttf" Plist="" />
                    <OutlineColor A="255" R="38" G="164" B="142" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="energyParticle" ActionTag="-1682221856" Tag="29" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="110.0000" RightMargin="110.0000" TopMargin="9.0000" BottomMargin="9.0000" ctype="ParticleObjectData">
                    <Size X="0.0000" Y="0.0000" />
                    <AnchorPoint />
                    <Position X="110.0000" Y="9.0000" />
                    <Scale ScaleX="0.3700" ScaleY="0.3700" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Normal" Path="particle_resourcebar.plist" Plist="" />
                    <BlendFunc Src="770" Dst="1" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="17.8240" Y="8.9999" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0557" Y="0.0158" />
                <PreSize X="0.6875" Y="0.0317" />
                <SingleColor A="255" R="49" G="216" B="212" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="capsuleButton" ActionTag="194747578" Tag="37" IconVisible="False" LeftMargin="220.0000" TopMargin="468.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="-15" Scale9OriginY="-11" Scale9Width="30" Scale9Height="22" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="100.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="270.0000" Y="50.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8438" Y="0.0880" />
                <PreSize X="0.3125" Y="0.1761" />
                <TextColor A="255" R="65" G="65" B="70" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="pauseButton" ActionTag="900992250" Tag="47" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="BottomEdge" LeftMargin="3.0000" RightMargin="249.0000" TopMargin="504.0000" BottomMargin="-4.0000" TouchEnable="True" FontSize="14" LeftEage="15" RightEage="15" TopEage="5" BottomEage="5" Scale9OriginX="15" Scale9OriginY="5" Scale9Width="38" Scale9Height="58" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="68.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="37.0000" Y="30.0000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1156" Y="0.0528" />
                <PreSize X="0.2125" Y="0.1197" />
                <FontResource Type="Normal" Path="Arial Rounded Bold.ttf" Plist="" />
                <TextColor A="255" R="229" G="229" B="229" />
                <NormalFileData Type="Normal" Path="guiImage/ingame_btn_pause.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dashButton" ActionTag="886569459" VisibleForFrame="False" Tag="132" IconVisible="False" LeftMargin="-35.9999" RightMargin="205.9999" TopMargin="451.0000" BottomMargin="-33.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" RightEage="50" TopEage="50" BottomEage="50" Scale9OriginY="50" Scale9Width="100" Scale9Height="50" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="150.0000" Y="150.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="39.0001" Y="42.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1219" Y="0.0739" />
                <PreSize X="0.4688" Y="0.2641" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/suit_skill_00.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="finishGameText" ActionTag="332453167" VisibleForFrame="False" Tag="133" RotationSkewX="10.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="70.5000" RightMargin="70.5000" TopMargin="96.6000" BottomMargin="437.4000" FontSize="20" LabelText="All Jobs Done" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="179.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="454.4000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="165" B="0" />
                <PrePosition X="0.5000" Y="0.8000" />
                <PreSize X="0.5594" Y="0.0599" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="finishGameSprite" ActionTag="-224209958" VisibleForFrame="False" Tag="837" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-40.0000" RightMargin="-40.0000" TopMargin="163.8000" BottomMargin="334.2000" ctype="SpriteObjectData">
                <Size X="400.0000" Y="70.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="369.2000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6500" />
                <PreSize X="1.2500" Y="0.1232" />
                <FileData Type="Normal" Path="guiImage/ingame_text_stageclear.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position X="-0.9600" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="-0.0030" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="tapHintPanel" ActionTag="-1099575482" Tag="24" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="60.0000" RightMargin="60.0000" TopMargin="368.0000" BottomMargin="100.0000" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="200.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="Text_1" ActionTag="1848454197" Tag="25" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-6.0000" RightMargin="-8.0000" TopMargin="4.5000" BottomMargin="60.5000" FontSize="28" LabelText="Tap a line here!" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="214.0000" Y="35.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="101.0000" Y="78.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="49" G="216" B="212" />
                <PrePosition X="0.5050" Y="0.7800" />
                <PreSize X="1.0700" Y="0.3500" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="tapAnimeNode" ActionTag="1191308479" Tag="34" IconVisible="True" LeftMargin="100.0000" RightMargin="100.0000" TopMargin="60.0000" BottomMargin="40.0000" StretchWidthEnable="False" StretchHeightEnable="False" InnerActionSpeed="1.0000" CustomSizeEnabled="False" ctype="ProjectNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <AnchorPoint />
                <Position X="100.0000" Y="40.0000" />
                <Scale ScaleX="0.2000" ScaleY="0.2000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4000" />
                <PreSize X="0.0000" Y="0.0000" />
                <FileData Type="Normal" Path="tapAnime.csd" Plist="" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" />
            <Position X="160.0000" Y="100.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.1761" />
            <PreSize X="0.6250" Y="0.1761" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="countDownFrame" ActionTag="-1154172208" Tag="45" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="0.4307" RightMargin="-0.4307" TopMargin="0.4307" BottomMargin="-0.4307" Scale9Enable="True" LeftEage="105" RightEage="105" TopEage="105" BottomEage="105" Scale9OriginX="105" Scale9OriginY="105" Scale9Width="110" Scale9Height="110" ctype="ImageViewObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position X="0.4307" Y="-0.4307" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0013" Y="-0.0008" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="guiImage/countdown_redframe.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeExtendPanel" ActionTag="744023478" VisibleForFrame="False" Tag="127" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="28.0000" BottomMargin="440.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="titleText" ActionTag="-1520997563" Tag="128" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="68.0000" RightMargin="68.0000" TopMargin="10.0000" BottomMargin="56.0000" FontSize="20" LabelText="TIME EXTENDED" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="184.0000" Y="34.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="73.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="208" B="0" />
                <PrePosition X="0.5000" Y="0.7300" />
                <PreSize X="0.5750" Y="0.3400" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="secondText" ActionTag="-1819424260" Tag="129" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="99.0000" RightMargin="99.0000" TopMargin="43.5000" BottomMargin="31.5000" FontSize="15" LabelText="+10 seconds" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="122.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="44.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.4400" />
                <PreSize X="0.3812" Y="0.2500" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="0" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="490.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.8627" />
            <PreSize X="1.0000" Y="0.1761" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>