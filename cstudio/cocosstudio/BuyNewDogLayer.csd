<GameFile>
  <PropertyGroup Name="BuyNewDogLayer" Type="Layer" ID="fc06e361-a01c-4635-a086-b9824f67109d" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="70" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="Panel_1" ActionTag="-204325457" Tag="11" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlock_aura_1" ActionTag="-1648599304" Alpha="172" Tag="46" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="105.6000" BottomMargin="162.4000" ctype="SpriteObjectData">
            <Size X="300.0000" Y="300.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="312.4000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5500" />
            <PreSize X="0.9375" Y="0.5282" />
            <FileData Type="Normal" Path="unlock_aura.png" Plist="" />
            <BlendFunc Src="1" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlock_aura_2" ActionTag="-1686772013" Alpha="172" Tag="47" RotationSkewX="30.0000" RotationSkewY="30.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="105.6000" BottomMargin="162.4000" ctype="SpriteObjectData">
            <Size X="300.0000" Y="300.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="312.4000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5500" />
            <PreSize X="0.9375" Y="0.5282" />
            <FileData Type="Normal" Path="unlock_aura.png" Plist="" />
            <BlendFunc Src="1" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="fireworkNode" ActionTag="1513405755" Tag="218" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="284.0000" BottomMargin="284.0000" ctype="SingleNodeObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.0000" Y="0.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ParticleJoyFul" ActionTag="-160636588" Tag="10" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="160.0000" RightMargin="160.0000" TopMargin="255.6000" BottomMargin="312.4000" ctype="ParticleObjectData">
            <Size X="0.0000" Y="0.0000" />
            <AnchorPoint />
            <Position X="160.0000" Y="312.4000" />
            <Scale ScaleX="0.4000" ScaleY="0.4000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5500" />
            <PreSize X="0.0000" Y="0.0000" />
            <FileData Type="Normal" Path="particle/particle_stars.plist" Plist="" />
            <BlendFunc Src="1" Dst="1" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="784400476" VisibleForFrame="False" Tag="112" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="35.2974" RightMargin="184.7026" TopMargin="5.4521" BottomMargin="522.5479" IsCustomSize="True" FontSize="35" LabelText="555" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="35.2974" Y="542.5479" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="0" />
            <PrePosition X="0.1103" Y="0.9552" />
            <PreSize X="0.3125" Y="0.0704" />
            <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="-510590873" VisibleForFrame="False" Tag="91" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-1.6804" RightMargin="279.6804" TopMargin="1.8490" BottomMargin="524.1510" ctype="SpriteObjectData">
            <Size X="42.0000" Y="42.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="22.0160" Y="543.5760" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0688" Y="0.9570" />
            <PreSize X="0.1312" Y="0.0739" />
            <FileData Type="Normal" Path="guiImage/star_normal.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-431759755" Tag="113" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TopMargin="113.6000" BottomMargin="113.6000" TouchEnable="True" ClipAble="False" ColorAngle="90.0000" LeftEage="211" RightEage="211" TopEage="25" BottomEage="25" Scale9OriginX="-211" Scale9OriginY="-25" Scale9Width="422" Scale9Height="50" ctype="PanelObjectData">
            <Size X="320.0000" Y="340.8000" />
            <Children>
              <AbstractNodeData Name="dogName" ActionTag="-1352898383" Tag="137" RotationSkewX="13.0000" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-59.0000" RightMargin="-59.0000" TopMargin="192.4719" BottomMargin="73.3282" FontSize="50" LabelText="Fox XXXXX Terrier" OutlineSize="6" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="438.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="110.8282" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.3252" />
                <PreSize X="1.3687" Y="0.2201" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="250" G="128" B="16" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="ui_result_unlock_1" ActionTag="-1283279545" Tag="160" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-31.3000" RightMargin="-5.7000" TopMargin="185.5955" BottomMargin="0.2045" ctype="SpriteObjectData">
                <Size X="357.0000" Y="155.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="147.2000" Y="77.7045" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4600" Y="0.2280" />
                <PreSize X="1.1156" Y="0.4548" />
                <FileData Type="Normal" Path="guiImage/ui_result_unlock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="okBtn" ActionTag="330378722" Tag="197" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="31.0000" RightMargin="31.0000" TopMargin="274.9009" BottomMargin="-9.1009" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="228" Scale9Height="53" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="258.0000" Y="75.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="28.3991" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.0833" />
                <PreSize X="0.8062" Y="0.2201" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_result_okbtn.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogSprite" ActionTag="1744384523" Tag="136" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="88.0000" RightMargin="88.0000" TopMargin="64.3200" BottomMargin="132.4800" ctype="SpriteObjectData">
                <Size X="144.0000" Y="144.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="204.4800" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.6000" />
                <PreSize X="0.4500" Y="0.4225" />
                <FileData Type="Normal" Path="guiImage/dselect_dog1_unlock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="ui_result_unlockdog_6" ActionTag="-1909737029" Tag="215" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="5.8302" RightMargin="6.1698" TopMargin="15.0002" BottomMargin="283.7998" ctype="SpriteObjectData">
                <Size X="308.0000" Y="42.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="159.8302" Y="304.7998" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4995" Y="0.8944" />
                <PreSize X="0.9625" Y="0.1232" />
                <FileData Type="Normal" Path="guiImage/ui_result_unlockdog.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="0.6000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>