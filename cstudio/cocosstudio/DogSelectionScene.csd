<GameFile>
  <PropertyGroup Name="DogSelectionScene" Type="Layer" ID="ffa9ed2c-7ce9-4cbb-8ee9-15b4005e5608" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="286" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="BGMain1_1cloud_6" ActionTag="105064131" Tag="106" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="446.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="122.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="160.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" />
            <PreSize X="2.0000" Y="0.2148" />
            <FileData Type="Normal" Path="BGMain1_1cloud.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="upperPanel" ActionTag="-1157465841" Tag="287" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" BottomMargin="468.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="100.0000" />
            <Children>
              <AbstractNodeData Name="previousBtn" ActionTag="-578006198" Tag="294" IconVisible="False" LeftMargin="85.6667" RightMargin="184.3333" TopMargin="15.0001" BottomMargin="34.9999" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="20" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="50.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="110.6667" Y="59.9999" />
                <Scale ScaleX="0.6500" ScaleY="0.6500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3458" Y="0.6000" />
                <PreSize X="0.1563" Y="0.5000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/btn_swipe_left.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nextBtn" ActionTag="2003608949" Tag="295" IconVisible="False" LeftMargin="275.6671" RightMargin="-5.6671" TopMargin="15.0000" BottomMargin="35.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="20" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="50.0000" Y="50.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="300.6671" Y="60.0000" />
                <Scale ScaleX="0.6500" ScaleY="0.6500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9396" Y="0.6000" />
                <PreSize X="0.1563" Y="0.5000" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/btn_swipe_right.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetSprite" ActionTag="-285094725" Tag="296" IconVisible="False" LeftMargin="48.5000" RightMargin="-43.5000" TopMargin="-31.9999" BottomMargin="-8.0001" ctype="SpriteObjectData">
                <Size X="315.0000" Y="140.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="206.0000" Y="61.9999" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6438" Y="0.6200" />
                <PreSize X="0.9844" Y="1.4000" />
                <FileData Type="Normal" Path="guiImage/ui_team_jupiter.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="backBtn" ActionTag="-1407773493" Tag="295" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-48.4000" RightMargin="188.4000" TopMargin="-9.5100" BottomMargin="9.5100" TouchEnable="True" FontSize="36" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="90" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="180.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="41.6000" Y="59.5100" />
                <Scale ScaleX="0.2700" ScaleY="0.2700" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1300" Y="0.5951" />
                <PreSize X="0.5625" Y="1.0000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/bg_green_border.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="1.0000" Y="0.1761" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="dogListView" ActionTag="-446969673" Tag="128" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="128.0000" BottomMargin="20.0000" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="0" DirectionType="Vertical" ctype="ListViewObjectData">
            <Size X="320.0000" Y="420.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="440.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.7746" />
            <PreSize X="1.0000" Y="0.7394" />
            <SingleColor A="255" R="150" G="150" B="255" />
            <FirstColor A="255" R="150" G="150" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="dogIndicatorPanel" ActionTag="1594606773" Tag="56" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="70.0000" BottomMargin="443.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="55.0000" />
            <Children>
              <AbstractNodeData Name="Sprite_11" ActionTag="998863439" Tag="85" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="80.0000" RightMargin="238.0000" TopMargin="-6.5000" BottomMargin="-6.5000" ctype="SpriteObjectData">
                <Size X="2.0000" Y="68.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="81.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.2531" Y="0.5000" />
                <PreSize X="0.0063" Y="1.2364" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_unlock_border.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogLockedIcon01" ActionTag="1335637877" Tag="143" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="75.5000" RightMargin="193.5000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="101.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3156" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_1locked.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogLockedIcon02" ActionTag="2036878795" Tag="144" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="109.5000" RightMargin="159.5000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4219" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_2locked.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogLockedIcon03" ActionTag="-1316389252" Tag="145" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="142.5000" RightMargin="126.5000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="168.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5250" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_3locked.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogLockedIcon04" ActionTag="-495551915" Tag="146" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="175.5000" RightMargin="93.5000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="201.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6281" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_4locked.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogLockedIcon05" ActionTag="1059741862" Tag="147" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="210.5000" RightMargin="58.5000" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="236.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7375" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_5locked.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_27" ActionTag="-690846522" Tag="148" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="244.0000" RightMargin="32.0000" TopMargin="5.5000" BottomMargin="5.5000" ctype="SpriteObjectData">
                <Size X="44.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="266.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8313" Y="0.5000" />
                <PreSize X="0.1375" Y="0.8000" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_arrow.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_15" ActionTag="-994269682" Tag="150" IconVisible="False" LeftMargin="-11.3344" RightMargin="226.3344" TopMargin="7.0000" BottomMargin="16.0000" FontSize="25" LabelText="UNLOCK" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="105.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="41.1656" Y="32.0000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1286" Y="0.5818" />
                <PreSize X="0.3281" Y="0.5818" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_16" ActionTag="1870268535" Tag="151" IconVisible="False" LeftMargin="32.2700" RightMargin="264.7300" TopMargin="24.5000" BottomMargin="5.5000" FontSize="20" LabelText="/5" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="23.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="43.7700" Y="18.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1368" Y="0.3273" />
                <PreSize X="0.0719" Y="0.4545" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetIcon" ActionTag="-556868625" Tag="149" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="89.5000" RightMargin="-184.5000" TopMargin="-180.0000" BottomMargin="-180.0000" ctype="SpriteObjectData">
                <Size X="415.0000" Y="415.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="297.0000" Y="27.5000" />
                <Scale ScaleX="0.0600" ScaleY="0.0600" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9281" Y="0.5000" />
                <PreSize X="1.2969" Y="7.5455" />
                <FileData Type="Normal" Path="planet_venus.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="numberText" ActionTag="1924804732" Tag="152" IconVisible="False" LeftMargin="26.5620" RightMargin="278.4380" TopMargin="24.5000" BottomMargin="5.5000" FontSize="20" LabelText="4" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="15.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="34.0620" Y="18.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="14" G="255" B="217" />
                <PrePosition X="0.1064" Y="0.3273" />
                <PreSize X="0.0469" Y="0.4545" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogUnlockedIcon01" ActionTag="608844405" Tag="193" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="75.9170" RightMargin="193.0830" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="101.4170" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3169" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogUnlockedIcon02" ActionTag="832977298" Tag="194" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="109.9169" RightMargin="159.0831" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.4169" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4232" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogUnlockedIcon03" ActionTag="1330040046" Tag="195" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="142.9173" RightMargin="126.0827" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="168.4173" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5263" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_3.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogUnlockedIcon04" Visible="False" ActionTag="968508246" Tag="196" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="175.9174" RightMargin="93.0826" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="201.4174" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6294" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_4.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogUnlockedIcon05" Visible="False" ActionTag="1320401130" Tag="197" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="210.9173" RightMargin="58.0827" TopMargin="-1.5000" BottomMargin="-1.5000" ctype="SpriteObjectData">
                <Size X="51.0000" Y="58.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="236.4173" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7388" Y="0.5000" />
                <PreSize X="0.1594" Y="1.0545" />
                <FileData Type="Normal" Path="guiImage/ui_dogmenu_5.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="498.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8768" />
            <PreSize X="1.0000" Y="0.0968" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="eventBanner" ActionTag="2076933054" Tag="229" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="70.0000" BottomMargin="443.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" ctype="PanelObjectData">
            <Size X="320.0000" Y="55.0000" />
            <Children>
              <AbstractNodeData Name="Image_1" ActionTag="1992471677" Tag="230" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-14.0000" BottomMargin="-14.0000" LeftEage="211" RightEage="211" TopEage="27" BottomEage="27" Scale9OriginX="211" Scale9OriginY="27" Scale9Width="218" Scale9Height="29" ctype="ImageViewObjectData">
                <Size X="640.0000" Y="83.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="2.0000" Y="1.5091" />
                <FileData Type="Normal" Path="guiImage/ui_xmas_dogmenubanner.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1" ActionTag="-721224201" Tag="231" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="0.5000" RightMargin="286.5000" TopMargin="-2.5000" BottomMargin="-2.5000" ctype="SpriteObjectData">
                <Size X="33.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="17.0000" Y="27.5000" />
                <Scale ScaleX="0.4000" ScaleY="0.4000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0531" Y="0.5000" />
                <PreSize X="0.1031" Y="1.0909" />
                <FileData Type="Normal" Path="guiImage/item_candystick.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="numOfCandy" ActionTag="-1390474051" Tag="232" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="4.6917" RightMargin="225.3083" TopMargin="5.5000" BottomMargin="5.5000" FontSize="35" LabelText="3000" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="90.0000" Y="44.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="49.6917" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="211" B="49" />
                <PrePosition X="0.1553" Y="0.5000" />
                <PreSize X="0.2813" Y="0.8000" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_2" ActionTag="-104207445" Tag="233" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="48.7300" RightMargin="116.2700" TopMargin="11.5000" BottomMargin="11.5000" FontSize="25" LabelText="Collect     in" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="155.0000" Y="32.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="126.2300" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3945" Y="0.5000" />
                <PreSize X="0.4844" Y="0.5818" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_1_0" ActionTag="-266213796" Tag="234" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="126.0600" RightMargin="160.9400" TopMargin="-2.5000" BottomMargin="-2.5000" ctype="SpriteObjectData">
                <Size X="33.0000" Y="60.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="142.5600" Y="27.5000" />
                <Scale ScaleX="0.3200" ScaleY="0.3200" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4455" Y="0.5000" />
                <PreSize X="0.1031" Y="1.0909" />
                <FileData Type="Normal" Path="guiImage/item_candystick.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="planetName" ActionTag="-1985878784" Tag="235" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="169.1800" RightMargin="24.8200" TopMargin="11.5000" BottomMargin="11.5000" FontSize="25" LabelText="North Pole" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="126.0000" Y="32.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="169.1800" Y="27.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="59" G="255" B="215" />
                <PrePosition X="0.5287" Y="0.5000" />
                <PreSize X="0.3938" Y="0.5818" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="498.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="0.8768" />
            <PreSize X="1.0000" Y="0.0968" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>