<GameFile>
  <PropertyGroup Name="UpdateNoticeLayer" Type="Layer" ID="aa174cf3-b1e1-4b53-9d4c-3dbc047cde50" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="192" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="bgPanel" ActionTag="-1028860903" Tag="193" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="126" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="0" G="0" B="0" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-843256670" Tag="194" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="84.0000" BottomMargin="84.0000" ClipAble="False" BackColorAlpha="0" ColorAngle="90.0000" LeftEage="211" RightEage="211" TopEage="42" BottomEage="42" Scale9OriginX="-211" Scale9OriginY="-42" Scale9Width="422" Scale9Height="84" ctype="PanelObjectData">
            <Size X="640.0000" Y="400.0000" />
            <Children>
              <AbstractNodeData Name="ui_pause_bg_1" ActionTag="-1441537861" Tag="104" IconVisible="False" LeftMargin="17.0000" RightMargin="53.0000" TopMargin="12.0000" BottomMargin="40.0000" ctype="SpriteObjectData">
                <Size X="570.0000" Y="348.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="302.0000" Y="214.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4719" Y="0.5350" />
                <PreSize X="0.8906" Y="0.8700" />
                <FileData Type="Normal" Path="guiImage/ui_pause_bg.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Text_1" ActionTag="-1606516499" Tag="195" IconVisible="False" LeftMargin="61.3898" RightMargin="87.6102" TopMargin="142.2434" BottomMargin="209.7566" FontSize="35" LabelText="New version Available " HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="491.0000" Y="48.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="306.8898" Y="233.7566" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4795" Y="0.5844" />
                <PreSize X="0.7672" Y="0.1200" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="closeButton" ActionTag="1506506118" Tag="196" IconVisible="False" LeftMargin="504.6236" RightMargin="70.3764" TopMargin="21.0727" BottomMargin="315.9273" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="35" Scale9Height="41" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="65.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="537.1236" Y="347.4273" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8393" Y="0.8686" />
                <PreSize X="0.1016" Y="0.1575" />
                <TextColor A="255" R="65" G="65" B="70" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_cancel.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="updateButton" ActionTag="-289677462" Tag="197" IconVisible="False" LeftMargin="135.1991" RightMargin="151.8009" TopMargin="229.5447" BottomMargin="76.4553" TouchEnable="True" FontSize="38" ButtonText="Update Now" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="323" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="353.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="311.6991" Y="123.4553" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4870" Y="0.3086" />
                <PreSize X="0.5516" Y="0.2350" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="255" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="284.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0000" Y="0.7042" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>