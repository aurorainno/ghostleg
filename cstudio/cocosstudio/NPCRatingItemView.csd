<GameFile>
  <PropertyGroup Name="NPCRatingItemView" Type="Layer" ID="a9fd7888-b244-4429-a973-8c4fdc057b0b" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="71" ctype="GameLayerObjectData">
        <Size X="292.0000" Y="54.0000" />
        <Children>
          <AbstractNodeData Name="bgSprite" ActionTag="600118219" Tag="72" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-148.5000" RightMargin="-148.5000" TopMargin="-28.5000" BottomMargin="-28.5000" ctype="SpriteObjectData">
            <Size X="589.0000" Y="111.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="146.0000" Y="27.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="2.0171" Y="2.0556" />
            <FileData Type="Normal" Path="guiImage/ui_result_feedback.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="npcSprite" ActionTag="1312716117" Tag="73" IconVisible="False" PositionPercentYEnabled="True" HorizontalEdge="LeftEdge" LeftMargin="-50.5000" RightMargin="185.5000" TopMargin="-51.5000" BottomMargin="-51.5000" ctype="SpriteObjectData">
            <Size X="157.0000" Y="157.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="28.0000" Y="27.0000" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0959" Y="0.5000" />
            <PreSize X="0.5377" Y="2.9074" />
            <FileData Type="Normal" Path="guiImage/ingame_mission_npc_1.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Sprite_3" ActionTag="-1971751043" Tag="74" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="20.5000" RightMargin="196.5000" TopMargin="-57.0000" BottomMargin="27.0000" ctype="SpriteObjectData">
            <Size X="75.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="58.0000" Y="27.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1986" Y="0.5000" />
            <PreSize X="0.2568" Y="1.5556" />
            <FileData Type="Normal" Path="guiImage/ingame_result_emoji.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="emojiSprite" ActionTag="-967795152" Tag="75" IconVisible="False" LeftMargin="16.6110" RightMargin="191.3890" TopMargin="-41.3565" BottomMargin="11.3565" ctype="SpriteObjectData">
            <Size X="84.0000" Y="84.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="58.6110" Y="53.3565" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2007" Y="0.9881" />
            <PreSize X="0.2877" Y="1.5556" />
            <FileData Type="Normal" Path="guiImage/imgame_emoji_3.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="Text_1" ActionTag="580547357" Tag="76" IconVisible="False" LeftMargin="85.0000" RightMargin="75.0000" BottomMargin="26.0000" FontSize="20" LabelText="time used:" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="132.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="85.0000" Y="40.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.2911" Y="0.7407" />
            <PreSize X="0.4521" Y="0.5185" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="ratingPanel" ActionTag="-44603638" VisibleForFrame="False" Tag="77" IconVisible="False" PercentHeightEnable="True" PercentHeightEnabled="True" LeftMargin="134.0448" RightMargin="57.9552" TopMargin="4.8213" BottomMargin="35.6787" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="100.0000" Y="13.5000" />
            <Children>
              <AbstractNodeData Name="Sprite_3" ActionTag="-1354217792" Tag="78" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-3.0000" RightMargin="67.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="15.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1500" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3_0" ActionTag="236975386" Tag="79" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="19.0000" RightMargin="45.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="37.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3700" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3_1" ActionTag="-184108740" Tag="80" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="41.0000" RightMargin="23.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5900" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3_0_0" ActionTag="2032107930" Tag="84" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="1.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="81.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8100" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="Sprite_3_0_1" ActionTag="-1643347224" Tag="85" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="85.0000" RightMargin="-21.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0300" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_OFF.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star01" ActionTag="-940817329" Tag="81" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-3.0000" RightMargin="67.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="15.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1500" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star02" ActionTag="-472923579" Tag="82" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="19.0000" RightMargin="45.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="37.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3700" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star03" ActionTag="-1167865107" Tag="83" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="41.0000" RightMargin="23.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="59.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5900" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star04" ActionTag="180010864" VisibleForFrame="False" Tag="86" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="63.0000" RightMargin="1.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="81.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8100" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="star05" ActionTag="262314455" VisibleForFrame="False" Tag="87" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="85.0000" RightMargin="-21.0000" TopMargin="-9.7500" BottomMargin="-9.7500" ctype="SpriteObjectData">
                <Size X="36.0000" Y="33.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="103.0000" Y="6.7500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="1.0300" Y="0.5000" />
                <PreSize X="0.3600" Y="2.4444" />
                <FileData Type="Normal" Path="guiImage/ingame_mission_difficulty_ON.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleY="0.5000" />
            <Position X="134.0448" Y="42.4287" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4591" Y="0.7857" />
            <PreSize X="0.3425" Y="0.2500" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="commentText" ActionTag="628726207" Tag="88" IconVisible="False" LeftMargin="35.1122" RightMargin="-1.1122" TopMargin="28.0000" BottomMargin="4.0000" FontSize="16" LabelText="that was fast! awesome!" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="258.0000" Y="22.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="164.1122" Y="15.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="247" G="231" B="80" />
            <PrePosition X="0.5620" Y="0.2778" />
            <PreSize X="0.8836" Y="0.4074" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeText" ActionTag="-1990528362" Tag="199" IconVisible="False" LeftMargin="159.0000" RightMargin="112.0000" BottomMargin="26.0000" FontSize="20" LabelText="8" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="21.0000" Y="28.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="159.0000" Y="40.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5445" Y="0.7407" />
            <PreSize X="0.0719" Y="0.5185" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="timeUnit" ActionTag="2101345538" Tag="200" IconVisible="False" LeftMargin="175.0000" RightMargin="102.0000" TopMargin="5.5000" BottomMargin="29.5000" FontSize="14" LabelText="s" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="15.0000" Y="19.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="175.0000" Y="39.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5993" Y="0.7222" />
            <PreSize X="0.0514" Y="0.3519" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>