<GameFile>
  <PropertyGroup Name="SuitItemView" Type="Layer" ID="cb0fe841-70ce-4312-b4ad-8a48858f5a16" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="89" ctype="GameLayerObjectData">
        <Size X="290.0000" Y="190.0000" />
        <Children>
          <AbstractNodeData Name="Image_18" ActionTag="-862381386" Tag="142" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-435.0000" RightMargin="-435.0000" TopMargin="-506.9575" BottomMargin="-3.0405" Scale9Enable="True" LeftEage="33" RightEage="33" TopEage="34" BottomEage="34" Scale9OriginX="33" Scale9OriginY="34" Scale9Width="1134" Scale9Height="2" ctype="ImageViewObjectData">
            <Size X="1160.0000" Y="699.9980" />
            <AnchorPoint ScaleX="0.5000" />
            <Position X="145.0000" Y="-3.0405" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="-0.0160" />
            <PreSize X="4.0000" Y="3.6842" />
            <FileData Type="Normal" Path="suits_window03.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="TitlePanel" ActionTag="-1010471052" Tag="90" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="-435.0000" RightMargin="-435.0000" TopMargin="-42.4000" BottomMargin="122.4000" Scale9Enable="True" LeftEage="396" RightEage="396" TopEage="33" BottomEage="33" Scale9OriginX="396" Scale9OriginY="33" Scale9Width="408" Scale9Height="34" ctype="ImageViewObjectData">
            <Size X="1160.0000" Y="110.0000" />
            <Children>
              <AbstractNodeData Name="Title" ActionTag="2146744479" Tag="91" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="104.7000" RightMargin="522.3000" TopMargin="-8.5000" BottomMargin="-8.5000" FontSize="100" LabelText="hunter dog" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="533.0000" Y="127.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="371.2000" Y="55.0000" />
                <Scale ScaleX="0.6000" ScaleY="0.6000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3200" Y="0.5000" />
                <PreSize X="0.4595" Y="1.1545" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Level" Visible="False" ActionTag="-469636628" VisibleForFrame="False" Tag="156" IconVisible="False" LeftMargin="89.0000" RightMargin="1037.0000" TopMargin="46.0000" BottomMargin="36.0000" FontSize="22" LabelText="lv3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="34.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="106.0000" Y="50.0000" />
                <Scale ScaleX="2.0000" ScaleY="2.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0914" Y="0.4545" />
                <PreSize X="0.0293" Y="0.2545" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="locked" ActionTag="92283803" VisibleForFrame="False" Tag="157" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="91.0000" RightMargin="1039.0000" TopMargin="36.0000" BottomMargin="36.0000" ctype="SpriteObjectData">
                <Size X="30.0000" Y="38.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="106.0000" Y="55.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0914" Y="0.5000" />
                <PreSize X="0.0259" Y="0.3455" />
                <FileData Type="Normal" Path="suits_locked.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="145.0000" Y="177.4000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.9337" />
            <PreSize X="4.0000" Y="0.5789" />
            <FileData Type="Normal" Path="suits_window01.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Info" ActionTag="-424810664" Tag="95" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-36.2500" RightMargin="-100.0500" TopMargin="-6.4100" BottomMargin="14.0100" Scale9Enable="True" LeftEage="280" RightEage="280" TopEage="36" BottomEage="36" Scale9OriginX="280" Scale9OriginY="36" Scale9Width="290" Scale9Height="40" ctype="ImageViewObjectData">
            <Size X="426.3000" Y="182.4000" />
            <Children>
              <AbstractNodeData Name="InfoText" ActionTag="-2006574823" Tag="146" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="12.7890" RightMargin="-386.4890" TopMargin="12.7680" BottomMargin="16.6320" FontSize="40" LabelText="Lanches homing missile at an object&#xA;duration while consuming a specific item.&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="800.0000" Y="153.0000" />
                <AnchorPoint ScaleY="1.0000" />
                <Position X="12.7890" Y="169.6320" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0300" Y="0.9300" />
                <PreSize X="1.8766" Y="0.8388" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="CurrentValuePanel" ActionTag="960235301" Tag="147" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="7.0000" RightMargin="-7.0000" TopMargin="77.6000" BottomMargin="59.2000" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="426.3000" Y="45.6000" />
                <Children>
                  <AbstractNodeData Name="ValueIcon" ActionTag="2017241462" Tag="149" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="8.0000" RightMargin="370.3000" TopMargin="-1.2000" BottomMargin="-1.2000" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="32.0000" Y="22.8000" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0751" Y="0.5000" />
                    <PreSize X="0.1126" Y="1.0526" />
                    <FileData Type="Normal" Path="suits_item2.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ValueText" ActionTag="-459520712" Tag="150" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="60.0000" RightMargin="303.3000" TopMargin="-6.2000" BottomMargin="-6.2000" FontSize="46" LabelText="x 1" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="63.0000" Y="58.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="60.0000" Y="22.8000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1407" Y="0.5000" />
                    <PreSize X="0.1478" Y="1.2719" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="7.0000" Y="82.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0164" Y="0.4496" />
                <PreSize X="1.0000" Y="0.2500" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="NextValuePanel" ActionTag="494700682" Tag="148" IconVisible="False" PositionPercentYEnabled="True" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" LeftMargin="7.0000" RightMargin="-7.0000" TopMargin="126.7862" BottomMargin="10.0138" ClipAble="False" BackColorAlpha="0" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="426.3000" Y="45.6000" />
                <Children>
                  <AbstractNodeData Name="ValueIcon" ActionTag="-1566630440" Tag="151" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="148.0000" RightMargin="230.3000" TopMargin="-1.2000" BottomMargin="-1.2000" ctype="SpriteObjectData">
                    <Size X="48.0000" Y="48.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="172.0000" Y="22.8000" />
                    <Scale ScaleX="0.7000" ScaleY="0.7000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4035" Y="0.5000" />
                    <PreSize X="0.1126" Y="1.0526" />
                    <FileData Type="Normal" Path="suits_item2.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="ValueText" ActionTag="-1516608512" Tag="152" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="200.0000" RightMargin="158.3000" TopMargin="-6.2000" BottomMargin="-6.2000" FontSize="46" LabelText="x 2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="68.0000" Y="58.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="200.0000" Y="22.8000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4692" Y="0.5000" />
                    <PreSize X="0.1595" Y="1.2719" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_26" ActionTag="-1809115751" Tag="153" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-50.0000" RightMargin="232.3000" TopMargin="-7.7000" BottomMargin="-7.7000" FontSize="48" LabelText="Next Level" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="244.0000" Y="61.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="72.0000" Y="22.8000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1689" Y="0.5000" />
                    <PreSize X="0.5724" Y="1.3377" />
                    <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint ScaleY="0.5000" />
                <Position X="7.0000" Y="32.8138" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0164" Y="0.1799" />
                <PreSize X="1.0000" Y="0.2500" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="176.9000" Y="105.2100" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6100" Y="0.5537" />
            <PreSize X="1.4700" Y="0.9600" />
            <FileData Type="Normal" Path="suits_window02.png" Plist="" />
          </AbstractNodeData>
          <AbstractNodeData Name="Icon" ActionTag="121599972" Tag="97" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-70.4821" RightMargin="144.4821" TopMargin="-42.0290" BottomMargin="-19.9710" ctype="SpriteObjectData">
            <Size X="216.0000" Y="252.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="37.5179" Y="106.0290" />
            <Scale ScaleX="0.2700" ScaleY="0.2700" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1294" Y="0.5580" />
            <PreSize X="0.7448" Y="1.3263" />
            <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="priceBtn" ActionTag="-1526865824" Tag="132" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-80.6000" RightMargin="-57.4000" TopMargin="88.5000" BottomMargin="-44.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="428.0000" Y="146.0000" />
            <Children>
              <AbstractNodeData Name="Image_5_0" ActionTag="778835990" Tag="131" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.5394" RightMargin="304.4606" TopMargin="19.6570" BottomMargin="18.3430" LeftEage="35" RightEage="35" TopEage="35" BottomEage="35" Scale9OriginX="35" Scale9OriginY="35" Scale9Width="38" Scale9Height="38" ctype="ImageViewObjectData">
                <Size X="108.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="69.5394" Y="72.3430" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1625" Y="0.4955" />
                <PreSize X="0.2523" Y="0.7397" />
                <FileData Type="Normal" Path="star_icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceText" ActionTag="-122368822" Tag="130" IconVisible="False" LeftMargin="216.0621" RightMargin="140.9379" TopMargin="36.3041" BottomMargin="63.6959" FontSize="36" LabelText="500" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="71.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="251.5621" Y="86.6959" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5878" Y="0.5938" />
                <PreSize X="0.1659" Y="0.3151" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceLevelText" ActionTag="-1875122811" Tag="129" IconVisible="False" LeftMargin="216.5587" RightMargin="141.4413" TopMargin="89.6130" BottomMargin="28.3870" FontSize="22" LabelText="unlock" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="251.5587" Y="42.3870" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5878" Y="0.2903" />
                <PreSize X="0.1636" Y="0.1918" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="133.4000" Y="28.5000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4600" Y="0.1500" />
            <PreSize X="1.4759" Y="0.7684" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="watchAdsBtn" Visible="False" ActionTag="-267325123" Tag="133" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-80.6000" RightMargin="-57.4000" TopMargin="88.5000" BottomMargin="-44.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="428.0000" Y="146.0000" />
            <Children>
              <AbstractNodeData Name="priceText" ActionTag="833033280" Tag="135" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="131.0000" RightMargin="131.0000" TopMargin="36.3000" BottomMargin="63.7000" FontSize="36" LabelText="watch ad" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="166.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="214.0000" Y="86.7000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5938" />
                <PreSize X="0.3879" Y="0.3151" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceLevelText" ActionTag="-788072765" Tag="136" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="179.0000" RightMargin="179.0000" TopMargin="89.6130" BottomMargin="28.3870" FontSize="22" LabelText="unlock" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="28.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="214.0000" Y="42.3870" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.2903" />
                <PreSize X="0.1636" Y="0.1918" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="133.4000" Y="28.5000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4600" Y="0.1500" />
            <PreSize X="1.4759" Y="0.7684" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="maxBtn" Visible="False" ActionTag="-77592550" Tag="137" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="BottomEdge" LeftMargin="-80.6000" RightMargin="-57.4000" TopMargin="90.0000" BottomMargin="-46.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="428.0000" Y="146.0000" />
            <Children>
              <AbstractNodeData Name="Image_5_0" ActionTag="2077893579" Tag="138" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="15.5394" RightMargin="304.4606" TopMargin="19.6570" BottomMargin="18.3430" LeftEage="35" RightEage="35" TopEage="35" BottomEage="35" Scale9OriginX="35" Scale9OriginY="35" Scale9Width="38" Scale9Height="38" ctype="ImageViewObjectData">
                <Size X="108.0000" Y="108.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="69.5394" Y="72.3430" />
                <Scale ScaleX="0.8000" ScaleY="0.8000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1625" Y="0.4955" />
                <PreSize X="0.2523" Y="0.7397" />
                <FileData Type="Normal" Path="star_icon.png" Plist="" />
              </AbstractNodeData>
              <AbstractNodeData Name="priceText" ActionTag="-1974546404" Tag="139" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="216.5621" RightMargin="141.4379" TopMargin="50.0000" BottomMargin="50.0000" FontSize="36" LabelText="max" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="70.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="251.5621" Y="73.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5878" Y="0.5000" />
                <PreSize X="0.1636" Y="0.3151" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="Panel_5" ActionTag="65207366" Tag="141" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="428.0000" Y="146.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="54" G="74" B="87" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="133.4000" Y="27.0000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4600" Y="0.1421" />
            <PreSize X="1.4759" Y="0.7684" />
            <TextColor A="255" R="65" G="65" B="70" />
            <PressedFileData Type="Normal" Path="bg_green_border.png" Plist="" />
            <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="useBtn" ActionTag="826306483" Tag="143" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="74.9000" RightMargin="-104.9000" TopMargin="88.5000" BottomMargin="-44.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="20" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="320.0000" Y="146.0000" />
            <Children>
              <AbstractNodeData Name="priceText" ActionTag="-1085999041" Tag="144" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="130.0000" RightMargin="130.0000" TopMargin="50.0000" BottomMargin="50.0000" FontSize="36" LabelText="use" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="60.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="73.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.1875" Y="0.3151" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="234.9000" Y="28.5000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8100" Y="0.1500" />
            <PreSize X="1.1034" Y="0.7684" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="bg_green_border.png" Plist="" />
            <NormalFileData Type="Normal" Path="bg_green_border.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="selectedBtn" Visible="False" ActionTag="-1222848027" Tag="154" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="74.9000" RightMargin="-104.9000" TopMargin="88.5000" BottomMargin="-44.5000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="40" RightEage="40" TopEage="10" BottomEage="40" Scale9OriginX="40" Scale9OriginY="10" Scale9Width="220" Scale9Height="54" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="320.0000" Y="146.0000" />
            <Children>
              <AbstractNodeData Name="priceText" ActionTag="1760967134" Tag="155" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="84.5000" RightMargin="84.5000" TopMargin="50.0000" BottomMargin="50.0000" FontSize="36" LabelText="selected" HorizontalAlignmentType="HT_Center" VerticalAlignmentType="VT_Center" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="151.0000" Y="46.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="73.0000" />
                <Scale ScaleX="1.5000" ScaleY="1.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="0.4719" Y="0.3151" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="234.9000" Y="28.5000" />
            <Scale ScaleX="0.2500" ScaleY="0.2500" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8100" Y="0.1500" />
            <PreSize X="1.1034" Y="0.7684" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Normal" Path="suits_btn3.png" Plist="" />
            <PressedFileData Type="Normal" Path="suits_btn3.png" Plist="" />
            <NormalFileData Type="Normal" Path="suits_btn3.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>