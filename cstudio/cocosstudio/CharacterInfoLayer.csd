<GameFile>
  <PropertyGroup Name="CharacterInfoLayer" Type="Layer" ID="99176d48-22b0-4f5d-8b6b-aed85b86e182" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="186" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="568.0000" />
        <Children>
          <AbstractNodeData Name="mainPanel" ActionTag="98850425" Tag="235" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="45.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="523.0000" />
            <Children>
              <AbstractNodeData Name="bgPanel" ActionTag="2004333323" Tag="1093" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="523.0000" />
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="1.0000" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="bgSprite" ActionTag="-484321963" Tag="236" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-160.0000" RightMargin="-160.0000" TopMargin="-256.0000" BottomMargin="-256.0000" ctype="SpriteObjectData">
                <Size X="640.0000" Y="1035.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="261.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5150" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="2.0000" Y="1.9790" />
                <FileData Type="Normal" Path="guiImage/ui_bg1.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="charSprite" ActionTag="1553372393" Tag="445" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="10.0000" RightMargin="10.0000" TopMargin="-38.3918" BottomMargin="261.3918" ctype="SpriteObjectData">
                <Size X="300.0000" Y="300.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="411.3918" />
                <Scale ScaleX="0.4500" ScaleY="0.4500" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.7866" />
                <PreSize X="0.9375" Y="0.5736" />
                <FileData Type="Normal" Path="guiImage/player_icon_sample.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="infoBtn" ActionTag="-44282304" Tag="280" IconVisible="False" LeftMargin="262.6997" RightMargin="7.3003" TopMargin="19.0000" BottomMargin="454.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="50.0000" Y="50.0000" />
                <Children>
                  <AbstractNodeData Name="Sprite_19" ActionTag="145821204" Tag="281" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-8.0000" RightMargin="-8.0000" TopMargin="-6.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                    <Size X="66.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="25.0000" Y="25.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.3200" Y="1.2400" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvlbg_infotop.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="262.6997" Y="454.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8209" Y="0.8681" />
                <PreSize X="0.1563" Y="0.0956" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="backBtn" ActionTag="-860831760" Tag="237" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-27.1361" RightMargin="211.1361" TopMargin="-2.3500" BottomMargin="440.3500" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="106" Scale9Height="63" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="136.0000" Y="85.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="40.8639" Y="482.8500" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1277" Y="0.9232" />
                <PreSize X="0.4250" Y="0.1625" />
                <TextColor A="255" R="65" G="65" B="70" />
                <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                <NormalFileData Type="Normal" Path="guiImage/ui_general_back.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nameText" ActionTag="2053541819" Tag="238" IconVisible="False" PositionPercentXEnabled="True" VerticalEdge="TopEdge" LeftMargin="34.5000" RightMargin="34.5000" TopMargin="18.5000" BottomMargin="463.5000" FontSize="24" LabelText="character name" OutlineSize="4" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="251.0000" Y="41.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="160.0000" Y="484.0000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.9254" />
                <PreSize X="0.7844" Y="0.0784" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="23" G="114" B="151" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="statPanel" ActionTag="-1693362149" Tag="282" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" VerticalEdge="TopEdge" TopMargin="161.0000" BottomMargin="237.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="125.0000" />
                <Children>
                  <AbstractNodeData Name="Text_5" ActionTag="283970344" Tag="247" IconVisible="False" LeftMargin="40.0000" RightMargin="135.0000" TopMargin="11.9039" BottomMargin="83.0961" FontSize="22" LabelText="max speed" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="145.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="40.0000" Y="98.0961" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1250" Y="0.7848" />
                    <PreSize X="0.4531" Y="0.2400" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_5_0" ActionTag="1768040288" Tag="248" IconVisible="False" LeftMargin="40.0000" RightMargin="99.0000" TopMargin="36.2893" BottomMargin="58.7107" FontSize="22" LabelText="acceleration" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="181.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="40.0000" Y="73.7107" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1250" Y="0.5897" />
                    <PreSize X="0.5656" Y="0.2400" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_5_1" ActionTag="-1353150357" Tag="249" IconVisible="False" LeftMargin="40.0001" RightMargin="121.9999" TopMargin="61.2290" BottomMargin="33.7710" FontSize="22" LabelText="slide speed" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="158.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="40.0001" Y="48.7710" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1250" Y="0.3902" />
                    <PreSize X="0.4938" Y="0.2400" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Text_5_1_0" ActionTag="-612580713" Tag="250" IconVisible="False" LeftMargin="40.0000" RightMargin="152.0000" TopMargin="85.0606" BottomMargin="9.9394" FontSize="22" LabelText="recovery" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="128.0000" Y="30.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="40.0000" Y="24.9394" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.1250" Y="0.1995" />
                    <PreSize X="0.4000" Y="0.2400" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="maxSpeedPanel" ActionTag="-125824467" Tag="253" IconVisible="False" LeftMargin="140.0000" RightMargin="26.0000" TopMargin="6.7831" BottomMargin="94.2169" ClipAble="True" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="154.0000" Y="24.0000" />
                    <Children>
                      <AbstractNodeData Name="progressBar" ActionTag="-29627755" Tag="251" IconVisible="False" RightMargin="-154.0000" ctype="SpriteObjectData">
                        <Size X="308.0000" Y="24.0000" />
                        <Children>
                          <AbstractNodeData Name="valueText" ActionTag="-904884353" Tag="260" IconVisible="False" LeftMargin="263.0000" RightMargin="-5.0000" TopMargin="-9.0000" BottomMargin="1.0000" FontSize="20" LabelText="160" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="50.0000" Y="32.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="288.0000" Y="17.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.9351" Y="0.7083" />
                            <PreSize X="0.1623" Y="1.3333" />
                            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                            <OutlineColor A="255" R="100" G="100" B="100" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition />
                        <PreSize X="2.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="guiImage/ui_character_statsbar_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="140.0000" Y="94.2169" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4375" Y="0.7537" />
                    <PreSize X="0.4812" Y="0.1920" />
                    <SingleColor A="255" R="77" G="77" B="77" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="accelerationPanel" ActionTag="219213595" Tag="254" IconVisible="False" LeftMargin="140.0000" RightMargin="26.0000" TopMargin="31.6554" BottomMargin="69.3446" ClipAble="True" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="154.0000" Y="24.0000" />
                    <Children>
                      <AbstractNodeData Name="progressBar" ActionTag="-47565935" Tag="255" IconVisible="False" RightMargin="-154.0000" ctype="SpriteObjectData">
                        <Size X="308.0000" Y="24.0000" />
                        <Children>
                          <AbstractNodeData Name="valueText" ActionTag="-470667974" Tag="261" IconVisible="False" LeftMargin="263.5000" RightMargin="-4.5000" TopMargin="-9.0000" BottomMargin="1.0000" FontSize="20" LabelText="130" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="49.0000" Y="32.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="288.0000" Y="17.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.9351" Y="0.7083" />
                            <PreSize X="0.1591" Y="1.3333" />
                            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                            <OutlineColor A="255" R="100" G="100" B="100" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition />
                        <PreSize X="2.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="guiImage/ui_character_statsbar_2.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="140.0000" Y="69.3446" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4375" Y="0.5548" />
                    <PreSize X="0.4812" Y="0.1920" />
                    <SingleColor A="255" R="77" G="77" B="77" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="turnSpeedPanel" ActionTag="952092207" Tag="256" IconVisible="False" LeftMargin="140.0000" RightMargin="26.0000" TopMargin="56.2680" BottomMargin="44.7320" ClipAble="True" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="154.0000" Y="24.0000" />
                    <Children>
                      <AbstractNodeData Name="progressBar" ActionTag="-284233000" Tag="257" IconVisible="False" RightMargin="-154.0000" ctype="SpriteObjectData">
                        <Size X="308.0000" Y="24.0000" />
                        <Children>
                          <AbstractNodeData Name="valueText" ActionTag="-1905329928" Tag="262" IconVisible="False" LeftMargin="268.0000" TopMargin="-9.0000" BottomMargin="1.0000" FontSize="20" LabelText="80" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="40.0000" Y="32.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="288.0000" Y="17.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.9351" Y="0.7083" />
                            <PreSize X="0.1299" Y="1.3333" />
                            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                            <OutlineColor A="255" R="100" G="100" B="100" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition />
                        <PreSize X="2.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="guiImage/ui_character_statsbar_3.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="140.0000" Y="44.7320" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4375" Y="0.3579" />
                    <PreSize X="0.4812" Y="0.1920" />
                    <SingleColor A="255" R="77" G="77" B="77" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="recoveryPanel" ActionTag="-966193213" Tag="258" IconVisible="False" LeftMargin="140.0000" RightMargin="26.0000" TopMargin="79.8668" BottomMargin="21.1332" ClipAble="True" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="154.0000" Y="24.0000" />
                    <Children>
                      <AbstractNodeData Name="progressBar" ActionTag="378267729" Tag="259" IconVisible="False" RightMargin="-154.0000" ctype="SpriteObjectData">
                        <Size X="308.0000" Y="24.0000" />
                        <Children>
                          <AbstractNodeData Name="valueText" ActionTag="-1838459168" Tag="263" IconVisible="False" LeftMargin="268.0000" TopMargin="-9.0000" BottomMargin="1.0000" FontSize="20" LabelText="80" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                            <Size X="40.0000" Y="32.0000" />
                            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                            <Position X="288.0000" Y="17.0000" />
                            <Scale ScaleX="1.0000" ScaleY="1.0000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="0.9351" Y="0.7083" />
                            <PreSize X="0.1299" Y="1.3333" />
                            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                            <OutlineColor A="255" R="100" G="100" B="100" />
                            <ShadowColor A="255" R="110" G="110" B="110" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition />
                        <PreSize X="2.0000" Y="1.0000" />
                        <FileData Type="Normal" Path="guiImage/ui_character_statsbar_4.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position X="140.0000" Y="21.1332" />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4375" Y="0.1691" />
                    <PreSize X="0.4812" Y="0.1920" />
                    <SingleColor A="255" R="77" G="77" B="77" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="237.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.4532" />
                <PreSize X="1.0000" Y="0.2390" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="levelPanel" ActionTag="220479405" Tag="266" IconVisible="False" PositionPercentXEnabled="True" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="284.0000" BottomMargin="152.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="87.0000" />
                <Children>
                  <AbstractNodeData Name="sprite" ActionTag="1290417348" Tag="264" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-148.0000" RightMargin="-148.0000" TopMargin="-34.0000" BottomMargin="-34.0000" ctype="SpriteObjectData">
                    <Size X="616.0000" Y="155.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="43.5000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.5000" />
                    <PreSize X="1.9250" Y="1.7816" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvlbg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="levelText" ActionTag="-233084131" Tag="267" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="101.5000" RightMargin="101.5000" TopMargin="9.0000" BottomMargin="42.0000" FontSize="26" LabelText="level 2" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="117.0000" Y="36.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="160.0000" Y="60.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.6897" />
                    <PreSize X="0.3656" Y="0.4138" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="Sprite_11" ActionTag="-1218517803" Tag="268" IconVisible="False" LeftMargin="255.0000" RightMargin="-1.0000" TopMargin="-23.3873" BottomMargin="48.3873" ctype="SpriteObjectData">
                    <Size X="66.0000" Y="62.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position X="288.0000" Y="79.3873" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.9000" Y="0.9125" />
                    <PreSize X="0.2062" Y="0.7126" />
                    <FileData Type="Normal" Path="guiImage/ui_character_lvlbg_info.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="unlockedPanel" ActionTag="1084242500" VisibleForFrame="False" Tag="292" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="7.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="Sprite_12" ActionTag="-521754618" Tag="269" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-58.0000" RightMargin="-58.0000" TopMargin="25.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                        <Size X="436.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="160.0000" Y="30.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5000" Y="0.3750" />
                        <PreSize X="1.3625" Y="0.6250" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_bg.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="levelSprite01" ActionTag="-1234008342" Tag="270" IconVisible="False" LeftMargin="27.5000" RightMargin="197.5000" TopMargin="25.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                        <Size X="95.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="75.0000" Y="30.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.2344" Y="0.3750" />
                        <PreSize X="0.2969" Y="0.6250" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_1.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="levelSprite02" ActionTag="-489978947" VisibleForFrame="False" Tag="271" IconVisible="False" LeftMargin="73.0000" RightMargin="155.0000" TopMargin="25.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                        <Size X="92.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="119.0000" Y="30.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.3719" Y="0.3750" />
                        <PreSize X="0.2875" Y="0.6250" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_2.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="levelSprite03" ActionTag="-98710845" VisibleForFrame="False" Tag="272" IconVisible="False" LeftMargin="116.0000" RightMargin="112.0000" TopMargin="25.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                        <Size X="92.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="162.0000" Y="30.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.5063" Y="0.3750" />
                        <PreSize X="0.2875" Y="0.6250" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_3.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="levelSprite04" ActionTag="231318101" VisibleForFrame="False" Tag="273" IconVisible="False" LeftMargin="159.0000" RightMargin="69.0000" TopMargin="25.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                        <Size X="92.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="205.0000" Y="30.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6406" Y="0.3750" />
                        <PreSize X="0.2875" Y="0.6250" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_4.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="levelSprite05" ActionTag="-1015709750" VisibleForFrame="False" Tag="274" IconVisible="False" LeftMargin="203.0000" RightMargin="25.0000" TopMargin="25.0000" BottomMargin="5.0000" ctype="SpriteObjectData">
                        <Size X="92.0000" Y="50.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="249.0000" Y="30.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7781" Y="0.3750" />
                        <PreSize X="0.2875" Y="0.6250" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_5.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="maxLabel" ActionTag="-299546588" VisibleForFrame="False" Tag="305" IconVisible="False" LeftMargin="226.8680" RightMargin="20.1320" TopMargin="13.2044" BottomMargin="20.7956" ctype="SpriteObjectData">
                        <Size X="73.0000" Y="46.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="263.3680" Y="43.7956" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8230" Y="0.5474" />
                        <PreSize X="0.2281" Y="0.5750" />
                        <FileData Type="Normal" Path="guiImage/ui_character_lvl_max.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="0.9195" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="lockedPanel" ActionTag="-1215214093" VisibleForFrame="False" Tag="293" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="7.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="80.0000" />
                    <Children>
                      <AbstractNodeData Name="Image_2" ActionTag="229905181" Tag="308" IconVisible="False" LeftMargin="48.0000" RightMargin="-213.0000" TopMargin="36.5000" BottomMargin="8.5000" Scale9Enable="True" LeftEage="66" RightEage="66" TopEage="7" BottomEage="7" Scale9OriginX="66" Scale9OriginY="7" Scale9Width="70" Scale9Height="8" ctype="ImageViewObjectData">
                        <Size X="485.0000" Y="35.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="48.0000" Y="26.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1500" Y="0.3250" />
                        <PreSize X="1.5156" Y="0.4375" />
                        <FileData Type="Normal" Path="guiImage/ui_customer_shardbg.png" Plist="" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="clippedNode" ActionTag="1136685488" Tag="310" IconVisible="False" LeftMargin="60.7773" RightMargin="15.2227" TopMargin="43.9090" BottomMargin="16.0910" TouchEnable="True" ClipAble="True" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                        <Size X="244.0000" Y="20.0000" />
                        <Children>
                          <AbstractNodeData Name="levelSprite" ActionTag="299372868" Tag="309" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="-245.0000" TopMargin="-7.5000" BottomMargin="-7.5000" ctype="SpriteObjectData">
                            <Size X="519.0000" Y="35.0000" />
                            <AnchorPoint ScaleY="0.5000" />
                            <Position X="-30.0000" Y="10.0000" />
                            <Scale ScaleX="0.5000" ScaleY="0.5000" />
                            <CColor A="255" R="255" G="255" B="255" />
                            <PrePosition X="-0.1230" Y="0.5000" />
                            <PreSize X="2.1270" Y="1.7500" />
                            <FileData Type="Normal" Path="guiImage/ui_customer_shardbg_bar.png" Plist="" />
                            <BlendFunc Src="1" Dst="771" />
                          </AbstractNodeData>
                        </Children>
                        <AnchorPoint />
                        <Position X="60.7773" Y="16.0910" />
                        <Scale ScaleX="1.0000" ScaleY="1.0000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1899" Y="0.2011" />
                        <PreSize X="0.7625" Y="0.2500" />
                        <SingleColor A="255" R="150" G="200" B="255" />
                        <FirstColor A="255" R="150" G="200" B="255" />
                        <EndColor A="255" R="255" G="255" B="255" />
                        <ColorVector ScaleY="1.0000" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="charSmallIcon" ActionTag="-774818899" Tag="300" IconVisible="False" LeftMargin="8.1836" RightMargin="230.8164" TopMargin="11.5000" BottomMargin="-14.5000" ctype="SpriteObjectData">
                        <Size X="81.0000" Y="83.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="48.6836" Y="27.0000" />
                        <Scale ScaleX="0.3500" ScaleY="0.3500" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1521" Y="0.3375" />
                        <PreSize X="0.2531" Y="1.0375" />
                        <FileData Type="Normal" Path="guiImage/ui_fragment_01.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Sprite_22_0_0" ActionTag="-1905729159" Tag="301" IconVisible="False" LeftMargin="39.5700" RightMargin="238.4300" TopMargin="42.9600" BottomMargin="-2.9600" ctype="SpriteObjectData">
                        <Size X="42.0000" Y="40.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="60.5700" Y="17.0400" />
                        <Scale ScaleX="0.3000" ScaleY="0.3000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1893" Y="0.2130" />
                        <PreSize X="0.1312" Y="0.5000" />
                        <FileData Type="Normal" Path="guiImage/ui_character_shard_bg_pizza.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="levelText" ActionTag="-1037056549" Tag="306" IconVisible="False" LeftMargin="126.0078" RightMargin="131.9922" TopMargin="43.0699" BottomMargin="11.9301" FontSize="18" LabelText="8/10" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="62.0000" Y="25.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="157.0078" Y="24.4301" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.4906" Y="0.3054" />
                        <PreSize X="0.1937" Y="0.3125" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="0.9195" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="152.0000" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="0.2906" />
                <PreSize X="1.0000" Y="0.1663" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="upgradePanel" ActionTag="-1924548977" Tag="275" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="373.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                <Size X="320.0000" Y="150.0000" />
                <Children>
                  <AbstractNodeData Name="Sprite_18" ActionTag="-284623379" Tag="276" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-148.0000" RightMargin="-148.0000" TopMargin="-200.8250" BottomMargin="7.8250" ctype="SpriteObjectData">
                    <Size X="616.0000" Y="343.0000" />
                    <AnchorPoint ScaleX="0.5000" />
                    <Position X="160.0000" Y="7.8250" />
                    <Scale ScaleX="0.5000" ScaleY="0.4500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.5000" Y="0.0522" />
                    <PreSize X="1.9250" Y="2.2867" />
                    <FileData Type="Normal" Path="guiImage/ui_character_upgradebg.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="titleText" ActionTag="-1785013299" Tag="277" IconVisible="False" LeftMargin="30.0000" RightMargin="118.0000" TopMargin="17.0000" BottomMargin="97.0000" FontSize="26" LabelText="next level" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="172.0000" Y="36.0000" />
                    <AnchorPoint ScaleY="0.5000" />
                    <Position X="30.0000" Y="115.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0938" Y="0.7667" />
                    <PreSize X="0.5375" Y="0.2400" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="infoText" ActionTag="-732017819" Tag="278" IconVisible="False" LeftMargin="30.0000" RightMargin="-230.0000" TopMargin="54.0000" BottomMargin="36.0000" IsCustomSize="True" FontSize="20" LabelText="carrier gets 12% more coins from jjjj hhhh hhhh" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                    <Size X="520.0000" Y="60.0000" />
                    <AnchorPoint ScaleY="1.0000" />
                    <Position X="30.0000" Y="96.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.0938" Y="0.6400" />
                    <PreSize X="1.6250" Y="0.4000" />
                    <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="0" G="0" B="0" />
                  </AbstractNodeData>
                  <AbstractNodeData Name="upgradeInfoPanel" ActionTag="778716861" Tag="311" IconVisible="False" PercentWidthEnable="True" PercentWidthEnabled="True" TopMargin="71.0000" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
                    <Size X="320.0000" Y="79.0000" />
                    <Children>
                      <AbstractNodeData Name="upgradeBtn" ActionTag="-1218605720" Tag="279" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="BottomEdge" LeftMargin="120.1817" RightMargin="-42.1817" TopMargin="-22.0720" BottomMargin="-0.9280" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="212" Scale9Height="80" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                        <Size X="242.0000" Y="102.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="241.1817" Y="50.0720" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7537" Y="0.6338" />
                        <PreSize X="0.7563" Y="1.2911" />
                        <TextColor A="255" R="65" G="65" B="70" />
                        <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
                        <NormalFileData Type="Normal" Path="guiImage/ui_character_upgrade_btn.png" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Text_14_0" ActionTag="439996144" Tag="283" IconVisible="False" LeftMargin="185.0595" RightMargin="22.9405" TopMargin="4.0000" BottomMargin="43.0000" FontSize="20" LabelText="upgrade" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="112.0000" Y="32.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="241.0595" Y="59.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.7533" Y="0.7468" />
                        <PreSize X="0.3500" Y="0.4051" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="226" G="106" B="12" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Sprite_20" ActionTag="861344932" Tag="284" IconVisible="False" LeftMargin="179.0000" RightMargin="103.0000" TopMargin="23.0000" BottomMargin="20.0000" ctype="SpriteObjectData">
                        <Size X="38.0000" Y="36.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="198.0000" Y="38.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6187" Y="0.4810" />
                        <PreSize X="0.1187" Y="0.4557" />
                        <FileData Type="Normal" Path="guiImage/ui_character_upgrade_coin.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="upgradeCoinText" ActionTag="-1299122184" Tag="285" IconVisible="False" LeftMargin="207.0000" RightMargin="57.0000" TopMargin="26.0000" BottomMargin="21.0000" FontSize="20" LabelText="200" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="56.0000" Y="32.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="207.0000" Y="37.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.6469" Y="0.4684" />
                        <PreSize X="0.1750" Y="0.4051" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="226" G="106" B="12" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="charSmallIcon01" ActionTag="1467084786" Tag="286" IconVisible="False" LeftMargin="221.6685" RightMargin="17.3315" TopMargin="0.5000" BottomMargin="-4.5000" ctype="SpriteObjectData">
                        <Size X="81.0000" Y="83.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="262.1685" Y="37.0000" />
                        <Scale ScaleX="0.2000" ScaleY="0.2000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8193" Y="0.4684" />
                        <PreSize X="0.2531" Y="1.0506" />
                        <FileData Type="Normal" Path="guiImage/ui_character_lvlbg_profile.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Sprite_22" ActionTag="-728565474" Tag="287" IconVisible="False" LeftMargin="246.2881" RightMargin="31.7119" TopMargin="26.9392" BottomMargin="12.0608" ctype="SpriteObjectData">
                        <Size X="42.0000" Y="40.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="267.2881" Y="32.0608" />
                        <Scale ScaleX="0.3000" ScaleY="0.3000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8353" Y="0.4058" />
                        <PreSize X="0.1312" Y="0.5063" />
                        <FileData Type="Normal" Path="guiImage/ui_character_shard_bg_pizza.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="upgradeFragText" ActionTag="1469487996" Tag="288" IconVisible="False" LeftMargin="273.0000" RightMargin="9.0000" TopMargin="26.0000" BottomMargin="21.0000" FontSize="20" LabelText="30" OutlineSize="2" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="38.0000" Y="32.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="273.0000" Y="37.0000" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.8531" Y="0.4684" />
                        <PreSize X="0.1187" Y="0.4051" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="226" G="106" B="12" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="charSmallIcon02" ActionTag="1793618739" Tag="289" IconVisible="False" LeftMargin="-1.8164" RightMargin="240.8164" TopMargin="1.5000" BottomMargin="-5.5000" ctype="SpriteObjectData">
                        <Size X="81.0000" Y="83.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="38.6836" Y="36.0000" />
                        <Scale ScaleX="0.2000" ScaleY="0.2000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1209" Y="0.4557" />
                        <PreSize X="0.2531" Y="1.0506" />
                        <FileData Type="Normal" Path="guiImage/ui_character_lvlbg_profile.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="Sprite_22_0" ActionTag="974264888" Tag="290" IconVisible="False" LeftMargin="22.8030" RightMargin="255.1970" TopMargin="27.0000" BottomMargin="12.0000" ctype="SpriteObjectData">
                        <Size X="42.0000" Y="40.0000" />
                        <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                        <Position X="43.8030" Y="32.0000" />
                        <Scale ScaleX="0.3000" ScaleY="0.3000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1369" Y="0.4051" />
                        <PreSize X="0.1312" Y="0.5063" />
                        <FileData Type="Normal" Path="guiImage/ui_character_shard_bg_pizza.png" Plist="" />
                        <BlendFunc Src="1" Dst="771" />
                      </AbstractNodeData>
                      <AbstractNodeData Name="collectedFragText" ActionTag="32743165" Tag="291" IconVisible="False" LeftMargin="51.9546" RightMargin="232.0454" TopMargin="28.2424" BottomMargin="22.7576" FontSize="20" LabelText="30" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                        <Size X="36.0000" Y="28.0000" />
                        <AnchorPoint ScaleY="0.5000" />
                        <Position X="51.9546" Y="36.7576" />
                        <Scale ScaleX="0.5000" ScaleY="0.5000" />
                        <CColor A="255" R="255" G="255" B="255" />
                        <PrePosition X="0.1624" Y="0.4653" />
                        <PreSize X="0.1125" Y="0.3544" />
                        <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                        <OutlineColor A="255" R="255" G="0" B="0" />
                        <ShadowColor A="255" R="110" G="110" B="110" />
                      </AbstractNodeData>
                    </Children>
                    <AnchorPoint />
                    <Position />
                    <Scale ScaleX="1.0000" ScaleY="1.0000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="1.0000" Y="0.5267" />
                    <SingleColor A="255" R="150" G="200" B="255" />
                    <FirstColor A="255" R="150" G="200" B="255" />
                    <EndColor A="255" R="255" G="255" B="255" />
                    <ColorVector ScaleY="1.0000" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition />
                <PreSize X="1.0000" Y="0.2868" />
                <SingleColor A="255" R="150" G="200" B="255" />
                <FirstColor A="255" R="150" G="200" B="255" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="0.9208" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="ui_main_topbar_1" ActionTag="1291219945" Tag="827" IconVisible="False" VerticalEdge="TopEdge" RightMargin="-320.0000" BottomMargin="476.0000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="92.0000" />
            <AnchorPoint ScaleY="1.0000" />
            <Position Y="568.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition Y="1.0000" />
            <PreSize X="2.0000" Y="0.1620" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbBtn" ActionTag="-509644776" VisibleForFrame="False" Tag="819" IconVisible="False" HorizontalEdge="LeftEdge" VerticalEdge="TopEdge" LeftMargin="-35.0650" RightMargin="163.0650" TopMargin="-2.0000" BottomMargin="513.0000" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="162" Scale9Height="35" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="192.0000" Y="57.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="60.9350" Y="541.5000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1904" Y="0.9533" />
            <PreSize X="0.6000" Y="0.1004" />
            <TextColor A="255" R="65" G="65" B="70" />
            <NormalFileData Type="Normal" Path="guiImage/ui_general_fb.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="diamondPanel" ActionTag="1998893345" Tag="820" IconVisible="False" HorizontalEdge="RightEdge" VerticalEdge="TopEdge" LeftMargin="215.0000" RightMargin="-15.0000" TopMargin="13.9900" BottomMargin="529.0100" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="120.0000" Y="25.0000" />
            <Children>
              <AbstractNodeData Name="sprite" ActionTag="-845414133" Tag="821" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="-31.4004" RightMargin="-10.5996" TopMargin="-9.0000" BottomMargin="-6.0000" ctype="SpriteObjectData">
                <Size X="162.0000" Y="40.0000" />
                <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
                <Position X="60.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5000" Y="0.5000" />
                <PreSize X="1.3500" Y="1.6000" />
                <FileData Type="Normal" Path="guiImage/ui_main_topbar_diamond.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="totalDiamondText" ActionTag="682514910" Tag="822" IconVisible="False" PositionPercentYEnabled="True" LeftMargin="-30.0000" RightMargin="30.0000" TopMargin="-7.5000" BottomMargin="-7.5000" IsCustomSize="True" FontSize="18" LabelText="122344" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
                <Size X="120.0000" Y="40.0000" />
                <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
                <Position X="90.0000" Y="12.5000" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.7500" Y="0.5000" />
                <PreSize X="1.0000" Y="1.6000" />
                <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
                <OutlineColor A="255" R="1" G="44" B="98" />
                <ShadowColor A="255" R="0" G="0" B="0" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="1.0000" />
            <Position X="335.0000" Y="529.0100" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="1.0469" Y="0.9314" />
            <PreSize X="0.3750" Y="0.0440" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="starSprite" ActionTag="305541660" Tag="824" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="91.4933" RightMargin="62.5067" TopMargin="4.9425" BottomMargin="523.0575" ctype="SpriteObjectData">
            <Size X="166.0000" Y="40.0000" />
            <AnchorPoint ScaleX="0.5642" ScaleY="0.4625" />
            <Position X="185.1505" Y="541.5575" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5786" Y="0.9534" />
            <PreSize X="0.5188" Y="0.0704" />
            <FileData Type="Normal" Path="guiImage/ui_main_topbar_money.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="totalCoinText" ActionTag="-157674906" Tag="823" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="114.3221" RightMargin="105.6779" TopMargin="7.6674" BottomMargin="520.3326" IsCustomSize="True" FontSize="18" LabelText="555" HorizontalAlignmentType="HT_Right" VerticalAlignmentType="VT_Center" OutlineSize="3" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="100.0000" Y="40.0000" />
            <AnchorPoint ScaleX="1.0000" ScaleY="0.5000" />
            <Position X="214.3221" Y="540.3326" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6698" Y="0.9513" />
            <PreSize X="0.3125" Y="0.0704" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="1" G="44" B="98" />
            <ShadowColor A="255" R="0" G="0" B="0" />
          </AbstractNodeData>
          <AbstractNodeData Name="fbName" ActionTag="595426444" Tag="825" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="56.0000" RightMargin="231.0000" TopMargin="11.0000" BottomMargin="523.0000" FontSize="20" LabelText="CC" VerticalAlignmentType="VT_Center" OutlineSize="3" OutlineEnabled="True" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="33.0000" Y="34.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="56.0000" Y="540.0000" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1750" Y="0.9507" />
            <PreSize X="0.1031" Y="0.0599" />
            <FontResource Type="Normal" Path="ObelixPro-cyr.ttf" Plist="" />
            <OutlineColor A="255" R="182" G="88" B="14" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="profilePicFrame" ActionTag="872773553" Tag="826" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="15.9350" RightMargin="252.0650" TopMargin="-0.2800" BottomMargin="516.2800" ctype="SpriteObjectData">
            <Size X="52.0000" Y="52.0000" />
            <AnchorPoint ScaleY="0.5000" />
            <Position X="15.9350" Y="542.2800" />
            <Scale ScaleX="0.6000" ScaleY="0.6000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.0498" Y="0.9547" />
            <PreSize X="0.1625" Y="0.0915" />
            <FileData Type="Normal" Path="guiImage/ui_result_leaderboard_profilepicBorder.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="blockPanel" ActionTag="344292484" VisibleForFrame="False" Tag="1038" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="568.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>