<GameFile>
  <PropertyGroup Name="GmDogSelectDialog" Type="Layer" ID="75d0bc93-af2a-4e4b-8862-e54b0485ae5c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="106" ctype="GameLayerObjectData">
        <Size X="320.0000" Y="500.0000" />
        <Children>
          <AbstractNodeData Name="Panel_2" ActionTag="-379647494" Tag="117" IconVisible="False" PercentWidthEnable="True" PercentHeightEnable="True" PercentWidthEnabled="True" PercentHeightEnabled="True" TouchEnable="True" ClipAble="False" ComboBoxIndex="2" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="500.0000" />
            <AnchorPoint />
            <Position />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition />
            <PreSize X="1.0000" Y="1.0000" />
            <SingleColor A="255" R="26" G="26" B="26" />
            <FirstColor A="255" R="3" G="3" B="155" />
            <EndColor A="255" R="26" G="26" B="26" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
          <AbstractNodeData Name="backButton" ActionTag="1763652582" Tag="115" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-48.5810" RightMargin="188.5810" TopMargin="-21.5132" BottomMargin="427.5132" TouchEnable="True" FontSize="25" ButtonText="Back" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="94.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="41.4190" Y="474.5132" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.1294" Y="0.9490" />
            <PreSize X="0.5625" Y="0.1880" />
            <FontResource Type="Default" Path="" Plist="" />
            <TextColor A="255" R="30" G="144" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="resetButton" ActionTag="1127664739" Tag="70" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="34.9312" RightMargin="105.0688" TopMargin="-20.2575" BottomMargin="426.2575" TouchEnable="True" FontSize="25" ButtonText="Reset" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="94.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="124.9312" Y="473.2575" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.3904" Y="0.9465" />
            <PreSize X="0.5625" Y="0.1880" />
            <FontResource Type="Default" Path="" Plist="" />
            <TextColor A="255" R="30" G="144" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="upgradeAllButton" ActionTag="446096609" Tag="134" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="108.9304" RightMargin="31.0696" TopMargin="-20.2575" BottomMargin="426.2575" TouchEnable="True" FontSize="25" ButtonText="Upgrade All" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="94.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="198.9304" Y="473.2575" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.6217" Y="0.9465" />
            <PreSize X="0.5625" Y="0.1880" />
            <FontResource Type="Default" Path="" Plist="" />
            <TextColor A="255" R="30" G="144" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="unlockAllButton" ActionTag="-1922807092" Tag="71" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="184.1532" RightMargin="-44.1532" TopMargin="-20.2698" BottomMargin="426.2698" TouchEnable="True" FontSize="25" ButtonText="Unlock all" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="180.0000" Y="94.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="274.1532" Y="473.2698" />
            <Scale ScaleX="0.5000" ScaleY="0.5000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8567" Y="0.9465" />
            <PreSize X="0.5625" Y="0.1880" />
            <FontResource Type="Default" Path="" Plist="" />
            <TextColor A="255" R="30" G="144" B="255" />
            <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="mainPanel" ActionTag="-2005729697" Tag="108" IconVisible="False" PositionPercentXEnabled="True" PositionPercentYEnabled="True" TopMargin="75.0000" BottomMargin="75.0000" TouchEnable="True" ClipAble="False" BackColorAlpha="102" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ctype="PanelObjectData">
            <Size X="320.0000" Y="350.0000" />
            <Children>
              <AbstractNodeData Name="infoScrollView" ActionTag="858484759" Tag="153" IconVisible="False" TopMargin="64.6203" BottomMargin="-14.6203" TouchEnable="True" ClipAble="True" BackColorAlpha="178" ComboBoxIndex="1" ColorAngle="90.0000" Scale9Width="1" Scale9Height="1" ScrollDirectionType="Vertical" ctype="ScrollViewObjectData">
                <Size X="320.0000" Y="300.0000" />
                <Children>
                  <AbstractNodeData Name="statText" ActionTag="859259823" Tag="116" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="-133.0600" RightMargin="-126.9400" TopMargin="5.0000" BottomMargin="-305.0000" IsCustomSize="True" FontSize="25" LabelText="Play Count: 10&#xA;Best Distance: 100&#xA;Star Collected: 2000&#xA;Star In One Game: 200&#xA;" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                    <Size X="580.0000" Y="600.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="1.0000" />
                    <Position X="156.9400" Y="295.0000" />
                    <Scale ScaleX="0.5000" ScaleY="0.5000" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition X="0.4904" Y="0.9833" />
                    <PreSize X="1.8125" Y="2.0000" />
                    <FontResource Type="Normal" Path="Arial Rounded Bold.ttf" Plist="" />
                    <OutlineColor A="255" R="255" G="0" B="0" />
                    <ShadowColor A="255" R="110" G="110" B="110" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position Y="-14.6203" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition Y="-0.0418" />
                <PreSize X="1.0000" Y="0.8571" />
                <SingleColor A="255" R="0" G="0" B="0" />
                <FirstColor A="255" R="255" G="150" B="100" />
                <EndColor A="255" R="255" G="255" B="255" />
                <ColorVector ScaleY="1.0000" />
                <InnerNodeSize Width="320" Height="300" />
              </AbstractNodeData>
              <AbstractNodeData Name="spriteNode" ActionTag="-308585620" Tag="118" IconVisible="True" PositionPercentXEnabled="True" PositionPercentYEnabled="True" LeftMargin="115.0880" RightMargin="204.9120" TopMargin="33.0353" BottomMargin="316.9647" ctype="SingleNodeObjectData">
                <Size X="0.0000" Y="0.0000" />
                <Children>
                  <AbstractNodeData Name="referenceSprite" ActionTag="-1928512194" Tag="109" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-23.0000" RightMargin="-23.0000" TopMargin="-23.0000" BottomMargin="-23.0000" ctype="SpriteObjectData">
                    <Size X="46.0000" Y="46.0000" />
                    <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                    <Position />
                    <Scale ScaleX="0.2500" ScaleY="0.2500" />
                    <CColor A="255" R="255" G="255" B="255" />
                    <PrePosition />
                    <PreSize X="0.0000" Y="0.0000" />
                    <FileData Type="Default" Path="Default/Sprite.png" Plist="" />
                    <BlendFunc Src="1" Dst="771" />
                  </AbstractNodeData>
                </Children>
                <AnchorPoint />
                <Position X="115.0880" Y="316.9647" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3596" Y="0.9056" />
                <PreSize X="0.0000" Y="0.0000" />
              </AbstractNodeData>
              <AbstractNodeData Name="nameText" ActionTag="-1600540151" Tag="110" IconVisible="False" LeftMargin="-90.8904" RightMargin="-89.1096" TopMargin="-46.6761" BottomMargin="333.6761" IsCustomSize="True" FontSize="50" LabelText="Dolly" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="500.0000" Y="63.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="159.1096" Y="365.1761" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4972" Y="1.0434" />
                <PreSize X="1.5625" Y="0.1800" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="nextButton" ActionTag="1871950082" Tag="113" IconVisible="False" LeftMargin="277.0208" RightMargin="-7.0208" TopMargin="-15.2570" BottomMargin="265.2570" TouchEnable="True" FontSize="18" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="20" Scale9Height="78" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="50.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="302.0208" Y="315.2570" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.9438" Y="0.9007" />
                <PreSize X="0.1563" Y="0.2857" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="0" />
                <NormalFileData Type="Normal" Path="guiImage/btn_arrow_right.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lastButton" ActionTag="595610848" Tag="114" IconVisible="False" LeftMargin="-4.1950" RightMargin="274.1950" TopMargin="-15.3962" BottomMargin="265.3962" TouchEnable="True" FontSize="18" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="20" Scale9Height="78" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="50.0000" Y="100.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="20.8050" Y="315.3962" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0650" Y="0.9011" />
                <PreSize X="0.1563" Y="0.2857" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="255" G="255" B="0" />
                <NormalFileData Type="Normal" Path="guiImage/btn_arrow_left.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="dogIDText" ActionTag="1365407077" Tag="119" IconVisible="False" VerticalEdge="TopEdge" LeftMargin="2.4488" RightMargin="289.5512" TopMargin="-24.3356" BottomMargin="355.3356" FontSize="15" LabelText="001" HorizontalAlignmentType="HT_Right" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="28.0000" Y="19.0000" />
                <AnchorPoint ScaleY="0.5000" />
                <Position X="2.4488" Y="364.8356" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.0077" Y="1.0424" />
                <PreSize X="0.0875" Y="0.0543" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockButton" ActionTag="652358651" Tag="67" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="-39.3598" RightMargin="199.3598" TopMargin="344.9645" BottomMargin="-88.9645" TouchEnable="True" FontSize="26" ButtonText="Unlock" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="40.6402" Y="-41.9645" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.1270" Y="-0.1199" />
                <PreSize X="0.5000" Y="0.2686" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="53" G="53" B="94" />
                <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="upgradeButton" ActionTag="-850715858" Tag="154" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="28.0320" RightMargin="131.9680" TopMargin="344.9645" BottomMargin="-88.9645" TouchEnable="True" FontSize="26" ButtonText="Upgrade" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="108.0320" Y="-41.9645" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.3376" Y="-0.1199" />
                <PreSize X="0.5000" Y="0.2686" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="53" G="53" B="94" />
                <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockText" ActionTag="1350409294" Tag="69" IconVisible="False" PositionPercentXEnabled="True" LeftMargin="231.4360" RightMargin="-0.4360" TopMargin="-25.3050" BottomMargin="350.3050" IsCustomSize="True" FontSize="20" LabelText="unlocked" HorizontalAlignmentType="HT_Right" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
                <Size X="89.0000" Y="25.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="275.9360" Y="362.8050" />
                <Scale ScaleX="1.0000" ScaleY="1.0000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8623" Y="1.0366" />
                <PreSize X="0.2781" Y="0.0714" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="lockedGUISprite" ActionTag="1349590228" VisibleForFrame="False" Tag="202" IconVisible="False" LeftMargin="86.6073" RightMargin="136.3927" TopMargin="111.4142" BottomMargin="141.5858" FlipX="True" ctype="SpriteObjectData">
                <Size X="97.0000" Y="97.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="135.1073" Y="190.0858" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.4222" Y="0.5431" />
                <PreSize X="0.3031" Y="0.2771" />
                <FileData Type="Normal" Path="dog/dselect_dog2.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="unlockGUISprite" ActionTag="-1778271707" Tag="203" IconVisible="False" LeftMargin="109.4375" RightMargin="66.5625" TopMargin="-42.1446" BottomMargin="248.1446" FlipX="True" ctype="SpriteObjectData">
                <Size X="144.0000" Y="144.0000" />
                <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
                <Position X="181.4375" Y="320.1446" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.5670" Y="0.9147" />
                <PreSize X="0.4500" Y="0.4114" />
                <FileData Type="Normal" Path="dog/dselect_dog2_unlock.png" Plist="" />
                <BlendFunc Src="1" Dst="771" />
              </AbstractNodeData>
              <AbstractNodeData Name="popUnlockButton" ActionTag="1274488076" Tag="205" IconVisible="False" LeftMargin="199.4678" RightMargin="-39.4678" TopMargin="340.2833" BottomMargin="-84.2833" TouchEnable="True" FontSize="26" ButtonText="Popup1" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5013" ScaleY="0.4004" />
                <Position X="279.6758" Y="-46.6457" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.8740" Y="-0.1333" />
                <PreSize X="0.5000" Y="0.2686" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="53" G="53" B="94" />
                <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
              <AbstractNodeData Name="popBuyButton" ActionTag="1521353368" Tag="206" IconVisible="False" LeftMargin="125.9440" RightMargin="34.0560" TopMargin="340.2833" BottomMargin="-84.2833" TouchEnable="True" FontSize="26" ButtonText="Popup2" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="174" Scale9Height="72" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
                <Size X="160.0000" Y="94.0000" />
                <AnchorPoint ScaleX="0.5013" ScaleY="0.4004" />
                <Position X="206.1520" Y="-46.6457" />
                <Scale ScaleX="0.5000" ScaleY="0.5000" />
                <CColor A="255" R="255" G="255" B="255" />
                <PrePosition X="0.6442" Y="-0.1333" />
                <PreSize X="0.5000" Y="0.2686" />
                <FontResource Type="Normal" Path="Caviar_Dreams_Bold.ttf" Plist="" />
                <TextColor A="255" R="53" G="53" B="94" />
                <NormalFileData Type="Normal" Path="guiImage/ui_inbox_btn_on.png" Plist="" />
                <OutlineColor A="255" R="255" G="0" B="0" />
                <ShadowColor A="255" R="110" G="110" B="110" />
              </AbstractNodeData>
            </Children>
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="160.0000" Y="250.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="0.7000" />
            <SingleColor A="255" R="150" G="200" B="255" />
            <FirstColor A="255" R="150" G="200" B="255" />
            <EndColor A="255" R="255" G="255" B="255" />
            <ColorVector ScaleY="1.0000" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>